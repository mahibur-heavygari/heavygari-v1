<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// main website
Route::get('/', 'HomeController@index');
Route::post('/contact-us', 'HomeController@postContactUs');
Route::post('/business-request', 'HomeController@businessRequest');
Route::post('/cost-calculator', 'HomeController@getCost');
Route::get('/page/{slug}', 'PageController@show');
Route::get('/booking/{booking}', 'BookingController@show');
Route::get('/authorized-points', 'HomeController@showAuthorizedPoints');
Route::get('/arp-howto', 'HomeController@showArpHowToPage');
//notifications
Route::post('get-notifications', 'NotificationBarController@getNotifications');
Route::post('change-notification-read-status', 'NotificationBarController@changeNotificationReadStatus');

//this route redirect to booking invoice. at accept time, invoices are generated after email sending to customer
Route::get('/invoice/{unique_id}', 'Customer\MyBookingsController@redirectToInvoice');

// login, logout, activation..
Route::group(['prefix' => 'session'], function() {
    //login-logout
	Route::get('login', 'SessionController@create');
	Route::post('login', 'SessionController@store');
	Route::get('logout', 'SessionController@logout');
    //Route::get('loggedin_user', 'SessionController@currentUser');?
    Route::get('redirect-to-dashboard', 'SessionController@redirectToDashBoard');

    Route::get('verify/{user}/resend', 'SessionController@resendVerification');

    # forgot password
    Route::get('login/forgot_password', 'ForgotPasswordController@create');
    Route::post('login/forgot_password', 'ForgotPasswordController@store');
    Route::get('recover/password', 'ForgotPasswordController@resetPassword');
    Route::post('recover/password', 'ForgotPasswordController@resetPasswordStore');

});

//vehicle owner specific routes
Route::group(['prefix' => 'owner'], function() {

    // Registration steps
    Route::group(['prefix' => 'register'], function() {

        // registration form
        Route::get('/', 'Owner\RegistrationController@create');
        Route::post('/', 'Owner\RegistrationController@store');

        // mobile verify
        Route::get('{owner}/verify-phone', 'Owner\RegistrationController@verifyPhone');
        Route::post('{owner}/verify-phone', 'Owner\RegistrationController@storeVerifyPhone');

        // complete profile
        Route::get('{owner}/profile/complete', 'Owner\RegistrationController@complete');
        Route::post('{owner}/profile/complete', 'Owner\RegistrationController@storeComplete');
    }); 

    //only logged in owner can access
    Route::group(['prefix' => 'panel', 'middleware' => 'sentinel-auth:owner'], function() {

        // only can access these pages if the account is fully active 
        // (sms verified + profile compelte + active account)
        Route::group(['middleware' => 'complete-profile:owner'], function() {

            //dashboard
            Route::get('dashboard', 'Owner\DashboardController@index');

            //profile
            Route::get('profile', 'Owner\ProfileController@show');
            Route::get('profile/edit', 'Owner\ProfileController@edit');        
            Route::put('profile', 'Owner\ProfileController@update');     
            Route::post('update_fcm', 'Owner\ProfileController@updateWebFcmToken');

            //vehicles        
            Route::resource('vehicles', 'Owner\VehicleController');

            //drivers
            Route::get('drivers/make-me-driver', 'Owner\DriverController@makeMeDriver');
            Route::get('drivers/verify/{driver}', 'Owner\DriverController@verifyDriver');
            Route::post('drivers/verify/{driver}', 'Owner\DriverController@verifyDriverPost');
            Route::get('drivers/verify/{driver}/resend', 'Owner\DriverController@resendVerification');
            Route::put('drivers/{driver}/change_password', 'Owner\DriverController@changeDriverPassword');
            Route::put('drivers/{driver}/preferences', 'Owner\DriverController@changeDriverPreferences');
            Route::resource('drivers', 'Owner\DriverController');

            //trips
            Route::get('trips-by-my-vehicles/{unique_id}/accept', 'Owner\BookingController@accept');
            Route::post('trips-by-my-vehicles/{unique_id}/accept', 'Owner\BookingController@acceptStore');
            Route::get('trips-by-my-vehicles/{unique_id}/start', 'Owner\BookingController@startTrip');
            Route::get('trips-by-my-vehicles/{unique_id}/complete', 'Owner\BookingController@completeTrip');
            Route::get('trips-by-my-vehicles/{unique_id}/cancel', 'Owner\BookingController@cancelTrip');
            Route::get('trips-by-my-vehicles/{unique_id}/rate_customer', 'Owner\BookingController@rateCustomer');
            Route::post('trips-by-my-vehicles/{unique_id}/rate_customer', 'Owner\BookingController@rateCustomerStore');
            Route::resource('trips-by-my-vehicles', 'Owner\BookingController');

            //invoices
            Route::resource('invoices', 'Owner\InvoiceController');

            //transaction_history
             Route::get('transactions', 'Owner\TransactionController@getTransactions');
            
            //settings
            Route::get('settings/password', 'Owner\SettingsController@changePassword');
            Route::post('settings/password', 'Owner\SettingsController@storeChangePassword');

            //contact us
            Route::get('contact-us', 'Owner\FeedbackController@create');
            Route::post('save-contact-us', 'Owner\FeedbackController@store');

        });
    });
});

//customer specific routes
Route::group(['prefix' => 'customer'], function() {

    // Registration steps
    Route::group(['prefix' => 'register'], function() {

        // registration form
        Route::get('/', 'Customer\RegistrationController@create');
        Route::post('/', 'Customer\RegistrationController@store');

        // mobile verify
        Route::get('{customer}/verify-phone', 'Customer\RegistrationController@verifyPhone');
        Route::post('{customer}/verify-phone', 'Customer\RegistrationController@storeVerifyPhone');

        // complete profile
        Route::get('{customer}/profile/complete', 'Customer\RegistrationController@complete');
        Route::post('{customer}/profile/complete', 'Customer\RegistrationController@storeComplete');

    });

    //only logged in customer can access
    Route::group(['prefix' => 'panel', 'middleware' => 'sentinel-auth:customer'], function() {

        // only can access these pages if the account is fully active 
        // (sms verified + profile compelte + active account)
        Route::group(['middleware' => 'complete-profile:customer'], function() {

            //profile
            Route::get('profile', 'Customer\ProfileController@show');
            Route::get('profile/edit', 'Customer\ProfileController@edit');
            Route::put('profile', 'Customer\ProfileController@update');  
            Route::post('update_fcm', 'Customer\ProfileController@updateWebFcmToken'); 

            //dashboard
            Route::get('dashboard', 'Customer\DashboardController@index');

            //settings
            Route::get('settings/password', 'Customer\SettingsController@changePassword');
            Route::post('settings/password', 'Customer\SettingsController@storeChangePassword');
           
            //popup notification
            Route::post('popup','Customer\CustomPopUpNotificationController@popupNotification');
            
            //new booking

            #full booking
            Route::get('bookings/full/create', 'Customer\FullBookingController@create');
            Route::post('bookings/full/get_fare', 'Customer\FullBookingController@showEstimatedFare');
            Route::post('bookings/full/confirm', 'Customer\FullBookingController@store');

            #parcel delivery
            Route::get('bookings/parcel/create', 'Customer\SharedBookingController@create');
            Route::post('bookings/parcel/get_fare', 'Customer\SharedBookingController@showEstimatedFare');
            Route::post('bookings/parcel/confirm', 'Customer\SharedBookingController@store');

            //my bookings
            Route::resource('bookings', 'Customer\MyBookingsController');
            Route::get('bookings/{unique_id}/cancel', 'Customer\MyBookingsController@cancelTrip');

            //transaction
            Route::get('transactions', 'Customer\TransactionController@getTransactions');

            //invoice
            Route::get('bookings/{unique_id}/invoice', 'Customer\MyBookingsController@showInvoice');

            //rate drivers
            Route::get('bookings/{unique_id}/rate_driver', 'Customer\MyBookingsController@rateDriver');
            Route::post('bookings/{unique_id}/rate_driver', 'Customer\MyBookingsController@rateDriverStore');

            //contact us
            Route::get('contact-us', 'Customer\FeedbackController@create');
            Route::post('save-contact-us', 'Customer\FeedbackController@store');
            
            //corporate user management
            Route::get('users', 'Customer\CorporateUserController@users');
            Route::get('users/create', 'Customer\CorporateUserController@createUser');
            Route::post('users/store', 'Customer\CorporateUserController@storeUser');
            Route::get('users/{id}', 'Customer\CorporateUserController@showUser');

            //payments
            //Route::resource('payments', 'Customer\PaymentMethodController');
        });
    });
});

//admin specific routes
Route::group(['prefix' => 'cms', 'middleware' => 'sentinel-auth:super-admin'], function() {

    //only logged in admins can access
    //Route::group(['prefix' => 'panel', /*'middleware' => 'super-admin'*/], function() {
        
        //profile
        Route::get('profile', 'CMS\ProfileController@show');
        
        //settings
        Route::get('settings', 'CMS\ProfileController@show');

        //dashboard
        Route::get('dashboard', 'CMS\DashboardController@index');

        //website
        Route::group(['prefix' => 'website'], function() {
            Route::resource('features', 'CMS\Website\FeaturesController');
        });

        //base values
        Route::group(['prefix' => 'base'], function() {
            Route::resource('points', 'CMS\Base\PointController');
            Route::get('points/{id}/delete', 'CMS\Base\PointController@delete');
            Route::resource('trip_categories', 'CMS\Base\TripCategoryController');
            Route::get('trip_categories/{id}/delete', 'CMS\Base\TripCategoryController@delete');
            Route::resource('vehicle_types', 'CMS\Base\VehicleTypeController');
            Route::get('vehicle_types/{id}/delete', 'CMS\Base\VehicleTypeController@delete');
            Route::resource('capacity_types', 'CMS\Base\VehicleCapacityTypeController');
            Route::get('capacity_types/{id}/delete', 'CMS\Base\VehicleCapacityTypeController@delete');
            Route::resource('product_categories', 'CMS\Base\ProductCategoryController');
            Route::resource('weight_categories', 'CMS\Base\WeightCategoryController');
            Route::resource('volumetric_weight_categories', 'CMS\Base\VolumetricWeightCategoryController');
            Route::get('trip_categories/{id}/vehicle_wise_charge', 'CMS\Base\TripCategoryController@showVehicles');
            Route::post('trip_categories/vehicle_wise_charge', 'CMS\Base\TripCategoryController@saveCharges');
            Route::get('settings', 'CMS\Base\SettingsController@showSettings');
            Route::post('settings/driver_comission', 'CMS\Base\SettingsController@saveDriverComission');
        });

        //different types of users
        Route::group(['prefix' => 'users'], function() {
            //Route::resource('all', 'CMS\Users\AllUserController');
            Route::resource('customers', 'CMS\Users\CustomerController');            
            Route::resource('owners', 'CMS\Users\OwnerController');
            Route::get('owners/change_mobile_app_status/{owner_profile}', 'CMS\Users\OwnerController@changeMobileAppStatus');
            Route::resource('drivers', 'CMS\Users\DriverController');
            Route::post('activate/{user}', 'CMS\Users\UserController@activateUser');
            Route::post('block/{user}', 'CMS\Users\UserController@blockUser');
            Route::get('customers/{user}/transactions', 'CMS\TransactionController@customerTransactions'); 
            Route::get('owners/{user}/transactions', 'CMS\TransactionController@ownerTransactions');  
            Route::get('drivers/{user}/transactions', 'CMS\TransactionController@driverTransactions');            
            Route::post('owners/{user}/save-note', 'CMS\Users\OwnerController@saveNote');         
            Route::resource('potential_owners', 'CMS\Users\PotentialOwnerController');      
            Route::get('potential_owners/{id}/delete', 'CMS\Users\PotentialOwnerController@delete');         
            Route::resource('potential_customers', 'CMS\Users\PotentialCustomerController');      
            Route::get('potential_customers/{id}/delete', 'CMS\Users\PotentialCustomerController@delete');         
            Route::resource('potential_drivers', 'CMS\Users\PotentialDriverController');      
            Route::get('potential_drivers/{id}/delete', 'CMS\Users\PotentialDriverController@delete');

            Route::resource('field_team', 'CMS\Users\FieldTeamController'); 
                  
            Route::get('field_team/{id}/delete', 'CMS\Users\FieldTeamController@delete');

            Route::get('field_team/{id}/customers', 'CMS\Users\FieldTeamController@customerNumber');

            Route::get('field_team/{id}/owners', 'CMS\Users\FieldTeamController@ownerNumber');

            Route::get('field_team/{id}/drivers', 'CMS\Users\FieldTeamController@driverNumber');

            Route::get('field_team/{id}/vehicles', 'CMS\Users\FieldTeamController@vehicleNumber');
        });
        
        //corporate customers
        Route::resource('/corporate_customers', 'CMS\CorporateCustomer\CorporateCustomerController'); 
        Route::get('corporate_customers/{id}/delete', 'CMS\CorporateCustomer\CorporateCustomerController@delete');
        Route::get('corporate_customers/{id}/users', 'CMS\CorporateCustomer\CorporateCustomerController@users');
        Route::get('corporate_customers/{id}/users/create', 'CMS\CorporateCustomer\CorporateCustomerController@createUser');
        Route::post('corporate_customers/{id}/users/store', 'CMS\CorporateCustomer\CorporateCustomerController@storeUser');
        Route::get('corporate_customers/{id}/users/{customer_id}', 'CMS\CorporateCustomer\CorporateCustomerController@showUser');

        //authorized points
        Route::resource('authorized_points', 'CMS\AuthorizedPointsController');
        Route::get('authorized_points/{authorized_point}/send_sms', 'CMS\AuthorizedPointsController@sendSms');
        Route::post('authorized_points/{authorized_point}/send_sms', 'CMS\AuthorizedPointsController@sendSmsPost');

        //send custom push notification
        Route::get('send/push_notification', 'CMS\CustomPushNotificationController@sendNotification');
        Route::post('send/push_notification', 'CMS\CustomPushNotificationController@sendNotificationPost');
        Route::get('send/push_notification/history', 'CMS\CustomPushNotificationController@getAllNotification');
        //send custom sms
        Route::get('send/sms', 'CMS\CustomSMSController@sendSMS');
        Route::post('send/sms', 'CMS\CustomSMSController@sendSMSPost');
        Route::get('send/sms/history', 'CMS\CustomSMSController@getAllSMS');
        //send custom email
        Route::get('send/email', 'CMS\CustomEmailController@sendemail');
        Route::post('send/email', 'CMS\CustomEmailController@sendEmailPost');
        Route::get('send/email/history', 'CMS\CustomEmailController@getAllEmail');
        Route::get('send/business_email/history', 'CMS\CustomEmailController@getBusinessEmail');

        //send popup notification
        Route::get('send/popup_notification', 'CMS\CustomPopUpNotificationController@popupNotification');
        Route::get('send/popup_notification/history', 'CMS\CustomPopUpNotificationController@getAllPopup'); 

        Route::post('send/post_popup_notification', 'CMS\CustomPopUpNotificationController@postPopupNotification');
        Route::post('make-pop-up-expired', 'CMS\CustomPopUpNotificationController@makeExpired');

        //transaction
        Route::get('transactions', 'CMS\TransactionController@allTransactions');

        //operational accounts
        Route::get('operational_accounts/paid', 'CMS\OperationalAccountController@paidOperations');
        Route::get('operational_accounts/unpaid', 'CMS\OperationalAccountController@unpaidOperations');

        //vehicles
        Route::resource('vehicles', 'CMS\VehicleController');

        //bookings
        Route::get('booking/create', 'CMS\BookingsController@createBooking');
        Route::post('booking/get_fare', 'CMS\BookingsController@showEstimatedFare');
        Route::post('booking/confirm', 'CMS\BookingsController@storeBooking');
        Route::resource('bookings', 'CMS\BookingsController');
        Route::post('authorized-vehicles', 'CMS\BookingsController@authorizedVehicles');


        //trip management
        Route::get('bookings/{unique_id}/accept', 'CMS\BookingsController@accept');
        Route::post('bookings/{unique_id}/accept', 'CMS\BookingsController@acceptStore');
        Route::get('bookings/{unique_id}/start', 'CMS\BookingsController@startTrip');
        Route::get('bookings/{unique_id}/complete', 'CMS\BookingsController@completeTrip');
        Route::get('bookings/{unique_id}/cancel', 'CMS\BookingsController@cancelTrip');
        Route::get('bookings/{unique_id}/edit', 'CMS\BookingsController@edit');
        Route::post('bookings/{unique_id}/edit/get_fare', 'CMS\BookingsController@showEditedEstimatedFare');
        Route::post('bookings/{unique_id}/edit/confirm', 'CMS\BookingsController@confirmEdit');

        //Admin note
        Route::post('bookings/{unique_id}/save_admin_note', 'CMS\BookingsController@saveNote');
        
        //Driver change 
        Route::get('bookings/{unique_id}/driver-change', 'CMS\BookingsController@changeDriver');

        //Extend expiry time of open bookings
        Route::get('bookings/{unique_id}/extend-expiry-time', 'CMS\BookingsController@extendExpiry');
        
        //make open a cancelled trip
        Route::get('bookings/{unique_id}/make-open', 'CMS\BookingsController@makeOpen');

        //msg to all owners and managers to notify a new booking
        Route::get('bookings/{unique_id}/msg-to-owners', 'CMS\BookingsController@msgToOwners');

        //msg to all drivers to notify a new booking
        Route::get('bookings/{unique_id}/msg-to-drivers', 'CMS\BookingsController@msgToDrivers');

        //msg to all owners and managers to notify a new booking
        Route::get('bookings/{unique_id}/msg-to-potential-owners', 'CMS\BookingsController@msgToPotentialOwners');
        
        //show delivery order photo
        Route::get('bookings/{unique_id}/delivery-order-photo', 'CMS\BookingsController@showDeliveryOrderPhoto');
        
        //show delivery invoice photo
        Route::get('bookings/{unique_id}/delivery-invoice-photo', 'CMS\BookingsController@showDeliveryInvoicePhoto');

        //price manager
        Route::group(['prefix' => 'price-manager'], function() {
            Route::resource('full-booking', 'CMS\PriceManager\FullBookingController');
            Route::resource('shared-booking', 'CMS\PriceManager\SharedBookingController');
        });

        //booking price update
        Route::get('bookings/{unique_id}/price-update', 'CMS\BookingsController@priceUpdate');
        Route::post('bookings/{unique_id}/price-update', 'CMS\BookingsController@savePriceUpdate');

        

        //invoices
        Route::get('invoices/cron_job', 'CMS\InvoiceController@cronJob');
        Route::get('invoices/{invoice_id}/pdf', 'CMS\InvoiceController@getPdf');
        Route::resource('invoices', 'CMS\InvoiceController');
        Route::put('invoices/{invoice_number}/mark-as-paid', 'CMS\InvoiceController@markAsPaid');
        Route::get('invoices/{invoice_number}/send-email', 'CMS\InvoiceController@sendEmail');
        Route::post('invoices/{invoice}/save-note', 'CMS\InvoiceController@saveNote'); 
        Route::get('invoices/{invoice_number}/send-sms', 'CMS\InvoiceController@sendSms'); 

        //Driver_payments
        Route::get('payment/drivers', 'CMS\DriverPaymentController@index');
        Route::get('payment/drivers/{id}/commissions', 'CMS\DriverPaymentController@commissions');
        Route::post('payment/drivers/commissions/save', 'CMS\DriverPaymentController@makePayment');
        
        
        //configs
        Route::group(['prefix' => 'configs'], function() {
            # Route::resource('push-texts', 'CMS\Config\PushTextController');
        });

        //search ref no
        Route::post('search-ref', 'CMS\DashboardController@searchByRefNo');

        //User feedbacks
        Route::get('feedbacks_and_ratings/user_feedbacks', 'CMS\FeedbackController@userFeedback');

        //Public feedback
        Route::get('feedbacks_and_ratings/public_feedbacks', 'CMS\FeedbackController@publicFeedback');

        //customer-rating
        Route::get('feedbacks_and_ratings/customer_ratings', 'CMS\RatingController@customerRating'); 

        //driver-rating
        Route::get('feedbacks_and_ratings/driver_ratings', 'CMS\RatingController@driverRating');

        //complain
        Route::get('complain/get_complain', 'CMS\ComplainController@getComplain');
        Route::post('complain/save_complain', 'CMS\ComplainController@saveComplain');
        Route::get('complain/history_complain', 'CMS\ComplainController@historyComplain');
        Route::get('complain/edit/{complain_id}/', 'CMS\ComplainController@edit');
        Route::post('complain/update/{complain_id}/', 'CMS\ComplainController@update');

    //});
});

# Temporary testing will be done through these routes
Route::group(['prefix' => 'test'], function () {
    Route::get('test', 'TestController@test');    
    Route::get('filesystem', 'TestController@fileSystem');    
    Route::post('filesystem', 'TestController@fileSystemStore');    
    Route::get('pdf/{unique_id}', 'TestController@pdfTest');
    Route::get('is-queue-running', 'TestController@testQueue');
    Route::get('send-sms', 'TestController@sendSMS');
    Route::get('send-email', 'TestController@sendEmail');
    Route::get('send-push/{booking_unique_id}/{type}/{token}', 'TestController@sendPush');
    Route::get('send-web-push/{booking_unique_id}/{type}/{token}', 'TestController@sendWebPush');
    Route::get('owner-invoice/{owner_profile}', 'TestController@invoiceTest');
    Route::get('driver_commission_create', 'TestController@createCommission');

    # For debuggin purposes
    Route::group(['prefix' => 'debugger'], function () {
        Route::get('build-trip/{demo_trip_id}', 'TestController@demoTripBuilder');
        Route::get('demo/{demo_trip_id}', 'TestController@demoTracker');
    });
});



Route::get('/unauthorized', function () {
    return view('errors.401');
});