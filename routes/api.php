<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/'], function() {
	Route::get('/page/{slug}', 'API\GeneralController@showPage');
});

Route::group(['prefix' => 'v1/driver'], function() {

	// Login
	Route::group(['prefix' => 'authentication','Cors'], function() {
		Route::post('/login', 'API\Driver\Authentication\LoginController@store');
		Route::post('/register/{driver_id}/verify', 'API\Driver\Authentication\RegistrationController@phoneVerify');
		Route::post('/register/{driver_id}/resend_code', 'API\Driver\Authentication\RegistrationController@resendVerificationCode');
		Route::post('/login/forgot_password', 'API\Driver\Authentication\ForgotPasswordController@store');
	});


	///////////////////////////////
	// Need Authorization Token  //
	///////////////////////////////

	Route::group(['middleware' => 'api-auth:driver'], function() {

		Route::post('/authentication/logout', 'API\Driver\Authentication\LogoutController@store');

		// me
		Route::group(['prefix' => 'authorized_vehicles'], function() {
			Route::get('/', 'API\Driver\Authentication\AuthorizedVehiclesController@index');
			Route::post('/{vehicle_id}/login', 'API\Driver\Authentication\AuthorizedVehiclesController@store');
		});

		// me
		Route::group(['prefix' => 'me'], function() {
			Route::group(['prefix' => 'duty_mode'], function() {
				Route::get('/', 'API\Driver\DutyController@getStatus');
				Route::post('/on', 'API\Driver\DutyController@onDuty');
				Route::post('/off', 'API\Driver\DutyController@offDuty');
			});
			
			Route::get('/profile', 'API\Driver\UserController@show');
			Route::patch('/profile', 'API\Driver\UserController@update');
			Route::patch('/fcm_registration_token', 'API\Driver\UserController@updateFcmToken');

			Route::get('/preferences', 'API\Driver\MyPreferencesController@getPreferences');
			Route::post('/preferences', 'API\Driver\MyPreferencesController@updatePreferences');
		});

		// bookings
		Route::group(['prefix' => 'bookings'], function() {
			Route::get('/home', 'API\Driver\BookingController@home');
			Route::get('/available', 'API\Driver\BookingController@available');
			Route::get('/{unique_id}', 'API\Driver\BookingController@show');
			Route::post('{unique_id}/accept', 'API\Driver\BookingController@accept'); // method
		});

		Route::group(['prefix' => 'my_trips'], function() {
			Route::get('/', 'API\Driver\MyTripController@index');
			Route::get('/history', 'API\Driver\MyTripController@history');
			Route::get('/{unique_id}', 'API\Driver\MyTripController@show');
			Route::post('{unique_id}/start', 'API\Driver\MyTripController@start');
			Route::post('{unique_id}/complete', 'API\Driver\MyTripController@complete');
			Route::post('{unique_id}/cancel', 'API\Driver\MyTripController@cancel');
			Route::post('/{unique_id}/rate_customer', 'API\Driver\MyTripController@giveRating');
			Route::post('/{unique_id}/payment-collection', 'API\Driver\MyTripController@updatePaymentStatus');
			Route::post('/{unique_id}/upload_delivery_invoice', 'API\Driver\MyTripController@uploadDeliveryInvoice');
		});

		// bookings
		Route::group(['prefix' => 'transactions'], function() {
			Route::get('/', 'API\Driver\TransactionController@index');
		});

		// general operations
		Route::group(['prefix' => 'general'], function() {
			Route::post('/contact-us', 'API\Driver\GeneralController@PostContactUs');
		});

	});
});

Route::group(['prefix' => 'v1/customer'], function() {

	// Login
	Route::group(['prefix' => 'authentication'], function() {
		Route::post('/login', 'API\Customer\Authentication\LoginController@store');
		Route::post('/register', 'API\Customer\Authentication\RegistrationController@store');
		Route::post('/register/{customer_id}/verify', 'API\Customer\Authentication\RegistrationController@phoneVerify');
		Route::post('/register/{customer_id}/resend_code', 'API\Customer\Authentication\RegistrationController@resendVerificationCode');
		Route::post('/register/{customer}/complete_profile', 'API\Customer\Authentication\RegistrationController@completeProfile');
		Route::post('/login/forgot_password', 'API\Customer\Authentication\ForgotPasswordController@store');
	});


	///////////////////////////////
	// Need Authorization Token  //
	///////////////////////////////

	Route::group(['middleware' => 'api-auth:customer'], function() {

		// me
		Route::group(['prefix' => 'me'], function() {
			Route::get('/profile', 'API\Customer\UserController@show');
			Route::patch('/profile', 'API\Customer\UserController@update');
			Route::patch('/profile/upload_photo', 'API\Customer\UserController@uploadPhoto');
			Route::post('/change_password', 'API\Customer\UserController@changePassword');	
			Route::patch('/fcm_registration_token', 'API\Customer\UserController@updateFcmToken');
		});


		// bookings
		Route::group(['prefix' => 'bookings'], function() {
			Route::post('/full', 'API\Customer\FullBookingController@create');
			Route::post('/full/get_cost', 'API\Customer\FullBookingController@getCost');

			Route::post('/shared', 'API\Customer\SharedBookingController@create');
		    Route::post('/shared/get_cost', 'API\Customer\SharedBookingController@getCost');
		});

		// my bookings
		Route::group(['prefix' => 'my_bookings'], function() {
			Route::get('/', 'API\Customer\MyBookingController@index');
			Route::get('/history', 'API\Customer\MyBookingController@history');
			Route::get('/{unique_id}', 'API\Customer\MyBookingController@show');			
			Route::post('/{unique_id}/cancel', 'API\Customer\MyBookingController@cancel');
			Route::post('/{unique_id}/rate_driver', 'API\Customer\MyBookingController@giveRating');
			Route::get('/track_booking/{unique_id}', 'API\Customer\MyBookingController@trackBooking');
		});

		// bookings
		Route::group(['prefix' => 'transactions'], function() {
			Route::get('/', 'API\Customer\TransactionController@index');
		});

		//base
		Route::group(['prefix' => 'base'], function() {
			Route::get('/vehicle_types', 'API\Customer\BaseController@vehicleTypes');
			Route::get('/points', 'API\Customer\BaseController@points');
			Route::get('/product_categories', 'API\Customer\BaseController@productCategories');
			Route::get('/weight_categories', 'API\Customer\BaseController@weightCategories');
			Route::get('/volumetric_categories', 'API\Customer\BaseController@volumetricCategories');
		});

		// general operations
		Route::group(['prefix' => 'general'], function() {
			Route::post('/contact-us', 'API\Customer\CustomerGeneralController@PostContactUs');
			Route::get('/pop-up', 'API\Customer\CustomerGeneralController@getPopUp');
		});

		Route::post('/authentication/logout', 'API\Customer\Authentication\LogoutController@store');

	});
});

Route::group(['prefix' => 'v1/field-admin'], function() {
	Route::group(['prefix' => 'authentication'], function() {
		Route::post('/login', 'API\FieldAdmin\Authentication\LoginController@store');
	});

	Route::group(['middleware' => 'api-auth:field-admin'], function() {

		//sending sms and email
		Route::post('/send-sms', 'API\FieldAdmin\EmailSMSController@sendSMS');	
		Route::post('/send-email', 'API\FieldAdmin\EmailSMSController@sendEmail');	
		
		//customer crud
		Route::get('users/customers', 'API\FieldAdmin\Customer\CustomerController@index');
		Route::get('users/customers/{id}', 'API\FieldAdmin\Customer\CustomerController@show');
		Route::post('users/customers/store', 'API\FieldAdmin\Customer\CustomerController@store');

		//owner
		Route::get('users/owners', 'API\FieldAdmin\Owner\OwnerController@index');
		Route::get('users/owners/{id}', 'API\FieldAdmin\Owner\OwnerController@show');
		Route::post('users/owners/store', 'API\FieldAdmin\Owner\OwnerController@store');	

		//driver crud
		Route::get('users/owners/{owner_phone}/drivers', 'API\FieldAdmin\Owner\DriverController@index');
		Route::get('users/owners/{owner_id}/drivers/{driver_id}', 'API\FieldAdmin\Owner\DriverController@show');	
		Route::post('users/owners/{owner_phone}/drivers/store', 'API\FieldAdmin\Owner\DriverController@store');

		Route::get('/vehicle_types', 'API\FieldAdmin\Vehicle\VehicleController@vehicleTypes');

		//vehicle crud
		Route::get('users/owners/{owner_id}/vehicles', 'API\FieldAdmin\Vehicle\VehicleController@index');
		Route::get('users/owners/{owner_id}/vehicles/{vehicle_id}', 'API\FieldAdmin\Vehicle\VehicleController@show');
		Route::post('users/owners/{owner_phone}/vehicles/store', 'API\FieldAdmin\Vehicle\VehicleController@store');

		//all history
		Route::get('history', 'API\FieldAdmin\HistoryController@history');
	});
});