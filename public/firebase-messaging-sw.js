importScripts('https://www.gstatic.com/firebasejs/5.7.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.7.2/firebase-messaging.js');

// Initialize Firebase
var config = {
    apiKey: "AIzaSyCpcCajfoD0LBT88TGAyzWUkAEgzOfBWWo",
    authDomain: "heavygari-web.firebaseapp.com",
    databaseURL: "https://heavygari-web.firebaseio.com",
    projectId: "heavygari-web",
    storageBucket: "heavygari-web.appspot.com",
    messagingSenderId: "422142305895"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  //console.log('[firebase-messaging-sw.js] Received background message ', payload);
  const title = payload.notification.title;
  const options = {
    body: payload.notification.body
  };
  return self.registration.showNotification(title, options);
});
