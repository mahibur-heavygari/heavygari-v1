<?php

namespace App;

use Carbon\Carbon;

class Trip extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id', 'unique_id', 'from_point', 'from_address','from_lat', 'from_lon', 'to_point', 'to_address', 'to_lat', 'to_lon', 'duration', 'distance', 'distance_type', 'multiplier'
    ];

    protected $dates = ['started_at', 'completed_at'];

     ///////////////
    // MUTATORS  //
    ///////////////

    /**
     * Convert UTC time to BD time
     */
    public function getStartedAtAttribute($value)
    {
        if ($value==null) {
            return $value;
        }

        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }

    /**
     * Convert UTC time to BD time
     */
    public function getCompletedAtAttribute($value)
    {
        if ($value==null) {
            return $value;
        }

        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * Get origin of this trip
     */
    public function origin()
    {
        return $this->belongsTo('App\Point', 'from_point');
    }

    /**
     * Get origin of this trip
     */
    public function destination()
    {
        return $this->belongsTo('App\Point', 'to_point');
    }

    /////////////
    // Helper  //
    /////////////

    /**
     * Get static map url of this trip
     */
    public function staticMapUrl()
    {
        /*return 'https://maps.googleapis.com/maps/api/staticmap?center='.$this->from_address.'&zoom=12&size=600x300&maptype=roadmap&markers=color:green%7Clabel:O%7C'.$this->from_lat.','.$this->from_lon.'&markers=color:red%7Clabel:D%7C'.$this->to_lat.','.$this->to_lon.'&key='.config('heavygari.google_maps.api_key');*/

        return 'https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap
&markers=color:green%7Clabel:S%7C'.$this->from_lat.','.$this->from_lon.'&markers=color:red%7Clabel:D%7C'.$this->to_lat.','.$this->to_lon.''.'&key='.config('heavygari.google_maps.api_key');
    }
    
    ////////////////
    // API MODELS //
    ////////////////
    
    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'booking_id' => $this->booking_id,
            'unique_id' => $this->unique_id,
            'from' => [
                'point' => [
                    'id' => $this->from_point,
                    'title' => $this->origin->title,
                ],
                'address' => $this->from_address,
                'coordinators' => [
                    'lat' => (float) $this->from_lat,
                    'lon' => (float) $this->from_lon,
                ],
            ],
            'to' => [
                'point' => [
                    'id' => $this->to_point,
                    'title' => $this->origin->title,
                ],
                'address' => $this->to_address,
                'coordinators' => [
                    'lat' => (float) $this->to_lat,
                    'lon' => (float) $this->to_lon,
                ],
            ],
            'static_map_url' => $this->staticMapUrl(),
            'distance' => $this->distance,
            'started_at' => ($this->started_at) ? $this->started_at->toDateTimeString() : $this->started_at,
            'completed_at' => ($this->started_at) ? $this->started_at->toDateTimeString() : $this->started_at,
        ];
    }    
}
