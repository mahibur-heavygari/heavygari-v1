<?php

namespace App;
 
class OwnerInvoice extends BaseModel
{
    protected $table = 'owner_invoices';

    /////////////////////
    // BEGIN RELATIONS //
    /////////////////////

    /*
    * Invoice has many Invoice items
    */
    public function invoiceItems()
    {
        return $this->hasMany('App\OwnerInvoiceItem', 'invoice_id');
    }

    /*
    * Invoice belongs to a payer
    */
    public function owner()
    {
        return $this->belongsTo('App\OwnerProfile');
    }

    /*
    * Get driver commission of this invoice
    */
    public function driverCommission()
    {
        return $this->hasOne('App\DriverCommission', 'owner_invoice_id');
    }

    ////////////////////
    // BEGIN MUTATORS //
    ////////////////////

    public function getMonthAttribute($value)
    {
        return date("F", mktime(0, 0, 0, $value, 1));
    }

}    
