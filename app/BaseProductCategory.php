<?php

namespace App;

class BaseProductCategory  extends BaseModel
{
    protected $table='base_product_categories';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'category'
    ];

    /////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'category'=>$this->category

        ];
    }
}
