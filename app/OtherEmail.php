<?php

namespace App;

class OtherEmail extends BaseModel
{
    protected $table = 'other_user_email';

    protected $fillable = [
        'email', 'user_type'
    ];
}