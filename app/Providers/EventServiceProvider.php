<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Owner\NewOwnerRegistered' => [
            'App\Listeners\Owner\SendSMSCode',
            'App\Listeners\Owner\SendWelcomeEmail',
        ],
        'App\Events\Customer\NewCustomerRegistered' => [
            'App\Listeners\Customer\SendSMSCode',
            'App\Listeners\Customer\SendWelcomeEmail',
        ],
        'App\Events\Driver\NewDriverRegistered' => [
            'App\Listeners\Driver\SendSMSCode',
            'App\Listeners\Driver\SendWelcomeEmail',
        ],
        'App\Events\Driver\DriverAccountVerified' => [
            'App\Listeners\Driver\AccountVerified\SendEmailNotification',
            'App\Listeners\Driver\AccountVerified\SendSms',
        ],
        'App\Events\Owner\OwnerAccountVerified' => [
            'App\Listeners\Owner\AccountVerified\SendEmailNotification',
            'App\Listeners\Owner\AccountVerified\SendSms',
        ],
        'App\Events\Booking\NewBookingCreated' => [
            'App\Listeners\Booking\NewBooking\NotifyDrivers',
            //'App\Listeners\Booking\NewBooking\NotifyOwners',
        ],
        'App\Events\Booking\DriverAcceptBooking' => [
            'App\Listeners\Booking\AcceptBooking\NotifyCustomer',
            //'App\Listeners\Booking\AcceptBooking\SendSMSToRecipient',
            'App\Listeners\Booking\AcceptBooking\NotifyOwner',
            'App\Listeners\Booking\AcceptBooking\GenerateInvoicePDF',
        ],
        'App\Events\Booking\TripStart' => [
            'App\Listeners\Booking\TripStart\NotifyCustomer',
            'App\Listeners\Booking\TripStart\NotifyOwner',
            'App\Listeners\Booking\TripStart\NotifyDriver',
            'App\Listeners\Booking\TripStart\SendSMSToRecipient',
        ],
        'App\Events\Booking\TripComplete' => [
            'App\Listeners\Booking\TripComplete\NotifyCustomer',
            'App\Listeners\Booking\TripComplete\NotifyOwner',
        ],
        'App\Events\OwnerInvoice\OwnerInvoiceCreated' => [
            'App\Listeners\OwnerInvoice\OwnerInvoiceCreated\GeneratePDF',
            'App\Listeners\OwnerInvoice\OwnerInvoiceCreated\GeneratePaidPDF',
        ],
        'App\Events\FromAdmin\SendSmsAtAcceptBooking' => [
            'App\Listeners\FromAdmin\SendSmsAtAcceptBooking\NotifyDriver',
            //'App\Listeners\FromAdmin\SendSmsAtAcceptBooking\NotifyOwner',
        ],
        'App\Events\FromAdmin\SendSmsAtCreateBookingByAdmin' => [
            'App\Listeners\FromAdmin\SendSmsAtCreateBookingByAdmin\NotifyCustomer',
        ],
        'App\Events\Booking\CancelBooking' => [
            'App\Listeners\Booking\CancelBooking\NotifyCustomer',
            'App\Listeners\Booking\CancelBooking\NotifyDriver',
        ],
        'App\Events\FromAdmin\SendPushEmail' => [

            'App\Listeners\FromAdmin\SendPushEmail\SendEmail'
            
        ],

        'App\Events\FromAdmin\SendPushOtherEmail' => [
            'App\Listeners\FromAdmin\SendPushOtherEmail\SendOtherEmail'],
            
        'App\Events\FromAdmin\BookingPriceChange' => [
            'App\Listeners\FromAdmin\BookingPriceChange\NotifyCustomer'

        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
