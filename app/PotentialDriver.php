<?php

namespace App;

class PotentialDriver extends BaseModel
{
    protected $table = 'potential_drivers';

    protected $fillable = [
         'driver_name', 'driver_phone', 'driver_email','driver_address', 'driver_area'
    ];

    public function updateProfile($request)
    {
        $this->update($request->only('driver_name', 'driver_phone', 'driver_email','driver_address', 'driver_area'));
    }

}