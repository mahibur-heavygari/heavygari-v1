<?php

namespace App;

class PopUpSetting extends BaseModel
{
    protected $table = 'pop_up_settings';
    protected $dates = ['expired_at'];

    protected $fillable = [
        'photo', 'button_position', 'expired_at'
    ];

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'photo' => url(\Storage::url($this->photo)),
            'button_position'=> $this->button_position
        ];
    }
}
