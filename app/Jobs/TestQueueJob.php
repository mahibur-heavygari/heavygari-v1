<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TestQueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $now;
    public $test = 1000;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($now)
    {
        $this->now = $now;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = $this->now;
        Log::info('I guess i am running properly at '.$now);
        Log::info('Test : '.$this->test);

        //dd('hmm : '. $now);
    }
}
