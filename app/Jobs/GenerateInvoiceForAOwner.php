<?php

namespace App\Jobs;

use App\OwnerProfile;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\MyLibrary\InvoiceLib\InvoiceSystem;

class GenerateInvoiceForAOwner implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $owner;
    public $month;
    public $year;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OwnerProfile $owner, $month=false, $year=false)
    {
        $this->owner = $owner;
        $this->month = ($month) ? $month : date('m');
        $this->year = ($year) ? $year : date('Y');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //$previous_month = (int)date('m') - 1;
        //$previous_month = (string)$previous_month;
        $month = date('m');
        $year = date('Y');


        //$invoice = InvoiceSystem::create($this->owner->id, $previous_month, date('Y'));
        $invoice = InvoiceSystem::create($this->owner->id, $month, $year);

        Log::info('GenerateInvoiceForAOwner Job running for Owner Profile id #'.$this->owner->id.', month : '.$this->month.','.$this->year.'.');
    }
}
