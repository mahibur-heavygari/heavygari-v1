<?php

namespace App;

class BaseWeightCategory  extends BaseModel
{
    protected $table='base_weight_category';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'weight'
    ];

    /////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'weight'=> (float) $this->weight           
        ];
    }
}
