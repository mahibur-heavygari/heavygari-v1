<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Events\Booking\CancelBooking as CancelBookingEvent;
use App\Booking;

class CancelBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command cancels bookings if the bookings are not accepted by drivers or owners within 30 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status = 'open';
        $send_by = 'task_scheduler';
        
        $bookings = Booking::where('status', $status)->get();
        foreach ($bookings as $key => $booking) {
            $created_at = $booking->created_at;
            $trip_departure_at = $booking->datetime;

            if($booking->booking_type == 'on-demand'){
                $expire_at = date('Y-m-d H:i:s', strtotime($created_at . ' +60 minutes'));
            }
            else{
                $trip_difference_time = $trip_departure_at->diffInSeconds($created_at);
                if($trip_difference_time > 36000){
                    $expire_at = date('Y-m-d H:i:s', strtotime($trip_departure_at . ' -240 minutes'));
                }else{
                    $expire_at = date('Y-m-d H:i:s', strtotime($trip_departure_at . ' +120 minutes'));
                }
            }

            $present_time = (string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka');

            if( strtotime( $expire_at ) < strtotime( $present_time )){
                Booking::where('id', $booking->id)->update(['status'=>'cancelled', 'cancelled_by'=>'Automatic']);

                if( $booking->first_discounted == 'yes' ){
                    $booking->first_discounted = NULL;
                    $booking->save();

                    $booking->customer->first_booking_discount = NULL;
                    $booking->customer->save();
                }

                if($booking->is_corporate=='yes'){
                    $booking->customer->corporateCustomer->balance = $booking->customer->corporateCustomer->balance + $booking->invoice->total_cost;
                    $booking->customer->corporateCustomer->save();
                }

                event(new CancelBookingEvent($booking , $status, $send_by));
            }
        }
    }
}
