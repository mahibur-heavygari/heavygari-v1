<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Jobs\GenerateInvoiceForAOwner;
use App\MyLibrary\InvoiceLib\InvoiceSystem;

class GenerateMonthlyInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will generate montly invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $owners = \App\OwnerProfile::whereHas('myBookings', function($q){
            $q->where('payment_invoice_generated', 'no');
        })->get();

        foreach ($owners as $owner) {
            dispatch(new GenerateInvoiceForAOwner($owner));
        }

        Log::info('Laravel scheduler running for generating invoice of : '.date('m').', '.date('Y').'.');
    }
}
