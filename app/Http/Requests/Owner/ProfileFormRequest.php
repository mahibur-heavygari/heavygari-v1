<?php

namespace App\Http\Requests\Owner;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProfileFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($profile)
    {
        $user = $profile->user;

        return [
            'name' => 'required',
            'email' => ['required', Rule::unique('users')->ignore($user->id)],
            //'phone' => 'required',
            'company_name' => '',
            'address' => '',
            'national_id' => 'required',
            'national_id_photo' => ''
        ];
    }
}
