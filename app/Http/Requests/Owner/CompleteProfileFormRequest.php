<?php

namespace App\Http\Requests\Owner;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CompleteProfileFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($profile)
    {
        $user = $profile->user;

        return [
            //'name' => 'required',
            //'email' => ['required', Rule::unique('users')->ignore($user->id)],
            //'phone' => 'required',
            'national_id' => 'required',
            'national_id_photo' => '',
            'photo' => '',
            'company_name' => '',
            'manager_name' => '',
            'manager_phone' => '',
            'address' => 'required',
            'ownership_card_number' => '',
            'ownership_card_picture' => '',
            'bank_name' => '',
            'bank_branch' => '',
            'account_number' => '',
        ];
    }
}
