<?php

namespace App\Http\Requests\Owner;

use Illuminate\Foundation\Http\FormRequest;

class AddVehicleFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'about' => '',
            'vehicle_type_id' => 'required',
            'vehicle_registration_number' => 'required',
            'fitness_number' => '',
            'fitness_expiry' => 'required',
            'number_plate' => 'required',
            'tax_token_number' => '',
            'tax_token_expirey' => 'required',
            'chesis_no' => '',
            'engine_capacity_cc' => '',
            'ride_sharing_certificate_no' => '',
            'insurance_no' => '',
            'registered_by' => 'required'
        ];
    }
}
