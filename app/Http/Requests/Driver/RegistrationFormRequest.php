<?php

namespace App\Http\Requests\Driver;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            //'email' => 'email',
            'phone' => 'required|unique:users',
            'password' => 'required',
            'password_confirmation' => 'required',
            'national_id' => 'required',
            'license_no' => 'required',
            'license_date_of_issue' => 'required',
            'license_date_of_expire' => 'required',
            'license_issuing_authority' => 'required',
        ];
    }
}
