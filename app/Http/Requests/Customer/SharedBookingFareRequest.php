<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class SharedBookingFareRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_category_id' => 'required',
            'weight_category_id' => 'required',
            'weight' => 'required_if:weight_category_id,==,0',
            'volumetric_category_id' => 'required',            
            'volumetric_width' => 'required_if:volumetric_category_id,==,0',
            'volumetric_height' => 'required_if:volumetric_category_id,==,0',
            'volumetric_length' => 'required_if:volumetric_category_id,==,0',
            'quantity' => 'required|integer',
            //--
            'from_point' => 'required',
            'from_address' => 'required',
            'from_lat' => 'required_with:from_address',
            'from_lon' => 'required_with:from_address',
            'to_point' => 'required',
            'to_address' => 'required',
            'to_lat' => 'required_with:to_address',
            'to_lon' => 'required_with:to_address'
        ];
    }

    public function messages()
    {
        return [
            'from_lat.required_with' => 'The address is not correct',
            'from_lon.required_with'  => 'The address is not correct',
            'to_lat.required_with' => 'The address is not correct',
            'to_lon.required_with'  => 'The address is not correct',
            'volumetric_width.required_if' => 'The width field is required',
            'volumetric_height.required_if'  => 'The height field is required',
            'volumetric_length.required_if' => 'The length field is required',
            'weight.required_if'  => 'The weight field is required',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            if ($this->weight_category_id == 0) {
                if (!is_numeric($this->weight)) {
                    $validator->errors()->add('weight', 'Please enter a number');
                }
            }

            if ($this->volumetric_category_id == 0) {
                if (!is_numeric($this->volumetric_width)) {
                    $validator->errors()->add('volumetric_width', 'Please enter a number');
                }

                if (!is_numeric($this->volumetric_height)) {
                    $validator->errors()->add('volumetric_height', 'Please enter a number');
                }

                if (!is_numeric($this->volumetric_length)) {
                    $validator->errors()->add('volumetric_length', 'Please enter a number');
                }
            }
        });
    }
}
