<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class FullBookingFareRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_type' => 'required',
            'capacity' => 'required|numeric',
            'trip_type' => 'required',
            //--
            'from_point' => 'required',
            'from_address' => 'required',
            'from_lat' => 'required_with:from_address',
            'from_lon' => 'required_with:from_address',
            'to_point' => 'required',
            'to_address' => 'required',
            'to_lat' => 'required_with:to_address',
            'to_lon' => 'required_with:to_address'            
        ];
    }

    public function messages()
    {
        return [
            'from_lat.required_with' => 'The address is not correct',
            'from_lon.required_with'  => 'The address is not correct',
            'to_lat.required_with' => 'The address is not correct',
            'to_lon.required_with'  => 'The address is not correct',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // over capacity?
            if ($this->vehicle_type) {
                if ($max_capacity = $this->isOverCapacity()) {
                    $validator->errors()->add('capacity', 'Sorry, maximum capacity is '.$max_capacity.'.');
                }
            }

            /*
            // from address correct?
            if ($point = $this->from_address) {
                if ($point_title = $this->isWrongAddress($this->from_point, $this->from_address)) {
                    $validator->errors()->add('from_address', 'Sorry, The address does not belongs to '.$point_title);
                }
            }

            // from address correct?
            if ($point = $this->to_address) {
                if ($point_title = $this->isWrongAddress($this->to_point, $this->to_address)) {
                    $validator->errors()->add('to_address', 'Sorry, The address does not belongs to '.$point_title);
                }
            }
            */
        });
    }

    /**
     * It is over maximum capacity for selected vehicle?
     * 
     * @return boolean
     */
    private function isOverCapacity()
    {
        $vehcile_type = \App\VehicleType::findOrFail($this->vehicle_type);
        
        if ($this->capacity > $vehcile_type->max_capacity) {
            return $vehcile_type->max_capacity;
        }

        return false;
    }

    /**
     * Is wrong address?
     * i.e point is dhaka but address is for chittagong
     * 
     * @return boolean
     */
    private function isWrongAddress($point, $address)
    {
        $point = \App\Point::findOrFail($point);
        
        if (strpos($address, $point->title) !== false) {
           return false;
        }

        return $point->title;
    }
}
