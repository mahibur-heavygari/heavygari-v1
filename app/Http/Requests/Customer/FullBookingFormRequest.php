<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class FullBookingFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'vehicle_type' => 'required',
            'capacity' => 'required|numeric',
            'trip_type' => 'required',
            //--
            'from_point' => 'required',
            'from_address' => 'required',
            'from_lat' => 'required_with:from_address',
            'from_lon' => 'required_with:from_address',
            'to_point' => 'required',
            'to_address' => 'required',
            'to_lat' => 'required_with:to_address',
            'to_lon' => 'required_with:to_address',
            'booking_type' => 'required',
            'date_time' => 'required_if:booking_type,==,advance',            
            'recipient_name' => 'required',
            'recipient_phone' => 'required',
            'payment_by' => 'required',
            'particular_details' => '',           
            'waiting_time' => ''
        ];
    }

    public function messages()
    {
        return [
            'from_lat.required_with' => 'The address is not correct',
            'from_lon.required_with'  => 'The address is not correct',
            'to_lat.required_with' => 'The address is not correct',
            'to_lon.required_with'  => 'The address is not correct',
        ];
    }
}
