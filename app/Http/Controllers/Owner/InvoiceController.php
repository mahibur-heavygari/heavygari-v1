<?php

namespace App\Http\Controllers\Owner;

use App\MyLibrary\InvoiceLib\InvoiceSystem;

class InvoiceController extends OwnerPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $profile = $this->ownerProfile();

        $list = InvoiceSystem::all($profile->id);

        return view('owner.invoices.list')->with('list', $list);
    }


    /**
     * Display the specified resource.
     */
    public function show($invoice_number)
    {
        $invoice = InvoiceSystem::show($invoice_number);

        return view('owner.invoices.show')->with('invoice', $invoice);
    }
}
