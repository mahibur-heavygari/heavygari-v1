<?php

namespace App\Http\Controllers\Owner;

use Sentinel;
use Illuminate\Http\Request;
use App\Http\Requests\User\ChangePasswordFormRequest;

class SettingsController extends OwnerPanelController
{
	/**
	 * show profile page
	 */
    public function changePassword()
    {
        return view('owner.settings.password');
    }

    /**
     * change password
     */
    public function storeChangePassword(ChangePasswordFormRequest $request)
    {
        $user = Sentinel::getUser();

        $password_change = $user->changePassword($request);

        if (isset($password_change['error'])) {
            $notification = ['danger',$password_change['error']];
            session()->flash('message', $notification);
            
            return redirect()->back();
        } 

        $notification = ['success','Your password has been changed'];
        session()->flash('message', $notification);

        return redirect('owner/panel/profile');
    }
}
