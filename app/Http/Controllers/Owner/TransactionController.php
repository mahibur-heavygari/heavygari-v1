<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\MyLibrary\TransactionLib\OwnerTransaction;


class TransactionController extends OwnerPanelController
{
   
    public function getTransactions( Request $request )
    {
    	$owner = $this->ownerProfile();
    	$transaction = new OwnerTransaction($owner);
    	$transactions = $transaction->getTransactions( $request );

    	return view('owner.transactions.history')->with('bookings', $transactions['bookings'])->with('total_fare', $transactions['total_fare'])->with('admin_commission_total', $transactions['admin_commission_total'])->with('owner_earning_total', $transactions['owner_earning_total'])->with('driver_commission_total', $transactions['driver_commission_total'])->with('admin_due_commission_total', $transactions['admin_due_commission_total'])->with('admin_paid_commission_total', $transactions['admin_paid_commission_total']);
    }

   

}
