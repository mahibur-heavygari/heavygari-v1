<?php

namespace App\Http\Controllers\Owner;

use Sentinel;
use Illuminate\Http\Request;
use App\UserFeedback;

class FeedbackController extends OwnerPanelController
{
	/**
	 * show profile page
	 */
    public function create()
    {
        return view('owner.feedback.create');
    }

    /**
     * change password
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();

        $this->validate($request, [
            'message' => 'required'
        ]);

        UserFeedback::create(['user_id'=>$user->id, 'user_type'=>'owner', 'message'=>$request->message]);

        $notification = ['success', 'Your message has been saved.'];
        session()->flash('message', $notification);

        return redirect('owner/panel/contact-us');
    }
}
