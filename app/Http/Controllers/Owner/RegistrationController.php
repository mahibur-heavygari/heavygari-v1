<?php

namespace App\Http\Controllers\Owner;

use App\User;
use App\OwnerProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\RegistrationFormRequest;
use App\Http\Requests\Owner\CompleteProfileFormRequest;
use App\MyLibrary\UserLib\Verification\PhoneVerification;
use App\MyLibrary\UserLib\Registration\OwnerRegistration;

class RegistrationController extends Controller
{
	/**
	 * registration form
	 */
    public function create()
    {
    	return view('auth.registration.owner.register');
    }

    /**
	 * register user after validation
	 */
    public function store(RegistrationFormRequest $request)
    {
    	$registration = new OwnerRegistration($request->all());
        
        $user = $registration->registerOwner();

        if (!$user) {
            $notification = ['danger','Sorry! Could not register.'];
            return $registration->getError()->getMessage();
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $profile = $user->ownerProfile;

        return redirect('owner/register/'.$profile->id.'/verify-phone');
    }

    /**
     * show verification page
     */
    public function verifyPhone(OwnerProfile $owner)
    {
        //TODO : is already verified?
        
        $user = $owner->user;
        
        return view('auth.verify')->with('user', $user);
    }

    /**
     * activate the owner if the code is right
     */
    public function storeVerifyPhone(OwnerProfile $owner, Request $request)
    {
        $code = $request->code;

        $user = $owner->user;
        
        $activation = new PhoneVerification($user);
        
        if ($activation->complete($code)) {
            $notification = ['success','Your phone is verified! Please complete your profile now.'];
            session()->flash('message', $notification);

            return redirect('owner/register/'.$owner->id.'/profile/complete?token='.$owner->token);
        }

        $notification = ['danger','Sorry! Wrong Code.'];
        session()->flash('message', $notification);

        return redirect()->back();
    }

    /**
     * Complete owner profile page
     */
    public function complete(OwnerProfile $owner, Request $request)
    {
        //TODO : is already completed?

        // valid token?
        if ($owner->token != $request->token) {
            return 'Sorry! Unauthorized page';
        }

        return view('auth.registration.owner.complete_profile')->with('profile', $owner);
    }

    /**
     *  Complete owner's profile
     */
    public function storeComplete(OwnerProfile $owner, Request $request)
    {
        // valid token?
        if ($owner->token != $request->token) {
            return 'Sorry! Unauthorized page';
        }
        
        // validation
        $formRequest = new CompleteProfileFormRequest();
        $rules = $formRequest->rules($owner);
        $this->validate($request, $rules);

        $owner->completeProfile($request);

        //flash message
        $notification = ['success','Thanks for registering. Once Admin approve your profile you will be able to login.'];
        session()->flash('message', $notification);

        return redirect('/session/login');
    }
}
