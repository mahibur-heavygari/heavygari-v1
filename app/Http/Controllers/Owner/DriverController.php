<?php

namespace App\Http\Controllers\Owner;

use Sentinel;
use App\OwnerProfile;
use App\DriverProfile;
use Illuminate\Http\Request;
use App\Http\Requests\Driver\ProfileFormRequest;
use App\Http\Requests\User\ChangePasswordFormRequest;
use App\Http\Requests\Driver\RegistrationFormRequest;
use App\MyLibrary\UserLib\Verification\PhoneVerification;

class DriverController extends OwnerPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $drivers = $this->ownerProfile()->myDrivers;

        // is the owner a driver?
        $user = Sentinel::getUser();
        $user_roles = $user->roles;
        $driverRole = Sentinel::findRoleBySlug('driver');
        $isDriver = $user->inRole($driverRole) ? true : false;

        return view('owner.drivers.list')->with(['drivers' => $drivers, 'isDriver' => $isDriver]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('owner.drivers.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RegistrationFormRequest $request)
    {
        $owner = $this->ownerProfile();

        $driver = DriverProfile::registerDriver($request, $owner);

        $profile = $driver->driverProfile;

        $profile->completeProfile($request);

        //flash message
        $notification = ['success','New Driver Added Successfully'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/drivers/verify/'.$profile->id);
    }

    /**
     * Display the specified resource.
     */
    public function show(DriverProfile $driver)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);
        
        $driver->is_phone_verified = $driver->isPhoneVerified();
        
        $rating = $driver -> myAverageRating();

        return view('owner.drivers.show')->with('profile', $driver)->with('rating', $rating);
    }



    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DriverProfile $driver)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);

        $owner = $driver->myOwner;

        $my_vehicles = $owner->myVehicles;

        $authorized_vehicles = $driver->myAuthorizedVehicles;
        
        return view('owner.drivers.edit')->with('profile', $driver)->with('my_vehicles', $my_vehicles)->with('authorized_vehicles', $authorized_vehicles);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, DriverProfile $driver)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);
        
        // validation
        $formRequest = new ProfileFormRequest();
        $rules = $formRequest->rules($driver);
        $this->validate($request, $rules);

        $owner = $this->ownerProfile();

        // if owner is making him a driver..
        if ($owner->user->id == $driver->user->id) {
            $driver->completeProfile($request);
        } else {
            $driver->updateProfile($request);
        }

        //flash message
        $notification = ['success','Profile Updated Successfully'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/drivers/'.$driver->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DriverProfile $driver)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);

        $driver->delete();
    }

    /**
     * Make a driver profile for this owner
     */
    public function makeMeDriver()
    {
        $owner = $this->ownerProfile();

        $driver = $owner->makeMeDriver();

        //flash message
        $notification = ['success','You have been added as a driver. Please update your driver profile.'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/drivers/'.$driver->id.'/edit');
    }

    /**
     * try to verify the driver
     */
    public function verifyDriver(DriverProfile $driver)
    {
        //TODO : is already verified?

        $user = $driver->user;
        
        return view('auth.verify')->with('user', $user);
    }

    /**
     * try to activate the driver
     */
    public function verifyDriverPost(DriverProfile $driver, Request $request)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);
        
        $code = $request->code;

        $user = $driver->user;
        
        $activation = new PhoneVerification($user);
        
        if ($activation->complete($code)) {
            $notification = ['success','This account is verified!'];
            session()->flash('message', $notification);

            return redirect('owner/panel/drivers/'.$driver->id);
        }

        $notification = ['danger','Sorry! Wrong Code.'];
        session()->flash('message', $notification);

        return redirect()->back();
    }

    /**
     * Change Driver Password
     */
    public function changeDriverPassword(ChangePasswordFormRequest $request, DriverProfile $driver)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);

        $user = $driver->user;

        $password_change = $user->changePassword($request);

        if (isset($password_change['error'])) {
            $notification = ['danger',$password_change['error']];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        //flash message
        $notification = ['success','Password Changed Successfully'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/drivers');
    }

    /**
     * Change Driver Password
     */
    public function changeDriverPreferences(Request $request, DriverProfile $driver)
    {
        # Authorization check
        $this->isAuthorizedOwner($driver);

        $user = $driver->user;

        $result = $driver->updatePreferences($request);

        if (!$result) {
            $notification = ['danger', 'Sorry! Could not update your preferences'];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        //flash message
        $notification = ['success','Preferences Changed Successfully'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/drivers');
    }

    /**
     * Check if the owner is authorized?
     */
    private function isAuthorizedOwner($driver) 
    {
        $owner = $this->ownerProfile();

        if( $owner->id != $driver->vehicle_owner_profile_id) {
            abort(403, 'Unauthorized action.');
        }

        return true;
    }
}
