<?php

namespace App\Http\Controllers\Owner;

use Storage;
use Validator;
use App\OwnerProfile;
use Illuminate\Http\Request;
use App\Http\Requests\Owner\ProfileFormRequest;

class ProfileController extends OwnerPanelController
{
	/**
     * Display the specified resource.
     */
    public function show()
    {
        $profile = $this->ownerProfile();

        return view('owner.profile.show')->with('profile', $profile);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(OwnerProfile $profile)
    {
    	$profile = $this->ownerProfile();

        return view('owner.profile.edit')->with('profile', $profile);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $profile = $this->ownerProfile();

        // validation
        $formRequest = new ProfileFormRequest();
        $rules = $formRequest->rules($profile);
        $this->validate($request, $rules);

        $owner = $profile->updateProfile($request);
        
        //flash message
        $notification = ['success','Profile Updated Successfully'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/profile');
    }

    public function updateWebFcmToken(Request $request){
        $fcm_reg_token = $request->fcm_token;
        $profile = $this->ownerProfile();
        $profile->updateFcmRegToken($fcm_reg_token);
        return ['success'=>true, 'data'=>'updated'];
    }
}
