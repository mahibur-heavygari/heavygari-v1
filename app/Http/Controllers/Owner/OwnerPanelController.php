<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;

use Sentinel;

use App\OwnerProfile;

class OwnerPanelController extends controller
{
    /**
     * Returns the logged in owner's profile id
     */
    public function ownerProfile()
    {
        $user = Sentinel::getUser();

        $profile = OwnerProfile::where('user_id', $user->id)->with('user')->first();

        return $profile;
    }
}
