<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;

use App\DriverProfile;
use App\Booking;
use App\Vehicle;
use App\Trip;
use App\HiredDriver;
use App\Events\Booking\CancelBooking;

class BookingController extends OwnerPanelController
{
    /**
     * Display trip listing
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter') ? $request->input('filter') : 'open';

        if($filter == 'open') {
            $bookings = Booking::where('status', $filter)->get()->sortByDesc('created_at');
        } else {
            $bookings = $this->ownerProfile()->myBookings->where('status', $filter)->sortByDesc('created_at');
        }

        return view('owner.bookings.list')->with('filter', $filter)->with('bookings', $bookings)->with('ownerPanel', true);
    }

    /**
     * Display the acceptance form
     */
    public function accept($unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();

        $my_drivers = $this->ownerProfile()->myDrivers->where('user.status', 'active');

        $my_vehicles = $this->ownerProfile()->myVehicles;

        return view('owner.bookings.accept')->with('booking', $booking)->with('my_drivers',$my_drivers)->with('my_vehicles',$my_vehicles)->with('ownerPanel', true);
    }

    /**
     * Accept the booking
     */
    public function acceptStore($unique_id, Request $request)
    {
        $this->validate($request, [
            'driver' => 'required',
            'vehicle' => 'required',
        ]);

        if($request->is_hired == 'true'){
            $this->validate($request, [
                'phone' => 'required'
            ]);
        }

        $booking = Booking::where('unique_id', $unique_id)->open()->first();

        if (!$booking) {
            abort(403, 'This is not an open booking.');
        }

        if($request->is_hired == 'true'){
            //for a new driver ( hired driver )
            $driver = DriverProfile::where('user_id', $this->ownerProfile()->user->id)->first();
            if(is_null($driver)){
                $this->ownerProfile()->makeMeDriver();
                $driver = DriverProfile::where('user_id', $this->ownerProfile()->user->id)->first();
            }
        }else{
            $driver = DriverProfile::findOrFail($request->driver);
        }

        $vehicle = Vehicle::findOrFail($request->vehicle);

        $accept = $booking->accept($driver, $vehicle); 
        $booking->accepted_by = 'owner';
        $booking->save();

        if($request->is_hired == 'true'){
            Booking::where('unique_id', $unique_id)->update(['is_hired_driver' => 'yes']);
            HiredDriver::create([
                'name'=>$request->driver, 
                'phone'=>$request->phone, 
                'booking_id'=>$booking->id, 
                'vehicle_owner_profile_id'=>$this->ownerProfile()->id, 
                'driver_profile_id'=>$driver->id
            ]);
        }

        return redirect('/owner/panel/trips-by-my-vehicles?filter=upcoming');
    }

    /**
     * Display the specified trip.
     */
    public function show($unique_id)
    {
        $booking = Booking::open()->where('unique_id', $unique_id)->first();

        if (!$booking ) {
            $booking = $this->getMyBookingDetails($unique_id);
        }

        return view('owner.bookings.show')->with('booking', $booking)->with('ownerPanel', true);
    }

    /**
     * Start the trip
     */
    public function startTrip($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);
       
        $start = $booking->start();

        $notification = ['success','Your trip has been started!'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/trips-by-my-vehicles?filter=ongoing');
    }

    /**
     * Complete the booking
     */
    public function completeTrip($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);
        
        $start = $booking->complete();

        $notification = ['success','Your trip has been completed!'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/trips-by-my-vehicles?filter=completed');
    }

    /**
     * cancel the booking
     */
    public function cancelTrip($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);

        event(new CancelBooking($booking , 'upcoming', 'owner'));
        
        $booking->cancel();
        $booking->cancelled_by = 'Owner';
        $booking->save();

        $notification = ['success','Your trip has been cancelled!'];
        session()->flash('message', $notification);


        return redirect('/owner/panel/trips-by-my-vehicles?filter=cancelled');
    }


    /**
     * Rate the customer page
     */
    public function rateCustomer($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);

        return view('owner.bookings.rate_customer')->with('booking', $booking);
    }

    /**
     * Rate the customer
     */
    public function rateCustomerStore($unique_id, Request $request)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();

        $driver = $booking->driver;

        $rate_customer = $driver->rateCustomer($booking, $request->rating, $request->review);

        if ($rate_customer['error']) {
            $notification = ['danger',$rate_customer['error']];
            session()->flash('message', $notification);

            return redirect()->back();
        }

        $notification = ['success', 'Thanks for the review.'];
        session()->flash('message', $notification);

        return redirect('owner/panel/trips-by-my-vehicles?filter=completed');
    }

    /**
     * Get Details of a Booking
     * check for authorization
     */
    private function getMyBookingDetails($unique_id)
    {
        $booking = $this->ownerProfile()->myBookings()->where('unique_id', $unique_id)->first();

        if (!$booking ) {
            abort(401);
        }

        return $booking;
    }
}
