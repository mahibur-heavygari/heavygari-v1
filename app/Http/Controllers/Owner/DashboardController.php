<?php

namespace App\Http\Controllers\Owner;

use App\MyLibrary\DashboardLib\OwnerDashboard;

class DashboardController extends OwnerPanelController
{
	/**
	 * show dashboard page
	 */
    public function index()
    {
    	$dashboard = new OwnerDashboard($this->ownerProfile());
    	$booking_stats = $dashboard->bookingStats();
    	$payment_stats = $dashboard->paymentStats();

        $vehicle_stats = $dashboard->vehicleStats();
        $driver_stats = $dashboard->driverStats();
        $driver_stats = $driver_stats->count();
        $vehicles = $vehicle_stats;
        $titles = $vehicles->keys()->all(); // for chart
        $counts = $vehicles->values()->all(); // for chart

        return view('owner.dashboard')->with([
    		'booking_stats' => $booking_stats,
    		'payment_stats' => $payment_stats,
            'vehicles' => $vehicles,
            'vehicle_titles' => $titles,
            'vehicle_counts' => $counts,
            'driver_stats' => $driver_stats

    	]);
    }

}
