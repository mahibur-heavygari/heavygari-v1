<?php

namespace App\Http\Controllers\Owner;

//Form requests
use App\Http\Requests\Owner\AddVehicleFormRequest;

use App\Vehicle;

use App\VehicleType;
use Illuminate\Http\Request;

class VehicleController extends OwnerPanelController
{
    protected $types;
    
    public function __construct()
    {
        $this->types = VehicleType::all();
    }

    /**
     * Display a listing of the resource.
     */
    public function index( Request $request )
    {
        $filter = $request->input('title') ? $request->input('title') : '';
        $vehicle_type = VehicleType::where('title', $filter)->first();
        
        if( $filter != '' && $vehicle_type){
            $vehicles = $this->ownerProfile()->myVehicles->where('vehicle_type_id', $vehicle_type->id);
        }else{
            $vehicles = $this->ownerProfile()->myVehicles;
        }

        return view('owner.vehicles.list')->with('vehicles', $vehicles);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $my_drivers = $this->ownerProfile()->myDrivers;

        return view('owner.vehicles.add')->with(['types' => $this->types, 'my_drivers' => $my_drivers]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AddVehicleFormRequest $request)
    {
        $owner = $this->ownerProfile();

        $vehicle = Vehicle::addRepo($owner, $request);

        return redirect('/owner/panel/vehicles');
    }

    /**
     * Display the specified resource.
     */
    public function show(Vehicle $vehicle)
    {
        return view('owner.vehicles.show')->with('vehicle', $vehicle)->with(['ownerPanel' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Vehicle $vehicle)
    {
        $my_drivers = $this->ownerProfile()->myDrivers;

        $authorized_drivers = $vehicle->authorizedDrivers;

        return view('owner.vehicles.edit')->with(['vehicle' => $vehicle, 'types' => $this->types, 'my_drivers' => $my_drivers, 'authorized_drivers' => $authorized_drivers]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AddVehicleFormRequest $request, Vehicle $vehicle)
    {
        $vehicle = $vehicle->updateProfile($request);

        $notification = ['success','Vehicle information was updated!'];
        session()->flash('message', $notification);

        return redirect('/owner/panel/vehicles/'.$vehicle->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
    }
}
