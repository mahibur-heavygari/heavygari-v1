<?php

namespace App\Http\Controllers;

use Validator;
use App\Point;
use App\HomeFeature;
use App\VehicleType;
use App\ContactMessage;
use App\AuthorizedPoint;
use App\PotentialCustomer;
use App\EmailHistory;
use Illuminate\Support\Facades\Mail;
use App\Mail\PushEmail\PushEmail;
use App\Mail\PushEmail\OthersEmail;
use Illuminate\Http\Request;
use App\MyLibrary\BookingLib\BookingManager;
use App\Http\Requests\Customer\FullBookingFareRequest;
use App\MyLibrary\GoogleRecaptchaLib\GoogleRecaptcha;

class HomeController extends Controller
{
    public $lang;

    public function __construct(Request $request)
    {
        $this->lang = $request->has('lang') ? $request->lang : 'bn';
    }
    /**
     * Display home page in bangla
     */
    public function index(Request $request)
    {
    	// $vehicle_types = VehicleType::all();
        // only bus
        $vehicle_types = VehicleType::where('title', 'Bus (Non AC)')->orWhere('title', 'Bus (AC)')->orWhere('title', 'XL Truck')->orWhere('title', 'Bus (Business Class)')->orWhere('title', 'Truck  (18feet)')->orWhere('title', 'Cover Truck  (22feet)')->orWhere('title', 'Trailer Truck 2XL  (20feet)')->orWhere('title', 'Trailer Truck 3XL  (40feet)')->orWhere('title', 'Pickup Van  (12feet)')->orWhere('title', 'Mini Pickup Van  (7feet)')->orderBy('preference', 'asc')->get();

        
        $points = Point::all();

        $capacity_types = [];
        foreach ($vehicle_types as $vehicle_type) {
            $capacity_types[($this->lang=='en') ?$vehicle_type->title : $vehicle_type->title_bn] = [
                'title' => $vehicle_type->capacityType->title,
                'capacity' => $vehicle_type->capacity,
            ];
        }

        $features = HomeFeature::all();

        return view('website.'.$this->lang.'.home')->with([
            'vehicle_types' => $vehicle_types,
            'capacity_types' => $capacity_types,
            'points' => $points,
            'features' => $features,
            'lang' => $this->lang
       ]);
    }

    /**
     * Display estimated cost page
     */
    public function getCost(Request $request)
    {
        $rules = (new FullBookingFareRequest)->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            //flash message
            $notification = ['danger','দয়া করে সকল প্রয়োজনীয় তথ্য প্রদান করুন'];
            session()->flash('message', $notification);

            return redirect()->back()->withErrors($validator)->withInput();
        }

        $booking_fields = $request->all();
        $manager = new BookingManager('full');
        $price_breakdown = $manager->getFare($booking_fields);

    	if (!$price_breakdown) {
            $notification = ['danger','Sorry, we are unable to provide a fare estimate for that booking.'];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $vehicle_type = VehicleType::where('id', $request->vehicle_type)->first();
        $route = $price_breakdown['route'];

        return view('website.cost_calculator')->with([
            'route' => $route,
            'fare_breakdown' => $price_breakdown['fare'],
            'booking_fields' => $booking_fields,
            'vehicle_type' => $vehicle_type,
            'lang' => $this->lang
        ]);
    }

    /**
     * Display ARP How To Page
     */
    public function showArpHowToPage(Request $request)
    {
        return view('website.'.$this->lang.'.arp_howto')->with([
            'lang' => $this->lang
        ]);
    }

    /**
     * Display autohorized points page
     */
    public function showAuthorizedPoints(Request $request)
    {
        $points = Point::all();

        if (isset($request->arp_point)) {
            $list = AuthorizedPoint::where('base_point_id', $request->arp_point)->get();
        } else {
            $list = AuthorizedPoint::all();
        }
    	
        return view('website.authorized_points')->with([
            'list' => $list,
            'points' => $points,
            'arp_point' => $request->arp_point,
            'lang' => $this->lang
        ]);
    }

    /**
     * Feedback
     */
    public function PostContactUs(Request $request)
    {
        $rules = [
            'phone' => 'required|numeric|digits:11|regex:/(01)[0-9]/',
            'message' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            //flash message
            $notification = ['danger','দয়া করে সকল প্রয়োজনীয় তথ্য প্রদান করুন'];
            session()->flash('message', $notification);

            return redirect()->back()->withErrors($validator)->withInput();
        }

        // Start google recaptcha code
        $inputs = $request->all();
        if($inputs['g-recaptcha-response'] == null){
            $notification = ['danger', 'Please fill out the captcha'];
            session()->flash('message', $notification);
            return redirect()->back();
        }

        $googleCaptcha = new GoogleRecaptcha();
        $googleCaptchaVerify = $googleCaptcha->VerifyCaptcha($inputs['g-recaptcha-response']);

        if ($googleCaptchaVerify["success"]==false)
        {
            $notification = ['danger','Invalid captcha'];
            session()->flash('message', $notification);
            return redirect()->back();
        }
        //end of google recaptcha code

        ContactMessage::create($request->all());

        //flash message
        $notification = ['success','আপনার মেসেজটি সফলভাবে পাঠানো হয়েছে'];
        session()->flash('message', $notification);

        return redirect('/');
    }
    public function BusinessRequest(Request $request)
    {   
        $rules = [
            'phone' => 'required|numeric|digits:11|regex:/(01)[0-9]/',
            'company_name' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            //flash message
            $notification = ['danger','দয়া করে সকল প্রয়োজনীয় তথ্য প্রদান করুন'];
            session()->flash('message', $notification);

            return redirect()->back()->withErrors($validator)->withInput();
        }
        PotentialCustomer::updateOrCreate(['customer_name' => $request->manager_name,'company_name' => $request->company_name,'customer_phone' => $request->phone,'customer_email' => $request->email]); 
        if($request->email){
            $user = "debashonchakraborty@gmail.com";
            $msg = $request->message;
            $title = "Corporate Customer";
            EmailHistory::Create(['email_text' => $msg, 'email' => $request->email, 'email_type' => "Business Email", 'response' => $request->_token,'user_type' => $title ]);
            $msg = [
                'company_name' => $request->company_name,
                'manager_name' => $request->manager_name,
                'designation' => $request->designation,
                'phone' => $request->phone,
                'email' => $request->email,
                'message' => $request->message,
            ];
            Mail::to($user)->send(new PushEmail($user, $msg, $image=false, $title));
            $user = $request->email;
            $msg = [
                'body'=>"Thankyou for showing your interest in HeavyGari for Business, our Business development manager will contact you soon.",
                'footer'=>"Regards",
                'sender'=>"Team HeavyGari",
            ];
            Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));

        };
        return response()->json(['success' =>TRUE, 'message'=>'ok']);

    }
}
