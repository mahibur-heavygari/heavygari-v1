<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Sentinel;
use App\CustomerProfile;
use DB;

class CustomerPanelController extends controller
{
    /**
     * Returns the logged in customer's profile id
     */
    public function customerProfile()
    {
        $user = Sentinel::getUser();

        $profile = CustomerProfile::where('user_id', $user->id)->with('user')->first();

        return $profile;
    }

    public function isCorporateManager()
    {
    	$rel_corporate_customers = DB::table('rel_corporate_customers')->where('customer_profile_id', $this->customerProfile()->id)->first();
    	
    	if(!is_null($rel_corporate_customers) && $rel_corporate_customers->user_type=='manager'){
    	   return TRUE;
        }else{
           return FALSE;
        }
    }
}
