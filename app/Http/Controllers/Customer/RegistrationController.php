<?php

namespace App\Http\Controllers\Customer;

use App\CustomerProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\RegistrationFormRequest;
use App\Http\Requests\Customer\CompleteProfileFormRequest;
use App\MyLibrary\UserLib\Verification\PhoneVerification;
use App\MyLibrary\UserLib\Registration\CustomerRegistration;

class RegistrationController extends Controller
{
	/**
	 * registration form
	 */
    public function create()
    {
    	return view('auth.registration.customer.register');
    }

    /**
	 * register user after validation
	 */
    public function store(RegistrationFormRequest $request)
    {
    	$registration = new CustomerRegistration($request->all());

        $user = $registration->registerCustomer();

        if (!$user) {
            $notification = ['danger','Sorry! Could not register.'];
            //return $registration->getError()->getMessage();
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $profile = $user->customerProfile;

        return redirect('/customer/register/'.$profile->id.'/verify-phone');
    }

    /**
     * show verification page
     */
    public function verifyPhone(CustomerProfile $customer)
    {
        //TODO : is already verified?
        
        $user = $customer->user;
        
        return view('auth.verify')->with('user', $user);
    }

    /**
     * activate the customer if the code is right
     */
    public function storeVerifyPhone(CustomerProfile $customer, Request $request)
    {
        $code = $request->code;

        $user = $customer->user;
        
        $activation = new PhoneVerification($user);
        
        if ($activation->complete($code)) {
            //$notification = ['success','Your phone is verified!'];
            //session()->flash('message', $notification);
            //return redirect('customer/register/'.$customer->id.'/profile/complete?token='.$customer->token);

            $customer->completeAtRegistration();

            $notification = ['success','Thanks for registering. Please login to continue.'];
            session()->flash('message', $notification);
            return redirect('/session/login');
        }

        $notification = ['danger','Sorry! Wrong Code.'];
        session()->flash('message', $notification);

        return redirect()->back();
    }

    /**
     * Complete customer profile page
     */
    public function complete(CustomerProfile $customer, Request $request)
    {
        //TODO : is already completed?
        
        // valid token?
        if ($customer->token != $request->token) {
            return 'Sorry! Unauthorized page';
        }

        return view('auth.registration.customer.complete_profile')->with('profile', $customer);
    }

    /**
     *  Complete customer's profile
     */
    public function storeComplete(CustomerProfile $customer, Request $request)
    {
        // valid token?
        if ($customer->token != $request->token) {
            return 'Sorry! Unauthorized page';
        }
        
        // validation
        $formRequest = new CompleteProfileFormRequest();
        $rules = $formRequest->rules($customer);
        $this->validate($request, $rules);

        $customer->completeProfile($request);

        //flash message
        $notification = ['success','Thanks for registering. Please login to continue.'];
        session()->flash('message', $notification);

        return redirect('/session/login');
    }
}
