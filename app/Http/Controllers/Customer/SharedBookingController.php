<?php

namespace App\Http\Controllers\Customer;

use App\Point;
use Validator;
use App\BaseWeightCategory;
use App\BaseProductCategory;
use Illuminate\Http\Request;
use App\BaseVolumetricCategory;
use Illuminate\Support\Facades\Log;
use App\MyLibrary\BookingLib\BookingManager;
use App\Http\Requests\Customer\SharedBookingFormRequest;
use App\Http\Requests\Customer\SharedBookingFareRequest;

class SharedBookingController extends CustomerPanelController
{
    /**
     * Show form for Shared Booking
     */
    public function create()
    {
        $points = Point::all();
        $product_categories = BaseProductCategory::all();
        $weight_categories = BaseWeightCategory::all();
        $volumetric_categories = BaseVolumetricCategory::all();

        return view('customer.booking.shared_booking.create')->with([
                'points' => $points,
                'product_categories' => $product_categories,
                'weight_categories' => $weight_categories,
                'volumetric_categories' => $volumetric_categories,
           ]);
    }

   
    /**
     * Show Estimated Fare and Additional Fields
     */
    public function showEstimatedFare(SharedBookingFareRequest $request)
    {
        $booking_fields = $request->all();

        // get fare
        $manager = new BookingManager('shared');
        $price_breakdown = $manager->getFare($booking_fields);

        if (!$price_breakdown) {
            $notification = ['danger','Sorry, we are unable to provide a fare estimate for that booking.'];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $route = $price_breakdown['route'];

        $booking_fields['product_category'] = BaseProductCategory::where('id', $request->product_category_id)->first();
        $booking_fields['weight_category'] = BaseWeightCategory::where('id', $request->weight_category_id)->first();
        $booking_fields['volumetric_category'] = BaseVolumetricCategory::where('id', $request->volumetric_category_id)->first();

        $customer_user = $this->customerProfile()->user;
        $customer['name'] = $customer_user->name;
        $customer['phone'] = $customer_user->phone;

        return view('customer.booking.shared_booking.add_details')->with([
            'route' => $route,
            'fare_breakdown' => $price_breakdown['fare'],
            'booking_fields' => $booking_fields,
            'customer' => $customer
        ]);
    }

   
     /**
     * Create new Booking
     * - validation
     * - do the booking
     */
    public function store(Request $request)
    {
        $rules = (new SharedBookingFareRequest())->rules();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Log::info('[Customer #'.$this->customerProfile()->id.' (From Web) confirmed booking with this fields :'.var_export($request->all(), true));

        $customer = $this->customerProfile();

        // do booking
        $manager = new BookingManager('shared');
        $manager->setBookingCustomer($customer);
        $booking = $manager->createBooking($request->all());

        if (!$booking) {
            return 'Sorry! Error occured during booking';
        }

        $notification = ['success','Your booking was placed successfully.'];
        session()->flash('message', $notification);

        return redirect('/customer/panel/bookings/'.$booking['unique_id'].'?confirmation=true');
    }

}
