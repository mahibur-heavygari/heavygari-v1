<?php

namespace App\Http\Controllers\Customer;

use Sentinel;
use Illuminate\Http\Request;
use App\UserFeedback;

class FeedbackController extends CustomerPanelController
{
	/**
	 * show profile page
	 */
    public function create()
    {
        return view('customer.feedback.create');
    }

    /**
     * change password
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();

        $this->validate($request, [
            'message' => 'required'
        ]);

        UserFeedback::create(['user_id'=>$user->id, 'user_type'=>'customer', 'message'=>$request->message]);

        $notification = ['success', 'Your message has been saved.'];
        session()->flash('message', $notification);

        return redirect('customer/panel/contact-us');
    }
}
