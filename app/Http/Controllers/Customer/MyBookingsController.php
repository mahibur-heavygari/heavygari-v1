<?php

namespace App\Http\Controllers\Customer;

use App\Booking;
use Illuminate\Http\Request;
use App\Events\Booking\CancelBooking;

class MyBookingsController extends CustomerPanelController
{
    
    /**
     * Create new Booking
     */
    public function create()
    {
        return view('customer.booking.select');
    }

    /**
     * Display my bookings
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter') ? $request->input('filter') : 'open';

        if($this->isCorporateManager()){
            $corporate_customer_profile_id_arr = $this->customerProfile()->corporate[0]->customers->pluck('id')->toArray();
            $bookings = Booking::whereIn('customer_profile_id', $corporate_customer_profile_id_arr)->where('status', $filter)->get()->sortByDesc('created_at');
        }else{
            $bookings = $this->customerProfile()->myBookings()->where('status', $filter)->get()->sortByDesc('created_at');
        }

        return view('customer.booking.list')->with('filter', $filter)->with('bookings', $bookings)->with('customerPanel', true);
    }

    /**
     * Display the specified booking.
     */
    public function show($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);

        return view('customer.booking.show')->with('booking', $booking)->with('customerPanel', true);
    }

    /**
     * Rate the driver page
     */
    public function rateDriver($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);

        return view('customer.booking.rate_driver')->with('booking', $booking);
    }

    /**
     * Rate the driver
     */
    public function rateDriverStore($unique_id, Request $request)
    {
        $booking = $this->getMyBookingDetails($unique_id);

        $customer = $booking->customer;

        $rate_driver = $customer->rateDriver($booking, $request->rating, $request->review);

        if ($rate_driver['error']) {
            $notification = ['danger',$rate_driver['error']];
            session()->flash('message', $notification);

            return redirect()->back();
        }

        $notification = ['success', 'Thanks for the review.'];
        session()->flash('message', $notification);

        return redirect('customer/panel/bookings?filter=completed');
    }

    /**
     * Show Invoice
     */
    public function showInvoice($unique_id)
    {
        $booking = $this->getMyBookingDetails($unique_id);

        return view('emails.booking.invoice')->with('booking', $booking)->with('view', true);
    }

    /**
     * Get Details of a Booking
     * check for authorization
     */
    private function getMyBookingDetails($unique_id)
    {
        if($this->isCorporateManager()){
            $corporate_customer_profile_id_arr = $this->customerProfile()->corporate[0]->customers->pluck('id')->toArray();
            $booking = Booking::whereIn('customer_profile_id', $corporate_customer_profile_id_arr)->where('unique_id', $unique_id)->first();
        }else{
            $booking = $this->customerProfile()->myBookings()->where('unique_id', $unique_id)->first();
        }

        if (!$booking ) {
            abort(401);
        }

        return $booking;
    }

    /*
    * Cancel the booking
    */
    public function cancelTrip($unique_id){
        $booking = $this->getMyBookingDetails($unique_id);

        if($booking->status == 'upcoming'){
            event(new CancelBooking($booking , 'upcoming', 'customer'));
        }
        
        $booking->cancel();
        $booking->cancelled_by = 'Customer';
        $booking->save();

        $notification = ['success','Your trip has been cancelled!'];
        session()->flash('message', $notification);


        return redirect('customer/panel/bookings?filter=cancelled');
    }

    public function redirectToInvoice($unique_id){
        $booking = Booking::where('unique_id', $unique_id)->first();
        return redirect(url(\Storage::url($booking->invoice_pdf)));
    }

}
