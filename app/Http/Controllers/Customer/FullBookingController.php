<?php

namespace App\Http\Controllers\Customer;

use App\Point;
use App\VehicleType;
use App\BaseProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\MyLibrary\BookingLib\BookingManager;
use App\MyLibrary\CorporateBookingLib\BookingManager as CorporateBookingManager;
use App\Http\Requests\Customer\FullBookingFormRequest;
use App\Http\Requests\Customer\FullBookingFareRequest;

class FullBookingController extends CustomerPanelController
{
    /**
     * Show form for Full Booking
     */
    public function create()
    {
        $points = Point::all();

        // $vehicle_types = VehicleType::all();
        // only bus
        $vehicle_types = VehicleType::where('title', 'Bus (Non AC)')->orWhere('title', 'Bus (AC)')->orWhere('title', 'XL Truck')->orWhere('title', 'Bus (Business Class)')->orWhere('title', 'Truck  (18feet)')->orWhere('title', 'Cover Truck  (22feet)')->orWhere('title', 'Trailer Truck 2XL  (20feet)')->orWhere('title', 'Trailer Truck 3XL  (40feet)')->orWhere('title', 'Pickup Van  (12feet)')->orWhere('title', 'Mini Pickup Van  (7feet)')->orderBy('preference', 'asc')->get();


        $capacity_types = [];
        foreach ($vehicle_types as $vehicle_type) {
            $capacity_types[$vehicle_type->title] = [
                'title' => $vehicle_type->capacityType->title,
                'capacity' => $vehicle_type->capacity,
            ];
        }

        return view('customer.booking.full_booking.create')
            ->with([
                'vehicle_types' => $vehicle_types,
                'capacity_types' => $capacity_types,
                'points' => $points,
           ]);
    }

    /**
     * Show Estimated Fare and Additional Fields
     */
    public function showEstimatedFare(FullBookingFareRequest $request)
    {
        $booking_fields = $request->all();

        // get fare

        //check customer whether corporate or not
        if($this->customerProfile()->corporate_customer_id){
            $manager = new CorporateBookingManager('full');
        }else{
            $manager = new BookingManager('full');
        }

        $manager->setBookingCustomer($this->customerProfile());
        $price_breakdown = $manager->getFare($booking_fields);

        if (!$price_breakdown) {
            $notification = ['danger','Sorry, we are unable to provide a fare estimate for that booking.'];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $vehicle_type = VehicleType::where('id', $request->vehicle_type)->first();
        $route = $price_breakdown['route'];
        $customer_user = $this->customerProfile()->user;
        $customer['name'] = $customer_user->name;
        $customer['phone'] = $customer_user->phone;
        $product_categories = BaseProductCategory::all();

        if ($vehicle_type->capacityType->title=='Person') {
            $product_categories = BaseProductCategory::where('category', 'ride_type')->orWhere('category', '=', NULL)->get();

        }else{
            $product_categories = BaseProductCategory::where('category', 'transport_type')->orWhere('category', '=', NULL)->get();
            
        }

        return view('customer.booking.full_booking.add_details')->with([
            'route' => $route,
            'fare_breakdown' => $price_breakdown['fare'],
            'booking_fields' => $booking_fields,
            'vehicle_type' => $vehicle_type,
            'customer' => $customer,
            'product_categories' => $product_categories
        ]);
    }

   
     /**
     * Create new Booking
     * - validation
     * - do the booking
     */
    public function store(FullBookingFormRequest $request)
    {
        Log::info('[Customer #'.$this->customerProfile()->id.' (From Web) confirmed booking with this fields :'.var_export($request->all(), true));

        $customer = $this->customerProfile();

        // do booking

        //check customer whether corporate or not
        if($this->customerProfile()->corporate_customer_id){
            $manager = new CorporateBookingManager('full');

            $manager->setBookingCustomer($customer);
            $booking_res = $manager->createBooking($request->all());

            if(!$booking_res['success'] && isset($booking_res['debit_failed'])){
                
                $notification = ['danger','Sorry, you do not have enough amount of balance.'];
                session()->flash('message', $notification);
                return redirect('/customer/panel/bookings/');

            }elseif(!$booking_res['success'] && isset($booking_res['credit_failed'])){
                $notification = ['danger','Sorry, unpaid amount exceeds your limit.'];
                session()->flash('message', $notification);
                return redirect('/customer/panel/bookings/');

            }elseif(!$booking_res['success'] && !$booking_res['booking']){
                $notification = ['danger','Sorry! Error occured during booking.'];
                session()->flash('message', $notification);
                return redirect('/customer/panel/bookings/');
            }

            //saving delivery order photo
            if ($request->file('delivery_order')) {
                $delivery_order_photo = $request->file('delivery_order')->store('public/booking_delivery_orders');
                $booking_res['booking']->update(['delivery_order_photo' => $delivery_order_photo]);
            }
            
            $notification = ['success','Your booking was placed successfully.'];
            session()->flash('message', $notification);
            return redirect('/customer/panel/bookings/'.$booking_res['booking']['unique_id']);

        }else{
            $manager = new BookingManager('full');

            $manager->setBookingCustomer($customer);
            $booking = $manager->createBooking($request->all());

            if (!$booking) {
                return 'Sorry! Error occured during booking';
            }

            //saving delivery order photo
            if ($request->file('delivery_order')) {
                $delivery_order_photo = $request->file('delivery_order')->store('public/booking_delivery_orders');
                $booking->update(['delivery_order_photo' => $delivery_order_photo]);
            }

            $notification = ['success','Your booking was placed successfully.'];
            session()->flash('message', $notification);

            return redirect('/customer/panel/bookings/'.$booking['unique_id']);
        }
    }

}
