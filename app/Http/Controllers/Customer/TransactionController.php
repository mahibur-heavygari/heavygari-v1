<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\MyLibrary\TransactionLib\CustomerTransaction;
use App\MyLibrary\TransactionLib\CorporateCustomerTransaction;

class TransactionController extends CustomerPanelController
{
    public function getTransactions( Request $request )
    {
    	$customer = $this->customerProfile();

    	if($this->isCorporateManager()){
    		$transaction = new CorporateCustomerTransaction($customer);
    	}else{
    		$transaction = new CustomerTransaction($customer);
    	}

    	$transactions = $transaction->getTransactions( $request );

    	return view('customer.transactions.history')->with('bookings', $transactions['bookings'])->with('total_fare', $transactions['total_fare'])->with('total_customer_cost', $transactions['total_customer_cost'])->with('admin_commission_total', $transactions['admin_commission_total'])->with('owner_earning_total', $transactions['owner_earning_total'])->with('driver_commission_total', $transactions['driver_commission_total'])->with('total_discount', $transactions['total_discount']);
        return view('customer.transactions.history');
    }
}
