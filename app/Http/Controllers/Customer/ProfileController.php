<?php

namespace App\Http\Controllers\Customer;

use App\CustomerProfile;
use Illuminate\Http\Request;
use App\Http\Requests\Customer\ProfileFormRequest;

class ProfileController extends CustomerPanelController
{
	/**
     * Display the specified resource.
     */
    public function show()
    {
        $profile = $this->customerProfile();

        return view('customer.profile.show')->with('profile', $profile);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CustomerProfile $profile)
    {
    	$profile = $this->customerProfile();

        return view('customer.profile.edit')->with('profile', $profile);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $profile = $this->customerProfile();

        // validation
        $formRequest = new ProfileFormRequest();
        $rules = $formRequest->rules($profile);
        $this->validate($request, $rules);

        $profile->updateProfile($request);

        //flash message
        $notification = ['success','Profile Updated Successfully'];
        session()->flash('message', $notification);

        return redirect('/customer/panel/profile');
    }

    /**
     * Complete customer profile page
     */
    public function complete(CustomerProfile $profile)
    {
        $profile = $this->customerProfile();

        return view('customer.profile.complete')->with('profile', $profile);
    }

    /**
     *  Complete customer's profile
     */
    public function storeComplete(ProfileFormRequest $request)
    {
        $customer = $this->customerProfile();

        $customer->completeProfile($request);

        //flash message
        $notification = ['success','Profile completed Successfully'];
        session()->flash('message', $notification);

        return redirect('/customer/panel/profile');
    }

    public function updateWebFcmToken(Request $request){
        $fcm_reg_token = $request->fcm_token;
        $profile = $this->customerProfile();
        $profile->updateFcmRegToken($fcm_reg_token);
        return ['success'=>true, 'data'=>'updated'];
    }
}
