<?php

namespace App\Http\Controllers\Customer;

use App\MyLibrary\DashboardLib\CustomerDashboard;
use App\MyLibrary\DashboardLib\CorporateCustomerDashboard;

class DashboardController extends CustomerPanelController
{
	/**
	 * show dashboard page
	 */
    public function index()
    {
        $is_corporate_manager = FALSE;

        //check customer whether corporate manager or not
        if($this->isCorporateManager()){
            $is_corporate_manager = TRUE;
    	   $dashboard = new CorporateCustomerDashboard($this->customerProfile());
        }else{
           $dashboard = new CustomerDashboard($this->customerProfile());
        }

    	$booking_stats = $dashboard->bookingStats();
    	$payment_stats = $dashboard->paymentStats();
        $popup_notification = $dashboard->customerPopupNotification();

        return view('customer.dashboard')->with([
            'profile' => $this->customerProfile(),
            'is_corporate_manager' => $is_corporate_manager,
            'booking_stats' => $booking_stats,
    		'payment_stats' => $payment_stats,
            'popup_notification' => $popup_notification
    	]);
    }

}
