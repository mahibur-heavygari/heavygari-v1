<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Sentinel;
use App\CustomerProfile;
use Validator;

class CorporateUserController extends CustomerPanelController
{
    public function users()
    {
        $corporate_customer = $this->customerProfile()->corporateCustomer;

        $users = $corporate_customer->customers()->wherePivot('user_type', 'user')->get();

        return view('customer.corporate_customer.users')->with('users', $users);
    }

    public function showUser( $id )
    {
        $customer = CustomerProfile::where('id', $id)->first();

        $rating = $customer->myAverageRating();

        $history = $customer->myBookings()->whereIn('status', ['completed', 'cancelled'])->orderBy('updated_at', 'desc')->get();

        return view('customer.corporate_customer.show_user')->with('profile', $customer)->with('rating', $rating)->with('history', $history);
    }

    public function createUser()
    {
        return view('customer.corporate_customer.create_user');
    }

    public function storeUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'name' => 'required',
            'password' => 'required'
        ]);

        $corporate_customer = $this->customerProfile()->corporateCustomer;

        try {
            $user_data = [
                'phone' => $request->phone,
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'sms' => 'আপনার রেজিস্ট্রেশন সফল হয়েছে, 
আপনার লগইন ইউজারনেম
ফোন নং: '.$request->phone.'
পাসওয়ার্ড: '.$request->password.'
টিম 
হেভিগাড়ী টেকনোলজিস লিমিটেড'
            ];

            $user = Sentinel::register($user_data);
            $role = Sentinel::findRoleBySlug('customer');
            $role->users()->attach($user);

            $user->status = 'active';
            $user->save();
            
            $profile = new CustomerProfile;
            $profile->user_id = $user->id;
            //$profile->profile_admin_id = $this->getFieldAdmin()->id;
            $profile->profile_complete = 'yes';
            $profile->token = uniqid();
            $profile->corporate_customer_id = $corporate_customer->id;
            $profile->save();

            $profile->completeProfile($request);

            $activation = Activation::create($user);
            $activation->code = mt_rand(1111, 9999);
            $activation->completed = 1;
            $activation->completed_at = date('Y-m-d H:i:s');
            $activation->save();

            $to = $user->phone;
            $ref_id = "$to"."_".time();
            //$status = SMSFacade::send($to, $user->sms, $ref_id);

            //rel_corporate_customers
            $corporate_customer->customers()->attach($profile);
            
            $user->sms = NULL;
            $user->save();

            return redirect('/customer/panel/users');

        } catch (\Exception $e) {
            return redirect('/customer/panel/users');
        }
    }
}
