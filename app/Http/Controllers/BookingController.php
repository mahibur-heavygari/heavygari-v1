<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display the specified booking.
     */
    public function show($tracking_code, Request $request)
    {
        $booking = Booking::where('tracking_code',$tracking_code)->first();
        if (!$booking) { abort(404); }

        $tracking_details = $booking->getTrackingModel();

        //$app_webview = ($request->view=='app_webview') ? true : false;
        $app_webview = true;

        return view('website.show-booking')->with('booking',$booking)->with('tracking_details',$tracking_details)->with('body_class', 'map-fixed')->with('app_webview', $app_webview);
    }
}
