<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MyLibrary\UserLib\ForgotPassword\RecoveryManager;

class ForgotPasswordController extends Controller
{
    /**
     * Forgot password page
     */
    public function create()
    {
        return view('auth.forgot_password');
    }

    /**
     * Forgot password form submitted
     */
    public function store(Request $request)
    {
        $recovery_manager = new RecoveryManager();

        $result = $recovery_manager->requestForRecovery($request);

        if ($result) {
            $notification = ['success','Please check your email!'];
            session()->flash('message', $notification);

            return redirect('session/login');
        } else {
            $notification = ['danger',$recovery_manager->error];
            session()->flash('message', $notification);

            return redirect()->back();
        }
    }

    /**
     * Reset password page
     */
    public function resetPassword(Request $request)
    {
        $recovery_manager = new RecoveryManager();

        $result = $recovery_manager->getResetPageAccess($request);

        if ($result) {
            return view('auth.reset_password');
        } else {
            $notification = ['danger',$recovery_manager->error];
            session()->flash('message', $notification);

            return redirect('session/login');
        }
    }

    /**
     * User had filled up reset password page
     */
    public function resetPasswordStore(Request $request)
    {
        $recovery_manager = new RecoveryManager();

        $result = $recovery_manager->resetUserPassword($request);

        if ($result) {
            $notification = ['success', 'Thanks! You can now login to your account.'];
             session()->flash('message', $notification);

            return redirect('session/login'); 
        } else {
            $notification = ['danger', $recovery_manager->error];
            session()->flash('message', $notification);

            return redirect()->back();
        }
    }
}
