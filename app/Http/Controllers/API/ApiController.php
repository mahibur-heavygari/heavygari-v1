<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Response as IlluminateResponse;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\CustomerProfile;

use App\DriverProfile;

use Validator;

class ApiController extends Controller
{
    protected $status_code = 200;

    //////////////////////
    // GETTER & SETTERS //
    //////////////////////

    public function getStatusCode()
    {
        return $this->status_code;
    }
    
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;

        return $this;
    }

    
    ///////////////////
    // API RESPONSES //
    ///////////////////
    
    /**
     * Resopose to api calls
     */
    public function apiResponse($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Error resopose to api calls
     */
    public function apiErrorResponse($message, $error_details = false)
    {
        $error = [
            'message' => $message,
        ];

        //dd($error_details);

        $error =  [
            'message' => 'This is a general message',
            'type' => null,
            'error_details' => null
        ];

        if ($error_details) {

            $error['message'] = isset($error_details['message']) ? $error_details['message'] :  $message;

            $error['type'] = $error_details['type'];

            if ($error_details['type'] == 'unverified_phone' || $error_details['type'] == 'incomplete_profile') {
                $error['error_details']['login_error'] = [
                    'token' => $error_details['token']
                ];

                if( isset($error_details['customer_id'])) {
                   $error['error_details']['login_error']['customer_id'] = $error_details['customer_id'];
                } if( isset($error_details['driver_id'])) {
                    $error['error_details']['login_error']['driver_id'] = $error_details['driver_id'];
                }               

                $error['error_details']['validation_errors'] = null;
            } else if ($error_details['type'] == 'validation_error') {
                $error['error_details']['validation_errors'] = $error_details['validation_errors'];
                $error['error_details']['login_error'] = null;
            }
            unset($error['error_details']['type']);
        } else {
            $error['message'] = $message;
            $error['type'] = null;
            $error['error_details'] = null;
        }

        return $this->apiResponse([
            'error' => $error
        ]);
    }
    
    public function respondError($message = 'Bad Request')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->apiErrorResponse($message);
    }

    public function respondErrorInDetails($message, $error_details)
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->apiErrorResponse($message, $error_details);
    }

     public function respondForbidden($message = 'Unautohrized')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_FORBIDDEN)->apiErrorResponse($message);
    }

    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->apiErrorResponse($message);
    }

    public function respondServerError($message = 'Internal Server Error')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->apiErrorResponse($message);
    }

    // Todo : refactor
    public function respondValidationError($fields, $message = 'Please correct the errors in the form!')
    {
        $error_details = [
            'type' => 'validation_error',
            'validation_errors' => $fields
        ];

        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->apiErrorResponse($message, $error_details);
    }



    //////////////////
    // Transformer  //
    //////////////////

    /**
     * Reform a collection
     */
    public function reformCollection($collection)
    {
        $collection->transform(function ($item, $key) {
            return $this->reform($item);
        });

        return $collection;
    }

    /**
     * Reform a eloquent object
     */
    public function reform($item)
    {
        return $item->getApiModel();
    }

    //////////////////
    //  Validation  //
    //////////////////

    /* 
     * Check for validation errors
     */
    protected function checkForValidation($formRequest, $request)
    {
        $rules = $formRequest->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        return false;
    }
    
    /* 
     * Convert validation errors to formatted array
     */
    public function transformErrorMessage($validator)
    {
        $validation_errors = $validator->errors()->messages();
        $error_messages = [];
    
        foreach($validation_errors as $key => $msg) {
            $error_messages[] = [
                'field' => $key,
                'errors' => $msg
            ];
        }

        return $error_messages;
    }
    
}
