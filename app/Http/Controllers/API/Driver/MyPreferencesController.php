<?php

namespace App\Http\Controllers\API\Driver;

use Validator;
use Illuminate\Http\Request;


class MyPreferencesController extends DriverAPIController
{
    public function getPreferences(Request $request)
    {
        $preferences = $this->getMyPreferences();

        if (!$preferences) {
            return $this->respondNotFound();
        }

        if ($request->type) {
            $preferences = $preferences[$request->type];
        }

        return $this->apiResponse([
            'data' => $preferences
        ]);
    }

    public function updatePreferences(Request $request)
    {
        // TODO : Validation
        
        $driver = $this->getDriver();

        $result = $driver->updatePreferences($request);

        if (!$result) {
            return $this->respondServerError('Sorry! Could not update your preferences');
        }

        return $this->apiResponse([
            'data' => ['message' => 'Preferences was updated']
        ]);
    }

    private function getMyPreferences() 
    {
        $driver = $this->getDriver();
        
        $preferences = [
            'booking_category' => $this->reform($driver->bookingCategoryPreference),
            'booking_type' => $this->reform($driver->bookingTypePreference),
            'distance' => $this->reform($driver->bookingDistancePreference)
        ];
           
        return $preferences;
    }
}
