<?php

namespace App\Http\Controllers\API\Driver;

use Illuminate\Http\Request;
use App\DriverProfile;
use Validator;

class UserController extends DriverAPIController
{
    // show profile 
    public function show()
    {
        $driver = $this->getDriver();

        return $this->apiResponse([
            'data' => $this->reform($driver)
        ]);
    }
	
    /**
	 * Update my profile
	 */
    public function update(Request $request)
    {
        // Todo : Validation
        
        $driver = $this->getDriver();

        // Todo :try catch
        $updated_profile = $driver->updateProfile($request);
    
        return $this->apiResponse([
            'data' => $this->reform($updated_profile)
        ]);
    }

    public function updateFcmToken (Request $request)
    {       
        $rules = [
            'fcm_registration_token' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        
        $driver = $this->getDriver();

        // Todo :try catch
        $updated_profile = $driver->updateFcmToken($request);
    
        return $this->apiResponse([
            'data' => $this->reform($updated_profile)
        ]); 
    }
}
