<?php

namespace App\Http\Controllers\API\Driver;

use Illuminate\Http\Request;

class DutyController extends DriverAPIController
{
    /**
     * get the driver's status (his vehicle and duty mode)
     */
    public function getStatus()
    {
        $driver = $this->getDriver();

        $current_mode = $driver->getCurrentMode();

        return $this->apiResponse([
            'data' => $current_mode
        ]);
    }

    /**
     * Set the driver and his vehicle to on duty mode
     */
    public function onDuty()
    {
        $driver = $this->getDriver();

        if (!$driver->onDutyMode()) {
            return $this->respondForbidden('Driver did not logged in on a vehicle yet!');
        }

        return $this->apiResponse([
            'data' => ['message' => 'Driver is now on On-Duty mode!']
        ]);
    }

    /**
     * Set the driver and his vehicle to off duty mode
     */
    public function offDuty()
    {
        $driver = $this->getDriver();

        $driver->offDutyMode();

        return $this->apiResponse([
            'data' => ['message' => 'Driver is now on Off-Duty mode!']
        ]);
    }

}
