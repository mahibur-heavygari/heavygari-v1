<?php

namespace App\Http\Controllers\API\Driver\Authentication;

use App\MyLibrary\UserLib\Login\DriverLogin;

use App\Http\Controllers\API\ApiController;

use Illuminate\Http\Request;

use App\DriverProfile;

use Validator;

class LoginController extends ApiController
{
    /**
     * login user
     */
    public function store(Request $request)
    {
        // Validation
        $rules = [
            'phone' => 'required',
            'password' => 'required',
            'device_type' => 'required',
            'device_registration_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $phone = $request->phone;
        $pass = $request->password;
        $vehicle_id = $request->vehicle_id;

        $login_manager = new DriverLogin();
        $user = $login_manager->login($phone,$pass);

        if ($user) {
            $this->setDeviceType($user, $request->device_type);
            $this->setFcmToken($user, $request->device_registration_id);

            $driver = $user->driverProfile;
            $data = $this->reform($driver);
            $data['api_token'] = $this->setApiToken($user);

            return $this->apiResponse([
                'data' => $data
            ]);
        } else {
            return $this->respondErrorInDetails('Sorry! could not login.', $login_manager->getError());
        }
    }

    /**
     * Set api token for current driver
     */
    private function setApiToken($user)
    {
        $api_token = str_random(60);

        $user->api_token = $api_token;

        $user->save();

        return $api_token;
    }

    /**
     * Set fcm registration token for user
     */
    private function setFcmToken($user, $device_token)
    {
        $user->fcm_registration_token = $device_token;

        $user->save();
    }
    
    /**
     * Set device type for user
     */
    private function setDeviceType($user, $device_type)
    {
        $user->device_type = $device_type;

        $user->save();
    }
}