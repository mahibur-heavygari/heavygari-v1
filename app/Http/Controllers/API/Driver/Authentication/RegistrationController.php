<?php

namespace App\Http\Controllers\API\Driver\Authentication;

use Validator;
use App\DriverProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use App\MyLibrary\UserLib\Login\DriverLogin;
use App\MyLibrary\UserLib\Verification\PhoneVerification;

class RegistrationController extends ApiController
{
    /**
     * Verify mobile number
     */
    public function phoneVerify($driver, Request $request)
    {
        //validation
        $rules = [
            'code' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        
        // get driver
        $driver = DriverProfile::where('id',$driver)->first();
        if (!$driver) {
            return $this->respondNotFound('Driver Not Found!');
        }
        $user = $driver->user;
        $activation = new PhoneVerification($user);

        // try to activate with given code
        $code = $request->code;
        if ($activation->complete($code)) {
            return $this->apiResponse([
                'data' => [
                    'message' => 'Mobile Verified Successfully.',
                ]
            ]);
        } else {
            return $this->respondError('Sorry! Incorrect Code.');
        }
    }

    /**
     * resened verification code
     */
    public function resendVerificationCode($driver)
    {   
        //get driver
        $driver = DriverProfile::where('id',$driver)->first();
        if (!$driver) {
            return $this->respondNotFound('Driver Not Found!');
        }
        $user = $driver->user;

        //send code
        $activation = new PhoneVerification($user);
        if ($activation->resend()) {
            return $this->apiResponse([
                'data' => [
                    'message' => 'Code Was Sent.',
                ]
            ]);
        } else {
            return $this->respondServerError('Sorry! Error Occured.');
        }
    }
}