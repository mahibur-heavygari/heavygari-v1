<?php

namespace App\Http\Controllers\API\Driver\Authentication;

use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use App\MyLibrary\UserLib\ForgotPassword\RecoveryManager;

class ForgotPasswordController extends ApiController
{
    /**
     * forgot password form submitted
     */
    public function store(Request $request)
    {
        $recovery_manager = new RecoveryManager();

        $result = $recovery_manager->requestForRecovery($request);

        if ($result) {
            return $this->apiResponse([
                'data' => [
                    'message' => 'Please check your email to recover the account.',
                ]
            ]);
        } else {
            return $this->respondError($recovery_manager->error);
        }
    }
}