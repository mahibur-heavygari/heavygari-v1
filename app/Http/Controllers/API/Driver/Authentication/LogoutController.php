<?php

namespace App\Http\Controllers\API\Driver\Authentication;

use App\Http\Controllers\API\Driver\DriverAPIController;

use Illuminate\Http\Request;

use App\DriverProfile;

class LogoutController extends DriverAPIController
{
    /**
	 * logout user
	 */
    public function store(Request $request)
    {
        $driver = $this->getDriver();

        $driver->exitTheVehicle();

        $user = $driver->user; 

        $user->api_token = null;
        $user->device_type = null;
        $user->fcm_registration_token = null;
        $user->web_fcm_registration_token = null;

        $user->save();

        return $this->apiResponse([
            'data' => ['message' => 'User Logged Out Successfully']
        ]);
    }
}
