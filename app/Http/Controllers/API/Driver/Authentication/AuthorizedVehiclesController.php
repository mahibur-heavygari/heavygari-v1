<?php

namespace App\Http\Controllers\API\Driver\Authentication;

use App\Http\Controllers\API\Driver\DriverAPIController;

use Illuminate\Http\Request;

use App\DriverProfile;

class AuthorizedVehiclesController extends DriverAPIController
{
    /**
     * logged in to this vehicle
     */
    public function index()
    {
        $driver = $this->getDriver();

        $authorized_vehicles = $driver->myAuthorizedVehicles;

        return $this->apiResponse([
            'data' => $this->reformCollection($authorized_vehicles)
        ]);
    }

    /**
	 * logged in to this vehicle
	 */
    public function store($vehicle_id)
    {
        $driver = $this->getDriver();

        $authorized_vehicle = $driver->myAuthorizedVehicles->where('id', $vehicle_id)->first();
        if (!$authorized_vehicle) {
            $this->error['message'] = 'This vehicle does not belong to this driver';
            $this->error['type'] = 'incorrect_vehicle';
            return $this->respondNotFound();
        }

        $driver->enterTheVehicle($authorized_vehicle);

        $driver->onDutyMode();

        return $this->apiResponse([
            'data' => ['message' => 'Vehicle Selected Successfully']
        ]);
    }
}
