<?php

namespace App\Http\Controllers\API\Driver;

use Illuminate\Http\Request;

use App\Booking;

class BookingController extends DriverAPIController
{
    /**
     * Home screen
     */
    public function home()
    {
        $driver = $this->getDriver();

        $stats =  [
            'current' => [
              [
                'title' => 'One Way Trips',
                'trip_type' => 'single',
                'booking_type' => 'on-demand',
                'count' => $driver->availableBookingsForMe('on-demand', 'single')->count()
              ],
              [
                'title' => 'Round Trips',
                'trip_type' => 'round',
                'booking_type' => 'on-demand',
                'count' => $driver->availableBookingsForMe('on-demand', 'round')->count()
              ],
            ],
            'advance' => [
              [
                'title' => 'One Way Trips',
                'trip_type' => 'single',
                'booking_type' => 'advance',
                'count' => $driver->availableBookingsForMe('advance', 'single')->count()
              ],
              [
                'title' => 'Round Trips',
                'trip_type' => 'round',
                'booking_type' => 'advance',
                'count' => $driver->availableBookingsForMe('advance', 'round')->count()
              ],
            ],
        ];

        return $this->apiResponse([
            'data' => $stats
        ]);
    }

    /**
     * List of available jobs
     */
    public function available(Request $request)
    {
        $driver = $this->getDriver();

        $trip_type = ($request->trip_type) ? ($request->trip_type) : false;
        
        $booking_type = ($request->booking_type) ? $request->booking_type : false;

        $bookings = $driver->availableBookingsForMe($booking_type, $trip_type);

        return $this->apiResponse([
            'data' => $this->reformCollection($bookings)
        ]);
    }
	
    /**
	 * Show details of a booking
	 */
    public function show($unique_id)
    {
        $booking = Booking::open()->where('unique_id', $unique_id)->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        return $this->apiResponse([
            'data' => $this->reform($booking)
        ]);
    }

    /**
     * Accept a booking
     */
    public function accept($unique_id)
    {
        $driver = $this->getDriver();

        $booking = Booking::where('unique_id', $unique_id)->open()->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        $accept = $booking->accept($driver);

        return $this->apiResponse([
            'data' => ['message' => 'Booking was accepted']
        ]);
    }
}
