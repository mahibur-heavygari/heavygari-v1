<?php

namespace App\Http\Controllers\API\Driver;

use App\UserFeedback;
use Illuminate\Http\Request;
use Validator;

class GeneralController extends DriverAPIController
{
    public function PostContactUs(Request $request)
    {
        $rules = [
            'message' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $contact_data= array(
            'user_id'   => $this->getDriver()->user->id, 
            'user_type' => 'driver', 
            'message'   => $request->message 
        );
        $contactMsgObj = UserFeedback::create($contact_data);
        
        if(!$contactMsgObj)
            return $this->respondServerError('Sorry! Could not take the feedback.');

        return $this->apiResponse([
            'data' => [
                'message'   => 'Feedback was taken'
            ]
        ]);
    }
}
