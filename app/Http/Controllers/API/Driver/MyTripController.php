<?php

namespace App\Http\Controllers\API\Driver;

use Validator;
use App\Booking;
use Illuminate\Http\Request;
use App\Events\Booking\CancelBooking;

class MyTripController extends DriverAPIController
{
    /**
     * List of all my trips
     */
    public function index(Request $request)
    {
        $driver = $this->getDriver();

        $bookings = $driver->myTrips();

        if ($request->status) {
            $scope = $request->status;
            $bookings = $bookings->$scope();
        } else {
            $bookings = $bookings->current();
        }

        if ($request->booking_type) {
            $bookings = $bookings->where('booking_type', $request->booking_type);
        }

        $bookings = $bookings->orderBy('datetime', 'asc')->get();

        return $this->apiResponse([
            'data' => $this->reformCollection($bookings)
        ]);
    }

    /**
     * List of all completed trips
     */
    public function history()
    {
        $driver = $this->getDriver();

        $bookings = $driver->myTrips()->completed()->orderBy('updated_at', 'desc')->get();

        return $this->apiResponse([
            'data' => $this->reformCollection($bookings)
        ]);
    }
	
    /**
	 * Show details of a trip
	 */
    public function show($unique_id)
    {
        $driver = $this->getDriver();

        $booking = Booking::where('unique_id', $unique_id)->first();
        if (!$booking) {
            return $this->respondNotFound();
        }

        // Authorized Trip?
        if ( ! (isset($booking->driver) && $booking->driver->id==$driver->id) ) {
            return $this->respondForbidden('Unauthorized Trip!');
        }

        return $this->apiResponse([
            'data' => $this->reform($booking)
        ]);
    }

    /**
     * Start a trip
     */
    public function start($unique_id)
    {
        $driver = $this->getDriver();

        $booking = Booking::where(['unique_id' => $unique_id, 'driver_profile_id' => $driver->id])->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        $start = $booking->start();

        return $this->apiResponse([
            'data' => ['message' => 'Trip was started']
        ]);
    }

    /**
     * Complete a trip
     */
    public function complete($unique_id)
    {
        $driver = $this->getDriver();

        $booking = Booking::where(['unique_id' => $unique_id, 'driver_profile_id' => $driver->id])->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        $end = $booking->complete();

        return $this->apiResponse([
            'data' => ['message' => 'Trip was completed']
        ]);        
    }

    /**
     * Cancel a trip
     */
    public function cancel($unique_id, Request $request)
    {
        $driver = $this->getDriver();

        $booking = Booking::where(['unique_id' => $unique_id, 'driver_profile_id' => $driver->id])->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        if ($booking->status == 'ongoing') {
           return $this->respondForbidden("Sorry, You can not cancel any ongoing booking");
        }elseif ($booking->status == 'completed') {
           return $this->respondForbidden("Sorry, You can not cancel any completed booking");
        }elseif ($booking->status == 'cancelled') {
           return $this->respondForbidden("Sorry, You can not cancel any cancelled booking");
        }

        if($booking->status == 'upcoming'){
            event(new CancelBooking($booking , 'upcoming', 'driver'));
        }

        $end = $booking->cancel($request->reason);
        
        $booking->cancelled_by = 'Driver';
        $booking->save();

        return $this->apiResponse([
            'data' => ['message' => 'Trip was cancelled']
        ]);        
    }

    /**
     * Does the customer/recipient paid?
     */
    public function updatePaymentStatus(Request $request, $unique_id)
    {
        $driver = $this->getDriver();

        $booking = Booking::where(['unique_id' => $unique_id, 'driver_profile_id' => $driver->id])->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        $booking->updatePaymentCollection($request->status);

        return $this->apiResponse([
            'data' => ['message' => 'Thanks for your update.']
        ]);
    }

    /**
     * Rate the customer
     */
    public function giveRating($unique_id, Request $request)
    {
        $driver = $this->getDriver();

        $booking = Booking::where('unique_id', $unique_id)->first();
        if (!$booking) {
            return $this->respondNotFound();
        }

        //validation
        $rules = [
            'rating' => 'required|numeric'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $rate_customer = $driver->rateCustomer($booking, $request->rating, $request->review);

        if ($rate_customer['error']) {
            return $this->respondError($rate_customer['error']);
        }

        return $this->apiResponse([
            'data' => [
                'message' => 'Thanks for your rating'
            ]
        ]);
    }

    public function uploadDeliveryInvoice($unique_id, Request $request)
    {
        $driver = $this->getDriver();

        $booking = Booking::where('unique_id', $unique_id)->first();
        if (!$booking) {
            return $this->respondNotFound();
        }

        //validation
        $rules = [
            'delivery_invoice' => 'required|file'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        if ($request->file('delivery_invoice')) {
            $delivery_invoice_photo = $request->file('delivery_invoice')->store('public/booking_delivery_invoices');
            $booking->update(['delivery_invoice_photo' => $delivery_invoice_photo]);
        }

        return $this->apiResponse([
            'data' => [
                'message' => 'Delivery invoice uploaded successfully'
            ]
        ]);
    }
}
