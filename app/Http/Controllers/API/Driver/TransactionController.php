<?php

namespace App\Http\Controllers\API\Driver;

use App\Booking;
use Illuminate\Http\Request;

class TransactionController extends DriverAPIController
{
    /**
     * List of my current bookings
     */
    public function index(Request $request)
    {
        $month = ($request->month) ? $request->month : date('m');
        $year = ($request->year) ? $request->year : date('Y');

        $start_month = $year."-".$month."-".'01';
        $end_month = $year."-".$month."-".'31';

        if(!$request->month && !$request->year){
            $start_month = '2018-01-01';
            $end_month = '2022-12-31';
        }

        $driver = $this->getDriver();

        $bookings = $driver->myTrips()->completed()->where('datetime', '>=', $start_month)->where('datetime', '<=', $end_month);

        $bookings = $bookings->orderBy('id', 'desc')->get();

        $bookings->transform(function ($item, $key) {
            return $item->getTransactionModel();
        });
        
        $driver_total_due = $bookings->where('driver_earning_status', 'due')->sum('driver_earning');
        $driver_total_unpaid = $bookings->where('driver_earning_status', 'unpaid')->sum('driver_earning');
        $driver_total_paid = $bookings->where('driver_earning_status', 'paid')->sum('driver_earning');
        $driver_total_earnings = $bookings->sum('driver_earning');

        return $this->apiResponse([
            'data' => [
                'transactions' => $bookings,
                'driver_total_due' => $driver_total_due,
                'driver_total_unpaid' => $driver_total_unpaid,
                'driver_total_paid' => $driver_total_paid,
                'driver_total_earnings' => $driver_total_earnings,
                'total' => $bookings->sum('fare')
            ]
        ]);
    }
}
