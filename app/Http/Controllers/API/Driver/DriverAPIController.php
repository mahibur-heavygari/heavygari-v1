<?php

namespace App\Http\Controllers\API\Driver;

use App\Http\Controllers\API\ApiController;

use Illuminate\Http\Request;

use App\DriverProfile;

use Auth;

class DriverAPIController extends ApiController
{
    /**
     * Get current logged in driver profile
     */
    public function getDriver()
    {
        $current_user = Auth::guard('api')->user();

        $driver = DriverProfile::where('user_id', $current_user->id)->first();

        return $driver ? $driver : false;
    }
}
