<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

class GeneralController extends ApiController
{
    /**
     * Show page contents
     */
    public function showPage($slug)
    {
        if (view()->exists('website.pages.'.$slug)) {
        	$view = view()->make('website.pages.contents.'.$slug);
			$contents = (string) $view;

        	return $this->apiResponse([
	            'data' => [
	                'html' => $contents
	            ]
	        ]);
		}
		
		return $this->respondNotFound();
    }
}
