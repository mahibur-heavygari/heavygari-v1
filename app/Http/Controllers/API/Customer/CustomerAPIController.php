<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\API\ApiController;

use Illuminate\Http\Request;

use App\CustomerProfile;

use Auth;

class CustomerAPIController extends ApiController
{
    /**
     * Get current logged in customer profile
     */
    public function getCustomer()
    {
        $current_user = Auth::guard('api')->user();

        $customer = CustomerProfile::where('user_id', $current_user->id)->first();

        return $customer ? $customer : false;
    }
}
