<?php

namespace App\Http\Controllers\API\Customer;

use App\Point;
use App\VehicleType;
use App\BaseWeightCategory;
use App\BaseProductCategory;
use Illuminate\Http\Request;
use App\BaseVolumetricCategory;

class BaseController extends CustomerAPIController
{
    // list of vehicle types 
    public function vehicleTypes()
    {
        // $vehicle_types = VehicleType::all();
        // only bus
        $vehicle_types = VehicleType::where('title', 'Bus (Non AC)')->orWhere('title', 'Bus (AC)')->orWhere('title', 'XL Truck')->orWhere('title', 'Bus (Business Class)')->orWhere('title', 'Truck  (18feet)')->orWhere('title', 'Cover Truck  (22feet)')->orWhere('title', 'Trailer Truck 2XL  (20feet)')->orWhere('title', 'Trailer Truck 3XL  (40feet)')->orWhere('title', 'Pickup Van  (12feet)')->orWhere('title', 'Mini Pickup Van  (7feet)')->orderBy('preference', 'asc')->get();

        return $this->apiResponse([
            'data' => $this->reformCollection($vehicle_types)
        ]);
    }
	
    // list of points 
    public function points()
    {
        $pointsWithDHKandCTG = Point::where('title', '=', 'Dhaka')->orWhere('title', '=', 'Chittagong')->orderBy('title', 'DESC')->get();

        $pointsWithoutDHKandCTG = Point::where('title', '!=', 'Dhaka')->where('title', '!=', 'Chittagong')->orderBy('title', 'ASC')->get();

        $merged = $pointsWithDHKandCTG->merge($pointsWithoutDHKandCTG);

        return $this->apiResponse([
            'data' => $this->reformCollection($merged)
        ]);        
    }

    // List of Weight Categories 
    public function productCategories()
    {
        $product_categories = BaseProductCategory::all();

        return $this->apiResponse([
            'data' => $this->reformCollection($product_categories)
        ]);        
    }

    // List of Weight Categories 
    public function weightCategories()
    {
        $weight_categories_db = BaseWeightCategory::all();

        $weight_categories = $this->reformCollection($weight_categories_db);

        $weight_categories[] = [
            'id' => 0,
            'title' => 'Custom',
            'weight' => null
        ];

        return $this->apiResponse([
            'data' =>  $weight_categories
        ]);
    }

    // List of Volumetric Categories 
    public function volumetricCategories()
    {
        $volumetric_categoreis_db = BaseVolumetricCategory::all();

        $volumetric_categoreis = $this->reformCollection($volumetric_categoreis_db);

        $volumetric_categoreis[] = [
            'id' => 0,
            'title' => 'Custom',
            'height' => null,
            'width' => null,
            'length' => null,
            'volumetric_weight' => null
        ];

        return $this->apiResponse([
            'data' =>  $volumetric_categoreis
        ]);      
    }
}
