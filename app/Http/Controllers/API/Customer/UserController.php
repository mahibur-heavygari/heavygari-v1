<?php

namespace App\Http\Controllers\API\Customer;

use Validator;
use App\CustomerProfile;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\Customer\ProfileFormRequest;
use App\Http\Requests\User\ChangePasswordFormRequest;

class UserController extends CustomerAPIController
{
    // show profile 
    public function show()
    {
        $customer = $this->getCustomer();
        
        return $this->apiResponse([
            'data' => $this->reform($customer)
        ]);

    }
	
    /**
	 * Update my profile
	 */
    public function update(Request $request)
    {
        $customer = $this->getCustomer();

        // validation
        $formRequest = new ProfileFormRequest();
        $rules = $formRequest->rules($customer);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        
        try {
            $updated_profile = $customer->updateProfile($request);

            return $this->apiResponse([
                'data' => $this->reform($updated_profile)
            ]);

        } catch (\Exception $e) {
            //dd($e->getMessage())
            return $this->respondServerError();
        }
    }

    /**
     * Update Photo (avatar, national id etc)
     */
    public function uploadPhoto(Request $request)
    {
        // Validation
        $rules = [
            'type' => [ 
                'required',
                Rule::in(['avatar', 'national_id']),
            ],
            'photo' => 'image',
            'national_id_photo' => 'image',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $customer = $this->getCustomer();

        if ($request->type=='avatar') {
            $url = $customer->user->uploadProfilePicture($request);
        } else if ($request->type=='national_id') {
            $url = $customer->user->uploadNationalId($request);
        }

        return $this->apiResponse([
            'data' => [
                'message' => 'Photo was uploaded',
                'url' =>  url(\Storage::url($url))
            ]
        ]);
    }

    /**
     * Change Password
     */
    public function changePassword(Request $request)
    {
        $customer = $this->getCustomer();

        // validation
        $formRequest = new ChangePasswordFormRequest();
        $rules = $formRequest->rules($customer);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        
        try {
            $password_change = $customer->user->changePassword($request);

            if (isset($password_change['error'])) {
                return $this->respondError($password_change['error']);
            } 

            return $this->apiResponse([
                'data' => [
                    'message' => 'Your password has been changed successfully',
                ]
            ]);

        } catch (\Exception $e) {
            //dd($e->getMessage())
            return $this->respondServerError();
        }
        
    }

    public function updateFcmToken (Request $request)
    {       
        $rules = [
            'fcm_registration_token' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        
        $customer = $this->getCustomer();

        // Todo :try catch
        $updated_profile = $customer->updateFcmToken($request);
    
        return $this->apiResponse([
            'data' => $this->reform($updated_profile)
        ]); 
    }

}
