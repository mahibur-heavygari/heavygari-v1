<?php

namespace App\Http\Controllers\API\Customer\Authentication;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use App\MyLibrary\UserLib\ForgotPassword\RecoveryManager;

class ForgotPasswordController extends ApiController
{
    /**
     * forgot password form submitted
     */
    public function store(Request $request)
    {
        // Validation
        $rules = [
            'email' => 'required|email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $recovery_manager = new RecoveryManager();
        
        $result = $recovery_manager->requestForRecovery($request);

        if ($result) {
            return $this->apiResponse([
                'data' => [
                    'message' => 'Please check your email to recover the account.',
                ]
            ]);
        } else {
            return $this->respondError($recovery_manager->error);
        }
    }
}