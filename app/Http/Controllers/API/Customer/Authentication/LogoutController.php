<?php

namespace App\Http\Controllers\API\Customer\Authentication;

use App\Http\Controllers\API\Customer\CustomerAPIController;

use Illuminate\Http\Request;

use App\CustomerProfile;

class LogoutController extends CustomerAPIController
{
    /**
	 * logout user
	 */
    public function store(Request $request)
    {
        $user = $this->getCustomer()->user; 

        $user->api_token = null;
        $user->device_type = null;
        $user->fcm_registration_token = null;
        $user->web_fcm_registration_token = null;

        $user->save();

        return $this->apiResponse([
            'data' => ['message' => 'User Logged Out Successfully']
        ]);
    }
}
