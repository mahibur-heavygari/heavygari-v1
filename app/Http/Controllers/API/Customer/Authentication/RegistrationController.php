<?php

namespace App\Http\Controllers\API\Customer\Authentication;

use Validator;
use App\CustomerProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use App\Http\Requests\Customer\RegistrationFormRequest;
use App\MyLibrary\UserLib\Verification\PhoneVerification;
use App\Http\Requests\Customer\CompleteProfileFormRequest;
use App\MyLibrary\UserLib\Registration\CustomerRegistration;

class RegistrationController extends ApiController
{
    /**
	 * login user
	 */
    public function store(Request $request)
    {
        // Validation
        $rules = (new RegistrationFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $registration = new CustomerRegistration($request->all());
        $customer = $registration->registerCustomer();
        
        if ($customer) {
            return $this->apiResponse([
                'data' => [
                    'message' => 'Successfully Registered',
                    'customer_id' => $customer->customerProfile->id
                ]
            ]);
        } else {
            // return $registration->getError()->getMessage();
            return $this->respondServerError();
        }
    }

    /**
     * Verify mobile number
     */
    public function phoneVerify($customer, Request $request)
    {
        //validation
        $rules = [
            'code' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        
        // get customer
        $customer = CustomerProfile::where('id',$customer)->first();
        if (!$customer) {
            return $this->respondNotFound('Customer Not Found!');
        }
        $user = $customer->user;
        $activation = new PhoneVerification($user);

        // try to activate with given code
        $code = $request->code;
        if ($activation->complete($code)) {
            /*return $this->apiResponse([
                'data' => [
                    'message' => 'Mobile Verified Successfully.',
                    'customer_id' => $customer->id,
                    'token' => $customer->token,
                ]
            ]);*/
            $customer->completeAtRegistration();
            return $this->apiResponse([
                'data' => [
                    'message' => 'Thanks for registering. Please login to continue.',
                ]
            ]);
        } else {
            return $this->respondError('Sorry! Incorrect Code.');
        }
    }

    /**
     * resened verification code
     */
    public function resendVerificationCode($customer)
    {   
        //get customer
        $customer = CustomerProfile::where('id',$customer)->first();
        if (!$customer) {
            return $this->respondNotFound('Customer Not Found!');
        }
        
        //send code
        $user = $customer->user;
        $activation = new PhoneVerification($user);
        if ($activation->resend()) {
            return $this->apiResponse([
                'data' => [
                    'message' => 'Code Was Sent.',
                ]
            ]);
        } else {
            return $this->respondServerError('Sorry! Error Occured.');
        }
    }

    /**
     *  Complete customer's profile
     */
    public function completeProfile($customer, Request $request)
    {
        // get customer
        $customer = CustomerProfile::where('id',$customer)->first();
        if (!$customer) {
            return $this->respondNotFound('Customer Not Found!');
        }

        // valid token?
        if ($customer->token != $request->token) {
            return $this->respondForbidden('Unauthorized!');
        }
    
        $formRequest = new CompleteProfileFormRequest();
        $rules = $formRequest->rules($customer);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        } 

        $customer->completeProfile($request);

        return $this->apiResponse([
            'data' => [
                'message' => 'Thanks for registering. Please login to continue.',
            ]
        ]);
       
        //validation
        /*
        $rules = [
            'national_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $customer->user->national_id = $request->national_id;
        $customer->user->save();

        $customer->profile_complete = 'yes';
        $customer->save();

        */
    }
}
