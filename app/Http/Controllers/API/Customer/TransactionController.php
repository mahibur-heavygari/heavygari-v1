<?php

namespace App\Http\Controllers\API\Customer;

use App\Booking;
use Illuminate\Http\Request;

class TransactionController extends CustomerAPIController
{
    /**
     * List of my current bookings
     */
    public function index(Request $request)
    {
        $month = ($request->month) ? $request->month : date('m');
        $year = ($request->year) ? $request->year : date('Y');

        $start_month = $year."-".$month."-".'01';
        $end_month = $year."-".$month."-".'31';

        if(!$request->month && !$request->year){
            $start_month = '2018-01-01';
            $end_month = '2022-12-31';
        }

        $customer = $this->getCustomer();

        $bookings = $customer->myBookings()->completed()->where('datetime', '>=', $start_month)->where('datetime', '<=', $end_month);

        $bookings = $bookings->orderBy('id', 'desc')->get();

        $bookings->transform(function ($item, $key) {
            return $item->getTransactionModel();
        });
        
        return $this->apiResponse([
            'data' => [
                'transactions' => $bookings,
                'total' => $bookings->sum('fare')
            ]
        ]);
    }
}
