<?php

namespace App\Http\Controllers\API\Customer;

use Illuminate\Http\Request;
use App\MyLibrary\BookingLib\BookingManager;
use App\Http\Requests\Customer\SharedBookingFormRequest;
use App\Http\Requests\Customer\SharedBookingFareRequest;

class SharedBookingController extends CustomerAPIController
{
    /**
     * Get estimated fare for a Shared Booking
     */
    public function getCost(Request $request)
    {
        //validation error?
        $validation_errors = $this->checkForValidation(new SharedBookingFareRequest(), $request);
        if ($validation_errors) {
            return $validation_errors;
        }
        
        // get fare
        $manager = new BookingManager('shared');
        $price_breakdown = $manager->getFare($request->all());

        if (!$price_breakdown) {
            return $this->respondServerError('Sorry! Could not calcualte the cost.');
        }

        return $this->apiResponse([
            'data' => $price_breakdown
        ]);
    }

    /**
     * Create a new Shared Booking
     */
    public function create(Request $request)
    {
        //validation error?
        $validation_errors = $this->checkForValidation(new SharedBookingFormRequest(), $request);
        if ($validation_errors) {
            return $validation_errors;
        }
        
        $customer = $this->getCustomer();

        // do the booking
        $manager = new BookingManager('shared');
        $manager->setBookingCustomer($customer);
        $booking = $manager->createBooking($request->all());
        
        if (!$booking) {
            return $this->respondServerError('Sorry! Could not do the booking.');
        }
        
        return $this->apiResponse([
            'data' => [
                'message' => 'Booking was created',
                'booking_id' => $booking['unique_id']
            ]
        ]);
    }
}
