<?php

namespace App\Http\Controllers\API\Customer;

use Validator;
use App\Booking;
use Illuminate\Http\Request;
use App\MyLibrary\BookingLib\BookingManager;
use App\Events\Booking\CancelBooking;

class MyBookingController extends CustomerAPIController
{
    
    /**
     * List of my current bookings
     */
    public function index(Request $request)
    {
        $customer = $this->getCustomer();

        if ($request->status) {
            $scope = $request->status;
            $bookings = $customer->myBookings()->$scope();
        } else {
            $bookings = $customer->myBookings()->current();
        }

        if ($request->booking_type) {
            $bookings = $bookings->where('booking_type', $request->booking_type);
        }

        $bookings = $bookings->orderBy('id', 'desc')->get();
        
        return $this->apiResponse([
            'data' => $this->reformCollection($bookings)
        ]);
    }

    /**
     * History
     */
    public function history()
    {
        $customer = $this->getCustomer();

        $bookings = $customer->myBookings()->whereIn('status', ['completed', 'cancelled'])->orderBy('updated_at', 'desc')->get();
        
        return $this->apiResponse([
            'data' => $this->reformCollection($bookings)
        ]);
    }

    /**
	 * Show details of a booking
	 */
    public function show($unique_id)
    {
        $customer = $this->getCustomer();

        $booking = Booking::where('unique_id', $unique_id)->first();

        if (!$booking) {
            return $this->respondNotFound();
        } else if ($booking->customer_profile_id != $customer->id) {
            return $this->respondForbidden("Sorry, This is not your booking");
        }

        return $this->apiResponse([
            'data' => $this->reform($booking)
        ]);
    }

    /**
     * Cancel a open booking
     */
    public function cancel($unique_id, Request $request)
    {
        $customer = $this->getCustomer();

        $booking = Booking::where('unique_id', $unique_id)->where('customer_profile_id', $customer->id)->first();

        if (!$booking) {
            return $this->respondNotFound();
        }

        if ($booking->status == 'ongoing') {
           return $this->respondForbidden("Sorry, You can not cancel any ongoing booking");
        }elseif ($booking->status == 'completed') {
           return $this->respondForbidden("Sorry, You can not cancel any completed booking");
        }elseif ($booking->status == 'cancelled') {
           return $this->respondForbidden("Sorry, You can not cancel any cancelled booking");
        }

        if($booking->status == 'upcoming'){
            event(new CancelBooking($booking , 'upcoming', 'customer'));
        }

        $booking->cancel($request->reason);
        
        $booking->cancelled_by = 'Customer';
        $booking->save();

        return $this->apiResponse([
            'data' => [
                'message' => 'Booking was cancelled'
            ]
        ]);
    }

    /**
     * Rate the driver
     */
    public function giveRating($unique_id, Request $request)
    {
        $customer = $this->getCustomer();

        $booking = Booking::where('unique_id', $unique_id)->first();
        if (!$booking) {
            return $this->respondNotFound();
        }

        //validation
        $rules = [
            'rating' => 'required|numeric'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $rate_driver = $customer->rateDriver($booking, $request->rating, $request->review);

        if ($rate_driver['error']) {
            return $this->respondError($rate_driver['error']);
        }

        return $this->apiResponse([
            'data' => [
                'message' => 'Thanks for your rating'
            ]
        ]);
    }

    public function trackBooking( $unique_id ){
        $customer = $this->getCustomer();
        $booking = Booking::where('unique_id',$unique_id)->first();
        
        if (!$booking) {
            return $this->respondNotFound();
        } else if ($booking->customer_profile_id != $customer->id) {
            return $this->respondForbidden("Sorry, This is not your booking");
        }       
        $tracking_details = $booking->getTrackingModel(); 

        return $this->apiResponse([
            'data' => $tracking_details
        ]);
    }
}
