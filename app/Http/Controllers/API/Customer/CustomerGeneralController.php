<?php

namespace App\Http\Controllers\API\Customer;

use App\UserFeedback;
use Illuminate\Http\Request;
use Validator;
use App\PopUpSetting;
use Carbon\Carbon;

class CustomerGeneralController extends CustomerAPIController
{
    public function PostContactUs(Request $request)
    {
        $rules = [
            'message' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $contact_data= array(
            'user_id'   => $this->getCustomer()->user->id, 
            'user_type' => 'customer', 
            'message'   => $request->message 
        );
        $contactMsgObj = UserFeedback::create($contact_data);
        
        if(!$contactMsgObj)
            return $this->respondServerError('Sorry! Could not take the feedback.');

        return $this->apiResponse([
            'data' => [
                'message'   => 'Feedback was taken'
            ]
        ]);
    }

    public function getPopUp()
    {
        $pop_up = PopUpSetting::orderBy('created_at', 'desc')->first();

        if(is_null($pop_up)){
            return $this->apiResponse([
                'data' => array('id' => 0 )
            ]);
        }

        if(!is_null($pop_up->expired_at)){
            $expired_at = (string) $pop_up->expired_at;
            $present_time = (string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka');

            if( strtotime( $expired_at ) < strtotime( $present_time )){
                return $this->apiResponse([
                    'data' => array('id' => 0 )
                ]);
            }
        }

        return $this->apiResponse([
            'data' => $this->reform($pop_up)
        ]);
    }
}
