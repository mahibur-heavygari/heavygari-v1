<?php

namespace App\Http\Controllers\API\Customer;

use Illuminate\Http\Request;
use App\MyLibrary\BookingLib\BookingManager;
use App\MyLibrary\CorporateBookingLib\BookingManager as CorporateBookingManager;
use App\Http\Requests\Customer\FullBookingFormRequest;
use App\Http\Requests\Customer\FullBookingFareRequest;

class FullBookingController extends CustomerAPIController
{
    /**
     * Get estimated fare for a full booking
     */
    public function getCost(Request $request)
    {
        //validation error?
        $validation_errors = $this->checkForValidation(new FullBookingFareRequest(), $request);
        if ($validation_errors) {
            return $validation_errors;
        }
        
        $customer = $this->getCustomer();

        // get fare
        
        //check customer whether corporate or not
        if($customer->corporate_customer_id){
            $manager = new CorporateBookingManager('full');
        }else{
            $manager = new BookingManager('full');
        }

        $manager->setBookingCustomer($customer);
        $price_breakdown = $manager->getFare($request->all());

        if (!$price_breakdown) {
            return $this->respondServerError('Sorry! Could not calcualte the cost.');
        }

        return $this->apiResponse([
            'data' => $price_breakdown
        ]);
    }

    /**
     * Create a new Full Booking
     */
    public function create(Request $request)
    {
        //validation error?
        $validation_errors = $this->checkForValidation(new FullBookingFormRequest(), $request);
        if ($validation_errors) {
            return $validation_errors;
        }
        
        $customer = $this->getCustomer();

        // do the booking

        //check customer whether corporate or not
        if($customer->corporate_customer_id){
            $manager = new CorporateBookingManager('full');

            $manager->setBookingCustomer($customer);
            $booking_res = $manager->createBooking($request->all());

            if(!$booking_res['success'] && isset($booking_res['debit_failed'])){
                return $this->respondError('Sorry! Insufficient Balance');

            }elseif(!$booking_res['success'] && isset($booking_res['credit_failed'])){
                return $this->respondError('Sorry! Uunpaid amount exceeds your limit');

            }elseif(!$booking_res['success'] && !$booking_res['booking']){
                return $this->respondError('Sorry! Could not do the booking.');
            }

            //saving delivery order photo
            if ($request->file('delivery_order')) {
                $delivery_order_photo = $request->file('delivery_order')->store('public/booking_delivery_orders');
                $booking_res['booking']->update(['delivery_order_photo' => $delivery_order_photo]);
            }
            
            return $this->apiResponse([
                'data' => [
                    'message' => 'Booking was created',
                    'booking_id' => $booking_res['booking']['unique_id']
                ]
            ]);
            
        }else{
            $manager = new BookingManager('full');

            $manager->setBookingCustomer($customer);
            $booking = $manager->createBooking($request->all());
            
            if (!$booking) {
                return $this->respondError('Sorry! Could not do the booking.');
            }

            //saving delivery order photo
            if ($request->file('delivery_order')) {
                $delivery_order_photo = $request->file('delivery_order')->store('public/booking_delivery_orders');
                $booking->update(['delivery_order_photo' => $delivery_order_photo]);
            }
            
            return $this->apiResponse([
                'data' => [
                    'message' => 'Booking was created',
                    'booking_id' => $booking['unique_id']
                ]
            ]);
        }
    }
}
