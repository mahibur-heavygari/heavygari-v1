<?php

namespace App\Http\Controllers\API\FieldAdmin\Customer;

use App\Http\Controllers\API\FieldAdmin\FieldAdminAPIController;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\MyLibrary\SMSLib\SMSFacade;
use Illuminate\Http\Request;
use App\CustomerProfile;
use App\User;
use Validator;
use Sentinel;

class CustomerController extends FieldAdminAPIController
{
    public function index()
    { 
    	$profile_admin_id = $this->getFieldAdmin()->id;
    	$list = CustomerProfile::where('profile_admin_id', $profile_admin_id)->get()->sortByDesc('created_at');

    	return $this->apiResponse([
            'data' => $this->reformCollection($list)
        ]);
    }

    public function show($id)
    {
    	$profile_admin_id = $this->getFieldAdmin()->id;
    	$customer = CustomerProfile::where('id', $id)->first();

    	if (!$customer) {
            return $this->respondNotFound();
        } /*else if ($customer->profile_admin_id != $profile_admin_id) {
            return $this->respondForbidden("Sorry, access denied");
        }*/

        return $this->apiResponse([
            'data' => $this->reform($customer)
        ]);
    }

    public function store( Request $request )
    {        
        $rules = [
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'name' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $user_data = [
			    'phone' => $request->phone,
			    'name' => $request->name,
			    'email' => $request->email,
			    'password' => $request->password,
			    'sms' => 'আপনার রেজিস্ট্রেশন সফল হয়েছে, 
আপনার লগইন ইউজারনেম
ফোন নং: '.$request->phone.'
পাসওয়ার্ড: '.$request->password.'
টিম 
হেভিগাড়ী টেকনোলজিস লিমিটেড'
			];

			$user = Sentinel::register($user_data);
			$role = Sentinel::findRoleBySlug('customer');
	    	$role->users()->attach($user);

			$user->status = 'active';
			$user->save();
			
			$profile = new CustomerProfile;
			$profile->user_id = $user->id;
			$profile->profile_admin_id = $this->getFieldAdmin()->id;
			$profile->profile_complete = 'yes';
			$profile->token = uniqid();
			$profile->save();

            $profile->completeProfile($request);

	        $activation = Activation::create($user);
	        $activation->code = mt_rand(1111, 9999);
	        $activation->completed = 1;
	        $activation->completed_at = date('Y-m-d H:i:s');
	        $activation->save();

	        $to = $user->phone;
	        $ref_id = "$to"."_".time();
	        $status = SMSFacade::send($to, $user->sms, $ref_id);
	        
	        $user->sms = NULL;
	        $user->save();

	        return $this->apiResponse([
	            'data' => [
	                'message' => 'Registration successful',
	                'customer_id' => $profile->id
	            ]
	        ]);
        } catch (\Exception $e) {
            return $this->respondServerError();
        }

    }
}