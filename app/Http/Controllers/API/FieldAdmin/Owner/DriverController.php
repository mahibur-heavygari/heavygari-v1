<?php

namespace App\Http\Controllers\API\FieldAdmin\Owner;

use App\Http\Controllers\API\FieldAdmin\FieldAdminAPIController;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use App\OwnerProfile;
use App\DriverProfile;
use App\User;
use Validator;
use Sentinel;

class DriverController extends FieldAdminAPIController
{
	public function index( $owner_phone )
    {     	
    	$profile_admin_id = $this->getFieldAdmin()->id;

        $user = User::where('phone', $owner_phone)->first();

        if (!$user) {
            return $this->respondNotFound("Owner not found");
        }

        if (!$user->ownerProfile) {
            return $this->respondNotFound("Owner not found");
        }

        $owner = $user->ownerProfile;

    	/*if ($owner->profile_admin_id != $profile_admin_id) {
            return $this->respondForbidden("Sorry, access denied");
        }*/

        $drivers = $owner->myDrivers;
        return $this->apiResponse([
            'data' => $this->reformCollection($drivers)
        ]);
    }

    public function show( $owner_id, $driver_id )
    {     	  	
    	$profile_admin_id = $this->getFieldAdmin()->id;
    	$owner = OwnerProfile::where('id', $owner_id)->first();

    	if (!$owner) {
            return $this->respondNotFound('Owner not found');
        } /*else if ($owner->profile_admin_id != $profile_admin_id) {
            return $this->respondForbidden("Sorry, access denied");
        }*/

        $driver = DriverProfile::where('id', $driver_id)->where('vehicle_owner_profile_id', $owner_id)->first();
        if (!$driver) {
            return $this->respondNotFound('Driver not found');
        }
        
        return $this->apiResponse([
            'data' => $this->reform($driver)
        ]);
    }

	public function store( Request $request, $owner_phone )
    {   
        $user = User::where('phone', $owner_phone)->first();

    	if (!$user) {
            return $this->respondNotFound("Owner not found");
        }

        if (!$user->ownerProfile) {
            return $this->respondNotFound("Owner not found");
        }

        $owner = $user->ownerProfile;

        $rules = [
            'name' => 'required',
            'phone' => 'required|unique:users',
            'password' => 'required',
            'national_id' => 'required',
            'license_no' => 'required',
            'license_date_of_issue' => 'required',
            'license_date_of_expire' => 'required',
            'license_issuing_authority' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $user_data = [
			    'phone' => $request->phone,
			    'name' => $request->name,
			    'password' => $request->password
			];

			$user = Sentinel::register($user_data);
			$role = Sentinel::findRoleBySlug('driver');
	    	$role->users()->attach($user);

			$user->status = 'active';
            $user->national_id = $request->national_id;
            $user->address = $request->address;
			$user->save();
			
			$profile = new DriverProfile;
            $profile->profile_admin_id = $this->getFieldAdmin()->id;
			$profile->user_id = $user->id;
			$profile->vehicle_owner_profile_id = $owner->id;
			$profile->profile_complete = 'yes';
			$profile->license_no = $request->license_no;
			$profile->license_date_of_issue = $request->license_date_of_issue;
			$profile->license_date_of_expire = $request->license_date_of_expire;
			$profile->license_issuing_authority = $request->license_issuing_authority;
			$profile->token = uniqid();
			$profile->save();

            $profile->user->uploadProfilePicture($request);
            $profile->user->uploadNationalId($request);
            $profile->uploadLicense($request);

			# insert default preferences
			$profile->bookingTypePreference()->create([
			    'on_demand' => true,
			    'advance' => true,
			]);
			
			$profile->bookingCategoryPreference()->create([
			    'full' => true,
			    'shared' => true,
			]);

			$profile->bookingDistancePreference()->create([
			    'short_distance' => true,
			    'long_distance' => true,
			]);

	        $activation = Activation::create($user);
	        $activation->code = mt_rand(1111, 9999);
	        $activation->completed = 1;
	        $activation->completed_at = date('Y-m-d H:i:s');
	        $activation->save();

	        return $this->apiResponse([
	            'data' => [
	                'message' => 'Registration successful',
	                'driver_id' => $profile->id
	            ]
	        ]);
        } catch (\Exception $e) {
            dd($e);
            return $this->respondServerError();
        }
    }
}