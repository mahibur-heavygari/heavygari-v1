<?php

namespace App\Http\Controllers\API\FieldAdmin;

use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use App\AdminProfile;
use Auth;

class FieldAdminAPIController extends ApiController
{
    /**
     * Get current logged in field admin profile
     */
    public function getFieldAdmin()
    {
        $current_user = Auth::guard('api')->user();
        $admin = AdminProfile::where('user_id', $current_user->id)->first();
        return $admin ? $admin : false;
    }
}
