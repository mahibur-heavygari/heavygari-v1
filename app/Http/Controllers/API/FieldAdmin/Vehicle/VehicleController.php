<?php

namespace App\Http\Controllers\API\FieldAdmin\Vehicle;

use App\Http\Controllers\API\FieldAdmin\FieldAdminAPIController;
use Illuminate\Http\Request;
use App\OwnerProfile;
use App\VehicleType;
use App\Vehicle;
use App\User;
use Validator;
use Auth;

class VehicleController extends FieldAdminAPIController
{
    /**
     * Get current logged in field admin profile
     */

    public function history()
    {
        $vehicle_types = VehicleType::get();

        return $this->apiResponse([
            'data' => $this->reformCollection($vehicle_types)
        ]);
    }

    public function index( $owner_id )
    {
        $profile_admin_id = $this->getFieldAdmin()->id;
        $owner = OwnerProfile::where('id', $owner_id)->first();

        if (!$owner) {
            return $this->respondNotFound('Owner not found');
        } /*else if ($owner->profile_admin_id != $profile_admin_id) {
            return $this->respondForbidden("Sorry, access denied");
        }*/

        $vehicles = $owner->myVehicles;

        return $this->apiResponse([
            'data' => $this->reformCollection($vehicles)
        ]);
    }

    public function show( $owner_id, $vehicle_id  )
    {
        $profile_admin_id = $this->getFieldAdmin()->id;
        $owner = OwnerProfile::where('id', $owner_id)->first();

        if (!$owner) {
            return $this->respondNotFound('Owner not found');
        } /*else if ($owner->profile_admin_id != $profile_admin_id) {
            return $this->respondForbidden("Sorry, access denied");
        }*/

        $vehicle = Vehicle::where('id', $vehicle_id)->first();
        
        if (!$vehicle) {
            return $this->respondNotFound('Vehicle not found');
        }
        
        return $this->apiResponse([
            'data' => $this->reform($vehicle)
        ]);
    }

    public function store( Request $request, $owner_phone )
    {
        $rules = [
            'name' => 'required',
            'about' => '',
            'vehicle_type_id' => 'required',
            'vehicle_registration_number' => 'required',
            'fitness_number' => '',
            'fitness_expiry' => 'required',
            'number_plate' => 'required',
            'tax_token_number' => '',
            'tax_token_expirey' => 'required',
            'chesis_no' => '',
            'engine_capacity_cc' => '',
            'ride_sharing_certificate_no' => '',
            'insurance_no' => '',
            'height' => '',
            'length' => '',
            'width' => ''
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }
        $request->request->add(['registered_by' => $this->getFieldAdmin()->user->name,'profile_admin_id' => $this->getFieldAdmin()->id]);


        $user = User::where('phone', $owner_phone)->first();

        if (!$user) {
            return $this->respondNotFound("Owner not found");
        }
        if (!$user->ownerProfile) {
            return $this->respondNotFound("Owner not found");
        }

        $owner = $user->ownerProfile;
        try{
            $vehicle = Vehicle::addRepo($owner, $request);
        }catch (\Exception $e) {
            return $this->respondServerError();
        }

        return $this->apiResponse([
            'data' => $this->reform($vehicle)
        ]);
    }
}
