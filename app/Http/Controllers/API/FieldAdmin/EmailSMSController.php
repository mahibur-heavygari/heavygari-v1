<?php

namespace App\Http\Controllers\API\FieldAdmin;

use Illuminate\Http\Request;
use App\MyLibrary\SMSLib\SMSFacade;
use Validator;
use App\Events\FromAdmin\SendPushOtherEmail;

class EmailSMSController extends FieldAdminAPIController
{
    /**
     * Get estimated fare for a full booking
     */
    public function sendSMS(Request $request)
    {
        $rules = [
            'phone' => 'required',
            'text' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $to = $request->phone;
        $text = $request->text;
        $ref_id = "$to"."_".time();

        $status = SMSFacade::send($to, $text, $ref_id, 'others');

        if(!$status){
            return $this->respondServerError('Sorry! Could not send sms.');
        }

        return $this->apiResponse([
            'data' => [
                'message'   => 'SMS sent successfully'
            ]
        ]);
    }

    public function sendEmail(Request $request)
    {
        $rules = [
            'email' => 'required',
            'subject' => 'required',
            'text' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $email = $request->email;
        $subject = $request->subject;
        $text = $request->text;

        event(new SendPushOtherEmail($email, $text, $image=false, $subject));
        
        return $this->apiResponse([
            'data' => [
                'message'   => 'Email sent successfully'
            ]
        ]);
    }
}
