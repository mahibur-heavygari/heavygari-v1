<?php

namespace App\Http\Controllers\API\FieldAdmin;

use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use App\OwnerProfile;
use App\DriverProfile;
use App\CustomerProfile;
use App\Vehicle;
use App\AdminProfile;
use App\User;
use Auth;

class HistoryController extends ApiController
{
    /**
     * Get all history of field admin
     */
    public function history()
    {   
        $items = array();
        $current_user = Auth::guard('api')->user();
        $admin = AdminProfile::where('user_id', $current_user->id)->first();
        $owners = OwnerProfile::where('profile_admin_id', $admin->id)->get();
        $owner_updated = [];

        foreach ($owners as $key => $value) {
            $owner_updated[$key]['phone'] = $value->user->phone;
            $owner_updated[$key]['name'] = $value->user->name;
            $owner_updated[$key]['created_at'] = $value->user->created_at;
            $owner_updated[$key]['field_admin_vehicles'] = count($value->myVehicles->where('profile_admin_id', $admin->id));
            $owner_updated[$key]['field_admin_vehicles_total'] = $value->myVehicles->where('profile_admin_id', $admin->id);
            $owner_updated[$key]['field_admin_drivers'] = count($value->myDrivers->where('profile_admin_id', $admin->id));
            $owner_updated[$key]['field_admin_drivers_total'] = $this->getDrivers($value->id,$admin->id);
        }       
        $customers = CustomerProfile::where('profile_admin_id', $admin->id)->get();
       
        return $this->apiResponse([
            'data' => array(
                'owners' => $owner_updated,
                'customers' => $this->reformCollection($customers)
            )
        ]);
    }

    public function getDrivers($owner_id ,$field_admin_id ){
        $drivers = DriverProfile::where('profile_admin_id',$field_admin_id)->where('vehicle_owner_profile_id',$owner_id)->get();
        return $drivers;
    }
}
