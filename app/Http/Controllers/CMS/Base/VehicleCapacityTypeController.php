<?php

namespace App\Http\Controllers\CMS\Base;

use App\Http\Controllers\CMS\CmsPanelController;

use Illuminate\Http\Request;

use App\VehicleCapacityType;

class VehicleCapacityTypeController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $list = VehicleCapacityType::all();

        return view('CMS.vehicle_capacity_types.list')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('CMS.vehicle_capacity_types.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        
        VehicleCapacityType::create($request->all());

        return redirect('/cms/base/capacity_types');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $vehicle_capacity_type = VehicleCapacityType::find($id);
        return view('CMS.vehicle_capacity_types.edit')->with('item', $vehicle_capacity_type);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $vehicle_capacity_type = VehicleCapacityType::find($id);
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);

        $vehicle_capacity_type->update($request->all());

        return redirect('/cms/base/capacity_types');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(VehicleCapacityType $vehicle_capacity_type)
    {
        $vehicle_capacity_type->delete($request);
    }

    public function delete($id)
    {
        $vehicle_capacity_type = VehicleCapacityType::where('id', $id);
        if(!$vehicle_capacity_type->exists())
            abort(404);

        $vehicle_capacity_type->delete();

        $notification = ['success','Vehicle capacity type successfully deleted'];
        session()->flash('message', $notification);
        
        return redirect('/cms/base/capacity_types');
    }
}
