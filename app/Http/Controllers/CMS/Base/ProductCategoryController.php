<?php

namespace App\Http\Controllers\CMS\Base;

use App\BaseProductCategory;
use App\Http\Controllers\CMS\CmsPanelController;
use Illuminate\Http\Request;

class ProductCategoryController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $base_product_categories = BaseProductCategory::all();

        return view('CMS.product_categories.list', compact('base_product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('CMS.product_categories.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'category' => 'required|max:255'
        ]);
        //dd($request->category);
        BaseProductCategory::Create(['title' => $request->title, 'category' => $request->category]);

        return redirect('/cms/base/product_categories');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(BaseProductCategory $product_category)
    {
        return view('CMS.product_categories.edit', compact('product_category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, BaseProductCategory $product_category)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'category' => 'required|max:255'
        ]);

        $product_category->update(['title' => $request->title, 'category' => $request->category]);

        return redirect('/cms/base/product_categories');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BaseProductCategory $product_category)
    {
        $product_category->delete();

        return redirect('/cms/base/product_categories');
    }
}
