<?php

namespace App\Http\Controllers\CMS\Base;

use App\BaseVolumetricCategory;
use App\Http\Controllers\CMS\CmsPanelController;
use Illuminate\Http\Request;

class VolumetricWeightCategoryController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $volumetric_categories = BaseVolumetricCategory::all();

        return view('CMS.volumetric_weight_categories.list', compact('volumetric_categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('CMS.volumetric_weight_categories.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'height' => 'numeric|min:0',
            'width' => 'numeric|min:0',
            'length' => 'numeric|min:0'
        ]);

        $volumetric_weight = $this->getVolumetricWeight($request);

        $volumetric_weight_category = BaseVolumetricCategory::firstOrCreate(['title'=>$request->title, 'height'=>$request->height, 'width'=>$request->width, 'length'=>$request->length, 'volumetric_weight'=>$volumetric_weight]);
        

        return redirect('/cms/base/volumetric_weight_categories');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(BaseVolumetricCategory $volumetric_weight_category)
    {
        return view('CMS.volumetric_weight_categories.edit', compact('volumetric_weight_category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, BaseVolumetricCategory $volumetric_weight_category)
    {
        $this->validate($request, [
            'title' => 'required',
            'height' => 'numeric|min:0',
            'width' => 'numeric|min:0',
            'length' => 'numeric|min:0'
        ]);

        $volumetric_weight = $this->getVolumetricWeight($request);

        $volumetric_weight_category->update(['title'=>$request->title, 'height'=>$request->height, 'width'=>$request->width, 'length'=>$request->length, 'volumetric_weight'=>$volumetric_weight]);
        
        return redirect('/cms/base/volumetric_weight_categories');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BaseVolumetricCategory $volumetric_weight_category)
    {
        $volumetric_weight_category->delete();

        $notification = ['success','Volumetric category successfully deleted'];
        session()->flash('message', $notification);

        return redirect('/cms/base/volumetric_weight_categories');
    }

    private function getVolumetricWeight($request)
    {
        $volumetric_weight = ($request->height* $request->width * $request->length) / 305;
        
        return $volumetric_weight;
    }
}
