<?php

namespace App\Http\Controllers\CMS\Base;

use App\Http\Controllers\CMS\CmsPanelController;

use Illuminate\Http\Request;

use App\Point;

class PointController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $list = Point::all();

        return view('CMS.points.list')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('CMS.points.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'fullname' => 'required|max:255',
            'lat' => '',
            'lon' => ''
        ]);
        
        Point::create($request->all());

        return redirect('/cms/base/points');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Point $point)
    {
        return view('CMS.points.edit')->with('item', $point);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Point $point)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'fullname' => 'required|max:255',
            'lat' => '',
            'lon' => ''
        ]);

        $point->update($request->all());

        return redirect('/cms/base/points');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Point $point)
    {
        $point->delete($request);
    }

    public function delete($id)
    {
        $point = Point::where('id', $id);
        if(!$point->exists())
            abort(404);

        $point->delete();

        $notification = ['success','Point successfully deleted'];
        session()->flash('message', $notification);
        
        return redirect('/cms/base/points');
    }
}
