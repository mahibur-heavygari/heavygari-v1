<?php

namespace App\Http\Controllers\CMS\Base;

use App\BaseWeightCategory;
use App\Http\Controllers\CMS\CmsPanelController;
use Illuminate\Http\Request;

class WeightCategoryController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $base_weight_categories = BaseWeightCategory::all();

        return view('CMS.weight_categories.list', compact('base_weight_categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('CMS.weight_categories.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'weight' => 'required|max:255'
        ]);

        BaseWeightCategory::firstOrCreate(['title'=>$request->title, 'weight'=>$request->weight]);

        return redirect('/cms/base/weight_categories');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(BaseWeightCategory $weight_category)
    {
        return view('CMS.weight_categories.edit', compact('weight_category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, BaseWeightCategory $weight_category)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'weight' => 'required|max:255'
        ]);

        $weight_category->update(['title'=>$request->title, 'weight'=>$request->weight]);

        return redirect('/cms/base/weight_categories');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BaseWeightCategory $weight_category)
    {
        $weight_category->delete();

        $notification = ['success','Weight category successfully deleted'];
        session()->flash('message', $notification);

        return redirect('/cms/base/weight_categories');
    }
}
