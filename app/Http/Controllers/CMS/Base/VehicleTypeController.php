<?php

namespace App\Http\Controllers\CMS\Base;

use App\Http\Controllers\CMS\CmsPanelController;

use Illuminate\Http\Request;

use App\VehicleCapacityType;

use App\VehicleType;

class VehicleTypeController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $list = VehicleType::all();

        return view('CMS.vehicle_types.list')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $capacities = VehicleCapacityType::all();

        return view('CMS.vehicle_types.add')->With('capacities', $capacities);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'title_bn' => 'required|max:255',
            'capacity_type_id' => 'required',
            'capacity' => 'required',
            'max_capacity' => 'required'
        ]);
        
        $vehicle_type = VehicleType::create($request->all());

        $vehicle_type->uploadIcon($request);

        return redirect('/cms/base/vehicle_types');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(VehicleType $vehicle_type)
    {
        $capacities = VehicleCapacityType::all();

        return view('CMS.vehicle_types.edit')->with(['item' => $vehicle_type, 'capacities' => $capacities]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, VehicleType $vehicle_type)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'title_bn' => 'required|max:255',
            'capacity_type_id' => 'required',
            'capacity' => 'required',
            'max_capacity' => 'required'
        ]);

        $vehicle_type->update($request->all());

        $vehicle_type->uploadIcon($request);

        return redirect('/cms/base/vehicle_types');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(VehicleType $vehicle_type)
    {
        $vehicle_type->delete($request);
    }

    public function delete($id)
    {
        $vehicle_type = VehicleType::where('id', $id);
        if(!$vehicle_type->exists())
            abort(404);

        $vehicle_type->delete();

        $notification = ['success','Vehicle type successfully deleted'];
        session()->flash('message', $notification);
        
        return redirect('/cms/base/vehicle_types');
    }
}
