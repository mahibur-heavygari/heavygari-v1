<?php

namespace App\Http\Controllers\CMS\Base;

use App\Http\Controllers\CMS\CmsPanelController;

use Illuminate\Http\Request;
use App\Point;
use App\VehicleType;
use App\TripCategory;
use App\RouteVehicleWiseCharge;

class TripCategoryController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $list = TripCategory::all();

        return view('CMS.trip_categories.list')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $points = Point::all();

        $types = ['up', 'down'];

        return view('CMS.trip_categories.add')->with('points', $points)->with('types', $types);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'src_point_id' => 'required',
            'dest_point_id' => 'required',
            'trip_category' => 'required'
        ]);
        
        TripCategory::create($request->all());

        return redirect('/cms/base/trip_categories');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TripCategory $trip_category)
    {
        $points = Point::all();

        $types = ['up', 'down'];

        return view('CMS.trip_categories.edit')->with(['item' => $trip_category, 'points' => $points, 'types' => $types]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, TripCategory $trip_category)
    {
        $this->validate($request, [
            'src_point_id' => 'required',
            'dest_point_id' => 'required',
            'trip_category' => 'required'
        ]);

        $trip_category->update($request->all());

        return redirect('/cms/base/trip_categories');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TripCategory $trip_category)
    {
        $trip_category->delete($request);
    }

    public function delete($id)
    {
        $trip_category = TripCategory::where('id', $id);
        if(!$trip_category->exists())
            abort(404);

        $trip_category->delete();

        $notification = ['success','Trip category successfully deleted'];
        session()->flash('message', $notification);
        
        return redirect('/cms/base/trip_categories');
    }

    public function showVehicles( $id ){
        $trip_category = TripCategory::find($id);

        if(!$trip_category)
            abort(404);

        $vehicle_types = VehicleType::leftJoin('route_vehicle_wise_charge', function($join) use ($id) {
              $join->on('base_vehicle_types.id', '=', 'route_vehicle_wise_charge.vehicle_type_id')
                ->where('route_vehicle_wise_charge.trip_category_id','=', $id);
            })
            ->where('base_vehicle_types.title', 'Bus (AC)')
            ->orWhere('base_vehicle_types.title', 'Bus (Non AC)')
            ->orWhere('base_vehicle_types.title', 'Bus (Business Class)')
            ->orWhere('base_vehicle_types.title', 'Truck  (18feet)')
            ->orWhere('base_vehicle_types.title', 'Cover Truck  (22feet)')
            ->orWhere('base_vehicle_types.title', 'Trailer Truck 2XL  (20feet)')
            ->orWhere('base_vehicle_types.title', 'Trailer Truck 3XL  (40feet)')
            ->orWhere('base_vehicle_types.title', 'Mini Pickup Van  (7feet)')
            ->orWhere('base_vehicle_types.title', 'Pickup Van  (12feet)')
            ->orderBy('base_vehicle_types.preference')
            ->get([
                'base_vehicle_types.id as id',
                'base_vehicle_types.title as title',
                'route_vehicle_wise_charge.trip_category_id as trip_category_id',
                'route_vehicle_wise_charge.multiplier as multiplier'
            ]);
        $route_vehicle_wise_charge = RouteVehicleWiseCharge::where('trip_category_id', $id)->first();
        $distance_type = isset($route_vehicle_wise_charge->distance_type) ? $route_vehicle_wise_charge->distance_type : NULL;

        return view('CMS.trip_categories.vehicle_wise_charge')->with('trip_category', $trip_category)->with('vehicle_types', $vehicle_types)->with('distance_type', $distance_type);
    }

    public function saveCharges(Request $request){
        $trip_category_id = $request->trip_category_id;
        $distance_type = $request->distance_type;

        $vehicle_types = $request->vehicle_types;
        foreach ($vehicle_types as $key => $vehicle_type) {
            $vehicle_type_id = $vehicle_type;
            $multiplier = $request->multiplier[$key];
            if(is_null($multiplier)){
                RouteVehicleWiseCharge::where('trip_category_id', $trip_category_id)->where('vehicle_type_id', $vehicle_type_id)->delete();
                continue;
            }
            $existance = RouteVehicleWiseCharge::where('trip_category_id', $trip_category_id)->where('vehicle_type_id', $vehicle_type_id)->first();
            if($existance){
                RouteVehicleWiseCharge::where('id', $existance->id)->update(['multiplier'=>$multiplier, 'distance_type'=>$distance_type]);
                continue;
            }
            RouteVehicleWiseCharge::create(['trip_category_id'=>$trip_category_id, 'vehicle_type_id'=>$vehicle_type_id, 'multiplier'=>$multiplier, 'distance_type'=>$distance_type]);           
        }
        
        $notification = ['success', 'Multiplier updated successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/base/trip_categories/'.$trip_category_id.'/vehicle_wise_charge');
    }
}
