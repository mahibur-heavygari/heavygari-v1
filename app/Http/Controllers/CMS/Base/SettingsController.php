<?php

namespace App\Http\Controllers\CMS\Base;

use App\Http\Controllers\CMS\CmsPanelController;
use Illuminate\Http\Request;
use App\Settings;

class SettingsController extends CmsPanelController
{
    public function showSettings()
    {
    	$settings = Settings::where('key', 'driver_comission')->first();
    	$driver_comission = $settings->value;
    	return view('CMS.settings.general')->with('driver_comission', $driver_comission);
    }

    public function saveDriverComission( Request $request )
    {
    	$this->validate($request, [
            'driver_comission' => 'required',
        ]);
        
    	$driver_comission = number_format($request->driver_comission, 2);
        Settings::where('key', 'driver_comission')->update(['value'=>$driver_comission]);

        $notification = ['success','Driver commission rate has been updated.'];
        session()->flash('message', $notification);
        return redirect('/cms/base/settings');
    }
}
