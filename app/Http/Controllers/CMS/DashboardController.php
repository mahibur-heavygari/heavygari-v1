<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\MyLibrary\DashboardLib\AdminDashboard;
use App\OwnerProfile;
use App\DriverProfile;
use App\CustomerProfile;
use App\Vehicle;
use App\VehicleType;
use App\Booking;
use App\UsersComplain;
use App\Trip;
use Sentinel;
use DB;

class DashboardController extends CmsPanelController
{
	/**
	 * show dashboard page
	 */
    public function index()
    {
    	$dashboard = new AdminDashboard();
        $trips = new Trip();
        $booking_stats = $dashboard->bookingStats();
        $chart_booking_stats = $dashboard->chartBookingStats();
    	$payment_stats = $dashboard->paymentStats();
        $vehicle_stats = $dashboard->vehicleStats();
        $trip_destination = $dashboard->tripDestination();
        $vehicle_type = $dashboard->vehicleType();
        $particulars = $dashboard->productType();
        $vehicles = $vehicle_stats;
        $vehicles_number = [];
        $titles = $vehicles->keys()->all(); // for chart
        $counts = $vehicles->values()->all(); // for chart
        $total_vehicles = Vehicle::all()->count();

        $vehicle_ids = VehicleType::get()->pluck('id')->toArray();   
        $vehicle_types = DB::table('vehicles')->whereIn('vehicle_type_id', $vehicle_ids)->select('vehicle_type_id', DB::raw('count(*) as number'))->groupBy('vehicle_type_id')->get();
        $vehicle_types = $vehicle_types->each(function ($item, $key) {
               $vehicle_type_id = VehicleType::where('id', $item->vehicle_type_id)->first();
               $item->vehicle_title = $vehicle_type_id->title;
        });
        $vehicle_stats = [];
        $total_vehicle = 0;

        foreach ($vehicle_types as $key => $value) {
            $vehicle_stats['title'][] = $value->vehicle_title;
            $vehicle_stats['number'][] = $value->number;
            $total_vehicle += $value->number;
        }

        return view('CMS.dashboard')->with([
            'booking_stats' => $booking_stats,
            'chart_booking_stats' => $chart_booking_stats,
            'payment_stats' => $payment_stats,
            'vehicles' => $vehicles,
            'vehicle_titles' => $titles,
            'vehicle_counts' => $counts,
            'total_vehicles' => $total_vehicles,
            'trip_destination' => $trip_destination,
            'vehicle_type' => $vehicle_type,
            'vehicle_stats' => $vehicle_stats,
            'total_vehicle' => $total_vehicle,
            'particulars' => $particulars
        ]);
            
    }

    public function searchByRefNo(Request $request){
        $this->validate($request, [
            'ref_no' => 'required'
        ]);

        $owner = OwnerProfile::join('users', 'users.id', '=', 'profile_vehicle_owners.user_id')->where('profile_vehicle_owners.ref_no', $request->ref_no)->orWhere('users.phone', $request->ref_no)->select('profile_vehicle_owners.*')->first();     
        $driver = DriverProfile::join('users', 'users.id', '=', 'profile_drivers.user_id')->where('profile_drivers.ref_no', $request->ref_no)->orWhere('users.phone', $request->ref_no)->select('profile_drivers.*')->first();
        $vehicle = Vehicle::where('ref_no', $request->ref_no)->first();
        $complain = UsersComplain::where('unique_id', $request->ref_no)->first();
        $customer = CustomerProfile::join('users', 'users.id', '=', 'profile_customers.user_id')->where('users.phone', $request->ref_no)->select('profile_customers.*')->first();
        $booking = Booking::where('unique_id', $request->ref_no)->first();

        if( $owner )
            $found = 'owner';
        elseif( $driver )
            $found = 'driver';
        elseif( $vehicle )
            $found = 'vehicle';
        elseif( $complain )
            $found = 'complain';
        elseif( $customer )
            $found = 'customer';
        elseif( $booking )
            $found = 'booking';
        else
            $found = false;

        switch ($found) {
            case "owner":
                return redirect( '/cms/users/owners/'.$owner->id );
                break;
            case "driver":
                return redirect( '/cms/users/drivers/'.$driver->id );
                break;
            case "vehicle":
                return redirect( '/cms/vehicles/'.$vehicle->id );
                break;
            case "complain":
                return redirect( '/cms/complain/edit/'.$complain->id );
                break;
            case "customer":
                return redirect( '/cms/users/customers/'.$customer->id );
                break;
            case "booking":
                return redirect( '/cms/bookings/'.$booking->unique_id );
                break;
            default:
                return abort(404);
        }
    }
}
