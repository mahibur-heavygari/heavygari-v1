<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\OwnerProfile;
use App\DriverProfile;
use App\DriverCommission;
use App\DriverCommissionItem;
use App\MyLibrary\DriverCommission\CommissionSystem;
use App\MyLibrary\SMSLib\SMSFacade;

class DriverPaymentController extends CmsPanelController
{
    public function index( Request $request )
    {
        $list = DriverCommission::orderBy('id', 'desc')->get();
    	return view('CMS.driver_payments.list')->with('list', $list);
    }

    public function commissions( $id ){
        $commission_items = CommissionSystem::getCommissionItems($id);

        $driver_commission = DriverCommission::find($id);

        return view('CMS.driver_payments.show')->with('commission_items', $commission_items)->with('driver_commission', $driver_commission);
    }

    public function makePayment(Request $request)
    {
        $this->validate($request, [
            'paid_at' => 'required',
            'payment_method' => 'required',
            'amount' => 'required'
        ]);
        $paymentRes = CommissionSystem::makePayment($request);
        $sms_text = $paymentRes['sms_text'];
        $to = $paymentRes['driver_number'];
        $ref_id = "$to"."_driver_payment_".time();
        $status = SMSFacade::send($to, $sms_text, $ref_id);

        $notification = $paymentRes['notification'];
        session()->flash('message', $notification);
        return redirect('/cms/payment/drivers/'.$request->driver_commission_id.'/commissions');
    }

}
