<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\MyLibrary\TransactionLib\AdminTransaction;
use App\MyLibrary\TransactionLib\OwnerTransaction;
use App\MyLibrary\TransactionLib\CustomerTransaction;
use App\MyLibrary\TransactionLib\DriverTransaction;
use App\CustomerProfile;
use App\OwnerProfile;
use App\DriverProfile;
use Carbon\Carbon;

class TransactionController extends CmsPanelController
{
    public function allTransactions( Request $request )
    {
    	$transaction = new AdminTransaction();
    	$transactions = $transaction->getTransactions( $request );

    	return view('CMS.transactions.all')->with('bookings', $transactions['bookings'])->with('total_fare', $transactions['total_fare'])->with('total_customer_cost', $transactions['total_customer_cost'])->with('admin_commission_total', $transactions['admin_commission_total'])->with('owner_earning_total', $transactions['owner_earning_total'])->with('driver_commission_total', $transactions['driver_commission_total']);
    }

    public function customerTransactions($id, Request $request)
    {
        $customer = CustomerProfile::find($id);
        $transaction = new CustomerTransaction($customer);
        $transactions = $transaction->getTransactions( $request );

        return view('CMS.transactions.customer')->with('total_customer_cost', $transactions['total_customer_cost'])->with('bookings', $transactions['bookings'])->with('total_fare', $transactions['total_fare'])->with('admin_commission_total', $transactions['admin_commission_total'])->with('owner_earning_total', $transactions['owner_earning_total'])->with('driver_commission_total', $transactions['driver_commission_total'])->with('name', $customer->user->name)->with('customer_id', $customer->id);
    }

    public function ownerTransactions($id, Request $request)
    {
    	$owner = OwnerProfile::find($id);
        $transaction = new OwnerTransaction($owner);
        $transactions = $transaction->getTransactions( $request );

        return view('CMS.transactions.owner')->with('total_customer_cost', $transactions['total_customer_cost'])->with('bookings', $transactions['bookings'])->with('total_fare', $transactions['total_fare'])->with('admin_commission_total', $transactions['admin_commission_total'])->with('owner_earning_total', $transactions['owner_earning_total'])->with('driver_commission_total', $transactions['driver_commission_total'])->with('name', $owner->user->name)->with('owner_id', $owner->id);
    }
    
    public function driverTransactions($id, Request $request)
    {
        $driver = DriverProfile::find($id);
        $transaction = new DriverTransaction($driver);
        $transactions = $transaction->getTransactions( $request );

        return view('CMS.transactions.driver')->with('total_customer_cost', $transactions['total_customer_cost'])->with('bookings', $transactions['bookings'])->with('total_fare', $transactions['total_fare'])->with('admin_commission_total', $transactions['admin_commission_total'])->with('owner_earning_total', $transactions['owner_earning_total'])->with('driver_commission_total', $transactions['driver_commission_total'])->with('name', $driver->user->name)->with('driver_id', $driver->id);
    }
}
