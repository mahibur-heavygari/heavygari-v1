<?php

namespace App\Http\Controllers\CMS;
use Illuminate\Http\Request;
use App\CustomerProfile;
use App\OwnerProfile;
use App\DriverProfile;
use App\UsersComplain;
use App\User;
use App\MyLibrary\SMSLib\SMSFacade;
use App\SmsHistory;
use Carbon\Carbon;

class ComplainController extends CmsPanelController
{
    /**
     * create the specified resource.
     */
    public function getComplain()
    {   $filter = 'active';
        $customers = CustomerProfile::$filter()->get();
        $owners = OwnerProfile::$filter()->get();
        $owner_managers = OwnerProfile::$filter()->where('manager_email', '!=', NULL)->get();
        $drivers = DriverProfile::$filter()->get();

        foreach ($customers as $key => $value) {
            $value->email = $value->user->email;
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($owners as $key => $value) {
            $value->email = $value->user->email;
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($drivers as $key => $value) {
            $value->email = $value->user->email;
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        return view('CMS.complains.create')->with(['customers' => $customers, 
            'owners' => $owners, 
            'drivers' => $drivers
        ]);
    }
    /**
     * save the specified resource.
     */
    public function saveComplain(Request $request)
    {   
        $status = UsersComplain::create(['note' => $request->text, 'user_type' => $request->user_type, 'phone' => $request->phone,'name' => $request->user_name, 'android' => $request->android, 'ios' => $request->ios, 'web' => $request->web,'others' => $request->others,'network' => $request->network]);
        $user_complain = UsersComplain::where('note',$request->text)->first();
        $unique_id = 'HGCOM-'.$user_complain->id;
        $user_complain->unique_id = $unique_id;
        $user_complain->save();
        $to = $request->phone;
        $msg = 'প্রিয় গ্রাহক, আপনার দেওয়া অভিযোগ ('. $unique_id .') সফল ভাবে গ্রহণ করা হয়েছে, আমরা দ্রুত সমাধান করার চেষ্টা করছি। হেভিগাড়ীর সাথে থাকার জন্য ধন্যবাদ।';

        $ref_id = "$to"."_".time();
        
        SMSFacade::send($to, $msg, $ref_id, $request->user_type);
        
        if ($status) {
            $notification = ['success','Complain created successfully'];
            session()->flash('message', $notification);
            SmsHistory::Create(['sms_text' => $msg, 'to' => $to, 'response' => $ref_id, 'single_bulk' => 'single', 'user_type' => $request->user_type ]);
        } else {
            $notification = ['danger','Sorry, Could not create'];
            session()->flash('message', $notification);
        }
        $filter = 'ongoing';
        $list = UsersComplain::all()->where('status', $filter)->sortByDesc('created_at');
        return view('CMS.complains.history')->with('filter', $filter)->with('lists', $list);
    }

    /**
     * Show the form for showing all the resource.
     */
    public function historyComplain(Request $request)
    {
        
        $filter = $request->input('filter') ? $request->input('filter') : 'ongoing';
        $list = UsersComplain::all()->where('status', $filter)->sortByDesc('created_at');
        $present_time_str = (string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka');
        return view('CMS.complains.history')->with('lists', $list)->with('filter', $filter)->with('present_time_str', $present_time_str);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($complain_id)
    {   
        $editable = UsersComplain::where('id',$complain_id)->get();

        return view('CMS.complains.edit')->with('editables', $editable);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($complain_id)
    {   
        $resolve = UsersComplain::where(['id' => $complain_id])->first();
        $to = $resolve->phone;
        $msg = "প্রিয় গ্রাহক,আপনার দেওয়া অভিযোগটি (". $resolve->unique_id ."
) আমরা সমাধান করতে পেরেছি। হেভিগাড়ীর সাথে থাকার জন্য ধন্যবাদ।";
        $ref_id = "$to"."_".time();
        $status = SMSFacade::send($to, $msg, $ref_id, $resolve->user_type);
        if ($status) {
            $resolve->status = 'resolved';
            $resolve->save();
            $notification = ['success','Resolved successfully'];
            session()->flash('message', $notification);
            SmsHistory::Create(['sms_text' => $resolve->note, 'to' => $to, 'response' => $ref_id, 'single_bulk' => 'single', 'user_type' => $resolve->user_type ]);
        } else {
            $notification = ['danger','Sorry, Could not resolve'];
            session()->flash('message', $notification);
        }
        $filter = 'resolved';
        $list = UsersComplain::all()->where('status', $filter)->sortByDesc('created_at');
        return view('CMS.complains.history')->with('filter', $filter)->with('lists', $list);
    }
}
