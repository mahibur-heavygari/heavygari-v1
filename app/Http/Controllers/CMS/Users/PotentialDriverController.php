<?php

namespace App\Http\Controllers\CMS\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\CMS\CmsPanelController;
use App\PotentialDriver;


class PotentialDriverController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = PotentialDriver::orderBy('created_at', 'desc')->get();
        return view('CMS.users.potential_drivers.list')->with(['list' => $list]);
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('CMS.users.potential_drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $Existing_email = User::where('email', $request->owner_email)->get();
        // dd($Existing_email == null);
        // $Existing_phone = User::where('phone', $request->owner_phone)->get();
        // dd($Existing_phone == null );
        // if( $Existing_phone !== null && 
        //     $Existing_email !== null ){
        //     dd($Existing_email);
        //     dd("fuhkush");
        // }
        $potential_driver = PotentialDriver::where('driver_phone','=',$request->driver_phone)->get();
        if(count($potential_driver) > 0){
            $notification = ['danger', 'This Potential Driver Already Exist.'];
            session()->flash('message', $notification);

            return redirect('/cms/users/potential_drivers/');
        }

        $parameters = $request->only('driver_name', 'driver_phone', 'driver_email', 'driver_address', 'driver_area');
        $profile = PotentialDriver::create($parameters);

        
        //flash message
        $notification = ['success', 'Profile added successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/potential_drivers/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = PotentialDriver::find($id);

        return view('CMS.users.potential_drivers.show')->with(['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = PotentialDriver::find($id);

        return view('CMS.users.potential_drivers.edit')->with(['profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        //dd($request->all());
        $potential_driver = PotentialDriver::where('driver_phone','=',$request->driver_phone)->get();
        if(count($potential_driver) > 0){
            $notification = ['danger', 'This Potential Driver Already Exist.'];
            session()->flash('message', $notification);

            return redirect('/cms/users/potential_drivers/');
        }

        $potential_driver = PotentialDriver::find($id);

        $profile = $potential_driver->updateProfile($request);

        
        //flash message
        $notification = ['success','Profile updated successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/potential_drivers/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        PotentialDriver::where('id', $id)->delete();

        //flash message
        $notification = ['success', 'Deleted successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/users/potential_drivers');
    }
}