<?php

namespace App\Http\Controllers\CMS\Users;

use App\User;
use Illuminate\Http\Request;
use App\Events\Owner\OwnerAccountVerified;
use App\Events\Driver\DriverAccountVerified;
use App\Http\Controllers\CMS\CmsPanelController;

class UserController extends CmsPanelController
{
    /**
     * activate a user
     */
    public function activateUser(User $user)
    {
        $user->status = 'active';
        $user->save();

        if ($user->ownerProfile && !$user->driverProfile) {
            $user->ownerProfile->makeMeDriver();
            event(new OwnerAccountVerified($user));
            
        } else if ($user->driverProfile) {
            event(new DriverAccountVerified($user));
        }

        //flash message
        $notification = ['success','User "'.$user->name.'" is now active.'];
        session()->flash('message', $notification);

        return redirect()->back();
    }

    /**
     * block a user
     */
    public function blockUser(User $user)
    {
        $user->status = 'blocked';
        $user->save();

        //flash message
        $notification = ['success','User "'.$user->name.'" is now blocked.'];
        session()->flash('message', $notification);

        return redirect()->back();
    }
}
