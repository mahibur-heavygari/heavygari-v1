<?php

namespace App\Http\Controllers\CMS\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\CMS\CmsPanelController;
use App\PotentialOwner;
use App\VehicleType;
use App\User;

class PotentialOwnerController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = PotentialOwner::orderBy('created_at', 'desc')->get();

        return view('CMS.users.potential_owners.list')->with(['list' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_types = VehicleType::all();

        return view('CMS.users.potential_owners.create')->with(['vehicle_types' => $vehicle_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // $Existing_email = User::where('email', $request->owner_email)->get();
        // dd($Existing_email == null);
        // $Existing_phone = User::where('phone', $request->owner_phone)->get();
        // dd($Existing_phone == null );
        // if( $Existing_phone !== null && 
        //     $Existing_email !== null ){
        //     dd($Existing_email);
        //     dd("fuhkush");
        // }



        $potential_owner = PotentialOwner::where('owner_phone','=',$request->owner_phone)->get();
        if(count($potential_owner) > 0){
            $notification = ['danger', 'This Potential Customer Already Exist.'];
            session()->flash('message', $notification);

            return redirect('/cms/users/potential_owners/');
        }

        
        $parameters = $request->only('company_name', 'owner_name', 'owner_phone', 'owner_email', 'manager_name', 'manager_phone', 'location');
        $profile = PotentialOwner::create($parameters);

        // assign/add vehicle types for a potential owner
        $profile->myVehicleTypes()->attach($request->vehicle_type);

        //flash message
        $notification = ['success', 'Profile added successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/potential_owners/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = PotentialOwner::find($id);
        $vehicle_types = VehicleType::all();

        return view('CMS.users.potential_owners.show')->with(['profile' => $profile])->with(['vehicle_types' => $vehicle_types]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = PotentialOwner::find($id);
        $vehicle_types = VehicleType::all();

        return view('CMS.users.potential_owners.edit')->with(['profile' => $profile])->with(['vehicle_types' => $vehicle_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $potential_owner = PotentialOwner::where('owner_phone','=',$request->owner_phone)->get();
        if(count($potential_owner) > 0){
            $notification = ['danger', 'This Potential Customer Already Exist.'];
            session()->flash('message', $notification);

            return redirect('/cms/users/potential_owners/');
        }
        
        $potential_owner = PotentialOwner::find($id);
        $profile = $potential_owner->updateProfile($request);

        // assign/add vehicle types for the potential owner
        $potential_owner->myVehicleTypes()->sync($request->vehicle_type);

        //flash message
        $notification = ['success','Profile updated successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/potential_owners/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        PotentialOwner::where('id', $id)->delete();

        //flash message
        $notification = ['success', 'Deleted successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/users/potential_owners');
    }
}
