<?php

namespace App\Http\Controllers\CMS\Users;

use App\Http\Controllers\CMS\CmsPanelController;

use Illuminate\Http\Request;

use App\CustomerProfile;
use App\User;

class CustomerController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter') ? $request->input('filter'): 'active';

        $list = CustomerProfile::$filter()->get()->sortByDesc('created_at');

        return view('CMS.users.customers.list')->with(['list' => $list, 'fitler' => $filter]);
    }

     /**
     * Display the specified resource.
     */
    public function show(CustomerProfile $customer)
    {
        $history = $customer->myBookings()->whereIn('status', ['completed', 'cancelled'])->orderBy('updated_at', 'desc')->get();

        $rating = $customer -> myAverageRating();
        return view('CMS.users.customers.show')->with(['profile' => $customer])->with('rating', $rating)->with('history', $history);
    }
}
