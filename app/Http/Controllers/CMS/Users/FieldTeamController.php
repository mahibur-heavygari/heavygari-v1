<?php

namespace App\Http\Controllers\CMS\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\CMS\CmsPanelController;
use App\User;
use App\CustomerProfile;
use App\OwnerProfile;
use App\Vehicle;
use App\DriverProfile;
use App\AdminProfile;
use Illuminate\Support\Facades\DB;


class FieldTeamController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $admin_profiles = AdminProfile::where('user_id','!=','1')->get();
        return view('CMS.users.field_teams.list')->with('users', $admin_profiles);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = User::find($id);

        return view('CMS.users.field_teams.show')->with(['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $profile = User::find($id);

        return view('CMS.users.field_teams.edit')->with(['profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $field_team = User::find($id);
        $field_team->name = $request->name;
        $field_team->phone = $request->phone;
        $field_team->email = $request->email;
        $field_team->national_id = $request->national_id;
        $field_team->address = $request->address;
        $field_team->save();

        
        //flash message
        $notification = ['success','Profile updated successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/field_team/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {   
        AdminProfile::where('user_id', $id)->delete();

        //flash message
        $notification = ['success', 'Deleted successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/users/field_team/');
    }
    public function customerNumber($id)
    {   
        $admin = AdminProfile::where('id',$id)->get();
        $customers = CustomerProfile::where('profile_admin_id',$id)->get()->sortByDesc('created_at');
        return view('CMS.users.field_teams.registered_users.customers')->with(['customers' => $customers])->with(['admin' => $admin]);
    }
    public function ownerNumber($id)
    {
        $admin = AdminProfile::where('id',$id)->get();
        $owners = OwnerProfile::where('profile_admin_id',$id)->get()->sortByDesc('created_at');
        return view('CMS.users.field_teams.registered_users.owners')->with(['admin' => $admin])->with(['owners' => $owners]);
    }
    public function driverNumber($id)
    {
        $admin = AdminProfile::where('id',$id)->get();
        $drivers = DriverProfile::where('profile_admin_id',$id)->get()->sortByDesc('created_at');
        return view('CMS.users.field_teams.registered_users.drivers')->with(['admin' => $admin])->with(['drivers' => $drivers]);
    }
    public function vehicleNumber($id)
    {
        $admin = AdminProfile::where('id',$id)->get();
        $vehicles = Vehicle::where('profile_admin_id',$id)->get()->sortByDesc('created_at');
        return view('CMS.users.field_teams.registered_users.vehicles')->with(['admin' => $admin])->with(['vehicles' => $vehicles]);
    }
}