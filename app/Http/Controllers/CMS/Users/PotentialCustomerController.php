<?php

namespace App\Http\Controllers\CMS\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\CMS\CmsPanelController;
use App\PotentialCustomer;


class PotentialCustomerController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = PotentialCustomer::orderBy('created_at', 'desc')->get();
        return view('CMS.users.potential_customers.list')->with(['list' => $list]);
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('CMS.users.potential_customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$potential_customer = PotentialCustomer::where('customer_phone','=',$request->customer_phone)->get();
        if(count($potential_customer) > 0){
            $notification = ['danger', 'This Potential Customer Already Exist.'];
            session()->flash('message', $notification);

            return redirect('/cms/users/potential_customers/');
        }*/


        $parameters = $request->only('customer_name','company_name', 'customer_phone', 'customer_email', 'customer_address', 'customer_area');
        $profile = PotentialCustomer::create($parameters);

        
        //flash message
        $notification = ['success', 'Profile added successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/potential_customers/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = PotentialCustomer::find($id);

        return view('CMS.users.potential_customers.show')->with(['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = PotentialCustomer::find($id);

        return view('CMS.users.potential_customers.edit')->with(['profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'customer_name' => 'required'
        // ]);

        //dd($request->all());
        $potential_customer = PotentialCustomer::where('customer_phone','=',$request->customer_phone)->get();
        if(count($potential_customer) > 0){
            $notification = ['danger', 'This Potential Customer Already Exist.'];
            session()->flash('message', $notification);

            return redirect('/cms/users/potential_customers/');
        }
        
        $potential_customer = PotentialCustomer::find($id);
        $profile = $potential_customer->updateProfile($request);

        
        //flash message
        $notification = ['success','Profile updated successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/potential_customers/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        PotentialCustomer::where('id', $id)->delete();

        //flash message
        $notification = ['success', 'Deleted successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/users/potential_customers');
    }
}