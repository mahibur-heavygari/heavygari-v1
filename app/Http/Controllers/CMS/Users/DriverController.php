<?php

namespace App\Http\Controllers\CMS\Users;

use App\User;
use App\DriverProfile;
use App\VehicleType;
use Illuminate\Http\Request;
use App\Http\Requests\Driver\ProfileFormRequest;
use App\Http\Controllers\CMS\CmsPanelController;


class DriverController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $vehicle_types = VehicleType::get();
        $filter = $request->input('filter') ? $request->input('filter'): 'active';
        $parameters = [];
        $appends = [];

        $list = DriverProfile::$filter()->get()->sortByDesc('created_at');
        if($request->address) {
            $address = $request->address;
            
            $list = DriverProfile::$filter()->with('user')->whereHas('user', function($user) use($address){
                 $user->where('address','LIKE', '%'.$address.'%');
            })->get();
            
            $appends['address'] = $request->address;
        }


        if($request->vehicle_type_id) {
            $vehicle_type_id = $request->vehicle_type_id;
            $list = $list->filter(function ($value, $key) use($vehicle_type_id){                
                $vehicle_type_exist = $value->myAuthorizedVehicles->where('vehicle_type_id', $vehicle_type_id)->first();
                if(!is_null($vehicle_type_exist))
                    return $value;
            });

            $appends['vehicle_type_id'] = $request->vehicle_type_id;
        }

        foreach ($list as $key => $value) {
            $vehicle_info = "";
            $grouped = $value->myAuthorizedVehicles->groupBy('vehicle_type_id');

            foreach ($grouped as $key => $group) {
                $single_info = 'Total '.$group[0]->vehicleType->title .' - '.count($group);
                $vehicle_info = $vehicle_info. $single_info.'<br>';
            }
            $value['vehicle_info'] = $vehicle_info;
        }
        
        return view('CMS.users.drivers.list')->with(['list' => $list, 'fitler' => $filter])->with('vehicle_types', $vehicle_types)->with('appends', $appends);
    }

    /**
     * Display the specified resource.
     */
    public function show(DriverProfile $driver)
    {
        $rating = $driver -> myAverageRating();
        $history = $driver->myTrips()->completed()->orderBy('updated_at', 'desc')->get();
        
        return view('CMS.users.drivers.show')->with(['profile' => $driver])->with('rating', $rating)->with('history', $history)->with('adminPanel', true);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DriverProfile $driver)
    {
        $owner = $driver->myOwner;

        $my_vehicles = $owner->myVehicles;

        $authorized_vehicles = $driver->myAuthorizedVehicles;

        return view('CMS.users.drivers.edit')->with('profile', $driver)->with('my_vehicles', $my_vehicles)->with('authorized_vehicles', $authorized_vehicles);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, DriverProfile $driver)
    {
        // validation
        $formRequest = new ProfileFormRequest();
        $rules = $formRequest->rules($driver);
        $this->validate($request, $rules);

        $profile = $driver->updateProfile($request);

        //flash message
        $notification = ['success','Profile Updated Successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/drivers/'.$driver->id.'/edit');
    }
}
