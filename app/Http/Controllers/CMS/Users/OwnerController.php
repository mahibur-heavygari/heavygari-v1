<?php

namespace App\Http\Controllers\CMS\Users;

use App\User;
use App\OwnerProfile;
use App\VehicleType;
use Illuminate\Http\Request;
use App\Http\Requests\Owner\ProfileFormRequest;
use App\Http\Controllers\CMS\CmsPanelController;

class OwnerController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {   
        $vehicle_types = VehicleType::get();

        $filter = $request->input('filter') ? $request->input('filter'): 'active';

        $parameters = [];
        $appends = [];

        $list = OwnerProfile::$filter()->get()->sortByDesc('created_at');
        
        /*$list = $list->filter(function ($value, $key){
            if($value->user->name == 'Sample owner')
                return $value;
        });*/

        if($request->address) {
            $address = $request->address;
            
            $list = OwnerProfile::$filter()->with('user')->whereHas('user', function($user) use($address){
                 $user->where('address','LIKE', '%'.$address.'%');
            })->get();
            
            $appends['address'] = $request->address;
        }

        if($request->vehicle_type_id) {
            $vehicle_type_id = $request->vehicle_type_id;
            
            $list = $list->filter(function ($value, $key) use($vehicle_type_id){                
                $vehicle_type_exist = $value->myVehicles->where('vehicle_type_id', $vehicle_type_id)->first();
                if(!is_null($vehicle_type_exist))
                    return $value;
            });
            $appends['vehicle_type_id'] = $request->vehicle_type_id;
        }

        foreach ($list as $key => $value) {
            $vehicle_info = "";
            $grouped = $value->myVehicles->groupBy('vehicle_type_id');
            foreach ($grouped as $key => $group) {
                $single_info = 'Total '.$group[0]->vehicleType->title .' - '.count($group);
                $vehicle_info = $vehicle_info. $single_info.'<br>';
            }
            $value['vehicle_info'] = $vehicle_info;
        }
        return view('CMS.users.owners.list')->with(['list' => $list, 'fitler' => $filter])->with('vehicle_types', $vehicle_types)->with('appends', $appends);
    }

    /**
     * Display the specified resource.
     */
    public function show(OwnerProfile $owner)
    {
        $history = $owner->myBookings()->completed()->orderBy('updated_at', 'desc')->get();
        
        return view('CMS.users.owners.show')->with(['profile' => $owner])->with(['history' => $history]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(OwnerProfile $owner)
    {
        return view('CMS.users.owners.edit')->with('profile', $owner);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, OwnerProfile $owner)
    {
        // validation
        $formRequest = new ProfileFormRequest();
        $rules = $formRequest->rules($owner);
        $this->validate($request, $rules);

        $profile = $owner->updateProfile($request);

        //flash message
        $notification = ['success','Profile Updated Successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/users/owners');
    }

    /**
     * Display the specified resource.
     */
    public function changeMobileAppStatus(OwnerProfile $owner_profile, Request $request)
    {
        $owner_profile->without_app = $request->status;
        $owner_profile->save();

        $notification = ['success','Status was changed!'];
        session()->flash('message', $notification);

        return redirect()->back();
    }

    public function saveNote(Request $request, $user)
    {
        OwnerProfile::where('id', $user)->update(['note'=>$request->note]);
        return response()->json(['success'=> true], 200);
    }
}
