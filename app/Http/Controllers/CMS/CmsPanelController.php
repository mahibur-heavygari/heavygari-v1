<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

class CmsPanelController extends controller
{
    /**
     * Returns the logged in owner's profile id
     */
    public function adminProfile()
    {
        $profile = ['name' => 'Demo Admin'];

        return $profile;
    }
}
