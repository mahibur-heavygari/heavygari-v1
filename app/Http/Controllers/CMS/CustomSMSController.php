<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\CustomerProfile;
use App\OwnerProfile;
use App\DriverProfile;
use App\AuthorizedPoint;
use App\MyLibrary\SMSLib\SMSFacade;
use App\SmsHistory;
use App\PushNotification;
use App\User;
use App\OtherUserPhone;
use App\PotentialOwner;
use App\PotentialCustomer;
use App\PotentialDriver;

class CustomSMSController extends CmsPanelController
{
    public function sendSMS()
    {
    	$filter = 'active';
    	$customers = CustomerProfile::$filter()->get();
        $owners = OwnerProfile::$filter()->get();
        $owner_managers = OwnerProfile::$filter()->where('manager_phone', '!=', NULL)->get();
    	$drivers = DriverProfile::$filter()->get();
        $authorized_points = AuthorizedPoint::get();
        $other_customers = OtherUserPhone::where('user_type', 'customer')->get();
        $other_owners = OtherUserPhone::where('user_type', 'owner')->get();
        $other_drivers = OtherUserPhone::where('user_type', 'driver')->get();
        $potential_owners = PotentialOwner::all();
        $potential_customers = PotentialCustomer::all();
        $potential_drivers = PotentialDriver::all();

    	foreach ($customers as $key => $value) {
    		$value->phone = $value->user->phone;
    		$value->name = $value->user->name;
    	}
        foreach ($owners as $key => $value) {
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($owner_managers as $key => $value) {
            $value->phone = $value->manager_phone;
            $value->name = $value->manager_name;
        }
        foreach ($drivers as $key => $value) {
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($authorized_points as $key => $value) {
            $value->phone = $value->phone;
            $value->name = $value->owner_name;
        }
        foreach ($other_customers as $key => $value) {
            $value->phone = $value->phone;
            $value->name = '';
        }
        foreach ($other_owners as $key => $value) {
            $value->phone = $value->phone;
            $value->name = '';
        }
        foreach ($other_drivers as $key => $value) {
            $value->phone = $value->phone;
            $value->name = '';
        }
        foreach ($potential_owners as $key => $value) {
            $value->phone = $value->owner_phone;
            $value->name = $value->owner_name;
        }
        foreach ($potential_customers as $key => $value) {
            $value->phone = $value->customer_phone;
            $value->name = $value->customer_name;
        }
        foreach ($potential_drivers as $key => $value) {
            $value->phone = $value->driver_phone;
            $value->name = $value->driver_name;
        }

    	return view('CMS.custom_sms.send')->with(['customers' => $customers, 'owners' => $owners, 'drivers' => $drivers, 'authorized_points' => $authorized_points, 'owner_managers' => $owner_managers, 'other_customers' => $other_customers, 'other_owners' => $other_owners, 'other_drivers' => $other_drivers, 'potential_owners' => $potential_owners, 'potential_drivers' => $potential_drivers, 'potential_customers' => $potential_customers]);
    }

    public function sendSMSPost(Request $request)
    {
    	$this->validate($request, [
            'text' => 'required'
        ]);
        //dd($request->all());

        $filter = 'active';

        $send_multiple = false;
        if($request->user_type == 'others'){
            $to = $request->others_phone;
        }elseif($request->phone_number == 'all' && $request->user_type == 'customer'){
            $to = CustomerProfile::$filter()->with('user')->get()->pluck('user.phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'owner'){
            $to = OwnerProfile::$filter()->with('user')->get()->pluck('user.phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'owner_manager'){
            $to = OwnerProfile::$filter()->where('manager_phone', '!=', NULL)->get()->pluck('manager_phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'driver'){
            $to = DriverProfile::$filter()->with('user')->get()->pluck('user.phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'arp'){
            $to = AuthorizedPoint::get()->pluck('phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'potential_owner'){
            $to = PotentialOwner::get()->pluck('phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'potential_customer'){
            $to = PotentialCustomer::get()->pluck('phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'potential_driver'){
            $to = PotentialDriver::get()->pluck('phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'other_customer'){
            $to = OtherUserPhone::where('user_type', 'customer')->get()->pluck('phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'other_owner'){
            $to = OtherUserPhone::where('user_type', 'owner')->get()->pluck('phone');
            $send_multiple = true;
        }elseif($request->phone_number == 'all' && $request->user_type == 'other_driver'){
            $to = OtherUserPhone::where('user_type', 'driver')->get()->pluck('phone');
            $send_multiple = true;
        }else{
            $phone_separate = trim(preg_replace('/\s+/', '', $request->phone));
            $phone_separate = explode(',', $phone_separate);
            foreach($phone_separate as $key => $phone){
                $to = $phone;
                $msg = $request->text;
                $ref_id = "$to"."_".time();
                $status = SMSFacade::send($to, $msg, $ref_id, $request->user_type);
            }
            
        }

        $msg = $request->text;
        $ref_id = "$to"."_".time();
        $status = null;

        if($send_multiple == false){
            if($request->user_type == 'others'){
                $user = User::where('phone', $to)->first();
                if(!is_null($user)){
                    $notification = ['danger','The phone number already resitered in the system'];
                    session()->flash('message', $notification);
                    return redirect('/cms/send/sms');
                }

                OtherUserPhone::updateOrCreate([
                    'phone' => $to,
                    'user_type'=>isset( $request->others_type ) ? $request->others_type : 'unknown'
                ]);
            }
            $status = SMSFacade::send($to, $msg, $ref_id, $request->user_type);
        }else{    
            $toArr = $to->toArray();   
            $totalPhnNumber = count( $toArr );  
            $numberOfItemsPerCycle = 150;
            $totalCycles = (int)ceil( $totalPhnNumber / $numberOfItemsPerCycle );

            for( $i = 0; $i<$totalCycles; $i++ ) {
                $start = $i * $numberOfItemsPerCycle;
                $phnNumbersArr = array_slice($toArr, $start, $numberOfItemsPerCycle);
                $phnNumbersCollection = collect($phnNumbersArr);              
                $status = SMSFacade::sendBulk($phnNumbersCollection, $msg, $request->user_type);
            }
        }

        if ($status) {
            $notification = ['success','SMS sent successfully'];
            session()->flash('message', $notification);
            return redirect('/cms/send/sms');
        } else {
            $notification = ['danger','Sorry, Could not send the SMS'];
            session()->flash('message', $notification);
            return redirect('/cms/send/sms');
        }
    }

    public function getAllSMS(){
        $all_sms = SmsHistory::get()->sortByDesc('created_at');
        foreach ($all_sms as $key => $sms) {
            if($sms->single_bulk == 'bulk'){
                $sms['user'] = 'all';
            }else{
                $sms['user'] = $sms->to;
            }

            if($sms->user_type == 'arp'){
                $sms['user_type'] = 'Authorized Registration Point';
            }
        }
        return view('CMS.custom_sms.history')->with(['all_sms' => $all_sms]);
    }
}
