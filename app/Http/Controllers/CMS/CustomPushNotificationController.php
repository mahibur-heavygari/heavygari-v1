<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\CustomerProfile;
use App\OwnerProfile;
use App\DriverProfile;
use App\User;
use App\MyLibrary\Firebase\PushNotification;
use App\MyLibrary\Firebase\WebPushNotification;
use App\PushNotification as PushNotificationModel;
use Sentinel;
use Illuminate\Support\Facades\Input;
use Storage;


class CustomPushNotificationController extends CmsPanelController
{
    public function sendNotification()
    {
    	$filter = 'active';
    	$customers = CustomerProfile::$filter()->get();
    	$drivers = DriverProfile::$filter()->get();

    	foreach ($customers as $key => $value) {
            $value->name = $value->user->name;
            $value->phone = $value->user->phone;
            $value->user_id = $value->user->id;
    	}
    	foreach ($drivers as $key => $value) {
    		$value->name = $value->user->name;
            $value->phone = $value->user->phone;
            $value->user_id = $value->user->id;
    	}
        
    	return view('CMS.custom_notification.send')->with(['customers' => $customers, 'drivers' => $drivers]);
    }

    public function sendNotificationPost(Request $request)
    {
        //dd($request->user);
        //dd($request->trafic != null);
        $this->validate($request, [
            'user_type' => 'required'
            // ,
            // 'user' => 'required',
            // 'title' => 'required',
            // 'text' => 'required'
        ]);
        $admin = Sentinel::getUser();

        $icon = '/website/images/fav.png';
        $image = NULL;

        if($request->user_type == "market")
        {   
            $request->title = "মার্কেট আপডেট";

           if($request->dhaka_market == "normal"){
                $market[] = "ঢাকাতে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->dhaka_market == "more"){
                $market[] = "ঢাকাতে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->dhaka_market == "less"){
                $market[] = "ঢাকাতে আজকের ভাড়া একটু কম আছে";
            }


            if($request->chittagong_market == "normal"){
                $market[] = "চট্টগ্রামে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->chittagong_market == "more"){
                $market[] = "চট্টগ্রামে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->chittagong_market == "less"){
                $market[] = "চট্টগ্রামে আজকের ভাড়া একটু কম আছে";
            }

            if($request->rajshahi_market == "normal"){
                $market[] = "রাজশাহীতে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->rajshahi_market == "more"){
                $market[] = "রাজশাহীতে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->rajshahi_market == "less"){
                $market[] = "রাজশাহীতে আজকের ভাড়া একটু কম আছে";
            }
           
           
            if($request->khulna_market == "normal"){
                $market[] = "খুলনাতে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->khulna_market == "more"){
                $market[] = "খুলনাতে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->khulna_market == "less"){
                $market[] = "খুলনাতে আজকের ভাড়া একটু কম আছে";
            }

            if($request->sylhet_market == "normal"){
                $market[] = "সিলেটে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->sylhet_market == "more"){
                $market[] = "সিলেটে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->sylhet_market == "less"){
                $market[] = "সিলেটে আজকের ভাড়া একটু কম আছে";
            }

           
            if($request->rangpur_market == "normal"){
                $market[] = "রংপুরে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->rangpur_market == "more"){
                $market[] = "রংপুরে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->rangpur_market == "less"){
                $market[] = "রংপুরে আজকের ভাড়া একটু কম আছে";
            }

       
            if($request->barisal_market == "normal"){
                $market[] = "বরিশালে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->barisal_market == "more"){
                $market[] = "বরিশালে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->barisal_market == "less"){
                $market[] = "বরিশালে আজকের ভাড়া একটু কম আছে";
            }

            if($request->mymensingh_market == "normal"){
                $market[] = "ময়মনসিংহে আজকের ভাড়া স্বাভাবিক আছে";
            }elseif($request->mymensingh_market == "more"){
                $market[] = "ময়মনসিংহে আজকের ভাড়া বাড়তি আছে";
            }elseif($request->mymensingh_market == "less"){
                $market[] = "ময়মনসিংহে আজকের ভাড়া একটু কম আছে";
            }
            $request->text = implode (", \n", $market);
            $request->text = $request->text .'।';

        }elseif($request->user_type == "trafic"){   
            $request->title = "ট্রাফিক আপডেট";

            if($request->north == "normal"){
                $traffic[] = "উত্তরবঙ্গে আজকের জ্যাম স্বাভাবিক আছে";
            }elseif($request->north == "more"){
                $traffic[] = "উত্তরবঙ্গে আজকের জ্যাম বাড়তি আছে";
            }elseif($request->north == "less"){
                $traffic[] = "উত্তরবঙ্গে আজকের জ্যাম একটু কম আছে";
            }
            if($request->south == "normal"){
                $traffic[] = "দক্ষিণবঙ্গে আজকের জ্যাম স্বাভাবিক আছে";
            }elseif($request->south == "more"){
                $traffic[] = "দক্ষিণবঙ্গে আজকের জ্যাম বাড়তি আছে";
            }elseif($request->south == "less"){
                $traffic[] = "দক্ষিণবঙ্গে আজকের জ্যাম একটু কম আছে";
            }

            if($request->east == "normal"){
                $traffic[] = "পূর্ববঙ্গে আজকের জ্যাম স্বাভাবিক আছে";
            }elseif($request->east == "more"){
                $traffic[] = "পূর্ববঙ্গে আজকের জ্যাম বাড়তি আছে";
            }elseif($request->east == "less"){
                $traffic[] = "পূর্ববঙ্গে আজকের জ্যাম একটু কম আছে";
            }
            if($request->west == "normal"){
                $traffic[] = "পশ্চিমবঙ্গে আজকের জ্যাম স্বাভাবিক আছে";
            }elseif($request->west == "more"){
                $traffic[] = "পশ্চিমবঙ্গে আজকের জ্যাম বাড়তি আছে";
            }elseif($request->west == "less"){
                $traffic[] = "পশ্চিমবঙ্গে আজকের জ্যাম একটু কম আছে";
            }
            if($request->dhaka == "normal"){
                $traffic[] = "ঢাকা শহরে আজকের জ্যাম স্বাভাবিক আছে";
            }elseif($request->dhaka == "more"){
                $traffic[] = "ঢাকা শহরে আজকের জ্যাম বাড়তি আছে";
            }elseif($request->dhaka == "less"){
                $traffic[] = "ঢাকা শহরে আজকের জ্যাম একটু কম আছে";
            }
            if($request->ctg == "normal"){
                $traffic[] = "চট্টগ্রাম শহরে আজকের জ্যাম স্বাভাবিক আছে";
            }elseif($request->ctg == "more"){
                $traffic[] = "চট্টগ্রাম শহরে আজকের জ্যাম বাড়তি আছে";
            }elseif($request->ctg == "less"){
                $traffic[] = "চট্টগ্রাম শহরে আজকের জ্যাম একটু কম আছে";
            }



            $links = implode (", \n", $traffic);

            $request->text = $links .'।';
            //dd($request->text);
            
        }

        if (Input::hasFile('image')) {
            $notification_image = $request->file('image')->store('public/notification_images');
            $image = Storage::url($notification_image);
            $icon = Storage::url($notification_image);
        }

        //message for android
        $mobile_message = [
            'data' => [
                'title' => $request->title,
                'body' => $request->text,
                'image' => $image,
                'id' => NULL,
                'unique_id' => NULL,
                'category' => 'admin-notification',
                'type' => NULL
            ]
        ];

        //message for ios
        $ios_mobile_message = [
            'content-available' => true,
            'notification'=> [
                'title' => $request->title,
                'body' => $request->text,
            ],
            'data' => [
                'title' => $request->title,
                'body' => $request->text,
                'image' => $image,
                'id' => NULL,
                'unique_id' => NULL,
                'category' => 'admin-notification',
                'type' => NULL
            ]
        ];

        $mobile_notification_model_data = [
            'mobile_web' => 'mobile', 
            'admin_notification' => 'yes', 
            'title' => $mobile_message['data']['title'], 
            'body' => $mobile_message['data']['body'], 
            'category' => $mobile_message['data']['category'], 
            'sent_user_id' => $admin->id
        ];
        $filter = 'active';
        $mbl_registration_ids = [];
        $ios_mbl_registration_ids = [];
        $model_data = [];
        $mobile_push = new PushNotification();
        if($request->user == 'all' && $request->user_type != 'market' && $request->user_type != 'trafic'){
            if($request->user_type == 'customer'){
                $customers = CustomerProfile::$filter()->get();
                foreach ($customers as $key => $customer) {
                    $user_id = $customer->user->id;
                    $fcm_registration_token = $customer->user->fcm_registration_token;

                    if(!is_null($fcm_registration_token)){
                        if($customer->user->device_type=='ios'){
                            $ios_mbl_registration_ids[] = $fcm_registration_token;
                        }else{                            
                            $mbl_registration_ids[] = $fcm_registration_token;
                        }
                    }
                } 
                $mobile_notification_model_data['received_user_type'] = 'customer';
                $mobile_notification_model_data['received_user_id'] = 0;
                PushNotificationModel::create($mobile_notification_model_data); 
                $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                $mobile_push->sendToMultipleDevice($ios_mbl_registration_ids, $ios_mobile_message); 

            }elseif($request->user_type == 'driver'){
                $drivers = DriverProfile::$filter()->get();
                foreach ($drivers as $key => $driver) {
                    $user_id = $driver->user->id;
                    $fcm_registration_token = $driver->user->fcm_registration_token;

                    if(!is_null($fcm_registration_token)){
                        $mbl_registration_ids[] = $fcm_registration_token;
                    }
                }
                $mobile_notification_model_data['received_user_type'] = 'driver';
                $mobile_notification_model_data['received_user_id'] = 0;
                PushNotificationModel::create($mobile_notification_model_data);   
                $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);         
            }
        }elseif($request->user_type == 'market'){
            if($request->user == 'all'){
                    $customers = CustomerProfile::$filter()->get();
                    foreach ($customers as $key => $customer) {
                        $user_id = $customer->user->id;
                        $fcm_registration_token = $customer->user->fcm_registration_token;
                        if(!is_null($fcm_registration_token)){
                            if($customer->user->device_type=='ios'){
                                $ios_mbl_registration_ids[] = $fcm_registration_token;
                            }else{                            
                                $mbl_registration_ids[] = $fcm_registration_token;
                            }
                        }
                        $mobile_notification_model_data['received_user_id'] = $user_id;
                        $mobile_notification_model_data['received_user_type'] = 'Customer';
                        
                    }
                        PushNotificationModel::create($mobile_notification_model_data);
                        $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                        $mobile_push->sendToMultipleDevice($ios_mbl_registration_ids, $ios_mobile_message);

                }else{
                    $user = User::where('id', $request->user)->first();
                    if(!is_null($user->fcm_registration_token)){
                        $fcm_registration_token = $user->fcm_registration_token;

                        if($user->device_type=='ios'){
                            $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $ios_mobile_message);
                        }
                        else{                    
                            $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $mobile_message);
                        }

                        $mobile_notification_model_data['received_user_id'] = $user->id;
                        $mobile_notification_model_data['received_user_type'] = 'Customer';
                        PushNotificationModel::create($mobile_notification_model_data);
                        $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                        $mobile_push->sendToMultipleDevice($ios_mbl_registration_ids, $ios_mobile_message);
                    }


                }
                
            
        }elseif($request->user_type == "trafic"){
            if($request->special_user_type=='driver'){
                if($request->user == 'all'){
                   $drivers = DriverProfile::$filter()->get();
                    foreach ($drivers as $key => $driver) {
                        $user_id = $driver->user->id;
                        $fcm_registration_token = $driver->user->fcm_registration_token;
                        if(!is_null($fcm_registration_token)){
                                                       
                                $mbl_registration_ids[] = $fcm_registration_token;
                            }
                        $mobile_notification_model_data['received_user_id'] = $user_id;
                        $mobile_notification_model_data['received_user_type'] = 'Driver';
                        PushNotificationModel::create($mobile_notification_model_data);
                        
                    }
                        
                        $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                }else{

                    $user = User::where('id', $request->user)->first();
                    if(!is_null($user->fcm_registration_token)){
                        $fcm_registration_token = $user->fcm_registration_token;

                        if($user->device_type=='ios'){
                            $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $ios_mobile_message);
                        }
                        else{                    
                            $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $mobile_message);
                        }

                        $mobile_notification_model_data['received_user_id'] = $user->id;
                        $mobile_notification_model_data['received_user_type'] = 'Driver';

                        PushNotificationModel::create($mobile_notification_model_data);
                   }
                    $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                    $mobile_push->sendToMultipleDevice($ios_mbl_registration_ids, $ios_mobile_message);
                }
            }elseif($request->special_user_type=='customer'){
                if($request->user == 'all'){
                    $customers = CustomerProfile::$filter()->get();
                    foreach ($customers as $key => $customer) {
                        $user_id = $customer->user->id;
                        $fcm_registration_token = $customer->user->fcm_registration_token;
                        if(!is_null($fcm_registration_token)){
                            if($customer->user->device_type=='ios'){
                                $ios_mbl_registration_ids[] = $fcm_registration_token;
                            }else{                            
                                $mbl_registration_ids[] = $fcm_registration_token;
                            }
                        }
                        $mobile_notification_model_data['received_user_id'] = $user_id;
                        $mobile_notification_model_data['received_user_type'] = 'Customer';
                        PushNotificationModel::create($mobile_notification_model_data);
                    }
                        $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                        $mobile_push->sendToMultipleDevice($ios_mbl_registration_ids, $ios_mobile_message);

                }else{
                    $user = User::where('id', $request->user)->first();
                    if(!is_null($user->fcm_registration_token)){
                        $fcm_registration_token = $user->fcm_registration_token;

                        if($user->device_type=='ios'){
                            $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $ios_mobile_message);
                        }
                        else{                    
                            $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $mobile_message);
                        }

                        $mobile_notification_model_data['received_user_id'] = $user->id;
                        $mobile_notification_model_data['received_user_type'] = 'Customer';
                        PushNotificationModel::create($mobile_notification_model_data);
                    }

                        
                        $mobile_push->sendToMultipleDevice($mbl_registration_ids, $mobile_message);
                        $mobile_push->sendToMultipleDevice($ios_mbl_registration_ids, $ios_mobile_message);


                }
            }


        }else{
            $user = User::where('id', $request->user)->first();
            if(!is_null($user->fcm_registration_token)){
                $fcm_registration_token = $user->fcm_registration_token;

                if($user->device_type=='ios'){
                    $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $ios_mobile_message);
                }
                else{                    
                    $mobile_send_res = $mobile_push->sendToSingleDevice($fcm_registration_token, $mobile_message);
                }

                $mobile_notification_model_data['received_user_id'] = $user->id;
                $mobile_notification_model_data['received_user_type'] = $user->user_type;
                PushNotificationModel::create($mobile_notification_model_data);
            }
        }

        $notification = ['success','Notification was sent successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/send/push_notification');
    }


    public function getAllNotification()
    {
        $all_notifications = PushNotificationModel::where('category', 'admin-notification')->get()->sortByDesc('created_at');
        foreach ($all_notifications as $key => $notification) {
            if($notification->received_user_id == '0'){
                $notification['type_user'] = $notification->received_user_type;
                $notification['user'] = 'all';
            }else{
                $user = User::find($notification->received_user_id);
                if($user == null){
                    return view('CMS.custom_notification.history')->with(['all_notifications' => $all_notifications]);
                }
                if($user->driverProfile)
                {
                    $notification['type_user'] = 'driver';
                    $notification['user'] = $user->name;
                }elseif($user->customerProfile)
                {
                    $notification['type_user'] = 'customer';
                    $notification['user'] = $user->name;
                }
            }
        }

        return view('CMS.custom_notification.history')->with(['all_notifications' => $all_notifications]);
    }
}
