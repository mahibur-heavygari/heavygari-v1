<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;

use App\MyLibrary\InvoiceLib\InvoiceSystem;
use App\OwnerInvoice;
use App\User;
use App\OwnerProfile;
use App\MyLibrary\SMSLib\SMSFacade;
use App\SmsHistory;

class InvoiceController extends CmsPanelController
{
    /**
     * Create invoice of this month for every owner
     */
    public function cronJob()
    {   
        $owners = \App\OwnerProfile::all();

        foreach ($owners as $owner) {
            $invoice = InvoiceSystem::create($owner->id, date('m'), date('Y'));
        }

        return 'invocie of this month for every owner created';
    }

    /**
     * Create an invoice
     */
    public function create($owner_profile_id, $month, $year)
    {
        $invoice = InvoiceSystem::create($owner_profile_id, $month, $year);

        return true;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $total_due = 0;
        $total_paid = 0;

        $list = InvoiceSystem::all();
        $total_due = $list->where('status', 'due')->sum('total_amount');
        $total_paid = $list->where('status', 'paid')->sum('total_amount');

        return view('CMS.invoices.list')->with('list', $list)->with('adminPanel', true)->with('total_due', $total_due)->with('total_paid', $total_paid);
    }

    public function saveNote(Request $request, $invoice)
    {
        OwnerInvoice::where('id', $invoice)->update(['note'=>$request->note]);
        return response()->json(['success'=> true], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show($invoice_number)
    {
        $invoice = InvoiceSystem::show($invoice_number);

        return view('CMS.invoices.show')->with('invoice', $invoice)->with('adminPanel', true);
    } 

    /**
     * Display the specified resource.
     */
    public function sendSms($invoice_number)
    {   
        $list = InvoiceSystem::all();
        $lists = $list->where('id',$invoice_number)->first();
        $owner = OwnerProfile::where('id',$lists->owner_id)->first();
        $user = User::where('id', $owner->user_id)->get();
        $to = $user['0']->phone;
        $msg = "প্রিয় ".$user['0']->name .", হেভিগাড়ীর সার্ভিস চার্জ বাবদ টাকা:".$lists->total_amount." বকেয়া রয়েছে, বকেয়া টাকা পরিশোধ করে আমাদের সাথে থাকুন। 
বিকাশ নং: 01999097252
ধন্যবাদ।";
        $ref_id = "$to"."_".time();

        $status = SMSFacade::send($to, $msg, $ref_id, 'Owner');

        if ($status) {
            SmsHistory::Create(['sms_text' => $msg, 'to' => $to, 'response' => $ref_id, 'single_bulk' => 'Single', 'user_type' => 'Owner']);
            $notification = ['success','SMS sent successfully'];
            session()->flash('message', $notification);
            return redirect()->back();
        } else {
            $notification = ['danger','Sorry, Could not send the SMS'];
            session()->flash('message', $notification);
            return redirect()->back();
        }

    }

    /**
     * Display the PDF.
     */
    public function getPDF($invoice_number)
    {
        $invoice = InvoiceSystem::show($invoice_number);

        // TODO :: generate pdf and show link

        return view('emails.invoice.owner')->with('invoice', $invoice);
    }


    /**
     * Display the specified resource.
     */
    public function markAsPaid($invoice_number, Request $request)
    {
        $this->validate($request, [
            'paid_at' => 'required',
            'payment_method' => 'required',
            'amount' => 'required'
        ]);

        $make_payment = InvoiceSystem::makePayment($invoice_number, $request);

        if ($make_payment) {
            
            InvoiceSystem::acknowledgementEmail($invoice_number);

            $notification = ['success','Invoice has been marked as Paid!'];
        } else {
            $notification = ['error','Sorry! Error in updating the status.'];
        }

        session()->flash('message', $notification);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     */
    public function sendEmail($invoice_number)
    {
        $email = InvoiceSystem::email($invoice_number);

        if ($email) {
            $notification = ['success','Invoice has been mailed to owner!'];
        } else {
            $notification = ['error','Sorry! Error in sending mail.'];
        }

        session()->flash('message', $notification);

        return redirect()->back();
    }
}
