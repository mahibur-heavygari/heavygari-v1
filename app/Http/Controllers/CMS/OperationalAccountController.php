<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\MyLibrary\AccountsLib\OperationalAccounts;
use App\OwnerInvoice;
use App\Booking;
use PDF;
use View;
use App;


class OperationalAccountController extends CmsPanelController
{
    public function paidOperations( Request $request )
    {
        $accounts = new OperationalAccounts;
        $paidAccounts = $accounts->paidOperations($request);

        $total_owner_amount = $paidAccounts->sum('owner_amount');
        $total_driver_amount = $paidAccounts->sum('driver_amount');
        $total_admin_amount = $paidAccounts->sum('admin_amount')-$paidAccounts->sum('discount');
        $total_discount = $paidAccounts->sum('discount');

        $total_driver_paid_amount = $paidAccounts->where('driver_amount_status', 'paid')->sum('driver_amount');
        $total_driver_unpaid_amount = $paidAccounts->where('driver_amount_status', 'unpaid')->sum('driver_amount');

        $html = View::make('CMS.operational_accounts.paid_print', compact('paidAccounts', 'total_owner_amount', 'total_driver_paid_amount', 'total_driver_unpaid_amount', 'total_driver_amount', 'total_admin_amount', 'total_discount'))->render();
        $html = str_replace("\n", "", $html);

        return view('CMS.operational_accounts.paid')->with('paidAccounts', $paidAccounts)->with('total_owner_amount', $total_owner_amount)->with('total_driver_paid_amount', $total_driver_paid_amount)->with('total_driver_unpaid_amount', $total_driver_unpaid_amount)->with('total_driver_amount', $total_driver_amount)->with('total_discount', $total_discount)->with('total_admin_amount', $total_admin_amount)->with('html', $html);
    }

    public function unpaidOperations( Request $request )
    {
        $accounts = new OperationalAccounts;
        $unpaidAccounts = $accounts->unpaidOperations($request);

        $total_owner_due_amount = $unpaidAccounts->sum('owner_amount');
        $total_driver_due_amount = $unpaidAccounts->sum('driver_amount');
        $total_admin_due_amount = $unpaidAccounts->sum('admin_amount')-$unpaidAccounts->sum('discount');
        $total_discount = $unpaidAccounts->sum('discount');

        $print_html = View::make('CMS.operational_accounts.unpaid_print', compact('unpaidAccounts', 'total_owner_due_amount', 'total_driver_due_amount', 'total_admin_due_amount', 'total_discount'))->render();
        $print_html = str_replace("\n", "", $print_html);

        return view('CMS.operational_accounts.unpaid')->with('unpaidAccounts', $unpaidAccounts)->with('total_owner_due_amount', $total_owner_due_amount)->with('total_driver_due_amount', $total_driver_due_amount)->with('total_admin_due_amount', $total_admin_due_amount)->with('total_discount', $total_discount)->with('print_html', $print_html);
    }

}
