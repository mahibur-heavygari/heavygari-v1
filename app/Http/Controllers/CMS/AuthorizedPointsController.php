<?php

namespace App\Http\Controllers\CMS;

use App\Point;
use App\AuthorizedPoint;
use Illuminate\Http\Request;
use App\MyLibrary\SMSLib\SMSFacade;

class AuthorizedPointsController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $list = AuthorizedPoint::all();

        return view('CMS.authorized_points.list')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $points = Point::all();

        return view('CMS.authorized_points.add')->with('points', $points);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'base_point_id' => 'required',
            'address' => 'required',
            'shop_name' => 'required'
        ]);
        
        AuthorizedPoint::create($request->all());

        return redirect('/cms/authorized_points');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AuthorizedPoint $authorized_point)
    {
        $points = Point::all();

        return view('CMS.authorized_points.edit')->with('points', $points)->with('item', $authorized_point);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AuthorizedPoint $authorized_point)
    {
        $this->validate($request, [
            'base_point_id' => 'required',
            'address' => 'required',
            'shop_name' => 'required'
        ]);

        $authorized_point->update($request->all());

        return redirect('/cms/authorized_points');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AuthorizedPoint $authorized_point)
    {
        $authorized_point->delete();

        //flash message
        $notification = ['success','Authorized Point Removede Successfully'];
        session()->flash('message', $notification);

        return redirect('/cms/authorized_points');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function sendSms(AuthorizedPoint $authorized_point)
    {
        return view('CMS.authorized_points.send_sms')->with('item', $authorized_point);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function sendSmsPost(Request $request, AuthorizedPoint $authorized_point)
    {
        $this->validate($request, [
            'phone' => 'required',
            'message' => 'required'
        ]);

        // code for send sms
        $to = $request->phone;
        $msg = $request->message;
        $ref_id = 'authorized point : '.time();

        $status = SMSFacade::send($to, $msg, $ref_id);

        if ($status) {
            $notification = ['success','SMS was sent successfully'];
            session()->flash('message', $notification);

            return redirect('/cms/authorized_points');
        } else {
            $notification = ['danger','Sorry, Could not sent the SMS'];
            session()->flash('message', $notification);

            return redirect('/cms/authorized_points/'.$authorized_point->id.'/send_sms');
        }

    }
}
