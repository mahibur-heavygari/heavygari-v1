<?php

namespace App\Http\Controllers\CMS\Website;

use App\Http\Controllers\CMS\CmsPanelController;

use Illuminate\Http\Request;

use App\HomeFeature;

class FeaturesController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $list = HomeFeature::all();

        return view('CMS.website.features.list')->with('list', $list);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(HomeFeature $feature)
    {
        return view('CMS.website.features.edit')->with('item', $feature);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, HomeFeature $feature)
    {
        $this->validate($request, [
            'title_en' => 'required|max:255',
            'title_bn' => 'required|max:255',
            'text_en' => 'required',
            'text_bn' => 'required'
        ]);

        $feature->update($request->all());

        $feature->uploadIcon($request);

        return redirect('/cms/website/features');
    }
}
