<?php

namespace App\Http\Controllers\CMS\PriceManager;

use App\VehicleType;
use Illuminate\Http\Request;
use App\FullBookingPriceManager;
use App\Http\Controllers\CMS\CmsPanelController;
use Carbon\Carbon;

class FullBookingController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    	$rates = FullBookingPriceManager::all();

        return view('CMS.price_manager.full_booking.list')->with('rates', $rates);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    	$vehicle_types = VehicleType::all();

    	//TODO : hide the vehicles that already has a rate

        return view('CMS.price_manager.full_booking.add')->with('vehicle_types', $vehicle_types);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
            'vehicle_type_id' => 'required|unique:full_booking_price_manager,vehicle_type_id',
            'short_trip_rate' => 'required',
            'up_trip_rate' => 'required',
            'down_trip_rate' => 'required',            
            'long_up_trip_rate' => 'required',
            'long_down_trip_rate' => 'required',
            'admin_commission' => 'required',
        ]);

        FullBookingPriceManager::create($request->all());

        return redirect('/cms/price-manager/full-booking');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FullBookingPriceManager $full_booking)
    {
    	$vehicle_types = VehicleType::all();

        return view('CMS.price_manager.full_booking.edit')->with('item', $full_booking)->with('vehicle_types', $vehicle_types);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FullBookingPriceManager $full_booking)
    {
        $this->validate($request, [
            'vehicle_type_id' => 'required',
            'short_trip_rate' => 'required',
            'up_trip_rate' => 'required',
            'down_trip_rate' => 'required',            
            'long_up_trip_rate' => 'required',
            'long_down_trip_rate' => 'required',
            'admin_commission' => 'required',
        ]);
        $requestData = $request->all();

        if(!is_null($request->from_discount_date)){
            $requestData['from_discount_date'] = (string) Carbon::createFromFormat('d/m/Y H:i A', $request->from_discount_date);
        }

        if(!is_null($request->to_discount_date)){
            $requestData['to_discount_date'] = (string) Carbon::createFromFormat('d/m/Y H:i A', $request->to_discount_date);
        }

        $full_booking->update($requestData);

        return redirect('/cms/price-manager/full-booking');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FullBookingPriceManager $full_booking)
    {
        $rate->delete($request);
    }
}
