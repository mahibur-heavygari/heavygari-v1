<?php

namespace App\Http\Controllers\CMS\PriceManager;

use App\VehicleType;
use Illuminate\Http\Request;
use App\SharedBookingPriceManager;
use App\Http\Controllers\CMS\CmsPanelController;

class SharedBookingController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rates = SharedBookingPriceManager::all();

        return view('CMS.price_manager.shared_booking.list')->with('rates', $rates);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('CMS.price_manager.shared_booking.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        	'min_weight' => 'required',
        	'max_weight' => 'required',
            'base_fare' => 'required',
            'pickup_charge' => 'required',
            'drop_off_charge' => 'required',
            'weight_rate' => 'required',            
            'shortest_trip_rate' => 'required',
            'short_trip_rate' => 'required',
            'normal_trip_rate' => 'required',
            'long_trip_rate' => 'required',
            'surcharge_rate' => 'required',
            'discount_percent' => 'required',
            'admin_commission' => 'required',
        ]);

        SharedBookingPriceManager::create($request->all());

        return redirect('/cms/price-manager/shared-booking');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SharedBookingPriceManager $shared_booking)
    {
        return view('CMS.price_manager.shared_booking.edit')->with('item', $shared_booking);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SharedBookingPriceManager $shared_booking)
    {
        $this->validate($request, [
        	'min_weight' => 'required',
        	'max_weight' => 'required',
            'base_fare' => 'required',
            'pickup_charge' => 'required',
            'drop_off_charge' => 'required',
            'weight_rate' => 'required',
            'shortest_trip_rate' => 'required',
            'short_trip_rate' => 'required',
            'normal_trip_rate' => 'required',
            'long_trip_rate' => 'required',
            'surcharge_rate' => 'required',
            'discount_percent' => 'required',
            'admin_commission' => 'required',
        ]);

        $shared_booking->update($request->all());

        return redirect('/cms/price-manager/shared-booking');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SharedBookingPriceManager $shared_booking)
    {
        $shared_booking->delete();

        return redirect('/cms/price-manager/shared-booking');
    }
}
