<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\Input;
use Storage;
use App\PopUpSetting;
use Carbon\Carbon;

class CustomPopUpNotificationController extends CmsPanelController
{
    public function popupNotification()
    {
        $pop_up = PopUpSetting::orderBy('created_at', 'desc')->first();
        $not_expired = True;

        if(!is_null($pop_up->expired_at)){
            $expired_at = (string) $pop_up->expired_at;
            $present_time = (string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka');

            if( strtotime( $expired_at ) <= strtotime( $present_time )){
                $not_expired = NULL;
            }
        }

        return view('CMS.pop_up.send')->with(['pop_up' => $pop_up])->with(['not_expired' => $not_expired]);
    }

    public function postPopupNotification(Request $request)
    {
        //dd($request);
        $photo = NULL;
        $expired_at = $request->expired_at;

        if(!is_null($expired_at)){
            $expired_at = \Carbon\Carbon::parse($expired_at);
        }

        if ($request->file('image')) {
           $photo = $request->file('image')->store('public/pop_up');
        }
        PopUpSetting::create(['photo'=>$photo, 'button_position'=>$request->button_position, 'expired_at'=>$expired_at]);

        $notification = ['success', 'Pop up updated'];
        session()->flash('message', $notification);
        return redirect('/cms/send/popup_notification');
    }

    public function makeExpired(Request $request)
    {
        $pop_up = PopUpSetting::orderBy('created_at', 'desc')->first();
        PopUpSetting::where('id', $pop_up->id)->update(['expired_at'=>(string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka')]);

        $notification = ['success', 'Pop up expired'];
        session()->flash('message', $notification);
        return redirect('/cms/send/popup_notification');
    }
    public function getAllPopup(){
        $pop_up = PopUpSetting::orderBy('created_at', 'desc')->get();
        return view('CMS.pop_up.history')->with(['pop_up' => $pop_up]);
    }
}
