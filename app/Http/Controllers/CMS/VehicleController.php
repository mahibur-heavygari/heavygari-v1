<?php

namespace App\Http\Controllers\CMS;

use App\Vehicle;
use App\VehicleType;
use App\OwnerProfile;
use Illuminate\Http\Request;
use App\Http\Requests\Owner\AddVehicleFormRequest;

class VehicleController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index( Request $request )
    {   
        // $filter = $request->input('filter') ? $request->input('filter'): 'active';
        $filter = $request->input('title') ? $request->input('title') : '';
        $vehicle_type = VehicleType::where('title', $filter)->first();
        
        if( $filter != '' && $vehicle_type){
            $list = Vehicle::where('vehicle_type_id', $vehicle_type->id)->get()->sortByDesc('created_at');
        }else{
            $list = Vehicle::all()->sortByDesc('created_at');
        }

        $parameters = [];
        $appends = [];

        $vehicle_types = VehicleType::get();
        if($request->address) {
            $address = $request->address;
            $list = OwnerProfile::with('user')->whereHas('user', function($user) use($address){
                 $user->where('address','LIKE', '%'.$address.'%');
            })->get();
            $appends['address'] = $request->address;
        }
        //dd($request->vehicle_type_id);
        if($request->vehicle_type_id) {
            $vehicle_type_id = $request->vehicle_type_id;
            
            $list = $list->filter(function ($value, $key) use($vehicle_type_id){  
            dd($value);              
                $vehicle_type_exist = $value->myVehicles->where('vehicle_type_id', $vehicle_type_id)->first();
                if(!is_null($vehicle_type_exist))
                    return $value;
            });
            $appends['vehicle_type_id'] = $request->vehicle_type_id;
        }
        // foreach ($list as $key => $value) {
        //     $vehicle_info = "";
        //     $grouped = $value->myVehicles->groupBy('vehicle_type_id');
        //     foreach ($grouped as $key => $group) {
        //         $single_info = 'Total '.$group[0]->vehicleType->title .' - '.count($group);
        //         $vehicle_info = $vehicle_info. $single_info.'<br>';
        //     }
        //     $value['vehicle_info'] = $vehicle_info;
        // }

        return view('CMS.vehicles.list')->with(['list' => $list])->with(['filter' => $filter])->with('vehicle_types', $vehicle_types)->with('appends', $appends);
    }

    /**
     * Display the specified resource.
     */
    public function show(Vehicle $vehicle)
    {
        return view('CMS.vehicles.show')->with(['vehicle' => $vehicle])->with(['adminPanel' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Vehicle $vehicle)
    {
        $owner = $vehicle->owner;

        $my_drivers = $owner->myDrivers;

        $authorized_drivers = $vehicle->authorizedDrivers;

        $types = VehicleType::all();

        return view('CMS.vehicles.edit')->with(['vehicle' => $vehicle, 'types' => $types, 'my_drivers' => $my_drivers, 'authorized_drivers' => $authorized_drivers]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        // validation
        $formRequest = new AddVehicleFormRequest();
        $rules = $formRequest->rules($vehicle);
        $this->validate($request, $rules);

        $vehicle = $vehicle->updateProfile($request);

        $notification = ['success','Vehicle information was updated!'];
        session()->flash('message', $notification);

        return redirect()->back();
    }
}
