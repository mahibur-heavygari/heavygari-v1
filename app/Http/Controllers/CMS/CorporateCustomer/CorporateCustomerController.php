<?php

namespace App\Http\Controllers\CMS\CorporateCustomer;

use Illuminate\Http\Request;
use App\Http\Controllers\CMS\CmsPanelController;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\MyLibrary\SMSLib\SMSFacade;
use App\CorporateFullBookingPriceManager;
use App\FullBookingPriceManager;
use App\CorporateCustomer;
use App\CustomerProfile;
use App\VehicleType;
use App\User;
use Validator;
use DB;
use Sentinel;

class CorporateCustomerController extends CmsPanelController
{
    public function index()
    {
        $list = CorporateCustomer::orderBy('created_at', 'desc')->get();

        return view('CMS.corporate_customers.index')->with(['list' => $list]);
    }

    public function create()
    {
        return view('CMS.corporate_customers.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required|unique:corporate_customers,company_name',
            'debit_credit' => 'required',
        ]);

        $corporate_customer = CorporateCustomer::registerCustomer($request);
        $corporate_customer->completeProfile($request);

        if($request->debit_credit=='debit'){
            $corporate_customer->balance = $request->amount;
        }elseif($request->debit_credit=='credit'){
            $corporate_customer->limit = $request->amount;
        }
        $corporate_customer->save();

        $this->addToFullBookingPriceManager( $corporate_customer );

        return redirect('/cms/corporate_customers');
    }

    public function show($id)
    {
        $corporate_customer = CorporateCustomer::find($id);

        return view('CMS.corporate_customers.show')->with(['corporate_customer' => $corporate_customer]);
    }

    public function edit($id)
    {
        $corporate_customer = CorporateCustomer::find($id);

        return view('CMS.corporate_customers.edit')->with(['corporate_customer' => $corporate_customer]);
    }

    public function update(Request $request, $id)
    {

    }

    public function delete($id)
    {
        $corporate_customer = CorporateCustomer::where('id', $id)->first();

        foreach ($corporate_customer->customers as $key => $customer) {
            $customer->user->delete();
            $customer->delete();
        }
        
        $corporate_customer->customers()->detach();
        CorporateCustomer::where('id', $id)->delete();

        //flash message
        $notification = ['success', 'Deleted successfully'];
        session()->flash('message', $notification);
        return redirect('/cms/corporate_customers');
    }

    public function users($id)
    {
        $corporate_customer = CorporateCustomer::find($id);

        //specific type customer
        //$corporate_customer->customers()->wherePivot('user_type', 'manager')->get();

        $customers = $corporate_customer->customers;

        return view('CMS.corporate_customers.users.index')->with(['list' => $customers])->with(['corporate_customer' => $corporate_customer]);
    }

    public function createUser($id)
    {
        $corporate_customer = CorporateCustomer::find($id);

        return view('CMS.corporate_customers.users.create')->with(['corporate_customer' => $corporate_customer]);
    }

    public function showUser($id, $customer_id)
    {
        $customer = CustomerProfile::find($customer_id);
        
        $history = $customer->myBookings()->whereIn('status', ['completed', 'cancelled'])->orderBy('updated_at', 'desc')->get();
        $rating = $customer -> myAverageRating();

        $rel_corporate_customers = DB::table('rel_corporate_customers')->where('customer_profile_id', $customer_id)->first();
        $user_type = ucfirst($rel_corporate_customers->user_type);

        return view('CMS.corporate_customers.users.show')->with(['profile' => $customer])->with('rating', $rating)->with('history', $history)->with('user_type', $user_type);
    }

    public function storeUser(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'name' => 'required',
            'password' => 'required',
            'user_type' => 'required'
        ]);

        $corporate_customer = CorporateCustomer::find($id);

        try {
            $user_data = [
                'phone' => $request->phone,
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'sms' => 'আপনার রেজিস্ট্রেশন সফল হয়েছে, 
আপনার লগইন ইউজারনেম
ফোন নং: '.$request->phone.'
পাসওয়ার্ড: '.$request->password.'
টিম 
হেভিগাড়ী টেকনোলজিস লিমিটেড'
            ];

            $user = Sentinel::register($user_data);
            $role = Sentinel::findRoleBySlug('customer');
            $role->users()->attach($user);

            $user->status = 'active';
            $user->save();
            
            $profile = new CustomerProfile;
            $profile->user_id = $user->id;
            //$profile->profile_admin_id = $this->getFieldAdmin()->id;
            $profile->profile_complete = 'yes';
            $profile->token = uniqid();
            $profile->corporate_customer_id = $corporate_customer->id;
            $profile->save();

            $profile->completeProfile($request);

            $activation = Activation::create($user);
            $activation->code = mt_rand(1111, 9999);
            $activation->completed = 1;
            $activation->completed_at = date('Y-m-d H:i:s');
            $activation->save();

            $to = $user->phone;
            $ref_id = "$to"."_".time();
            //$status = SMSFacade::send($to, $user->sms, $ref_id);
            
            $user->sms = NULL;
            $user->save();

            //rel_corporate_customers
            $corporate_customer->customers()->attach($profile, array('user_type' => $request->user_type));
            
            return redirect('/cms/corporate_customers/'.$id.'/users');

        } catch (\Exception $e) {
            return redirect('/cms/corporate_customers/'.$id.'/users');
        }

        //$corporate_customer = CorporateCustomer::find($id);
    }

    protected function addToFullBookingPriceManager( $corporate_customer )
    {
        $full_booking_rates = FullBookingPriceManager::all();

        foreach ($full_booking_rates as $key => $booking_rate) {
            $corporate_full_booking = new CorporateFullBookingPriceManager();
            $corporate_full_booking->corporate_id = $corporate_customer->id;
            $corporate_full_booking->vehicle_type_id = $booking_rate->vehicle_type_id;
            $corporate_full_booking->base_fare = $booking_rate->base_fare;
            $corporate_full_booking->short_trip_rate = $booking_rate->short_trip_rate;
            $corporate_full_booking->up_trip_rate = $booking_rate->up_trip_rate;
            $corporate_full_booking->down_trip_rate = $booking_rate->down_trip_rate;
            $corporate_full_booking->long_up_trip_rate = $booking_rate->long_up_trip_rate;
            $corporate_full_booking->long_down_trip_rate = $booking_rate->long_down_trip_rate;
            $corporate_full_booking->surcharge_rate = $booking_rate->surcharge_rate;
            $corporate_full_booking->discount_percent = $booking_rate->discount_percent;
            $corporate_full_booking->from_discount_date = $booking_rate->from_discount_date;
            $corporate_full_booking->to_discount_date = $booking_rate->to_discount_date;
            $corporate_full_booking->admin_commission = $booking_rate->admin_commission;
            $corporate_full_booking->save();
        }
    }
}
