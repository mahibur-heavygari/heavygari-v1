<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use DB;
use App\Point;
use App\VehicleType;
use App\DriverProfile;
use App\OwnerProfile;
use App\CustomerProfile;
use App\Booking;
use App\Vehicle;
use App\Trip;
use App\User;
use App\HiredDriver;
use App\BaseProductCategory;
use App\Settings;
use App\PotentialOwner;
use App\SmsHistory;
use App\Events\FromAdmin\SendSmsAtAcceptBooking;
use App\Events\FromAdmin\BookingPriceChange;
use App\Events\FromAdmin\SendSmsAtCreateBookingByAdmin;
use App\Events\Booking\CancelBooking;
use App\MyLibrary\BookingLib\BookingManager;
use App\Http\Requests\Customer\FullBookingFormRequest;
use App\Http\Requests\Customer\FullBookingFareRequest;
use Illuminate\Support\Facades\Log;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\MyLibrary\SMSLib\SMSFacade;
use Sentinel;
use Carbon\Carbon;
use DateTime;

class BookingsController extends CmsPanelController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter') ? $request->input('filter') : 'open';

        $list = Booking::all()->where('status', $filter)->sortByDesc('created_at');

        $present_time_str = (string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka');

        return view('CMS.bookings.list')->with(['bookings' => $list])->with('filter', $filter)->with('present_time_str', $present_time_str)->with('adminPanel', true);
    }

    /**
     * Display the specified resource.
     */
    public function show($unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
       
        $expire_at_str = (string)date('Y-m-d H:i:s', strtotime($booking->created_at . ' +60 minutes'));
        $present_time_str = (string)(new Carbon(date("Y-m-d H:i:s")))->timezone('Asia/Dhaka');    
        $cancel_left_time = (int) round(abs(strtotime($expire_at_str) - strtotime($present_time_str)) / 60, 2);    

        return view('CMS.bookings.show')->with('booking', $booking)->with('cancel_left_time', $cancel_left_time)->with('adminPanel', true);
    }

    /**
     * Display the acceptance form
     */
    public function accept($unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->open()->first();

        // TODO : active drivers and vehicles?? 
        // driver vehicle assign system..

        $drivers = DriverProfile::all();

        $vehicles = Vehicle::all();

        $owners = OwnerProfile::all();
        return view('CMS.bookings.accept')->with('booking', $booking)->with('drivers',$drivers)->with('vehicles',$vehicles)->with('adminPanel', true)->with('owners', $owners);

    }

    /**
     * Accept the booking
     */
    public function acceptStore($unique_id, Request $request)
    {   
        if($request->is_hired){            
            $this->validate($request, [
                'owner' => 'required',
                'hired_driver' => 'required',
                'hired_driver_phone' => 'required'
            ]);
            if(!$request->is_short_vehicle){   
                $this->validate($request, [
                    'owner_vehicle' => 'required'
                ]);
            }
        }else{            
            $this->validate($request, [
                'driver' => 'required',
                'driver_phone' => 'required'
            ]);
        }

        if($request->is_short_vehicle){         
            $this->validate($request, [
                'number_plate' => 'required',
                'vehicle_name' => 'required'
            ]);
        }

        $booking = Booking::where('unique_id', $unique_id)->open()->first();

        if (!$booking) {
            abort(403, 'This is not an open booking.');
        }
        
        if($request->is_hired){    
            $owner = OwnerProfile::where('id', $request->owner)->first();
            $driver = DriverProfile::where('user_id', $owner->user->id)->first();
            if(is_null($driver)){
                $owner->makeMeDriver();
                $driver = DriverProfile::where('user_id', $owner->user->id)->first();
            }
            if($request->is_short_vehicle){ 
                $vehicleArr = [
                    'vehicle_type_id'=>$booking->fullBookingDetails->vehicleType->id,
                    'number_plate'=>$request->number_plate,
                    'name'=>$request->vehicle_name,
                    'authorized_drivers'=>[$driver->id]
                ];
                $vehicleReq = new Request($vehicleArr);
                $vehicle = Vehicle::addRepo($driver->myOwner, $vehicleReq);
            }else
                $vehicle = Vehicle::where('id', $request->owner_vehicle)->first();
        }else{            
            $user = User::where('phone', $request->driver_phone)->first();
            $driver = $user->driverProfile;
            if($request->is_short_vehicle){ 
                $vehicleArr = [
                    'vehicle_type_id'=>$booking->fullBookingDetails->vehicleType->id,
                    'number_plate'=>$request->number_plate,
                    'name'=>$request->vehicle_name,
                    'authorized_drivers'=>[$driver->id]
                ];
                $vehicleReq = new Request($vehicleArr);
                $vehicle = Vehicle::addRepo($driver->myOwner, $vehicleReq);

            }else
                if(isset($driver->currentVehicle)){
                    $vehicle = $driver->currentVehicle;
                }else{
                    $vehicle = $request->driver_selected_vehicle;
                    $vehicle = Vehicle::where('id',(int)$vehicle)->first();
                }
        }
       // checking full booking's vehicle type and driver's vehicle type
        
        if(($booking->booking_category=='full') && ($booking->fullBookingDetails->vehicle_type_id != $vehicle->vehicle_type_id)){
            $notification = ['danger', 'Driver\'s vehicle type does not match!'];
            session()->flash('message', $notification);
            return redirect('/cms/bookings/'.$unique_id.'/accept');
        }


            $accept = $booking->accept($driver, $vehicle); 
            $booking->accepted_by = 'admin';
            $booking->save();
        
            
            
        

        if($request->is_hired == 'true'){
            Booking::where('unique_id', $unique_id)->update(['is_hired_driver' => 'yes']);            
            HiredDriver::create([
                'name'=>$request->hired_driver, 
                'phone'=>$request->hired_driver_phone, 
                'booking_id'=>$booking->id, 
                'vehicle_owner_profile_id'=>$owner->id, 
                'driver_profile_id'=>$driver->id
            ]);
        }
        event(new SendSmsAtAcceptBooking($booking));

        return redirect('/cms/bookings?filter=upcoming');
    }

    /**
     * Start the trip
     */
    public function startTrip($unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        
        $start = $booking->start();

        $notification = ['success','Your trip has been started!'];
        session()->flash('message', $notification);

        return redirect('/cms/bookings?filter=ongoing');
    }

    /**
     * Complete the booking
     */
    public function completeTrip($unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        
        $complete = $booking->complete();

        $notification = ['success','Your trip has been completed!'];
        session()->flash('message', $notification);

        return redirect('/cms/bookings??filter=completed');
    }

    /*
    * Cancel the trip
    */
    public function cancelTrip($unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $booking_status = $booking->status;
        
        $complete = $booking->cancel();

        $booking->cancelled_by = 'Admin';
        $booking->save();

        $notification = ['success','Your trip has been cancelled!'];
        session()->flash('message', $notification);


        event(new CancelBooking($booking , $booking_status, 'admin'));

        return redirect('/cms/bookings??filter=cancelled');
    }

    public function createBooking()
    {
        $points = Point::all();

        $vehicle_types = VehicleType::where('title', 'Bus (Non AC)')->orWhere('title', 'Bus (AC)')->orWhere('title', 'XL Truck')->orWhere('title', 'Bus (Business Class)')->orWhere('title', 'Truck  (18feet)')->orWhere('title', 'Cover Truck  (22feet)')->orWhere('title', 'Trailer Truck 2XL  (20feet)')->orWhere('title', 'Trailer Truck 3XL  (40feet)')->orWhere('title', 'Pickup Van  (12feet)')->orWhere('title', 'Mini Pickup Van  (7feet)')->orderBy('preference', 'asc')->get();


        $capacity_types = [];
        foreach ($vehicle_types as $vehicle_type) {
            $capacity_types[$vehicle_type->title] = [
                'title' => $vehicle_type->capacityType->title,
                'capacity' => $vehicle_type->capacity,
            ];
        }

        return view('CMS.bookings.create')->with([
            'vehicle_types' => $vehicle_types,
            'capacity_types' => $capacity_types,
            'points' => $points,
        ]);
    }

    public function showEstimatedFare(Request $request)
    {
        $booking_fields = $request->all();

        // get fare
        $manager = new BookingManager('full');
        $price_breakdown = $manager->getFare($booking_fields);

        if (!$price_breakdown) {
            $notification = ['danger','Sorry, we are unable to provide a fare estimate for that booking.'];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $vehicle_type = VehicleType::where('id', $request->vehicle_type)->first();
        $route = $price_breakdown['route'];
        $customer['name'] = 'test name';
        $customer['phone'] = 'test phone';

        $customers = CustomerProfile::all();

        if ($vehicle_type->capacityType->title=='Person') {
            $product_categories = BaseProductCategory::where('category', 'ride_type')->orWhere('category', '=', NULL)->get();

        }else{
            $product_categories = BaseProductCategory::where('category', 'transport_type')->orWhere('category', '=', NULL)->get();
        }

        return view('CMS.bookings.add_details')->with([
            'route' => $route,
            'fare_breakdown' => $price_breakdown['fare'],
            'booking_fields' => $booking_fields,
            'vehicle_type' => $vehicle_type,
            'customer' => $customer,
            'product_categories' => $product_categories,
            'customers' => $customers

        ]);

    }

    public function storeBooking(Request $request)
    {
        if($request->is_short_customer){            
            $this->validate($request, [
                'short_customer_name' => 'required',
                'short_customer_phone' => 'required'
            ]);
        }else{            
            $this->validate($request, [
                'customer' => 'required'
            ]);
        }
        $password = "";

        if($request->is_short_customer){ 
            $user_check = User::where('phone', $request->short_customer_phone)->first();
            
            if(!is_null($user_check)){
                $notification = ['danger', 'Short customer phone already exists!'];
                session()->flash('message', $notification);
                return redirect('/cms/booking/create');
            }

            try{
                DB::beginTransaction();

                $password = substr($request->short_customer_phone, -6);

                $user_data = [
                    'phone' => $request->short_customer_phone,
                    'name' => $request->short_customer_name,
                    'password' => $password,
                    'status' => 'active'
                ];

                $user = Sentinel::register($user_data);
                $role = Sentinel::findRoleBySlug('customer');
                $role->users()->attach($user);

                $activation = Activation::create($user);
                $code = mt_rand(1111, 9999);
                $activation->code = $code;
                $activation->save();

                Activation::complete($user, $code);

                $customer = new CustomerProfile;
                $customer->user_id = $user->id;
                $customer->token = uniqid();
                $customer->profile_complete = 'no';
                $customer->save();

                DB::commit();

            } catch(\Exception $e) {

                if(isset($e->errorInfo[2]))
                    $message = $e->errorInfo[2];
                else
                    $message = 'There was an error. Booking was not created.';

                $notification = ['danger', $message];
                session()->flash('message', $notification);
                return redirect('/cms/booking/create');
            }
            $is_short_customer = true;

        }else{
            $customer = customerProfile::find($request->customer);
            $is_short_customer = false;
        }


        Log::info('[Customer #'.$customer->id.' (From Web) confirmed booking with this fields :'.var_export($request->all(), true));

        // do booking
        $manager = new BookingManager('full');
        $manager->setBookingCustomer($customer);
        $booking = $manager->createBooking($request->all());
        Booking::where('id', $booking->id)->update(['created_by'=>'admin']);

        if (!$booking) {
            $notification = ['danger','Sorry! Error occured during booking'];
            session()->flash('message', $notification);
            return redirect('/cms/booking/create');
        }

        //saving delivery order photo
        if ($request->file('delivery_order')) {
            $delivery_order_photo = $request->file('delivery_order')->store('public/booking_delivery_orders');
            $booking->update(['delivery_order_photo' => $delivery_order_photo]);
        }

        event(new SendSmsAtCreateBookingByAdmin($booking, $is_short_customer, $password));

        $notification = ['success','A booking was placed successfully.'];
        session()->flash('message', $notification);

        return redirect('/cms/bookings');
    }

    public function saveNote($unique_id, Request $request)
    {
        $booking = Booking::where('unique_id', $unique_id)->update(['admin_note'=>$request->admin_note]);
        $notification = ['success', 'Note save successfully.'];
        session()->flash('message', $notification);        
        return redirect('/cms/bookings/'.$unique_id);
    }

    public function authorizedVehicles(Request $request){
        $authorised_vehicle = $request->all();
        $user = User::where('phone', $authorised_vehicle)->first();
        $data = $user->driverProfile->myAuthorizedVehicles;
        if(isset($data)){
            return ['success' => true, 'data' => $data];
        }
    }

    public function changeDriver( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        
        $booking->vehicle->current_driver_profile_id = NULL;
        $booking->vehicle->status = 'available';
        $booking->vehicle->save();

        $booking->vehicle_id = NULL;
        $booking->driver_profile_id = NULL;
        $booking->vehicle_owner_profile_id = NULL;        
        $booking->accepted_at = NULL;    
        $booking->accepted_by = NULL;
        $booking->is_hired_driver = NULL;
        $booking->created_at = Carbon::now();
        $booking->save();

        $booking->status = 'open';
        $booking->save();

        HiredDriver::where('booking_id', $booking->id)->delete();

        return redirect('/cms/bookings/'.$unique_id.'/accept');  
    }
    
    public function makeOpen( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();

        if(isset($booking->vehicle)){            
            $booking->vehicle->current_driver_profile_id = NULL;
            $booking->vehicle->status = 'available';
            $booking->vehicle->save();
        }

        $booking->vehicle_id = NULL;
        $booking->driver_profile_id = NULL;
        $booking->vehicle_owner_profile_id = NULL;        
        $booking->accepted_at = NULL;    
        $booking->accepted_by = NULL;
        $booking->is_hired_driver = NULL;
        $booking->created_at = Carbon::now();
        $booking->status = 'open';
        $booking->msg_to_owners = 'no';
        $booking->save();

        HiredDriver::where('booking_id', $booking->id)->delete();

        $notification = ['success', 'Booking has been made open successfully.'];
        session()->flash('message', $notification); 
        return redirect('/cms/bookings/'.$unique_id);  
    }

    public function extendExpiry( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $booking->created_at = date("Y-m-d H:i:s");
        $booking->save();

        $notification = ['success', 'Booking expiry time has been extended for next 1 hour.'];
        session()->flash('message', $notification);        
        return redirect()->back();
    }

    public function priceUpdate( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();

        return view('CMS.bookings.price_update')->with('booking', $booking)->with('adminPanel', true);
    }

    public function savePriceUpdate( $unique_id, Request $request )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        
        $total_fare = (float) $request->total_fare;
        $discount = (float) $request->discount;
        $total_cost = (float) $request->total_cost;

        $heavygari_percent = (float) VehicleType::where('id', $booking->fullBookingDetails->vehicle_type_id)->first()->fullBookingRate->admin_commission; 
        $driver_comission_obj = Settings::where('key', 'driver_comission')->first();
        $driver_percent = (float)$driver_comission_obj->value;
        
        $owner_earning = round( ($total_fare * 100) / (100 + $heavygari_percent) );
        $heavygari_fee = round( $total_fare - $owner_earning );
        $driver_earning = round( $heavygari_fee * $driver_percent );     
        $heavygari_earning = round( $heavygari_fee - $driver_earning );

        if($booking->trips[0]->distance_type == 'normal' || $booking->trips[0]->distance_type == 'long'){
            $distance_cost = $total_fare - $heavygari_fee;
        }else if($booking->trips[0]->distance_type == 'short'){
            $distance_cost_with_base_and_waiting_cost = $total_fare - $heavygari_fee;
            $distance_cost_with_base = $distance_cost_with_base_and_waiting_cost - $booking->invoice->waiting_cost;
            $distance_cost = $distance_cost_with_base - $booking->invoice->base_fare;
        }

        $booking->earnings->update(['owner_earning'=>$owner_earning, 'driver_earning'=>$driver_earning, 'heavygari_earning'=>$heavygari_earning]);
        $booking->invoice->update(['heavygari_fee'=>$heavygari_fee, 'total_fare'=>$total_fare, 'discount'=>$discount, 'total_cost'=>$total_cost, 'distance_cost'=>$distance_cost]);

        $booking = Booking::where('unique_id', $unique_id)->first();
        event(new BookingPriceChange($booking));

        $notification = ['success', 'Booking price updated successfully.'];
        session()->flash('message', $notification);        
        return redirect('/cms/bookings/'.$unique_id);
    }

    public function msgToOwners( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $status = false;
        $phones = [];

        if($booking->booking_category=='full'){
            $vehicles = Vehicle::where('vehicle_type_id', $booking->fullBookingDetails->vehicle_type_id)->get();
            $vehicles = $vehicles->unique('vehicle_owner_profile_id');
            foreach ($vehicles as $vehicle) {
                if($vehicle->owner->user->status=='active'){
                    $phones[] = $vehicle->owner->user->phone;
                }

                if(($vehicle->owner->manager_phone != NULL) && ($vehicle->owner->manager_phone != 'NULL') && ($vehicle->owner->manager_phone != 'null') && ($vehicle->owner->user->status=='active')){
                    $phones[] = $vehicle->owner->manager_phone;
                }
            }
        }        
        
        if(count($phones) == 0){            
            $notification = ['danger','There are no owners for the specific vehicle type'];
            session()->flash('message', $notification);       
            return redirect('/cms/bookings/'.$unique_id);
        }
        $phonesCollection = collect($phones); 

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $msg_body = $src_point.' থেকে '.$destination_point.' ট্রিপ আছে, চাইলে কল করুন 01909222777';

        $status = SMSFacade::sendBulk($phonesCollection, $msg_body, 'owner');

        if (!$status) {
            $notification = ['danger','Sorry, Could not send the SMS'];
            session()->flash('message', $notification);       
            return redirect('/cms/bookings/'.$unique_id);
        }

        $booking->msg_to_owners = 'yes';
        $booking->save();

        $notification = ['success', 'Sms sent successfully to all owners and managers.'];
        session()->flash('message', $notification);        
        return redirect('/cms/bookings/'.$unique_id);
    }

    public function msgToDrivers( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $status = false;
        $phones = [];

        if($booking->booking_category=='full'){
            $vehicles = Vehicle::where('vehicle_type_id', $booking->fullBookingDetails->vehicle_type_id)->get();
            foreach ($vehicles as $vehicle) {
                foreach ($vehicle->authorizedDrivers as $key => $driver) {
                    if($driver->user->status=='active')
                        $phones[] = $driver->user->phone;
                }
            }
        }   
        
        if(count($phones) == 0){            
            $notification = ['danger','There are no drivers for the specific vehicle type'];
            session()->flash('message', $notification);       
            return redirect('/cms/bookings/'.$unique_id);
        }

        $phones = array_unique($phones);
        $phonesCollection = collect($phones); 

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $msg_body = $src_point.' থেকে '.$destination_point.' ট্রিপ আছে, চাইলে কল করুন 01909222777';

        $status = SMSFacade::sendBulk($phonesCollection, $msg_body, 'driver');

        if (!$status) {
            $notification = ['danger','Sorry, Could not send the SMS'];
            session()->flash('message', $notification);       
            return redirect('/cms/bookings/'.$unique_id);
        }

        $booking->msg_to_drivers = 'yes';
        $booking->save();

        $notification = ['success', 'Sms sent successfully to all drivers.'];
        session()->flash('message', $notification);        
        return redirect('/cms/bookings/'.$unique_id);
    }

    public function msgToPotentialOwners( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $vehicle_type_id = $booking->fullBookingDetails->vehicle_type_id;
        $status = false;
        $phones = [];
        $potential_owners = PotentialOwner::all();

        if($booking->booking_category=='full'){
            $potential_owners = $potential_owners->filter(function ($value, $key) use($vehicle_type_id){  
                $vehicle_type_exist = $value->myVehicleTypes->where('id', $vehicle_type_id)->first();
                if(!is_null($vehicle_type_exist)){
                    return $value;     
                }
            });
        }

        foreach ($potential_owners as $key => $potential_owner) {
            $phones[] = $potential_owner->owner_phone;
            if(($potential_owner->manager_phone != NULL) && ($potential_owner->manager_phone != 'NULL') && ($potential_owner->manager_phone != 'null')){
                    $phones[] = $potential_owner->manager_phone;
                }
        }

        if(count($phones) == 0){            
            $notification = ['danger','There are no potential owners for the specific vehicle type'];
            session()->flash('message', $notification);       
            return redirect('/cms/bookings/'.$unique_id);
        }
        $phonesCollection = collect($phones); 

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $msg_body = $src_point.' থেকে '.$destination_point.' ট্রিপ আছে, চাইলে কল করুন 01909222777';

        $status = SMSFacade::sendBulk($phonesCollection, $msg_body, 'potential_owner');
 
        if (!$status) {
            $notification = ['danger','Sorry, Could not send the SMS'];
            session()->flash('message', $notification);       
            return redirect('/cms/bookings/'.$unique_id);
        }

        $booking->msg_to_potential_owners = 'yes';
        $booking->save();

        $notification = ['success', 'Sms sent successfully to all potential owners and managers.'];
        session()->flash('message', $notification);        
        return redirect('/cms/bookings/'.$unique_id);
    }

    public function edit( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $booking->datetime_formated = $booking->datetime->format('d/m/Y H:i A');

        $points = Point::all();

        $vehicle_types = VehicleType::where('title', 'Bus (Non AC)')->orWhere('title', 'Bus (AC)')->orWhere('title', 'XL Truck')->orWhere('title', 'Bus (Business Class)')->orWhere('title', 'Truck  (18feet)')->orWhere('title', 'Cover Truck  (22feet)')->orWhere('title', 'Trailer Truck 2XL  (20feet)')->orWhere('title', 'Trailer Truck 3XL  (40feet)')->orWhere('title', 'Pickup Van  (12feet)')->orWhere('title', 'Mini Pickup Van  (7feet)')->orderBy('preference', 'asc')->get();


        $capacity_types = [];
        foreach ($vehicle_types as $vehicle_type) {
            $capacity_types[$vehicle_type->title] = [
                'title' => $vehicle_type->capacityType->title,
                'capacity' => $vehicle_type->capacity,
            ];
        }
        
        $booking->trips[0]->from_address = str_replace(array("\r\n", "\n"), " ", $booking->trips[0]->from_address);
        $booking->trips[0]->to_address = str_replace(array("\r\n", "\n"), " ", $booking->trips[0]->to_address);

        return view('CMS.bookings.edit')->with([
            'vehicle_types' => $vehicle_types,
            'capacity_types' => $capacity_types,
            'points' => $points,
            'booking' => $booking
        ]);
    }

    public function showEditedEstimatedFare(Request $request, $unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        $booking_fields = $request->all();

        // get fare
        $manager = new BookingManager('full');
        $manager->setBookingCustomer($booking->customer);
        $price_breakdown = $manager->getFare($booking_fields);

        if (!$price_breakdown) {
            $notification = ['danger','Sorry, we are unable to provide a fare estimate for that booking.'];
            session()->flash('message', $notification);
            
            return redirect()->back();
        }

        $price_change = $this->priceParameterChange( $request, $unique_id );

        if($price_change==FALSE){
            $price_breakdown['fare']['fare_breakdown']['discount'] = $booking->invoice->discount;
            $price_breakdown['fare']['fare_breakdown']['total_cost'] = $booking->invoice->total_cost;
            $price_breakdown['fare']['total_cost'] = $booking->invoice->total_cost;
            $price_breakdown['fare']['fare_breakdown']['total_fare'] = $booking->invoice->total_fare;
        }else{            
            if($booking->first_discounted=='yes'){
                $price_breakdown['fare']['fare_breakdown']['discount'] = 500;
                $price_breakdown['fare']['total_cost'] = $price_breakdown['fare']['fare_breakdown']['total_fare'] - 500;
                $price_breakdown['fare']['fare_breakdown']['total_cost'] = $price_breakdown['fare']['fare_breakdown']['total_fare'] - 500;
            }
        }

        $vehicle_type = VehicleType::where('id', $request->vehicle_type)->first();
        $route = $price_breakdown['route'];
        $customer_user = $booking->customer->user;
        $customer['name'] = $customer_user->name;
        $customer['phone'] = $customer_user->phone;
        $product_categories = BaseProductCategory::all();

        if ($vehicle_type->capacityType->title=='Person') {
            $product_categories = BaseProductCategory::where('category', 'ride_type')->orWhere('category', '=', NULL)->get();

        }else{
            $product_categories = BaseProductCategory::where('category', 'transport_type')->orWhere('category', '=', NULL)->get();   
        }

        $booking->special_instruction = str_replace(array("\r\n", "\n"), " ", $booking->special_instruction);

        return view('CMS.bookings.edit_details')->with([
            'route' => $route,
            'fare_breakdown' => $price_breakdown['fare'],
            'booking_fields' => $booking_fields,
            'vehicle_type' => $vehicle_type,
            'customer' => $customer,
            'booking' => $booking,
            'product_categories' => $product_categories
        ]);
    }

    public function confirmEdit(Request $request, $unique_id)
    {
        $booking = Booking::where('unique_id', $unique_id)->first();

        //update bookings table
        $booking->update($request->only('trip_type', 'booking_type', 'payment_by', 'recipient_name', 'recipient_phone', 'particular_details', 'waiting_time', 'special_instruction'));

        //update trips table
        $booking->trips[0]->update($request->only('from_lat', 'from_lon', 'from_point', 'from_address', 'to_point', 'to_address', 'to_lat', 'to_lon', 'duration', 'distance', 'distance_type'));

        //trigger event if price changed
        if((float)$booking->invoice->total_cost != (int)$request->total_cost){
            event(new BookingPriceChange($booking));
        }

        //update booking_invoices table
        $booking->invoice->update(['total_fare'=>$request->total_fare, 'discount'=>$request->discount, 'total_cost'=>$request->total_cost]);

        //update full_booking_details table
        $booking->fullBookingDetails->update(['vehicle_type_id'=>$request->vehicle_type, 'capacity'=>$request->capacity]);

        //update booking_earnings table
        $heavygari_percent = (float) VehicleType::where('id', $booking->fullBookingDetails->vehicle_type_id)->first()->fullBookingRate->admin_commission; 
        $driver_comission_obj = Settings::where('key', 'driver_comission')->first();
        $driver_percent = (float)$driver_comission_obj->value;
        
        $owner_earning = round( ($request->total_fare * 100) / (100 + $heavygari_percent) );
        $heavygari_fee = round( $request->total_fare - $owner_earning );
        $driver_earning = round( $heavygari_fee * $driver_percent );     
        $heavygari_earning = round( $heavygari_fee - $driver_earning );

        if($booking->trips[0]->distance_type == 'normal' || $booking->trips[0]->distance_type == 'long'){
            $distance_cost = $request->total_fare - $heavygari_fee;
        }else if($booking->trips[0]->distance_type == 'short'){
            $distance_cost_with_base_and_waiting_cost = $request->total_fare - $heavygari_fee;
            $distance_cost_with_base = $distance_cost_with_base_and_waiting_cost - $booking->invoice->waiting_cost;
            $distance_cost = $distance_cost_with_base - $booking->invoice->base_fare;
        }

        $booking->earnings->update(['owner_earning'=>$owner_earning, 'driver_earning'=>$driver_earning, 'heavygari_earning'=>$heavygari_earning]);

        //update booking_invoices table
        $booking->invoice->update(['heavygari_fee'=>$heavygari_fee, 'distance_cost'=>$distance_cost]);

        //for first time discount purpose
        $off_amount = (int)$request->off_amount;
        if($off_amount > 0){
            $booking->first_discounted = 'yes';
            $booking->save();

            $booking->customer->first_booking_discount = 500;
            $booking->customer->save();
        }
        
        $notification = ['success', 'Edited successfully.'];
        session()->flash('message', $notification);        
        return redirect('/cms/bookings/'.$unique_id);
    }

    private function priceParameterChange( $request, $unique_id )
    {
        $price_should_change = FALSE;
        $vehicle_type = (int) $request->vehicle_type;
        $trip_type = $request->trip_type;
        $from_point = (int) $request->from_point;
        $from_address = $request->from_address;
        $to_point = (int) $request->to_point;
        $to_address = $request->to_address;

        $booking = Booking::where('unique_id', $unique_id)->first();

        if($booking->fullBookingDetails->vehicle_type_id != $vehicle_type){
            $price_should_change = TRUE;
        }

        if($booking->trip_type != $trip_type){
            $price_should_change = TRUE;
        }

        if($booking->trips[0]->from_point != $from_point || $booking->trips[0]->from_address != $from_address || $booking->trips[0]->to_point != $to_point || $booking->trips[0]->to_address != $to_address){
            $price_should_change = TRUE;
        }

        return $price_should_change;
    }

    public function showDeliveryOrderPhoto( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        return view('CMS.bookings.delivery_order_photo')->with(['booking' => $booking]);
    }

    public function showDeliveryInvoicePhoto( $unique_id )
    {
        $booking = Booking::where('unique_id', $unique_id)->first();
        return view('CMS.bookings.delivery_invoice_photo')->with(['booking' => $booking]);
    }
}
