<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\CustomerRating;
use App\DriverRating;
use App\Booking;
use App\DriverProfile;
use App\CustomerProfile;
use Illuminate\Database\Eloquent\Model;
use App\User;


class RatingController extends CmsPanelController 
{
    

    public function customerRating()
    {
        $ratings = customerRating::get()->sortByDesc('created_at');
        return view('CMS.rating.customer_rating')->with( 'ratings', $ratings );
    }   

    public function driverRating()
    {   
        $ratings = DriverRating::get()->sortByDesc('created_at');
        return view('CMS.rating.driver_rating')->with( 'ratings', $ratings );
    }
  

}
