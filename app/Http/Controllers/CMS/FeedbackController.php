<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\UserFeedback;
use App\User;
use App\ContactMessage;
Use \Carbon\Carbon;
use App\Vehicle;
use App\Booking;
use App\Trip;
use App\Point;
use App\VehicleType;
use App\FullBookingDetails;
use DB;

class FeedbackController extends CmsPanelController 
{
    public function userFeedback()
    {
    	$feedbacks = UserFeedback::paginate(10);
    	return view('CMS.feedbacks.user_feedbacks')->with('feedbacks', $feedbacks);
    }

    public function publicFeedback()
    {
    	$feedbacks =  ContactMessage::paginate(10);

    	return view('CMS.feedbacks.public_feedbacks')->with('feedbacks', $feedbacks);

    }
    
}
