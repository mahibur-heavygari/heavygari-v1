<?php

namespace App\Http\Controllers\CMS;

class ProfileController extends CmsPanelController
{
	/**
	 * show profile page
	 */
    public function show()
    {
        return $this->adminProfile();
    }

    /**
	 * show settings page
	 */
    public function settings()
    {
        return $this->adminProfile();
    }
}
