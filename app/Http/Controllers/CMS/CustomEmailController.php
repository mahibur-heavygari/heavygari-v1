<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\CustomerProfile;
use App\OwnerProfile;
use App\DriverProfile;
use App\AuthorizedPoint;
use Illuminate\Support\Facades\Mail;
use App\Mail\PushEmail\PushEmail;
use App\Mail\PushEmail\OthersEmail;
use App\User;
use App\OtherEmail;
use App\EmailHistory;
use App\Events\FromAdmin\SendPushEmail;
use App\Events\FromAdmin\SendPushOtherEmail;
use App\PotentialOwner;
use App\PotentialCustomer;
use App\PotentialDriver;

class CustomEmailController extends CmsPanelController
{   public function sendEmail()
    {
        $filter = 'active';
        $customers = CustomerProfile::$filter()->get();
        $owners = OwnerProfile::$filter()->get();
        $owner_managers = OwnerProfile::$filter()->where('manager_email', '!=', NULL)->get();
        $drivers = DriverProfile::$filter()->get();
        $authorized_points = AuthorizedPoint::get();
        $other_customers = OtherEmail::where('user_type', 'customer')->get();
        $other_owners = OtherEmail::where('user_type', 'owner')->get();
        $other_drivers = OtherEmail::where('user_type', 'driver')->get();
        $potential_owners = PotentialOwner::all();
        $potential_customers = PotentialCustomer::all();
        $potential_drivers = PotentialDriver::all();

        foreach ($customers as $key => $value) {
            $value->email = $value->user->email;
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($owners as $key => $value) {
            $value->email = $value->user->email;
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($owner_managers as $key => $value) {
            $value->email = $value->manager_email;
            $value->phone = $value->manager_phone;
            $value->name = $value->manager_name;
        }
        foreach ($drivers as $key => $value) {
            $value->email = $value->user->email;
            $value->phone = $value->user->phone;
            $value->name = $value->user->name;
        }
        foreach ($authorized_points as $key => $value) {
            $value->email = $value->email;
            $value->name = $value->owner_name;
        }
        foreach ($other_customers as $key => $value) {
            $value->email = $value->email;
            
        }
        foreach ($other_owners as $key => $value) {
            $value->email = $value->email;
            
        }
        foreach ($other_drivers as $key => $value) {
            $value->email = $value->email;
            
        }
        foreach ($potential_owners as $key => $value) {
            $value->phone = $value->owner_phone;
            $value->name = $value->owner_name;
            $value->email = $value->owner_email;
        }
        foreach ($potential_customers as $key => $value) {
            $value->phone = $value->customer_phone;
            $value->name = $value->customer_name;
            $value->email = $value->customer_email;
        }
        foreach ($potential_drivers as $key => $value) {
            $value->phone = $value->driver_phone;
            $value->name = $value->driver_name;
            $value->email = $value->driver_email;
        }
        return view('CMS.custom_email.send')->with(['customers' => $customers, 
            'owners' => $owners, 
            'drivers' => $drivers, 
            'authorized_points' => $authorized_points, 
            'owner_managers' => $owner_managers, 
            'other_customers' => $other_customers,
            'other_owners' => $other_owners,
            'other_drivers' => $other_drivers, 
            'potential_owners' => $potential_owners, 
            'potential_customers' => $potential_customers, 
            'potential_drivers' => $potential_drivers ]);
    }

   public function sendEmailPost(Request $request)
   {    //dd($request->all());
        if($request->email == 'null'){
            if($request->other_email == 'null'){
                $notification = ['danger','No Email Available'];
                session()->flash('message', $notification);
                return redirect('/cms/send/email');
            }elseif(!isset($request->other_email)){
                $notification = ['danger','No Email Available'];
                session()->flash('message', $notification);
                return redirect('/cms/send/email');
            }
        }
        $this->validate($request, [
            'text' => 'required'
        ]);
        $send_multiple = false;
        if($request->user_type == 'not_defined'){
            $email_separate = trim(preg_replace('/\s+/', '', $request->email));
            $email_separate = explode(',', $email_separate);
            if(isset($email_separate)){
                foreach($email_separate as $key => $email){
                        $to=$email;
                        $msg = $request->text;
                        $ref_id = "$to"."_".time();
                        $title = $request->email_type;
                        $user=$to;
                    
                $Existing_email = User::where('email', $user)->first();
                if(isset($Existing_email)){
                    $notification = ['danger','This Email Address is Registered User Email'];
                    session()->flash('message', $notification);
                    return redirect('/cms/send/email');
                }else{
                    OtherEmail::updateOrCreate(['email' => $user, 'user_type' => $request->other_user_type]);  
                }   
                if ($request->image) {
                    $image = $request->image->store('public/image');
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }else{
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }
            }
        }
        if (isset($status)){
            EmailHistory::Create(['email_text' => $request->text, 'email' => $request->email, 'email_type' => $request->email_type, 'image' => $image, 'response' => $request->_token, 'other_email' =>$request->other_email, 'user_type' =>$request->user_type ]);
            $notification = ['success','Email was sent successfully'];
            session()->flash('message', $notification);
            return redirect('/cms/send/email');
            }else {
                $notification = ['danger','Sorry, Could not sent the Email'];
                session()->flash('message', $notification);
                return redirect('/cms/send/email');
            }
        }
        if($request->user_type == 'not_defined'){
            $to = $request->email;
            $Existing_email = User::where('email', $to)->first();
            if(isset($Existing_email)){
                $notification = ['danger','This Email Address is Registered User Email'];
                session()->flash('message', $notification);
                return redirect('/cms/send/email');
            }else{
                OtherEmail::updateOrCreate(['email' => $request->email, 'user_type' => $request->other_user_type]);  
            }
           
        }elseif($request->email == 'all' && $request->user_type == 'customer'){
            $to = CustomerProfile::with('user')->get()->pluck('user.email');
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'owner'){
            $to = OwnerProfile::with('user')->get()->pluck('user.email');
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'owner_manager'){
            $to = OwnerProfile::where('manager_email', '!=', NULL)->get()->pluck('manager_email');
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'driver'){
            $to = DriverProfile::with('user')->get()->pluck('user.email');
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'arp'){
            $to = AuthorizedPoint::get()->pluck('email');$to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->other_email == 'all' && $request->user_type == 'other_customers'){
            $to = OtherEmail::where('user_type', 'customer')->get();
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->other_email == 'all' && $request->user_type == 'other_drivers'){
            $to = OtherEmail::where('user_type', 'driver')->get();
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->other_email == 'all' && $request->user_type == 'other_owners'){
            $to = OtherEmail::where('user_type', 'owner')->get();
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'potential_owners'){
            $to = PotentialOwner::get();
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'potential_customers'){
            $to = PotentialCustomer::get();
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->email == 'all' && $request->user_type == 'potential_drivers'){
            $to = PotentialDriver::get();
            $to = $to->filter()->all();
            $to = array_unique($to);
            $send_multiple = true;
        }elseif($request->user_type == 'other_customers'){
            $to = $request->other_email;
        }elseif($request->user_type == 'other_owners'){
            $to = $request->other_email;
        }elseif($request->user_type == 'other_drivers'){
            $to = $request->other_email;
        }elseif( $request->user_type == 'potential_owners'){
             $email_separate = trim(preg_replace('/\s+/', '', $request->email));
            $email_separate = explode(',', $email_separate);
            if(isset($email_separate)){
                $email_separate = array_unique($email_separate);
                foreach($email_separate as $key => $email){
                        $to=$email;
                        $msg = $request->text;
                        $to = collect($to);
                        $ref_id = "$to"."_".time();
                        $title = $request->email_type;
                        $user=$to;
                    if ($request->image) {
                        $image = $request->image->store('public/image');
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                    }else{
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                    }
                }
            }
                    //$to = $request->email;
        }elseif( $request->user_type == 'potential_customers'){
            $email_separate = trim(preg_replace('/\s+/', '', $request->email));
            $email_separate = explode(',', $email_separate);
            if(isset($email_separate)){
                $email_separate = array_unique($email_separate);
                foreach($email_separate as $key => $email){
                        $to=$email;
                        $msg = $request->text;
                        $to = collect($to);
                        $ref_id = "$to"."_".time();
                        $title = $request->email_type;
                        $user=$to;
                    if ($request->image) {
                        $image = $request->image->store('public/image');
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                    }else{
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                    }
                }
            }
            //$to = $request->email;
        }elseif( $request->user_type == 'potential_drivers'){
            $email_separate = trim(preg_replace('/\s+/', '', $request->email));
            $email_separate = explode(',', $email_separate);
            if(isset($email_separate)){
                $email_separate = array_unique($email_separate);
                foreach($email_separate as $key => $email){
                        $to=$email;
                        $msg = $request->text;
                        $to = collect($to);
                        $ref_id = "$to"."_".time();
                        $title = $request->email_type;
                        $user=$to;
                    if ($request->image) {
                        $image = $request->image->store('public/image');
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                    }else{
                        $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                    }
                }
            }
            // $to = $request->email;
        }else{
            $to = $request->email;
        }
        $msg = $request->text;
        $to = collect($to);
        $ref_id = "$to"."_".time();
        if($send_multiple == false){
            if($request->user_type == 'not_defined'){
                $user=$to;
            }elseif($request->user_type == 'other_customers'){
                $user=$to;
            }elseif($request->user_type == 'other_owners'){
                $user=$to;
            }elseif($request->user_type == 'other_drivers'){
                $user=$to;
            }elseif($request->user_type == 'potential_owners'){
                $user=$to;
            }elseif($request->user_type == 'potential_customers'){
                $user=$to;
            }elseif($request->user_type == 'potential_drivers'){
                $user=$to;
            }
            else{
                $user = User::where('email', $to)->get();
            }
        }
        else{
            if($request->user_type == 'other_owners' || $request->user_type == 'other_customers' || $request->user_type == 'other_drivers'){
                if($request->user_type == 'other_owners'){
                    $user_type = 'owner';
                }elseif($request->user_type == 'other_drivers'){
                    $user_type = 'driver';
                }else{
                    $user_type = 'customer';
                }
                $user = OtherEmail::where('user_type', $user_type)->get();
            }elseif($request->user_type == 'potential_owners'){
                $user= PotentialOwner::get();
            }elseif($request->user_type == 'potential_customers'){
                $user= PotentialCustomer::get();
            }elseif($request->user_type == 'potential_drivers'){
                $user= PotentialDriver::get();
            }else{
                $user= User::whereIn('email',  $to )->get();
            }
        }
        $title = $request->email_type;
        if ($request->image) {
            $image = $request->image->store('public/image');

            if($send_multiple == false){
                if($request->user_type == 'not_defined'){
                   $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }elseif($request->user_type == 'other_customers'){
                   $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title)); 
                }elseif($request->user_type == 'other_owners'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }elseif($request->user_type == 'other_drivers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }elseif($request->user_type == 'potential_owners'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }elseif($request->user_type == 'potential_customers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }elseif($request->user_type == 'potential_drivers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
                }
                else{
                    $status = Mail::to($user)->send(new PushEmail($user, $msg, $image, $title)); 
                }
            }else{
                if($request->user_type == 'other_owners' || $request->user_type == 'other_customers' || $request->user_type == 'other_drivers'){
                    if($request->user_type == 'other_owners'){
                        $user_type = 'owner';
                    }elseif($request->user_type == 'other_drivers'){
                        $user_type = 'driver';
                    }else{
                        $user_type = 'customer';
                    }
                    foreach ($user as $key => $user) {
                    $status = event(new SendPushOtherEmail($user, $msg, $image, $title));
                    }
                }elseif($request->user_type == "potential_owners"){
                    foreach ($user as $key => $user) {
                    $user = $user->owner_email;
                    $status = event(new SendPushOtherEmail($user, $msg, $image, $title));
                    }

                }elseif($request->user_type == "potential_customers"){
                    foreach ($user as $key => $user) {
                    $user = $user->customer_email;
                    $status = event(new SendPushOtherEmail($user, $msg, $image, $title));
                    }

                }elseif($request->user_type == "potential_drivers"){
                    foreach ($user as $key => $user) {
                    $user = $user->driver_email;
                    $status = event(new SendPushOtherEmail($user, $msg, $image, $title));
                    }

                }else{
                    foreach ($user as $key => $user) {
                    $status = event(new SendPushEmail($user, $msg, $image, $title));
                    }
                }          
            }
        }else{
            if($send_multiple == false){
                if($request->user_type == 'not_defined'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }elseif($request->user_type == 'other_customers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }elseif($request->user_type == 'other_owners'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }elseif($request->user_type == 'other_drivers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }elseif($request->user_type == 'potential_owners'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }elseif($request->user_type == 'potential_customers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }elseif($request->user_type == 'potential_drivers'){
                    $status = Mail::to($user)->send(new OthersEmail($user, $msg, $image=false, $title));
                }
                else{
                    $status = Mail::to($user)->send(new PushEmail($user, $msg, $image=false, $title));
                }
            }else{ 
                if($request->user_type == 'other_owners' || $request->user_type == 'other_customers' || $request->user_type == 'other_drivers'){
                if($request->user_type == 'other_owners'){
                    $user_type = 'owner';
                }elseif($request->user_type == 'other_drivers'){
                    $user_type = 'driver';
                }else{
                    $user_type = 'customer';
                }
                foreach ($user as $key => $user){
                $status = event(new SendPushOtherEmail($user, $msg, $image=false, $title));
                }
            }elseif($request->user_type == "potential_owners"){
                    foreach ($user as $key => $user) {
                            $user = $user->owner_email;
                            $status = event(new SendPushOtherEmail($user, $msg, $image=false, $title));
                    }

            }elseif($request->user_type == "potential_customers"){
                    foreach ($user as $key => $user) {
                            $user = $user->customer_email;
                            $status = event(new SendPushOtherEmail($user, $msg, $image=false, $title));
                    }

            }elseif($request->user_type == "potential_drivers"){
                    foreach ($user as $key => $user) {
                            $user = $user->driver_email;
                            $status = event(new SendPushOtherEmail($user, $msg, $image=false, $title));
                    }

            }else{
                foreach ($user as $key => $user) {
                    $status = event(new SendPushEmail($user, $msg, $image=false, $title));
                }
                    
                }
            }
        }
        if (isset($status)){
            EmailHistory::Create(['email_text' => $request->text, 'email' => $request->email, 'email_type' => $request->email_type, 'image' => $image, 'response' => $request->_token, 'other_email' =>$request->other_email, 'user_type' =>$request->user_type ]);
            $notification = ['success','Email was sent successfully'];
            session()->flash('message', $notification);
            return redirect('/cms/send/email');
        }else {
            $notification = ['danger','Sorry, Could not sent the Email'];
            session()->flash('message', $notification);
            return redirect('/cms/send/email');
        }
    }

    public function getAllEmail(){
        $all_email = EmailHistory::where('user_type', '!=', NULL)->where('email_type','!=','Business Email')->get()->sortByDesc('created_at');
        return view('CMS.custom_email.history')->with(['all_email' => $all_email]);
    }

    public function getBusinessEmail(){
        $all_email = EmailHistory::where('user_type', '!=', NULL)->where('email_type','Business Email')->get()->sortByDesc('created_at');
        return view('CMS.custom_email.business_email_history')->with(['all_email' => $all_email]);
    } 
}
