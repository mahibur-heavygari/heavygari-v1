<?php

namespace App\Http\Controllers;

use App\User;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MyLibrary\UserLib\Login\DashboardLogin;
use App\MyLibrary\UserLib\Verification\PhoneVerification;

class SessionController extends Controller
{
	/**
	 * login form
	 */
    public function create()
    {
    	return view('auth.login');
    }

    /**
	 * login user
	 */
    public function store(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required',
            'password' => 'required',
        ]);

        $phone = $request->phone;
        $pass = $request->password;

        $login_manager = new DashboardLogin();
        $user = $login_manager->login($phone,$pass);

        if ($user) {
            // redirect to high priority role's panel 
            $user_roles = $user->roles;
            $dashboard = $this->getUserDashboard($user);
            
            return redirect($dashboard);
        } else {
            $error = $login_manager->getError();
            
            if ($error['type']=='incomplete_profile') {
                // user is logged in..
                return redirect($error['user_type'].'/register/'.$error['profile_id'].'/profile/complete?token='.$error['token']);
            } else if ($error['type']=='unverified_phone') {
                // verify phone page
                return redirect($error['user_type'].'/register/'.$error['profile_id'].'/verify-phone');
            } else {
                // inactive user or incorrect user/pass
                $notification = ['danger',$error['message']];
                session()->flash('message', $notification);

                return redirect()->back();
            }
        }
    }

    /**
     * resened verification code
    */
    public function resendVerification(User $user)
    {
        $activation = new PhoneVerification($user);
        
        $activation->resend();

        $notification = ['success','Please check your sms!'];
        session()->flash('message', $notification);

        return redirect()->back();
    }

    /**
     * redirect to dashboard from website
     */
    public function redirectToDashBoard()
    {
        $user = Sentinel::getUser();
        $dashboard = $this->getUserDashboard($user);
            
        return redirect($dashboard);
    }

    /**
     * logout the user
     */
    public function logout()
    {
        Sentinel::logout();

        return redirect('/session/login');
    }

    /**
     * get user's highest priority role's panel 
     */
    private function getUserDashboard($user)
    {
        $adminRole = Sentinel::findRoleBySlug('super-admin');
        $ownerRole = Sentinel::findRoleBySlug('owner');
        $customerRole = Sentinel::findRoleBySlug('customer');

        if ($user->inRole($adminRole)) {
            $dashboard = 'cms/dashboard';
        } elseif ($user->inRole($ownerRole)) {
            $dashboard = 'owner/panel/dashboard';
        } elseif ($user->inRole($customerRole)) {
            $dashboard = 'customer/panel/dashboard';
        }

        return $dashboard;
    }

    /**
     * get current user and his roles
    public function currentUser()
    {
        $user = Sentinel::getUser();
        return $user;
    }
    */
}
