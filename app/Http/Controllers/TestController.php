<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

use App\User;
use App\CustomerProfile;

use App\Mail\Registration\WelcomeCustomerEmail;
use App\Mail\Booking\BookingDeliveredEmail;
use App\Mail\Booking\BookingAcceptedEmail;
use App\Mail\Booking\NotifyOwnerTripCompleteEmail;
use App\Mail\Invoice\OwnerMonthlyInvoiceEmail;
use Illuminate\Support\Facades\Redis;

use App\MyLibrary\SMSLib\SMSFacade;
use App\MyLibrary\InvoiceLib\InvoiceSystem;
use App\MyLibrary\Firebase\PushNotification;
use App\MyLibrary\Firebase\WebPushNotification;
use App\MyLibrary\UserLib\Registration\OwnerRegistration;
use App\MyLibrary\UserLib\Verification\EmailVerification;
use App\MyLibrary\UserLib\Verification\PhoneVerification;
use App\MyLibrary\DriverCommission\CommissionSystem;

use App\Jobs\TestQueueJob;
use App\Jobs\GenerateInvoiceForAOwner;
use App\Events\Owner\OwnerAccountVerified;
use App\Events\Booking\TripComplete;
use App\Booking;

use Sentinel;
use Carbon\Carbon;
use GuzzleHttp;
use PDF;
use Storage;

class TestController extends Controller
{
    public function createCommission()
    {
        //$invoice = InvoiceSystem::create('325', '01', '2019');
        //$commission = CommissionSystem::create('325', '01', '2019');
        dd("stop it");
        $owners = \App\OwnerProfile::all();

        foreach ($owners as $owner) {
            $this->invoiceJob($owner);
        }
        dd("okk");
    }

    private function invoiceJob ($owner)
    {
        $invoice = InvoiceSystem::create($owner->id, date('m'), date('Y'));
    }

    public function test()
    {
        $pass = \Hash::make('1/2A1hG#techLTD');
        dd($pass);
    }

    public function pdfTest($unique_id)
    {
        $booking = \App\Booking::where('unique_id', $unique_id)->first();
        return view('emails.booking.invoice')->with('booking', $booking);

        $pdf_link = 'public/booking_invoices/invoice_'.$booking->unique_id.'.pdf';
        $data['booking'] = $booking;
        $pdf = PDF::loadView('emails.booking.invoice', $data);

        Storage::put($pdf_link, $pdf->output()); 
    

        // from view
        /*
        $html = View::make('emails.booking.invoice', $data)->render();
        $pdf = PDF::loadHTML($html);
        return $pdf->save();
        */

        //direcl url
        //return PDF::loadFile('http://www.github.com')->inline('github.pdf');

        return view('emails.booking.invoice')->with('booking', $booking);
    }

    public function sendEmail(Request $request)
    {
        $customer = \App\CustomerProfile::where('id', '231')->first();
        $booking = \App\Booking::where('id', '90')->first();
        //dd($booking);


        //event(new TripComplete($booking));
    
        //return view('emails.booking.notify_owner')->with('booking', $booking);
        return view('emails.booking.delivered')->with('booking', $booking);

        $status = Mail::to($booking->customer->user)->send(new BookingDeliveredEmail($booking));
        dd($status);
    }

    public function sendSMS() 
    {
        //$to = '8801993174775';
        $to = '8801765349536';
        $msg = 'this is a বাংলা test';
        $ref_id = rand();
        
        $result = SMSFacade::send($to, $msg, $ref_id);

        dd($result);
    }

    public function sendPush($booking_unique_id, $type, $token) 
    {
        $fcm_registration_token = $token;

        $push = new PushNotification();

        if ($type=='start-trip') {

            $msg = 'আপনার দেয়া পরিবহন অনুরোধ, চালকঃ xxx এবং গাড়ী নিবন্ধন নংঃ yyy দ্বারা গৃহীত হয়েছে (বুকিং আইডিঃ zzz) ';

            $message = [
                'notification' => [
                    'title' => 'Trip Started!',
                    'body' => $msg,
                ],
                'data' => [
                    'id' => 0,
                    'unique_id' => $booking_unique_id,
                    'category' => 'booking',
                    'type' => 'ongoing'
                ]
            ];

        } else if ($type=='complete-trip') {

            $message = [
                'notification' => [
                    'title' => 'Trip Completed!',
                    'body' => 'Booking #'.$booking_unique_id.' : Driver has delivered the good.',
                ],
                'data' => [
                    'id' => 0,
                    'unique_id' => $booking_unique_id,
                    'category' => 'booking',
                    'type' => 'completed'
                ]
            ];

        }

        // TODO : add custom values for android, iOS

        $test = $push->sendToSingleDevice($fcm_registration_token, $message);

        dd($message);
    }

    public function sendWebPush($booking_unique_id, $type, $token) 
    {
        $fcm_registration_token = $token;

        $push = new WebPushNotification();

        if ($type=='start-trip') {

            $msg = 'আপনার দেয়া পরিবহন অনুরোধ, চালকঃ xxx এবং গাড়ী নিবন্ধন নংঃ yyy দ্বারা গৃহীত হয়েছে (বুকিং আইডিঃ zzz) ';

            $message = [
                'notification' => [
                    'title' => 'Trip Started!',
                    'body' => $msg,
                ],
                'data' => [
                    'id' => 0,
                    'unique_id' => $booking_unique_id,
                    'category' => 'booking',
                    'type' => 'ongoing'
                ]
            ];

        } else if ($type=='complete-trip') {

            $message = [
                'notification' => [
                    'title' => 'Trip Completed!',
                    'body' => 'Booking #'.$booking_unique_id.' : Driver has delivered the good.',
                ],
                'data' => [
                    'id' => 0,
                    'unique_id' => $booking_unique_id,
                    'category' => 'booking',
                    'type' => 'completed'
                ]
            ];

        }

        // TODO : add custom values for android, iOS

        $test = $push->sendToSingleBrowser($fcm_registration_token, $message);

        dd($message);
    }

    public function testQueue() 
    {
        $now = Carbon::now('Asia/Dhaka');

        dispatch(new TestQueueJob($now));

        dd('I think it\'s working fine. check the log file. time : '. $now);
    }

    public function invoiceTest($owner_id)
    {
        //create invoice
        $owner = \App\OwnerProfile::findOrFail($owner_id)->first();

        //dispatch(new GenerateInvoiceForAOwner($owner));
        //dd($invoice);

        // show invoice
        $invoice = InvoiceSystem::show('1');

        // view tempalte
        return view('emails.owner_invoice.pdf')->with('invoice', $invoice);
    }

    public function fileSystem(Request $request)
    {
        //$file = Storage::url('MYDIRECTORY/test.png');
        //dd($file);
        return view('debugging.filesystem');
    }

    public function fileSystemStore(Request $request)
    {
        if ($request->file('file')) {
            
            $path = $request->file('file')->store(
                'mydirectory', 'public'
            );

            dd($path);
        }
    }

    /**
     * Create a demo trip for tracking testing
     */
    public function demoTripBuilder($demo_trip_id)
    {
        return view('debugging.trip_builder')->with('demo_trip_id', $demo_trip_id);
    }

    /**
     * Display the specified booking.
     */
    public function demoTracker($demo_trip_id)
    {
        //TODO :: Authorization
        $booking = Booking::where('unique_id',$demo_trip_id)->first();
        if ($booking) {
            dd('Sorry, You can not use real booking id!');
        }

        $trip_redis_data = Redis::zrange(config('heavygari.tracker.redis_db_prefix').':trip:trackInfo:'.$demo_trip_id, 0, -1);

        $trip_path_history = array_map( function($val){ return json_decode($val); }, $trip_redis_data);

        // if trip is in the middle, get old path
            // center = last lat,long
        // else , center = start of the pickup

        return view('debugging.demo')->with('demo_trip_id', $demo_trip_id)->with('trip_path_history', $trip_path_history);
    }
}
