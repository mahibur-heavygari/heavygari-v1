<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    /**
     * Display the specified page.
     */
    public function show($slug)
    {
    	if (view()->exists('website.pages.'.$slug)) {
		    return view('website.pages.'.$slug);
		} else {
			abort(404);
		}
    }

}
