<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PushNotification;
use App\User;
use Sentinel;
use Carbon\Carbon;

class NotificationBarController extends Controller
{

    public function getNotifications(Request $request){
        if($request->user_type=='admin'){
            $notifications = PushNotification::where('admin_notification', 'no')->orderBy('id', 'desc')->get();
            $notifications = $notifications->reject(function ($value, $key) {
                if ($value->type=='new' && $value->received_user_id!=0) {
                    return $value;
                }
            });
            foreach ($notifications as $key => $value) {
                $body = str_replace("আপনার দেয়া ", "", $notifications[$key]['body']);
                $notifications[$key]['body'] = $body;
                $body = str_replace("আপনার ", "", $notifications[$key]['body']);
                $notifications[$key]['body'] = $body;
                $title = str_replace("আপনার ", "", $notifications[$key]['title']);
                $title = 'একটি '.$title;
                $notifications[$key]['title'] = $title;
            }
            $notifications = $notifications->unique('body');

            $notifications = $notifications->filter(function ($item) {
                return $item['title']!='একটি ট্রিপ গৃহীত হয়েছে';
            })->values();

            foreach ($notifications as $key => $value) {
                $title = str_replace("বুকিং", "ট্রিপ", $notifications[$key]['title']);
                $notifications[$key]['title'] = $title;
            }

        }elseif($request->user_type=='customer'){
        	$user = Sentinel::getUser();
            $notifications = PushNotification::where('received_user_id', $user->id)->orderBy('id', 'desc')->take(10)->get();
        }elseif($request->user_type=='owner'){
        	$user = Sentinel::getUser();
            $owner_driver_ids[] = $user->id;
            foreach ($user->ownerProfile->myDrivers as $key => $driver) {
                $owner_driver_ids[] = $driver->user_id;
            }

            $notifications = PushNotification::whereIn('received_user_id', $owner_driver_ids)->orderBy('id', 'desc')->take(10)->get();
        }
        $output = '';
 
        if(count($notifications) > 0)
        {
            foreach ($notifications as $key => $row) {
                if($request->user_type=='admin' && $row->admin_read == 0)
                    $background_color = '--unread';
                elseif($request->user_type=='customer' && $row->customer_read == 0)
                    $background_color = '--unread';
                elseif($request->user_type=='owner' && $row->owner_read == 0)
                    $background_color = '--unread';
                else
                    $background_color = '';
                $admin_notification = $row->admin_notification;

                $output .= '
                   <li class="single-notice" data-is_admin="'.$admin_notification.'" data-id="'.$row->id.'" data-unique_id="'.$row->unique_id.'">
                        <a href="javascript:void(0)">
                            <div class="notification-item '.$background_color.'">
                                <h6 class="notification-title">'.$row["title"].'</h6>
                                <span class="notification-note">
                                    '.$row["body"].'
                                </span>
                                <h6 style="text-align:right; font-size:12px;">'. $row->created_at->diffForHumans() .'</h6>
                            </div>
                            
                        </a>
                   </li>

                   ';
                }
            }
        else
        {
            $output .= '<li><a href="#" style="color: #454545;" class="text-bold text-italic">No Notification Found</a></li>';
        }

        if($request->bar_icon_clicked !="" && $request->user_type=='admin'){
            PushNotification::where('admin_seen', 0)->update(['admin_seen'=>1]);
        }elseif($request->bar_icon_clicked !="" && $request->user_type=='customer'){
            PushNotification::where('received_user_id', $user->id)->where('customer_seen', 0)->update(['customer_seen'=>1]);
        }elseif($request->bar_icon_clicked !="" && $request->user_type=='owner'){
            PushNotification::whereIn('received_user_id', $owner_driver_ids)->where('owner_seen', 0)->update(['owner_seen'=>1]);
        }

        if($request->user_type=='admin'){
	        //$count_unseen = PushNotification::where('admin_seen', 0)->where('admin_notification', 'no')->count();
            $notifications = PushNotification::where('admin_notification', 'no')->where('admin_seen', 0)->get();
            $notifications = $notifications->reject(function ($value, $key) {
                if ($value->type=='new' && $value->received_user_id!=0) {
                    return $value;
                }
            });
            $count_unseen = $notifications->count();
	    }elseif($request->user_type=='customer'){
	        $count_unseen = PushNotification::where('received_user_id', $user->id)->where('customer_seen', 0)->count();
	    }elseif($request->user_type=='owner'){
	        $count_unseen = PushNotification::whereIn('received_user_id', $owner_driver_ids)->where('owner_seen', 0)->count();
	    }

        $data = array(
            'notification'   => $output,
            'unseen_notification' => $count_unseen
        );

        return ['success' => true, 'data' => $data];
    }

    public function changeNotificationReadStatus(Request $request){
        $notification_id = $request->notification_id;
        if($request->user_type=='admin'){
        	$update = PushNotification::where('id', $notification_id)->update(['admin_read'=>1]);
        }
        if($request->user_type=='customer'){
        	$update = PushNotification::where('id', $notification_id)->update(['customer_read'=>1]);
        }
        if($request->user_type=='owner'){
        	$update = PushNotification::where('id', $notification_id)->update(['owner_read'=>1]);
        }
        if( $update )
            return ['success' => true];
    }
}