<?php

namespace App\Http\Middleware;

use Closure;

use Sentinel;

class CompleteProfile
{
    /**
     * if account his not activated, user will not be able to browse.
     * If any user did not completed his profile yet, he will be 
     * redirected to complete profile page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $user = Sentinel::getUser();

        // Mobile verifcation?

        if ($user->status!='active') {
            $notification = ['danger','Sorry! Your account is not active. Please contact Admin.'];
            session()->flash('message', $notification);

            return redirect('session/login');
        }

        if ($role!=null) {
            $requested_role = Sentinel::findRoleBySlug($role);
            $role_profile = $role.'Profile';
            if ($user->$role_profile->profile_complete == 'no') {
                return redirect($role.'/register/'.$user->$role_profile->id.'/profile/complete');
            }
        }

        return $next($request);
    }
}
