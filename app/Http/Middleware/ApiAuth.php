<?php

namespace App\Http\Middleware;

use Closure;

use Sentinel;

use Auth;

class ApiAuth
{
    /**
     * Authentication (role based) for api calls
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            return response()->json(['error' => [
                'message' => 'Unauthenticated',
                'type' => null,
                'error_details' => null,
            ]], 401);
        }

        if ($role!=null) {
            $requested_role = Sentinel::findRoleBySlug($role);
            if (!$user->inRole($requested_role)) {
                return response()->json(['error' => [ 
                    'message' => 'You are not a '.$role,
                    'type' => null,
                    'error_details' => null,
                ]], 401);
            }
        }

        return $next($request);
    }
}
