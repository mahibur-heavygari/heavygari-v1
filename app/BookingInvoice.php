<?php

namespace App;

class BookingInvoice extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id', 'pickup_charge', 'dropoff_charge', 'base_fare', 'distance_cost', 'weight_cost', 'surcharge', 'waiting_cost', 'heavygari_fee', 'total_fare', 'discount', 'total_cost'
    ];

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'pickup_charge'=> (float) $this->pickup_charge,
            'dropoff_charge'=> (float) $this->dropoff_charge,            
            'base_fare'=> (float) $this->base_fare,
            'distance_cost'=> (float) $this->distance_cost,
            'weight_cost'=>(float) $this->weight_cost,
            'surcharge'=> (float) $this->surcharge,
            'total_fare'=> (float) $this->total_fare,
            'discount'=> (float) $this->discount,
            'total_cost'=> (float) $this->total_cost
        ];
    }
}
