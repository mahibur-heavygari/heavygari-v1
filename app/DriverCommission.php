<?php

namespace App;

class DriverCommission extends BaseModel
{
    protected $table = 'driver_commissions';

    public function commissionItems()
    {
        return $this->hasMany('App\DriverCommissionItem', 'driver_commission_id');
    }

    /*
    * Driver invoice belongs to an owner invoice
    */
    public function ownerInvoice()
    {
        return $this->belongsTo('App\OwnerInvoice');
    }

    public function owner()
    {
        return $this->belongsTo('App\OwnerProfile', 'owner_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    public function getMonthAttribute($value)
    {
        return date("F", mktime(0, 0, 0, $value, 1));
    }
}
