<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class BaseModel extends Model
{
    use SoftDeletes;

    /////////////////////////////////
    // Convert UTC time to BD time //
    /////////////////////////////////
    
    /**
     * Convert UTC time to BD time
     * override laravel's asDateTime() method
     * Problem : This will override on everywhere including insertion in db
    
    public function asDateTime($value)
    {
        return (parent::asDateTime($value))->timezone('Asia/Dhaka');
    }
    */
   
    // created at
    public function getCreatedAtAttribute($value)
    {   
        if ($value==null) {
            return $value;
        }

        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }

    // updated at
    public function getUpdatedAtAttribute($value)
    {   
        if ($value==null) {
            return $value;
        }
        
        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }
}
