<?php

namespace App;

class AdminProfile extends BaseModel
{
    protected $table = 'profile_admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'about',
    ];

    ////////////////
    // RELATIONS  //
    ////////////////
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function customersByMe()
    {
        return $this->hasMany('App\CustomerProfile', 'profile_admin_id');
    }

    public function ownersByMe()
    {
        return $this->hasMany('App\OwnerProfile', 'profile_admin_id');
    }

    public function driversByMe()
    {
        return $this->hasMany('App\DriverProfile', 'profile_admin_id');
    }

    public function vehiclesByMe()
    {
        return $this->hasMany('App\Vehicle', 'profile_admin_id');
    }

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'address' => $this->user->address,
            'about' => $this->about,
            'photo' => url(\Storage::url($this->user->photo)),
            'thumb_photo' =>  url(\Storage::url($this->user->thumb_photo)),
            'national_id' => $this->user->national_id,
            'national_id_photo' => url(\Storage::url($this->user->national_id_photo)),
            'status' => $this->user->status,
            'mobile_verified' => true,
            'fcm_registration_token' => $this->user->fcm_registration_token,
            'created_at' => $this->user->created_at->toDateTimeString(),
            'updated_at' => $this->user->updated_at->toDateTimeString(),
        ];

    }
}
