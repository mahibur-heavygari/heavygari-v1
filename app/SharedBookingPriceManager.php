<?php

namespace App;

class SharedBookingPriceManager extends BaseModel
{
    protected $table = 'shared_booking_price_manager';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'min_weight', 'max_weight', 'base_fare', 'pickup_charge', 'drop_off_charge', 'weight_rate', 'shortest_trip_rate', 'short_trip_rate', 'normal_trip_rate',
        'long_trip_rate', 'surcharge_rate', 'discount_percent', 'admin_commission'
    ];

}
