<?php

namespace App;

class PotentialCustomer extends BaseModel
{
    protected $table = 'potential_customers';

    protected $fillable = [
         'customer_name', 'company_name', 'customer_phone', 'customer_email','customer_address', 'customer_area'
    ];

    public function updateProfile($request)
    {
        $this->update($request->only('customer_name','company_name', 'customer_phone', 'customer_email','customer_address', 'customer_area'));
    }

}