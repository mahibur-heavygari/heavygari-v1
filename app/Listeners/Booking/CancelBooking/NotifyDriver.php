<?php

namespace App\Listeners\Booking\CancelBooking;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Booking\BookingAcceptedEmail;
use App\Events\Booking\CancelBooking;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\PushNotification as PushNotificationModel;
use App\MyLibrary\SMSLib\SMSFacade;

class NotifyDriver implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(CancelBooking $event)
    {
        $booking = $event->booking;
        $status = $event->status;
        $send_by = $event->send_by;

        $this->sendPushNotification($booking, $status, $send_by);
        $this->sendSMSToDriver($booking, $status, $send_by);
        $this->sendSMSToOwnerWithoutApp($booking);
    }

    private function sendSMSToOwnerWithoutApp($booking)
    {
        if($booking->owner->without_app=='no')
            return;

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $booking_number = $booking->unique_id;

        $msg_body = $src_point.' থেকে '.$destination_point.' একটি ট্রিপ বাতিল করা হয়েছে, '.'গাড়ী নিবন্ধন নং : '.$booking->vehicle->number_plate.' (বুকিং আইডি : '.$booking_number.')';

        $phone = $booking->owner->user->phone;

        $ref_id = 'witoutapp-'.$booking->owner->id.'-'.$booking->id.'-'.time();

        SMSFacade::send($phone, $msg_body, $ref_id);
    }

    private function sendSMSToDriver($booking, $status, $send_by)
    {
        if($status == 'open'){
            return;
        }elseif($send_by == 'driver'){
            return;
        }

        //$to = $booking->driver->user->phone;
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }
        $to = $driver_user->phone;

        $msg_body = 'হ্যালো '.$driver_user->name.', আপনার গাড়ি বুকিং বাতিল করা হয়েছে (বুকিং আইডি : '.$booking->unique_id.')';
        $ref_id = "$to"."-".time();
        
        SMSFacade::send($to, $msg_body, $ref_id);
    }

    private function sendPushNotification($booking, $status, $send_by)
    {
        if($status == 'open'){
            return;
        }elseif($send_by == 'driver'){
            return;
        }

        $booking_number = $booking->unique_id;

        $push = new PushNotification();
        $msg_body = 'আপনার গাড়ি বুকিং বাতিল করা হয়েছে (বুকিং আইডি : '.$booking_number.')';
        //message for android
        $message = [
            'data' => [
                'title' => 'আপনার গৃহীত বুকিং বাতিল করা হয়েছে',
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'cancelled'
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $message['data']['title'], 
            'body' => $message['data']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => 1, 
            'received_user_id' => $booking->driver->user->id, 
            'received_user_type' => 'driver', 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id 
        ]);

        $fcm_registration_token = $booking->driver->user->fcm_registration_token;
        if (!$fcm_registration_token) {
            return;
        }
        
        $send_res = $push->sendToSingleDevice($fcm_registration_token, $message);
        if($send_res == false){
            return;
        }
    }
}
