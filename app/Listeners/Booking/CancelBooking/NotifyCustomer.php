<?php

namespace App\Listeners\Booking\CancelBooking;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Booking\BookingAcceptedEmail;
use App\Events\Booking\CancelBooking;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\PushNotification as PushNotificationModel;
use App\MyLibrary\SMSLib\SMSFacade;
use App\Booking;
use Carbon\Carbon;
use App\HiredDriver;

class NotifyCustomer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(CancelBooking $event)
    {
        $booking = $event->booking;
        $status = $event->status;
        $send_by = $event->send_by;

        $this->sendPushNotification($booking, $status, $send_by);
        $this->sendSMS($booking, $send_by);
    }

    private function sendSMS($booking, $send_by)
    {
        if($send_by == 'customer'){
            return;
        }
        $to = $booking->customer->user->phone;
        if($send_by == 'task_scheduler'){
            $msg_body = 'আপনার বুকিং বাতিল করা হয়েছে (বুকিং আইডি : '.$booking->unique_id.'), এই  মুহূর্তে আপনার আশেপাশে সকল চালক ব্যস্ত আছেন, একটু পরে আবার চেষ্টা করুন, ধন্যবাদ ।';
        }else{
            $msg_body = 'হ্যালো '.$booking->customer->user->name.', আপনার গাড়ি বুকিং বাতিল করা হয়েছে ।';
        }

        $ref_id = "$to"."-".time();
        
        Booking::where('id', $booking->id)->update(['status'=>'cancelled']);

        SMSFacade::send($to, $msg_body, $ref_id);
    }

    private function sendPushNotification($booking, $status, $send_by)
    {
        if($send_by == 'customer'){
            return;
        }

        if($send_by == 'task_scheduler'){
            $msg_body = 'আপনার বুকিং বাতিল করা হয়েছে (বুকিং আইডি : '.$booking->unique_id.'), এই  মুহূর্তে আপনার আশেপাশে সকল চালক ব্যস্ত আছেন, একটু পরে আবার চেষ্টা করুন, ধন্যবাদ ।';
        }else{
            $msg_body = 'হ্যালো '.$booking->customer->user->name.', আপনার গাড়ি বুকিং বাতিল করা হয়েছে ।';
        }

        $push = new PushNotification();

        //message for android
        $message = [
            'data' => [
                'title' => 'আপনার বুকিং বাতিল করা হয়েছে',
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'cancelled'
            ]
        ];

        //message for ios
        $ios_message = [
            'content-available' => true,
            'notification'=> [
                'title' => 'আপনার বুকিং বাতিল করা হয়েছে',
                'body' => $msg_body,
            ],
            'data' => [
                'title' => 'আপনার বুকিং বাতিল করা হয়েছে',
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'cancelled'
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $message['data']['title'], 
            'body' => $message['data']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => 1, 
            'received_user_id' => $booking->customer->user->id, 
            'received_user_type' => 'customer', 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id 
        ]);
        
        $fcm_registration_token = $booking->customer->user->fcm_registration_token;
        if (!$fcm_registration_token) {
            return;
        }

        if($booking->customer->user->device_type=='ios'){   
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $ios_message);
        }else{        
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $message);
        }

        if($send_res == false){
            return;
        }
    }
}
