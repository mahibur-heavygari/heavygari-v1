<?php

namespace App\Listeners\Booking\NewBooking;

use App\Vehicle;
use App\OwnerProfile;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Booking\NewBookingCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\WebPushNotification;
use App\PushNotification as PushNotificationModel;

class NotifyOwners implements ShouldQueue
{
	private $booking;
    private $message;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBookingCreated  $event
     * @return void
     */
    public function handle(NewBookingCreated $event)
    {
        $this->booking = $event->booking;

        $this->message = $this->prepareMessage();

        /*if ($this->booking->booking_category == 'full') {
            $this->notifyFullBookingOwners();
        }*/
    }

    private function notifyFullBookingOwners()
    {
        $vehicle_type_id = $this->booking->fullBookingDetails->vehicle_type_id;

        $available_vehicles = Vehicle::where('vehicle_type_id', $vehicle_type_id)->get();

        // all the drivers who are logged in with the specific vehicle type
        foreach ($available_vehicles as $vehicle) {
            if ($vehicle->owner) {
                /*PushNotificationModel::create([
                    'mobile_web' => 'web', 
                    'admin_notification' => 'no', 
                    'title' => $this->message['notification']['title'], 
                    'body' => $this->message['notification']['body'], 
                    'category' => $this->message['data']['category'], 
                    'type' => $this->message['data']['type'], 
                    'sent_user_id' => $this->booking['customer']['user']['id'], 
                    'received_user_id' => $vehicle->owner->user->id, 
                    'booking_id' => $this->booking['id'],
                    'unique_id' => $this->booking['unique_id'] 
                ]);
                $this->notifyOwner($vehicle->owner);   */
            }
        }
    }

    private function notifyOwner($owner) 
    {
        $web_fcm_registration_token = $owner->user->web_fcm_registration_token;
        if (!$web_fcm_registration_token) {
            return;
        }

        $push = new WebPushNotification();
        $send_res = $push->sendToSingleBrowser($web_fcm_registration_token, $this->message);
        return $send_res; 
    }

    /**
     * Prepare the push notificaion message
     */
    private function prepareMessage()
    {
        $src_point = $this->booking->trips[0]->origin->title; 
        $destination_point = $this->booking->trips[0]->destination->title;
        $this->booking_number = $this->booking->unique_id;
        $msg_body = $src_point.' থেকে '.$destination_point.' একটি নতুন ট্রিপ এসেছে (বুকিং আইডি : '.$this->booking_number.')';

        $message = [
            'notification' => [
                'title' => 'নতুন ট্রিপ এসেছে',
                'body' => $msg_body,
            ],
            'data' => [
                'id' => $this->booking->id,
                'unique_id' => $this->booking->unique_id,
                'category' => 'booking',
                'type' => 'new'
            ]
        ];

        return $message;
    }
}