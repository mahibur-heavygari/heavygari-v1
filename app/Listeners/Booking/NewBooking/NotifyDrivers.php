<?php

namespace App\Listeners\Booking\NewBooking;

use App\Vehicle;
use App\DriverProfile;
use App\MyLibrary\SMSLib\SMSFacade;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Booking\NewBookingCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\PushNotification as PushNotificationModel;
use GuzzleHttp;

class NotifyDrivers implements ShouldQueue
{
    private $booking;

    private $message;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBookingCreated  $event
     * @return void
     */
    public function handle(NewBookingCreated $event)
    {
        $this->booking = $event->booking;

        $this->message = $this->prepareMessage();

        if ($this->booking->booking_category == 'full') {
            $this->notifyFullBookingDrivers();
        } else {
            $this->notifyParcelBookingDrivers();
        }

        //this two functions commented out only for localhost
        //$this->sendSmsToAdmin();
        //$this->sendMessageToSlack();
    }

    /**
     * Notify the drivers applicable for Full Booking
     */
    private function notifyFullBookingDrivers()
    {
        $vehicle_type_id = $this->booking->fullBookingDetails->vehicle_type_id;

        $available_vehicles = Vehicle::where('vehicle_type_id', $vehicle_type_id)->get();

        // all the drivers who are logged in with the specific vehicle type
        foreach ($available_vehicles as $vehicle) {
            if ($vehicle->currentDriver) {
                $current_mode_arr = $vehicle->currentDriver->getCurrentMode();
                if($current_mode_arr['duty_mode'] == 'on'){
                    $result = $this->notifyDriver($vehicle->currentDriver);
                    if($result){
                       PushNotificationModel::create([
                            'mobile_web' => 'mobile', 
                            'admin_notification' => 'no', 
                            'title' => $this->message['data']['title'], 
                            'body' => $this->message['data']['body'], 
                            'category' => $this->message['data']['category'], 
                            'type' => $this->message['data']['type'], 
                            'sent_user_id' => $this->booking['customer']['user']['id'], 
                            'received_user_type' => 'driver', 
                            'received_user_id' => $vehicle->currentDriver->user->id, 
                            'booking_id' => $this->booking['id'],
                            'unique_id' => $this->booking['unique_id'] 
                        ]);
                    }
                }
            }
        }

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $this->message['data']['title'], 
            'body' => $this->message['data']['body'], 
            'category' => $this->message['data']['category'], 
            'type' => $this->message['data']['type'], 
            'sent_user_id' => $this->booking['customer']['user']['id'], 
            'received_user_type' => 'driver', 
            'received_user_id' => 0, 
            'booking_id' => $this->booking['id'],
            'unique_id' => $this->booking['unique_id'] 
        ]);
    }

    /**
     * Notify the drivers applicable for Parcel Booking
     */
    private function notifyParcelBookingDrivers()
    {
        // TODO :: refactor

        $drivers = DriverProfile::all();
        foreach ($drivers as $driver) {
            if ($driver->bookingCategoryPreference->full==1) {
                $result = $this->notifyDriver($driver);
                if($result){
                   PushNotificationModel::create([
                        'mobile_web' => 'mobile', 
                        'admin_notification' => 'no', 
                        'title' => $this->message['data']['title'], 
                        'body' => $this->message['data']['body'], 
                        'category' => $this->message['data']['category'], 
                        'type' => $this->message['data']['type'], 
                        'sent_user_id' => $this->booking['customer']['user']['id'], 
                        'received_user_id' => $driver->user->id, 
                        'booking_id' => $this->booking['id'],
                        'unique_id' => $this->booking['unique_id']
                    ]);
                }
            }
        }
    }

    /**
     * Send Push to a specific driver
     */
    private function notifyDriver($driver) 
    {
        $fcm_registration_token = $driver->user->fcm_registration_token;

        if (!$fcm_registration_token) {
            return;
        }

        $push = new PushNotification();

        $send_res = $push->sendToSingleDevice($fcm_registration_token, $this->message);
        return $send_res; 
    }

    /**
     * Prepare the push notificaion message
     */
    private function prepareMessage()
    {
        $src_point = $this->booking->trips[0]->origin->title; 
        $destination_point = $this->booking->trips[0]->destination->title;
        $this->booking_number = $this->booking->unique_id;
        $msg_body = $src_point.' থেকে '.$destination_point.' একটি নতুন ট্রিপ এসেছে (বুকিং আইডি : '.$this->booking_number.')';

        /*$message = [
            'notification' => [
                'title' => 'নতুন ট্রিপ এসেছে',
                'body' => $msg_body
            ],
            'data' => [
                'id' => $this->booking->id,
                'unique_id' => $this->booking->unique_id,
                'category' => 'booking',
                'type' => 'new'
            ]
        ];*/

        //android message
        $message = [
            'data' => [
                'title' => 'নতুন ট্রিপ এসেছে',
                'body' => $msg_body,
                'id' => $this->booking->id,
                'unique_id' => $this->booking->unique_id,
                'category' => 'booking',
                'type' => 'new'
            ]
        ];

        return $message;
    }

    private function sendSmsToAdmin()
    {
        $unique_id = $this->booking['unique_id'];

        $to = ['01712997766', '01723943819', '01733399209', '01687620414', '01713187271'];
        $to = collect($to);

        $src_point = $this->booking->trips[0]->origin->title;
        $destination_point = $this->booking->trips[0]->destination->title;
        $vehicle_type = $this->booking->fullBookingDetails->vehicleType->title; 
        $fare = $this->booking->invoice->total_cost;

        $msg_body = 'A booking ('.$unique_id.') from '.$src_point.' to '.$destination_point.' has been created. Please take immediate action. ('.$vehicle_type.', Tk '.(int)$fare.')
http://heavygari.com/booking/'.$this->booking['tracking_code'];

        SMSFacade::sendBulk($to, $msg_body);
    }

    private function sendMessageToSlack(){
        $vehicle_type = $this->booking->fullBookingDetails->vehicleType->title; 
        $customer_name = $this->booking->customer->user->name;
        $customer_phone = $this->booking->customer->user->phone;
        $src_point = $this->booking->trips[0]->origin->title; 
        $destination_point = $this->booking->trips[0]->destination->title;
        $booking_type = $this->booking->booking_type;
        $date_time = $this->booking->datetime->setTimezone('UTC')->format('h:ia d/m/Y');

        $fare = $this->booking->invoice->total_cost;
        $materials = $this->booking->particular_details;

        $text = 'Customer Name: '.$customer_name.' '.'( '.$customer_phone.' )'.', Vehicle Type: '.$vehicle_type.', Materials: '.$materials.', Fare: Tk '.$fare;

        $title = 'A new booking has been created from '.$src_point.' to '.$destination_point. ' ( Booking ID: '.$this->booking->unique_id.', Booking Type: '.$booking_type.', Date and Time: '.$date_time.' )';

        $message['attachments'][0]['title'] = $title;
        $message['attachments'][0]['text'] = $text;

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $response = $client->post('https://hooks.slack.com/services/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',
            ['body' => json_encode($message)]
        );
    }
}
