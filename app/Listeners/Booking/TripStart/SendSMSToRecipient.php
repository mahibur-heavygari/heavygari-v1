<?php

namespace App\Listeners\Booking\TripStart;

use App\MyLibrary\SMSLib\SMSFacade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Booking\TripStart;

class SendSMSToRecipient implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TripStart  $event
     * @return void
     */
    public function handle(TripStart $event)
    {
        $booking = $event->booking;
    
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }

        $to = $booking->recipient_phone;

        $msg = 'আপনার জন্য দেয়া বুকিংটি গ্রহণ করা হয়েছে, বুকিং আইডি '.$booking->unique_id.', চালকের নাম '.$driver_user->name.', ফোন '.$driver_user->phone.', ট্র্যাকিং url: http://www.heavygari.com/booking/'.$booking->tracking_code;

        $ref_id = 'booking-'.$booking->id.'-'.time();

        SMSFacade::send($to, $msg, $ref_id);
    }
}
