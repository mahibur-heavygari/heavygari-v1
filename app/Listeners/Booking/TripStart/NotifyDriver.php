<?php

namespace App\Listeners\Booking\TripStart;

use App\Events\Booking\TripStart;
use App\MyLibrary\SMSLib\SMSFacade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\PushNotification as PushNotificationModel;

class NotifyDriver implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TripStart  $event
     * @return void
     */
    public function handle(TripStart $event)
    {
        $booking = $event->booking;
        $this->sendSMS($booking);
    }

    private function SendSMS($booking)
    {        
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $recipient_name = $booking->recipient_name;
        $recipient_phone = $booking->recipient_phone;

        $ref_id = 'notify-driver-'.$driver_user->id.'-'.$booking->id.'-'.time();
        $message = 'আপনার এই '.$src_point.' হইতে '.$destination_point.' ট্রিপটির ( আইডিঃ '.$booking->unique_id.' ) গন্তব্যে
প্রাপক: '.$recipient_name.', ফোন: '.$recipient_phone.'
প্রয়োজনে কল করুন।';

        SMSFacade::send($driver_user->phone, $message, $ref_id);
    }
}