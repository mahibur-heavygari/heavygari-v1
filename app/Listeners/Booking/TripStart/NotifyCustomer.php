<?php

namespace App\Listeners\Booking\TripStart;

use App\Events\Booking\TripStart;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\MyLibrary\Firebase\WebPushNotification;
use App\PushNotification as PushNotificationModel;

class NotifyCustomer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TripStart  $event
     * @return void
     */
    public function handle(TripStart $event)
    {
        $booking = $event->booking;

        $this->sendPushNotification($booking);

        //$this->sendWebPushNotification($booking);
    }

    private function sendPushNotification($booking)
    {
        $driver_name = $booking->driver->user->name; 
        $vehicle_number = $booking->vehicle->number_plate; 
        $booking_number = $booking->unique_id;
        $msg_body = 'চালক : '.$driver_name.' এবং পরিবহন নিবন্ধন নং : '.$vehicle_number.' দ্বারা আপনার ট্রিপ শুরু হলো (বুকিং আইডি : '.$booking_number.') ';

        //message for android
        $message = [
            'data' => [
                'title' => 'আপনার ট্রিপ শুরু হলো ',
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'ongoing'
            ]
        ];

        //message for ios
        $ios_message = [
            'content-available' => true,
            'notification'=> [
                'title' => 'আপনার ট্রিপ শুরু হলো ',
                'body' => $msg_body,
            ],
            'data' => [
                'title' => 'আপনার ট্রিপ শুরু হলো ',
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'ongoing'
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $message['data']['title'], 
            'body' => $message['data']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => $booking->driver->user->id, 
            'received_user_id' => $booking->customer->user->id, 
            'received_user_type' => 'customer', 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id
        ]);

        $fcm_registration_token = $booking->customer->user->fcm_registration_token;
        if (!$fcm_registration_token) {
            return;
        }

        $push = new PushNotification();
        if($booking->customer->user->device_type=='ios'){
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $ios_message);
        }else{
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $message);
        }

        if($send_res == false){
            return;
        }
    }

    private function sendWebPushNotification($booking)
    {
        $push = new WebPushNotification();

        $driver_name = $booking->driver->user->name; 
        $vehicle_number = $booking->vehicle->number_plate; 
        $booking_number = $booking->unique_id;
        $msg_body = 'চালক : '.$driver_name.' এবং পরিবহন নিবন্ধন নং : '.$vehicle_number.' দ্বারা আপনার ট্রিপ শুরু হলো (বুকিং আইডি : '.$booking_number.') ';

        $message = [
            'notification' => [
                'title' => 'আপনার ট্রিপ শুরু হলো ',
                'body' => $msg_body,
            ],
            'data' => [
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'ongoing'
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'web', 
            'admin_notification' => 'no', 
            'title' => $message['notification']['title'], 
            'body' => $message['notification']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => $booking->driver->user->id, 
            'received_user_id' => $booking->customer->user->id, 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id
        ]);     

        $web_fcm_registration_token = $booking->customer->user->web_fcm_registration_token;
        if (!$web_fcm_registration_token) {
            return;
        }

        $send_res = $push->sendToSingleBrowser($web_fcm_registration_token, $message);
        if($send_res == false){
            return;
        }
    }
}