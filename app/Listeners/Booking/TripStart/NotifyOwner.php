<?php

namespace App\Listeners\Booking\TripStart;

use App\Events\Booking\TripStart;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\WebPushNotification;
use App\PushNotification;
use App\MyLibrary\SMSLib\SMSFacade;

class NotifyOwner implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(TripStart $event)
    {
        $booking = $event->booking;

        $this->sendWebPushNotification($booking);
        
        $this->sendSMSToOwner($booking);
    }

    private function sendSMSToOwner($booking)
    {
        // if($booking->owner->without_app=='no')
        //     return;

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $booking_number = $booking->unique_id;

        $msg_body = ' আপনার গৃহীত '.$src_point.' থেকে '.$destination_point.' ট্রিপটি শুরু হয়েছে, '.'গাড়ী নিবন্ধন নং : '.$booking->vehicle->number_plate.' (বুকিং আইডি : '.$booking_number.')';

        $phone = $booking->owner->user->phone;

        $ref_id = 'witoutapp-'.$booking->owner->id.'-'.$booking->id.'-'.time();

        SMSFacade::send($phone, $msg_body, $ref_id);
    }

    private function sendWebPushNotification($booking)
    {
        $driver_name = $booking->driver->user->name; 
        $vehicle_number = $booking->vehicle->number_plate; 
        $booking_number = $booking->unique_id;
        $msg_body = 'চালক : '.$driver_name.' এবং পরিবহন নিবন্ধন নং : '.$vehicle_number.' দ্বারা ট্রিপ শুরু হলো (বুকিং আইডি : '.$booking_number.') ';

        $message = [
            'notification' => [
                'title' => 'ট্রিপ শুরু হলো ',
                'body' => $msg_body,
            ],
            'data' => [
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'ongoing'
            ]
        ];

        PushNotification::create([
            'mobile_web' => 'web', 
            'admin_notification' => 'no', 
            'title' => $message['notification']['title'], 
            'body' => $message['notification']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => $booking->driver->user->id, 
            'received_user_id' => $booking->owner->user->id, 
            'received_user_type' => 'driver', 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id
        ]);
        
        $web_fcm_registration_token = $booking->owner->user->web_fcm_registration_token;
        if (!$web_fcm_registration_token) {
            return;
        }
        
        $push = new WebPushNotification();
        $send_res = $push->sendToSingleBrowser($web_fcm_registration_token, $message);
        if($send_res == false){
            return;
        }
    }
}
