<?php

namespace App\Listeners\Booking\AcceptBooking;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Booking\BookingAcceptedEmail;
use App\Events\Booking\DriverAcceptBooking;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\WebPushNotification;
use App\PushNotification;
use App\MyLibrary\SMSLib\SMSFacade;

class NotifyOwner implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(DriverAcceptBooking $event)
    {
        $booking = $event->booking;
        $this->sendWebPushNotification($booking);
        $this->sendSMSToDriver($booking);
        //$this->sendSMSToOwnerWithoutApp($booking);
        $this->sendSMSToOwner($booking);
    }

    private function sendSMSToOwner($booking)
    {
        if($booking->accepted_by == 'owner'){
            return;
        }

        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }

        $owner_user= $booking->owner->user;
        $customer_user= $booking->customer->user;

        $ref_id = 'from-admin-'.$owner_user->id.'-'.$booking->id.'-'.time();
        $message = 'ড্রাইভার '.$driver_user->name.' এর একটি ট্রিপ গৃহীত হয়েছে, ট্রিপ আইডিঃ '.$booking->unique_id.' গ্রাহকের নাম: '.$customer_user->name.', ফোন নাম্বার: '.$customer_user->phone;

        SMSFacade::send($owner_user->phone, $message, $ref_id);
    }

    private function sendSMSToOwnerWithoutApp($booking)
    {
        if($booking->owner->without_app=='no')
            return;

        $src_point = $booking->trips[0]->origin->title;
        $destination_point = $booking->trips[0]->destination->title;
        $booking_number = $booking->unique_id;

        $msg_body = $src_point.' থেকে '.$destination_point.' একটি ট্রিপ গৃহীত হয়েছে, '.'গাড়ী নিবন্ধন নং : '.$booking->vehicle->number_plate.' (বুকিং আইডি : '.$booking_number.')';

        $phone = $booking->owner->user->phone;

        $ref_id = 'witoutapp-'.$booking->owner->id.'-'.$booking->id.'-'.time();

        SMSFacade::send($phone, $msg_body, $ref_id);
    }

    private function sendSMSToDriver($booking)
    {
        if($booking->accepted_by != 'owner'){
            return;
        }
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }

        //$driver_user = $booking->driver->user;
        $customer_user= $booking->customer->user;

        $ref_id = 'from-owner-'.$driver_user->id.'-'.$booking->id.'-'.time();
        $message = 'আপনার একটি ট্রিপ গৃহীত হয়েছে, ট্রিপ আইডিঃ '.$booking->unique_id.' যাত্রীর নাম: '.$customer_user->name.', যাত্রীর ফোন নাম্বার: '.$customer_user->phone;

        SMSFacade::send($driver_user->phone, $message, $ref_id);
    }

    private function sendWebPushNotification($booking)
    {
        $push = new WebPushNotification();

        //$driver_name = $booking->driver->user->name; 
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }
        $vehicle_number = $booking->vehicle->number_plate; 
        $booking_number = $booking->unique_id;
        $msg_body = 'পরিবহন অনুরোধ, চালক : '.$driver_user->name.' এবং গাড়ী নিবন্ধন নং : '.$vehicle_number.' দ্বারা গৃহীত হয়েছে (বুকিং আইডি : '.$booking_number.') ';

        $message = [
            'notification' => [
                'title' => 'বুকিং গৃহীত হয়েছে',
                'body' => $msg_body,
            ],
            'data' => [
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'upcoming'
            ]
        ];

        PushNotification::create([
            'mobile_web' => 'web', 
            'admin_notification' => 'no', 
            'title' => $message['notification']['title'], 
            'body' => $message['notification']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => $booking->driver->user->id, 
            'received_user_type' => 'driver', 
            'received_user_id' => $booking->owner->user->id, 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id  
        ]);        
        
        $web_fcm_registration_token = $booking->owner->user->web_fcm_registration_token;
        if (!$web_fcm_registration_token) {
            return;
        }

        $send_res = $push->sendToSingleBrowser($web_fcm_registration_token, $message);
        if($send_res == false){
            return;
        }
    }
}
