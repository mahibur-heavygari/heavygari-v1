<?php

namespace App\Listeners\Booking\AcceptBooking;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Booking\BookingAcceptedEmail;
use App\Events\Booking\DriverAcceptBooking;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\MyLibrary\Firebase\WebPushNotification;
use App\PushNotification as PushNotificationModel;
use App\MyLibrary\SMSLib\SMSFacade;

class NotifyCustomer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(DriverAcceptBooking $event)
    {
        $booking = $event->booking;

        $this->sendPushNotification($booking);

        //$this->sendWebPushNotification($booking);

        $this->sendEmail($booking);

        //send sms to customer if admin created the booking
        $this->SendSMS($booking);
    }

    private function sendPushNotification($booking)
    {
        //$driver_name = $booking->driver->user->name; 
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }
        $vehicle_number = $booking->vehicle->number_plate; 
        $booking_number = $booking->unique_id;

        $number_of_acceptance = (int)$booking->number_of_acceptance;

        if($number_of_acceptance <= 1){
            $title = 'আপনার বুকিং গৃহীত হয়েছে';
            $msg_body = 'আপনার দেয়া পরিবহন অনুরোধ, চালক : '.$driver_user->name.' এবং গাড়ী নিবন্ধন নং : '.$vehicle_number.' দ্বারা গৃহীত হয়েছে (বুকিং আইডি : '.$booking_number.') ';
        }else{
            $title = 'চালক পরিবর্তন করা হয়েছে';
            $msg_body = 'আপনার বুকিংটির চালক পরিবর্তন করা হয়েছে, চালক : '.$driver_user->name.' এবং গাড়ী নিবন্ধন নং : '.$vehicle_number.' দ্বারা গৃহীত হয়েছে (বুকিং আইডি : '.$booking_number.') ';
        }

        //message for android
        $message = [
            'data' => [
                'title' => $title,
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'upcoming'
            ]
        ];

        //message for ios
        $ios_message = [
            'content-available' => true,
            'notification'=> [
                'title' => $title,
                'body' => $msg_body,
            ],
            'data' => [
                'title' => $title,
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'upcoming'
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $message['data']['title'], 
            'body' => $message['data']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => $booking->driver->user->id, 
            'received_user_id' => $booking->customer->user->id, 
            'received_user_type' => 'customer', 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id 
        ]);

        $fcm_registration_token = $booking->customer->user->fcm_registration_token;
        if (!$fcm_registration_token) {
            return;
        }

        $push = new PushNotification();

        if($booking->customer->user->device_type=='ios'){
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $ios_message);
        }else{
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $message);
        }

        if($send_res == false){
            return;
        }
    }

    private function sendWebPushNotification($booking)
    {
        $push = new WebPushNotification();

        $driver_name = $booking->driver->user->name; 
        $vehicle_number = $booking->vehicle->number_plate; 
        $booking_number = $booking->unique_id;
        $msg_body = 'আপনার দেয়া পরিবহন অনুরোধ, চালক : '.$driver_name.' এবং গাড়ী নিবন্ধন নং : '.$vehicle_number.' দ্বারা গৃহীত হয়েছে (বুকিং আইডি : '.$booking_number.') ';

        $message = [
            'notification' => [
                'title' => 'আপনার বুকিং গৃহীত হয়েছে',
                'body' => $msg_body,
            ],
            'data' => [
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'upcoming'
            ]
        ];
        
        // TODO : add custom values for android, iOS

        PushNotificationModel::create([
            'mobile_web' => 'web', 
            'admin_notification' => 'no', 
            'title' => $message['notification']['title'], 
            'body' => $message['notification']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => $booking->driver->user->id, 
            'received_user_id' => $booking->customer->user->id, 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id 
        ]);

        $web_fcm_registration_token = $booking->customer->user->web_fcm_registration_token;
        if (!$web_fcm_registration_token) {
            return;
        }

        $send_res = $push->sendToSingleBrowser($web_fcm_registration_token, $message);
        if($send_res == false){
            return;
        }

    }

    private function sendEmail($booking)
    {
        Mail::to($booking->customer->user)->send(new BookingAcceptedEmail($booking));
    }

    private function SendSMS( $booking )
    {
        if(is_null($booking->created_at))
            return;
        if($booking->recipient_phone == $booking->customer->user->phone)
            return;

        $number_of_acceptance = (int)$booking->number_of_acceptance;
        if($number_of_acceptance  > 1)
            return;

        $to = $booking->customer->user->phone;

        $msg = 'আপনার বুকিংটি গ্রহণ করা হয়েছে, বুকিং আইডিঃ '.$booking->unique_id.', ট্র্যাকিং url: http://www.heavygari.com/booking/'.$booking->tracking_code;

        $ref_id = 'booking-'.$booking->id.'-customer-'.time();
        SMSFacade::send($to, $msg, $ref_id);
    }
}
