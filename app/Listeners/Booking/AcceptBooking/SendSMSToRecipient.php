<?php

namespace App\Listeners\Booking\AcceptBooking;

use App\MyLibrary\SMSLib\SMSFacade;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Booking\NewBookingCreated;
use App\Events\Booking\DriverAcceptBooking;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSMSToRecipient implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(DriverAcceptBooking $event)
    {
        $booking = $event->booking;

        $number_of_acceptance = (int)$booking->number_of_acceptance;
        if($number_of_acceptance  > 1)
            return;

        // send sms to recipient
        $to = $booking->recipient_phone;

        $msg = 'আপনার বুকিং গ্রহণ করা হয়েছে, বুকিং আইডিঃ '.$booking->unique_id.', ট্র্যাকিং url: http://www.heavygari.com/booking/'.$booking->tracking_code;

        $ref_id = 'booking-'.$booking->id.'-'.time();

        SMSFacade::send($to, $msg, $ref_id);
    }
}
