<?php

namespace App\Listeners\Booking\AcceptBooking;

use PDF;
use Storage;
use App\Events\Booking\DriverAcceptBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenerateInvoicePDF implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAcceptBooking  $event
     * @return void
     */
    public function handle(DriverAcceptBooking $event)
    {
        $booking = $event->booking;

        $pdf_link = 'public/booking_invoices/invoice_'.$booking->tracking_code.'.pdf';

        $data['booking'] = $booking;
        $pdf = PDF::loadView('emails.booking.invoice', $data);
        Storage::put($pdf_link, $pdf->output());

        // update db with pdf link
        $booking->invoice_pdf = $pdf_link;
        $booking->save();
    }
}
