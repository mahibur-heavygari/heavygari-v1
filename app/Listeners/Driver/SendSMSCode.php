<?php

namespace App\Listeners\Driver;

use App\Events\Driver\NewDriverRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\MyLibrary\UserLib\Verification\PhoneVerification;

class SendSMSCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewDriverRegistered  $event
     * @return void
     */
    public function handle(NewDriverRegistered $event)
    {
        $user = $event->user;

        $sms = new PhoneVerification($user);
        $sms->create();
    }
}
