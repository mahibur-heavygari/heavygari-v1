<?php

namespace App\Listeners\Driver\AccountVerified;

use Illuminate\Queue\InteractsWithQueue;
use App\Events\Driver\DriverAccountVerified;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\SMSLib\SMSFacade;
use App\User; 

class SendSms implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAccountVerified  $event
     * @return void
     */
    public function handle(DriverAccountVerified $event)
    {
        $user = $event->user;
        $this->sendSMS($user);
    }

    private function sendSMS($user)
    {
        if(is_null($user->sms))
            return;

        $to = $user->phone;
        $msg = $user->sms;
        $ref_id = 'user-'.$user->id.'-'.time();
        SMSFacade::send($to, $msg, $ref_id);

        User::where('id', $user->id)->update(['sms'=>null]);
    }
}
