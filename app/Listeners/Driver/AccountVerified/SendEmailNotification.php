<?php

namespace App\Listeners\Driver\AccountVerified;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Driver\DriverAccountVerified;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Registration\AccountVerifiedEmail;

class SendEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAccountVerified  $event
     * @return void
     */
    public function handle(DriverAccountVerified $event)
    {
        $user = $event->user;

        Mail::to($user)->send(new AccountVerifiedEmail($user));
    }
}
