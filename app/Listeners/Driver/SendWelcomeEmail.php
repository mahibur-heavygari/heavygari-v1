<?php

namespace App\Listeners\Driver;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Driver\NewDriverRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Registration\WelcomeDriverEmail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewDriverRegistered  $event
     * @return void
     */
    public function handle(NewDriverRegistered $event)
    {
        $user = $event->user;
        $driver = $user->driverProfile;

        if(!is_null($user->email)){
            $this->SendEmail($user, $driver);
        }
    }

    private function SendEmail($user, $driver){
        Mail::to($user)->send(new WelcomeDriverEmail($driver));
    }
}
