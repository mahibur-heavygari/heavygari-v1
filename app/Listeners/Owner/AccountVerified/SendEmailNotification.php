<?php

namespace App\Listeners\Owner\AccountVerified;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Owner\OwnerAccountVerified;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Registration\AccountVerifiedEmail;

class SendEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountVerified  $event
     * @return void
     */
    public function handle(OwnerAccountVerified $event)
    {
        $user = $event->user;

        Mail::to($user)->send(new AccountVerifiedEmail($user));
    }
}
