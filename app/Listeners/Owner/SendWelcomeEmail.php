<?php

namespace App\Listeners\Owner;

use Illuminate\Support\Facades\Mail;
use App\Events\Owner\NewOwnerRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Registration\WelcomeOwnerEmail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewOwnerRegistered  $event
     * @return void
     */
    public function handle(NewOwnerRegistered $event)
    {
        $user = $event->user;
        $owner = $user->ownerProfile;

        Mail::to($user)->send(new WelcomeOwnerEmail($owner,  "Welcome"));
    }
}
