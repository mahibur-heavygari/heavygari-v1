<?php

namespace App\Listeners\Owner;

use App\Events\Owner\NewOwnerRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\MyLibrary\UserLib\Verification\PhoneVerification;

class SendSMSCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewOwnerRegistered  $event
     * @return void
     */
    public function handle(NewOwnerRegistered $event)
    {
        $user = $event->user;

        $sms = new PhoneVerification($user);
        $sms->create();
    }
}
