<?php

namespace App\Listeners\Customer;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Customer\NewCustomerRegistered;
use App\Mail\Registration\WelcomeCustomerEmail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCustomerRegistered  $event
     * @return void
     */
    public function handle(NewCustomerRegistered $event)
    {
        $user = $event->user;
        $customer = $user->customerProfile;

        Mail::to($user)->send(new WelcomeCustomerEmail($customer, "Welcome"));
    }
}
