<?php

namespace App\Listeners\OwnerInvoice\OwnerInvoiceCreated;

use App\MyLibrary\InvoiceLib\InvoiceSystem;
use App\Events\OwnerInvoice\OwnerInvoiceCreated as OwnerInvoiceCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePaidPDF implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerInvoiceCreated  $event
     * @return void
     */
    public function handle(OwnerInvoiceCreatedEvent $event)
    {
        $invoice = $event->owner_invoice;
        $this->exportPaidPdf($invoice);
    }

    private function exportPaidPdf($invoice){
        $pdf_paid_link = InvoiceSystem::exportPaid($invoice->invoice_number);

        // update db with pdf link
        $invoice->pdf_paid = $pdf_paid_link;
        $invoice->save();
    }
}
