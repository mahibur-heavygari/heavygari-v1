<?php

namespace App\Listeners\OwnerInvoice\OwnerInvoiceCreated;

use App\MyLibrary\InvoiceLib\InvoiceSystem;
use App\Events\OwnerInvoice\OwnerInvoiceCreated as OwnerInvoiceCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePDF implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerInvoiceCreated  $event
     * @return void
     */
    public function handle(OwnerInvoiceCreatedEvent $event)
    {
        $invoice = $event->owner_invoice;
        $this->exportPdf($invoice);
    }

    private function exportPdf($invoice){
        $pdf_link = InvoiceSystem::export($invoice->invoice_number);

        // update db with pdf link
        $invoice->pdf = $pdf_link;
        $invoice->save();
    }
}
