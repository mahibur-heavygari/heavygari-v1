<?php

namespace App\Listeners\FromAdmin\SendPushOtherEmail;

use App\Events\FromAdmin\SendPushOtherEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\PushEmail\OthersEmail;

class SendOtherEmail implements ShouldQueue
{  
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendOtherEmail  $event
     * @return void
     */
    public function handle(SendPushOtherEmail $event)
    {  //dd($event);
        $msg = $event->msg;
        $user = $event->user;
        $title = $event->title;
        $image = $event->image;
        $this->SendOtherEmail($msg, $user, $title, $image);
    }

    private function SendOtherEmail($msg, $user, $title=false, $image=false )
    {        
        Mail::to($user)->send(new OthersEmail($user, $msg, $image, $title));
        return redirect('/cms/send/email');
    }

    
}
