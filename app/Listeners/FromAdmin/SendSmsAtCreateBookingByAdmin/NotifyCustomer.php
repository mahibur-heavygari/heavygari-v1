<?php

namespace App\Listeners\FromAdmin\SendSmsAtCreateBookingByAdmin;

use App\Events\FromAdmin\SendSmsAtCreateBookingByAdmin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\SMSLib\SMSFacade;

class NotifyCustomer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendSmsAtCreateBookingByAdmin  $event
     * @return void
     */
    public function handle(SendSmsAtCreateBookingByAdmin $event)
    {
        $booking = $event->booking;
        $is_short_customer = $event->is_short_customer;
        $password = $event->password;

        if($is_short_customer == true){            
            $message = 'হেভিগাড়ীতে আপনার লগইন ইউজারনেম
ফোন নং: '.$booking->customer->user->phone.'
পাসওয়ার্ড: '.$password.'
Android অ্যাপ: https://bit.ly/2Nj2msh
iOS অ্যাপ: https://goo.gl/T1a1n3';
            
            $ref_id = 'from-admin-'.$booking->customer->user->phone.'-'.time();
            $this->SendSMS($booking->customer->user->phone, $message, $ref_id);
        }

        $message = 'আপনার জন্য একটি বুকিং করা হয়েছে, বুকিং আইডিঃ '.$booking->unique_id.', ট্র্যাকিং url: http://www.heavygari.com/booking/'.$booking->tracking_code;
        
        $ref_id = 'from-admin-'.$booking->customer->user->phone.'-'.$booking->tracking_code.'-'.time();

        $this->SendSMS($booking->customer->user->phone, $message, $ref_id);
    }

    private function SendSMS($phone, $text, $ref_id )
    {
        SMSFacade::send($phone, $text, $ref_id);
    }
}
