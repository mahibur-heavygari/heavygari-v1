<?php

namespace App\Listeners\FromAdmin\SendPushEmail;

use App\Events\FromAdmin\SendPushEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\PushEmail\BulkEmail;



class SendEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmail  $event
     * @return void
     */
    public function handle(SendPushEmail $event)
    {
        $msg = $event->msg;
        $user = $event->user;
        $title = $event->title;
        $image = $event->image;
        $this->SendEmail($msg, $user, $title, $image);
    }

    private function SendEmail($msg, $user, $title=false, $image=false )
    {        
        Mail::to($user)->send(new BulkEmail($user, $msg, $image, $title));
        return redirect('/cms/send/email');
    }    
}
