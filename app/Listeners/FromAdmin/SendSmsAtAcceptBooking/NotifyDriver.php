<?php

namespace App\Listeners\FromAdmin\SendSmsAtAcceptBooking;

use App\Events\FromAdmin\SendSmsAtAcceptBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\SMSLib\SMSFacade;
use App\MyLibrary\Firebase\PushNotification;
use App\PushNotification as PushNotificationModel;

class NotifyDriver implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendSmsAtAcceptBooking  $event
     * @return void
     */
    public function handle(SendSmsAtAcceptBooking $event)
    {
        //dd($event);
        $booking = $event->booking;
        $this->SendSMS($booking);
        $this->sendPushNotification($booking);
    }

    private function SendSMS($booking)
    {        
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }
        
        $customer_user= $booking->customer->user;

        $ref_id = 'from-admin-'.$driver_user->id.'-'.$booking->id.'-'.time();
        $message = 'আপনার একটি ট্রিপ গৃহীত হয়েছে, ট্রিপ আইডিঃ '.$booking->unique_id.' যাত্রীর নাম: '.$customer_user->name.', যাত্রীর ফোন নাম্বার: '.$customer_user->phone;

        SMSFacade::send($driver_user->phone, $message, $ref_id);
    }

    private function sendPushNotification($booking)
    {
        $msg_body = 'আপনার একটি ট্রিপ গৃহীত হয়েছে, ট্রিপ আইডিঃ '.$booking->unique_id;
        //message for android
        $message = [
            'data' => [
                'title' => 'আপনার ট্রিপ গৃহীত হয়েছে',
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => 'cancelled'
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $message['data']['title'], 
            'body' => $message['data']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => 1, 
            'received_user_id' => $booking->driver->user->id, 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id 
        ]);
        
        $push = new PushNotification();
        $fcm_registration_token = $booking->driver->user->fcm_registration_token;
        if (!$fcm_registration_token) {
            return;
        }

        $send_res = $push->sendToSingleDevice($fcm_registration_token, $message);
        if($send_res == false){
            return;
        }
    }
}
