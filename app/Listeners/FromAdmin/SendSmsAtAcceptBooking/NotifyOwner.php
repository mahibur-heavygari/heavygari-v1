<?php

namespace App\Listeners\FromAdmin\SendSmsAtAcceptBooking;

use App\Events\FromAdmin\SendSmsAtAcceptBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\SMSLib\SMSFacade;

class NotifyOwner implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendSmsAtAcceptBooking  $event
     * @return void
     */
    public function handle(SendSmsAtAcceptBooking $event)
    {
        //dd($event);
        $booking = $event->booking;
        $this->SendSMS($booking);
    }

    private function SendSMS($booking)
    {
        if($booking->is_hired_driver == 'yes'){
            $driver_user = $booking->hiredDriver;
        }
        else{
            $driver_user = $booking->driver->user;
        }

        $owner_user= $booking->owner->user;
        $customer_user= $booking->customer->user;

        $ref_id = 'from-admin-'.$owner_user->id.'-'.$booking->id.'-'.time();
        $message = 'ড্রাইভার '.$driver_user->name.' এর একটি ট্রিপ গৃহীত হয়েছে, ট্রিপ আইডিঃ '.$booking->unique_id.' যাত্রীর নাম: '.$customer_user->name.', ফোন নাম্বার: '.$customer_user->phone;

        SMSFacade::send($owner_user->phone, $message, $ref_id);
    }
}
