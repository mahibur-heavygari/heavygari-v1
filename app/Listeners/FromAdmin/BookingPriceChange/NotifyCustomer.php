<?php

namespace App\Listeners\FromAdmin\BookingPriceChange;

use App\Events\FromAdmin\BookingPriceChange;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyLibrary\Firebase\PushNotification;
use App\PushNotification as PushNotificationModel;
use Illuminate\Support\Facades\Storage;
use PDF;
use App\SmsHistory;

class NotifyCustomer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingPriceChange  $event
     * @return void
     */
    public function handle(BookingPriceChange $event)
    {
        $booking = $event->booking;
        $this->sendPushNotification($booking);

        $this->changeInvoice($booking);
    }

    private function changeInvoice($booking)
    {
        if($booking->status=='open')
            return;

        $pdf_link = $booking->invoice_pdf;

        if (Storage::disk('local')->exists($pdf_link)){
            unlink(storage_path('app/'.$pdf_link));
        }

        $data['booking'] = $booking;
        $pdf = PDF::loadView('emails.booking.invoice', $data);
        Storage::put($pdf_link, $pdf->output());
    }

    private function sendPushNotification($booking)
    { 
        $booking_number = $booking->unique_id;


        $title = 'ভাড়া পরিবর্তন হয়েছে';
        $msg_body = 'আপনার বুকিং (বুকিং আইডি : '.$booking_number.') এর ভাড়া পরিবর্তন করা হয়েছে, পরিবর্তিত ভাড়া '.$booking->invoice->total_cost. ' টাকা, ধন্যবাদ';

        //message for android
        $message = [
            'data' => [
                'title' => $title,
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => NULL
            ]
        ];

        //message for ios
        $ios_message = [
            'content-available' => true,
            'notification'=> [
                'title' => $title,
                'body' => $msg_body,
            ],
            'data' => [
                'title' => $title,
                'body' => $msg_body,
                'id' => $booking->id,
                'unique_id' => $booking->unique_id,
                'category' => 'booking',
                'type' => NULL
            ]
        ];

        PushNotificationModel::create([
            'mobile_web' => 'mobile', 
            'admin_notification' => 'no', 
            'title' => $message['data']['title'], 
            'body' => $message['data']['body'], 
            'category' => $message['data']['category'], 
            'type' => $message['data']['type'], 
            'sent_user_id' => 1, 
            'received_user_id' => $booking->customer->user->id, 
            'received_user_type' => 'customer', 
            'booking_id' => $booking->id,
            'unique_id' => $booking->unique_id 
        ]);

        $fcm_registration_token = $booking->customer->user->fcm_registration_token;
        if (!$fcm_registration_token) {
            return;
        }

        $push = new PushNotification();

        if($booking->customer->user->device_type=='ios'){
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $ios_message);
        }else{
            $send_res = $push->sendToSingleDevice($fcm_registration_token, $message);
        }

        if($send_res == false){
            return;
        }
    }
}
