<?php

namespace App;

class FullBookingPriceManager extends BaseModel
{
    protected $table = 'full_booking_price_manager';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehicle_type_id', 'short_trip_rate', 'up_trip_rate', 'down_trip_rate', 'long_up_trip_rate', 'long_down_trip_rate', 'base_fare', 'surcharge_rate', 'discount_percent', 'from_discount_date', 'to_discount_date', 'admin_commission'
    ];


    protected $dates = ['from_discount_date', 'to_discount_date'];

    // BEGIN RELATIONS
    
    /**
     * Price belongs to a Vehicle Type 
     */
    public function vehicleType()
    {
        return $this->belongsTo('App\VehicleType', 'vehicle_type_id');
    }

    // END OF RELATIONS
}
