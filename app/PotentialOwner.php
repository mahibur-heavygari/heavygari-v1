<?php

namespace App;

class PotentialOwner extends BaseModel
{
    protected $table = 'potential_owners';

    protected $fillable = [
        'company_name', 'owner_name', 'owner_phone', 'owner_email','manager_name', 'manager_phone', 'location'
    ];

    public function updateProfile($request)
    {
        $this->update($request->only('company_name', 'owner_name', 'owner_phone', 'owner_email', 'manager_name', 'manager_phone', 'location'));
    }

    public function myVehicleTypes()
    {
        return $this->belongsToMany('App\VehicleType', 'rel_potential_owners_vehicle_types', 'potential_owner_id', 'base_vehicle_type_id');
    }
}
