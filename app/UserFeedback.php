<?php

namespace App;

class UserFeedback extends BaseModel
{
    protected $table = 'user_feedbacks';

    protected $fillable = [
        'user_id', 'user_type', 'message'
    ];

     /**
     * user
     */
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
