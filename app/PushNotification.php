<?php

namespace App;

class PushNotification extends BaseModel
{
    protected $table = 'push_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mobile_web', 'admin_notification', 'title', 'body', 'category', 'type', 'sent_user_id', 'received_user_type', 'received_user_id', 'booking_id', 'unique_id'
    ];
}
