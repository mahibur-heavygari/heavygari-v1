<?php

namespace App;

class Vehicle extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ref_no', 'name', 'about', 'photo', 'thumb_photo', 'vehicle_type_id', 'vehicle_registration_number', 'fitness_number', 'fitness_expiry', 'number_plate', 'tax_token_number', 'tax_token_expirey', 'chesis_no', 'engine_capacity_cc', 'ride_sharing_certificate_no', 'insurance_no', 'registered_by', 'length', 'width', 'height', 'custom_capacity', 'route_note', 'status'];

    //////////////////////////
    // Accessors & Mutators //
    //////////////////////////
   
    // photo
    public function getPhotoAttribute($value)
    {   
        if (!$value) {
            return 'public/vehicles/default.png';
        }

        return $value;
    }

    // thumb photo
    public function getThumbPhotoAttribute($value)
    {   
        if (!$value) {
            return 'public/vehicles/default.png';
        }
        
        return $value;
    }

    // BEGIN RELATIONS
    
    /**
     * owner
     */
    public function owner()
    {
        return $this->belongsTo('App\OwnerProfile', 'vehicle_owner_profile_id');
    }

    /**
     * currentDriver
     */
    public function currentDriver()
    {
        return $this->belongsTo('App\DriverProfile', 'current_driver_profile_id');
    }

    /**
     * authorizedDriver
     */
    public function authorizedDrivers()
    {
        return $this->belongsToMany('App\DriverProfile', 'rel_vehicles_drivers');
    }

    /**
     * bookings
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'vehicle_id');
    }

    /**
     * vehicleType
     */
    public function vehicleType()
    {
        return $this->belongsTo('App\VehicleType', 'vehicle_type_id');
    }

    // END OF RELATIONS
    

    /* Repository Like Methods */

    public static function addRepo($owner, $request)
    {
        $vehicle = $owner->myVehicles()->create($request->all());

        //authorized drivers
        if ($request->has('authorized_drivers')) {
            foreach ($request->authorized_drivers as $driver) {
                $vehicle->authorizedDrivers()->attach($driver);
            }
        }
        
        //update photo
        if ($request->file('photo')) {
           $photo = $request->file('photo')->store('public/vehicles');
           // TODO: $thumb = SomeHelperOrClass::getThumbnail($photo, '300', '300')
           $thumb = $photo;

           $vehicle->update(['photo'=>$photo, 'thumb_photo'=>$thumb]);
        }

        return $vehicle;
    }

    public function updateProfile($request)
    {
        $this->update($request->all());

        //authorized drivers
        $this->authorizedDrivers()->sync($request->authorized_drivers);

        //update photo
        if ($request->file('photo')) {
           $photo = $request->file('photo')->store('public/vehicles');
           // TODO: $thumb = SomeHelperOrClass::getThumbnail($photo, '300', '300')
           $thumb = $photo;

           $this->update(['photo'=>$photo, 'thumb_photo'=>$thumb]);
        }
       
        return $this;
    }

    public function newDriverLoggedIn(DriverProfile $driver)
    {
        $this->current_driver_profile_id = $driver->id;

        $this->status = 'available';

        $this->save();
    }

    public function driverLoggedOut()
    {
        $this->current_driver_profile_id = null;

        $this->status = 'off-duty';

        $this->save();
    }

    public function changeStatus($status) 
    {
        $this->status = $status;
        $this->save();

        return $this;
    }


    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'about'=> $this->about,
            'photo' => url(\Storage::url($this->photo)),
            'thumb_photo' => url(\Storage::url($this->thumb_photo)),
            'vehicle_type'=> [
                'id'=> $this->vehicleType->id,
                'title'=> $this->vehicleType->title
            ],
            'vehicle_registration_number'=> $this->vehicle_registration_number,
            'fitness_number'=> $this->fitness_number,
            'fitness_expiry'=> $this->fitness_expiry,
            'number_plate'=> $this->number_plate,
            'tax_token_number'=> $this->tax_token_number,
            'tax_token_expirey'=> $this->tax_token_expirey,
            'chesis_no'=> $this->chesis_no,
            'engine_capacity_cc'=> $this->engine_capacity_cc,
            'custom_capacity'=> $this->custom_capacity,
            'route_note'=> $this->route_note,
            'status'=> $this->status
        ];
    }    
    
}
