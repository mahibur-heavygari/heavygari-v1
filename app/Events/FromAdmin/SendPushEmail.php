<?php

namespace App\Events\FromAdmin;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Controllers\CMS\CustomEmailController;

class SendPushEmail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $msg;
    public $user;
    public $title;
    public $image;
    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct( $user, $msg, $image=false, $title )
    {
        $this->msg = $msg;
        $this->user = $user;
        $this->title = $title;
        $this->image = $image;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
