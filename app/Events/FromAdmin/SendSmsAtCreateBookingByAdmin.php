<?php

namespace App\Events\FromAdmin;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Booking;

class SendSmsAtCreateBookingByAdmin
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $booking;
    public $is_short_customer;
    public $password;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, $is_short_customer, $password)
    {
        $this->booking = $booking;
        $this->is_short_customer = $is_short_customer;
        $this->password = $password;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
