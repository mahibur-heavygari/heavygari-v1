<?php

namespace App\Events\Booking;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Booking;

class CancelBooking
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $booking;
    public $status;
    public $send_by;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, $status=null, $send_by=null)
    {
        $this->booking = $booking;
        $this->status = $status;
        $this->send_by = $send_by;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
