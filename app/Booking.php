<?php

namespace App;

use Carbon\Carbon;
use App\DriverProfile;
use App\Events\Booking\TripStart;
use App\Events\Booking\TripComplete;
use Illuminate\Support\Facades\Redis;
use App\Events\Booking\DriverAcceptBooking;

class Booking extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['unique_id', 'tracking_code', 'customer_profile_id', 'datetime','payment_by','recipient_name','recipient_phone','particular_details', 'category_type','special_instruction','waiting_time','booking_category','booking_type','trip_type','is_hired_driver','is_payment_collected', 'invoice_pdf', 'delivery_order_photo', 'delivery_invoice_photo', 'first_discounted', 'created_by', 'cancelled_by', 'cancel_reason', 'accepted_by', 'number_of_acceptance', 'admin_note', 'msg_to_owners', 'msg_to_potential_owners', 'is_corporate'];

    protected $dates = ['datetime', 'accepted_at'];

    ///////////////
    // MUTATORS  //
    ///////////////

    /**
     * Convert UTC time to BD time
     */
    public function getDateTimeAttribute($value)
    {
        if ($value==null) 
            return $value;

        $time = new Carbon($value);
        
        if ($this->booking_type=='advance') 
            return $time->timezone('UTC');

        return $time->timezone('Asia/Dhaka');
    }

    /**
     * Convert UTC time to BD time
     */
    public function getAcceptedAtAttribute($value)
    {
        if ($value==null) {
            return $value;
        }
        
        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }

    ////////////////
    // RELATIONS  //
    ////////////////
    
    /**
     * Get vehicle owner from user table
     */
    public function owner()
    {
        return $this->belongsTo('App\OwnerProfile', 'vehicle_owner_profile_id');
    }

    /**
     * Get driver of this booking
     */
    public function driver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    /**
     * Get customer of this booking
     */
    public function customer()
    {
        return $this->belongsTo('App\CustomerProfile', 'customer_profile_id');
    }

    /**
     * Get vehicle of this booking
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle', 'vehicle_id');
    }

    /**
     * Get full booking details of this booking
     */
    public function fullBookingDetails()
    {
        return $this->hasOne('App\FullBookingDetail', 'booking_id');
    }

    /**
     * Get full booking details of this booking
     */
    public function sharedBookingDetails()
    {
        return $this->hasOne('App\SharedBookingDetail', 'booking_id');
    }

    /**
     * Get trips of this booking
     */
    public function trips()
    {
        return $this->hasMany('App\Trip', 'booking_id');
    }

    /**
     * Get trips of this booking
     */
    public function hiredDriver()
    {
        return $this->hasOne('App\HiredDriver', 'booking_id');
    }

    /**
     * Get invoice of this booking
     */
    public function invoice()
    {
        return $this->hasOne('App\BookingInvoice', 'booking_id');
    }

    /**
     * Get earnings of this booking
     */
    public function earnings()
    {
        return $this->hasOne('App\BookingEarning', 'booking_id');
    }

    /**
     * Get customer review of this booking
     */
    public function customerRating()
    {
        return $this->hasOne('App\CustomerRating', 'booking_id');
    }

    /**
     * Get customer review of this booking
     */
    public function driverRating()
    {
        return $this->hasOne('App\DriverRating', 'booking_id');
    }

    public function driverCommissionItem()
    {
        return $this->hasOne('App\DriverCommissionItem', 'booking_id');
    }

    public function ownerInvoiceItem()
    {
        return $this->hasOne('App\OwnerInvoiceItem', 'booking_id');
    }
    
    ////////////
    // SCOPES //
    ////////////
    
    /**
     * current bookings 
     * open + upcoming + ongoing
     */
    public function scopeCurrent($query)
    {
        return $query->whereIn('status', ['open', 'upcoming', 'ongoing']);
    }

    /**
     * Accepted bookings 
     * upcoming + ongoing
     */
    public function scopeAccepted($query)
    {
        return $query->whereIn('status', ['upcoming', 'ongoing']);
    }
     
    /**
     * open bookings
     */
    public function scopeOpen($query)
    {
        return $query->where('status', 'open');
    }

    /**
     * upcoming bookings
     */
    public function scopeUpcoming($query)
    {
        return $query->where('status', 'upcoming');
    }

    /**
     * ongoing bookings
     */
    public function scopeOngoing($query)
    {
        return $query->where('status', 'ongoing');
    }

    /**
     * completed bookings
     */
    public function scopeCompleted($query)
    {
        return $query->where('status', 'completed');
    }

    /**
     * cancelled bookings
     */
    public function scopeCancelled($query)
    {
        return $query->where('status', 'cancelled');
    }

    /**
     * on demand bookings
     */
    public function scopeOnDemand($query)
    {
        return $query->where('booking_type', 'on-demand');
    }

    /**
     * advance bookings
     */
    public function scopeAdvance($query)
    {
        return $query->where('booking_type', 'advance');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        $this->customer = ($this->customer) ? $this->customer->getApiModel() : null;
        $this->fullBookingDetails = ($this->fullBookingDetails) ? $this->fullBookingDetails->getApiModel() : null;
        $this->sharedBookingDetails = ($this->sharedBookingDetails) ? $this->sharedBookingDetails->getApiModel() : null;
        $this->owner = ($this->owner) ? $this->owner->getApiModel() : null;
        $this->driver = ($this->driver) ? $this->driver->getApiModel() : null;
        //$this->vehicle_type = ($this->vehicleType) ? $this->vehicleType->getApiModel() : null;
        $this->vehicle = ($this->vehicle) ? $this->vehicle->getApiModel() : null;
        $this->trip = ($this->trips) ? $this->trips[0]->getApiModel() : null;
        $this->invoice = ($this->invoice) ? $this->invoice->getApiModel() : null;
        $this->customerRating = ($this->customerRating) ? $this->customerRating->getApiModel() : null;
        $this->driverRating = ($this->driverRating) ? $this->driverRating->getApiModel() : null;

        $status_label = ($this->status=='open') ? 'available' : $this->status;

        //check hired drivers
        if(is_null($this->hiredDriver)){
            $driver_is_hired = false;
            $hired_driver_info = null;
        }else{
            $driver_is_hired = true;
            $hired_driver_info = ['name'=>$this->hiredDriver->name, 'phone'=>$this->hiredDriver->phone];
        }

        return [
            'id'=> $this->id,
            'unique_id'=> $this->unique_id,
            'datetime'=> $this->datetime->toDateTimeString(),
            'booking_category'=> $this->booking_category,
            'fullBookingDetails'=> $this->fullBookingDetails,
            'sharedBookingDetails'=> $this->sharedBookingDetails,
            'booking_type'=> $this->booking_type,
            'trip_type'=> $this->trip_type,
            'status'=> $this->status,
            'status_label' => $status_label,
            'recipient_name'=> $this->recipient_name,
            'recipient_phone'=> $this->recipient_phone,
            'particular_details'=> $this->particular_details,
            'waiting_time' => $this->waiting_time,
            'special_instruction'=> $this->special_instruction,
            'payment_by'=> $this->payment_by,
            'trip'=> $this->trip,
            'customer'=> $this->customer,
            'owner'=> $this->owner,
            'driver'=> $this->driver,
            'driver_is_hired'=> $driver_is_hired,
            'hired_driver_info'=> $hired_driver_info,
            'vehicle'=> $this->vehicle,
            'capacity'=> $this->capacity,
            'customer_review' => $this->customerRating,
            'driver_review' => $this->driverRating,
            'fare'=> [
                'total_cost' => $this->invoice['total_cost'],
                'fare_breakdown' => $this->invoice
            ],
            'invoice' => isset($this->invoice_pdf) ? url(\Storage::url($this->invoice_pdf)) : NULL,
            'delivery_order' => isset($this->delivery_order_photo) ? url(\Storage::url($this->delivery_order_photo)) : NULL,
            'delivery_invoice' => isset($this->delivery_invoice_photo) ? url(\Storage::url($this->delivery_invoice_photo)) : NULL,
            'tracking_webview_url' => url('/booking/'.$this->tracking_code.'?view=app_webview'),
            'accepted_at'=> ($this->accepted_at) ? $this->accepted_at->toDateTimeString() : $this->accepted_at,
            'created_at'=> $this->created_at->toDateTimeString(),
            'updated_at'=> $this->updated_at->toDateTimeString(),
        ];
    }

    public function getTransactionModel()
    {
        $this->trip = ($this->trips) ? $this->trips[0]->getApiModel() : null;
        $this->invoice = ($this->invoice) ? $this->invoice->getApiModel() : null;
        $driver_earning_status = ($this->driverCommissionItem) ? $this->driverCommissionItem->getApiModel()['status'] : 'due';

        return [
            'id'=> $this->id,
            'unique_id'=> $this->unique_id,
            'datetime'=> $this->datetime->toDateTimeString(),
            'booking_category'=> $this->booking_category,
            'booking_type'=> $this->booking_type,
            'trip_type'=> $this->trip_type,        
            'fare' => $this->invoice['total_cost'],
            'driver_earning' => $this->earnings['driver_earning'],
            'driver_earning_status' => $driver_earning_status,
            'accepted_at'=> ($this->accepted_at) ? $this->accepted_at->toDateTimeString() : $this->accepted_at,
            'started_at'=> $this->trip['started_at'],
            'completed_at'=> $this->trip['completed_at'],
            'created_at'=> $this->created_at->toDateTimeString(),
            'updated_at'=> $this->updated_at->toDateTimeString(),
        ];
    }

    public function getTrackingModel()
    {
        // TODO : get only minimal values for secuirity reasons
    
        $booking_details = clone($this);

        $booking_details->customer = ($booking_details->customer) ? $booking_details->customer->getApiModel() : null;
        $booking_details->owner = ($booking_details->owner) ? $booking_details->owner->getApiModel() : null;
        $booking_details->driver = ($booking_details->driver) ? $booking_details->driver->getApiModel() : null;
        //$booking_details->vehicle_type = ($booking_details->vehicleType) ? $booking_details->vehicleType->getApiModel() : null;
        $booking_details->vehicle = ($booking_details->vehicle) ? $booking_details->vehicle->getApiModel() : null;
        $booking_details->trip = ($booking_details->trips) ? $booking_details->trips[0]->getApiModel() : null;

        
        $trip_path_history = [];
        if ($booking_details->status=='completed') {
            // Todo : if trip is completed then get from mysql.
            $trip_redis_data = Redis::zrange(config('heavygari.tracker.redis_db_prefix').':trip:trackInfo:'.$booking_details->unique_id, 0, -1);
            $trip_path_history = array_map( function($val){ return json_decode($val); }, $trip_redis_data);

        } else if ($booking_details->status=='ongoing') {
            $trip_redis_data = Redis::zrange(config('heavygari.tracker.redis_db_prefix').':trip:trackInfo:'.$booking_details->unique_id, 0, -1);
            $trip_path_history = array_map( function($val){ return json_decode($val); }, $trip_redis_data);
        }

        return [
            'id'=> $booking_details->id,
            'unique_id'=> $booking_details->unique_id,
            'datetime'=> $booking_details->datetime->toDateTimeString(),
            'booking_type'=> $booking_details->booking_type,
            'vehicle_type'=> $booking_details->vehicle_type,
            'trip_type'=> $booking_details->trip_type,
            'status'=> $booking_details->status,
            'recipient_name'=> $booking_details->recipient_name,
            'recipient_phone'=> $booking_details->recipient_phone,
            'particular_details'=> $booking_details->particular_details,
            'waiting_time' => $booking_details->waiting_time,
            'trip'=> $booking_details->trip,
            'customer'=> $booking_details->customer,
            'owner'=> $booking_details->owner,
            'driver'=> $booking_details->driver,
            'vehicle'=> $booking_details->vehicle,
            'capacity'=> $booking_details->capacity,
            'trip_path_history' => $trip_path_history,            
            'accepted_at'=> ($booking_details->accepted_at) ? $booking_details->accepted_at->toDateTimeString() : $booking_details->accepted_at,
            'created_at'=> $booking_details->created_at->toDateTimeString(),
            'updated_at'=> $booking_details->updated_at->toDateTimeString(),
        ];
    }

    /////////////////////////////
    // Booking Related Actions //
    /////////////////////////////

    public function accept(DriverProfile $driver, Vehicle $vehicle = null)
    {
        // TODO : try/catch?
        
        if ($vehicle) {
            $this->vehicle_id = $vehicle->id;

            $vehicle->current_driver_profile_id = $driver->id;
            $vehicle->save();
        } else {
            if(isset($driver->currentVehicle->id))
            {
                $this->vehicle_id = $driver->currentVehicle->id;
            }
            else{
                $this->vehicle_id = $driver->myAuthorizedVehicles->id;
            }
        }
        
        $this->driver_profile_id = $driver->id;
        $this->vehicle_owner_profile_id = $driver->vehicle_owner_profile_id;  
        $this->number_of_acceptance = $this->number_of_acceptance + 1;        
        $this->accepted_at = Carbon::now();
        $this->status = 'upcoming'; // TODO : advance booking?
        $this->save();

        $this->vehicle->status = 'booked';
        $this->vehicle->save();

        event(new DriverAcceptBooking($this));
    }

    public function start()
    {
        $trip = $this->trips[0];
        $trip->started_at = Carbon::now();
        $trip->save();

        $this->status = 'ongoing';
        $this->save();

        $this->vehicle->status = 'on-trip';
        $this->vehicle->save();

        event(new TripStart($this));
    }

    public function complete()
    {
        $trip = $this->trips[0];
        $trip->completed_at = Carbon::now();
        $trip->save();

        $this->status = 'completed';
        $this->save();

        $this->vehicle->status = 'available';
        $this->vehicle->save();

        event(new TripComplete($this));
    }

    public function cancel($reason=null)
    {
        if($this->status == 'upcoming'){
            $this->vehicle->status = 'available';
            $this->vehicle->save();
        }

        $this->status = 'cancelled';
        $this->cancel_reason = $reason;
        $this->save();

        //update booking if it first discounted
        if($this->first_discounted == 'yes'){
            $this->first_discounted = NULL;
            $this->save();

            $this->customer->first_booking_discount = NULL;
            $this->customer->save();
        }

        if($this->is_corporate=='yes'){
            $this->customer->corporateCustomer->balance = $this->customer->corporateCustomer->balance + $this->invoice->total_cost;
            $this->customer->corporateCustomer->save();
        }
    }

    public function updatePaymentCollection($status)
    {
        $this->is_payment_collected = $status;
        $this->save();
    }
    

    /////////////
    // Helpers //
    /////////////
    
    public function banglaStatus()
    {
        switch ($this->status)  {
            case 'open' :
                return 'উন্মুক্ত';
                break;
            case 'upcoming' :
                return 'আসন্ন';
                break;
            case 'ongoing' :
                return 'চলমান';
                break;
            case 'completed' :
                return 'সম্পন্ন';
                break;
            case 'cancelled' :
                return 'বাতিল';
                break;
            default :
                return $this->status;
        }
    }

}