<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiredDriver extends Model
{
    protected $table = 'hired_drivers';

    protected $fillable = [
        'name', 'phone', 'booking_id', 'vehicle_owner_profile_id', 'driver_profile_id'
    ];

    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * get driver
     */
    public function driver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    /**
     * get owner
     */
    public function owner()
    {
        return $this->belongsTo('App\OwnerProfile', 'vehicle_owner_profile_id');
    }
}
