<?php

namespace App;

class customerRating extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id', 'customer_profile_id', 'review', 'rating'
    ];

    // BEGIN RELATIONS
    
    /**
     * get booking
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * get customer
     */
    public function customer()
    {
        return $this->belongsTo('App\CustomerProfile', 'customer_profile_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'review' => $this->review,
            'rating' => $this->rating,
            //'booking' => $booking,
        ];
    }
}
