<?php

namespace App\Mail\Registration;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\CustomerProfile;

class WelcomeCustomerEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CustomerProfile $customer, $title = false)
    {
        $this->customer = $customer;
        $this->title = $title;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Thank you for registering with HeavyGari')->markdown('emails.registration.welcome_customer');
    }
}
