<?php

namespace App\Mail\Registration;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\OwnerProfile;

class WelcomeOwnerEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $owner;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OwnerProfile $owner, $title=false)
    {
        $this->owner = $owner;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Thank you for registering with HeavyGari')->markdown('emails.registration.welcome_owner');
    }
}
