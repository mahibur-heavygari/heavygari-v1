<?php

namespace App\Mail\Invoice;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\OwnerProfile;
use App\OwnerInvoice;

class OwnerPaymentAcknowledgementEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $owner;

    public $invoice;

    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OwnerProfile $owner, OwnerInvoice $invoice, $title=false)
    {
        $this->owner = $owner;
        $this->invoice = $invoice;
        $this->title = $title;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Heavygari payment acknowledgement')->markdown('emails.owner_invoice.acknowledgement');
    }
}
