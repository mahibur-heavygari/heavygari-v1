<?php

namespace App\Mail\Booking;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Booking;

class NotifyOwnerTripCompleteEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $booking;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, $title=false)
    {
        $this->booking = $booking;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Earnings from your trip - '.$this->booking->unique_id)->markdown('emails.booking.notify_owner');
    }
}
