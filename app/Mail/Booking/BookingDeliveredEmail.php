<?php

namespace App\Mail\Booking;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Booking;

class BookingDeliveredEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $booking;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, $title = false)
    {
        $this->booking = $booking;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Booking '.$this->booking->unique_id.' was delivered')->markdown('emails.booking.delivered');
    }
}
