<?php

namespace App\Mail\PushEmail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\CMS\CustomEmailController;
use App\User;



class BulkEmail extends Mailable implements ShouldQueue
{    
    use Queueable, SerializesModels;

     public $user;
     public $msg;
     public $title;
     public $image;
     
     /**
     * Create a new message instance.
     *
     * @return void
     */


    public function __construct($user, $msg, $image=false, $title=false)
    {
       $this->title = $title;
       $this->user = $user;
       $this->msg = $msg;
       $this->image = $image;

    }


     /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build()
    {
        return $this->subject($this->title)->markdown('emails.custom_email.bulkemail');
    }
}
