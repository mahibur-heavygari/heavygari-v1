<?php

namespace App\Mail\ForgotPassword;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class RecoverPasswordEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;

    public $reminder_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $reminder_code)
    {
        $this->user = $user;
        $this->reminder_code = $reminder_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Change password request')->markdown('emails.forgot_password.recover_password');
    }
}
