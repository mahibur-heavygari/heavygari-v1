<?php

namespace App;

class SharedBookingDetail extends BaseModel
{
    protected $table = 'shared_booking_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_category_id', 'weight_category_id', 'volumetric_category_id', 'weight', 'volumetric_width', 'volumetric_height', 'volumetric_length', 'quantity'
    ];

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * Get productCategory
     */
    public function productCategory()
    {
        return $this->belongsTo('App\BaseProductCategory', 'product_category_id');
    }

    /**
     * Get weightCategory
     */
    public function weightCategory()
    {
        return $this->belongsTo('App\BaseWeightCategory', 'weight_category_id');
    }

    /**
     * Get volumetricCategory 
     */
    public function volumetricCategory()
    {
        return $this->belongsTo('App\BaseVolumetricCategory', 'volumetric_category_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'product_category' => $this->productCategory->getApiModel(),
            'weight_category' => ($this->weight_category_id!=null) ? $this->weightCategory->getApiModel() : null,
            'volumetric_category' => ($this->volumetric_category_id!=null) ? $this->volumetricCategory->getApiModel() : null ,
            'weight' => $this->weight,
            'volumetric_width' => $this->volumetric_width,
            'volumetric_height' => $this->volumetric_height,
            'volumetric_length' => $this->volumetric_length,
            'quantity' => $this->quantity,
        ];
    }
}
