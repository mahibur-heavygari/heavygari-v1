<?php

namespace App;

class DriverPreferenceBookingCategory extends BaseModel
{
    protected $table='driver_preferences_booking_categories';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'full' => 'boolean',
        'shared' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['driver_profile_id', 'full', 'shared'];

    ///////////////
    // RELATIONS //
    ///////////////

    /**
     * getDriver
     */
    public function getDriver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'full'=> $this->full,
            'shared' => $this->shared
        ];
    }
    
}
