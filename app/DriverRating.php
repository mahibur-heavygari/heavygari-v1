<?php

namespace App;

class DriverRating extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id', 'driver_profile_id', 'review', 'rating'
    ];

    ////////////////
    // RELATIONS  //
    ////////////////
    
    /**
     * get booking
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * get driver
     */
    public function driver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'review' => $this->review,
            'rating' => $this->rating,
            //'booking' => $booking,
        ];
    }
}
