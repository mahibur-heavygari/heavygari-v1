<?php

namespace App;

class FullBookingDetail extends BaseModel
{
    protected $table = 'full_booking_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehicle_type_id', 'capacity'
    ];

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * Get parent booking 
     */
    public function vehicleType()
    {
        return $this->belongsTo('App\VehicleType');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'vehicle_type' => $this->vehicleType->getApiModel(),
            'capacity' => $this->capacity
        ];
    }
}
