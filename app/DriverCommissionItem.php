<?php

namespace App;

class DriverCommissionItem extends BaseModel
{
    protected $table = 'driver_commission_items';

    public function commission()
    {
        return $this->belongsTo('App\DriverCommission', 'driver_commission_id');
    }

    public function bookings()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'driver_commission_id' => $this->driver_commission_id,
            'booking_id' => $this->booking_id,
            'payable' => $this->payable,
            'status' => $this->commission->status
        ];
    }
}
