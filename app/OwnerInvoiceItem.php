<?php

namespace App;
 
class OwnerInvoiceItem extends BaseModel
{
    protected $table = 'owner_invoice_items';

    /*
    * InvoiceItem belongs to an invoice
    */
    public function invoice()
    {
        return $this->belongsTo('App\OwnerInvoice', 'invoice_id');
    }

    /*
    * InvoiceItem has many bookings
    */
    public function bookings()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }
    
    public function driverCommissionItem()
    {
        return $this->hasOne('App\DriverCommissionItem', 'booking_id', 'booking_id');
    }

}    
