<?php

namespace App;

class AuthorizedPoint extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'base_point_id', 'arp_no', 'owner_name', 'shop_name', 'nature_of_business', 'address', 'phone', 'fitness_number'
    ];

    // BEGIN RELATIONS
    
    /**
     * Get Point
     */
    public function point()
    {
        return $this->belongsTo('App\Point', 'base_point_id');
    }
    
}
