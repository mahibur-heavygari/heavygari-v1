<?php

namespace App;

class CorporateFullBookingPriceManager extends BaseModel
{
    protected $table = 'corporate_full_booking_price_manager';

    protected $fillable = [
        'corporate_customer_id', 'vehicle_type_id', 'short_trip_rate', 'up_trip_rate', 'down_trip_rate', 'long_up_trip_rate', 'long_down_trip_rate', 'base_fare', 'surcharge_rate', 'discount_percent', 'from_discount_date', 'to_discount_date', 'admin_commission'
    ];

    protected $dates = ['from_discount_date', 'to_discount_date'];

    public function vehicleType()
    {
        return $this->belongsTo('App\VehicleType', 'vehicle_type_id');
    }
}
