<?php

namespace App;

class VehicleType extends BaseModel
{
    protected $table = 'base_vehicle_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'title_bn', 'capacity_type_id', 'capacity', 'max_capacity', 'icon_url'
    ];

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get Owner
     */
    public function capacityType()
    {
        return $this->belongsTo('App\VehicleCapacityType', 'capacity_type_id');
    }

    /**
     * Get Price
     */
    public function fullBookingRate()
    {
        return $this->hasOne('App\FullBookingPriceManager', 'vehicle_type_id');
    }
    
    /////////////
    // Actions //
    /////////////

    public function uploadIcon($request)
    {
        if ($request->file('icon')) {

            $icon_url = $request->file('icon')->store('public/vehicle_types');

            $this->update(['icon_url' => $icon_url]);

            return $icon_url;
        }

        return false;
    }

    /////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'title_bn'=> $this->title_bn,
            'capacity'=> $this->capacity,
            'max_capacity'=> $this->max_capacity,
            'icon_url'=> $this->icon_url ? url(\Storage::url($this->icon_url)) : null,
            'capacity_type'=> [
                'id'=> $this->capacityType->id,
                'title'=> $this->capacityType->title,
            ],
        ];
    }    
}
