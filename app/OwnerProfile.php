<?php

namespace App;

use Sentinel;
use App\Http\Requests\Owner\ProfileFormRequest;

class OwnerProfile extends BaseModel
{
	protected $table = 'profile_vehicle_owners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_no', 'company_name', 'ownership_card_number', 'ownership_card_picture', 'bank_name','bank_branch', 'account_number', 'manager_name', 'manager_phone', 'manager_email', 'note'
    ];

    // BEGIN RELATIONS

    /**
     * user
     */
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * myVehicles
     */
    public function myVehicles()
    {
    	return $this->hasMany('App\Vehicle', 'vehicle_owner_profile_id');
    }

    /**
     * myDrivers
     */
    public function myDrivers()
    {
        return $this->hasMany('App\DriverProfile', 'vehicle_owner_profile_id');
    }

    /**
     * Trips By My Vehicles
     */
    public function myBookings()
    {
    	return $this->hasMany('App\Booking', 'vehicle_owner_profile_id');
    }

    /**
     * My Invoices
     */
    public function myInvoices()
    {
        return $this->hasMany('App\OwnerInvoice', 'owner_id');
    }

    // END OF RELATIONS 
   
    ////////////////////
    // Owner Actions  //
    ////////////////////

    public function completeProfile($request)
    {
        //$this->updateProfile($request);

        //update profile table
        $this->update($request->only('company_name', 'manager_name', 'manager_phone', 'ownership_card_number', 'bank_name','bank_branch', 'account_number'));
        $this->uploadOwnershipCardPicture($request);

        //update user table
        $this->user->update($request->only('address','national_id'));
        $this->user->uploadProfilePicture($request);
        $this->user->uploadNationalId($request);

        $this->profile_complete = 'yes';
        $this->token = uniqid();
        $this->save();

        return $this;
    }
    
    public function updateProfile($request)
    {
        //update profile table
        $this->update($request->only('ref_no', 'company_name', 'manager_name', 'manager_phone', 'ownership_card_number', 'bank_name','bank_branch', 'account_number'));
        $this->uploadOwnershipCardPicture($request);

        //update user table
        $this->user->update($request->only('name','email','address','national_id'));
        $this->user->uploadProfilePicture($request);
        $this->user->uploadNationalId($request);
    }

    public function updateFcmRegToken($token){
        $this->user->update(['web_fcm_registration_token'=>$token]);
    }

    public function uploadOwnershipCardPicture($request)
    {
        if ($request->file('ownership_card_picture')) {
            $photo = $request->file('ownership_card_picture')->store('public/ownership_cards');
            $this->update(['ownership_card_picture' => $photo]);

            return $photo;
        }

        return false;
    }

    public function makeMeDriver()
    {
        // TODO :: need to refactor

        // add driver role
        $role = Sentinel::findRoleBySlug('driver');
        $role->users()->attach($this->user);

        //add driver profile
        $driver_profile = new \App\DriverProfile;
        $driver_profile->user_id = $this->user->id;
        $driver_profile->vehicle_owner_profile_id = $this->id;
        $driver_profile->save();

        # insert default preferences
        
        $driver_profile->bookingTypePreference()->create([
            'on_demand' => true,
            'advance' => true,
        ]);
        
        $driver_profile->bookingCategoryPreference()->create([
            'full' => true,
            'shared' => true,
        ]);

        $driver_profile->bookingDistancePreference()->create([
            'short_distance' => true,
            'long_distance' => true,
        ]);

        return $driver_profile;
    }

    // BEGIN SCOPES

    /**
     * active owners
     */
    public function scopeActive($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->active();
        });
    }

    /**
     * inactive owners
     */
    public function scopeInactive($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->inactive();
        });
    }

    /**
     * blocked owners
     */
    public function scopeBlocked($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->blocked();
        });
    }

    /**
     * owners who do not have mobile apps
     */
    public function scopeWithoutapp($query)
    {
        return $query->where('without_app', 'yes');
    }

    // END OF SCOPES

    // //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'address' => $this->user->address,
            'photo' => url(\Storage::url($this->user->photo)),
            'thumb_photo' =>  url(\Storage::url($this->user->thumb_photo)),
            'national_id' => $this->user->national_id,
            'national_id_photo' => url(\Storage::url($this->user->national_id_photo)),
            'status' => $this->user->status,
            'mobile_verified' => true,
            'created_at' => $this->user->created_at->toDateTimeString(),
            'updated_at' => $this->user->updated_at->toDateTimeString(),
        ];
    }
}
