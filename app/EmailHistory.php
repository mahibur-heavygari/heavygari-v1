<?php

namespace App;

use Hash;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class EmailHistory extends SentinelUser
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'email_history';
    protected $fillable = [
         'email_text', 'email', 'email_type', 'image', 'response', 'other_email', 'single_bulk', 'user_type' 
    ];
}