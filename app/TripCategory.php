<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripCategory extends Model
{
    protected $table = 'base_trip_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'src_point_id', 'dest_point_id', 'trip_category'
    ];

    // BEGIN RELATIONS
    
    /**
     * Source Point
     */
    public function sourcePoint()
    {
        return $this->hasOne('App\Point', 'id', 'src_point_id');
    }

    /**
     * Destination Point
     */
    public function destinationPoint()
    {
        return $this->hasOne('App\Point', 'id', 'dest_point_id');
    }

    // END OF RELATIONS
}
