<?php

namespace App;

class ContactMessage extends BaseModel
{
    protected $table = 'contact_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'message'
    ];
}
