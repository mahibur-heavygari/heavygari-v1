<?php

namespace App;

class DriverPreferenceBookingType extends BaseModel
{
    protected $table='driver_preferences_booking_types';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'on_demand' => 'boolean',
        'advance' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['driver_profile_id', 'on_demand', 'advance'];

    ///////////////
    // RELATIONS //
    ///////////////

    /**
     * getDriver
     */
    public function getDriver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'on_demand'=> $this->on_demand,
            'advance' => $this->advance
        ];
    }
    
}
