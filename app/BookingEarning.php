<?php

namespace App;

class BookingEarning extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id', 'owner_earning', 'driver_earning', 'heavygari_earning', 'ride_sharing_tax'
    ];

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }
}
