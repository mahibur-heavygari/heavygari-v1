<?php

namespace App;

class CorporateCustomer  extends BaseModel
{
    protected $table = 'corporate_customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 'cheque', 'agreement', 'bank_guarantee', 'debit_credit', 'balance', 'limit'
    ];

    public static function registerCustomer($request)
    {
        $request = $request->only('company_name', 'debit_credit');
        /*if(is_null($request['amount']))
        	$request['amount'] = 0;*/

        $corporate_customer = self::create($request);

        return $corporate_customer;
    }

    public function completeProfile($request)
    {
        $this->uploadCheque($request);
        $this->uploadAgreement($request);
        $this->uploadBankGuarantee($request);

        return $this;
    }

    public function uploadCheque($request) 
    {
        if ($request->file('cheque')) {
            $image_url = $request->file('cheque')->store('public/corporate_customer/cheque');
            $this->update(['cheque' => $image_url]);
            return $image_url;
        }
        return false;
    }

    public function uploadAgreement($request) 
    {
        if ($request->file('agreement')) {
            $image_url = $request->file('agreement')->store('public/corporate_customer/agreement');
            $this->update(['agreement' => $image_url]);
            return $image_url;
        }
        return false;
    }

    public function uploadBankGuarantee($request) 
    {
        if ($request->file('bank_guarantee')) {
            $image_url = $request->file('bank_guarantee')->store('public/corporate_customer/bank_guarantee');
            $this->update(['bank_guarantee' => $image_url]);
            return $image_url;
        }
        return false;
    }

    public function customers()
    {
        return $this->belongsToMany('App\CustomerProfile', 'rel_corporate_customers')->withPivot('user_type');
    }
}
