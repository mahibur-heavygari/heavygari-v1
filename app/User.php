<?php

namespace App;

use Hash;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class User extends SentinelUser
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'phone', 'password', 'name', 'email', 'address', 'national_id', 'national_id_photo', 'photo', 'thumb_photo', 'status', 'device_type', 'fcm_registration_token', 'web_fcm_registration_token', 'sms'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Array of login column names.
     * can be logged in with any of these.
     *
     * @var array
     */
    protected $loginNames = [
        'phone', 'email'
    ];

    //////////////////////////
    // Accessors & Mutators //
    //////////////////////////
   
    // photo
    public function getPhotoAttribute($value)
    {   
        if (!$value) {
            return 'public/avatars/default.png';
        }

        return $value;
    }

    // thumb photo
    public function getThumbPhotoAttribute($value)
    {   
        if (!$value) {
            return 'public/avatars/default.png';
        }
        
        return $value;
    }

    /////////////////////////////////
    // Convert UTC time to BD time //
    /////////////////////////////////
   
    // created at
    public function getCreatedAtAttribute($value)
    {   
        if ($value==null) {
            return $value;
        }

        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }

    // updated at
    public function getUpdatedAtAttribute($value)
    {   
        if ($value==null) {
            return $value;
        }
        
        $dhaka_time = new Carbon($value);
        return $dhaka_time->timezone('Asia/Dhaka');
    }

    ///////////////
    // RELATIONS //
    ///////////////

    /**
     * Get user's owner profile
     */
    public function ownerProfile()
    {
        return $this->hasOne('App\OwnerProfile');
    }

    /**
     * Get user's driver profile
     */
    public function driverProfile()
    {
        return $this->hasOne('App\DriverProfile');
    }

    /**
     * Get user's passneger profile
     */
    public function customerProfile()
    {
        return $this->hasOne('App\CustomerProfile');
    }

    /**
     * Get user's admin profile
    */
    public function adminProfile()
    {
        return $this->hasOne('App\AdminProfile');
    }

    /**
     * Sentinel based relations can be found at Cartalyst\Sentinel\Users
     * ie, roles(), activations(), permission() etc
     */
    
    
    ////////////
    // SCOPES //
    ////////////
    
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeInActive($query)
    {
        return $query->where('status', 'inactive');
    }

    public function scopeBlocked($query)
    {
        return $query->where('status', 'blocked');
    }

    //////////////
    // ACTIONS  //
    //////////////

    public function uploadProfilePicture($request)
    {
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public/avatars');
            // TODO: $thumb = SomeHelperOrClass::getThumbnail($photo, '300', '300')
            $thumb = $photo;
            $this->update(['photo' => $photo, 'thumb_photo' => $thumb]);

            return $thumb;
        }

        return false;
    }

    public function uploadNationalId($request)
    {
        if ($request->file('national_id_photo')) {
           $photo = $request->file('national_id_photo')->store('public/national_ids');
           $this->update(['national_id_photo' => $photo]);

           return $photo;
        }

        return false;
    }

    public function changePassword($request)
    {
        if (!Hash::check($request->old_password, $this->password)) {
           return ['error' => 'Sorry, Old password is incorrect'];
        }

        $this->password = Hash::make($request->password);
        $this->save();

        return true;
    }
}
