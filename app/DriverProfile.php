<?php

namespace App;

use App\MyLibrary\UserLib\Registration\DriverRegistration;

class DriverProfile extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_no', 'about', 'license_no', 'license_image', 'license_date_of_issue', 'license_date_of_expire', 'license_issuing_authority'
    ];

    protected $table = 'profile_drivers';
    
    ///////////////
    // RELATIONS //
    ///////////////

    /**
     * getUser
     */
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
   
    /**
     * myAuthorizedVehicles
     */
    public function myAuthorizedVehicles()
    {
        return $this->belongsToMany('App\Vehicle', 'rel_vehicles_drivers');
    }

    /**
     * currentVehicle
     */
    public function currentVehicle()
    {
        return $this->hasOne('App\Vehicle', 'current_driver_profile_id');
    }

    /**
     * getOwner
     */
    public function myOwner()
    {
        return $this->hasOne('App\OwnerProfile', 'id', 'vehicle_owner_profile_id');
    }

    /**
     * myTrips
     */
    public function myTrips()
    {
    	return $this->hasMany('App\Booking', 'driver_profile_id');
    }

    /**
     * myRatings
     */
    public function myRatings()
    {
        return $this->hasMany('App\DriverRating', 'driver_profile_id');
    }

    /**
     * bookingTypePreference
     */
    public function bookingTypePreference()
    {
        return $this->hasOne('App\DriverPreferenceBookingType', 'driver_profile_id');
    }

    /**
     * bookingCategoryPreference
     */
    public function bookingCategoryPreference()
    {
        return $this->hasOne('App\DriverPreferenceBookingCategory', 'driver_profile_id');
    }

    /**
     * bookingDistancePreference
     */
    public function bookingDistancePreference()
    {
        return $this->hasOne('App\DriverPreferenceBookingDistance', 'driver_profile_id');
    }

    /////////////
    // Helpers //
    /////////////

    /**
     * myAverageRating
     */
    public function myAverageRating()
    {
        $my_ratings = $this->myRatings;
        $avg_rating = $my_ratings->average('rating');
        $avg_rating = (float) number_format($avg_rating, 2);

        return $avg_rating;
    }

    /**
     * isPhoneVerified
     */
    public function isPhoneVerified()
    {
        return isset($this->user->activations[0]->completed) ? ($this->user->activations[0]->completed) : false;
    }


    //////////////////////////////
    // Repository Like Methods  //
    //////////////////////////////

    public static function registerDriver($request, $owner)
    {
        $request = $request->only('ref_no', 'name','email', 'phone', 'password', 'address','national_id','about','license_no','license_date_of_issue','license_date_of_expire','license_issuing_authority');

        $request['vehicle_owner_profile_id'] = $owner->id;

        // register driver in my owner
        $registration = new DriverRegistration($request);
        $driver = $registration->registerDriver();

        if (!$driver) {
            // $result['error']->getMessage()
            $notification = ['danger','Sorry! Could not add driver.'];
            session()->flash('message', $notification);

            return redirect()->back();
        }

        return $driver;
    }

    public function completeProfile($request)
    {
        $this->updateProfile($request);

        $this->profile_complete = 'yes';
        $this->save();

        return $this;
    }

    public function updateProfile($request)
    {
        //update user table
        $this->user->update($request->only( 'name','email','address', 'national_id'));
        $this->user->uploadProfilePicture($request);
        $this->user->uploadNationalId($request);

        //authorized vehicles
        $this->myAuthorizedVehicles()->sync($request->authorized_vehicles);

        // update profile table
        $this->update($request->only('ref_no', 'about','license_no','license_date_of_issue','license_date_of_expire','license_issuing_authority'));
        $this->uploadLicense($request);

        return $this;
    }

    public function updateFcmToken($request)
    {
        //update user table fcm_registration_token column
        $this->user->update(['fcm_registration_token' => $request->fcm_registration_token]);
        return $this;
    }

    public function uploadLicense($request) 
    {
        if ($request->file('license_image')) {
            $image_url = $request->file('license_image')->store('public/licenses');
            $this->update(['license_image' => $image_url]);

            return $image_url;
        }

        return false;
    }

    public function updatePreferences($request) 
    {
        $values = $request->all();
        $items = array('full','shared','short_distance','long_distance', 'on_demand', 'advance');

        foreach ($items as $item) {
            $values[$item] = ( isset($values[$item]) && ( $values[$item] == 'on' || $values[$item] == 'true' ) ) ? 1 : 0;
        }

        $this->bookingCategoryPreference->update($values);
        $this->bookingTypePreference->update($values);
        $this->bookingDistancePreference->update($values);

        return true;
    }

    ////////////////////
    // Driver Actions //
    ////////////////////
    
    public function enterTheVehicle(Vehicle $vehicle)
    {
        $vehicle->newDriverLoggedIn($this);
    }

    public function exitTheVehicle()
    {
        $vehicle = $this->currentVehicle;

        if (!$vehicle) {
            return false; 
        }

        $vehicle->driverLoggedOut();
    }

    public function getCurrentMode()
    {
        $vehicle = $this->currentVehicle;

        $mode = ($vehicle) ? ( ($vehicle->status=='off-duty') ? 'off' : 'on' ) : 'off';

        return [
            'duty_mode' => $mode,
            'vehicle' => $vehicle
        ];
    }

    public function onDutyMode()
    {
        $vehicle = $this->currentVehicle;

        if (!$vehicle) {
            return false;
        }

        $vehicle->changeStatus('available');

        return true;
    }

    public function offDutyMode()
    {
        $vehicle = $this->currentVehicle;

        if (!$vehicle) {
            return false;
        }

        $vehicle->changeStatus('off-duty');

        return true;
    }

    public function rateCustomer(Booking $booking, $rating, $review)
    {
        // authorized booking?
        if ($booking->driver_profile_id != $this->id) {
            return [
                'error' => 'Sorry, This trip does not belongs to you!'
            ];
        }

        // already reviewed?
        $already_reviewed = \App\CustomerRating::where('booking_id', $booking->id)->first();
        if ($already_reviewed) {
            return [
                'error' => 'Sorry, You already rated this trip!'
            ];
        }
        
        $customer_review = new \App\CustomerRating;

        $customer_review->booking_id = $booking->id;
        $customer_review->customer_profile_id = $booking->customer_profile_id;
        $customer_review->rating = $rating;
        $customer_review->review = $review;
        $customer_review->save();

        return true;
    }

    public function availableBookingsForMe($booking_type = false, $trip_type = false)
    {
        $current_vehicle_type_id = $this->currentVehicle->vehicle_type_id;

        $bookings = Booking::open();

        if ($booking_type) {
            $bookings = $bookings->where('booking_type', $booking_type);
        }

        if ($trip_type) {
            $bookings = $bookings->where('trip_type', $trip_type);
        }

        $bookings->orderBy('datetime', 'asc');

        $bookings = $bookings->get();

        $bookings = $bookings->filter(function ($value, $key) use ($current_vehicle_type_id){

            if(is_null($value->fullBookingDetails))
                return false;

            if($value->fullBookingDetails->vehicle_type_id == $current_vehicle_type_id)
                return $value;
        })->values();
        
        return $bookings;
    }

    /////////////
    // SCOPES  //
    /////////////

    /**
     * active drivers
     */
    public function scopeActive($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->active();
        });
    }

    /**
     * inactive drivers
     */
    public function scopeInactive($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->inactive();
        });
    }

    /**
     * blocked drivers
     */
    public function scopeBlocked($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->blocked();
        });
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'address' => $this->user->address,
            'photo' => url(\Storage::url($this->user->photo)),
            'thumb_photo' =>  url(\Storage::url($this->user->thumb_photo)),
            'national_id' => $this->user->national_id,
            'national_id_photo' => url(\Storage::url($this->user->national_id_photo)),
            'license_no' => $this->license_no,
            'license_image' => url(\Storage::url($this->license_image)),
            'license_date_of_issue' => $this->license_date_of_issue,
            'license_date_of_expire' => $this->license_date_of_expire,
            'license_issuing_authority' => $this->license_issuing_authority,
            'status' => $this->user->status,
            'mobile_verified' => true,
            'average_rating' => $this->myAverageRating(),
            'fcm_registration_token' => $this->user->fcm_registration_token,
            'created_at' => $this->user->created_at->toDateTimeString(),
            'updated_at' => $this->user->updated_at->toDateTimeString(),
        ];
    }    
}
