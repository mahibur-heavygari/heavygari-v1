<?php

namespace App;

class DriverPreferenceBookingDistance extends BaseModel
{
    protected $table='driver_preferences_distance_types';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'short_distance' => 'boolean',
        'long_distance' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['driver_profile_id', 'short_distance', 'long_distance'];

    ///////////////
    // RELATIONS //
    ///////////////

    /**
     * getDriver
     */
    public function getDriver()
    {
        return $this->belongsTo('App\DriverProfile', 'driver_profile_id');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'short_distance'=> $this->short_distance,
            'long_distance' => $this->long_distance
        ];
    }
    
}
