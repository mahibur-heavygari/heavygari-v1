<?php

namespace App;

class BaseVolumetricCategory  extends BaseModel
{
    protected $table='base_volumetric_category';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'height', 'width', 'length', 'volumetric_weight'
    ];

    /////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'height'=> (float) $this->height,
            'width'=> (float) $this->width,
            'length'=> (float) $this->length,
            'volumetric_weight'=> (float) $this->volumetric_weight
        ];
    }
}
