<?php

namespace App;

class UsersComplain extends BaseModel
{
    protected $table = 'user_complain';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note', 'name', 'user_type', 'phone', 'status', 'android', 'ios', 'web', 'others', 'network',
    ];
    
}
