<?php

namespace App;

class SmsHistory extends BaseModel
{
    protected $table = 'sms_history';

    protected $fillable = [
        'sms_text', 'to', 'response', 'single_bulk', 'user_type'
    ];
}
