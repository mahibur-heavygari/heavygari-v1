<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteVehicleWiseCharge extends Model
{
    protected $table = 'route_vehicle_wise_charge';

    protected $fillable = [
        'trip_category_id', 'vehicle_type_id', 'shared_or_full', 'multiplier', 'distance_type', 'surcharge',
    ];
}
