<?php

namespace App;

class OtherUserPhone extends BaseModel
{
    protected $table = 'other_user_phones';

    protected $fillable = [
        'phone', 'user_type' 
    ];
}
