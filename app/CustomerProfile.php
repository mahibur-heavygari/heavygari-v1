<?php

namespace App;

use App\MyLibrary\BookingLib\BookingFacade;

class CustomerProfile extends BaseModel
{
    protected $table = 'profile_customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'about', 'first_booking_discount', 'corporate_customer_id'
    ];

    ////////////////
    // RELATIONS  //
    ////////////////

    /**
     * user
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * myBookings
     */
    public function myBookings()
    {
        return $this->hasMany('App\Booking', 'customer_profile_id');
    }

    /**
     * myRatings
     */
    public function myRatings()
    {
        return $this->hasMany('App\CustomerRating', 'customer_profile_id');
    }

    //////////////
    // Actions  //
    //////////////

    public function updateProfile($request)
    {
        //update profile table
        $this->update($request->only('about'));

        //update user table
        $this->user->update($request->only('name', 'email', 'address', 'national_id'));
        $this->user->uploadProfilePicture($request);
        $this->user->uploadNationalId($request);

        return $this;
    }

    
    public function updateFcmToken($request)
    {
        //update user table fcm_registration_token column
        $this->user->update(['fcm_registration_token' => $request->fcm_registration_token]);
        return $this;
    }

    public function updateFcmRegToken($token){
        $this->user->update(['web_fcm_registration_token'=>$token]);
    }

    public function completeProfile($request)
    {
        //update profile table
        $this->update($request->only('about'));

        //update user table
        $this->user->update($request->only('address', 'national_id'));
        $this->user->uploadProfilePicture($request);
        $this->user->uploadNationalId($request);

        return $this;
    }

    public function completeAtRegistration()
    {
        $this->profile_complete = 'yes';
        $this->token = uniqid();
        $this->save();

        return $this;
    }

    /*
    public function createNewBooking($request)
    {
        // prepare fields
        $booking_fields = BookingFacade::buildFields($request);

        // do booking
        $booking_fields['customer_profile_id'] = $this->id;
        $booking_result = BookingFacade::bookThis($booking_fields);

        return $booking_result;
    }
    */

    public function rateDriver(Booking $booking, $rating, $review)
    {
        // authorized booking?
        if ($booking->customer_profile_id != $this->id) {
            return [
                'error' => 'Sorry, This bookings does not belongs to you!'
            ];
        }

        // already reviewed?
        $already_reviewed = \App\DriverRating::where('booking_id', $booking->id)->first();
        if ($already_reviewed) {
            return [
                'error' => 'Sorry, You already rated this booking!'
            ];
        }

        $driver_review = new \App\DriverRating;

        $driver_review->booking_id = $booking->id;
        $driver_review->driver_profile_id = $booking->driver_profile_id;
        $driver_review->rating = $rating;
        $driver_review->review = $review;
        $driver_review->save();

        return true;
    }

    public function myAverageRating()
    {
        $my_ratings = $this->myRatings;
        $avg_rating = $my_ratings->average('rating');
        $avg_rating = (string) number_format($avg_rating, 2);

        return $avg_rating;
    }

    ////////////
    // SCOPES //
    ////////////

    /**
     * active customers
     */
    public function scopeActive($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->active();
        });
    }

    /**
     * inactive customers
     */
    public function scopeInactive($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->inactive();
        });
    }

    /**
     * blocked customers
     */
    public function scopeBlocked($query)
    {
        return $query->whereHas('user', function ($query) {
            $query->blocked();
        });
    }

    public function corporate()
    {
        return $this->belongsToMany('App\CorporateCustomer', 'rel_corporate_customers');
    }

    public function corporateCustomer()
    {
        return $this->belongsTo('App\CorporateCustomer');
    }

    //////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'address' => $this->user->address,
            'about' => $this->about,
            'photo' => url(\Storage::url($this->user->photo)),
            'thumb_photo' =>  url(\Storage::url($this->user->thumb_photo)),
            'national_id' => $this->user->national_id,
            'national_id_photo' => url(\Storage::url($this->user->national_id_photo)),
            'status' => $this->user->status,
            'mobile_verified' => true,
            'average_rating' => $this->myAverageRating(),
            'fcm_registration_token' => $this->user->fcm_registration_token,
            'created_at' => $this->user->created_at->toDateTimeString(),
            'updated_at' => $this->user->updated_at->toDateTimeString(),
        ];

    }
}
