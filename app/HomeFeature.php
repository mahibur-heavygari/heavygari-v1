<?php

namespace App;

class HomeFeature extends BaseModel
{
    protected $table = 'home_features';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_en', 'text_en', 'title_bn', 'text_bn', 'icon', 'comming_soon' 
    ];

    /////////////
    // Actions //
    /////////////

    public function uploadIcon($request)
    {
        if ($request->file('icon')) {

            $icon_url = $request->file('icon')->store('public/home');

            $this->update(['icon' => $icon_url]);

            return $icon_url;
        }

        return false;
    }
}
