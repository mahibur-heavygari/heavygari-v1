<?php

namespace App\MyLibrary\BookingLib\CostCalculator;

use App\TripCategory;
use App\VehicleType;

class FullBookingCalculator extends FareCalculator
{
	private $distance;
	private $duration;
	private $distance_type;
	private $trip_category;
	private $vehicle_fullbooking_rate;
	private $booking_type;
	private $datetime;
	protected $customer;

	public function __construct($duration, $distance, $distance_type, $trip_type, $trip_category, $vehicle_type, $customer, $booking_type, $datetime)
	{
		$this->distance = $distance;
		$this->duration = $duration;
		$this->distance_type = $distance_type;
		$this->trip_type = $trip_type;
		$this->trip_category = $trip_category;
		$this->vehicle_fullbooking_rate = VehicleType::where('id', $vehicle_type)->first()->fullBookingRate;
		$this->customer = $customer;
		$this->booking_type = $booking_type;
		$this->datetime = $datetime;
	}

	/**
	 * Calculate Base Fare
	 */
	protected function getBaseFare()
	{
		return $base_fare = ($this->distance_type=='short') ? $this->vehicle_fullbooking_rate['base_fare'] : 0;
	}

	/**
	 * Calculate Distance Cost
	 */
	protected function getDistanceCost()
	{
		$distance_cost = $this->getPerKiloRate() * $this->distance;
		
		return round($distance_cost);
	}

	/**
	 * Calculate waiting Cost
	 */
	protected function getWaitingCost(){
		$waiting_cost = 0;

		if($this->distance_type=='normal' || $this->distance_type=='long'){
			return $waiting_cost;
		}

		$waiting_cost = $this->duration / 60;
		if ($this->trip_type=='round') {
			$waiting_cost *= 2;
		}
		return round($waiting_cost);
	}

	/**
	 * Get the Per Kilo meter Rate
	 * 
	 * FULL DISCUSS GOES HERE...
	 */
    protected function getPerKiloRate()
    {
		if ($this->distance_type=='short') {
			$per_km_rate = ($this->trip_type=='round') ? ( ($this->vehicle_fullbooking_rate['short_trip_rate'] * $this->trip_category['up_trip_multiplier']) + ($this->vehicle_fullbooking_rate['short_trip_rate'] * $this->trip_category['down_trip_multiplier'])) : ($this->vehicle_fullbooking_rate['short_trip_rate'] * $this->trip_category['multiplier']);

		} else if ($this->distance_type=='normal') {
			$per_km_rate = ($this->trip_type=='round') ? ( $this->vehicle_fullbooking_rate['up_trip_rate'] * $this->trip_category['up_trip_multiplier'] + ($this->vehicle_fullbooking_rate['down_trip_rate'] * $this->trip_category['down_trip_multiplier'] * .30)  ) : ($this->vehicle_fullbooking_rate[$this->trip_category['category'].'_trip_rate'] * $this->trip_category['multiplier']);
			
		} else if ($this->distance_type=='long') {
			$per_km_rate = ($this->trip_type=='round') ? ( $this->vehicle_fullbooking_rate['long_up_trip_rate'] * $this->trip_category['up_trip_multiplier'] + ($this->vehicle_fullbooking_rate['long_down_trip_rate'] * $this->trip_category['down_trip_multiplier'] * .30) ) : ($this->vehicle_fullbooking_rate['long_'.$this->trip_category['category'].'_trip_rate'] * $this->trip_category['multiplier']);
		}

		return round($per_km_rate);
    }

    /**
	 * Calculate Surcharge
	 */
	protected function getSurchargeCost()
	{
		$surcharge = $this->vehicle_fullbooking_rate['surcharge_rate'] * $this->distance;

		return round($surcharge);
	}

	/**
	 * Calculate HeavyGari Fee
	 */
	protected function getHeavyGariFee($total)
	{
		$heavygari_percent = $this->vehicle_fullbooking_rate['admin_commission'];

		$heavygari_fee = $total * ($heavygari_percent/100);

		return round($heavygari_fee);
	}

    /**
	 * Calculate Discount
	 */
	protected function getDiscountAmount($total_fare)
	{
		$from_discount_date = $this->vehicle_fullbooking_rate['from_discount_date'];
		$to_discount_date = $this->vehicle_fullbooking_rate['to_discount_date'];
		
		if( (!is_null($from_discount_date) || !is_null($to_discount_date)) && !isset($this->booking_type) ){
			$discount = 0;
			
		}elseif( (!is_null($from_discount_date) || !is_null($to_discount_date)) && isset($this->booking_type) ){
			
			if( $this->booking_type == 'on-demand' ){
				$datetime = (string)( $this->datetime )->timezone('Asia/Dhaka');
			}else{
				$datetime = (string)$this->datetime;
			}

			$discount = 0;

			if( !is_null($from_discount_date) && !is_null($to_discount_date) ){
				if( strtotime($from_discount_date) <= strtotime($datetime) && strtotime($to_discount_date) >= strtotime($datetime) ){
					$discount_percent = $this->vehicle_fullbooking_rate['discount_percent'];
					$discount = $total_fare * ($discount_percent/100);
					$discount =  round($discount);
				}

			}elseif( !is_null($from_discount_date) ){
				if( strtotime($from_discount_date) <= strtotime($datetime) ){
					$discount_percent = $this->vehicle_fullbooking_rate['discount_percent'];
					$discount = $total_fare * ($discount_percent/100);
					$discount =  round($discount);
				}
				
			}elseif( !is_null($to_discount_date) ){
				if( strtotime($to_discount_date) >= strtotime($datetime) ){
					$discount_percent = $this->vehicle_fullbooking_rate['discount_percent'];
					$discount = $total_fare * ($discount_percent/100);
					$discount =  round($discount);
				}				
			}

		}else{
			$discount_percent = $this->vehicle_fullbooking_rate['discount_percent'];
			$discount = $total_fare * ($discount_percent/100);
			$discount =  round($discount);
		}

		if($discount > 0){
			$off_amount = 0;
		}elseif (is_null($this->customer)) {
			$off_amount = 0;
		}elseif (!is_null($this->customer)) {
			if(is_null($this->customer->first_booking_discount)){
				$discount = 500;
				$off_amount = 500;
			}else{
				$off_amount = 0;
			}
		}
		return compact( 'discount', 'off_amount' );
	}

	protected function getRideSharingTaxAmount( $total_fare )
	{
		//Ride sharing tax is 7%
		$tax = $total_fare * 0;

		return $tax;
	}
}