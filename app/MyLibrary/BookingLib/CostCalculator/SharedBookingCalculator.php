<?php

namespace App\MyLibrary\BookingLib\CostCalculator;

use App\SharedBookingPriceManager;

class SharedBookingCalculator extends FareCalculator
{
	private $distance;
	private $distance_type;	
	private $product_parameters;

	public function __construct($distance, $distance_type, $product_parameters)
	{
		$this->distance = $distance;
		$this->distance_type = $distance_type;
		$this->product_parameters = $product_parameters;
		$this->price_rate = $this->getPriceRate();
	}

	/**
	 * Get Price Rate from DB
	 */
	protected function getPriceRate()
	{
		$price_rate = SharedBookingPriceManager::where('min_weight', '<=', $this->product_parameters['final_weight'])->where('max_weight', '>=', $this->product_parameters['final_weight'])->first();

		return $price_rate;
	}

	/**
	 * Calculate Base Fare
	 */
	protected function getBaseFare()
	{
		return $this->price_rate['base_fare'];
	}

	/**
	 * Calculate Pickup Charge
	 */
	protected function getPickupCharge()
	{
		return $this->price_rate['pickup_charge'];
	}

	/**
	 * Calculate Dropoff Charge
	 */
	protected function getDropOffCharge()
	{
		return $this->price_rate['drop_off_charge'];
	}

	/**
	 * Calculate Distance Cost
	 */
	protected function getDistanceCost()
	{
		// get rate for distance type (short trip, long trip etc)
		$trip_rate = $this->price_rate[$this->distance_type.'_trip_rate']; 

		$distance_cost = $trip_rate * $this->distance * $this->product_parameters['quantity'];

		return round($distance_cost);
	}

	/**
	 * Calculate Weight Cost
	 */
	protected function getWeightCost()
	{
		$weight_cost = $this->price_rate['weight_rate'] * $this->product_parameters['final_weight'];

		return round($weight_cost);
	}

	/**
	 * Calculate Surcharge
	 */
	protected function getSurchargeCost()
	{
		$surcharge_cost = $this->price_rate['surcharge_rate'] * $this->distance;

		return round($surcharge_cost);
	}

	/**
	 * Calculate waiting Cost
	 */
	protected function getWaitingCost(){
		return 0;
	}

	/**
	 * Calculate HeavyGari Fee
	 */
	protected function getHeavyGariFee($total_fare)
	{
		$heavygari_percent = $this->price_rate['admin_commission'];

		$heavygari_fee = $total_fare * ($heavygari_percent/100);

		return round($heavygari_fee);
	}

    /**
	 * Calculate Discount
	 */
	protected function getDiscountAmount($total_fare)
	{
		$discount_percent =  $this->price_rate['discount_percent'];

		$discount = $total_fare * ($discount_percent/100);

		return round($discount);
	}

}