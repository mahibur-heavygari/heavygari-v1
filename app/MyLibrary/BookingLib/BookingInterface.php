<?php

namespace App\MyLibrary\BookingLib;


interface BookingInterface
{
	public function getEstimatedFare($booking_info, $customer);

	public function create($booking_info, $customer);
}