<?php

namespace App\MyLibrary\BookingLib;

use DB;
use App\Booking;
use App\BaseWeightCategory;
use App\BaseVolumetricCategory;
use App\MyLibrary\GoogleMapLib\GoogleMapFacade;
use App\MyLibrary\BookingLib\CostCalculator\SharedBookingCalculator;

class SharedBooking implements BookingInterface
{
	private $booking_info;
	private $product_parameters;
	private $distance_type;
	private $distance;
	private $duration;
	private $customer;

	/**
	 * Determine and set some attributes
	 */
	public function setter($booking_info)
	{
		$this->booking_info = $booking_info;
		$this->product_parameters = $this->prepareProductParameters();
		$distance_duration =$this->getBookingDistanceDuration();
		$this->distance = $distance_duration['distance'];
		$this->duration = $distance_duration['duration'];
		$this->distance_type = $this->getDistanceType();
	}

	/**
	 * Get cost breakdown
	 * 
	 * @return Array
	 */
	public function getEstimatedFare($booking_info)
	{
		$this->setter($booking_info);

		$calculator = new SharedBookingCalculator($this->distance, $this->distance_type, $this->product_parameters);

		$fare_breakdown = $calculator->getFareBreakDown();

		$fare_breakdown = [
			'fare' => [
				'total_cost' => $fare_breakdown['total_cost'],
				'fare_breakdown' => $fare_breakdown
			],
	        'route' => [
	        	'distance' => $this->distance,
	        	'distance_type' => $this->distance_type,
	        	'category' => null,
	        	'orgigin' => [
	                'address' => $booking_info['from_address'],
	                'point' => $booking_info['from_point'],
	                'coordinators' => [
	                    'lat' => (float) $booking_info['from_lat'],
	                    'lon' => (float) $booking_info['from_lon']
		            ]
	            ],
	            'destination' => [
	                'address' => $booking_info['to_address'],
	                'point' => $booking_info['to_point'],
	                'coordinators' => [
	                    'lat' => (float) $booking_info['to_lat'],
	                    'lon' => (float) $booking_info['to_lon']
	                ]
	            ]
	        ]
		];
		
		return $fare_breakdown;
	}

	/**
	 * Create a Shared Booking
	 * 
	 * @return Array
	 */
	public function create($booking_info)
	{
		try {

			$this->setter($booking_info);

			$calculator = new SharedBookingCalculator($this->distance, $this->distance_type, $this->product_parameters);
			$cost_breakdown = $calculator->getFullCostBreakDown();


			$booking_info['booking_category'] = 'shared';
	 		$booking_info['distance'] = $this->distance;
	 		//$booking_info['duration'] = $this->distance;
	        $booking_info['distance_type'] = $this->distance_type;
	        $booking_info['weight_category_id'] = $this->product_parameters['weight_category_id'];
	        $booking_info['volumetric_category_id'] = $this->product_parameters['volumetric_category_id'];
	        $booking_info['weight'] = $this->product_parameters['final_weight'];

	        $db_records['booking'] = $booking_info; 
	        $db_records['invoice'] = $cost_breakdown['invoice'];
	        $db_records['earnings'] = $cost_breakdown['earnings'];

	        // insert into database tables
	        
	        DB::beginTransaction();
	        
	        $booking = Booking::create($db_records['booking']);       
	        $shared_booking_details = $booking->sharedBookingDetails()->create($db_records['booking']);
	        $trip = $booking->trips()->create($db_records['booking']);
	        $invoice = $booking->invoice()->create($db_records['invoice']);
	        $earnings = $booking->earnings()->create($db_records['earnings']);

	        DB::commit();

	    } catch(\Exception $e) {
	    	DB::rollBack();
	    	return false;
	    }
        
        return $booking;
	}

	// SETTERS 

    /**
     * Determine and set the distance from Source to Destination
     * 
     * @return Array
     */
    private function getBookingDistanceDuration()
    {
        $distance_duration = GoogleMapFacade::getDistanceWithDuration($this->booking_info['from_lat'].','.$this->booking_info['from_lon'] , $this->booking_info['to_lat'].','.$this->booking_info['to_lon'] );

        return $distance_duration;
    }

    /**
	 * Determine and set the Distance Type (short, normal..)
	 */
	private function getDistanceType()
	{
		$distance_type = 'normal';

		$shortest_distance = config('heavygari.distance.shared_booking.shortest');
		$short_distance = config('heavygari.distance.shared_booking.short');
		$long_distance = config('heavygari.distance.shared_booking.long');

		if ($this->distance < $shortest_distance) {
			$distance_type = 'shortest';
		} else if ($this->distance < $short_distance) {
			$distance_type = 'short';
		} else if ($this->distance > $long_distance) {
			$distance_type = 'long';
		}

		return $distance_type;
	}

	/**
	 * Determine and set the product realated parameters
	 */
	private function prepareProductParameters()
	{
		$product_parameters['product_category_id'] = $this->booking_info['product_category_id'];
		$product_parameters['quantity'] = $this->booking_info['quantity'];

		$product_parameters['weight_category_id'] = $this->booking_info['weight_category_id']!=0 ? $this->booking_info['weight_category_id'] : null;
		$product_parameters['weight'] = ($product_parameters['weight_category_id']) ? null : $this->booking_info['weight'];

		$product_parameters['volumetric_category_id'] = $this->booking_info['volumetric_category_id'] !=0 ? $this->booking_info['volumetric_category_id'] : null;
		$product_parameters['volumetric_width'] = ($product_parameters['volumetric_category_id']) ? null : $this->booking_info['volumetric_width'];
		$product_parameters['volumetric_height'] = ($product_parameters['volumetric_category_id']) ? null : $this->booking_info['volumetric_height'];
		$product_parameters['volumetric_length'] = ($product_parameters['volumetric_category_id']) ? null : $this->booking_info['volumetric_length'];

		$product_parameters['final_weight'] = $this->getFinalWeight();

		return $product_parameters;
	}

	/**
	 * Calculatd the Final Weight of the product
	 */
	private function getFinalWeight()
	{
		if ($this->booking_info['weight_category_id']) {
			$wc_weight = BaseWeightCategory::findOrFail($this->booking_info['weight_category_id'])->first()->weight;
		} else {
			$wc_weight = $this->booking_info['weight'];
		}

		if ($this->booking_info['volumetric_category_id']) {
			$vm_weight = BaseVolumetricCategory::findOrFail($this->booking_info['volumetric_category_id'])->first()->volumetric_weight;
		} else {
			//Todo : need to merge duplicate function in CMS
			$vm_weight = ($this->booking_info['volumetric_width'] * $this->booking_info['volumetric_height'] * $this->booking_info['volumetric_length']) / 305;			
		}

		$final_weight = max($wc_weight, $vm_weight);

		return $final_weight;
	}

}