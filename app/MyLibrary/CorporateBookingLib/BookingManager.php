<?php

namespace App\MyLibrary\CorporateBookingLib;

use App\Booking;
use Carbon\Carbon;
use App\CustomerProfile;
use App\Events\Booking\NewBookingCreated;

class BookingManager
{
	public $booking;

    public $category;

	public $customer;

    /**
	 * Set Booking Category
	 * 
	 * @return Void
	 */
	public function __construct($category) 
    {
    	if ($category == 'shared') {
            $this->category = 'shared';
    		$this->booking = new SharedBooking();
    	} else {
            $this->category = 'full';
    		$this->booking = new FullBooking();
    	}
    }

    /**
	 * Set Customer
	 * 
	 * @return Void
	 */
	public function setBookingCustomer(CustomerProfile $customer) 
    {
    	$this->customer = $customer;
    }

    /**
	 * Get Estimated Fate of booking
	 * 
	 * @return Array
	 */
    public function getFare($booking_info) 
    {
        $cost = $this->booking->getEstimatedFare($booking_info, $this->customer);

        return $cost;
    }

    /**
	 * Create new Booking
	 * 
	 * @return Array
	 */
    public function createBooking($booking_info) 
    {
        $booking_info = $this->prepareFields($booking_info);

        $booking_res = $this->booking->create($booking_info, $this->customer);

        if ($booking_res['success']) {
            // fire events
            event(new NewBookingCreated($booking_res['booking']));
        }

        return $booking_res;
    }

    /**
     * Prepare booking fields before database insertion
     */
    private function prepareFields($booking_fields)
    {
        $booking_fields['customer_profile_id'] = $this->customer->id;
        $booking_fields['unique_id'] = $this->getUniqueId();
        $booking_fields['tracking_code'] = $this->getTrackingCode();
        $booking_fields['datetime'] = ($booking_fields['booking_type']=='advance') ? Carbon::createFromFormat('d/m/Y H:i A', $booking_fields['date_time']) : Carbon::now();
        
        return $booking_fields;
    }

    /**
     * Get unique id for new booking
     * @return String
     */
    private function getUniqueId()
    {
        $last_id = Booking::all()->last() ? Booking::all()->last()->id : 0;

        $id = (int) $last_id + 1;

        $postfix = ($this->category=='shared') ? 'S' : 'F';

        return 'HG'.'-'.$id.$postfix;
    }

    /**
     * Get tracking code for new booking
     * @return String
     */
    private function getTrackingCode()
    {
        $random_code = bin2hex(random_bytes(5));

        // TODO : does it already exist?

        return $random_code;
    }
    
}