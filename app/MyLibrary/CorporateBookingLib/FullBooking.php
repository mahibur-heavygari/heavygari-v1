<?php

namespace App\MyLibrary\CorporateBookingLib;

use DB;
use App\Booking;
use App\TripCategory;
use Carbon\Carbon;
use App\RouteVehicleWiseCharge;
use App\MyLibrary\GoogleMapLib\GoogleMapFacade;
use App\MyLibrary\CorporateBookingLib\CostCalculator\FullBookingCalculator;

class FullBooking implements BookingInterface
{	
	private $booking_info;
	private $trip_category;
	private $distance_type;
	private $distance;
	private $duration;
	private $multiplier = null;
	private $customer;

	/**
	 * Determine and set some attributes
	 */
	public function setter($booking_info)
	{
		$this->booking_info = $booking_info;

		$this->getBookingDistanceWithDuration();
		$this->setDistanceType();
		$this->setTripCategory();
	}
	
	/**
	 * Get Estimated Fare
	 *
	 * @return Array
	 */
	public function getEstimatedFare($booking_info, $customer)
	{
		if(!isset($booking_info['booking_type']) && !isset($booking_info['date_time'])){
			$booking_info['booking_type'] = NULL;
			$booking_info['datetime'] = NULL;
		}elseif(!isset($booking_info['date_time'])){
			$booking_info['booking_type'] = 'on-demand';
			$booking_info['datetime'] = Carbon::now();
		}elseif(isset($booking_info['booking_type']) && $booking_info['booking_type']=='on-demand'){
			$booking_info['booking_type'] = 'on-demand';
			$booking_info['datetime'] = Carbon::now();
		}elseif(isset($booking_info['booking_type']) && $booking_info['booking_type']=='advance'){
			$booking_info['booking_type'] = 'advance';
			$booking_info['datetime'] = Carbon::createFromFormat('d/m/Y H:i A', $booking_info['date_time']);
		}

		$this->customer = $customer;

		$this->setter($booking_info);

		$calculator = new FullBookingCalculator($this->duration, $this->distance, $this->distance_type,  $this->booking_info['trip_type'], $this->trip_category, $this->booking_info['vehicle_type'], $this->customer, $booking_info['booking_type'], $booking_info['datetime']);

		$fare_breakdown = $calculator->getFareBreakDown();

		$fare_breakdown = [
			'fare' => [
				'total_cost' => $fare_breakdown['total_cost'],
				'fare_breakdown' => $fare_breakdown,
			],			
	        'route' => [
	        	'distance' => $this->distance,
	        	'duration' => $this->duration,
	        	'distance_type' => $this->distance_type,
       			'category' => $this->trip_category,
	        	'orgigin' => [
	                'address' => $booking_info['from_address'],
	                'point' => $booking_info['from_point'],
	                'coordinators' => [
	                    'lat' => (float) $booking_info['from_lat'],
	                    'lon' => (float) $booking_info['from_lon']
		            ]
	            ],
	            'destination' => [
	                'address' => $booking_info['to_address'],
	                'point' => $booking_info['to_point'],
	                'coordinators' => [
	                    'lat' => (float) $booking_info['to_lat'],
	                    'lon' => (float) $booking_info['to_lon']
	                ]
	            ]
	        ]
		];
		
		return $fare_breakdown;
	}


	/**
	 * Create a Full Booking
	 * 
	 * @return Array
	 */
	public function create($booking_info, $customer)
	{
		$this->customer = $customer;

		try {
			$this->setter($booking_info);

			// get price breakdown 
			$calculator = new FullBookingCalculator($this->duration, $this->distance, $this->distance_type, $this->booking_info['trip_type'], $this->trip_category, $this->booking_info['vehicle_type'], $this->customer, $booking_info['booking_type'], $booking_info['datetime']);
			$cost_breakdown = $calculator->getFullCostBreakDown();
			$booking_info['booking_category'] = 'full';
			$booking_info['is_corporate'] = 'yes';
	 		$booking_info['distance'] = $this->distance;
	 		$booking_info['duration'] = $this->duration;
	 		$booking_info['multiplier'] = $this->multiplier;
	        $booking_info['distance_type'] = $this->distance_type;
	        $booking_info['vehicle_type_id'] = $this->booking_info['vehicle_type'];

	        $db_records['booking'] = $booking_info; 
	        $db_records['invoice'] = $cost_breakdown['invoice'];
	        $db_records['earnings'] = $cost_breakdown['earnings'];

	        //for first booking time discount
	        $first_booking_discount = NULL;
	        if($cost_breakdown['invoice']['off_amount']>0)
	        	$first_booking_discount = $cost_breakdown['invoice']['off_amount'];   

	        $debit_operation = FALSE;
	        $credit_operation = FALSE;

	        if($this->customer->corporateCustomer->debit_credit == 'debit'){
	        	$debit_operation = $this->checkDebit( $cost_breakdown );
	        	
	        	if (!$debit_operation) {
	        		return ['success'=>FALSE, 'booking'=>NULL, 'debit_failed'=>TRUE];
	        	}	
	        }

	        if($this->customer->corporateCustomer->debit_credit == 'credit'){
	        	$credit_operation = $this->checkCredit( $cost_breakdown );	
	        	
	        	if (!$credit_operation) {
	        		return ['success'=>FALSE, 'booking'=>NULL, 'credit_failed'=>TRUE];
	        	}
	        }

	        // insert into database tables
	        
	        DB::beginTransaction();

	        $booking = Booking::create($db_records['booking']);       
	        $full_booking_details = $booking->fullBookingDetails()->create($db_records['booking']); 
	        $trip = $booking->trips()->create($db_records['booking']);
	        $invoice = $booking->invoice()->create($db_records['invoice']);
	        $earnings = $booking->earnings()->create($db_records['earnings']);

	        //customer discount update for first time booking;
	        if( !is_null($first_booking_discount) ){
	        	$booking->first_discounted = 'yes';
	        	$booking->save();

		        $this->customer->first_booking_discount = $first_booking_discount;
		        $this->customer->save();

		    }
		    
		    if( $debit_operation || $credit_operation ){
		    	$total_cost = $cost_breakdown['invoice']['total_cost'];
				$balance = $this->customer->corporateCustomer->balance;
				$this->customer->corporateCustomer->balance = $balance - $total_cost;
				$this->customer->corporateCustomer->save();
		    }

	        DB::commit();

        } catch(\Exception $e) {
        	// Todo : log this?
	    	DB::rollBack();
	    	return ['success'=>FALSE, 'booking'=>NULL];
	    }

	    return ['success'=>TRUE, 'booking'=>$booking];
	}

	private function checkDebit( $cost_breakdown ){
		$total_cost = $cost_breakdown['invoice']['total_cost'];
		$balance = $this->customer->corporateCustomer->balance;
		if( $total_cost > $balance ){
			return FALSE;
		}

		return TRUE;
	}

	private function checkCredit( $cost_breakdown ){
		$total_cost = $cost_breakdown['invoice']['total_cost'];
		$balance = $this->customer->corporateCustomer->balance;
		$balance_abs = abs($balance);
		$limit = $this->customer->corporateCustomer->limit;

		if( ($balance_abs + $total_cost) > $limit ){
			return FALSE;
		}

		return TRUE;
	}


    // SETTERS 

    /**
     * Determine and set the distance from Source to Destination
     * 
     * @return Array
     */
    private function getBookingDistanceWithDuration()
    {
        $distance_duration = GoogleMapFacade::getDistanceWithDuration($this->booking_info['from_lat'].','.$this->booking_info['from_lon'] , $this->booking_info['to_lat'].','.$this->booking_info['to_lon'] );

        $this->distance = $distance_duration['distance'];
        $this->duration = $distance_duration['duration'];
    }

    /**
	 * Determine and set the Distance Type (short, normal..)
	 */
	private function setDistanceType()
	{
		$distance_type = 'normal';

		$short_distance = config('heavygari.distance.full_booking.short');
		$long_distance = config('heavygari.distance.full_booking.long');

		if ($this->distance < $short_distance) {
			$distance_type = 'short';
		} else if ($this->distance > $long_distance) {
			$distance_type = 'long';
		}

		$this->distance_type = $distance_type;
	}

	/**
	 * Determine and set the Trip Type (up/down)
	 */
	private function setTripCategory()
	{
		if($this->booking_info['trip_type']=='single'){			
			$trip_category = [
				'category' => 'up',
				'multiplier' => 1
			];

			$trip = TripCategory::where(['src_point_id' => $this->booking_info['from_point'], 'dest_point_id' => $this->booking_info['to_point']])->first();

			if ( $trip ) {
				$multiplier = $this->getMultiplier( $trip->id );
				$trip_category = [
					'category' => $trip['trip_category'],
					'multiplier' => $multiplier
				]; 
			}

		}else{
			$trip_category = [
				'category' => 'round',
				'up_trip_multiplier' => 1,
				'down_trip_multiplier' => 1,
			];

			$departure_trip = TripCategory::where(['src_point_id' => $this->booking_info['from_point'], 'dest_point_id' => $this->booking_info['to_point']])->first();
			$arrival_trip = TripCategory::where(['src_point_id' => $this->booking_info['to_point'], 'dest_point_id' => $this->booking_info['from_point']])->first();

			if ($departure_trip && $departure_trip->trip_category=='up') {
				$trip_category['up_trip_multiplier'] = $this->getMultiplier( $departure_trip->id );
			}elseif ($departure_trip && $departure_trip->trip_category=='down') {
				$trip_category['down_trip_multiplier'] = $this->getMultiplier( $departure_trip->id );
			}

			if ($arrival_trip && $arrival_trip->trip_category=='up') {
				$trip_category['up_trip_multiplier'] = $this->getMultiplier( $arrival_trip->id );
			}elseif ($arrival_trip && $arrival_trip->trip_category=='down') {
				$trip_category['down_trip_multiplier'] = $this->getMultiplier( $arrival_trip->id );
			}
		}
		$this->trip_category = $trip_category;
	}

	//set multiplier for spcific route and vehicle
	private function getMultiplier( $id )
	{
		$routeVehicleWiseCharge = RouteVehicleWiseCharge::where('trip_category_id', $id)->where('vehicle_type_id', $this->booking_info['vehicle_type'])->where('shared_or_full', 'full')->first();

		if( $routeVehicleWiseCharge ){
			$this->multiplier = $routeVehicleWiseCharge->multiplier;
			return (float)$routeVehicleWiseCharge->multiplier;
		}

		return 1;
	}
}