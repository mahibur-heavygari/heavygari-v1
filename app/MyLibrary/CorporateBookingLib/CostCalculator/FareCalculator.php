<?php

namespace App\MyLibrary\CorporateBookingLib\CostCalculator;

use App\Settings;

abstract class FareCalculator {

	
	/**
	 * Get Full Costing Breakdown of a Shared Booking
     *
     * details : 
     *	 
	 */
	final public function getFullCostBreakDown()
	{
		$invoice_breakdown = $this->getFareBreakDown();
		
		# Earnings
		//$driver_percent = config('heavygari.driver_comission');
		$settings = Settings::where('key', 'driver_comission')->first();
    	$driver_percent = (float)$settings->value;

    	$ride_sharing_tax = $invoice_breakdown['ride_sharing_tax'];
    	$total_fare_without_tax = $invoice_breakdown['total_fare'] - $ride_sharing_tax;

		$owner_earning = $total_fare_without_tax - $invoice_breakdown['heavygari_fee'];
		$heavygari_fee = $invoice_breakdown['heavygari_fee'];
		$driver_earning = $heavygari_fee * $driver_percent; 	
		$heavygari_earning = $heavygari_fee - $driver_earning;

		$cost_breakdown = [
			'invoice' => $invoice_breakdown,
			'earnings' => [
				'owner_earning' => (float) $owner_earning,
				'heavygari_earning' => (float) $heavygari_earning,
				'driver_earning' => (float) $driver_earning,
				'ride_sharing_tax' => (float) $ride_sharing_tax,
			]
		];

		return $cost_breakdown;
	}
	

	/**
	 * Get Fare Brakedown
     *
     * details : 
     *	 
	 */
	final public function getFareBreakDown()
	{
	    $base_fare = $this->getBaseFare();
		$pickup_charge = $this->getPickupCharge();
		$dropoff_charge = $this->getDropOffCharge();
		$distance_cost = $this->getDistanceCost();
		$weight_cost = $this->getWeightCost();
		$surcharge = $this->getSurchargeCost();
		$waiting_cost = $this->getWaitingCost();
        //---------------------------------------------
        $total = $base_fare + $pickup_charge + $dropoff_charge + $distance_cost + $weight_cost + $surcharge + $waiting_cost;
        $heavygari_fee = $this->getHeavyGariFee($total);
        //---------------------------------------------
		$total_fare = $total + $heavygari_fee;
		$ride_sharing_tax = $this->getRideSharingTaxAmount($total_fare);
		$total_fare = $total_fare + $ride_sharing_tax;

		$discount_info = $this->getDiscountAmount($total_fare);
		$discount = $discount_info['discount'];
        //---------------------------------------------------
		$total_cost = $total_fare - $discount;

		$cost_breakdown = [
			'pickup_charge' => (float) $pickup_charge,
			'dropoff_charge' => (float) $dropoff_charge,
			'base_fare' => (float) $base_fare,
			'distance_cost' => (float)$distance_cost,
			'weight_cost' => (float) $weight_cost,
			'surcharge' => (float) $surcharge,
			'waiting_cost' => (float) $waiting_cost,
			'heavygari_fee' => (float) $heavygari_fee,
			'total_fare' => (float) $total_fare,
			'discount' => (float) $discount,
			'off_amount' => (float) $discount_info['off_amount'],
			'total_cost' => (float) $total_cost,
			'ride_sharing_tax' => (float) $ride_sharing_tax
		];

		return $cost_breakdown;
	}

	/**
	 * This methods must be implemented
	 */
	abstract protected function getBaseFare();
	abstract protected function getDistanceCost();
	abstract protected function getSurchargeCost();
	abstract protected function getWaitingCost();
	abstract protected function getDiscountAmount($total_fare);
	abstract protected function getRideSharingTaxAmount($total_fare);

	/**
	 * Optional methods (for parcel booking)
	 */
	protected function getPickupCharge()
	{
	    return 0;
	}

	protected function getDropOffCharge()
	{
	    return 0;
	}

	protected function getWeightCost()
	{
	    return 0;
	}
}