<?php

namespace App\MyLibrary\TransactionLib;

use App\Booking;
use App\DriverProfile;

class DriverTransaction extends TransactionManager
{
	protected $driver;

	public function __construct(DriverProfile $driver) {
		$this->driver = $driver;
	}

	public function getTransactions( $request )
	{
		$filters = $this->modifyFilters( $request );
        
		$bookings = Booking::where('driver_profile_id', $this->driver->id)->completed()->where('datetime', '>=', $filters['start_date'])->where('datetime', '<=', $filters['end_date'])->orderBy('id', 'desc')->get();

		$bookings->transform(function ($item, $key) {
            return $this->reformStats($item);
        });

        $admin_commission_total = $bookings->sum('admin_commission');
        $owner_earning_total = $bookings->sum('owner_earning');
        $driver_commission_total = $bookings->sum('driver_commission');
        
		return [ 'bookings' => $bookings, 'total_fare' => $bookings->sum('customer_cost'), 'total_customer_cost' => $bookings->sum('customer_cost'), 'admin_commission_total'=>$admin_commission_total, 'owner_earning_total'=>$owner_earning_total, 'driver_commission_total'=>$driver_commission_total];
	}

}