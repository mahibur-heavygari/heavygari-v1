<?php

namespace App\MyLibrary\TransactionLib;

use App\Booking;

class AdminTransaction extends TransactionManager
{
	/**
	 * get all booking stats
	 */
	public function getTransactions( $request )
	{	
        $filters = $this->modifyFilters( $request );

		$bookings = Booking::completed()->where('datetime', '>=', $filters['start_date'])->where('datetime', '<=', $filters['end_date'])->orderBy('id', 'desc')->get();

		$bookings->transform(function ($item, $key) {
            return $this->reformStats($item);
        });

        $admin_commission_total = $bookings->sum('admin_commission')-$bookings->sum('discount');
        $owner_earning_total = $bookings->sum('owner_earning');
        $driver_commission_total = $bookings->sum('driver_commission');
        
		return [ 'bookings' => $bookings, 'total_customer_cost' => $bookings->sum('customer_cost'), 'total_fare' => $bookings->sum('total_fare'), 'admin_commission_total'=>$admin_commission_total, 'owner_earning_total'=>$owner_earning_total, 'driver_commission_total'=>$driver_commission_total];
	}
}