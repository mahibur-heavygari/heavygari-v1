<?php

namespace App\MyLibrary\TransactionLib;

use App\Booking;

class TransactionManager
{
	protected function reformStats( $booking )
	{
		$driver_commission_status = isset($booking->driverCommissionItem) ? $booking->driverCommissionItem->commission->status : 'due';
		$admin_commission_status = isset($booking->driverCommissionItem->commission->ownerInvoice) ? $booking->driverCommissionItem->commission->ownerInvoice->status : 'due';
		
            return [
                  'id'=> $booking->id,
                  'unique_id'=> $booking->unique_id,
                  'datetime'=> $booking->datetime->toDateTimeString(),
                  'booking_category'=> $booking->booking_category,
                  'booking_type'=> $booking->booking_type,
                  'trip_type'=> $booking->trip_type,  
                  'owner_id' => $booking->owner->id,
                  'owner_name' => $booking->owner->user->name,
                  'owner_phone' => $booking->owner->user->phone,
                  'owner_earning' => $booking->earnings['owner_earning'],
                  'owner_earning_status' => 'paid',
                  'admin_commission' => $booking->earnings['heavygari_earning'],
                  'admin_commission_status' => $admin_commission_status,
                  'driver_id' => $booking->driver->id,
                  'driver_name' => $booking->driver->user->name,
                  'driver_phone' => $booking->driver->user->phone,
                  'driver_commission' => $booking->earnings['driver_earning'],
                  'driver_commission_status' => $driver_commission_status,
                  'customer_id' => $booking->customer->id,
                  'customer_name' => $booking->customer->user->name,
                  'customer_phone' => $booking->customer->user->phone,
                  'customer_cost' => $booking->invoice['total_cost'],
                  'total_fare' => $booking->invoice['total_fare'],
                  'discount' => $booking->invoice['discount'],
                  'customer_cost_status' => 'paid',
                  'accepted_at'=> ($booking->accepted_at) ? $booking->accepted_at->toDateTimeString() : $booking->accepted_at,
                  'started_at'=> $booking->trip['started_at'],
                  'completed_at'=> $booking->trip['completed_at'],
                  'created_at'=> $booking->created_at->toDateTimeString(),
                  'updated_at'=> $booking->updated_at->toDateTimeString(),
            ];
	}

      protected function modifyFilters( $request )
      {       
            $modal = $request->modal;

            if(is_null($modal)){
                  $start_date = '2016-01-01';
                  $end_date = '2022-12-31';
            }elseif($modal == 'daily'){
                  $start_date = $request->year."-".$request->month."-".$request->day." 00:00:00";
                  $end_date = $request->year."-".$request->month."-".$request->day." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='1'){
                  $start_date = $request->year."-".$request->month."-"."01"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."07"." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='2'){
                  $start_date = $request->year."-".$request->month."-"."08"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."14"." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='3'){
                  $start_date = $request->year."-".$request->month."-"."15"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."21"." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='4'){
                  $start_date = $request->year."-".$request->month."-"."22"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."31"." 23:59:59";
            }elseif($modal == 'monthly'){
                  $start_date = $request->year."-".$request->month."-"."01"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."31"." 23:59:59";
            }elseif($modal == 'yearly'){
                  $start_date = $request->year."-"."01"."-"."01"." 00:00:00";
                  $end_date = $request->year."-"."12"."-"."31"." 23:59:59";
            }
            
            $start_date = date('Y-m-d H:i:s', strtotime($start_date . ' -6 hours'));
            $end_date = date('Y-m-d H:i:s', strtotime($end_date . ' -6 hours'));

            return compact("start_date", "end_date");  
      }
}