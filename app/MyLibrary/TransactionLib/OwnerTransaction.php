<?php

namespace App\MyLibrary\TransactionLib;

use App\Booking;
use App\OwnerProfile;

class OwnerTransaction extends TransactionManager
{
	protected $owner;

	public function __construct(OwnerProfile $owner) {
		$this->owner = $owner;
	}

	public function getTransactions( $request )
	{
		$filters = $this->modifyFilters( $request );
        
		$bookings = Booking::where('vehicle_owner_profile_id', $this->owner->id)->completed()->where('datetime', '>=', $filters['start_date'])->where('datetime', '<=', $filters['end_date'])->orderBy('id', 'desc')->get();

		$bookings->transform(function ($item, $key) {
            return $this->reformStats($item);
        });

        $only_admin_due_commission_total = $bookings->where('admin_commission_status', 'due')->sum('admin_commission');
        $only_driver_due_commission_total = $bookings->where('driver_commission_status', 'due')->sum('driver_commission');
        $admin_due_commission_total = $only_admin_due_commission_total  + $only_driver_due_commission_total;

        $only_admin_paid_commission_total = $bookings->where('admin_commission_status', 'paid')->sum('admin_commission');
        $only_driver_paid_commission_total = $bookings->where('driver_commission_status', 'paid')->sum('driver_commission');
        $only_driver_unpaid_commission_total = $bookings->where('driver_commission_status', 'unpaid')->sum('driver_commission');
        $admin_paid_commission_total = $only_admin_paid_commission_total  + $only_driver_paid_commission_total + $only_driver_unpaid_commission_total;

        $admin_commission_total = $bookings->sum('admin_commission')-$bookings->sum('discount');
        $owner_earning_total = $bookings->sum('owner_earning');
        $driver_commission_total = $bookings->sum('driver_commission');
        
		return [ 'bookings' => $bookings, 'total_customer_cost' => $bookings->sum('customer_cost'), 'total_fare' => $bookings->sum('total_fare'), 'admin_commission_total'=>$admin_commission_total, 'owner_earning_total'=>$owner_earning_total, 'driver_commission_total'=>$driver_commission_total, 'admin_due_commission_total'=>$admin_due_commission_total, 'admin_paid_commission_total'=>$admin_paid_commission_total];
	}
}