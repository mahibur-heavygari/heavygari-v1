<?php

namespace App\MyLibrary\AccountsLib;

use App\Booking;
use App\OwnerInvoiceItem;
use App\MyLibrary\AccountsLib\OperationalAccounts;

class OperationalAccountsManager
{
	protected function modifyFilters( $request )
      {       
            $modal = $request->modal;

            if(is_null($modal)){
                  $start_date = '2016-01-01';
                  $end_date = '2022-12-31';
            }elseif($modal == 'daily'){
                  $start_date = $request->year."-".$request->month."-".$request->day." 00:00:00";
                  $end_date = $request->year."-".$request->month."-".$request->day." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='1'){
                  $start_date = $request->year."-".$request->month."-"."01"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."07"." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='2'){
                  $start_date = $request->year."-".$request->month."-"."08"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."14"." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='3'){
                  $start_date = $request->year."-".$request->month."-"."15"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."21"." 23:59:59";
            }elseif($modal == 'weekly' && $request->week =='4'){
                  $start_date = $request->year."-".$request->month."-"."22"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."31"." 23:59:59";
            }elseif($modal == 'monthly'){
                  $start_date = $request->year."-".$request->month."-"."01"." 00:00:00";
                  $end_date = $request->year."-".$request->month."-"."31"." 23:59:59";
            }elseif($modal == 'yearly'){
                  $start_date = $request->year."-"."01"."-"."01"." 00:00:00";
                  $end_date = $request->year."-"."12"."-"."31"." 23:59:59";
            }
            
            $start_date = date('Y-m-d H:i:s', strtotime($start_date . ' -6 hours'));
            $end_date = date('Y-m-d H:i:s', strtotime($end_date . ' -6 hours'));

            return compact("start_date", "end_date");  
      }
}