<?php

namespace App\MyLibrary\AccountsLib;

use App\Booking;
use App\OwnerInvoiceItem;
use App\MyLibrary\AccountsLib\OperationalAccountsManager;

class OperationalAccounts extends OperationalAccountsManager
{
  	public function paidOperations($request)
  	{
        $filters = $this->modifyFilters( $request );
        
        $owner_invoice_items = OwnerInvoiceItem::with('invoice')->whereHas('invoice', function($invoice) {
    	     $invoice->where('status','=','paid');
        })->get();

        $owner_invoice_items->transform(function ($item, $key) {
            return $this->reformOwnerPaidItem($item);
        });

        $owner_invoice_items = $owner_invoice_items->where('paid_at', '>=', $filters['start_date'])->where('paid_at', '<=', $filters['end_date'])->sortByDesc('booking_id');
            
    	return $owner_invoice_items;
  	}
	
  	private function reformOwnerPaidItem( $item )
  	{
    	return [
            'booking_id'=> $item->bookings->unique_id,
            'discount'=> $item->bookings->invoice->discount,
            'paid_at'=> $item->invoice->paid_at,
            'owner_amount'=> $item->receivable,
            'driver_amount'=> $item->driverCommissionItem->payable,
            'driver_amount_status'=> $item->driverCommissionItem->commission->status,
            'admin_amount'=> $item->receivable-$item->driverCommissionItem->payable,
        ];
  	}
  	
  	public function unpaidOperations( $request )
  	{  
        $filters = $this->modifyFilters( $request );

        $bookings = Booking::completed()->orderBy('id', 'desc')->get();
        $bookings = $bookings->where('datetime', '>=', $filters['start_date'])->where('datetime', '<=', $filters['end_date']);

        $due_items = $bookings->filter(function ($value, $key) {
            if(is_null($value->ownerInvoiceItem))
            	 return $value;
            if($value->ownerInvoiceItem->invoice->status == 'due')
          	   return $value;
        });

        $due_items->transform(function ($item, $key) {
            return $this->reformOwnerUnpaidItem($item);
        });

        return $due_items;
  	}

  	private function reformOwnerUnpaidItem( $item )
  	{
		return [
            'booking_id'=> $item->unique_id,
            'discount'=> $item->invoice->discount,
            'owner_amount'=>$item->earnings->driver_earning + $item->earnings->heavygari_earning,
            'driver_amount'=>$item->earnings->driver_earning,
            'admin_amount'=>$item->earnings->heavygari_earning
        ];
  	}
}