<?php

namespace App\MyLibrary\GoogleMapLib;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GoogleMapFacade
{
	/* Get distance of two address */

	public static function getDistanceWithDuration($from, $to)
	{
		$distance = 0;
		$duration = 0;
		$distanceURL = 'https://maps.googleapis.com/maps/api/distancematrix/json?key='.config('heavygari.google_maps.api_key').'&origins='.$from.'&destinations='.$to;

		$client = new Client();
		$res = $client->get($distanceURL);
		$output = json_decode($res->getBody()->getContents());

		if(isset($output->rows[0]->elements[0]->distance->value)) {
			$distance =  ($output->rows[0]->elements[0]->distance->value)/1000;
		}
		if(isset($output->rows[0]->elements[0]->duration->value)) {
			$duration =  $output->rows[0]->elements[0]->duration->value;
		}

		return array('distance' => $distance, 'duration' => $duration);
	}

}