<?php

namespace App\MyLibrary\Firebase;

use GuzzleHttp;
use Illuminate\Support\Facades\Log;

class WebPushNotification
{
	private $access_token;

	public function __construct() {
		$this->access_token = config('heavygari.firebase.web_fcm_server_key');
	}

	public function sendToSingleBrowser($reg_id, $message)
	{
        if (!$reg_id) {
            return false;
        }

        $message['to'] = $reg_id;

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key='.$this->access_token,
            ]
        ]);
        // Log::info('Tried to send Push Notification. Message :'.var_export($message, true));

        try {            
            $response = $client->post('https://fcm.googleapis.com/fcm/send',
                ['body' => json_encode($message)]
            );
            
            if($response->getStatusCode() == 200){
                return true;
            }

        } catch(\Exception $e) {
            return false;
        }

        // Log::info('Response from Push Notification. Message :'.var_export($response, true));
        // TODO :: true/false and log if not sent.
              
        
	}
}