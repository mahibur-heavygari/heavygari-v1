<?php

namespace App\MyLibrary\Firebase;

use GuzzleHttp;
use Illuminate\Support\Facades\Log;

class PushNotification
{
	private $access_token;

	public function __construct() {
		$this->access_token = config('heavygari.firebase.fcm_server_key');
	}

	public function sendToSingleDevice($reg_id, $message)
	{
        if (!$reg_id) {
            return false;
        }

        $message['to'] = $reg_id;

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key='.$this->access_token,
            ]
        ]);

        // Log::info('Tried to send Push Notification. Message :'.var_export($message, true));

        $response = $client->post('https://fcm.googleapis.com/fcm/send',
            ['body' => json_encode($message)]
        );

        // Log::info('Response from Push Notification. Message :'.var_export($response, true));

        // TODO :: true/false and log if not sent.
              
        return true;
	}

    public function sendToMultipleDevice($reg_ids, $message)
    {
        if( count($reg_ids) ==0 ) {
            return false;
        }
        
        $message['registration_ids'] = $reg_ids;

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key='.$this->access_token,
            ]
        ]);
        //Log::info('Tried to send Push Notification. Message :'.var_export($message, true));

        $response = $client->post('https://fcm.googleapis.com/fcm/send',
            ['body' => json_encode($message)]
        );

        // Log::info('Response from Push Notification. Message :'.var_export($response, true));
        // TODO :: true/false and log if not sent.
              
        return true;
    }
}