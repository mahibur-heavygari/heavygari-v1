<?php

namespace App\MyLibrary\DashboardLib;

use App\Vehicle;
use App\Booking;
use App\Trip;
use App\Point;
use Carbon\Carbon;   

class AdminDashboard extends DashboardManager
{
	/**
	 * get all booking stats
	 */
	public function bookingStats()
	{
		$bookings = Booking::all();
		$booking_stats = $this->reformBookingStats($bookings);
		return $booking_stats;
	}

	public function chartBookingStats()
	{
		$bookings = Booking::select('id', 'status', 'created_at')
		    ->get()
		    ->groupBy(function($date) {
		        return Carbon::parse($date->created_at)->format("M'Y");
		    });

		$booking_stats = $this->reformChartBookingStats($bookings);

		return $booking_stats;
	}

	/**
	 * get all payments stats
	 */
	public function paymentStats()
	{
		$bookings = Booking::all();

		$payment_stats = $this->reformPaymentStats($bookings);

		return $payment_stats;
	}

	/**
	 * get all vehicle stats
	 */
	public function vehicleStats()
	{
		$vehicles = Vehicle::all();

		$vehcile_stats = $this->reformVehicleStats($vehicles);

		return $vehcile_stats;
	}
	public function tripDestination()
	{
		$trip_stats = $this->reformTripStats();
		return $trip_stats;
	}
	public function vehicleType()
	{
		$vehicle_types = $this->reformVehicleTypes();
		return $vehicle_types;
	}
	public function productType()
	{
		$particulars = $this->reformProductTypes();
		return $particulars;
	}

}