<?php

namespace App\MyLibrary\DashboardLib;

use App\Booking;
use App\CustomerProfile;
use App\PopUpSetting;
use Carbon\Carbon;

class CorporateCustomerDashboard extends DashboardManager
{
	protected $customer;

	public function __construct(CustomerProfile $customer) {
		$this->customer = $customer;
	}

	/**
	 * get booking stats by customer
	 */
	public function bookingStats()
	{
		$corporate_customer_profile_id_arr = $this->customer->corporate[0]->customers->pluck('id')->toArray();
		$bookings = Booking::whereIn('customer_profile_id', $corporate_customer_profile_id_arr)->get();

		$booking_stats = $this->reformBookingStats($bookings);

		return $booking_stats;
	}

	/**
	 * get payments stats by customer
	 */
	public function paymentStats()
	{
		$corporate_customer_profile_id_arr = $this->customer->corporate[0]->customers->pluck('id')->toArray();
		$bookings = Booking::whereIn('customer_profile_id', $corporate_customer_profile_id_arr)->get();

		$payment_stats = $this->reformPaymentStats($bookings);

		return $payment_stats;
	}

	public function customerPopupNotification()
	{	
		$popup_notification = PopUpSetting::orderBy('created_at', 'desc')->first();
		if(Carbon::now()->lte($popup_notification->expired_at)){
			$popup_id = $popup_notification->id;
			$web_app_id =  $this->customer->web_app;
			$popup_no =  $this->customer->popup_no;
			if($web_app_id === null){
				$web_app_id = $popup_id;
				if( $popup_id === $web_app_id && $web_app_id > 0) {
					CustomerProfile::where('id',$this->customer->id)->update(['web_app' => '0', 'popup_no' => $popup_id]);
					return $popup_notification;	
				}
			}elseif( $web_app_id === 0 &&  $popup_id > $popup_no ){
				$web_app_id = $popup_id; 
				if( $popup_id === $web_app_id && $web_app_id > 0) {
					CustomerProfile::where('id',$this->customer->id)->update(['web_app' => '0', 'popup_no' => $popup_id]);
					return $popup_notification;	
				}
			}
		}elseif($popup_notification->expired_at == null){
			$popup_id = $popup_notification->id;
			$web_app_id =  $this->customer->web_app;
			$popup_no =  $this->customer->popup_no;
			if($web_app_id === null){
				$web_app_id = $popup_id;
				if( $popup_id === $web_app_id && $web_app_id > 0) {
					CustomerProfile::where('id',$this->customer->id)->update(['web_app' => '0', 'popup_no' => $popup_id]);
					return $popup_notification;	
				}
			}elseif( $web_app_id === 0  &&  $popup_id > $popup_no){
				$web_app_id = $popup_id;
				if( $popup_id === $web_app_id && $web_app_id > 0) {
					CustomerProfile::where('id',$this->customer->id)->update(['web_app' => '0', 'popup_no' => $popup_id]);
					return $popup_notification;	
				}
			}
		}
		return;		
	}
	
	/**
	 * get bookings count by customer
	 
	public static function getBookingsStat(CustomerProfile $customer)
	{
		$bookings = $customer->myBookings;

		$bookings_stat = [
			'open' => $bookings->where('status','open')->count(),
			'upcoming' => $bookings->where('status','upcoming')->count(),
			'ongoing' => $bookings->where('status','ongoing')->count(),
			'completed' => $bookings->where('status','completed')->count()
		];

		return $bookings_stat;
	}
	*/

	/**
	 * get payment statistics by customer
	 
	public static function getPaymentsStat(CustomerProfile $customer)
	{
		
	}

	*/

}