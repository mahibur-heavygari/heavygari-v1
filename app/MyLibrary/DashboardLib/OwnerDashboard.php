<?php

namespace App\MyLibrary\DashboardLib;

use App\Booking;
use App\OwnerProfile;

class OwnerDashboard extends DashboardManager
{
	protected $owner;

	public function __construct(OwnerProfile $owner) {
		$this->owner = $owner;
	}

	/**
	 * get booking stats by owner
	 */
	public function bookingStats()
	{
		$bookings = $this->owner->myBookings;

		$booking_stats = $this->reformBookingStats($bookings);

		return $booking_stats;
	}

	/**
	 * get payments stats by owner
	 */
	public function paymentStats()
	{
		$bookings = $this->owner->myBookings;

		$payment_stats = $this->reformPaymentStats($bookings);

		return $payment_stats;
	}

	/**
	 * get vehicle stats by owner
	 */
	public function vehicleStats()
	{
		$vehicles = $this->owner->myVehicles;

		$booking_stats = $this->reformVehicleStats($vehicles);

		return $booking_stats;
	}

	/** get Driver stats by owner
	*/

	public function driverStats()
	{
		$owner = $this->owner->id;
		
		$driver_stats = $this->reformDriverStats($owner);

		return $driver_stats;
	}

}