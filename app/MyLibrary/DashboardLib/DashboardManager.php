<?php

namespace App\MyLibrary\DashboardLib;
Use \Carbon\Carbon;
use App\Vehicle;
use App\Booking;
use App\Trip;
use App\Point;
use App\VehicleType;
use App\FullBookingDetails;
use App\DriverProfile;
use App\OwnerProfile;
use DB;

class DashboardManager
{
	protected function reformBookingStats($bookings)
	{
		$total_distance = 0;
		$completed_bookings = $bookings->where('status','completed');
		$completed_bookings->each(function ($item, $key) use( &$total_distance ) {
		    $total_distance += $item->trips[0]->distance;
		});

		return [
			'open' => $bookings->where('status','open')->count(),
			'upcoming' => $bookings->where('status','upcoming')->count(),
			'ongoing' => $bookings->where('status','ongoing')->count(),
			'completed' => $bookings->where('status','completed')->count(),
			'total_distance' => (int) $total_distance
		];
	}

	protected function reformChartBookingStats($bookings)
	{
		$total_cancelled = 0;
		$total_completed = 0;
		$total_revenue = 0;
		$stat = [];
		foreach ($bookings as $key => $booking) {
			$stat['cancelled'][] = $booking->where('status','cancelled')->count();
			$stat['completed'][] = $booking->where('status','completed')->count();
			$stat['revenue'][] = $booking->where('status','completed')->sum('invoice.total_fare');
			$stat['month_year'][] = $key;
			$total_cancelled += $booking->where('status','cancelled')->count();
			$total_completed += $booking->where('status','completed')->count();
			$total_revenue += $booking->where('status','completed')->sum('invoice.total_fare');
		}

		return [
			'stat' => $stat,
			'total_revenue' => $total_revenue,
			'total_cancelled' => $total_cancelled,
			'total_completed' => $total_completed
		];
	}

	protected function reformPaymentStats($bookings)
	{
		$total_cost = 0;
		$driver_commission = 0;
		$admin_cost = 0;
		$owner_revenue = 0;
		$total_fare = 0;

		$payable_bookings = $bookings->where('status', 'completed');

		foreach ($payable_bookings as $booking) {
			$total_cost += $booking->invoice->total_cost;
			//$admin_cost += $booking->invoice->heavygari_fee;
			$admin_cost += $booking->earnings->heavygari_earning;
			$driver_commission += $booking->earnings->driver_earning;
			$owner_revenue += $booking->earnings->owner_earning;
			$total_fare += $booking->invoice->total_fare;
		}

		$payment_stats = [
			'total_cost' => $total_cost,
			'admin_cost' => $admin_cost,
			'driver_earning' => $driver_commission,
			'owner_revenue' => $owner_revenue,
			'total_fare' => $total_fare,
		];

		return $payment_stats;
	}
	protected function reformVehicleStats($vehicles)
	{
        $group = $vehicles->groupBy('vehicle_type_id');

        $vehicles = $group->mapWithKeys(function ($item) {
            return [$item[0]->vehicleType->title => $item->count()];
        });
        
		$stats_array = $vehicles->toArray();

		$stats_collection = collect($stats_array)->sort();

		return $stats_collection->reverse();
	}
	protected function reformDriverStats($owner)
	{       
      	$drivers = DriverProfile::where('vehicle_owner_profile_id', $owner)->get();
       	return $drivers;
	}
	protected function reformTripStats()
	{
		$booking_ids = Booking::get()->pluck('id')->toArray();      

        $trips = DB::table('trips')->whereIn('booking_id', $booking_ids)->select('to_point', DB::raw('count(*) as total'))->groupBy('to_point')->orderBy('total', 'desc')->get();     

        $trips = $trips->each(function ($item, $key) {
               $to_point = Point::where('id', $item->to_point)->first();
               $item->to_point_title = $to_point->title;
        });
        $trips = $trips->take(10);
        return $trips;
	}
	protected function reformVehicleTypes()
	{
		$booking_ids = Booking::get()->pluck('id')->toArray();   
		$vehicle_type = DB::table('full_booking_details')->whereIn('booking_id', $booking_ids)->select('vehicle_type_id', DB::raw('count(*) as total'))->groupBy('vehicle_type_id')->orderBy('total', 'desc')->get();
     	$vehicle_type = $vehicle_type->each(function ($item, $key) {
               $vehicle_type_id = VehicleType::where('id', $item->vehicle_type_id)->first();
               $item->vehicle_title = $vehicle_type_id->title;
        });
        $vehicle_type = $vehicle_type->take(10);
		return $vehicle_type;
	}
	protected function reformProductTypes()
	{ 
		$particulars = Booking::where('particular_details','!=','null')->get()->groupBy('particular_details');
		$materials = array();
		foreach ($particulars as $key => $value) {
			$materials[$key] = $value->count();
		}
		array_multisort($materials, SORT_DESC, $particulars->toArray());
		$materials = array_slice($materials,0, 10, true);
		return $materials;
	}
}