<?php

namespace App\MyLibrary\InvoiceLib;

use DB;
use PDF;
use Storage;
use App\Booking;
use App\OwnerProfile;
use App\OwnerInvoice;
use App\OwnerInvoiceItem;
use App\DriverCommission;
use Illuminate\Support\Facades\Mail;
use App\Events\OwnerInvoice\OwnerInvoiceCreated;
use App\Mail\Invoice\OwnerMonthlyInvoiceEmail;
use App\Mail\Invoice\OwnerPaymentAcknowledgementEmail;
use App\MyLibrary\DriverCommission\CommissionSystem;
use App\MyLibrary\SMSLib\SMSFacade;

class InvoiceSystem
{
	/**
	 * create a new invoice
	 * an owner's invoice for a specific month
	 * 
	 * @param  int $owner_id
	 * @param  int $month
	 * @param  int $year
	 * @return [type]
	 */
	public static function create($owner_id, $month, $year)
	{
		//$owner_invoice = OwnerInvoice::where('owner_id', $owner_id)->where('month', $month)->where('year', $year)->first();
		//if($owner_invoice)
		//	return;
		
		$owner = OwnerProfile::findOrFail($owner_id);
        //$bookings = $owner->myBookings()->completed()->whereYear('datetime', $year)->whereMonth('datetime', $month)->get();
   		$bookings = $owner->myBookings()->completed()->where('payment_invoice_generated', 'no')->get();
   
        $total_amount = $bookings->sum('earnings.heavygari_earning') + $bookings->sum('earnings.driver_earning');
        if($total_amount == 0)
        	return;

        DB::beginTransaction();
       
        try 
        {
		   	// create new invoice
	        $invoice = new OwnerInvoice;
	        $invoice->invoice_date = date('Y-m-d');
	        $invoice->invoice_number = self::getInvoiceNumber(); 
	        $invoice->due_date = date('Y-m-d');
	        $invoice->owner_id = $owner_id;
	        $invoice->total_amount = $total_amount;
	        $invoice->note = '';
	        $invoice->month = $month;
	        $invoice->year = $year;
	        $invoice->save();

	        $invoice_id = $invoice->id;

	        // create invoice items
	        foreach ($bookings as $booking) {
	            $invoice_item = new OwnerInvoiceItem;
	            $invoice_item->invoice_id = $invoice_id;
	            $invoice_item->booking_id = $booking->id;
	            $invoice_item->receivable = $booking->earnings['heavygari_earning'] + $booking->earnings['driver_earning'];
	            $invoice_item->save();

	            $booking->payment_invoice_generated = 'yes';
	            $booking->save();
	        }

	        $commission = CommissionSystem::create($owner_id, $month, $year, $invoice_id);

			DB::commit();

			//send sms
			$to = $owner->user->phone;
			$name = $owner->user->name;
			$text = "প্রিয় ".$name .", হেভিগাড়ীর সার্ভিস চার্জ বাবদ টাকা:".$total_amount." বকেয়া রয়েছে, বকেয়া টাকা পরিশোধ করে আমাদের সাথে থাকুন।
বিকাশ নং: 01999097252
ধন্যবাদ।";
			$ref_id = "$to"."_".time();
        	$status = SMSFacade::send($to, $text, $ref_id, 'owner');

			// fire event
			event(new OwnerInvoiceCreated($invoice));
		}
		catch (\Exception $e)
		{
			DB::rollback();
			$invoice = false;
		}
		
		return $invoice;
	}

	/**
	 * make a payment against an invoice
	 * 
	 * @param  string $invoice_number
	 * @param  string $date
	 * @return boolean
	 */
	public static function makePayment($invoice_number, $request)
	{
		$invoice = OwnerInvoice::where('invoice_number',$invoice_number)->first();
		
		$invoice->status = 'paid';
		$invoice->paid_at = $request->paid_at;
		$invoice->payment_method = $request->payment_method;
		$invoice->account_number = $request->account_number;

		if($invoice->save() == true){
			DriverCommission::where('owner_invoice_id', $invoice->id)->update(['status'=>'unpaid']);
		}
		
		return $invoice->save() ? true : false;
	}

	/**
	 * get all invoices
	 *
	 * @param  int $owner_id
	 * @return collection
	 */
	public static function all($owner_id=null)
	{
		if ($owner_id) {
			$payer = OwnerProfile::findOrFail($owner_id);
        	$invoices = $payer->myInvoices;
		} else {
			$invoices = OwnerInvoice::orderBy('id', 'desc')->get();
		}

		return $invoices;
	}

	/**
	 * get details of an invoice
	 * 
	 * @param  int $id
	 * @return array
	 */
	public static function show($invoice_number)
	{
		$invoice = OwnerInvoice::where('invoice_number',$invoice_number)->first();

		return $invoice;
	}

	/**
	 * export the invoice (pdf, doc etc)
	 * 
	 * @param  string $invoice_number 
	 * @param  string $format
	 * @return string
	 */
	public static function export($invoice_number, $format='pdf')
	{
		$invoice = OwnerInvoice::where('invoice_number',$invoice_number)->first();

		$random_code = bin2hex(random_bytes(5));
		$pdf_link = 'public/owner_invoices/owner_'.$invoice->invoice_number.'-'.$random_code.'.pdf';

        $data['invoice'] = $invoice;
        $pdf = PDF::loadView('emails.owner_invoice.pdf', $data);

        Storage::put($pdf_link, $pdf->output());

		return $pdf_link;
	}

	public static function exportPaid($invoice_number, $format='pdf')
	{
		$invoice = OwnerInvoice::where('invoice_number',$invoice_number)->first();

		$random_code = bin2hex(random_bytes(5));
		$pdf_paid_link = 'public/owner_invoices/paid_'.$invoice->invoice_number.'-'.$random_code.'.pdf';

        $data['invoice'] = $invoice;
        $pdf = PDF::loadView('emails.owner_invoice.pdf_paid', $data);

        Storage::put($pdf_paid_link, $pdf->output());

		return $pdf_paid_link;
	}

	/**
	 * Email the invoice to customer
	 * 
	 * @param  string $invoice_number
	 * @return boolean
	 */
	public static function email($invoice_number)
	{
		$invoice = InvoiceSystem::show($invoice_number);

        // to view tempalte,
        //return view('emails.owner_invoice.owner')->with('invoice', $invoice);

        $status = Mail::to($invoice->owner->user)->send(new OwnerMonthlyInvoiceEmail($invoice->owner, $invoice, "Weekly Invoice"));

        return $status;
	}

	public static function acknowledgementEmail($invoice_number)
	{
		$invoice = InvoiceSystem::show($invoice_number);

        // to view tempalte,
        //return view('emails.owner_invoice.owner')->with('invoice', $invoice);

        $status = Mail::to($invoice->owner->user)->send(new OwnerPaymentAcknowledgementEmail($invoice->owner, $invoice, "Payment Acknowledgement"));

        return $status;
	}

	private static function getInvoiceNumber()
	{
		$last_id = OwnerInvoice::all()->last() ? OwnerInvoice::all()->last()->id : 0;

        $id = (int) $last_id + 1;

        return 'INV-'.$id;
	}

}