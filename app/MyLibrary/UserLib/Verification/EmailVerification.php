<?php

namespace App\MyLibrary\UserLib\Verification;

use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\Mail;

use App\Mail\Registration\WelcomeUserEmail;

use App\User;

class EmailVerification implements Verification
{
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * send a mail to user with activation code
     * @return void
	 */
	public function create()
	{
		//$activation_code = 'create code';

        // Send mail to user
        // Mail::to($this->user)->send(new WelcomeUserEmail($this->user, $activation_code));
	}

	/**
	 * if code is correct, activate user
	 * @param  string  $code
     * @return bool
	 */
	public function complete($code)
	{
		// complete the activation
	}

	/**
	 * resend the verification code
	 * @param  string  $code
     * @return bool
	 */
	public function resend()
	{		
		// $activation_code = 'existing code';
	
		// Mail::to($this->user)->send(new ResendActivationEmail($this->user, $activation_code));
	}
}