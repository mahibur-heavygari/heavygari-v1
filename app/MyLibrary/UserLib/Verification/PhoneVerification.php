<?php

namespace App\MyLibrary\UserLib\Verification;

use Cartalyst\Sentinel\Laravel\Facades\Activation;

use App\MyLibrary\SMSLib\SMSFacade;

use App\User;

class PhoneVerification implements Verification
{    
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * send a sms to user with activation code
	 */
	public function create()
	{
        $activation = Activation::create($this->user);
        // overwrite the code to fit in SMS
        $activation->code = mt_rand(1111, 9999);
        $activation->save();

        return $this->sendSMS($activation->code);
	}

	/**
	 * if code is correct, complete activation
	 */
	public function complete($code)
	{
		return Activation::complete($this->user, $code);
	}

	/**
	 * resend the verification code
	 */
	public function resend()
	{
		$activation = Activation::exists($this->user);

		if ($activation) {
			return $this->sendSMS($activation->code);
		} else {
			$this->create();
			return true;
		}
	}

	/**
	 * send SMS
	 */
	private function sendSMS($code)
	{
		$to = $this->user->phone;
        $msg = 'Your verification code is '.$code;
        $ref_id = 'user-'.$this->user->id.'-'.time();

        return SMSFacade::sendEn($to, $msg, $ref_id);
	}
}