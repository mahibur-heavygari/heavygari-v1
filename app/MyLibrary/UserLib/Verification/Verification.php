<?php

namespace App\MyLibrary\UserLib\Verification;

interface Verification {

	 public function create();

	 public function resend();
	 
	 public function complete($code);
}