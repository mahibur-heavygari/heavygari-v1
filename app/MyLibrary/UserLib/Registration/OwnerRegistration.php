<?php

namespace App\MyLibrary\UserLib\Registration;

use App\Events\Owner\NewOwnerRegistered;
use App\OwnerProfile;
use App\PotentialOwner;
use Sentinel;

class OwnerRegistration extends UserRegistration
{
	/**
	 * Complete registration steps
	 */
	public function registerOwner()
	{
		$owner = $this->registerUser();
		return $owner;
	}

	/**
	 * Attach 'owner' role to this user
	 */
	protected function addRole()
	{
		$role = Sentinel::findRoleBySlug('owner');
        $role->users()->attach($this->user);
	}

	/**
	 * Insert profile data into profile table
	 */
	protected function createProfile()
	{
		$profile = new OwnerProfile;
		$profile->user_id = $this->user->id;
		$profile->token = uniqid();
		//$profile->owner_name = $this->user_info['owner_name'];
		$profile->save();
		
		//delete from potential_owners table if phone exists
		$potential_owner = PotentialOwner::where('owner_phone', $this->user->phone);
        if($potential_owner->exists()){
			PotentialOwner::where('owner_phone', $this->user->phone)->delete();
        }
	}

	protected function fireEvents()
	{
		event(new NewOwnerRegistered($this->user));
	}
}