<?php

namespace App\MyLibrary\UserLib\Registration;

use App\Events\Driver\NewDriverRegistered;
use App\DriverProfile;
use Sentinel;

class DriverRegistration extends UserRegistration
{
	/**
	 * Complete registration steps
	 */
	public function registerDriver()
	{
		$driver = $this->registerUser();
		return $driver;
	}

	/**
	 * Attach 'driver' role to this user
	 */
	protected function addRole()
	{
		$role = Sentinel::findRoleBySlug('driver');
        $role->users()->attach($this->user);
	}

	/**
	 * Insert profile data into profile table
	 */
	protected function createProfile()
	{
		$profile = new DriverProfile;
		$profile->user_id = $this->user->id;
		$profile->vehicle_owner_profile_id = $this->user_info['vehicle_owner_profile_id'];
		$profile->token = uniqid();
		$profile->save();

		# insert default preferences
		
		$profile->bookingTypePreference()->create([
		    'on_demand' => true,
		    'advance' => true,
		]);
		
		$profile->bookingCategoryPreference()->create([
		    'full' => true,
		    'shared' => true,
		]);

		$profile->bookingDistancePreference()->create([
		    'short_distance' => true,
		    'long_distance' => true,
		]);
	}

	protected function fireEvents()
	{
		event(new NewDriverRegistered($this->user));
	}
}