<?php

namespace App\MyLibrary\UserLib\Registration;

use Sentinel;

abstract class UserRegistration
{
	protected $user;

	protected $user_info;

	protected $error = false;

	public function __construct($user_info)
	{
		$this->user_info = $user_info;
	}

	public function getError() {
		return $this->error;
	}

	protected function registerUser()
	{
		$user_data = [
		    'phone' => $this->user_info['phone'],
		    'name' => $this->user_info['name'],
		    'email' => $this->user_info['email'],
		    'password' => $this->user_info['password'],
		    'sms' => 'আপনার রেজিস্ট্রেশন সফল হয়েছে, 
আপনার লগইন ইউজারনেম
ফোন নং: '.$this->user_info['phone'].'
পাসওয়ার্ড: '.$this->user_info['password'].'
টিম 
হেভিগাড়ী টেকনোলজিস লিমিটেড'
		];

		try {

			$this->user = Sentinel::register($user_data);

			$this->addRole();

			$this->createProfile();

			$this->fireEvents();

			return $this->user;

		} catch (\Exception $e) {

			$this->error = $e;

			return false;
		}
	}

	abstract protected function addRole();

	abstract protected function createProfile();

	abstract protected function fireEvents();
}