<?php

namespace App\MyLibrary\UserLib\Registration;

use App\Events\Customer\NewCustomerRegistered;
use App\CustomerProfile;
use Sentinel;

class CustomerRegistration extends UserRegistration
{
	/**
	 * Complete registration steps
	 */
	public function registerCustomer()
	{
		$customer = $this->registerUser();
		return $customer;
	}

	/**
	 * Attach 'customer' role to this user
	 */
	protected function addRole()
	{
		$role = Sentinel::findRoleBySlug('customer');
        $role->users()->attach($this->user);
	}

	/**
	 * Insert profile data into profile table
	 */
	protected function createProfile()
	{
		$this->user->status = 'active';
		$this->user->save();
		
		$profile = new CustomerProfile;
		$profile->user_id = $this->user->id;
		$profile->token = uniqid();
		$profile->save();
	}

	protected function fireEvents()
	{
		event(new NewCustomerRegistered($this->user));
	}
}