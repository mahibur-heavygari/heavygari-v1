<?php

namespace App\MyLibrary\UserLib\ForgotPassword;

use Mail;
use Sentinel;
use Validator;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use App\Mail\ForgotPassword\RecoverPasswordEmail;

class RecoveryManager
{
	public $error;

	/**
	 * user forgot password, requesting for recovery
	 *
	 * @param   $email
	 * @return  mixed
	 */
	public function requestForRecovery(Request $request)
	{
		// does this email exist?
		$user = Sentinel::findByCredentials(['email' => $request->email]);
        if (!$user) {
            $this->error = 'Sorry, This email does not exist.';
			return false;
        }
		
		// ok, we are taking action..
        $reminder = Reminder::create($user);
        Mail::to($user->email)->send(new RecoverPasswordEmail($user, $reminder->code));

		return true;
	}

	/**
	 * user comes to reset password page with reminder code
	 *
	 * @param   $email
	 * @param   $reminder_code
	 * @return  mixed
	 */
	public function getResetPageAccess(Request $request)
	{
		//validation
		$rules = [
            'email' => 'required|email',
            'reminder_code' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            $this->error = 'Sorry, The email and/or the code is not valid';
			return false;
        }

		// does this email exist?
		$user = Sentinel::findByCredentials(['email' => $request->email]);
        if (!$user) {
            $this->error = 'Sorry, This email does not exist.';
			return false;
        }

        // is the code correct?
        if (Reminder::exists($user, $request->reminder_code)) { 
            return true;
        } else {
            $this->error = 'Sorry, This code is incorrect.';
		    return false;
        }
	}

	/**
	 * user had filled up the reset password form
	 *
	 * @param   $email
	 * @param   $reminder_code
	 * @return  mixed
	 */
	public function resetUserPassword(Request $request)
	{
		//validation
		$rules = [
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            $this->error = 'Please enter all the fields';
			return false;
        }

        // does this email exist?
		$user = Sentinel::findByCredentials(['email' => $request->email]);
        if (!$user) {
            $this->error = 'Sorry, This email does not exist.';
			return false;
        }

        if ($reminder = Reminder::complete($user, $request->reminder_code, $request->password))
		{
		    return true;
		}
		else
		{
			dd($reminder);

		    $this->error = 'Sorry, This code is incorrect.';
		    return false;
		}
	}

}