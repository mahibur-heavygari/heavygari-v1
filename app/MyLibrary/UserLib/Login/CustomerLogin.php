<?php

namespace App\MyLibrary\UserLib\Login;

use App\CustomerProfile;
use Sentinel;
use DB;

class CustomerLogin extends UserLogin
{
	/**
	 * is the user an customer?
	 */
	protected function isAuthorizedRole()
	{
		$user = $this->user;

        $customer_role = Sentinel::findRoleBySlug('customer');
        if (!$user->inRole($customer_role)) {
        	$this->error['message'] = 'User is not a customer';
			$this->error['type'] = 'incorrect_role';
            return false;
        }

        $corporate_manager = $this->checkCorporateManager( $user->customerProfile );
        if(!is_null($corporate_manager)){
        	$this->error['message'] = 'This is a corporate manager account. Please use web dashboard.';
			$this->error['type'] = 'incorrect_role';
            return false;
        }

		return true;
	}

	/**
	 * did the customer completed his profile?
	 */
	protected function isProfileComplete()
	{
		$user = $this->user;

        $customer = $user->customerProfile;

        if ($customer->profile_complete=='no') {
        	$this->error['message'] = 'Please complete your profile';
			$this->error['type'] = 'incomplete_profile';
			$this->setErrorDetails();
            return false;
        }

		return true;
	}

	/**
	 * set customer id, token
	 */
	protected function setErrorDetails()
	{
		$user = $this->user;

		if ($this->isAuthorizedRole()) {
			$customer = $user->customerProfile;
			$this->error['customer_id'] = $customer->id;
			$this->error['token'] = $customer->token;
		}

	}

	protected function checkCorporateManager($customer_profile)
	{
		$is_corporate = DB::table('rel_corporate_customers')->where('customer_profile_id', $customer_profile->id)->where('user_type', 'manager')->first();
		return $is_corporate;
	}
}