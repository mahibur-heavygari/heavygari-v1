<?php

namespace App\MyLibrary\UserLib\Login;

use \Cartalyst\Sentinel\Checkpoints as SentinelCheckpoint;
use Sentinel;

abstract class UserLogin
{
	protected $user = null;

	protected $error;


	public function getError() {
		return $this->error;
	}

	public function login($phone, $password)
	{
		$authenticated = $this->isAuthenticated($phone, $password);
		if (! ( $authenticated && $this->isAuthorizedRole() && $this->isProfileComplete() && $this->isAdminApproved() )) {

			// user should not be logged in?
			if( $authenticated ){
				$user = Sentinel::getUser();
				Sentinel::logout($user);
			}			
			
			$this->user = null;

			return false;
		}

		return $this->user;
	}

	protected function isAuthenticated($phone, $password)
	{
		$credentials = [
		    'phone'    => $phone,
		    'password' => $password,
		];

		try {
		    $user = Sentinel::authenticate($credentials);
		    if ($user) {
	    		$this->user = $user;
	    		return true;
		    } else {
		    	$this->error['message'] = 'Incorrect username/password';
				$this->error['type'] = 'incorrect_login';
		    }
		}
		catch (SentinelCheckpoint\NotActivatedException $e) {
			$this->error['message'] = 'Phone is not verified';
			$this->error['type'] = 'unverified_phone';
			$this->user = $e->getUser();
			$this->setErrorDetails();
		}
		catch (\Exception $e) {
		    $this->error['message'] = 'Unexpected Error';
			$this->error['type'] = 'unknown_error';
		}

		return false;
	}

	abstract protected function isAuthorizedRole();

	abstract protected function isProfileComplete();

	protected function isAdminApproved()
	{
		if ($this->user->status!='active') {
    		$this->error['message'] = 'Sorry! Your account is not active. Please contact Admin.';
			$this->error['type'] = 'inactive_account';
			$this->setErrorDetails();
			return false;
    	}

    	return true;
	}
}