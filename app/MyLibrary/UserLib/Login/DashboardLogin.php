<?php

namespace App\MyLibrary\UserLib\Login;

use Sentinel;

class DashboardLogin extends UserLogin
{
	/**
	 * is the user an customer, admin or owner?
	 */
	protected function isAuthorizedRole()
	{
		$user = $this->user;

        $user_role_profile = $this->getUserRoleProfile();
        if ($user_role_profile) {
            return true;
        }

    	$this->error['message'] = 'Sorry! there is no dashboard for drivers';
		$this->error['type'] = 'incorrect_role';

        return false;
	}

    /**
     * did the user complete his profile?
     */
    protected function isProfileComplete()
    {
        $user = $this->user;

        $profile = $this->getUserRoleProfile();

        if ($profile->profile_complete=='no') {
            $this->error['message'] = 'Please complete your profile';            
            $this->error['type'] = 'incomplete_profile';
            $this->setErrorDetails();
            return false;
        }

        return true;
    }

    /**
     * set error details (user type, profile id etc)
     */
    protected function setErrorDetails()
    {
        $role = $this->getUserRole();
        $profile = $this->getUserRoleProfile();
        $this->error['user_type'] = $role;
        $this->error['profile_id'] = $profile->id;
        $this->error['token'] = $profile->token;
    }

    /**
     * which type of user id he?
     */
    private function getUserRole()
    {
        $user = $this->user;

        $adminRole = Sentinel::findRoleBySlug('super-admin');
        $ownerRole = Sentinel::findRoleBySlug('owner');
        $customerRole = Sentinel::findRoleBySlug('customer');

        if ($user->inRole($adminRole)) {
            return 'admin';
        } elseif ($user->inRole($ownerRole)) {
            return 'owner';
        } elseif ($user->inRole($customerRole)) {
            return 'customer';
        }
        
        return false;
    }

    /**
     * database profile for this user
     */
    private function getUserRoleProfile()
    {
        $user = $this->user;

        $role = $this->getUserRole();

        $rel_name = $role.'Profile';
        $profile = $user->$rel_name;

        return $profile;
    }
}