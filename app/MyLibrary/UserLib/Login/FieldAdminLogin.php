<?php

namespace App\MyLibrary\UserLib\Login;

use App\AdminProfile;
use Sentinel;

class FieldAdminLogin extends UserLogin
{
	/**
	 * is the user an field team?
	 */
	protected function isAuthorizedRole()
	{
		$user = $this->user;

        $field_admin_role = Sentinel::findRoleBySlug('field-admin');
        if (!$user->inRole($field_admin_role)) {
        	$this->error['message'] = 'User is not a field admin';
			$this->error['type'] = 'incorrect_role';
            return false;
        }

		return true;
	}

	/**
	 * did the field admin completed his profile?
	 */
	protected function isProfileComplete()
	{
		$user = $this->user;

        $admin = $user->adminProfile;

        if ($admin->profile_complete=='no') {
        	$this->error['message'] = 'Please complete your profile';
			$this->error['type'] = 'incomplete_profile';
			$this->setErrorDetails();
            return false;
        }

		return true;
	}

	/**
	 * set field admin id, token
	 */
	protected function setErrorDetails()
	{
		$user = $this->user;

		if ($this->isAuthorizedRole()) {
			$admin = $user->adminProfile;
			$this->error['admin_id'] = $admin->id;
			$this->error['token'] = $admin->token;
		}

	}
}