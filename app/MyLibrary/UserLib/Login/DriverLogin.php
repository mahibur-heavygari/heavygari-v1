<?php

namespace App\MyLibrary\UserLib\Login;

use App\CustomerProfile;
use Sentinel;

class DriverLogin extends UserLogin
{
	/**
	 * is the user an driver?
	 */
	protected function isAuthorizedRole()
	{
		$user = $this->user;

        $driver_role = Sentinel::findRoleBySlug('driver');
        if (!$user->inRole($driver_role)) {
        	$this->error['message'] = 'User is not a driver';
			$this->error['type'] = 'incorrect_role';
			return false;
        }

        return true;
	}

	/**
	 * did this driver complete the profile?
	 */
	protected function isProfileComplete()
	{
		$user = $this->user;

        $driver = $user->driverProfile;

        if ($driver->profile_complete=='no') {
        	$this->error['message'] = 'Please complete your profile';
			$this->error['type'] = 'incomplete_profile';		
			$this->setErrorDetails();
            return false;
        }

		return true;
	}

	/**
	 * set driver id, token
	 */
	protected function setErrorDetails()
	{
		$user = $this->user;

		if ($this->isAuthorizedRole()) {
			$driver = $user->driverProfile;
			$this->error['driver_id'] = $driver->id;
			$this->error['token'] = $driver->token;
		}
	}
}