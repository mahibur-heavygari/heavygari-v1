<?php

namespace App\MyLibrary\SMSLib;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;
use App\SmsHistory;


class SMSFacade
{
	/* send sms to a number */
	public static function send($to, $message, $ref_id, $user_type=NULL)
	{	
		// TODO :: validation $to, $msg, $ref

		$to = self::formatNumberProperly($to);
		
		$credentials = self::getCredentials();
		
		$client = new Client();

		$unicode_msg = mb_strtoupper(bin2hex(mb_convert_encoding($message, 'UTF-16BE', 'UTF-8')));

		try{
			$response = $client->request('POST', $credentials['url'], [
			    'form_params' => [
			        'user' => $credentials['user'],
		    		'pass' => $credentials['pass'],
		    		'sid' => $credentials['sid'],
		    		'sms[0][0]' => $to,
		    		'sms[0][1]' => $unicode_msg,
		    		'sms[0][2]' => $ref_id	    		
			    ]
			]);
		} catch(\Exception $e) {
	    	return false;
	    }

		Log::info('Trying to Send SMS. To :'.$to.', Message : '.$message.' Response : '. var_export($response, true));

		$xml = simplexml_load_string($response->getBody()->getContents());
		$json = json_encode($xml);

		SmsHistory::create(['sms_text'=>$message, 'to'=>$to, 'response'=>$json, 'user_type'=>$user_type]);

		$array = json_decode($json,TRUE);
		if (isset($array['SMSINFO']['REFERENCEID'])) {
			return true;
		}		
		return false;
	}

	public static function sendEn($to, $message, $ref_id, $user_type=NULL)
	{	
		// TODO :: validation $to, $msg, $ref

		$to = self::formatNumberProperly($to);
		
		$credentials = self::getCredentialsEn();
		
		$client = new Client();

		//$unicode_msg = mb_strtoupper(bin2hex(mb_convert_encoding($message, 'UTF-16BE', 'UTF-8')));

		try{
			$response = $client->request('POST', $credentials['url'], [
			    'form_params' => [
			        'user' => $credentials['user'],
		    		'pass' => $credentials['pass'],
		    		'sid' => $credentials['sid'],
		    		'sms[0][0]' => $to,
		    		'sms[0][1]' => $message,
		    		'sms[0][2]' => $ref_id	    		
			    ]
			]);
		} catch(\Exception $e) {
	    	return false;
	    }

		Log::info('Trying to Send SMS. To :'.$to.', Message : '.$message.' Response : '. var_export($response, true));

		$xml = simplexml_load_string($response->getBody()->getContents());
		$json = json_encode($xml);

		SmsHistory::create(['sms_text'=>$message, 'to'=>$to, 'response'=>$json, 'user_type'=>$user_type]);

		$array = json_decode($json,TRUE);
		if (isset($array['SMSINFO']['REFERENCEID'])) {
			return true;
		}		
		return false;
	}

	//send sms to multiple numbers
	public static function sendBulk($toCollection, $message, $user_type=NULL)
	{	
		$toCollection->transform(function ($item, $key) {
		    return self::formatNumberProperly($item);;
		});
		$toStr = $toCollection->toJson();

		$credentials = self::getCredentials();
		
		$client = new Client();

		$unicode_msg = mb_strtoupper(bin2hex(mb_convert_encoding($message, 'UTF-16BE', 'UTF-8')));

		$param = [
			'form_params' => [
			        'user' => $credentials['user'],
		    		'pass' => $credentials['pass'],
		    		'sid' => $credentials['sid'],    		
			    ]
		];
		foreach ($toCollection as $key => $to) {
			$param["form_params"]["sms[$key][0]"] = $to;
			$param["form_params"]["sms[$key][1]"] = $unicode_msg;
			$param["form_params"]["sms[$key][2]"] = "$to"."_".time();
		}

		try{
			$response = $client->request('POST', $credentials['url'], $param);
		} catch(\Exception $e) {
	    	return false;
	    }

		Log::info('Trying to Send SMS. To :'.$to.', Message : '.$message.' Response : '. var_export($response, true));

		$xml = simplexml_load_string($response->getBody()->getContents());
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);

		SmsHistory::create(['sms_text'=>$message, 'to'=>$toStr, 'response'=>$json, 'single_bulk'=>'bulk', 'user_type'=>$user_type]);

		if (isset($array['PARAMETER']) && ($array['PARAMETER']=='OK') && isset($array['LOGIN']) && ($array['LOGIN']=='SUCCESSFULL') && isset($array['STAKEHOLDERID']) && ($array['STAKEHOLDERID']=='OK') && isset($array['PERMITTED']) && ($array['PERMITTED']=='OK')) {
			return true;
		}		
		return false;
	}

	private static function getCredentials()
	{
		// return [
		// 	'url' => env('SSL_URL'),
		// 	'user' => env('SSL_USER'),
		// 	'pass' => env('SSL_PASS'),
		// 	'sid' => env('SSL_SID'),
		// ];
		return [
			'url' => 'http://sms.sslwireless.com/pushapi/dynamic/server.php',
			'user' => 'heavygari',
			'pass' => 'Heavygari@123',
			'sid' => 'HeavyGariBangla',
		];
	}

	private static function getCredentialsEn()
	{
		return [
			'url' => 'http://sms.sslwireless.com/pushapi/dynamic/server.php',
			'user' => 'heavygari',
			'pass' => 'Heavygari@123',
			'sid' => 'HeavyGari',
		];
	}

	/**
	 * format the phone number properly
	 *
	 * @param  string $phone 
	 * @return string
	 */
	public static function formatNumberProperly($phone)
	{
		// remove '+' sign if any
		$phone = ltrim($phone, '+');

		// if 88 is not present add it.
		$phone = substr( $phone, 0, 2 ) === "88" ? $phone : '88'.$phone;

		return $phone;
	}

}