<?php

namespace App\MyLibrary\DriverCommission;

use DB;
use App\Booking;
use App\OwnerProfile;
use App\DriverProfile;
use App\OwnerInvoiceItem;
use App\DriverCommission as DriverCommissionModel;
use App\DriverCommissionItem;
use App\DriverCommission;

class CommissionSystem
{
	public static function create($owner_id, $month, $year, $owner_invoice_id)
	{
		$owner = OwnerProfile::findOrFail($owner_id);
		$drivers = $owner->myDrivers;

		foreach ($drivers as $key => $driver) {

			$commission = DriverCommissionModel::where('driver_profile_id', $driver->id)->where('owner_invoice_id', $owner_invoice_id)->first();
			if($commission)
				continue;

			//$trips = $driver->myTrips()->completed()->whereYear('datetime', $year)->whereMonth('datetime', $month)->get();
			//$total_amount = $trips->sum('earnings.driver_earning');
			$driver_all_trips = $driver->myTrips()->completed()->get();
            $last_week_trips = array();
            $total_amount = 0;

            foreach ($driver_all_trips as $key => $trip) {
                $invoice_item = OwnerInvoiceItem::where('invoice_id', $owner_invoice_id)->where('booking_id', $trip->id)->first();
                if($invoice_item){
                    $last_week_trips[] = $invoice_item->bookings;
                    $total_amount += $invoice_item->bookings->earnings->driver_earning;
                }

            }
			
			if($total_amount == 0)
        		continue;

			//create driver_commissions table record
			$driver_commission = new DriverCommissionModel;
	        $driver_commission->driver_profile_id = $driver->id;
	        $driver_commission->owner_id = $owner_id; 
	        $driver_commission->owner_invoice_id = $owner_invoice_id;
	        $driver_commission->total_amount = $total_amount;
	        $driver_commission->month = $month;
	        $driver_commission->year = $year;
	        $driver_commission->generate_date = date('Y-m-d');
	        $driver_commission->due_date = null;
	        $driver_commission->save();

	        $driver_commission_id = $driver_commission->id;

			foreach ($last_week_trips as $key => $trip) {
				//create driver_commission_items table record
				$driver_commission_item = new DriverCommissionItem;
				$driver_commission_item->driver_commission_id = $driver_commission_id;
				$driver_commission_item->booking_id = $trip->id;
				$driver_commission_item->payable = $trip->earnings['driver_earning'];
				$driver_commission_item->save();
			}
		}
	}

	public static function getCommissionItems($id){
		$driver_commission_item = DriverCommissionItem::where('driver_commission_id', $id);
        if($driver_commission_item->count()==0)
            abort(404);

        $commission_items = $driver_commission_item->orderBy('id', 'desc')->get();
        return $commission_items;
	}

	public static function makePayment($request){
		$driver_commission = DriverCommission::find($request->driver_commission_id);
		$sms_text = '';
		$driver_number = '';
		if(is_null($driver_commission))
			abort(404);
		if($driver_commission->status=='due')
			$notification = ['danger', 'Owner did not pay'];
		if($driver_commission->status=='paid')
			$notification = ['success', 'Commission already paid'];
		if($driver_commission->status=='unpaid'){
			$driver_commission->status = 'paid';
			$driver_commission->paid_at = $request->paid_at;
			$driver_commission->payment_method = $request->payment_method;
			$driver_commission->account_number = $request->account_number;
			$driver_commission->total_amount = $request->amount;			
			$driver_commission->save();

			$sms_text = self::getSmsText($driver_commission, $request->amount);
			$driver_number = $driver_commission->driver->user->phone;

			$notification = ['success', 'Payment Successfully Completed'];
		}
		return compact('notification', 'sms_text', 'driver_number');
	}

	private static function  getSmsText($driver_commission, $amount){
		/*$monthsArr = ['January' => 'জানুয়ারী', 'February' => 'ফেব্রুয়ারী', 'March' => 'মার্চ', 'April' => 'এপ্রিল', 'May' => 'মে', 'June' => 'জুন', 'July' => 'জুলাই', 'August' => 'আগস্ট', 'September' => 'সেপ্টেম্বর ', 'October' => 'অক্টোবর', 'November' => 'নভেম্বর', 'December' => 'ডিসেম্বর'];*/

		$name = $driver_commission->driver->user->name;
		/*$month = $driver_commission->month;
		$month = $monthsArr[$month];*/
		$sms_text = 'হ্যালো '.$name.',
আপনাকে '.$amount.' টাকা পরিশোধ করা হলো,
ধন্যবাদ ।';
		return $sms_text;
	}

}