<?php

namespace App\MyLibrary\GoogleRecaptchaLib;

use Illuminate\Support\Facades\Config;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GoogleRecaptcha
{
    static $SITE_KEY  = "";
    static $SECRET_KEY = "";

    function __construct() {
        self::$SITE_KEY = Config::get('heavygari.google_recaptcha.site_key');
        self::$SECRET_KEY = Config::get('heavygari.google_recaptcha.secret_key');
    }

    function VerifyCaptcha($captchaResponse)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $client = new Client();

        $response = $client->request('POST', $url, [
            'form_params' => [
                'secret' => GoogleRecaptcha::$SECRET_KEY,
                'response' => $captchaResponse
            ]
        ]);
        $resArr = json_decode( $response->getBody()->getContents(), true );
        return $resArr;
    }
}