<?php

namespace App;

class VehicleCapacityType extends BaseModel
{
    protected $table = 'base_vehicle_capacity_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    // BEGIN RELATIONS
    
    /**
     * getOwner
     */
    public function vehicleTypes()
    {
        return $this->hasMany('App\VehicleType', 'capacity_type_id');
    }

    // END OF RELATIONS
}
