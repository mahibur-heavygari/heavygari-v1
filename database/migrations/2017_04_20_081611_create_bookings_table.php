<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_id')->unique();
            $table->string('tracking_code')->unique();            
            $table->bigInteger('vehicle_id')->unsigned()->nullable();
            $table->bigInteger('vehicle_owner_profile_id')->unsigned()->nullable();
            $table->bigInteger('customer_profile_id')->unsigned();
            $table->bigInteger('driver_profile_id')->unsigned()->nullable();
            $table->timestamp('datetime')->nullable()->comment('UTC');
            $table->enum('payment_by', ['customer', 'recipient']);
            $table->string('recipient_name');
            $table->string('recipient_phone');
            $table->mediumText('particular_details')->nullable();
            $table->string('waiting_time')->nullable();
            $table->enum('booking_category', ['full', 'shared']);
            $table->enum('booking_type', ['on-demand', 'advance'])->default('on-demand');
            $table->enum('trip_type', ['single', 'round'])->default('single');
            $table->enum('is_payment_collected', ['no', 'yes']);
            $table->text('invoice_pdf')->nullable();
            $table->enum('status', ['open', 'upcoming', 'ongoing', 'completed','cancelled']);
            $table->timestamp('accepted_at')->nullable()->comment('UTC');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
            $table->foreign('vehicle_owner_profile_id')->references('id')->on('profile_vehicle_owners')->onDelete('cascade');
            $table->foreign('customer_profile_id')->references('id')->on('profile_customers')->onDelete('cascade');
            $table->foreign('driver_profile_id')->references('id')->on('profile_drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
