<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_booking_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->unsigned();
            $table->integer('product_category_id')->unsigned();
            $table->integer('weight_category_id')->unsigned()->nullable();
            $table->integer('volumetric_category_id')->unsigned()->nullable();
            $table->float('weight')->comment('/kg');
            $table->float('volumetric_width')->nullable()->comment('cm');
            $table->float('volumetric_height')->nullable()->comment('cm');
            $table->float('volumetric_length')->nullable()->comment('cm');
            $table->integer('quantity');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->foreign('product_category_id')->references('id')->on('base_product_categories')->onDelete('cascade');
            $table->foreign('weight_category_id')->references('id')->on('base_weight_category')->onDelete('cascade');
            $table->foreign('volumetric_category_id')->references('id')->on('base_volumetric_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_booking_details');
    }
}
