<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerManagerManagerEmailonVehicleOwnerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_vehicle_owners', function (Blueprint $table) {
            $table->string('manager_email')->after('manager_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_vehicle_owners', function (Blueprint $table) {
            $table->dropColumn('manager_email');
        });
    }
}
