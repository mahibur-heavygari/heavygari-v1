<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryInvoicePhotoDeliveryOrderPhotoToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->text('delivery_order_photo')->nullable()->after('invoice_pdf');
            $table->text('delivery_invoice_photo')->nullable()->after('delivery_order_photo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('delivery_order_photo');
            $table->dropColumn('delivery_invoice_photo');
        });
    }
}
