<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfileCustomers extends Migration
{

    public function up()
    {
        Schema::create('profile_customers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->text('about')->nullable();
            $table->text('token')->nullable();
            $table->enum('profile_complete', ['no', 'yes'])->default('no');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::drop('profile_customers');
    }
}
