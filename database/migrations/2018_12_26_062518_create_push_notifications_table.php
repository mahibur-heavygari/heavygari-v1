<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('mobile_web', ['mobile', 'web'])->default('mobile');
            $table->enum('admin_notification', ['yes', 'no'])->default('no');
            $table->string('title');
            $table->text('body');
            $table->string('category')->nullable();
            $table->string('type')->nullable();
            $table->bigInteger('sent_user_id')->nullable();
            $table->string('received_user_type')->nullable();
            $table->string('received_user_id')->nullable();
            $table->bigInteger('booking_id')->unsigned()->nullable();
            $table->string('unique_id')->nullable();
            $table->tinyInteger('admin_seen')->default(0);
            $table->tinyInteger('admin_read')->default(0);
            $table->tinyInteger('owner_seen')->default(0);
            $table->tinyInteger('owner_read')->default(0);
            $table->tinyInteger('customer_seen')->default(0);
            $table->tinyInteger('customer_read')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notifications');
    }
}
