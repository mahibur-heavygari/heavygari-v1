<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebAppToProfileCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->integer('web_app')->nullable()->after('first_booking_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->dropColumn('web_app');
        });
    }
}
