<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHiredDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hired_drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('phone')->nullable(); 
            $table->bigInteger('booking_id')->unsigned()->nullable();
            $table->bigInteger('vehicle_owner_profile_id')->unsigned()->nullable();
            $table->bigInteger('driver_profile_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('set null');
            $table->foreign('vehicle_owner_profile_id')->references('id')->on('profile_vehicle_owners')->onDelete('cascade');
            $table->foreign('driver_profile_id')->references('id')->on('profile_drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hired_drivers');
    }
}
