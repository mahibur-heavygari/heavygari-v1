<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromToDiscountDateColumnsToFullBookingPriceManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('full_booking_price_manager', function (Blueprint $table) {
            $table->timestamp('from_discount_date')->nullable()->after('discount_percent');
            $table->timestamp('to_discount_date')->nullable()->after('from_discount_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('full_booking_price_manager', function (Blueprint $table) {
            $table->dropColumn('from_discount_date');
            $table->dropColumn('to_discount_date');
        });
    }
}
