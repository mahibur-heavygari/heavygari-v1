<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRideSharingTaxToBookingEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_earnings', function (Blueprint $table) {
            $table->decimal('ride_sharing_tax', 10, 2)->nullable()->after('heavygari_earning');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_earnings', function (Blueprint $table) {
            $table->dropColumn('ride_sharing_tax');
        });
    }
}
