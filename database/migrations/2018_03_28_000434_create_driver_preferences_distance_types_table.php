<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverPreferencesDistanceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_preferences_distance_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('driver_profile_id')->unsigned();
            $table->boolean('short_distance')->default(true)->comment('boolean');
            $table->boolean('long_distance')->default(true)->comment('boolean');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('driver_profile_id')->references('id')->on('profile_drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_preferences_distance_types');
    }
}
