<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOwnerInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_invoices', function (Blueprint $table) {
            $table->string('payment_method')->after('total_amount')->nullable();     
            $table->string('account_number')->after('payment_method')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_invoices', function (Blueprint $table) {
            $table->dropColumn('payment_method');
            $table->dropColumn('account_number');
        });
    }
}
