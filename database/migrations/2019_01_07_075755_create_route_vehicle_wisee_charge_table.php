<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteVehicleWiseeChargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_vehicle_wise_charge', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_category_id')->unsigned();
            $table->smallInteger('vehicle_type_id')->unsigned();
            $table->enum('shared_or_full', ['full', 'shared'])->default('full');
            $table->decimal('multiplier', 10, 2)->nullable();
            $table->decimal('surcharge', 10, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('trip_category_id')->references('id')->on('base_trip_categories')->onDelete('cascade');
            $table->foreign('vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_vehicle_wise_charge');
    }
}
