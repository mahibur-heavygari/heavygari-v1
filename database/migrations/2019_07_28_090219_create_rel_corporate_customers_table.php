<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelCorporateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_corporate_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('corporate_customer_id')->unsigned();
            $table->bigInteger('customer_profile_id')->unsigned();
            $table->enum('user_type',['admin','manager','user'])->default('user');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('corporate_customer_id')->references('id')->on('corporate_customers')->onDelete('cascade');
            $table->foreign('customer_profile_id')->references('id')->on('profile_customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_corporate_customers');
    }
}
