<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDiscountDateColumnFromFullBookingPriceManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('full_booking_price_manager', function (Blueprint $table) {
            $table->dropColumn('discount_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('full_booking_price_manager', function (Blueprint $table) {       
            $table->date('discount_date')->nullable()->after('discount_percent');
        });
    }
}
