<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAmountToCorporateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_customers', function (Blueprint $table) {
            $table->dropColumn('amount');
            $table->decimal('balance', 10, 2)->default(0)->after('bank_guarantee');
            $table->decimal('limit', 10, 2)->nullable()->after('balance');
            $table->enum('debit_credit',['debit','credit'])->nullable()->after('limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_customers', function (Blueprint $table) {            
            $table->decimal('amount', 10, 2)->default(0)->after('bank_guarantee');
            $table->dropColumn('balance');
            $table->dropColumn('limit');
            $table->dropColumn('debit_credit');
        });
    }
}
