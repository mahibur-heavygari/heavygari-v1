<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverCommissionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_commission_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('driver_commission_id')->unsigned();
            $table->bigInteger('booking_id')->unsigned();
            $table->decimal('payable',13,2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('driver_commission_id')->references('id')->on('driver_commissions')->onDelete('restrict');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_commission_items');
    }
}
