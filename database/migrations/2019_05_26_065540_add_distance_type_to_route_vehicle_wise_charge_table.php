<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistanceTypeToRouteVehicleWiseChargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_vehicle_wise_charge', function (Blueprint $table) {
            $table->string('distance_type')->nullable()->after('multiplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('route_vehicle_wise_charge', function (Blueprint $table) {
            $table->dropColumn('distance_type');
        });
    }
}
