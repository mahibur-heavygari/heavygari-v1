<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfileVehicleOwners extends Migration
{

    public function up()
    {
        Schema::create('profile_vehicle_owners', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('ref_no')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->string('company_name')->nullable();
            $table->text('about')->nullable();           
            $table->string('ownership_card_number')->nullable();
            $table->string('ownership_card_picture')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('account_number')->nullable();
            $table->text('token')->nullable();
            $table->enum('profile_complete', ['no', 'yes'])->default('no');
            $table->enum('without_app', ['no', 'yes'])->default('no');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::drop('profile_vehicle_owners');
    }
}
