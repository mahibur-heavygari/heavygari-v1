<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BasePoints extends Migration
{

    public function up()
    {
        Schema::create('base_points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('fullname')->unique();
            $table->decimal('lat', 10, 8);
            $table->decimal('lon', 11, 8);
            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::drop('base_points');
    }
}
