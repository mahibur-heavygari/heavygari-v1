<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWaitingChargeToBookingInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_invoices', function (Blueprint $table) {            
            $table->decimal('waiting_cost', 10, 2)->nullable()->after('surcharge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_invoices', function (Blueprint $table) {
            $table->dropColumn('waiting_cost');
        });
    }
}
