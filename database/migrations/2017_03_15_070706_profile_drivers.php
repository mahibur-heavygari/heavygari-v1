<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfileDrivers extends Migration
{

    public function up()
    {
        Schema::create('profile_drivers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('ref_no')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('vehicle_owner_profile_id')->unsigned()->nullable();
            $table->text('about')->nullable();
            $table->string('license_no')->nullable();
            $table->string('license_image')->nullable();
            $table->string('license_date_of_issue')->nullable();
            $table->string('license_date_of_expire')->nullable();
            $table->string('license_issuing_authority')->nullable();
            $table->text('token')->nullable();
            $table->enum('profile_complete', ['no', 'yes'])->default('no');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('vehicle_owner_profile_id')->references('id')->on('profile_vehicle_owners')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('profile_drivers');
    }
}
