<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_ratings', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->unsigned()->unique();
            $table->bigInteger('driver_profile_id')->unsigned();
            $table->mediumText('review')->nullable();
            $table->tinyInteger('rating');
            $table->timestamps();
            $table->softDeletes();
           
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->foreign('driver_profile_id')->references('id')->on('profile_drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('driver_ratings');
    }
}
