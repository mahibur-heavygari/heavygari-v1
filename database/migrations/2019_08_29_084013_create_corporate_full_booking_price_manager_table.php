<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorporateFullBookingPriceManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_full_booking_price_manager', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('corporate_id')->unsigned();
            $table->smallInteger('vehicle_type_id')->unsigned();
            $table->decimal('base_fare', 6, 2);
            $table->decimal('short_trip_rate', 6, 2);
            $table->decimal('up_trip_rate', 6, 2);
            $table->decimal('down_trip_rate', 6, 2);
            $table->decimal('long_up_trip_rate', 6, 2);
            $table->decimal('long_down_trip_rate', 6, 2);               
            $table->decimal('surcharge_rate', 6, 2);
            $table->decimal('discount_percent', 6, 2);
            $table->timestamp('from_discount_date')->nullable();
            $table->timestamp('to_discount_date')->nullable();
            $table->decimal('admin_commission', 6, 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('corporate_id')->references('id')->on('corporate_customers')->onDelete('cascade');
            $table->foreign('vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporate_full_booking_price_manager');
    }
}
