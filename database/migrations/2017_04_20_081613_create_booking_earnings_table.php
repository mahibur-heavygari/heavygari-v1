<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_earnings', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->unsigned();
            $table->decimal('owner_earning', 10, 2);
            $table->decimal('driver_earning', 10, 2);
            $table->decimal('heavygari_earning', 10, 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_earnings');
    }
}
