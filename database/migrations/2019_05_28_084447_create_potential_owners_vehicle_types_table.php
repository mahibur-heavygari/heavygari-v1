<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePotentialOwnersVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_potential_owners_vehicle_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('potential_owner_id')->unsigned();
            $table->smallInteger('base_vehicle_type_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('potential_owner_id')->references('id')->on('potential_owners')->onDelete('cascade');
            $table->foreign('base_vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potential_owners_vehicle_types');
    }
}
