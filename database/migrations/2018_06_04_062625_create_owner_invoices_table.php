<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_number')->unique();            
            $table->date('invoice_date');
            $table->date('due_date');
            $table->bigInteger('owner_id')->unsigned();
            $table->decimal('total_amount',13,2);
            $table->text('note');
            $table->string('month');
            $table->string('year');
            $table->enum('status', ['due', 'paid'])->default('due');
            $table->date('paid_at')->nullable();
            $table->string('pdf')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('owner_id')->references('id')->on('profile_vehicle_owners')->onDelete('restrict');
            $table->unique(array('year', 'month','owner_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('owner_invoices');
    }
}
