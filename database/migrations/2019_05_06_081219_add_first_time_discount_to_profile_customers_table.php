<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstTimeDiscountToProfileCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->decimal('first_booking_discount',13,2)->after('profile_complete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->dropColumn('first_booking_discount');
        });
    }
}
