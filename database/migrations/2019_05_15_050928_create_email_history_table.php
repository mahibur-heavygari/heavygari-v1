<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('email_text')->nullable();  
            $table->longText('email')->nullable();  
            $table->longText('response')->nullable();  
            $table->string('other_email')->nullable();
            $table->enum('single_bulk', ['single', 'bulk'])->default('single'); 
            $table->string('user_type')->nullable(); 
            $table->text('email_type')->nullable();
            $table->text('image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_history');
    }
}
