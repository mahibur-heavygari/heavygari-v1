<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedBookingPriceManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_booking_price_manager', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('min_weight')->comment('kg');
            $table->integer('max_weight')->comment('kg');
            $table->decimal('base_fare', 6, 2)->comment('taka');
            $table->decimal('pickup_charge', 6, 2)->comment('taka');
            $table->decimal('drop_off_charge', 6, 2)->comment('taka');
            $table->decimal('weight_rate', 6, 2)->comment('/kg');
            $table->decimal('shortest_trip_rate', 6, 2)->comment('/km');
            $table->decimal('short_trip_rate', 6, 2)->comment('/km');
            $table->decimal('normal_trip_rate', 6, 2)->comment('/km');
            $table->decimal('long_trip_rate', 6, 2)->comment('/km');
            $table->decimal('surcharge_rate', 6, 2)->comment('/km');
            $table->decimal('discount_percent', 6, 2)->comment('%');
            $table->decimal('admin_commission', 6, 2)->comment('%');
            $table->timestamps();
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('shared_booking_price_manager');
    }
}
