<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminIdToProfileVehicleOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_vehicle_owners', function (Blueprint $table) {
            $table->bigInteger('profile_admin_id')->after('user_id')->unsigned()->nullable();
            $table->foreign('profile_admin_id')->references('id')->on('profile_admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_vehicle_owners', function (Blueprint $table) {
            $table->dropForeign('profile_vehicle_owners_profile_admin_id_foreign');
            $table->dropColumn('profile_admin_id');
        });
    }
}
