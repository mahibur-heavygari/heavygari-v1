<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelVehiclesDrivers extends Migration
{

    public function up()
    {
        Schema::create('rel_vehicles_drivers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('vehicle_id')->unsigned();
            $table->bigInteger('driver_profile_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
            $table->foreign('driver_profile_id')->references('id')->on('profile_drivers')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('rel_vehicles_drivers');
    }
}
