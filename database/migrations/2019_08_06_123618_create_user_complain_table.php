<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserComplainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_complain', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('note')->nullable();
            $table->char('name', 100);
            $table->char('user_type', 100);
            $table->char('phone', 100);
            $table->char('android',100)->nullable();
            $table->char('ios',100)->nullable();
            $table->char('web',100)->nullable();
            $table->char('others',100)->nullable();
            $table->char('network',100)->nullable();
            $table->enum('status', ['ongoing', 'resolved'])->default('ongoing');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_complain');
    }
}
