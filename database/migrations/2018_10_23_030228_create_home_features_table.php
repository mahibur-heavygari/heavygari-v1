<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->text('text_en');
            $table->string('title_bn');
            $table->text('text_bn');
            $table->string('icon');
            $table->boolean('comming_soon')->default(false)->comment('boolean');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_features');
    }
}
