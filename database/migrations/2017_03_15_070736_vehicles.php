<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vehicles extends Migration
{

    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref_no')->nullable();
            $table->bigInteger('vehicle_owner_profile_id')->unsigned();
            $table->bigInteger('current_driver_profile_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('about')->nullable();
            $table->string('photo')->nullable();
            $table->string('thumb_photo')->nullable();
            $table->smallInteger('vehicle_type_id')->unsigned();
            $table->string('vehicle_registration_number')->nullable();
            $table->string('fitness_number')->nullable();
            $table->string('fitness_expiry')->nullable();
            $table->string('number_plate')->nullable();
            $table->string('tax_token_number')->nullable();
            $table->string('tax_token_expirey')->nullable();
            $table->string('chesis_no')->nullable();
            $table->string('engine_capacity_cc')->nullable();
            $table->string('ride_sharing_certificate_no')->nullable();
            $table->string('insurance_no')->nullable();
            $table->enum('status', ['available', 'booked', 'on-trip', 'off-duty'])->default('off-duty');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('vehicle_owner_profile_id')->references('id')->on('profile_vehicle_owners')->onDelete('cascade');
            $table->foreign('current_driver_profile_id')->references('id')->on('profile_drivers')->onDelete('set null');
            $table->foreign('vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('vehicles');
    }
}
