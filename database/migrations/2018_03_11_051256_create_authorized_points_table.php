<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorizedPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorized_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('base_point_id')->unsigned();
            $table->string('arp_no');
            $table->string('owner_name');
            $table->string('shop_name');
            $table->string('nature_of_business');
            $table->string('address');
            $table->string('phone');
            $table->timestamps();
            $table->softDeletes();
           
            $table->foreign('base_point_id')->references('id')->on('base_points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authorized_points');
    }
}
