<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_commissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('driver_profile_id')->unsigned()->nullable();
            $table->bigInteger('owner_id')->unsigned();
            $table->bigInteger('owner_invoice_id')->unsigned();
            $table->decimal('total_amount',13,2);     
            $table->string('month')->nullable();
            $table->string('year')->nullable();     
            $table->date('generate_date')->nullable();
            $table->date('due_date')->nullable();
            $table->date('paid_at')->nullable();
            $table->enum('status', ['due', 'unpaid', 'paid'])->default('due');
            $table->text('note')->nullable(); 
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('driver_profile_id')->references('id')->on('profile_drivers')->onDelete('restrict');
            $table->foreign('owner_id')->references('id')->on('profile_vehicle_owners')->onDelete('restrict');
            $table->foreign('owner_invoice_id')->references('id')->on('owner_invoices')->onDelete('restrict');
            $table->unique(array('year', 'month', 'driver_profile_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_commission');
    }
}
