<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManagerInfoColumnsToProfileVehicleOwners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_vehicle_owners', function (Blueprint $table) {
            $table->string('manager_name')->after('without_app')->nullable(); 
            $table->string('manager_phone')->after('manager_name')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_vehicle_owners', function (Blueprint $table) {
            $table->dropColumn('manager_name');
            $table->dropColumn('manager_phone');
        });
    }
}
