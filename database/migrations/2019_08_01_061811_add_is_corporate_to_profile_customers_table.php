<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCorporateToProfileCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->bigInteger('corporate_customer_id')->after('popup_no')->unsigned()->nullable();
            $table->foreign('corporate_customer_id')->references('id')->on('corporate_customers')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->dropForeign('profile_customers_corporate_customer_id_foreign');
            $table->dropColumn('corporate_customer_id');   
        });
    }
}
