<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPopupNoColumnToProfileCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
            $table->bigInteger('popup_no')->nullable()->after('web_app');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_customers', function (Blueprint $table) {
           $table->dropColumn('popup_no');
        });
    }
}
