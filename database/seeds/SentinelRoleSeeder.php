<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Super Admin',
            'slug' => 'super-admin',
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Customer',
            'slug' => 'customer',
        ]); 

        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Vehicle Owner',
            'slug' => 'owner',
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Driver',
            'slug' => 'driver',
        ]);               

        $this->command->info('Roles seeded!');
    }
}
