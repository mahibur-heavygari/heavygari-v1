<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BaseWeightCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('base_weight_category')->insert([
            [
                'title' => 'Below 1 Kg',
                'weight' => '1'
            ],
            [
                'title' => '1 to 5 kg',
                'weight' => '5'
            ],
            [
                'title' => '5 to 10 kg',
                'weight' => '10'
            ]
        ]);

        $this->command->info('Base Points seeded!');
    }
}
