<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BasePriceManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('full_booking_price_manager')->insert([
            [
                'vehicle_type_id' => 1,
                'base_fare' => 2000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 2,
                'base_fare' => 2500.00,
                'short_trip_rate' => 500.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 50.00,
                'long_down_trip_rate' => 46.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 3,
                'base_fare' => 1000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 48.00,
                'down_trip_rate' => 44.00,
                'long_up_trip_rate' => 58.00,
                'long_down_trip_rate' => 54.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 4,
                'base_fare' => 700.00,
                'short_trip_rate' => 200.00,
                'up_trip_rate' => 33.00,
                'down_trip_rate' => 29.00,
                'long_up_trip_rate' => 43.00,
                'long_down_trip_rate' => 38.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 5,
                'base_fare' => 2500.00,
                'short_trip_rate' => 500.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 6,
                'base_fare' => 3000.00,
                'short_trip_rate' => 600.00,
                'up_trip_rate' => 80.00,
                'down_trip_rate' => 78.00,
                'long_up_trip_rate' => 90.00,
                'long_down_trip_rate' => 88.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],        
            [   
                'vehicle_type_id' => 7,
                'base_fare' => 2500.00,
                'short_trip_rate' => 500.00,
                'up_trip_rate' => 70.00,
                'down_trip_rate' => 68.00,
                'long_up_trip_rate' => 80.00,
                'long_down_trip_rate' => 78.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 8,
                'base_fare' => 1000.00,
                'short_trip_rate' => 150.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 9,
                'base_fare' => 1000.00,
                'short_trip_rate' => 300.00,
                'up_trip_rate' => 40.00,
                'down_trip_rate' => 36.00,
                'long_up_trip_rate' => 50.00,
                'long_down_trip_rate' => 46.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 10,
                'base_fare' => 3000.00,
                'short_trip_rate' => 2000.00,
                'up_trip_rate' => 2000.00,
                'down_trip_rate' => 1998.00,
                'long_up_trip_rate' => 2000.00,
                'long_down_trip_rate' => 1998.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 11,
                'base_fare' => 3000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 48.00,
                'down_trip_rate' => 44.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 12,
                'base_fare' => 2000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 40.00,
                'down_trip_rate' => 36.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 13,
                'base_fare' => 4000.00,
                'short_trip_rate' => 700.00,
                'up_trip_rate' => 100.00,
                'down_trip_rate' => 93.00,
                'long_up_trip_rate' => 120.00,
                'long_down_trip_rate' => 113.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 14,
                'base_fare' => 2500.00,
                'short_trip_rate' => 1000.00,
                'up_trip_rate' => 80.00,
                'down_trip_rate' => 76.00,
                'long_up_trip_rate' => 85.00,
                'long_down_trip_rate' => 80.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 15,
                'base_fare' => 4000.00,
                'short_trip_rate' => 1000.00,
                'up_trip_rate' => 82.00,
                'down_trip_rate' => 78.00,
                'long_up_trip_rate' => 92.00,
                'long_down_trip_rate' => 88.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 16,
                'base_fare' => 4000.00,
                'short_trip_rate' => 700.00,
                'up_trip_rate' => 40.00,
                'down_trip_rate' => 36.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 17,
                'base_fare' => 5000.00,
                'short_trip_rate' => 800.00,
                'up_trip_rate' => 50.00,
                'down_trip_rate' => 46.00,
                'long_up_trip_rate' => 55.00,
                'long_down_trip_rate' => 50.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 18,
                'base_fare' => 50.00,
                'short_trip_rate' => 24.00,
                'up_trip_rate' => 18.00,
                'down_trip_rate' => 14.00,
                'long_up_trip_rate' => 15.00,
                'long_down_trip_rate' => 13.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
        ]);

        $this->command->info('Price Manager seeded!');
    }
}
