<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //temporarily disable the mass assignment protection
        Model::unguard();

        $this->call(BasePointsSeeder::class);

        $this->call(BaseProductCategoriesSeeder::class);

        $this->call(BaseWeightCategorySeeder::class);

        $this->call(BaseVolumetricCategorySeeder::class);

        $this->call(BaseVehicleCapacitiesSeeder::class);

        $this->call(BaseVehicleTypesSeeder::class);

        $this->call(BasePriceManagerSeeder::class);

        $this->call(SentinelRoleSeeder::class);

        $this->call(SentinelAdminSeeder::class);

        $this->call(SettingsSeeder::class);

        $this->command->info('All seeder seeded!');

        Model::reguard();
    }
}
