<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private function GetInsertArray($key, $value)
    {
        $insertArray = [
            'key'   =>  $key,
            'value'     => $value,
            'created_at'   => date('Y-m-d H:i:s'),
            'updated_at'   => date('Y-m-d H:i:s')
        ];
        return $insertArray;
    }

    private function seedDriverComission()
    {
        $setting = DB::table('settings')->where('key', 'driver_comission')->first();
        if (is_null($setting)) {
            $key = 'driver_comission';
            $value = '0.50';
            $insertArr = $this->GetInsertArray($key, $value);
            DB::table('settings')->insert($insertArr);
        }
    }

    public function run()
    {        
        $this->seedDriverComission();
    }
}
