<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BaseProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('base_product_categories')->insert([
            [
                'title' => 'Electronics',
            ],
            [
                'title' => 'Furnitures',
            ],
			    [
                'title' => 'raw material',
            ],
			[
                'title' => 'spare parts',
            ],
            [
                'title' => 'Vehicles',
            ],
			    [
                'title' => 'Commodity',
            ],
			[
                'title' => 'Liquids',
            ],
            [
                'title' => 'Chemicals',
            ],
			    [
                'title' => 'Packaging material',
            ],
			[
                'title' => 'Document',
            ],
            [
                'title' => 'Garments',
            ],
			    [
                'title' => 'Fragile Item',
            ],
			[
                'title' => 'Food',
            ],
            [
                'title' => 'Cosmetics',
            ],
			    [
                'title' => 'Leather Goods',
            ],
			[
                'title' => 'Accessories',
            ],
            [
                'title' => 'Hardware',
            ]		
        ]);

        $this->command->info('Base Points seeded!');
    }
}
