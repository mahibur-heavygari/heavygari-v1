<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BaseVehicleCapacitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('base_vehicle_capacity_types')->insert([
            [
                'id' => 1,
                'title' => 'Kg',
            ],
            [
                'id' => 2,
                'title' => 'Ltr',
            ],
            [
                'id' => 3,
                'title' => 'Hours',
            ],
            [
                'id' => 4,
                'title' => 'Person',
            ],
            [
                'id' => 5,
                'title' => 'Size',
            ]
        ]);

        $this->command->info('Base Vehicle Capacities seeded!');
    }
}
