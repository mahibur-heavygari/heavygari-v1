<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BasePointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('base_points')->insert([
            [
                'id' => 1,
                'title' => 'Dhaka',
                'fullname' => 'Dhaka',
                'lat' => 23.71152530,
                'lon' => 90.41114510
            ],
            [
                'id' => 2,
                'title' => 'Chittagong',
                'fullname' => 'Chittagong',
                'lat' => 22.33510900,
                'lon' => 91.83407300
            ],
            [
                'id' => 3,
                'title' => 'Bagerhat',
                'fullname' => 'Bagerhat',
                'lat' => 22.65156800,
                'lon' => 89.78593800
            ],
            [
                'id' => 4,
                'title' => 'Bandarban',
                'fullname' => 'Bandarban',
                'lat' => 22.19532750,
                'lon' => 92.21837730
            ],
            [
                'id' => 5,
                'title' => 'Barguna',
                'fullname' => 'Barguna',
                'lat' => 22.09529200,
                'lon' => 90.11207000
            ],
            [
                'id' => 6,
                'title' => 'Barisal',
                'fullname' => 'Barisal',
                'lat' => 22.38111300,
                'lon' => 90.33718900
            ],
            [
                'id' => 7,
                'title' => 'Bhola',
                'fullname' => 'Bhola',
                'lat' => 22.68592300,
                'lon' => 90.64817900
            ],
            [
                'id' => 8,
                'title' => 'Bogra',
                'fullname' => 'Bogra',
                'lat' => 24.84652280,
                'lon' => 89.37775500
            ],
            [
                'id' => 9,
                'title' => 'Brahmanbaria',
                'fullname' => 'Brahmanbaria',
                'lat' => 23.95709040,
                'lon' => 91.11192860
            ],
            [
                'id' => 10,
                'title' => 'Burimari',
                'fullname' => 'Burimari',
                'lat' => 26.39617200,
                'lon' => 88.93472700
            ],
            [
                'id' => 11,
                'title' => 'Chandpur',
                'fullname' => 'Chandpur',
                'lat' => 23.23325850,
                'lon' => 90.67129120
            ],
            [
                'id' => 12,
                'title' => 'Chapai Nawabgan',
                'fullname' => 'Chapai Nawabgan',
                'lat' => 24.59650340,
                'lon' => 88.27751220
            ],
            [
                'id' => 13,
                'title' => 'Chuadanga',
                'fullname' => 'Chuadanga',
                'lat' => 23.64019610,
                'lon' => 88.84184100
            ],
            [
                'id' => 14,
                'title' => 'Comilla',
                'fullname' => 'Comilla',
                'lat' => 23.46827470,
                'lon' => 91.17881350
            ],
            [
                'id' => 15,
                'title' => 'Cox\'s Bazar',
                'fullname' => 'Cox\'s Bazar',
                'lat' => 21.43946360,
                'lon' => 92.00773160
            ],
            [
                'id' => 16,
                'title' => 'Dinajpur',
                'fullname' => 'Dinajpur',
                'lat' => 25.62170610,
                'lon' => 88.63545040
            ],
            [
                'id' => 17,
                'title' => 'Faridpur',
                'fullname' => 'Faridpur',
                'lat' => 23.60708220,
                'lon' => 89.84294060
            ],

            [
                'id' => 18,
                'title' => 'Feni',
                'fullname' => 'Feni',
                'lat' => 23.02323100,
                'lon' => 91.38408440
            ],
            [
                'id' => 19,
                'title' => 'Gaibandha',
                'fullname' => 'Gaibandha',
                'lat' => 25.32875100,
                'lon' => 89.52808800
            ],
            [
                'id' => 20,
                'title' => 'Gazipur',
                'fullname' => 'Gazipur',
                'lat' => 24.00228580,
                'lon' => 90.42642830
            ],
            [
                'id' => 21,
                'title' => 'Gopalganj',
                'fullname' => 'Gopalganj',
                'lat' => 23.00508570,
                'lon' => 89.82660590
            ],
            [
                'id' => 22,
                'title' => 'Habiganj',
                'fullname' => 'Habiganj',
                'lat' => 24.37494500,
                'lon' => 91.41553000
            ],
            [
                'id' => 23,
                'title' => 'Hilli',
                'fullname' => 'Hilli',
                'lat' => 25.27761300,
                'lon' => 89.00676700
            ],
            [
                'id' => 24,
                'title' => 'Jamalpur',
                'fullname' => 'Jamalpur',
                'lat' => 24.93753300,
                'lon' => 89.93777500
            ],
            [
                'id' => 25,
                'title' => 'Jessore',
                'fullname' => 'Jessore',
                'lat' => 23.16643000,
                'lon' => 89.20811260
            ],
            [
                'id' => 26,
                'title' => 'Jhalokati',
                'fullname' => 'Jhalokati',
                'lat' => 22.57208000,
                'lon' => 90.18696400
            ],
            [
                'id' => 27,
                'title' => 'Jhenaidah',
                'fullname' => 'Jhenaidah',
                'lat' => 23.54481760,
                'lon' => 89.15392130
            ],
            [
                'id' => 28,
                'title' => 'Joypurhat',
                'fullname' => 'Joypurhat',
                'lat' => 25.09473500,
                'lon' => 89.09449400
            ],
            [
                'id' => 29,
                'title' => 'Khagrachari',
                'fullname' => 'Khagrachari',
                'lat' => 23.11928500,
                'lon' => 91.98466300
            ],
            [
                'id' => 30,
                'title' => 'Khulna',
                'fullname' => 'Khulna',
                'lat' => 22.81577400,
                'lon' => 89.56867900
            ],
            [
                'id' => 31,
                'title' => 'Kishoreganj',
                'fullname' => 'Kishoreganj',
                'lat' => 24.44493700,
                'lon' => 90.77657500
            ],
            [
                'id' => 32,
                'title' => 'Kurigram',
                'fullname' => 'Kurigram',
                'lat' => 25.80544500,
                'lon' => 89.63617400
            ],
            [
                'id' => 33,
                'title' => 'Kushtia',
                'fullname' => 'Kushtia',
                'lat' => 23.90125800,
                'lon' => 89.12048200
            ],
            [
                'id' => 34,
                'title' => 'Lakshmipur',
                'fullname' => 'Lakshmipur',
                'lat' => 22.94247700,
                'lon' => 90.84118400
            ],
            [
                'id' => 35,
                'title' => 'Lalmonirhat',
                'fullname' => 'Lalmonirhat',
                'lat' => 25.99234000,
                'lon' => 89.28472500
            ],
            [
                'id' => 36,
                'title' => 'Madaripur',
                'fullname' => 'Madaripur',
                'lat' => 23.16410200,
                'lon' => 90.18968050
            ],
            [
                'id' => 37,
                'title' => 'Magura',
                'fullname' => 'Magura',
                'lat' => 23.48733700,
                'lon' => 23.48733700
            ],
            [
                'id' => 38,
                'title' => 'Manikganj',
                'fullname' => 'Manikganj',
                'lat' => 23.86643700,
                'lon' => 90.00596100
            ],
            [
                'id' => 39,
                'title' => 'Meherpur',
                'fullname' => 'Meherpur',
                'lat' => 23.76221300,
                'lon' => 88.63182100
            ],
            [
                'id' => 40,
                'title' => 'Mongla',
                'fullname' => 'Mongla',
                'lat' => 22.49422000,
                'lon' => 89.60161700
            ],
            [
                'id' => 41,
                'title' => 'Moulvibazar',
                'fullname' => 'Moulvibazar',
                'lat' => 24.48293400,
                'lon' => 91.77741700
            ],
            [
                'id' => 42,
                'title' => 'Munshiganj',
                'fullname' => 'Munshiganj',
                'lat' => 23.54136400,
                'lon' => 90.52052200
            ],
            [
                'id' => 43,
                'title' => 'Mymensingh',
                'fullname' => 'Mymensingh',
                'lat' => 24.75388900,
                'lon' => 90.40305600
            ],
            [
                'id' => 44,
                'title' => 'Naogaon',
                'fullname' => 'Naogaon',
                'lat' => 24.91316000,
                'lon' => 88.75309500
            ],
            [
                'id' => 45,
                'title' => 'Narail',
                'fullname' => 'Narail',
                'lat' => 23.17253400,
                'lon' => 89.51267200
            ],
            [
                'id' => 46,
                'title' => 'Narayanganj',
                'fullname' => 'Narayanganj',
                'lat' => 23.63366000,
                'lon' => 90.49648200
            ],
            [
                'id' => 47,
                'title' => 'Narshingdi',
                'fullname' => 'Narshingdi',
                'lat' => 23.93223300,
                'lon' => 90.71541000
            ],
            [
                'id' => 48,
                'title' => 'Natore',
                'fullname' => 'Natore',
                'lat' => 24.42055600,
                'lon' => 89.00028200
            ],
            [
                'id' => 49,
                'title' => 'Netrokona',
                'fullname' => 'Netrokona',
                'lat' => 24.87095500,
                'lon' => 24.87095500
            ],
            [
                'id' => 50,
                'title' => 'Nilphamari',
                'fullname' => 'Nilphamari',
                'lat' => 25.93179400,
                'lon' => 88.85600600
            ],
            [
                'id' => 51,
                'title' => 'Noakhali',
                'fullname' => 'Noakhali',
                'lat' => 22.86956300,
                'lon' => 91.09939800
            ],      
            [
                'id' => 52,
                'title' => 'Pabna',
                'fullname' => 'Pabna',
                'lat' => 23.99852400,
                'lon' => 89.23364500
            ],
            [
                'id' => 53,
                'title' => 'Panchagarh',
                'fullname' => 'Panchagarh',
                'lat' => 26.34110000,
                'lon' => 88.55416060
            ],
            [
                'id' => 54,
                'title' => 'Patgram',
                'fullname' => 'Patgram',
                'lat' => 26.35695100,
                'lon' => 89.00837300
            ],
            [
                'id' => 55,
                'title' => 'Patuakhali',
                'fullname' => 'Patuakhali',
                'lat' => 22.35963160,
                'lon' => 90.32987120
            ],
            [
                'id' => 56,
                'title' => 'Pirojpur',
                'fullname' => 'Pirojpur',
                'lat' => 22.57907400,
                'lon' => 89.97592600
            ],
            [
                'id' => 57,
                'title' => 'Rajbari',
                'fullname' => 'Rajbari',
                'lat' => 23.75743050,
                'lon' => 89.64446650
            ],            
            [
                'id' => 58,
                'title' => 'Rajshahi',
                'fullname' => 'Rajshahi',
                'lat' => 24.71057800,
                'lon' => 88.94138700
            ], 
            [
                'id' => 59,
                'title' => 'Rangamati',
                'fullname' => 'Rangamati',
                'lat' => 22.73241700,
                'lon' => 92.29851300
            ], 
            [
                'id' => 60,
                'title' => 'Rangpur',
                'fullname' => 'Rangpur',
                'lat' => 25.75580960,
                'lon' => 89.24446200
            ],
            [
                'id' => 61,
                'title' => 'Saidpur',
                'fullname' => 'Saidpur',
                'lat' => 25.78295420,
                'lon' => 88.89826660
            ],
            [
                'id' => 62,
                'title' => 'Satkhira',
                'fullname' => 'Satkhira',
                'lat' => 22.31548100,
                'lon' => 89.11145300
            ],
            [
                'id' => 63,
                'title' => 'Shariatpur',
                'fullname' => 'Shariatpur',
                'lat' => 23.24232100,
                'lon' => 90.43477100
            ],
            [
                'id' => 64,
                'title' => 'Sherpur',
                'fullname' => 'Sherpur',
                'lat' => 25.02049330,
                'lon' => 90.01529660
            ],
            [
                'id' => 65,
                'title' => 'Sirajganj',
                'fullname' => 'Sirajganj',
                'lat' => 24.45339780,
                'lon' => 89.70068150
            ],
            [
                'id' => 66,
                'title' => 'Sonamasjid',
                'fullname' => 'Sonamasjid',
                'lat' => 24.81389800,
                'lon' => 88.14328700
            ],            
            [
                'id' => 67,
                'title' => 'Sunamganj',
                'fullname' => 'Sunamganj',
                'lat' => 25.06580420,
                'lon' => 91.39501150
            ],
            [
                'id' => 68,
                'title' => 'Sylhet',
                'fullname' => 'Sylhet',
                'lat' => 24.88979560,
                'lon' => 91.86978940
            ],
            [
                'id' => 69,
                'title' => 'Tamabil',
                'fullname' => 'Tamabil',
                'lat' => 25.17200700,
                'lon' => 92.09042500
            ],  
            [
                'id' => 70,
                'title' => 'Tangail',
                'fullname' => 'Tangail',
                'lat' => 24.26004100,
                'lon' => 89.91711400
            ],
            [
                'id' => 71,
                'title' => 'Thakurgaon',
                'fullname' => 'Thakurgaon',
                'lat' => 26.03369450,
                'lon' => 88.46168340
            ]
        ]);

        $this->command->info('Base Points seeded!');
    }
}
