 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

</style>

@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">সম্পূর্ণ গাড়ি বুকিং  করুন</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card">
                <form class="" method="POST" enctype='multipart/form-data' action="/customer/panel/bookings/full/get_fare">
                    {{ csrf_field() }}
                    <div class="form-group @if($errors->first('vehicle_type')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-ve-type">গাড়ীর ধরণ</span>
                            <select id="vehicle_type" name="vehicle_type" class="custom-select" aria-describedby="addon-ve-type">
                                <option value=''>--অনুগ্রহ পূর্বক নির্বাচন করুন--</option>
                                @foreach($vehicle_types as $vehicle_type)
                                    <option @if(old('vehicle_type')==$vehicle_type->id) selected @endif value="{{$vehicle_type->id}}">{{$vehicle_type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('vehicle_type')}}</div>
                    </div>
                    <div class="form-group @if($errors->first('capacity')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-capacity">ধারণক্ষমতা</span>
                            <input id="capacity" name="capacity" type="text" class="form-control text-right" value="{{old('capacity')}}" aria-describedby="addon-capacity" readonly="" style="background-color:white;">
                            <span class="input-group-addon" id="capacity_label">{{old('capacity_label')}}</span>                       
                        </div>
                        <div class="form-control-feedback">{{$errors->first('capacity')}}</div>
                    </div>
                    <div class="form-group field-join @if($errors->first('from_address')!=null || $errors->first('from_lat')!=null || $errors->first('from_lon')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-from">পিকাপের শহর</span>
                            <select id="from_point" name="from_point" class="custom-select" aria-describedby="addon-from">
                                @foreach($points as $point)
                                    <option @if(old('from_point')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                                @endforeach
                            </select>
                            <input name="from_lat" id="from_lat" type="hidden" value="{{old('from_lat')}}">
                            <input name="from_lon" id="from_lon" type="hidden" value="{{old('from_lon')}}">
                        </div>
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-from-address">ঠিকানা</span>
                            <input id="from_address" name="from_address" type="text" class="form-control map_field" value="{{old('from_address')}}" aria-describedby="addon-from-address">
                        </div>                        
                        <div class="form-control-feedback">{{$errors->first('from_address')}}</div>
                        <div class="form-control-feedback">{{$errors->first('from_lat')}}</div>
                        <div class="form-control-feedback">{{$errors->first('from_lon')}}</div>
                    </div>
                    <div class="form-group field-join @if($errors->first('to_address')!=null || $errors->first('to_lat')!=null || $errors->first('to_lon')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-to">গন্তব্যের শহর</span>
                            <select id="to_point" name="to_point" class="custom-select" aria-describedby="addon-to">
                                @foreach($points as $point)
                                    <option @if(old('to_point')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-to-address">ঠিকানা</span>
                            <input id="to_address" name="to_address" type="text" class="form-control map_field" value="{{old('to_address')}}" aria-describedby="addon-to-address">
                            <input name="to_lat" id="to_lat" type="hidden" value="{{old('to_lat')}}">
                            <input name="to_lon" id="to_lon" type="hidden" value="{{old('to_lon')}}">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('to_address')}}</div>
                        <div class="form-control-feedback">{{$errors->first('to_lat')}}</div>
                        <div class="form-control-feedback">{{$errors->first('to_lon')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-booking-type">বুকিং-এর সময়</span>
                            <select id="booking_type" name="booking_type" class="custom-select" aria-describedby="addon-booking-type">
                                <option @if(old('booking_type')=='on-demand') selected @endif value="on-demand">এখনি</option>
                                <option @if(old('booking_type')=='advance') selected @endif value="advance">আগাম বুকিং</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group booking-date-time @if($errors->first('date_time')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-date-time">তারিখ ও সময় </span>
                            <input id="date_time" name="date_time" type="text" class="form-control icon-field-datetime datetimepick-only" value="{{old('date_time')}}" aria-describedby="addon-date-time">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('date_time')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-booking-type">যাত্রার ধরণ</span>
                            <select class="custom-select @if($errors->first('trip_type')!=null) is-invalid @endif" id="trip_type" name="trip_type">                    
                               <option @if(old('trip_type')=='single') selected @endif value="single">একমুখী</option>
                               <option @if(old('trip_type')=='round') selected @endif value="round">যাওয়া আসা</option>
                            </select>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary book-submit btn-block">সাবমিট</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div id="map"></div>
            <div id="infowindow-content">
              <img src="" width="16" height="16" id="place-icon">
              <span id="place-name"  class="title"></span><br>
              <span id="place-address"></span>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script type = "text/javascript"> 
$(document).ready(function() {
   
    $(function() {
        $('.booking-date-time').hide();
        $('#booking_type').change(function() {
            if ($('#booking_type').val() == 'advance') {
                $('.booking-date-time').show();
            } else {
                $('.booking-date-time').hide();
            }
        });
    });

    $(function() {

        var type_capacity = {!! json_encode($capacity_types, JSON_PRETTY_PRINT) !!};
        //console.log(type_capacity);

        $('#vehicle_type').change(function() {
            var selected = $(this).find("option:selected").text();
            console.log(type_capacity[selected]);
            $("#capacity_label").text(type_capacity[selected]['title'])
            $("#capacity").val(type_capacity[selected]['capacity'])
        });
    });
});

// map
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 23.777176,
            lng: 90.399452
        },
        zoom: 7
    });
    // place autocomplete feature on map fields
    /*
    var input = document.getElementsByClassName('map_field');
    for (i = 0; i < input.length; i++) {
        autocomplete = new google.maps.places.Autocomplete(input[i]);
        autocomplete.setComponentRestrictions({'country': 'BD'});

        showPlace(autocomplete);
    }
    */
    var fromInput = document.getElementById('from_address');
    var toInput = document.getElementById('to_address');

    from_autocomplete = new google.maps.places.Autocomplete(fromInput);
    from_autocomplete.setComponentRestrictions({
        'country': 'BD'
    });
    showPlace(from_autocomplete, 'from');

    to_autocomplete = new google.maps.places.Autocomplete(toInput);
    to_autocomplete.setComponentRestrictions({
        'country': 'BD'
    });
    showPlace(to_autocomplete, 'to');

    // Show the place user entered
    function showPlace(location, type) {

        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        location.addListener('place_changed', function() {
            var place = location.getPlace();
            if (!place.geometry) {
                // Place was not found
                window.alert("No details available for input: '" + place.name + "'");
                prepareLocationData(false, type);
                marker.setVisible(false);
                return;
            } else {
                prepareLocationData(place, type);

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
            }
        });
    }

    // put coordinators to lat, lon fields
    // from_lat/from_lon, to_lat/to_lat
    function prepareLocationData(place, type) {

        if (place==false) {
            document.getElementById(type + '_lat').value = '';
            document.getElementById(type + '_lon').value = '';
        } else {
             // DEBUGING PURPOSE
            console.log('Type :' + type);
            console.log('address components :');
            console.log(place.address_components);
            console.log('lat, lon :');
            console.log(place.geometry.location.lat(), place.geometry.location.lng());

            document.getElementById(type + '_lat').value = place.geometry.location.lat();
            document.getElementById(type + '_lon').value = place.geometry.location.lng();
        }        
    }
} 
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{config('heavygari.google_maps.api_key')}}&libraries=places&callback=initMap" async defer></script>
@stop