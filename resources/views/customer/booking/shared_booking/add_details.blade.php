@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">বুকিং নিশ্চিত করুন</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card">
                <form method="POST" action="/customer/panel/bookings/parcel/confirm" onsubmit="return validateForm()">
                    
                    {{ csrf_field() }}

                    <ul class="confrim-list">

                        <li>
                            <span>পণ্য এর ক্যাটাগরি:</span>
                            <span>
                                {{$booking_fields['product_category']['title']}}
                            </span>
                            <input name="product_category_id" type="hidden" value="{{$booking_fields['product_category_id']}}">
                        </li>

                        <li>
                            <span>পণ্য সংখ্যা:</span>
                            <span>
                                {{$booking_fields['quantity']}}
                            </span>
                            <input name="quantity" type="hidden" value="{{$booking_fields['quantity']}}">
                        </li>

                        <li>
                            <span>ওজন এর ধরন:</span>
                            <span>
                                {{$booking_fields['weight_category']['title']}}
                            </span>
                            <input name="weight_category_id" type="hidden" value="{{$booking_fields['weight_category_id']}}">

                            <input name="weight" type="hidden" value="{{$booking_fields['weight']}}">                            
                        </li>

                        <li>
                            <span>আয়তনের ধরন:</span>
                            <span>
                                {{$booking_fields['volumetric_category']['title']}}
                            </span>
                            <input name="volumetric_category_id" type="hidden" value="{{$booking_fields['volumetric_category_id']}}">

                            <input name="volumetric_width" type="hidden" value="{{$booking_fields['volumetric_width']}}">

                            <input name="volumetric_height" type="hidden" value="{{$booking_fields['volumetric_height']}}">

                            <input name="volumetric_length" type="hidden" value="{{$booking_fields['volumetric_length']}}">
                        </li>
                    
                        <li>
                            <span>পিক-আপের ঠিকানা:</span>
                            <span>
                                {{$booking_fields['from_address']}}
                            </span>
                            <input name="from_point" type="hidden" value="{{$booking_fields['from_point']}}">
                            <input name="from_address" type="hidden" value="{{$booking_fields['from_address']}}">     
                            <input name="from_lat" type="hidden" value="{{$booking_fields['from_lat']}}">
                            <input name="from_lon" type="hidden" value="{{$booking_fields['from_lon']}}">                       
                        </li>
                        
                        <li>
                            <span>গন্তব্যের ঠিকানা:</span>
                            <span>
                                {{$booking_fields['to_address']}}
                            </span>
                            <input name="to_point" type="hidden" value="{{$booking_fields['to_point']}}">
                            <input name="to_address" type="hidden" value="{{$booking_fields['to_address']}}">
                            <input name="to_lat" type="hidden" value="{{$booking_fields['to_lat']}}">
                            <input name="to_lon" type="hidden" value="{{$booking_fields['to_lon']}}">
                        </li>
                        
                        <li>
                            <span>দূরত্ব:</span>
                            <span>
                                {{$route['distance']}} km
                            </span>
                        </li>

                        <li>
                            <span>বুকিং এর সময়:</span>
                            <span>{{$booking_fields['booking_type']}}</span>
                            <input name="booking_type" type="hidden" value="{{$booking_fields['booking_type']}}">
                        </li>
                        
                        @if($booking_fields['booking_type']=='advance')
                        <li>
                            <span>তারিখ ও সময়:</span>
                            <span>{{$booking_fields['date_time']}}</span>
                            <input name="date_time" type="hidden" value="{{$booking_fields['date_time']}}">
                        </li>
                        @endif

                    </ul>

                    <hr />

                    <h2 class="your-cost text-center">আপনার খরচ :  {{$fare_breakdown['total_cost']}} টাকা</h2>

                    <h5 class="bfc-title">পারিশ্রমিকের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('[payment_by]')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">পারিশ্রমিকদাতা</span>
                            <select id="payment_by" name="payment_by" class="custom-select" aria-describedby="addon-booking-type">                                
                                <option @if(old('payment_by')=='recipient') selected @endif value="recipient">প্রাপক</option>
                                <option @if(old('payment_by')=='customer') selected @endif value="customer">নিজে</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('[payment_by]')}}</div>
                    </div>                    

                    <h5 class="bfc-title mt-5">প্রাপকের বিবরণ</h5>
                    <div class="form-group">
                        <input id="self_recipient" type="checkbox"> নিজে?
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('recipient_name')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">নাম*</span>
                            <input id="recipient_name" name="recipient_name" type="text" class="form-control" aria-describedby="addon-name">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('recipient_name')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('recipient_phone')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-phone">ফোন নং*</span>
                            <input id="recipient_phone" name="recipient_phone" type="text" class="form-control" aria-describedby="addon-phone">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('recipient_phone')}}</div>
                    </div>

                    <h5 class="bfc-title mt-5">পণ্যের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('particular_details')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-description">বিবরণ</span>
                            <textarea id="particular_details" name="particular_details" rows="3" class="form-control" aria-describedby="addon-description"></textarea>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('particular_details')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('waiting_time')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-goods-weight">অপেক্ষার সময় (প্রায়)</span>
                            <input id="waiting_time" name="waiting_time" type="text" class="form-control" aria-describedby="addon-goods-weight">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('waiting_time')}}</div>
                    </div>                   
                    
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <button type="submit" class="btn btn-outline-secondary book-submit btn-block">বুক করুন</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div class="map-block map-booking">
                <iframe src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking_fields['from_lat']}},{{$booking_fields['from_lon']}}&destination={{$booking_fields['to_lat']}},{{$booking_fields['to_lon']}}" width="800" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<div class="map-block-overlay"></div>-->
            </div>
        </div>
    </div>
    
    <!--<ul class="confrim-list">
        <pre /> {{ print_r($fare_breakdown) }} </pre>
    </ul>-->
    
</div>
@stop

@section('footer')
<script type = "text/javascript" > 
$(document).ready(function() {

    $(function() {
        var customer = {!! json_encode($customer, JSON_PRETTY_PRINT) !!};
        
        $('#self_recipient').on('click', function() {
            $("#recipient_name").val(customer.name);
            $("#recipient_phone").val(customer.phone);
        });
    });

    function validateForm() {
        var recipient_name = document.getElementById("recipient_name").value;
        var recipient_phone = document.getElementById("recipient_phone").value;
        if (recipient_name == "" || recipient_phone == "") {
            alert("Please provide Recipient's Name & Phone");
            return false;
        }
    }
});
</script>
@stop