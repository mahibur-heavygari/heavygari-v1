 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

</style>

@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">পার্সেল ডেলিভারি করুন</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card">
                <form class="" method="POST" enctype='multipart/form-data' action="/customer/panel/bookings/parcel/get_fare">
                    {{ csrf_field() }}
                    <div class="form-group @if($errors->first('product_category_id')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-pr-cat">পণ্য এর ক্যাটাগরি </span>
                            <select id="product_category_id" name="product_category_id" class="custom-select" aria-describedby="addon-pr-cat">
                                <option value=''>--অনুগ্রহ পূর্বক নির্বাচন করুন--</option>
                                @foreach($product_categories as $product_category_id)
                                    <option @if(old('product_category_id')==$product_category_id->id) selected @endif value="{{$product_category_id->id}}">{{$product_category_id->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('product_category_id')}}</div>
                    </div>

                    <div class="form-group @if($errors->first('quantity')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-quantity">পণ্য সংখ্যা</span>
                            <input id="quantity" name="quantity" type="text" class="form-control" value="{{old('quantity')}}" aria-describedby="addon-quantity">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('quantity')}}</div>
                    </div>

                    <div class="form-group @if($errors->first('weight_category_id')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-pr-cat">ওজন এর ধরন </span>
                            <select id="weight_category_id" name="weight_category_id" class="custom-select" aria-describedby="addon-pr-cat">
                                <option value=''>--অনুগ্রহ পূর্বক নির্বাচন করুন--</option>
                                @foreach($weight_categories as $weight_category_id)
                                    <option @if(old('weight_category_id')==$weight_category_id->id) selected @endif value="{{$weight_category_id->id}}">{{$weight_category_id->title}}</option>
                                @endforeach
                                <option @if(old('weight_category_id')=='0') selected @endif value='0'>Custom</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('weight_category_id')}}</div>
                    </div>

                    <div class="form-group @if($errors->first('weight')!=null) has-danger @else custom-weight @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-custom-weight">ওজন</span>
                            <input id="weight" name="weight" type="text" class="form-control" value="{{old('weight')}}" aria-describedby="addon-custom-weight">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('weight')}}</div>
                    </div>

                    <div class="form-group @if($errors->first('volumetric_category_id')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-pr-cat">আয়তনের ধরন</span>
                            <select id="volumetric_category_id" name="volumetric_category_id" class="custom-select" aria-describedby="addon-pr-cat">
                                <option value=''>--অনুগ্রহ পূর্বক নির্বাচন করুন--</option>
                                @foreach($volumetric_categories as $volumetric_category_id)
                                    <option @if(old('volumetric_category_id')==$volumetric_category_id->id) selected @endif value="{{$volumetric_category_id->id}}">{{$volumetric_category_id->title}}</option>
                                @endforeach
                                <option @if(old('volumetric_category_id')=='0') selected @endif value='0'>Custom</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('volumetric_category_id')}}</div>                        
                    </div>

                    <div class="form-group @if($errors->first('volumetric_width')!=null || $errors->first('volumetric_height')!=null || $errors->first('volumetric_length')!=null) has-danger @else custom-volumetric @endif">
                        <div class="input-group input-flex d-times-input">
                            <span class="input-group-addon" id="addon-phone">আয়তন</span>
                            <input type="text" id="volumetric_height" name="volumetric_height" class="form-control" value="{{old('volumetric_height')}}"aria-describedby="addon-phone" placeholder="Height (cm)">
                            <span class="d-times">X</span>
                            <input type="text" id="volumetric_width" name="volumetric_width" class="form-control" value="{{old('volumetric_width')}}" aria-describedby="addon-phone" placeholder="Width (cm)">
                            <span class="d-times">X</span>
                            <input type="text" id="volumetric_length" name="volumetric_length" class="form-control" value="{{old('volumetric_length')}}"aria-describedby="addon-phone" placeholder="Length (cm)">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('volumetric_width')}}</div>
                        <div class="form-control-feedback">{{$errors->first('volumetric_height')}}</div>
                        <div class="form-control-feedback">{{$errors->first('volumetric_length')}}</div>
                    </div>
 
                    <div class="form-group field-join @if($errors->first('from_address')!=null || $errors->first('from_lat')!=null || $errors->first('from_lon')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-from">পিক-আপ</span>
                            <select id="from_point" name="from_point" class="custom-select" aria-describedby="addon-from">
                                @foreach($points as $point)
                                    <option @if(old('from_point')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                                @endforeach
                            </select>
                            <input name="from_lat" id="from_lat" type="hidden" value="{{old('from_lat')}}">
                            <input name="from_lon" id="from_lon" type="hidden" value="{{old('from_lon')}}">
                        </div>
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-from-address">ঠিকানা</span>
                            <input id="from_address" name="from_address" type="text" class="form-control map_field" value="{{old('from_address')}}" aria-describedby="addon-from-address">
                        </div>                        
                        <div class="form-control-feedback">{{$errors->first('from_address')}}</div>
                        <div class="form-control-feedback">{{$errors->first('from_lat')}}</div>
                        <div class="form-control-feedback">{{$errors->first('from_lon')}}</div>
                    </div>
                    <div class="form-group field-join @if($errors->first('to_address')!=null || $errors->first('to_lat')!=null || $errors->first('to_lon')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-to">গন্তব্য</span>
                            <select id="to_point" name="to_point" class="custom-select" aria-describedby="addon-to">
                                @foreach($points as $point)
                                    <option @if(old('to_point')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-to-address">ঠিকানা</span>
                            <input id="to_address" name="to_address" type="text" class="form-control map_field" value="{{old('to_address')}}" aria-describedby="addon-to-address">
                            <input name="to_lat" id="to_lat" type="hidden" value="{{old('to_lat')}}">
                            <input name="to_lon" id="to_lon" type="hidden" value="{{old('to_lon')}}">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('to_address')}}</div>
                        <div class="form-control-feedback">{{$errors->first('to_lat')}}</div>
                        <div class="form-control-feedback">{{$errors->first('to_lon')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-booking-type">বুকিং-এর সময়</span>
                            <select id="booking_type" name="booking_type" class="custom-select" aria-describedby="addon-booking-type">
                                <option @if(old('booking_type')=='on-demand') selected @endif value="on-demand">এখন</option>
                                <option @if(old('booking_type')=='advance') selected @endif value="advance">আগাম বুকিং</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group booking-date-time @if($errors->first('date_time')!=null) has-danger @endif">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-date-time">তারিখ ও সময় </span>
                            <input id="date_time" name="date_time" type="text" class="form-control icon-field-datetime datetimepick-only" value="{{old('date_time')}}" aria-describedby="addon-date-time">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('date_time')}}</div>
                    </div>                   
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <button type="submit" class="btn btn-outline-secondary book-submit btn-block">সাবমিট</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div id="map"></div>
            <div id="infowindow-content">
              <img src="" width="16" height="16" id="place-icon">
              <span id="place-name"  class="title"></span><br>
              <span id="place-address"></span>
            </div>
        </div>
    </div>
</div>

@stop

@section('footer')
<script type = "text/javascript"> 
$(document).ready(function() {
   
    $(function() {
        $('.booking-date-time').hide();
        $('#booking_type').change(function() {
            if ($('#booking_type').val() == 'advance') {
                $('.booking-date-time').show();
            } else {
                $('.booking-date-time').hide();
            }
        });

        $('.custom-weight').hide();
        $('#weight_category_id').change(function() {
            if ($('#weight_category_id').val() == '0') {
                $('.custom-weight').show();
            } else {
                $('.custom-weight').hide();
                // todo : add value there..
            }
        });

        $('.custom-volumetric').hide();
        $('#volumetric_category_id').change(function() {
            if ($('#volumetric_category_id').val() == '0') {
                $('.custom-volumetric').show();
            } else {
                $('.custom-volumetric').hide();
                // todo : add value there..
            }
        });
    });

});

// map
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 23.777176,
            lng: 90.399452
        },
        zoom: 7
    });
    // place autocomplete feature on map fields
    /*
    var input = document.getElementsByClassName('map_field');
    for (i = 0; i < input.length; i++) {
        autocomplete = new google.maps.places.Autocomplete(input[i]);
        autocomplete.setComponentRestrictions({'country': 'BD'});

        showPlace(autocomplete);
    }
    */
    var fromInput = document.getElementById('from_address');
    var toInput = document.getElementById('to_address');

    from_autocomplete = new google.maps.places.Autocomplete(fromInput);
    from_autocomplete.setComponentRestrictions({
        'country': 'BD'
    });
    showPlace(from_autocomplete, 'from');

    to_autocomplete = new google.maps.places.Autocomplete(toInput);
    to_autocomplete.setComponentRestrictions({
        'country': 'BD'
    });
    showPlace(to_autocomplete, 'to');

    // Show the place user entered
    function showPlace(location, type) {

        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        location.addListener('place_changed', function() {
            var place = location.getPlace();
            if (!place.geometry) {
                // Place was not found
                window.alert("No details available for input: '" + place.name + "'");
                prepareLocationData(false, type);
                marker.setVisible(false);
                return;
            } else {
                prepareLocationData(place, type);

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
            }
        });
    }

    // put coordinators to lat, lon fields
    // from_lat/from_lon, to_lat/to_lat
    function prepareLocationData(place, type) {

        if (place==false) {
            document.getElementById(type + '_lat').value = '';
            document.getElementById(type + '_lon').value = '';
        } else {
             // DEBUGING PURPOSE
            console.log('Type :' + type);
            console.log('address components :');
            console.log(place.address_components);
            console.log('lat, lon :');
            console.log(place.geometry.location.lat(), place.geometry.location.lng());

            document.getElementById(type + '_lat').value = place.geometry.location.lat();
            document.getElementById(type + '_lon').value = place.geometry.location.lng();
        }        
    }
} 
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{config('heavygari.google_maps.api_key')}}&libraries=places&callback=initMap" async defer></script>
@stop