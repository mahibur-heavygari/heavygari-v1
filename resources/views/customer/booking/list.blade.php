@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
@php
$is_corporate_customer = Sentinel::getUser()->customerProfile->corporate()->wherePivot('user_type', 'manager')->where('customer_profile_id', Sentinel::getUser()->customerProfile->id)->first();
@endphp

<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">বুকিং  সমূহ</h1>
            <div class="title-sub">
                Showing {{count($bookings)}} bookings
            </div>            
        </div>
        @if(!isset($is_corporate_customer))
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/customer/panel/bookings/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> যাত্রা বুক করুন</a>
                </li>
            </ul>
        </div>
        @endif
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link @if($filter=='open') active @endif" href="/customer/panel/bookings?filter=open">উন্মুক্ত</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='upcoming') active @endif" href="/customer/panel/bookings?filter=upcoming">আসন্ন</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='ongoing') active @endif" href="/customer/panel/bookings?filter=ongoing">চলমান </a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='completed') active @endif" href="/customer/panel/bookings?filter=completed">সম্পন্ন</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='cancelled') active @endif" href="/customer/panel/bookings?filter=cancelled">বাতিল</a>
        </li>
    </ul>
</div>
@stop

@section('content-body')
    @include('/common/bookings/lists/'.$filter)
@stop