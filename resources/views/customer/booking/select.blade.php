 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

</style>

@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">বুকিং  করুন</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="row no-gutters b-castel align-items-center">
          <div class="col-md-6 text-center">
              <div class="castel-inside">
                    <div class="stat-card-icon">
                      <i class="fa fa-truck booking-img-font"></i>
                    </div>
                    <h3 class="castel-title font-color">সম্পূর্ণ গাড়ির বুকিং করতে চান?</h3>
                    <a class="btn btn-primary btn-lg text-uppercase fs-14 font-weight-semibold letter-spacing-1" href="{{URL('/customer/panel/bookings/full/create')}}">Full Booking</a>
              </div>
            </div>
            <div class="col-md-6 text-center parcel-padding">
                <div class="castel-inside">
                  <div class="stat-card-icon">
                      <i class="icofont icofont-box parcel-img-font"></i>
                  </div>
                    <h3 class="castel-title font-color">পার্সেল ডেলিভারি করবেন?</h3>
                    <a class="btn btn-disable btn-lg text-uppercase fs-14 font-weight-semibold letter-spacing-1">Parcel Booking</a>
                    <!-- href="{{URL('/customer/panel/bookings/parcel/create')}}" -->
                </div>
                <h5 class="coming-soon-text-blink">Coming Soon</h5>
            </div>
        </div>
    </div>
</div>
@stop