@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">নতুন ইউজার</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
    <div class="row">
	    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
	        <div class="wagon wagon-huge wagon-borderd-dashed rounded">

	            <div class="wagon-body">
	                <form class="" method="POST" enctype="multipart/form-data" action="/customer/panel/users/store" >
	                {{ csrf_field() }}
	                    <div class="row">
	                        <div class="col-md-9 col-lg-9">                           
	                            <div class="form-group row @if($errors->first('name')!=null) has-danger @endif">
	                                <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নাম</label>
	                                <div class="col-md-12 col-lg-8">
	                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('name')}}" id="name" name="name" required="">
	                                    <div class="form-control-feedback">@if($errors->first('name')!=null) {{ $errors->first('name')}} @endif</div>
	                                </div>
	                            </div>                          
	                            <div class="form-group row @if($errors->first('phone')!=null) has-danger @endif">
	                                <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফোন নং</label>
	                                <div class="col-md-12 col-lg-8">
	                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('phone')}}" id="phone" name="phone" required="">
	                                    <div class="form-control-feedback">@if($errors->first('phone')!=null) {{ $errors->first('phone')}} @endif</div>
	                                </div>
	                            </div>                          
	                            <div class="form-group row @if($errors->first('email')!=null) has-danger @endif">
	                                <label for="email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ইমেইল</label>
	                                <div class="col-md-12 col-lg-8">
	                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('email')}}" id="email" name="email" required="">
	                                    <div class="form-control-feedback">@if($errors->first('email')!=null) {{ $errors->first('email')}} @endif</div>
	                                </div>
	                            </div>                          
	                            <div class="form-group row @if($errors->first('password')!=null) has-danger @endif">
	                                <label for="password" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">পাসওয়ার্ড</label>
	                                <div class="col-md-12 col-lg-8">
	                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('password')}}" id="password" name="password" required="">
	                                    <div class="form-control-feedback">@if($errors->first('password')!=null) {{ $errors->first('password')}} @endif</div>
	                                </div>
	                            </div>            	                              

	                            <div class="form-group row mb-0">
	                                <div class="col-md-12 col-lg-8 offset-lg-4">
	                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা</button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
@stop