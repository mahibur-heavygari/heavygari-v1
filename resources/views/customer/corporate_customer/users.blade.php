@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')

<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">ইউজার সমূহ</h1>
            <div class="title-sub">
                Showing {{count($users)}} users
            </div>            
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/customer/panel/users/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add User</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@stop
@section('content-body')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
                <table id="example" class="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Create Date</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $item)
                            <tr>
                                <td>
                                    <div class="text-nowrap text-lighter text-sm">{{date('d/m/Y', strtotime($item->created_at))}}</div>
                                </td>
                                <td>
                                    <div class="text-nowrap text-lighter text-sm">{{$item->user->name}}</div>
                                </td>
                                <td>
                                    <div class="text-nowrap text-lighter text-sm">{{$item->user->phone}}</div>
                                </td>
                                
                                <td class="text-right">
                                    <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">                                    
                                        <li>
                                            <a href="/customer/panel/users/{{$item->id}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </li>   
                                    </ul>
                                </td>                           
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
