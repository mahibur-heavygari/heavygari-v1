@if(isset($popup_notification))
<div id="myModal" class="modal-pop">
    <div class="modal-content-pop">
       
        <img src="https://www.heavygari.com{{Storage::url($popup_notification->photo)}}" width="auto" height="auto" >
        @if($popup_notification->button_position == '0')
        <a class="btn btn-modal" style="margin-top:10px;" href="bookings/create">Click Here</a>
        @endif
        @if($popup_notification->button_position == '1')
        <a class="btn btn-modal2" style="margin-top:10px;" href="bookings/create">Click Here</a>
        @endif
        @if($popup_notification->button_position == '2')
        <a class="btn btn-modal3" style="margin-top:10px;" href="bookings/create">Click Here</a> 
        @endif
        @if($popup_notification->button_position == '3')
        <a class="btn btn-modal4" style="margin-top:10px;" href="bookings/create">Click Here</a>
        @endif
        @if($popup_notification->button_position == '4')
        <a class="btn btn-modal5" style="margin-top:10px;" href="bookings/create">Click Here</a>
        @endif

        @if($popup_notification->button_position == '5')
        <a class="btn btn-modal6" style="margin-top:10px;" href="bookings/create">Click Here</a>
        @endif
        @if($popup_notification->button_position == '2')
        <button id="modalClose" class="btn" style="left: 15%;"><i class="icofont icofont-close"></i></button>
        @else
        <button id="modalClose" class="btn"><i class="icofont icofont-close"></i></button>
        @endif
    </div>
</div>
@endif
@extends('dashboards.layout')

@section('menu')
    @include('customer.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            @if($is_corporate_manager)
                <h1 class="title">কর্পোরেট ম্যানেজার ড্যাশবোর্ড</h1>               
            @else
                <h1 class="title">গ্রাহক ড্যাশবোর্ড</h1>
            @endif

            @if(!is_null($profile->corporate_customer_id))
                <div class="title-sub">
                    {{$profile->corporateCustomer->company_name}}
                </div>
            @endif
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="card-group">
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/customer/panel/bookings?filter=ongoing">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['ongoing']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-fast-delivery"></i>
                        </div>
                        <span class="stat-card-name">চলমান বুকিংসমূহ </span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/customer/panel/bookings?filter=upcoming">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['upcoming']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-delivery-time"></i>
                        </div>
                        <span class="stat-card-name">আসন্ন বুকিংসমূহ</span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/customer/panel/bookings?filter=open">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['open']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-box"></i>
                        </div>
                        <span class="stat-card-name">বর্তমান বুকিংসমূহ </span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/customer/panel/bookings?filter=completed">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['completed']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-vehicle-delivery-van"></i>
                        </div>
                        <span class="stat-card-name">সম্পন্ন বুকিংসমূহ </span>
                    </div>
                </a>
            </div>            
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/customer/panel/transactions">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$payment_stats['total_cost']}} টাকা </h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-cur-taka-minus"></i>
                        </div>
                        <span class="stat-card-name">মোট পারিশ্রমিক</span>
                    </div>
                </a>
            </div>
        </div>

        {{--
        <div class="row">            
            <div class="col-sm-12 col-md-6">
                <div class="card-group">
                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <h4 class="card-heading-title title-sm">যাত্রা</h4>
                        </div>
                        <ul class="static-lists">
                            <li>
                                <span>ঢাকা</span>
                                <span class="text-center">১০</span>
                                <span class="bullet-span justify-content-end">
                                    <span class="bullet-span-bol" style="background-color: rgba(255, 99, 132, 0.5)"></span>
                                </span>
                            </li>
                            <li>
                                <span>চিটাগাং</span>
                                <span class="text-center">০৮</span>
                                <span class="bullet-span justify-content-end">
                                    <span class="bullet-span-bol" style="background-color: rgba(54, 162, 235, 0.5)"></span>
                                </span>
                            </li>
                            <li>
                                <span>খুলনা</span>
                                <span class="text-center">০৫</span>
                                <span class="bullet-span justify-content-end">
                                    <span class="bullet-span-bol" style="background-color: rgba(255, 206, 86, 0.5)"></span>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block">
                            <div class="chart-vehicle-holder">
                                <canvas id="chartCustomers" width="200" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="card-group">
                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <h4 class="card-heading-title title-sm">সংক্ষিপ্ত গড়</h4>
                        </div>
                        <table class="table table-profile-avag">
                            <tbody>
                                <tr>
                                    <td><span class="tp-text">পাঁচ তারকা</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">চার তারকা</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">তিন  তারকা</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">দুই তারকা</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">এক  তারকা</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block text-center ">
                            <div class="avg-block">
                                <div>
                                    <h2>৪.২/৫</h2>
                                    <span class="text-lighter">বাবহারকারির প্রোফাইল পরিদর্শন গড়</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        --}}
    </div>
</div>
@stop

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase.js"></script> -->
<meta name="csrf-token" content="{{ csrf_token() }}"/>

<script>
    /*$(function() {
        var ctx = document.getElementById("chartVehicles");
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ["Truck", "Van", "Microbus"],
                datasets: [{
                    label: 'Vehicles',
                    data: [12, 19, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)'
                    ]
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });
    });

    $(function() {
        var ctx = document.getElementById("chartCustomers");
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ["Truck", "Van", "Microbus"],
                datasets: [{
                    label: 'Customers',
                    data: [12, 19, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)'
                    ]
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });
    });*/


   /* var config = {
        apiKey: "AIzaSyCpcCajfoD0LBT88TGAyzWUkAEgzOfBWWo",
        authDomain: "heavygari-web.firebaseapp.com",
        databaseURL: "https://heavygari-web.firebaseio.com",
        projectId: "heavygari-web",
        storageBucket: "heavygari-web.appspot.com",
        messagingSenderId: "422142305895"
    };
    firebase.initializeApp(config);
    const messaging = firebase.messaging();
    messaging.usePublicVapidKey("BIpR-a98ybgXNnbbPIxiCCghv63Nhur1xxuzZ4uUAZcz5QULJ5neu34BzxBIxQ-VnpkK9wOJYw7VHMfhC2hCzlI");

    messaging.requestPermission().then(function() {
        return messaging.getToken();
    }).then(function(token){
        updateTokenToHeavygari(token);
    })
    .catch(function(err) {
        updateTokenToHeavygari(null);
    });

    messaging.onTokenRefresh(function() {
        messaging.getToken().then(function(refreshedToken) {
            updateTokenToHeavygari(refreshedToken);
        }).catch(function(err) {
            updateTokenToHeavygari(null);
        });
    });

    function updateTokenToHeavygari(token){
        $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });
        $.post('update_fcm', { fcm_token: token }, function( response ) {
            if(response.success==true){
                //console.log('updated');
            }
        })
    }*/
    $('#myModal').modal('show');
    $('#modalClose').click(function(){
        $('#myModal').modal('toggle');
    })
</script> 
@stop