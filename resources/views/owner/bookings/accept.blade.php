@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop


@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card booking-confrim">
                <div class="booking-confrim-block">
                    <!-- <div class="cost-text-card">
                        <h4 class="">Total Cost : BDT. 9276.75</h4>
                        <h5 class="">My Earning : BDT. 8835.00</h5>
                    </div> -->
                    <ul class="confrim-list mb-3">
                        <li>
                            <span>বুকিং এর :</span>
                            <span>টাকা {{$booking->invoice->total_cost}}</span>
                        </li>
                        <li>
                            <span>গাড়ির মালিকের আয়:</span>
                            <span>টাকা {{$booking->earnings->owner_earning}}</span>
                        </li>
                    </ul>
                    <form method="POST">
                        {{ csrf_field() }}
                        <div class="form-group field-join">
                            <div class="input-group input-flex @if($errors->first('vehicle')!=null) has-danger @endif">
                                <span class="input-group-addon" id="addon-from-address">গাড়ি </span>
                                <select name="vehicle" class="custom-select">
                                    <option value="" selected >--অনুগ্রহ পূর্বক নির্বাচন করুন--</option>
                                    @foreach($my_vehicles as $vehicle)
                                        <option value="{{ $vehicle->id }}">{{ $vehicle->number_plate }} ( {{ $vehicle->name }} )</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->first('vehicle')!=null)
                                <div class="form-control-feedback">{{$errors->first('vehicle')}}</div>
                            @endif
                            <div id="driver-selection">
                                <div v-if="seen">
                                    <div v-if="seen" class="input-group input-flex @if($errors->first('driver')!=null) has-danger @endif">
                                        <span class="input-group-addon" id="addon-from">চালক</span>
                                        <select name="driver" class="custom-select" >
                                            <option value="" selected >--অনুগ্রহ পূর্বক নির্বাচন করুন--</option>
                                            @foreach($my_drivers as $driver)
                                                <option value="{{ $driver->id }}" >{{ $driver->user->name }}</option>
                                            @endforeach                                    
                                        </select>                                
                                    </div>
                                    @if($errors->first('driver')!=null)
                                        <div class="form-control-feedback">{{$errors->first('driver')}}</div>
                                    @endif
                                </div>
                                <div v-if="!seen">
                                    <div class="input-group input-flex @if($errors->first('driver')!=null) has-danger @endif">
                                        <span class="input-group-addon" id="addon-from">চালক এর নাম</span>
                                        <input class="form-control form-control-lg like-field" type="text" value="{{old('driver')}}" id="driver" name="driver" required="">
                                    </div>

                                    <div class="input-group input-flex @if($errors->first('phone')!=null) has-danger @endif">
                                        <span class="input-group-addon" id="addon-from">চালক এর ফোন</span>
                                        <input class="form-control form-control-lg like-field" type="text" value="{{old('phone')}}" id="phone" name="phone" required="">
                                    </div>
                                </div>
                                <br>
                                <div class="input-group input-flex" v-if="seen">
                                    <a v-on:click="greet" class="new-driver" href="javascript:void(0)">নতুন ড্রাইভার যোগ করুন</a>
                                </div>
                                <input type="hidden" v-model="is_hired" name="is_hired">
                            </div>

                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary text-uppercase fs-14 font-weight-semibold letter-spacing-1 px-4">Accept Job</button>
                        </div>
                    </form>
                </div>                        
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div class="map-block map-booking">
                <iframe src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking->trips[0]->from_lat}},{{$booking->trips[0]->from_lon}}&destination={{$booking->trips[0]->to_lat}},{{$booking->trips[0]->to_lon}}" width="800" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<div class="map-block-overlay"></div>-->
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="https://unpkg.com/vue"></script>
<script>
    new Vue({
        el: '#driver-selection',
        data: {
            seen: true,
            is_hired: false
        },
        methods: 
        {
            greet(){
                this.seen = false;
                this.is_hired = true;
            }
        }
    });
</script>
@stop