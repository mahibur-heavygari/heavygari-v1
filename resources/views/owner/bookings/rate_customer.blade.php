@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">গ্রাহককে রেট করুন</h1>
            <div class="title-sub">
               Booking ID # {{$booking['unique_id']}}
            </div>
        </div>
    </div>
</div>
@stop


@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-body">
                <form class="" method="POST" action="" >
                    {{ csrf_field() }}
                    <div class="row">                       
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row">
                                <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">গ্রাহক</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" value="{{$booking['customer']['user']['name']}}" disabled/>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('rating')!=null) has-danger @endif">
                                <label for="rating" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেট করুন</label>
                                <div class="col-md-12 col-lg-8">                                    
                                    <div class="rating-site">
                                        <input type="text" name="rating" class="kv-uni-star rating-loading" value="" data-size="xs" title="">
                                    </div>
                                    <div class="form-control-feedback">@if($errors->first('rating')!=null) {{ $errors->first('rating')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('name')!=null) has-danger @endif">
                                <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">মন্তব্য</label>
                                <div class="col-md-12 col-lg-8">
                                     <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="review" name="review">{{old('review')}}</textarea>
                                    <div class="form-control-feedback">@if($errors->first('review')!=null) {{ $errors->first('review')}} @endif</div>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা করুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop