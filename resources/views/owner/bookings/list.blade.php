@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">গাড়ী বুকিংসমূহ</h1>
            <div class="title-sub">
                Showing {{count($bookings)}} bookings
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link @if($filter=='open') active @endif" href="/owner/panel/trips-by-my-vehicles?filter=open">উন্মুক্ত</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='upcoming') active @endif" href="/owner/panel/trips-by-my-vehicles?filter=upcoming">আসন্ন</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='ongoing') active @endif" href="/owner/panel/trips-by-my-vehicles?filter=ongoing">চলমান</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='completed') active @endif" href="/owner/panel/trips-by-my-vehicles?filter=completed">সম্পন্ন</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='cancelled') active @endif" href="/owner/panel/trips-by-my-vehicles?filter=cancelled">বাতিল</a>
        </li>
    </ul>
</div>
@stop

@section('content-body')
    @include('/common/bookings/lists/'.$filter)
@stop