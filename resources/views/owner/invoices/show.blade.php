@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-body')
    @include('common/invoices/show')
@stop
