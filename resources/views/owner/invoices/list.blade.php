@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">চালানপত্র সমূহ</h1>
            <div class="title-sub">
                Showing {{count($list)}} invoices
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
</div>
@stop

@section('content-body')
    @include('/common/invoices/list')
@stop