@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">যোগাযোগ</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">            
            <div class="wagon-body">
                <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/save-contact-us" >
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9"> 
                            <div class="form-group row @if($errors->first('message')!=null) has-danger @endif"">
                                <label for="message" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">আপনার মতামত</label>
                                <div class="col-md-12 col-lg-8">
                                    <textarea class="form-control form-control-lg like-field" type="text" id="message" name="message"></textarea>
                                    <div class="form-control-feedback">@if($errors->first('message')!=null) {{ $errors->first('message')}} @endif</div>
                                </div>
                            </div>                                                                  

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা করুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop