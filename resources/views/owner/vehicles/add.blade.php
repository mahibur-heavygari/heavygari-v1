@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">গাড়ী যোগ করুন</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">১. মৌলিক তথ্যসমূহ</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/owner/panel/vehicles">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="col-sm-1">ছবি</div>
                            <div class="upload-styled-image rounded-circle" style="width: 120px; height: 120px;">
                                <div class="uploaded-image uploaded-here"></div>
                                <div class="input-file">
                                    <input type="file" name="photo" class="file-input">
                                    <span class="upload-icon">
                                        <i class="icofont icofont-upload-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('vehicle_type_id')!=null) has-danger @endif">
                                <label for="ex-reg-number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ধরণ</label>
                                <div class="col-md-12 col-lg-8">
                                    <select name="vehicle_type_id" class="custom-select width-100per mb-2">
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->title }}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-control-feedback">@if($errors->first('vehicle_type_id')!=null) {{ $errors->first('vehicle_type_id')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('name')!=null) has-danger @endif">
                                <label for="ex-reg-number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ব্র্যান্ড ও মডেল <span class="required-star">*</span></label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('name')}}" id="name" name="name">
                                    <div class="form-control-feedback">@if($errors->first('name')!=null) {{ $errors->first('name')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('ref_no')!=null) has-danger @endif">
                                <label for="ref_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেফারেন্স নং</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('ref_no')}}" id="ref_no" name="ref_no">
                                    <div class="form-control-feedback">@if($errors->first('ref_no')!=null) {{ $errors->first('ref_no')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('about')!=null) has-danger @endif">
                                <label for="ex-reg-number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নিজ সম্পর্কিত </label>
                                <div class="col-md-12 col-lg-8">                               
                                    <textarea id="about" name="about" class="form-control form-control-lg like-field form-control-danger">{{old('about')}}</textarea>
                                    <div class="form-control-feedback">@if($errors->first('about')!=null) {{ $errors->first('about')}} @endif</div>
                                </div>
                            </div>                        
                            <div class="form-group row @if($errors->first('vehicle_registration_number')!=null) has-danger @endif">
                                <label for="ex-reg-number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেজিস্ট্রেশন নং <span class="required-star">*</span></label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('vehicle_registration_number')}}" id="vehicle_registration_number" name="vehicle_registration_number">
                                    <div class="form-control-feedback">@if($errors->first('vehicle_registration_number')!=null) {{ $errors->first('vehicle_registration_number')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('fitness_number')!=null) has-danger @endif">
                                <label for="ex-fit-number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফিটনেস সনদ নং </label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('fitness_number')}}" id="fitness_number" name="fitness_number">
                                    <div class="form-control-feedback">@if($errors->first('fitness_number')!=null) {{ $errors->first('fitness_number')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('fitness_expiry')!=null) has-danger @endif">
                                <label for="ex-fit-expiry" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফিটনেস সনদ মেয়াদোত্তীর্ণ তারিখ<span class="required-star">*</span></label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="field-only-date has-control-lg">
                                        <input class="form-control form-control-lg like-field form-control-danger datepick-only" type="text" value="{{old('fitness_expiry')}}" id="fitness_expiry" name="fitness_expiry">
                                        <div class="form-control-feedback">@if($errors->first('fitness_expiry')!=null) {{ $errors->first('fitness_expiry')}} @endif</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('number_plate')!=null) has-danger @endif">
                                <label for="ex-number-plate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নাম্বার প্লেট <span class="required-star">*</span></label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('number_plate')}}" id="number_plate" name="number_plate">
                                    <div class="form-control-feedback">@if($errors->first('number_plate')!=null) {{ $errors->first('number_plate')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('tax_token_number')!=null) has-danger @endif">
                                <label for="ex-tax-token-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ট্যাক্স টোকেন নং </label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('tax_token_number')}}" id="tax_token_number" name="tax_token_number">
                                    <div class="form-control-feedback">@if($errors->first('tax_token_number')!=null) {{ $errors->first('tax_token_number')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('tax_token_expirey')!=null) has-danger @endif">
                                <label for="ex-tax-token-expirey" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ট্যাক্স টোকেন মেয়াদোত্তীর্ণ তারিখ<span class="required-star">*</span></label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="field-only-date has-control-lg">
                                        <input class="form-control form-control-lg like-field form-control-danger datepick-only" type="text" value="{{old('tax_token_expirey')}}" id="tax_token_expirey" name="tax_token_expirey">
                                        <div class="form-control-feedback">@if($errors->first('tax_token_expirey')!=null) {{ $errors->first('tax_token_expirey')}} @endif</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('chesis_no')!=null) has-danger @endif">
                                <label for="ex-chesis-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">চেসিস নং </label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('chesis_no')}}" id="chesis_no" name="chesis_no">
                                    <div class="form-control-feedback">@if($errors->first('chesis_no')!=null) {{ $errors->first('chesis_no')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('engine_capacity_cc')!=null) has-danger @endif">
                                <label for="ex-cc-limit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ইঞ্জিনের ধারণক্ষমতা মাত্রা</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('engine_capacity_cc')}}" id="engine_capacity_cc" name="engine_capacity_cc">
                                    <div class="form-control-feedback">@if($errors->first('engine_capacity_cc')!=null) {{ $errors->first('engine_capacity_cc')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('ride_sharing_certificate_no')!=null) has-danger @endif">
                                <label for="ex-cc-limit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">গাড়ীর রাইড শেয়ারিং এনলিসমেন্ট সনদ</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('ride_sharing_certificate_no')}}" id="ride_sharing_certificate_no" name="ride_sharing_certificate_no">
                                    <div class="form-control-feedback">@if($errors->first('ride_sharing_certificate_no')!=null) {{ $errors->first('ride_sharing_certificate_no')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('insurance_no')!=null) has-danger @endif">
                                <label for="ex-cc-limit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">গাড়ীর ইন্সুরেন্স নং</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('insurance_no')}}" id="insurance_no" name="insurance_no">
                                    <div class="form-control-feedback">@if($errors->first('insurance_no')!=null) {{ $errors->first('insurance_no')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('authorized_drivers')!=null) has-danger @endif">
                                <label for="ex-cc-limit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">অথরাইজড চালক </label>
                                <div class="col-md-12 col-lg-8">
                                    <select name="authorized_drivers[]" class="custom-select width-100per mb-2" multiple>
                                        @foreach($my_drivers as $driver)
                                            <option value="{{ $driver->id }}" selected >{{ $driver->user->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-control-feedback">@if($errors->first('authorized_drivers')!=null) {{ $errors->first('authorized_drivers')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('height')!=null) has-danger @endif">
                                <label for="ex-chesis-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">উচ্চতা ( Height in feet )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('height')}}" id="height" name="height">
                                    <div class="form-control-feedback">@if($errors->first('height')!=null) {{ $errors->first('height')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('length')!=null) has-danger @endif">
                                <label for="ex-chesis-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">দৈর্ঘ্য ( Length in feet )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('length')}}" id="length" name="length">
                                    <div class="form-control-feedback">@if($errors->first('length')!=null) {{ $errors->first('length')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('width')!=null) has-danger @endif">
                                <label for="ex-chesis-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">প্রস্থ ( Width in feet )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('width')}}" id="width" name="width">
                                    <div class="form-control-feedback">@if($errors->first('width')!=null) {{ $errors->first('width')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('custom_capacity')!=null) has-danger @endif">
                                <label for="ex-chesis-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ধারণক্ষমতা ( Custom Capacity )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('custom_capacity')}}" id="custom_capacity" name="custom_capacity">
                                    <div class="form-control-feedback">@if($errors->first('custom_capacity')!=null) {{ $errors->first('custom_capacity')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('registered_by')!=null) has-danger @endif">
                                <label for="ex-chesis-no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেজিস্ট্রেশনকারীর নাম <span class="required-star">*</span></label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('registered_by')}}" id="registered_by" name="registered_by">
                                    <div class="form-control-feedback">@if($errors->first('registered_by')!=null) {{ $errors->first('registered_by')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা করুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop