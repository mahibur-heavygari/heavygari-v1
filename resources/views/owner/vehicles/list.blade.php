@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">গাড়ীসমূহ </h1>
            <div class="title-sub">
                Showing {{count($vehicles)}} vehicles
            </div>            
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/owner/panel/vehicles/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> গাড়ী যোগ করুন</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 col-xxl-8 offset-xxl-2">
        <div class="vehicle-group">
        @foreach ($vehicles as $vehicle)
            <div class="vehicle">
                <div class="row">
                    <div class="col-sm-4 col-lg-3 vehicle-image-block booking-cards">
                        <a href="/owner/panel/vehicles/{{ $vehicle->id }}" class="vehicle-image">
                            <img src="{{Storage::url($vehicle->thumb_photo)}}" alt="...">
                        </a>
                        <div class="vehicle-id">{{ $vehicle->number_plate }}</div>
                        <span class="badge badge-pill @if($vehicle->status=='available') badge-success @elseif($vehicle->status=='booked') badge-warning @elseif($vehicle->status=='off-duty') badge-danger @else badge-warning @endif"> {{ucfirst($vehicle->status)}} </span>
                    </div>

                    <div class="col-sm-8 col-lg-9 vehicle-block">
                        <div class="vehicle-header d-flex align-items-center justify-content-between">
                            <div class="vehicle-header-col d-flex align-items-center">
                                <h4 class="vehicle-name"><a href="/owner/panel/vehicles/{{ $vehicle->id }}">{{ $vehicle->name }}</a></h4>

                            </div>
                            <div class="vehicle-header-col d-flex align-items-center header-metas">
                                <div class="dropdown dropdown-default dropdown-arrow-off">
                                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="ion-more"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="/owner/panel/vehicles/{{ $vehicle->id }}"><i class="fa fa-info-circle"></i> <span class="dt-text">দেখুন</span></a>
                                        <a class="dropdown-item" href="/owner/panel/vehicles/{{ $vehicle->id }}/edit"><i class="fa fa-pencil"></i> <span class="dt-text">সম্পাদন </span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vehicle-body">
                            <p>
                                গাড়ীর ধরণ : {{$vehicle->vehicleType->title_bn}}
                            </p>
                            <p>
                                {{ $vehicle->about }}
                            </p>
                            <ul class="vehicle-sort-info">
                                <li>রেজিস্ট্রেশন নং: <span>{{ $vehicle->vehicle_registration_number }}</span></li>
                                <li>ফিটনেস সনদ নং : <span>{{ $vehicle->fitness_number }}</span></li>
                                <li>ফিটনেস সনদ মেয়াদোত্তীর্ণ তারিখ : <span>{{ $vehicle->fitness_expiry }}</span></li>
                            </ul>
                            <div class="vehicle-Driver">
                                <span><i class="icofont icofont-steering" aria-hidden="true"></i> অথরাইজড চালকগণ : &nbsp;</span>
                                @foreach($vehicle->authorizedDrivers as $key => $authorizedDriver)
                                    {{$key+1}})&nbsp;<a href="/owner/panel/drivers/{{$authorizedDriver->id}}">{{$authorizedDriver->user->name}}</a>&nbsp;&nbsp;
                                @endforeach

                            </div>
                            <div class="vehicle-Driver">
                                <span><i class="icofont icofont-steering" aria-hidden="true"></i> বর্তমান চালক : &nbsp;</span>
                                <span class="no-capt capt-name">
                                    @if($vehicle->currentDriver)
                                        <a href="/owner/panel/drivers/{{$vehicle->currentDriver->id}}">{{$vehicle->currentDriver->user->name}}</a>
                                    @else
                                        N/A
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
@stop

