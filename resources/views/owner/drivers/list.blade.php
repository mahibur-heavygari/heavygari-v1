@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">চালকগন</h1>
            <div class="title-sub">
                Showing {{count($drivers)}} driver(s)
            </div>            
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/owner/panel/drivers/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i>নতুন চালক  যোগ করুন </a>                    
                </li>
                @if(!$isDriver)
                <li>
                    <a href="/owner/panel/drivers/make-me-driver" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i>আমাকে চালক করুন</a>                    
                </li>                        
                @endif
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 col-xxl-8 offset-xxl-2">
        <div class="row">
            @foreach ($drivers as $driver)
                <div class="col-xs-12 col-sm-6 col-md-4 ">
                    <div class="driver-card text-center booking-cards">
                        <div class="driver-avater">
                            <a href="">
                                <img src="{{Storage::url($driver->user->thumb_photo)}}" alt="...">
                            </a>
                        </div>
                        <div class="driver-conent">
                            <h4 class="driver-name"><a href="/owner/panel/drivers/{{ $driver->id }}">{{ $driver->user->name }}</a></h4>
                            <div class="rating-site">
                                <input type="text" class="kv-uni-star rating-loading" value="{{$driver->myAverageRating()}}" data-size="xs" title="" readonly="">
                            </div>
                            <ul class="driver-sort-info list-inline">
                                <li>ফোন নং: <span>{{ $driver->user->phone }}</span></li>
                                {{--<li>ইমেইল : <span>{{ $driver->user->email }}</span></li>--}}
                                <li>জাতীয় পরিচয় পত্র নং : <span>{{ $driver->user->national_id }}</span></li>
                            </ul>
                        </div>
                        <div class="footer-metas">
                            <div class="dropdown dropdown-default dropdown-arrow-off">
                                <a class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ion-android-more-vertical"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="/owner/panel/drivers/{{ $driver->id }}"><i class="fa fa-info-circle"></i> <span class="dt-text">দেখুন </span></a>
                                    <a class="dropdown-item" href="/owner/panel/drivers/{{ $driver->id }}/edit"><i class="fa fa-pencil"></i> <span class="dt-text">সম্পাদন</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             @endforeach
        </div>
    </div>
</div>
@stop