@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">চালক পরিবর্তন করুণ</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">

        <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/drivers/{{$profile->id}}" >
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            @include('/common/driver/_partials/edit')
        </form>

        <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/drivers/{{$profile->id}}/preferences" >
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            @include('/common/driver/_partials/change_preferences')
        </form>

        <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/drivers/{{$profile->id}}/change_password" >
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            @include('/common/driver/_partials/change_password')
        </form>

    </div>
</div>
@stop