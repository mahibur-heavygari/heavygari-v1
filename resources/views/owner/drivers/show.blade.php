@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">চালকের প্রোফাইল</h1>
            <div class="title-sub">
                {{$profile->user->name}}
            </div>
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <span href="" class="text @if($profile->user->status=='active') text-success @elseif($profile->user->status=='inactive') text-danger @elseif($profile->user->status=='blocked') text-danger @endif "><i class="fa fa-user"></i> {{$profile->user->status}}</span>
                    |
                    <span href="" class="text @if($profile->is_phone_verified==1) text-success @else text-danger @endif "><i class="fa fa-phone"></i> @if($profile->is_phone_verified==1) Verified @else Unverified @endif</span>

                    @if($profile->is_phone_verified!=1)
                        <span><a href="/owner/panel/drivers/verify/{{$profile->id}}">(verify now)</a></span>
                    @endif
                </li>
            </ul>
        </div>
    </div>
    <!--
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Profile</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Reviews</a>
        </li>        
    </ul>-->
</div>
@stop

@section('content-body')
	@include('common/driver/details')
@stop