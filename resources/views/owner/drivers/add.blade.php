@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">চালক যোগ করুন </h1>
            <div class="title-sub">
					নতুন চালক নিন 
            </div>
        </div>        
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">১. মৌলিক তথ্যসমূহ </h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/drivers" >
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                        <div>ছবি</div>
                            <div class="upload-styled-image rounded-circle" style="width: 120px; height: 120px;">
                                <div class="uploaded-image uploaded-here" style=""></div>
                                <div class="input-file">
                                    <input type="file" name="photo" class="file-input">
                                    <span class="upload-icon">
                                        <i class="icofont icofont-upload-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9">                           
                            <div class="form-group row @if($errors->first('phone')!=null) has-danger @endif">
                                <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফোন নং</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('phone')}}" id="phone" name="phone">
                                    <div class="form-control-feedback">@if($errors->first('phone')!=null) {{ $errors->first('phone')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('password')!=null) has-danger @endif">
                                <label for="password" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">পাসওয়ার্ড</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="password" value="{{old('password')}}" id="password" name="password">
                                    <div class="form-control-feedback">@if($errors->first('password')!=null) {{ $errors->first('password')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('password_confirmation')!=null) has-danger @endif">
                                <label for="password_confirmation" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">পাসওয়ার্ড নিশ্চয়তা</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="password" value="{{old('password_confirmation')}}" id="password_confirmation" name="password_confirmation">
                                    <div class="form-control-feedback">@if($errors->first('password_confirmation')!=null) {{ $errors->first('password_confirmation')}} @endif</div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group row @if($errors->first('name')!=null) has-danger @endif">
                                <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নাম</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('name')}}" id="name" name="name">
                                    <div class="form-control-feedback">@if($errors->first('name')!=null) {{ $errors->first('name')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('ref_no')!=null) has-danger @endif">
                                <label for="ref_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেফারেন্স নং</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('ref_no')}}" id="ref_no" name="ref_no">
                                    <div class="form-control-feedback">@if($errors->first('ref_no')!=null) {{ $errors->first('ref_no')}} @endif</div>
                                </div>
                            </div>
                            {{--<div class="form-group row @if($errors->first('email')!=null) has-danger @endif">
                                <label for="email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ইমেইল </label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="email" value="{{old('email')}}" id="email" name="email">
                                    <div class="form-control-feedback">@if($errors->first('email')!=null) {{ $errors->first('email')}} @endif</div>
                                </div>
                            </div>--}}
                            <div class="form-group row @if($errors->first('national_id')!=null) has-danger @endif">
                                <label for="national_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">জাতীয় পরিচয় পত্র নং </label>
                               
                                <div class="col-md-12 col-lg-8">     
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('national_id')}}" id="national_id" name="national_id">
                                            <div class="form-control-feedback">@if($errors->first('national_id')!=null) {{ $errors->first('national_id')}} @endif</div>
                                        </div>
                                        <div class="col-sm-1">
                                            ছবি
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="upload-styled-image upload-styled-image-40p mb-2">
                                                <div class="uploaded-image uploaded-here" style=""></div>
                                                <div class="input-file">
                                                    <input class="file-input" type="file" name="national_id_photo" id="national_id_photo">
                                                    <span class="upload-icon">
                                                        <i class="icofont icofont-upload-alt"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('about')!=null) has-danger @endif">
                                <label for="about" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নিজ সম্পর্কিত </label>
                                <div class="col-md-12 col-lg-8">
                                    <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="about" name="about">{{old('about')}}</textarea>
                                    <div class="form-control-feedback">@if($errors->first('about')!=null) {{ $errors->first('about')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                            <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">লাইসেন্স  নং</label>                           
                            <div class="col-md-12 col-lg-8">     
                                <div class="row">
                                    <div class="col-sm-7">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('license_no')}}" id="license_no" name="license_no">
                                        <div class="form-control-feedback">@if($errors->first('license_no')!=null) {{ $errors->first('license_no')}} @endif</div>
                                    </div>
                                    <div class="col-sm-1">
                                        ছবি
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="upload-styled-image upload-styled-image-40p mb-2">
                                            <div class="uploaded-image uploaded-here" style=""></div>
                                            <div class="input-file">
                                                <input class="file-input" type="file" name="license_image" id="license_image">
                                                <span class="upload-icon">
                                                    <i class="icofont icofont-upload-alt"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="form-group row @if($errors->first('license_date_of_issue')!=null) has-danger @endif">
                                <label for="license_date_of_issue" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">প্রদানের তারিখ</label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="field-only-date has-control-lg">
                                        <input class="form-control form-control-lg like-field datepick-only form-control-danger" type="text" value="{{old('license_date_of_issue')}}" id="license_date_of_issue" name="license_date_of_issue">
                                    </div>
                                    <div class="form-control-feedback">@if($errors->first('license_date_of_issue')!=null) {{ $errors->first('license_date_of_issue')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_date_of_expire')!=null) has-danger @endif">
                                <label for="license_date_of_expire" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">মেয়াদোত্তীর্ণ তারিখ</label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="field-only-date has-control-lg">
                                        <input class="form-control form-control-lg like-field datepick-only form-control-danger" type="text" value="{{old('license_date_of_expire')}}" id="license_date_of_expire" name="license_date_of_expire">
                                    </div>
                                    <div class="form-control-feedback">@if($errors->first('license_date_of_expire')!=null) {{ $errors->first('license_date_of_expire')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_issuing_authority')!=null) has-danger @endif">
                                <label for="license_issuing_authority" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">প্রদানকারী কর্তৃপক্ষ</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('license_issuing_authority')}}" id="license_issuing_authority" name="license_issuing_authority">
                                    <div class="form-control-feedback">@if($errors->first('license_issuing_authority')!=null) {{ $errors->first('license_issuing_authority')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('address')!=null) has-danger @endif">
                                <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ঠিকানা</label>
                                <div class="col-md-12 col-lg-8">
                                    <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="address" name="address">{{old('address')}}</textarea>
                                    <div class="form-control-feedback">@if($errors->first('address')!=null) {{ $errors->first('address')}} @endif</div>
                                </div>
                            </div>                        

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1"> জমা করুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop