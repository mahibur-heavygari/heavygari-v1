@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">নিজ প্রোফাইল</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">১.  ব্যাক্তিগত তথ্য</h4>
                </div>
                <div class="wh-col">
                    <div class="wh-meta">
                        <a href="/owner/panel/profile/edit">
							সম্পাদন <i class="icofont icofont-ui-edit"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="wagon-body">
                <form action="">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div>ছবি</div>
                            <div class="upload-styled-image rounded-circle upload-image-caption" style="width: 120px; height: 120px;">
                                <div class="uploaded-image uploaded-here" style="background-image: url('{{Storage::url($profile->user->thumb_photo)}}');"></div>
                                <a href="{{Storage::url($profile->user->photo)}}" class="eye-view image-popup"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row">
                                <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নাম</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="text" value="{{$profile->user->name}}" id="name" name="name" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফোন নং</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="text" value="{{$profile->user->phone}}" id="phone" name="phone" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ইমেইল</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="email" value="{{$profile->user->email}}" id="email" name="email" readonly>
                                </div>
                            </div>        
                            <div class="form-group row">
                                <label for="company_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">কোম্পানি</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="company_name" value="{{$profile->company_name}}" id="company_name" name="company_name" readonly>
                                </div>
                            </div>    
                            <div class="form-group row">
                                <label for="manager_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ম্যানেজারের নাম</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="manager_name" value="{{$profile->manager_name}}" id="manager_name" name="manager_name" readonly>
                                </div>
                            </div>    
                            <div class="form-group row">
                                <label for="manager_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ম্যানেজারের ফোন</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="manager_phone" value="{{$profile->manager_phone}}" id="manager_phone" name="manager_phone" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="national_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">জাতীয় পরিচয় পত্র</label>                               
                                <div class="col-md-12 col-lg-8">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <input class="form-control form-control-lg like-field" type="text" value="{{$profile->user->national_id}}" id="national_id" name="national_id" readonly>
                                        </div>
                                        <div class="col-sm-1">
                                            ছবি
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="tumblr-img tumblr-img-pro">
                                                <img src="{{Storage::url($profile->user->national_id_photo)}}" alt="...">
                                                <a href="{{Storage::url($profile->user->national_id_photo)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ownership_card_number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">মালিকানা কার্ড</label>                               
                                <div class="col-md-12 col-lg-8">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <input class="form-control form-control-lg like-field" type="text" value="{{$profile->ownership_card_number}}" id="ownership_card_number" name="ownership_card_number" readonly>
                                        </div>
                                        <div class="col-sm-1">
                                            ছবি
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="tumblr-img tumblr-img-pro">
                                                <img src="{{Storage::url($profile->ownership_card_picture)}}" alt="...">
                                                <a href="{{Storage::url($profile->ownership_card_picture)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="company_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">কোম্পানি</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="company_name" value="{{$profile->company_name}}" id="company_name" name="company_name" readonly>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ঠিকানা</label>
                                <div class="col-md-12 col-lg-8">
                                    <textarea class="form-control form-control-lg like-field" type="text" id="address" name="address" readonly>{{$profile->user->address}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">২.  ব্যাংকের তথ্য</h4>
                </div>
                <div class="wh-col">
                    <div class="wh-meta">                        
                    </div>
                </div>
            </div>
            <div class="wagon-body">
                <form action="">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">                            
                        </div>
                        <div class="col-md-9 col-lg-9">                               
                            <div class="form-group row">
                                <label for="bank_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ব্যাংকের নাম</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="company_name" value="{{$profile->bank_name}}" id="bank_name" name="bank_name" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bank_branch" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ব্যাংকের ব্রাঞ্চ</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="bank_branch" value="{{$profile->bank_branch}}" id="bank_branch" name="bank_branch" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="account_number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">অ্যাকাউন্ট নং</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="account_number" value="{{$profile->account_number}}" id="account_number" name="account_number" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop