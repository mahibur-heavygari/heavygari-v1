@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">প্রোফাইল</h1>
            <div class="title-sub">
                Welcome {{ Sentinel::getUser()->name}}!
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/profile" >
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include('common.owner.edit')
        </form>
    </div>
</div>
@stop