<ul class="aside-nav">
    <li class="aside-nav-item {{ Request::is('owner/panel/dashboard*') ? 'active' : '' }}"><a href="/owner/panel/dashboard"><i class="icofont icofont-dashboard"></i> <span class="nav-text">ড্যাশবোর্ড</span> </a></li>

    <li class="aside-nav-item {{ Request::is('owner/panel/trips-by-my-vehicles*') ? 'active' : '' }}"><a href="/owner/panel/trips-by-my-vehicles"><i class="icofont icofont-calendar"></i> <span class="nav-text">বুকিং সমূহ</span> </a></li>
   
    <li class="aside-nav-item {{ Request::is('owner/panel/vehicles*') ? 'active' : '' }}"><a href="/owner/panel/vehicles"><i class="icofont icofont-truck-loaded"></i> <span class="nav-text">গাড়ী সমূহ</span> </a></li>
   
    <li class="aside-nav-item {{ Request::is('owner/panel/drivers*') ? 'active' : '' }}"><a href="/owner/panel/drivers"><i class="icofont icofont-steering"></i> <span class="nav-text">চালকগন</span> </a></li>
    

    <li class="aside-nav-item {{ Request::is('owner/panel/invoices*') ? 'active' : '' }}"><a href="/owner/panel/invoices"><i class="icofont icofont-attachment"></i> <span class="nav-text">চালানপত্র সমূহ</span> </a></li> 

    <li class="aside-nav-item {{ Request::is('owner/panel/transactions*') ? 'active' : '' }}"><a href="/owner/panel/transactions"><i class="icofont icofont-money"></i> <span class="nav-text">লেনদেন</span> </a></li>

    <li class="aside-nav-item {{ Request::is('owner/panel/contact-us*') ? 'active' : '' }}"><a href="/owner/panel/contact-us"><i class="icofont icofont-comment"></i> <span class="nav-text">যোগাযোগ</span> </a></li>  
      
    <li class="aside-nav-item {{ Request::is('owner/panel/profile*') ? 'active' : '' }}"><a href="/owner/panel/profile"><i class="icofont icofont-user-alt-4"></i> <span class="nav-text">প্রোফাইল</span> </a></li>
    
    <li class="aside-nav-item {{ Request::is('owner/panel/settings*') ? 'active' : '' }}"><a href="/owner/panel/settings/password"><i class="icofont icofont-settings"></i> <span class="nav-text">সেটিংস</span> </a></li>

    <li class="aside-nav-item"><a href="https://bit.ly/2X1YAeQ" target="_blank"><i class="icofont icofont-youtube-play"></i> <span class="nav-text">ভিডিও টিউটোরিয়াল</span> </a></li>
</ul>