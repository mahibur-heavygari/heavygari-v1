@extends('dashboards.layout')

@section('menu')
    @include('owner.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">সেটিংস</h1>
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link active" href="#">পাসওয়ার্ড পরিবর্তন</a>
        </li>        
    </ul>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">            
            <div class="wagon-body">
                <form class="" method="POST" enctype="multipart/form-data" action="/owner/panel/settings/password" >
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('old_password')!=null) has-danger @endif">
                                <label for="old_password" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">বর্তমান পাসওয়ার্ড</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="password" value="" id="old_password" name="old_password">
                                    <div class="form-control-feedback">@if($errors->first('old_password')!=null) {{ $errors->first('old_password')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('password')!=null) has-danger @endif"">
                                <label for="password" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নতুন পাসওয়ার্ড</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="password" value="" id="password" name="password">
                                    <div class="form-control-feedback">@if($errors->first('password')!=null) {{ $errors->first('password')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('password_confirmation')!=null) has-danger @endif"">
                                <label for="password_confirmation" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">পাসওয়ার্ড নিশ্চিত করুন</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="password" value="" id="password_confirmation" name="password_confirmation">
                                    <div class="form-control-feedback">@if($errors->first('password_confirmation')!=null) {{ $errors->first('password_confirmation')}} @endif</div>
                                </div>
                            </div>                                                                        

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা করুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop