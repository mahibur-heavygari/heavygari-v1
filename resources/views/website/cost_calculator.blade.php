@extends('website.layout')

@section('content-body')

<div class="main-container">
    <div class="main-content">
        <div class="content-body">
            <div class="container">
                <div class="banner-negative-top-right">
                    <div class="row">
                        <div class="col-md-6 col-xl-5">
                            <div class="booking-form-card">
                                <ul class="confrim-list">
                                
                                    <li>
                                        <span>গাড়ীর ধরণ :</span>
                                        <span>{{$vehicle_type->title}}</span>
                                    </li>

                                    <li>
                                        <span>ধারণক্ষমতা:</span>
                                        <span>{{$booking_fields['capacity']}} {{$vehicle_type->capacityType->title}}</span>
                                    </li>

                                    <li>
                                        <span>পিক-আপের ঠিকানা:</span>
                                        <span>
                                            {{$booking_fields['from_address']}}
                                        </span>
                                    </li>
                                    
                                    <li>
                                        <span>গন্তব্যের ঠিকানা:</span>
                                        <span>
                                            {{$booking_fields['to_address']}}
                                        </span>
                                    </li>
                                    
                                    <li>
                                        <span>দূরত্ব:</span>
                                        <span>
                                            {{$route['distance']}} km
                                        </span>
                                    </li>

                                    <li>
                                        <span>যাত্রার ধরণ:</span>
                                        <span>
                                           {{$booking_fields['trip_type']}}
                                        </span>
                                    </li>
                                </ul>

                                <hr />

                                @if($fare_breakdown['fare_breakdown']['discount']>0)
                                    <h2 class="your-cost text-center">আপনার খরচঃ <strike>{{$fare_breakdown['total_cost']+$fare_breakdown['fare_breakdown']['discount']}}</strike>&nbsp;{{$fare_breakdown['total_cost']}} টাকা &nbsp;<span class="badge badge-success home-discount-pirce">{{$fare_breakdown['fare_breakdown']['discount']}} টাকা ডিসকাউন্ট </span></h2>
                                @else
                                    <h2 class="your-cost text-center">আপনার খরচঃ   টাকা. {{$fare_breakdown['total_cost']}}</h2>
                                @endif

                            </div>
                        </div>
                        <div class="col-md-6 col-xl-7">
                            <div class="map-block map-booking">
                                <iframe allowfullscreen="" frameborder="0" height="520" src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking_fields['from_lat']}},{{$booking_fields['from_lon']}}&destination={{$booking_fields['to_lat']}},{{$booking_fields['to_lon']}}" style="border:0" width="800">
                                </iframe>
                                <!--<div class="map-block-overlay"></div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
