@extends('website.layout')

@section('header')
<link rel="stylesheet" href="/dashboard/plugins/datatable/css/dataTables.bootstrap4.min.css">
@stop

@section('content-body')
<div style="background-color: black; height: 10%;"></div>
<div class="section hero-authorize-reg">
    <div class="container text-center">
        <h1 class="hero-heading mb-4">Register with HeavyGari for a chance to earn an extra income. As an authorized registration point (ARP), earn every time your shop registers a vehicle for HeavyGari.</h1>
        <h2 class="color-primary">A great opportunity to earn an extra income without any investment. </h2>
    </div>
</div>
<div class="main-container">
    <div class="main-content">
        <div class="content-body py-0">
            <div>
                <div class="s-flex -left-image">
                    <div class="s-image" style="background-image: url('{{URL("website/images/bg/7.jpg")}}');"></div>
                    <div class="s-content-flow d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 ml-md-auto">
                                    <div class="s-content">
                                        <h3 class="s-title">Requirements to become HeavyGari’s Authorized Registration Point (ARP)</h3>
                                        <ul class="s-steps-list">
                                            <li>
                                                <div class="s-number">1</div>
                                                <div class="s-denote">
                                                    Your shop has to be a telecom, computer, photocopy or a general store.
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">2</div>
                                                <div class="s-denote">
                                                     The shop owner has to have a valid trade license and a National ID card.
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">3</div>
                                                <div class="s-denote">
                                                    The shop owner has to have a valid bank account or a Bkash number.
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="s-flex -right-image">
                    <div class="s-image" style="background-image: url('{{URL("website/images/bg/8.jpg")}}');"></div>
                    <div class="s-content-flow d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="s-content">
                                        <h3 class="s-title">To-do when a shop becomes HeavyGari’s Authorized Registration Point (ARP)</h3>
                                        <ul class="s-steps-list">
                                            <li>
                                                <div class="s-number">1</div>
                                                <div class="s-denote">
                                                    The shop has to display all materials provided by HeavyGari (Banners, Stickers, Leaflets).
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">2</div>
                                                <div class="s-denote">
                                                    After showing interested vehicle owners HeavyGari’s tutorial video, the shop owners have to help them with the registration process.
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">3</div>
                                                <div class="s-denote">
                                                   The vehicle owners must fill out a registration form. You will have to help them to fill out that form, if they need any.
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">4</div>
                                                <div class="s-denote">
                                                    In order to claim your commission- you have to keep a log of how many vehicle owners you registered and send it to us.
                                                    <!--কমিশন পেতে- রেজিস্টার্ড গাড়ী মালিকদের বিবরণ লগবুকে সংরক্ষণ করে, মাসের শেষে তা আমাদের কাছে পাঠাতে হবে।-->
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s-customer-card">
                <div class="container text-center">
                    <h4>For any further details please call our customer care number:</h4>
                    <h3 class="color-primary mb-0">01909222777</h3>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <div class="card-enterprises">
                        <div class="card-image">
                            <img class="img-fluid" src="{{URL("website/images/44.png")}}" alt="...">
                        </div>
                        <div class="card-body">
                            <h2 class="card-title">Find Your Nearest Authorized Registration Point </h2>
                            <p>
                                We have set up Authorized Registration Points all over Bangladesh. These have been set up in order to assist you and make your registration with Heavygari absolutely hassle free. Please click below to find your nearest Authorized Registration Point
                            </p>
                            <a href="{{URL('/authorized-points')}}" class="btn btn-primary -site card-btn text-uppercase">More Details <svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right"></i> --></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="/dashboard/plugins/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    //data tables
    $('#example').DataTable();

    $('#searchButton').on('click', function() {
        searchAuthorizedPoint();
    });
});

function searchAuthorizedPoint() {
    var arp_point = $('#arp_point').val();
    var url = '/authorized-points?arp_point='+arp_point;
    $(location).attr('href', url);
}

</script>
@stop
