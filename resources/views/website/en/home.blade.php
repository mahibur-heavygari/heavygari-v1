@extends('website.layout')

@section('content-body')
<section class="section hero on-dark" style="background-image: url('{{URL("website/images/bg/Heavygari-updated.png")}}');">
    <div class="container image-wrap">
        <div class="row">
            <div class="col-md-12 col-lg-12 ml-auto mr-auto">
                <div class="hero-content text-center">
                    <h1 class="english" style="font-family: 'Roboto Slab', serif;">
                        Rent&nbsp;Vehicle&nbsp;Instantly
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-form hero-form-absolute" style="background-color: transparent; padding: 54px;">
        <form action="/cost-calculator" method="POST">
            {{ csrf_field() }}
            <ul class="hero-form-list">                
                <li class="hf-list-small">
                    <select id="from_point" name="from_point" class="custom-select @if($errors->first('from_point')!=null) is-invalid @endif">
                        <option selected="" value="">
                            Pickup
                        </option>
                        @foreach($points as $point)
                            <option @if(old('from_point')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                        @endforeach
                    </select>
                </li>
                <li>
                    <input id="from_address" name="from_address" type="text" class="form-control map_field @if($errors->first('from_address')!=null) is-invalid @endif" value="{{old('from_address')}}" placeholder="Address">
                    <input name="from_lat" id="from_lat" type="hidden" value="{{old('from_lat')}}">
                    <input name="from_lon" id="from_lon" type="hidden" value="{{old('from_lon')}}">         
                </li>
                <li class="hf-list-small">
                    <select id="to_point" name="to_point" class="custom-select @if($errors->first('to_point')!=null) is-invalid @endif" aria-describedby="addon-to">
                        <option selected="" value="">
                            Destination
                        </option>
                        @foreach($points as $point)
                            <option @if(old('to_point')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                        @endforeach
                    </select>
                </li>
                <li>
                    <input id="to_address" name="to_address" type="text" class="form-control map_field @if($errors->first('to_address')!=null) is-invalid @endif" value="{{old('to_address')}}" placeholder="Address">
                    <input name="to_lat" id="to_lat" type="hidden" value="{{old('to_lat')}}">
                    <input name="to_lon" id="to_lon" type="hidden" value="{{old('to_lon')}}">                   
                </li>
                <li>
                    <select class="custom-select @if($errors->first('vehicle_type')!=null) is-invalid @endif" id="vehicle_type" name="vehicle_type">
                        <option selected="" value="">
                            Vehicle Type
                        </option>
                       @foreach($vehicle_types as $vehicle_type)
                            <option @if(old('vehicle_type')==$vehicle_type->id) selected @endif value="{{$vehicle_type->id}}">{{$vehicle_type->title}}</option>
                        @endforeach
                    </select>
                </li>
                <li>
                    <input class="form-control @if($errors->first('capacity')!=null) is-invalid @endif" id="capacity" name="capacity" placeholder="{{old('capacity_label', 'Capacity')}}" type="text" readonly="" style="background-color: white;">
                    </input>
                </li>                              
                <li>
                    <select class="custom-select @if($errors->first('trip_type')!=null) is-invalid @endif" id="trip_type" name="trip_type">
                        <option selected="" value="">
                            Trip Type
                        </option>                       
                       <option value="single">একমুখী</option>
                       <option value="round">যাওয়া আসা</option>
                    </select>
                </li>
                <li>
                    <input id="mobile_no" name="mobile_no" class="form-control" placeholder="Mobile" type="text">
                    </input>
                </li>
                <li class="hf-list-small">
                    <button class="btn btn-primary btn-block">
                        Estimated Fare
                    </button>
                </li>
            </ul> 
        </form>
    </div>
</section>
<main>
    <div class="section" id="home">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-8">
                    <ul class="steps-list">
                        <li>
                            <div class="step-number">
                                1
                            </div>
                            <div class="step-denote">
                                <h4 class="sd-title">
                                    Book a vehicle with HeavyGari all over Bangladesh, instantly
                                </h4>
                                <div>
                                    Choose your pickup and drop off address, select the desired vehicle and a convenient time, we’ll take care of the rest
                                </div>
                                <div class="download-app-section">
                                    <h5>
                                        Download the Heavygari app on
                                    </h5>
                                    <div class="hero-btns d-flex flex-column flex-md-row">
                                        <a class="mr-3" href="https://itunes.apple.com/us/app/heavygari/id1353386111?mt=8&ign-mpt=uo%3D4">
                                            <img alt="App Store" src="{{URL::asset('website/images/download-on-the-app-store.png')}}" width="130">
                                            </img>
                                        </a>
                                        <a href="https://play.google.com/store/apps/details?id=com.heavygari.heavygaricustomer">
                                            <img alt="Google play" src="{{URL::asset('website/images/google-play-badge-300x89.png')}}" width="130">
                                            </img>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </li>
                        <li>
                            <div class="step-number">
                                2
                            </div>
                            <div class="step-denote">
                                <h4 class="sd-title">
                                  Real time tracking
                                </h4>
                                <div>
                                    Track the movement of your particulars accross the country with our app.
                                    <a data-toggle="modal" href="#modalTrack" role="button">
                                        TRACK ORDER HERE
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="step-number">
                                3
                            </div>
                            <div class="step-denote">
                                <h4 class="sd-title">
                                    Rate your experience
                                </h4>
                                <div>
                                    Once your goods are delivered, please provide your valuable feedback and share your experience with our service. We’d love to hear it!
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-5 col-lg-4 align-self-md-center">
                    <div class="app-slider-image-wrap float-md-right mt-small-40">
                        <div class="app-image">
                            <div class="app-slider-image">
                                <div class="item-image item">
                                    <img alt="..." src="{{URL::asset('website/images/app-slider/1.png')}}">
                                    </img>
                                </div>
                            </div>
                        </div>
                        <img alt="..." class="video-background" src="{{URL::asset('website/images/iphone.svg')}}">
                        </img>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-how-wrok bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-4 offset-lg-1">
                    <div class="app-slider-image-wrap mb-small-40">
                        <div class="app-image">
                            <div class="app-slider-image">
                                <div class="app-slider-video">
                                    <video autoplay="autoplay" height="240" loop="" width="320">
                                        <source src="{{URL::asset('website/images/customer-booking.MP4')}}" type="video/mp4">
                                        </source>
                                    </video>
                                </div>
                            </div>
                        </div>
                        <img alt="..." class="video-background" src="website/images/iphone.svg">
                        </img>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-header">
                        <h2 class="section-heading">
                            How it works?
                        </h2>
                    </div>
                    <ul class="how-work-list">
                        <li>
                            <div class="h-work-list-image">
                                <img alt="..." src="{{URL::asset('website/images/work/01.png')}}">
                                </img>
                            </div>
                            <div class="h-work-text">
                                <h5>
                                    The customer creates a booking by choosing a vehicle, when and where to pickup & drop, particular details and recipient info.
                                </h5>
                            </div>
                        </li>
                        <li>
                            <div class="h-work-list-image">
                                <img alt="..." src="{{URL::asset('website/images/work/02.png')}}">
                                </img>
                            </div>
                            <div class="h-work-text">
                                <h5>
                                    After the booking has been accepted, customer can view and call the assigned driver and get a PDF copy of the booking invoices.
                                </h5>
                            </div>
                        </li>
                        <li>
                            <div class="h-work-list-image">
                                <img alt="..." src="{{URL::asset('website/images/work/03.png')}}">
                                </img>
                            </div>
                            <div class="h-work-text">
                                <h5>
                                    When a booking is created every relevant drivers and owners are notified via SMS & notificaitons.
                                </h5>
                            </div>
                        </li>
                        <li>
                            <div class="h-work-list-image">
                                <img alt="..." src="{{URL::asset('website/images/work/04.png')}}">
                                </img>
                            </div>
                            <div class="h-work-text">
                                <h5>
                                    The driver delivers the particulars to the designated recipient set by the customer and completes the trip.
                                </h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="feature" class="section section-featured">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 ml-auto mr-auto">
                        <div class="section-header text-center">
                            <h2 class="section-heading">Features of Heavygari</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($features as $feature)
                    <div class="col-md-6 col-lg-6">
                        <div class="card-featured">
                            <div class="c-featured-image">
                                <img src="{{Storage::url($feature->icon)}}" alt="...">
                            </div>
                            <div class="c-featured-denote">
                                @if($feature->comming_soon)
                                    <span class="text-coming-soon-blink">Coming soon</span>
                                @endif
                                <h4>{{$feature->title_en}}</h4>
                                <p>{{$feature->text_en}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    <div class="section bg-gray section-choose">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 ml-auto mr-auto">
                    <div class="section-header text-center">
                        <h2 class="section-heading">
                            Why Choose Heavygari
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-offer">
                        <div class="c-offer-image">
                            <img alt="..." src="{{URL::asset('website/images/offer_1.png')}}">
                            </img>
                        </div>
                        <div class="c-offer-denote">
                            <h4>
                                Hassle-Free Booking
                            </h4>
                            <p>
                                Book a vehicle. Whenever you need, wherever you need
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-offer">
                        <div class="c-offer-image">
                            <img alt="..." src="{{URL::asset('website/images/offer_2.png')}}">
                            </img>
                        </div>
                        <div class="c-offer-denote">
                            <h4>
                                Transparent Pricing
                            </h4>
                            <p>
                                Enjoy the most affordable rates in the country with our transparent pricing
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-offer">
                        <div class="c-offer-image">
                            <img alt="..." src="{{URL::asset('website/images/offer_3.png')}}">
                            </img>
                        </div>
                        <div class="c-offer-denote">
                            <h4>
                                Realtime Tracking
                            </h4>
                            <p>
                                Track the movement of your particulars across the country with our app
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-offer">
                        <div class="c-offer-image">
                            <img src="{{URL::asset('website/images/offer_6.png')}}" alt="...">
                        </div>
                        <div class="c-offer-denote">
                            <h4>Quick</h4>
                            <p>Immediate Response, Minimal Waiting Time</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-offer">
                        <div class="c-offer-image">
                            <img alt="..." src="{{URL::asset('website/images/offer_4.png')}}">
                            </img>
                        </div>
                        <div class="c-offer-denote">
                            <h4>
                                Safe and Reliable Vehicles
                            </h4>
                            <p>
                             Superior safety ensured with our team of verified &amp; trained partners
                            </p>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-3 offset-md-3">
                        <div class="card-offer">
                            <div class="c-offer-image">
                                <img src="website/images/offer_4.png" alt="...">
                            </div>
                            <div class="c-offer-denote">
                                <h4>Real time driver spot </h4>
                                <p>See when drivers are available </p>
                            </div>
                        </div>
                    </div> -->
                <div class="col-md-4">
                    <div class="card-offer">
                        <div class="c-offer-image">
                            <img alt="..." src="{{URL::asset('website/images/offer_5.png')}}">
                            </img>
                        </div>
                        <div class="c-offer-denote">
                            <h4>
                                24/7 Customer Support
                            </h4>
                            <p>
                                Contact us with any issues, anytime
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="owner">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 ml-auto mr-auto">
                    <div class="section-header text-center">
                        <h2 class="section-heading">
                          Own a Vehicle? Be a part of Heavygari!
                        </h2>
                        <p>
                            It's not everyday you get to do something for yourself and others at the same time. Sign up today and be a part of our growing Heavygari Community.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-manage-content-block -left-denote clearfix">
            <div class="manage-conent-denote">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                No more waiting on the stand
                            </h2>
                            <p>
                                By partnering with Heavygari, you have a steady stream of trips with a minimum assured income and added incentives, so that there is no waiting and idle time at the stand!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="manage-content-image">
                <img alt="..." src="{{URL::asset('website/images/block/blocks1.png')}}">
                </img>
            </div>
        </div>
        <div class="section-manage-content-block -right-denote clearfix">
            <div class="manage-content-image">
                <img alt="..." src="{{URL::asset('website/images/block/map-stripe1.png')}}">
                </img>
            </div>
            <div class="manage-conent-denote">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 offset-md-6 align-self-md-center">
                            <h2 class="mt-5">
                                No more bargaining, Standard Rates
                            </h2>
                            <p>
                                The rates and calculation methods are standardized and completely transparent. No more wasting time in fixing the rates for every trip.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section business-bg" id="heavygari-business">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-4 offset-lg-1 ml-auto mr-auto">
                    <div class="section-header text-center">
                        <h2 class="section-heading">
                          HeavyGari For Business
                        </h2>
                        <p>
                            We will provide your business with the most convenient way to make your transportation easier by tailor making the service for your business,we will cater your business with all kind of transportation need and give you the best suited service available .
                            For a customised experience of HeavyGari Business fillup the form ,we shall contact you soon
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <form id="heavygari_for_business" action="{{URL('/business-request')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" name="company_name" id="company_name" value="" class="form-control" placeholder="Company Name" required="">
                    </div>
                    <div class="form-group">
                        <input type="text" name="manager_name" id="manager_name" value="" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="designation" id="designation" value="" class="form-control" placeholder="Designation">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" id="phone" value="" class="form-control" placeholder="Mobile" required="">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" id="email" value="" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" rows="5" class="form-control" placeholder="Message"></textarea>
                    </div>
                    <div class="g-recaptcha" data-sitekey="{{config('heavygari.google_recaptcha.site_key')}}"></div>
                    <br>
                    <div class="clearfix">
                        <button type="submit" id="submit" class="btn btn-primary btn-site">Submit</button>
                         <span class="float-right" id="submit-response" style="color: green;display: none;">Thank you for submitting your details.</span> 
                    </div>
                </form>
                </div>
            </div>
        </div>
   </div>
    <div class="section bg-gray" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 ml-auto mr-auto">
                    <div class="section-header text-center">
                        <h2 class="section-heading">
                            Heavygari for you
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="slider-nav slider-vehicle-type-nav">
                <div class="slide-item -item-bus">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Bus
                    </div>
                </div>
                <div class="slide-item -item-bus-ac">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Bus (AC)
                    </div>
                </div>
                <div class="slide-item -item-truck">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Truck
                    </div>
                </div>
                <div class="slide-item -item-cover-truck">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Cover Truck 
                    </div>
                </div>
                <div class="slide-item -item-pickup-van">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Pickup Van
                    </div>
                </div>
                <div class="slide-item -item-mini-pickup-van">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Mini Pickup Van
                    </div>
                </div>
                <div class="slide-item -item-tanker-truck">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                          Tanker Truck
                    </div>
                </div>                
                <div class="slide-item -item-mini-bus">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                       Mini Bus
                    </div>
                </div>
                <div class="slide-item -item-micro-bus">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                        Micro Bus
                    </div>
                </div>
                <div class="slide-item -item-ambulance">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Ambulance
                    </div>
                </div>                
                <div class="slide-item -item-xl-truck">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            XL Truck
                    </div>
                </div>
                <div class="slide-item -item-mini-truck">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Mini Truck
                    </div>
                </div>
                <div class="slide-item -item-freezer-truck">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Freezer Truck
                    </div>
                </div>
                <div class="slide-item -item-coaster-bus">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Coaster Bus
                    </div>
                </div>                
                <div class="slide-item -item-container-truck-20ft">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Trailer Truck 2Xl
                    </div>
                </div>
                <div class="slide-item -item-container-truck-40ft">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Trailer Truck 3XL
                    </div>
                </div>
                <div class="slide-item -item-car">
                    <div class="vehicle-type-image">
                    </div>
                    <div class="vehicle-type-text">
                            Car
                    </div>
                </div>
            </div>
            <div class="slider-for slider-vehicle-type-content">

            <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/bus.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Bus
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                  Capacity: 
                                            </span>
                                            <span>
                                                    36 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    5000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    70 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/ac-bus.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Bus (AC)
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    36 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    5000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    130 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/truck.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Truck
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    14000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    3000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                   42 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                       Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                       Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/cover-truck.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Cover Truck
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    12000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    3500
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    45 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/pickup-van.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Pickup Van
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                   Capacity:
                                            </span>
                                            <span>
                                                    5000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    1500 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    48 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                       Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                       Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/mini-pickup-van.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Mini Pickup Van
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity: 
                                            </span>
                                            <span>
                                                    2000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    700 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    40 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/tanker-truck.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Tanker Truck
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    5000 Ltr
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                   Tk.
                                            </span>
                                            <span class="base-value">
                                                    2500 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    38 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/mini-bus.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                           Mini Bus
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                  Capacity: 
                                            </span>
                                            <span>
                                                    36 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    5000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    70 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/micro-bus.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Micro Bus
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    10 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    1000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    38 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/ambulance.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Ambulance
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity: 
                                            </span>
                                            <span>
                                                    3 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    1000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    40 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/xl-truck.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            XL Truck
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    18000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    4000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                Tk
                                            </span>
                                            <span class="base-value">
                                                54 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/mini-truck.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Mini Truck
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                    8000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                 <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    3000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    48 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                       Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                       Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/freezer-truck.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Freezer Truc
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity: 
                                            </span>
                                            <span>
                                                    8000 Kg
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                 <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    6000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                100 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/coaster-bus.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Coaster Bus
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                   18 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                 <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    3500 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                   70 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                       Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/container-truck-20-feet.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Trailer Truck 2XL
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity: 
                                            </span>
                                            <span>
                                                    20 Feet
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    5000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                           Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                40 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/container-truck-40-feet.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Trailer Truck 3XL
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity: 
                                            </span>
                                            <span>
                                                    40 Feet
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    7000 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    50 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                       Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="" class="img-fluid" src="{{URL::asset('website/images/vehicle/car.png')}}">
                            </img>
                        </div>
                        <div class="col-md-6">
                            <div class="vehicleype-content">
                                <div class="vehicle-name-capacity-section clearfix">
                                    <h4 class="float-md-left">
                                            Car
                                    </h4>
                                    <ul class="vehicle-info-lists float-md-right">
                                        <li>
                                            <span>
                                                    Capacity:
                                            </span>
                                            <span>
                                                   4 Person
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                 <h5 class="text-coming-soon-blink">Coming Soon</h5>

                                <ul class="vehicle-pricing">
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk.
                                            </span>
                                            <span class="base-value">
                                                    50 
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                               Base Fare
                                        </span>
                                    </li>
                                    <li>
                                        <div class="vehicle-value">
                                            <span class="tk-icon">
                                                    Tk
                                            </span>
                                            <span class="base-value">
                                                    18 /km
                                            </span>
                                        </div>
                                        <span class="vp-label">
                                                Standard Rate Per Km
                                        </span>
                                    </li>
                                </ul>
                                <ul class="vehicle-conditions-explained">
                                    <li>
                                        Trips less than 120 kms has a base fare and time charge which are applicable along with the standard rate per km. This is subject to change at any time
                                    </li>
                                    
                                    <li>
                                        Trips greater than 120 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                    <li>
                                       Trips greater than 400 kms has a set rate per km according to the type of vehicle booked. This is subject to change at any time. Price per kilometre may multiply due to demand, season, weather, situation in certain areas.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div><span class="small">*All pictures shown are for illustration purposes only. Actual vehicles may vary according to size and availability.</span></div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="card-enterprises">
                <div class="card-image">
                    <img alt="..." class="img-fluid" src="{{URL::asset('website/images/44.png')}}">
                    </img>
                </div>
                <div class="card-body">
                    <h2 class="card-title">
                        Find Your Nearest Authorized Registration Point
                    </h2>
                    <p>
                       We have set up Authorized Registration Points all over Bangladesh. These have been set up in order to assist you and make your registration with Heavygari absolutely hassle free. Please click below to find your nearest Authorized Registration Point
                    </p>
                    <a class="btn btn-primary -site card-btn text-uppercase" href="{{URL('/authorized-points')}}">
                        Know More
                        <i class="fas fa-angle-right">
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</main>
<section class="section section-featureIn bg-gray">
    <div class="container">
        <h6 class="featureIn-title">Featured on</h6>
        <ul class="featureIn-brands">
                <li>
                    <a href="https://www.thedailystar.net/bytes/news/look-heavygari-1716430" target="_blank">
                        <img src="{{URL::asset('website/images/featurein/DS.png')}}" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://www.prothomalo.com/technology/article/1584162/%E0%A6%AF%E0%A6%BE%E0%A6%A8%E0%A6%AC%E0%A6%BE%E0%A6%B9%E0%A6%A8-%E0%A6%A1%E0%A6%BE%E0%A6%95%E0%A6%BE%E0%A6%B0-%E0%A6%A8%E0%A6%A4%E0%A7%81%E0%A6%A8-%E0%A6%85%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%AA" target="_blank">
                        <img src="{{URL::asset('website/images/featurein/PA.png')}}" alt="">
                    </a>
                </li>
                <li>
                    <a href="http://www.bd-pratidin.com/corporate-corner/2019/03/26/411185?fbclid=IwAR2LV0_kz126-vlZNCb5MQbW7UosiefBDV2Gg5g3_J0yxSr_AenKJxds7yg" target="_blank">
                        <img src="{{URL::asset('website/images/featurein/bd-daily.png')}}" alt="">
                    </a>
                </li>
                <li>
                    <a href="http://thefinancialexpress.com.bd/trade/heavygari-commercial-vehicle-renting-is-just-a-click-away-1554622149?fbclid=IwAR22zcdxnmsdfdZ-OjLhEGCgl0v63AjNyfUROINEpq6klGVQi8K2AMlAXkU" target="_blank">
                        <img src="{{URL::asset('website/images/featurein/FE.png')}}" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://m.bdnews24.com/bn/detail/tech/1613668" target="_blank">
                        <img src="{{URL::asset('website/images/featurein/bdnews.png')}}" alt="">
                    </a>
                </li>
        </ul>
    </div>
</section>
<!-- <div class="section section-feature-in">
    <div class="container">
        
        <div class="feature-in-row">
            <div class="feature-in-appended">
                <h5 class="feature-in-title">Featured in</h5>
            </div>
            <div class="feature-in-brands">
                <div>
                    <ul class="brands-list">
                        <div class="col-md-3 feature-margin">
                           <a href="https://www.thedailystar.net/bytes/news/look-heavygari-1716430" target="_blank"><img src="{{URL::asset('website/images/featurein/DS.png')}}" alt="..." class="img-fluid"></a>
                        </div>
                        <div class="col-md-3 pa-paddding">
                            <a href="https://www.prothomalo.com/technology/article/1584162/%E0%A6%AF%E0%A6%BE%E0%A6%A8%E0%A6%AC%E0%A6%BE%E0%A6%B9%E0%A6%A8-%E0%A6%A1%E0%A6%BE%E0%A6%95%E0%A6%BE%E0%A6%B0-%E0%A6%A8%E0%A6%A4%E0%A7%81%E0%A6%A8-%E0%A6%85%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%AA" target="_blank"><img src="{{URL::asset('website/images/featurein/PA.png')}}" alt="..." class="img-fluid"></a>
                        </div>
                        <div class="col-md-3 feature-padding">
                            <a href="http://www.bd-pratidin.com/corporate-corner/2019/03/26/411185?fbclid=IwAR2LV0_kz126-vlZNCb5MQbW7UosiefBDV2Gg5g3_J0yxSr_AenKJxds7yg" target="_blank"><img src="{{URL::asset('website/images/featurein/bd-daily.png')}}" alt="..." class="img-fluid"></a>
                        </div>
                        <div class="col-md-3 feature-padding feature-padding-top">
                            <a href="http://thefinancialexpress.com.bd/trade/heavygari-commercial-vehicle-renting-is-just-a-click-away-1554622149?fbclid=IwAR22zcdxnmsdfdZ-OjLhEGCgl0v63AjNyfUROINEpq6klGVQi8K2AMlAXkU" target="_blank"><img src="{{URL::asset('website/images/featurein/FE.png')}}" alt="..." class="img-fluid"></a>
                        </div>
                     </ul>
                </div>
            </div>
        </div>
        <div class="feature-in-row padding-top-feature">
            <div class="feature-in-appended">
                <h5 class="feature-in-title feature-padding-left"></h5>
            </div>
            <div class="feature-in-brands">
                <div>
                    <ul class="brands-list">
                        <div class="col-md-3">
                           <a href="https://m.bdnews24.com/bn/detail/tech/1613668" target="_blank"><img src="{{URL::asset('website/images/featurein/bdnews.png')}}" alt="..." class="img-fluid img-width"></a>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> -->
@stop

@section('footer')
<script type="text/javascript">
$(document).ready(function() {
    $(function() {
        var type_capacity = {!! json_encode($capacity_types, JSON_PRETTY_PRINT) !!};
        console.log(type_capacity);

        $('#vehicle_type').change(function() {
            var selected = $(this).find("option:selected").text();
            $("#capacity").attr("placeholder", "Capacity ("+type_capacity[selected]['title']+")");
            $("#capacity").val(type_capacity[selected]['capacity']);            
        });
    });

    $('#heavygari_for_business').on('submit', function(e){
        e.preventDefault();

        $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });

        $.ajax({
            url: '/business-request',
            type: 'POST',
            data: $('#heavygari_for_business').serialize(),
            dataType: 'json',
            success: function (response){
                if(response.success){
                    $('#submit-response').show();
                    $('#submit').attr("disabled", true);

                }else{
                    $('#submit-response').hide();
                }
            }
        });
    })

    
});

function initMap() {
    var fromInput = document.getElementById('from_address');
    var toInput = document.getElementById('to_address');

    from_autocomplete = new google.maps.places.Autocomplete(fromInput);
    from_autocomplete.setComponentRestrictions({
        'country': 'BD'
    });
    getLatLon(from_autocomplete, 'from');

    to_autocomplete = new google.maps.places.Autocomplete(toInput);
    to_autocomplete.setComponentRestrictions({
        'country': 'BD'
    });
    getLatLon(to_autocomplete, 'to');
    
    // get lat, lon from user's place
    function getLatLon(location, type) {
        location.addListener('place_changed', function() {
            var place = location.getPlace();
            if (!place.geometry) {
                // Place was not found
                document.getElementById(type + '_lat').value = '';
                document.getElementById(type + '_lon').value = '';
                window.alert("No details available for input: '" + place.name + "'");
                return;
            } else {
                document.getElementById(type + '_lat').value = place.geometry.location.lat();
                document.getElementById(type + '_lon').value = place.geometry.location.lng();
            }
        });
    }
}
</script>
<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key={{config('heavygari.google_maps.api_key')}}&libraries=places&callback=initMap">
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
@stop
