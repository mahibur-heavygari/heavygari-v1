<footer class="new-footer">
    <div class="container">
        <div class="text-center">
            <div class="footer-logo">
                <img width="120" src="{{URL::asset('website/images/logo-text.png')}}" alt="Heavygari">
            </div>
            <div class="footer-label">Follow Us</div>
            <div class="footer-socials d-flex justify-content-center">
                <li><a href="https://www.facebook.com/Heavygari-1970065606543494/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://twitter.com/heavygari" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <li><a href="https://www.linkedin.com/company/13669172/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCbK5eLsnc1SKgC-qe7RMTDw?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a></li>
                <li><a href="mailto:support@heavygari.com"><i class="fa fa-envelope"></i></a></li>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <span class="copy-info">&copy; 2018 Heavygari Technologies Ltd.</span>
            </div>
            <div class="col-md-8">
                <ul class="foo-nav text-md-right">
                    <li style="color: white;">Email <a href="mailTo:"> support@heavygari.com </a></li>
                    <li style="color: white;">Customer care <a href=""> +8801909222777 </a></li>
                    <li><a href="{{URL('page/toc')}}" style="color: white;">Terms of use</a></li>
                    <li><a href="{{URL('page/privacy-policy')}}" style="color: white;">Privacy</a></li>
                </ul>
            </div>
        </div>

        
    </div>
</footer>

<!-- Modal -->
<div class="modal fade modal-track" id="modalTrack" tabindex="-1" role="dialog" aria-labelledby="modalTrackLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTrackLabel">Track Booking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <input type="text" id="tracking_id" name="tracking_id" class="form-control" placeholder="Tracking Id">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="showTrackingButton" class="btn btn-primary btn-site">Track</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContactLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalContactLabel">Contact Us</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{URL('/contact-us')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" name="name" id="name" value="" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" id="phone" value="" class="form-control" placeholder="Mobile">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" id="email" value="" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" rows="5" class="form-control" placeholder="Message"></textarea>
                    </div>
                    <div class="g-recaptcha" data-sitekey="{{config('heavygari.google_recaptcha.site_key')}}"></div>
                    <br>
                    <div class="clearfix">
                        <button type="submit" class="btn btn-primary btn-site">Submit</button>
                        <span class="float-right">To find out more, please contact this number <a href="#"> 01909222777 </a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>