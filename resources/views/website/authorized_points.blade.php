@extends('website.layout')

@section('header')
<link rel="stylesheet" href="/dashboard/plugins/datatable/css/dataTables.bootstrap4.min.css">
@stop

@section('content-body')
<section class="section page-hero on-dark" style="background-image: url('{{URL("website/images/bg/6.jpg")}}');">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-10 ml-auto mr-auto">
                <div class="hero-content text-center">
                    <h1 class="hero-heading">List of HeavyGari Authorized Registration Point </h1>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="section">
        <div class="container">
            <div class="row">                
                <div class="col-md-6 ml-md-auto">
                    <form>
                        <div class="input-group mb-3">

                            <select id="arp_point" name="arp_point" class="custom-select @if($errors->first('arp_point')!=null) is-invalid @endif">
                                <option selected="" value="">
                                    --শহর নির্বাচন করুন--
                                </option>
                                @foreach($points as $point)
                                    <option @if(isset($arp_point) && $arp_point==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</option>
                                @endforeach
                            </select>

                            <!--<input type="text" class="form-control" id="arp_no" name="arp_no" placeholder="Enter ARP No" aria-label="Search..." aria-describedby="" value="@isset($arp_no){{$arp_no}}@endif">-->

                            <div class="input-group-append">
                                <button class="btn btn-primary btn-site" id="searchButton" type="button">Search</button>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>
            <div class="table-responsive table-site-primary">
                <table class="table table-listview">
                    <thead>
                        <tr>
                            <th class="text-left">ARP No.</th>
                            <th class="text-left">Point (City)</th>
                            <th class="text-center">Owner Name</th>
                            <th class="text-center">Shop Name</th>
                            <th class="text-center">Nature Of Business</th>
                            <th class="text-center">Address</th>
                            <th class="text-right">Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($list as $item)
                        <tr>
                            <td class="text-left">{{$item->arp_no}}</td>
                            <td class="text-left">{{$item->point->title}}</td>
                            <td class="text-center text-name">{{$item->owner_name}}</td>
                            <td class="text-center">{{$item->shop_name}}</td>
                            <td class="text-center">{{$item->nature_of_business}}</td>
                            <td class="text-center">{{$item->address}}</td>
                            <td class="text-right">{{$item->phone}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @if(!count($list))
            <div>
                আমরা বর্তমানে আপনার শহরে অথরাইজড রেজিস্ট্রেশন পয়েন্ট স্থাপনের কাজ করছিI অনুসন্ধানের জন্য, আমাদের কাস্টমার কেয়ার নম্বরে কল করুনI
            </div>
            @endif
        </div>
    </div>
</main>
@stop

@section('footer')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="/dashboard/plugins/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    //data tables
    $('#example').DataTable();

    $('#searchButton').on('click', function() {
        searchAuthorizedPoint();
    });
});

function searchAuthorizedPoint() {
    var arp_point = $('#arp_point').val();
    var url = '/authorized-points?arp_point='+arp_point;
    $(location).attr('href', url);
}

</script>
@stop
