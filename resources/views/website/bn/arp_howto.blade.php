@extends('website.layout')

@section('header')
<link rel="stylesheet" href="/dashboard/plugins/datatable/css/dataTables.bootstrap4.min.css">
@stop

@section('content-body')
<div style="background-color: black; height: 10%;"></div>
<div class="section hero-authorize-reg">
    <div class="container text-center">
        <h1 class="hero-heading mb-4">হেভিগাড়ীতে গাড়ি রেজিস্ট্রেশন করে বাড়তি আয়ের সুযোগ নিন। আমাদের অথরাইজড রেজিস্ট্রেশন পয়েন্ট হয়ে আয় করুন প্রতি রেজিস্ট্রেশনে।</h1>
        <h2 class="color-primary">ইনভেস্টমেন্ট না করেই বাড়তি আয়ের দারুণ সুযোগ! </h2>
    </div>
</div>
<div class="main-container">
    <div class="main-content">
        <div class="content-body py-0">
            <div>
                <div class="s-flex -left-image">
                    <div class="s-image" style="background-image: url('{{URL("website/images/bg/7.jpg")}}');"></div>
                    <div class="s-content-flow d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 ml-md-auto">
                                    <div class="s-content">
                                        <h3 class="s-title">অথোরাইজড রেজিস্ট্রেশন পয়েন্ট হতে নিয়মাবলী - </h3>
                                        <ul class="s-steps-list">
                                            <li>
                                                <div class="s-number">1</div>
                                                <div class="s-denote">
                                                    টেলিকম, কম্পিউটার কিংবা ফটোকপি দোকান অথবা জেনারেল স্টোর হতে হবে। 
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">2</div>
                                                <div class="s-denote">
                                                     দোকান মালিকের ট্রেড লাইসেন্স এবং জাতীয় পরিচয় পত্র থাকতে হবে।
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">3</div>
                                                <div class="s-denote">
                                                    ব্যাংক অ্যাকাউন্ট অথবা বিকাশ নং প্রয়োজন হবে। 
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="s-flex -right-image">
                    <div class="s-image" style="background-image: url('{{URL("website/images/bg/8.jpg")}}');"></div>
                    <div class="s-content-flow d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="s-content">
                                        <h3 class="s-title">অথরাইজড রেজিস্ট্রেশন পয়েন্ট হলে যা করণীয় - </h3>
                                        <ul class="s-steps-list">
                                            <li>
                                                <div class="s-number">1</div>
                                                <div class="s-denote">
                                                    হেভিগাড়ী প্রদত্ত মেটারিয়াল সবসময় ডিসপ্লেতে রাখতে হবে। 
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">2</div>
                                                <div class="s-denote">
                                                    গাড়ী মালিকদের টিউটোরিয়াল ভিডিও দেখানোর পর, রেজিস্ট্রেশন প্রক্রিয়ায় সহযোগিতা করতে হবে।
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">3</div>
                                                <div class="s-denote">
                                                   গাড়ির মালিকদের ফর্ম পূরণ  করতে হবে। আপনার কাজ হবে তাদের সহযোগিতা করা ।
                                                </div>
                                            </li>
                                            <li>
                                                <div class="s-number">4</div>
                                                <div class="s-denote">
                                                    কমিশন পেতে- রেজিস্টার্ড গাড়ী মালিকদের বিবরণ লগবুকে সংরক্ষণ করে তা আমাদের কাছে পাঠাতে হবে।
                                                    <!--কমিশন পেতে- রেজিস্টার্ড গাড়ী মালিকদের বিবরণ লগবুকে সংরক্ষণ করে, মাসের শেষে তা আমাদের কাছে পাঠাতে হবে।-->
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s-customer-card">
                <div class="container text-center">
                    <h4>আরো জানতে আমাদের কাস্টমার কেয়ারে যোগাযোগ করুন এই নাম্বারেঃ </h4>
                    <h3 class="color-primary mb-0">০১৯০৯২২২৭৭৭</h3>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <div class="card-enterprises">
                        <div class="card-image">
                            <img class="img-fluid" src="{{URL("website/images/44.png")}}" alt="...">
                        </div>
                        <div class="card-body">
                            <h2 class="card-title">আপনার নিকটস্থ অথরাইজড রেজিস্ট্রেশন পয়েন্ট সম্পর্কে জানুন </h2>
                            <p>
                                বাংলাদেশের সর্বত্র আমাদের অথরাইজড রেজিস্ট্রেশন পয়েন্ট আছে, যা হেভিগাড়ীতে আপনার রেজিস্ট্রেশন সহজসাধ্য করবে। অনুগ্রপূর্বক নিচে ক্লিক করে আপনার নিকটস্থ অথরাইজড রেজিস্ট্রেশন পয়েন্ট সম্পর্কে জেনে নিন।
                            </p>
                            <a href="{{URL('/authorized-points')}}" class="btn btn-primary -site card-btn text-uppercase">আরও জানুন <svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right"></i> --></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="/dashboard/plugins/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    //data tables
    $('#example').DataTable();

    $('#searchButton').on('click', function() {
        searchAuthorizedPoint();
    });
});

function searchAuthorizedPoint() {
    var arp_point = $('#arp_point').val();
    var url = '/authorized-points?arp_point='+arp_point;
    $(location).attr('href', url);
}

</script>
@stop
