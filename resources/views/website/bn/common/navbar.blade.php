<div class="dropdown dropdown-language-nav -arrow-off">
    <!-- <a class="dropdown-toggle" href="#" role="button" id="languageMobile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="ion-android-globe"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageMobile">
        <a class="dropdown-item" href="{{url('/?lang=en')}}">English</a>
        <a class="dropdown-item active" href="#">Bangla</a>
    </div> -->
    <div class="btn-group mr-2" role="group" aria-label="First group">
        <a class="btn btn-secondary" href="{{url('/?lang=en')}}" id="enButton">
         ENG
        </a>
        <a class="btn btn-secondary button-lang" href="#">
        বাংলা
        </a>
    </div>
</div>

<div id="navbarCollapsed" class="collapse navbar-collapse">
    <ul class="navbar-nav" role="tablist">
        <li class="nav-item">
            <a class="nav-link smooth-scroll"  href="{{URL('/?lang=bn#home')}}">হোম <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link smooth-scroll " href="{{URL('/?lang=bn#feature')}}">বৈশিষ্ট্যসমূহ</a>
        </li>
        <li class="nav-item">
            <a class="nav-link smooth-scroll " href="{{URL('/?lang=bn#owner')}}">মালিকগন</a>
        </li>
        <li class="nav-item">
            <a class="nav-link smooth-scroll "  href="{{URL('/?lang=bn#pricing')}}">মূল্যনীতি</a>
        </li>
         <li class="nav-item">
            <a class="nav-link smooth-scroll "  href="{{URL('/arp-howto?lang=bn')}}">অথরাইজড রেজিস্ট্রেশন পয়েন্ট</a>
        </li>                    
        <li class="nav-item"><a class="nav-link " role="button" data-toggle="modal" href="#modalTrack">ট্র্যাক অর্ডার</a></li>
        <li class="nav-item">
            <a class="nav-link smooth-scroll " href="{{URL('/?lang=bn#heavygari-business')}}">ব্যবসা</a>
        </li>
        <li class="nav-item"><a class="nav-link " role="button" data-toggle="modal" href="#modalContact">যোগাযোগ করুন</a></li>
    </ul>

    @if(!(isset($app_webview) && $app_webview==true))
    <ul class="navbar-nav navbar-nav-admin ml-auto">
        @if(Sentinel::getUser())
            <li class="nav-item dropdown -has-avater">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownLabel__signup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="{{Storage::url(Sentinel::getUser()->thumb_photo)}}" alt="...">
                  {{ Sentinel::getUser()->name}}
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLabel__signup">
                    <a class="dropdown-item "  href="/session/redirect-to-dashboard">Dashboard</a>
                    <a class="dropdown-item" href="/session/logout">Logout</a>
                </div>
            </li> 
        @else
            <li class="nav-item">
                <a href="{{url('/session/login')}}" class="nav-link btn btn-secondary btn-signin signup-font-color" style="color:white;">লগইন</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn btn-secondary btn-signin btn-signup" style="color:white;" href="#" id="dropdownLabel__signup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                সাইনআপ
                </a>
                <div class="dropdown-menu dropdown-signup" aria-labelledby="dropdownLabel__signup">
                    <a class="dropdown-item btn-signup-choose dropdown-signup-menu" href="{{url('/customer/register')}}">Customer</a>
                    <a class="dropdown-item btn-signup-choose dropdown-signup-menu" href="{{url('/owner/register')}}">Vehicle Owner</a>
                </div>
            </li>            
        @endif
        <li class="nav-item dropdown nav-item-languge">
            <div class="btn-group mr-2" role="group" aria-label="First group">
                <a class="btn btn-secondary" href="{{url('/?lang=en')}}" id="enButton">
                 ENG
                </a>
                <a class="btn btn-secondary button-lang" href="#">
                বাংলা
                </a>
            </div>
           <!--  <a class="nav-link dropdown-toggle nav-link-languge" href="#" id="dropdownLabel__languge" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ion-android-globe"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLabel__languge">
                <a class="dropdown-item" href="{{url('/?lang=en')}}">English</a>
                <a class="dropdown-item active" href="#">Bangla</a>
            </div> -->
        </li>
    </ul>
    @endif
</div>