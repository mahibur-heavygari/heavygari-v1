<style>
.Blink {
  animation: blinker 1.5s cubic-bezier(.5, 0, 1, 1) infinite alternate;  
}
@keyframes blinker {  
  from { opacity: 1; }
  to { opacity: 0; }
}
</style>
@extends('website.layout')

@section('header')
<link rel="stylesheet" href="{{URL('website/plugins/scrollbar/jquery.mCustomScrollbar.css')}}">
@stop

@section('content-body')
<div class="booking-page-map">
    <div class="booking-map-exam -bm-header">
        <div class="bm-toggle-cell">
            <button class="btn map-toggler -mt-open"><i class="ion-navicon"></i></button>
        </div>
        <div class="bm-title-cell">
            <h5>বুকিং #{{$booking->unique_id}} @if($booking->status=='ongoing') <i class="fa fa-circle text-danger Blink"></i>&nbsp; @endif</h5> 


        </div>
    </div>
    <div class="booking-map-content c-scroll">
        <div class="booking-form-card booking-confrim">
            <span class="small" id="last_update"></span>
            <hr />
            <div class="booking-map-exam -bm-body mb-3">                    
                <div class="bm-title-cell">
                    <h5>বুকিং #{{$booking->unique_id}}                    
                    @if($booking->status=='ongoing')<i class="fa fa-circle text-danger Blink"></i>&nbsp;@endif</h5>
                    <span class="mt-3 badge @if($booking->status=='completed') badge-primary @elseif($booking->status=='ongoing') badge-success @elseif($booking->status=='open') badge-info @elseif($booking->status=='upcoming') badge-primary  @elseif($booking->status=='cancelled') badge-danger @endif">{{$booking->status}}</span>
                </div>
                <div class="bm-toggle-cell">
                    <button class="btn map-toggler -mt-close"><i class="ion-android-close"></i></button>
                </div>
            </div>

            <ul class="confrim-list">
                <li>
                    <span>গ্রাহক:</span>
                    <span>{{$booking->customer->user->name}}</span>
                </li>
                <li>
                    <span>চালক:</span>
                    <span>@isset($booking->driver) {{$booking->driver->user->name}} @endif</span>
                </li>
                <li>
                    <span>গাড়ি:</span>
                    <span>@isset($booking->vehicle) {{$booking->vehicle->number_plate}} ({{$booking->vehicle->vehicleType->title}} ) @endif</span>
                </li>
            </ul>
            <h5 class="bfc-title">বুকিং-এর  বিবরণ</h5>
            <ul class="confrim-list">
                @if($booking->booking_category=='full')
                    <li>
                        <span>বুকিং-এর ধরণ:</span>
                        <span>সম্পূর্ণ গাড়ির বুকিং</span>
                    </li>                                
                    <li>
                        <span>গাড়ীর ধরণ :</span>
                        <span>{{$booking->fullBookingDetails->vehicleType->title}}</span>
                    </li>                        
                    <li>
                        <span> ধারণ ক্ষমতা:</span>
                        <span>{{$booking->fullBookingDetails->capacity}} {{$booking->fullBookingDetails->vehicleType->capacityType->title}}</span>
                    </li>
                    
                @else
                    <li>
                        <span>বুকিং-এর ধরণ:</span>
                        <span>পার্সেল ডেলিভারি</span>
                    </li>
                @endif
                <li>
                    <span>পিকআপ এর স্থান:</span>
                    <span>{{$booking->trips[0]->from_address}}</span>
                </li>
                <li>
                    <span>গন্তব্যস্থান:</span>
                    <span>{{$booking->trips[0]->to_address}}</span>
                </li>
                <li>
                    <span>দূরত্ব:</span>
                    <span>{{$booking->trips[0]->distance}} km</span>
                </li>                
                <li>
                    <span>বুকিং-এর ধরণ:</span>
                    <span>{{ucfirst($booking->booking_type)}}</span>
                </li>
                <li>
                    <span>তারিখ ও সময়:</span>
                    <span>{{$booking->datetime->toDayDateTimeString()}}</span>
                </li>
            </ul>
            <h5 class="bfc-title">Trip Information</h5>
            <ul class="confrim-list">
                <li>
                    <span>শুরুর সময়:</span>
                    <span>@if($booking->trips[0]->started_at) {{$booking->trips[0]->started_at->toDayDateTimeString()}}  @else - @endif</span>
                </li>
                <li>
                    <span>সম্পন্ন সময়:</span>
                    <span>@if($booking->trips[0]->completed_at) {{$booking->trips[0]->completed_at->toDayDateTimeString()}}  @else - @endif</span>
                </li>
            </ul>
            <h5 class="bfc-title">প্রাপকের বিবরণ</h5>
            <ul class="confrim-list">
                <li>
                    <span>নাম:</span>
                    <span>{{$booking->recipient_name}}</span>
                </li>
                <li>
                    <span>ফোন নং:</span>
                    <span>{{$booking->recipient_phone}}</span>
                </li>
            </ul>
            <h5 class="bfc-title">পণ্যের  বিবরণ</h5>
            <ul class="confrim-list">
                @if($booking->booking_category=='shared')
                    <li>
                        <span>পণ্য এর ক্যাটাগরি:</span>
                        <span>
                            {{$booking->sharedBookingDetails->productCategory->title}}
                        </span>                                
                    </li>

                    <li>
                        <span>পণ্য সংখ্যা:</span>
                        <span>
                            {{$booking->sharedBookingDetails->quantity}}
                        </span>
                    </li>

                    <li>
                        <span>ওজন:</span>
                        <span>
                            {{$booking->sharedBookingDetails->weightCategory->title}}
                        </span>                       
                    </li>

                    <li>
                        <span>আয়তন:</span>
                        <span>
                            {{$booking->sharedBookingDetails->volumetricCategory->title}}
                        </span>
                    </li>
                @endif
                <li>
                    <span>বিবরণ:</span>
                    <span>{{$booking->particular_details}}</span>
                </li>
            </ul>            
        </div>
    </div>
    <div class="booking-map-block">
        <div id="map" class="map-block map-booking"></div>
    </div>
</div>
@stop

@section('footer')
<script src="{{URL('website/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.c-scroll').mCustomScrollbar();
        $(document).on('click', '.-mt-open', function(e){
            $('.booking-page-map').addClass('-map-open');
            $('.c-scroll').mCustomScrollbar();
        });
        $(document).on('click', '.-mt-close', function(e){
            $('.booking-page-map').removeClass('-map-open');
        });
    });
</script>

@include('common.bookings._partials.tracker')
@stop