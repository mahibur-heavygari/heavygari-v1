<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P58GJH3');</script>
    <!-- End Google Tag Manager -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:image" content="{{ url('/website/images/og-image.png') }}" />
    <meta property="og:image" content="{{ url('/website/images/og_1200x600.png') }}" />
    <meta property="og:image" content="{{ url('/website/images/og_1200x630.png') }}" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image" content="{{ url('/website/images/og-whatsapp.png') }}" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="400" />
    <meta name="description" content="HeavyGari connects customers with vehicles in order to provide a hassle-free solution for all their transportation needs all over Bangladesh.">

    <title>হেভিগাড়ী</title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::asset('website/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{URL::asset('website/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::asset('website/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('website/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::asset('website/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('website/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::asset('website/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('website/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::asset('website/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{URL::asset('website/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::asset('website/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{URL::asset('website/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::asset('website/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{URL::asset('website/images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('website/plugins/bootstrap/css/bootstrap.min.css')}}">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('website/plugins/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('website/plugins/slick/slick.css')}}">
    <link rel="stylesheet" href="{{URL::asset('website/plugins/slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{URL::asset('website/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/custom.css?bb=624')}}">
    <link rel="stylesheet" href="{{URL::asset('css/custom-style.css?y=420')}}">
   @yield('header')
</head>

<body class="
    @isset($lang) 
     @if($lang=='bn') lang-bangla @elseif($lang=='en') lang-english @endif
    @else lang-bangla
    @endif
    " 
    data-spy="scroll" data-target="#navbarScroll" data-offset="62">

<body class="@isset($body_class) map-fixed @endif"  data-spy="scroll" data-target="#navbarScroll" data-offset="62">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P58GJH3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav id="navbarScroll" class="navbar navbar-expand-lg fixed-top navbar-dark">
        <!-- <ul class="language-bar">
            <li class="active">
                <a href="">English</a>
            </li>
            <li>
                <a href="">Bangla</a>
            </li>
        </ul> -->
        <div class="container">
            <a class="navbar-brand" href="{{URL('/')}}">
                <img src="{{URL::asset('website/images/brand-icon.png')}}" height="32" class="d-inline-block align-top" alt="Heavygari">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapsed" aria-controls="navbarCollapsed" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            @isset($lang)
                @include('website.'.$lang.'.common.navbar')
            @else
                @include('website.bn.common.navbar')
            @endif
        </div>
    </nav>

    @if(session('message'))

        <div class="container">
            <div class="alert alert-{{session('message')[0]}} alert-dismissible fade show mt-3" role="alert">
                {{session('message')[1]}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif
    

    @yield('content-body')

    @isset($lang)
        @include('website.'.$lang.'.common.footer')
    @else
        @include('website.bn.common.footer')
    @endif


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{URL::asset('website/plugins/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="{{URL::asset('website/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('website/plugins/smoothscroll/jquery.smooth-scroll.js')}}"></script>
    <script src="{{URL::asset('website/plugins/slick/slick.min.js')}}"></script>

    <script>
        
        $(document).ready(function() {
            // Script for smooth scroll
            $('.smooth-scroll').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 60
                        }, 400);
                        return false;
                    }
                }
            });

             $('.slider-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                //centerMode: true,
                focusOnSelect: true,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        slidesToShow: 4
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 3
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 2
                      }
                    }
                  ]
            });

            // tracking url
            $('#showTrackingButton').on('click', function() {
                redirectToTrackingPage();
            });
        });

        function redirectToTrackingPage() {
            var tracking_id = $('#tracking_id').val();
            var url = '/booking/'+tracking_id;
            $(location).attr('href', url);
        }


$(window).scroll(function() {
        var sTop = $(window).scrollTop();
        if (sTop > 200) {
            $('.fixed-top').removeClass('navbar-dark').addClass('navbar-light bg-light shadow');
        } else {
            $('.fixed-top').removeClass('navbar-light bg-light').addClass('navbar-dark shadow');
        }
    });
       
    </script>

    @yield('footer')
</body>

</html>