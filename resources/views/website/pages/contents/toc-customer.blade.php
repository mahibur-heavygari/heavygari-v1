<h1 class="page-heading">
    Customer and Recepient Terms and Condition
</h1>
<div class="text-widget -style-2">
    <h3>
        1. INTRODUCTORY
    </h3>
    <p>
        The Terms of Use stated herein (collectively, the “Terms of Use” or this “Agreement”) constitute a legal agreement between you and Heavygari technologies Ltd. (the “Company”). In order to use the Service defined below you must agree to the Terms of Use that are set out below. 
        <br/>
        The Company reserves the right to modify, vary and change the Terms of Use or its policies relating to the Service at any time as it deems fit. Such modifications, variations and or changes to the Terms of Use or its policies relating to the Service shall be effective upon the posting of an updated version at Heavygari. You agree that it shall be your responsibility to review the Terms of Use regularly whereupon the continued use of the Service after any such changes, whether or not reviewed by you, shall constitute your consent and acceptance to such changes. 
        <br/>
        Important – please read these terms carefully. By using the Service (as defined below), you agree that you have read, understood, accepted and agreed with the Terms of Use. You further agree to the representations made by yourself below. If you do not agree to or fall within the Terms of Use of the Service and wish to discontinue using the Service, please do not continue using the Application (as defined below) or the Service.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        2. THE SERVICES
    </h3>
    <p>
        The Services comprise mobile applications and related services (each, an "Application"), which enable users to arrange and schedule transportation, logistics and/or delivery services and/or to purchase certain goods, including with third party providers of such services and goods under agreement with the Company ("Third Party Providers"). In certain instances, the Services may also include an option to receive transportation, logistics and/or delivery services for an upfront price, subject to acceptance by the respective Third Party Providers. Unless otherwise agreed by the Company in a separate written agreement with you, the Services are made available solely for your personal, non-commercial use.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        2.1 Your Obligations
    </h3>
    <p>
        You agree to ensure that the information you supply in the Order Schedule is complete and accurate; co-operate with the Driver and owner and Heavygari technologies Ltd in all matters relating to the provision of the Service(s); provide the Driver/owner with access to your premises, office accommodation and other facilities as reasonably required, if/when any of these are to be the Collection Point or Delivery Point and be responsible for ensuring that the premises are free of hazardous materials and do not pose a health and safety risk to the Driver/owner. 
        <br/>
        Heavygari technologies ltd with such information and materials as may reasonably require in order to supply the Service(s) and ensure that such information is accurate in all material respects. You agree that the Driver/Owner shall not be required, and that you shall not cause them, to carry anything if it would be illegal or unlawful for them to do so under the laws of the Peoples republic of Bangladesh. You agree that should you do this, you will indemnify Heavygari technologies ltd and against any losses and/or damage that we may suffer as a consequence 
        <br/>
        The Driver/owner shall not, carry gasses; pyrotechnics; arms; ammunition; corrosive; toxic; flammable; explosive; oxidizing or radioactive materials. In addition, the driver/owner will not carry any items which are on our prohibited by the laws of the Land. The driver/owner and Heavygari technologies ltd reserve the right to refuse to carry any parcels which are neither the property of, nor sent on behalf of, you. In terms of capacity and in accordance with local laws and regulations, the allowable highest load (including weight of goods and vehicle) for each two-excel (six-wheel) vehicle is 22 tonnes, while for each three-excel (10-wheel) vehicle the load is 30 tonnes, and for each four-excel (14-wheel) the highest allowed load is 40 tonnes.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        2.2 It is understood that you agree that:
    </h3>
    <p>
        All Consignments shall be accepted at the Delivery Point and that the recipient shall give the Driver/owner an appropriate receipt and you agree that this receipt shall be conclusive evidence of delivery of the Consignment. Unless specifically agreed otherwise, “working days” do not include Friday, Saturday or public holidays. No refund or reduction shall be provided of charges if less than the number of parcels for which you have contracted has been received. That you cannot send a package weighing more than the maximum capacity of the said vehicle and that Driver/owner reserves the right to refuse delivery of products weighing over the set limit. That you will be barred to send contrabands as specified by the laws of the Land. That you will liable to disclose all necessary information regarding the product to the Driver/owner. That you will not be allowed to send any unlawful item and if you carry to insist to carry any perishable items it is up to your risk and your responsibility. That you are not allowed to send passport, bank cheques or any material pertaining to a cash transaction between two entities That you are encouraged not to transfer extremely expensive items; if they do, they are asked to be cautious That you are to maintain their orders according to the outlined product categories, fully understanding each category description and strictly abiding by any restrictions a product category imposes on the user That you are to maintain extreme caution while packing their product, so as to diminish chances of damage as much as possible That you are to make sure that their items do not harm the driver/owner in any way ,that you are encouraged to bubble-wrap any electronics they wish to transfer, so as to avoid damaging the item That you have to pay the driver/owner upfront. Failure to do so will result in the driver/owner cancelling your request. That you are to report any damaged/missing/lost items to our support team within 5 hours of dispatch, Heavygari will not take full liability of any item that is damaged or lost. However, in an event if Heavygari find your report to hold solid ground for compensation, Heavygari may compensate for the losses up to 10,000 BDT only.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        2.3 Liability
    </h3>
    <p>
        YOUR ATTENTION IS DRAWN PARTICULARLY TO THIS CLAUSE AND THE LIMITS OF OUR LIABILITY WITHIN IT. As a responsible business, Heavygari technologies ltd and the respective Driver/Owner shall hopefully perform the Service(s) in a professional manner with the appropriate level of skill and care. Heavygari technologies ltd will not take full liability of any item that is damaged or lost. Damage to a Consignment may still occur as a consequence of handling of it and in such circumstances, Heavygari technologies Ltd and the driver/owner liability shall be limited as set out in these Terms and Conditions. The reasoning behind this limitation of our liability is as follows: The value of a Consignment and the amount of potential loss to you that could arise if a Consignment is damaged or lost is not something which can be easily ascertained but is something which is better known to you. In many cases it cannot be known to Heavygari or the driver/owner at all and can only be known to you; The potential amount of loss that might be caused or alleged to be caused to you is likely to be disproportionate to the sum that could reasonably be expected to charge you for providing the Service(s) under this Agreement; It is not possible to obtain cover which would give unlimited compensation for full potential liability to all customers and, even if it were, such cover would be much cheaper if taken out by you and on that basis, it is more reasonable for you to take out such cover from an independent third party. It is imperative to keep the costs of providing the Service(s) to you as low as possible; In these Terms and Conditions, damage to you means any loss of, or damage to, a Consignment. Heavygari technologies Ltd. shall not be liable to you: a) Under any circumstances where there are any material discrepancies between the declared dimensions and weights and the actual dimensions and weights; c) In any circumstances in respect of the items on the Prohibited items; Special Provisions items and for damage to the No Compensation Items lists, unless otherwise stated by us. To the best of our abilities, we can connect you through to the rightful authority to investigate damage or loss of your item. However, we will not be held liable to damage of your item or loss because of the aforementioned reasons in these Terms and Conditions.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        3. NOT A TRANSPORTATION PROVIDER
    </h3>
    <p>
        THE COMPANY IS A TECHNOLOGY COMPANY THAT DOES NOT PROVIDE TRANSPORTATION SERVICES AND THE COMPANY IS NOT A TRANSPORTATION PROVIDER. IT IS UP TO THE THIRD-PARTY TRANSPORTATION PROVIDERS TO OFFER TRANSPORTATION SERVICES TO YOU AND IT IS UP TO YOU TO ACCEPT SUCH TRANSPORTATION SERVICES. THE SERVICE OF THE COMPANY IS TO LINK YOU WITH SUCH THIRD-PARTY TRANSPORTATION PROVIDERS, BUT DOES NOT, AND NOR IS IT INTENDED TO, PROVIDE TRANSPORTATION SERVICES OR ANY ACT THAT CAN BE CONSTRUED IN ANY WAY AS AN ACT OF A TRANSPORTATION PROVIDER. 
        <br/>
        ACCORDINGLY, YOU UNCONDITIONALLY UNDERSTAND AND ACKNOWLEDGE THAT YOUR ABILITY TO OBTAIN TRANSPORTATION, LOGISTICS AND/OR DELIVERY SERVICES THROUGH THE USE OF THE SERVICES DOES NOT ESTABLISH THE COMPANY AS A PROVIDER OF TRANSPORTATION, LOGISTICS OR DELIVERY SERVICES OR AS A TRANSPORTATION CARRIER AND THE COMPANY IS NOT, IN ANY WHATSOEVER, RESPONSIBLE NOR LIABLE FOR THE ACTS AND/OR OMISSIONS OF ANY THIRD PARTY TRANSPORTATION PROVIDER AND/OR ANY TRANSPORTATION SERVICES PROVIDED TO YOU.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        4. THE SERVICE AND INVOICES/RECEIPT/CHALAN
    </h3>
    <p>
        The Services constitute a technology platform that enables users of Heavygari’s mobile applications or website provided as part of the Services to pre-book and schedule transportation, logistics, delivery, and/or vendor services with independent and third party providers of such services, including independent third party transportation providers, independent third party logistics and/or delivery providers.
    </p>
    <p>
        Unless both you and Heavygari otherwise agree in writing, any arbitration/issues will be conducted only on an individual basis and not in a class, collective, consolidated, or representative (court trials etc) proceeding. 
    </p>
    <p>
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        5. RIGHT TO REFUSE AND BLACKLIST
    </h3>
    <p>
        Notwithstanding anything herein written, the Company may, at its sole and absolute discretion, blacklist you permanently or temporarily and reject your request to use the Application and / or Service or any part thereof for such reasons as it deems fit, including but not limited to receiving complaints about you from third party providers or employees of the Company about your behavior or interaction with anyone whatsoever (including but not limited to third party provider, Company’s employees, law enforcement, government authorities) whilst and/or before using the Service.
    </p>

    <p>
        <!--EndFragment-->
    </p>

    <p>
        <!--StartFragment-->
    </p>
        6. REPRESENTATIONS AND WARRANTIES
    </h3>
    <p>
        By using the Service, you expressly represent and warrant that you are legally entitled to accept and agree to the Terms of Use and that you are at least eighteen (18) years old. Without limiting the generality of the foregoing, the Service is not available to persons under the age of eighteen (18) or such persons that are forbidden for any reason whatsoever to enter into a contractual relationship. By using the Service, you further represent and warrant that you have the right, authority and capacity to use the Service and to abide by the Terms of Use. You further confirm that all the information which you provide shall be true and accurate. Your use of the Service is for your own sole, personal use. You undertake not to authorize others to use your identity or user status, and you may not assign or otherwise transfer your user account to any other person or entity. When using the Service, you agree to comply with all applicable laws whether in your home nation or otherwise in the country, state and city in which you are present while using the Service. 
        <br/>
        You may only access the Service using authorized means. It is your responsibility to check and ensure that you have downloaded the correct Software for your device. The Company is not liable if you do not have a compatible device or if you have downloaded the wrong version of the Software to your device 
        <br/>
        The Company reserves the unconditional right to not permit you to use the Service should you use the Application and/or the Software with an incompatible or unauthorized device or for purposes other than which the Software and/or the Application is intended to be used. 
        <br/>
        Without limitation to the generality of the foregoing, by using the Software or the Application and availing the services of Heavygari, you agree that:
    </p>
    <p>
        a. You will only use the Service for lawful purposes; 
        <br/>
        b. You will only use the Service for the purpose for which it is intended to be used; 
        <br/>
        c. You will not use the Application for sending or storing any unlawful material or for fraudulent purposes; 
        <br/>
        d. You will not use the Application and/or the Software to cause nuisance, annoyance, inconvenience or make fake bookings;
        <br/>
        e. You will not use the Service, the Application and/or the Software for purposes other than obtaining the Service; 
        <br/>
        f. You shall not contact the third-party transportation provider for purposes other than the Service; 
        <br/>
        g. You will not impair the proper operation of the network; 
        <br/>
        h. You shall not intentionally or unintentionally cause or attempt to cause damage to the third-party transportation provider; 
        <br/>
        i. You will not try to harm the Service, the Application and/or the Software in any way whatsoever; 
        <br/>
        j. You will not copy, or distribute the Software or other content without written permission from the Company; 
        <br/>
        k. You will only use the Software and/or the Application for your own use and will not resell it to a third party; 
        <br/>
        l. You will keep secure and confidential your account password or any identification we provide you which allows access to the Service; 
        <br/>
        m. You will provide the Company with proof of identity as it may reasonably request or require; 
        <br/>
        n. You agree to provide accurate, current and complete information as required for the Service and undertake the responsibility to maintain and update your information in a timely manner to keep it accurate, current and complete at all times during the term of this Agreement. You agree that the Company may rely on your information as accurate, current and complete. You acknowledge that if your information is untrue, inaccurate, not current or incomplete in any respect, the Company has the right but not the obligation to terminate this Agreement and your use of the Service at any time with or without notice; 
        <br/>
        o. You will only use an access point or data account which you are authorized to use; 
        <br/> 
        p. You shall not employ any means to defraud the Company or enrich yourself, through any means, whether fraudulent or otherwise, through any event, promotion or campaign launched by the Company to encourage new subscription or usage of the Service by new or existing passengers; 
        <br/>    
        q. You are aware that when requesting transportation services by SMS or use of the Service, standard telecommunication charges will apply; 
        <br/>
        r. You shall not impair or circumvent the proper operation of the network which the Service operates on; 
        <br/>
        s. You will not use the Service or any part thereof for carrying contraband items and if, in the event that you display suspicious behavior, you will fully comply with the request of the third party service provider, any government authority and / or law enforcement, to inspect any bags and / or items you are carrying with you which may or may not be readily visible; 
        <br/>
        t. You when collecting goods from a Driver/owner, the recipient can only conduct a inspection of the exterior of the sealed package of the products in order to determine the condition of the products as received from the customer. If any defect is found upon this initial inspection, you may have tri party communication to solve the matter upon mutual understanding. 
        <br/>
        u. You can not claim any such claim from Heavygari. Heavygari can never be liable of any damage or theft, which cannot be ascertained by the exterior inspection. 
        <br/>
        v. You agree that the Service is provided on a reasonable effort basis; and 
        <br/>
        w. You agree that your use of the Service will be subject to the Company’s Privacy Policy as may be amended from time to time.
    </p>
    <p>
        You unconditionally agree to assume full responsibility and liability for all loss or damage suffered by yourself, the third-party service provider, the Company, Merchant and/or any third party as a result of any breach of the Terms of Use.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        7.Loading and Unloading
    </h3>
    <p>
        If collection or delivery of a Consignment takes place at your premises, Heavygari technologies ltd shall not be under any obligation to provide any equipment or labour which, apart from the Rider/Freelancer collecting the Consignment, may be required for loading or unloading of a Consignment. Any Consignment (or part of a Consignment) requiring any special equipment for loading and unloading shall be accepted by us for transportation only on the understanding and condition that such special equipment will be made available at the Collection Point and the Delivery Point as required. Where such equipment is not available and if the Rider/Freelancer agrees to load or unload the Consignment (or part of the Consignment) Heavygari shall be under no liability or obligation of any kind to you for any damage caused (however it may be caused) during the loading or unloading of the Consignment. This includes any damage caused whether or not by the Driver/owner negligence and you shall agree to indemnify and hold us harmless against any claim or demand from any person arising out of this agreement to load or unload the Consignment in these circumstances.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        7.1 Collection and Deliveries
    </h3>
    <p>
        The Driver/Owner will make one attempt to deliver a Consignment during normal working hours. If the Driver/owner cannot obtain a delivery receipt at the Delivery Point you agree that the Driver/owner shall be authorized to attempt to: deliver the Consignment to, or obtain a delivery receipt from, an alternative address close to the Delivery Point; or deliver the Consignment to a safe location at the Delivery Point and (if successful) the Driver/owner agrees that he/she will leave at the Delivery Point details of the address or safe location to which he/she have delivered the Consignment. If the Driver/owner is unable to deliver to the Delivery Point, a nearby address or a safe location, the Driver/owner shall return the Consignment to a designated location and leave a request for the recipient of the Consignment to contact the Driver/owner to make alternative delivery arrangements to the Delivery Point. If the recipient does not contact us to arrange the alternative delivery within 10 days we will return the Consignment to you at your cost (such cost to be discharged before delivery to you). If the driver/owner consider that the Consignment has become a Damaged Consignment and cannot be delivered because it is or in his/her reasonable opinion is likely to be unsafe hazardous or harmful they reserve the right to refuse delivery and/or dispose of the Damaged Consignment immediately
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        7.2 PAYMENT
    </h3>
    <p>
        a. You may choose to pay for the transportation services by cash and where available. 
        <br/>
        b. Once you have completed or before you have started the trip, using the Service, you are required to make payment in full to the third party transportation provider and such payment is non-refundable. If you have any complaints in relation to the transportation service provided, then that dispute must be taken up with the third transportation provider directly.
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        8. TAXES
    </h3>
    <p>
        You agree that this Agreement shall be subject to all prevailing statutory taxes, duties, fees, charges and/or costs, however denominated, as may be in force in Bangladesh and in connection with any future taxes that may be introduced at any point of time. You further agree to use your best efforts to do everything necessary and required by the relevant laws to enable, assist and/or defend the Company to claim or verify any input tax credit, set off, rebate or refund in respect of any taxes paid or payable in connection with the Service supplied under this Agreement.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        9. LICENSE GRANT & RESTRICTIONS
    </h3>
    <p>
        The Company and its licensors, where applicable, hereby grants you a revocable, non-exclusive, non-transferable, non-assignable, personal, limited license to use the Application and/or the Software, solely for your own personal, non-commercial purposes, subject to the Terms of Use herein. All rights not expressly granted to you are reserved by the Company and its licensors.
    </p>
    <p>
        <strong>
            (a) You shall not 
        </strong>
        <br/>
        i. license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Application and/or the Software in any way; 
        <br/>
        ii. modify or make derivative works based on the Application and/or the Software;    
        <br/>
        iii. create internet “links” to the Application or “frame” or “mirror” the Software on any other server or wireless or internet-based device; 
        <br/>
        iv. reverse engineer or access the Software in order to (a) build a competitive product or service, (b) build a product using similar ideas, features, functions or graphics of the Application and/or the Software, or (c) copy any ideas, features, functions or graphics of the Application and/or the Software, 
        <br/>
        v. launch an automated program or script, including, but not limited to, web spiders, web crawlers, web robots, web ants, web indexers, bots, viruses or worms, or any program which may make multiple server requests per second, or unduly burdens or hinders the operation and/or performance of the Application and/or the Software, 
        <br/>
        vi. use any robot, spider, site search/retrieval application, or other manual or automatic device or process to retrieve, index, “data mine”, or in any way reproduce or circumvent the navigational structure or presentation of the Service or its contents; 
        <br/>
        vii. post, distribute or reproduce in any way any copyrighted material, trademarks, or other proprietary information without obtaining the prior consent of the owner of such proprietary rights, 
        <br/>
        viii. remove any copyright, trademark or other proprietary rights notices contained in the Service.
    </p>
    <p>
        <strong>
              (b) You may use the Software and/or the Application only for your personal, non-commercial purposes and shall not use the Software and/or the Application to: 
        </strong>
        <br/>
        i. send spam or otherwise duplicative or unsolicited messages; 
        <br/>
        ii. send or store infringing, obscene, threatening, libelous, or otherwise unlawful or tortious material, including but not limited to materials harmful to children or violation of third party privacy rights 
        <br/>
        iii. send material containing software viruses, worms, trojan horses or other harmful computer code, files, scripts, agents or programs; 
        <br/>
        iv. interfere with or disrupt the integrity or performance of the Software and/or the Application or the data contained therein; 
        <br/>
        v. attempt to gain unauthorized access to the Software and/or the Application or its related systems or networks; or 
        <br/>
        vi. Impersonate any person or entity or otherwise misrepresent your affiliation with a person or entity 
        <br/>
        vii. to abstain from any conduct that could possibly damage the Company’s reputation or amount to being disreputable.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        10. INTELLECTUAL PROPERTY OWNERSHIP
    </h3>
    <p>
        The Company and its licensors, where applicable, shall own all right, title and interest, including all related intellectual property rights, in and to the Software and/or the Application and by extension, the Service and any suggestions, ideas, enhancement requests, feedback, recommendations or other information provided by you or any other party relating to the Service. The Terms of Use do not constitute a sale agreement and do not convey to you any rights of ownership in or related to the Service, the Software and/or the Application, or any intellectual property rights owned by the Company and/or its licensors. The Company’s name, the Company’s logo, the Service, the Software and/or the Application and the third party transportation providers’ logos and the product names associated with the Software and/or the Application are trademarks of the Company or third parties, and no right or license is granted to use them. For the avoidance of doubt, the term the Software and the Application herein shall include its respective components, processes and design in its entirety.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        11. CONFIDENTIALITY
    </h3>
    <p>
        You shall maintain in confidence all information and data relating to the Company, its services, products, business affairs, marketing and promotion plans or other operations and its associated companies which are disclosed to you by or on behalf of the Company (whether orally or in writing and whether before, on or after the date of this Agreement) or which are otherwise directly or indirectly acquired by you from the Company, or any of its affiliated companies, or created in the course of this Agreement. You shall further ensure that it, its officers, employees and agents only use such confidential information in order to perform the Service, and shall not without the Company’s prior written consent, disclose such information to any third-party nor use it for any other purpose. You shall only disclose such information to such officers, employees and agents as need to know it to fulfill its obligations under this Agreement. 
        <br/>
        The above obligations of confidentiality shall not apply to the extent that you can show that the relevant information:
    </p>
    <p>
        a. was at the time of receipt already in your possession; 
        <br/>
        b. is, or becomes in the future, public knowledge through no fault or omission of you; 
        <br/>
        c. was received from a third-party having the right to disclose it; or 
        <br/>
        d. is required to be disclosed by law.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        12. PERSONAL DATA PROTECTION
    </h3>
    <p>
        You agree and consent to the Company using and processing your Personal Data for the Purposes and in the manner as identified hereunder. 
        <br/>
        For the purposes of this Agreement, “Personal Data” means information about you, from which you are identifiable, including but not limited to your name, identification card number, birth certificate number, passport number, nationality, address, telephone number, credit or debit card details, race, gender, date of birth, email address, any information about you which you have provided to the Company in registration forms, application forms or any other similar forms and/or any information about you that has been or may be collected, stored, used and processed by the Company from time to time and includes sensitive personal data such as data relating to health, religious or other similar beliefs. 
        <br/>
        The provision of your Personal Data is voluntary. However, if you do not provide the Company your Personal Data, your request for the Application may be incomplete and the Company will not be able to process your Personal Data for the Purposes outlined below and may cause the Company to be unable to allow you to use the Service
        <br/>
        The Company may use and process your Personal Data for business and activities of the Company which shall include, without limitation the following (the “Purposes”):
    </p>
    <p>
        a. To perform the Company’s obligations in respect of any contract entered with you; 
        <br/>
        b. To provide you with any services pursuant to the Terms of Use herein; 
        <br/>
        c. To process your participation in any events, promotions, activities, focus groups, research studies, contests, promotions, polls, surveys or any productions and to communicate with you regarding your attendance thereto; 
        <br/>
        d. Process, manage or verify your application for the Service pursuant to the Terms of Use herein; 
        <br/>
        e. To validate and/or process payments pursuant to the Terms of Use herein; 
        <br/>
        f. To develop, enhance and provide what is required pursuant to the Terms of Use herein to meet your needs; 
        <br/>
        g. To process any refunds, rebates and or charges pursuant to the Terms of Use herein; 
        <br/>
        h. To facilitate or enable any checks as may be required pursuant to the Terms of Use herein
        <br/>
        i. To respond to questions, comments and feedback from you; 
        <br/>
        j. To communicate with you for any of the purposes listed herein; 
        <br/>
        k. For internal administrative purposes, such as auditing, data analysis, database records; 
        <br/>
        l. For purposes of detection, prevention and prosecution of crime; 
        <br/>
        m. For the Company to, its partners, advertisers and or sponsors; 
        <br/>
        n. To notify and invite you to events or activities organized by the Company, its partners, advertisers, and or sponsors; and/or
        <br/>
        o. To share your Personal Data amongst the companies within the Company’s group of companies comprising the subsidiaries, associate companies and or jointly controlled entities of the holding company of the group (the “Group”) and with the Company’s and Group’s agents, third party providers, developers, advertisers, partners, event companies or sponsors who may communicate with you for any reasons whatsoever.
    </p>
    <p>
        If you do not consent to the Company processing your Personal Data for any of the Purposes, please notify the Company using the support contact details as provided in the Application. 
        <br/>
        If any of the Personal Data that you have provided to us changes, for example, if you change your e-mail address, telephone number, payment details or if you wish to cancel your account, please update your details by sending your request to the support contact details as provided in the Application. 
        <br/>
        The Company will, to the best of its abilities, effect such changes as requested within fourteen (14) working days of receipt of such notice of change 
        <br/>
        By submitting your information to the Company, you consent to the use of that information as set out in the form of submission and in the Terms of Use.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        13. THIRD PARTY INTERACTIONS
    </h3>
    <p>
        During use of the Service, you may enter into correspondence with, purchase goods and/or services from, or participate in promotions of third party providers, advertisers or sponsors showing their goods and/or services through the Service, the Software and/or the Application. Any such activity, and any terms, conditions, warranties or representations associated with such activity, is solely between you and the applicable third-party. The Company and its licensors shall have no liability, obligation or responsibility for any such correspondence, purchase, transaction or promotion between you and any such third-party. The Group does not endorse any applications or sites on the Internet that are linked through the Service, the Application and/or the Software, and in no event, shall the Company, its licensors or the Group be responsible for any content, products, services or other materials on or available from such sites or third party providers. The Company provides the Service to you pursuant to the Terms of Use. You recognize, however, that certain third party providers of transportation, goods and/or services may require your agreement to additional or different Terms of Use prior to your use of or access to such goods or services, and the Company is not a party to and disclaims any and all responsibility and/or liability arising from such agreements between you and the third party providers
    </p>
    <p>
        The Company may rely on third party advertising and marketing supplied through the Service and other mechanisms to subsidize the Service and/or to earn additional revenue. By agreeing to the Terms of Use you agree to receive such advertising and marketing. If you do not want to receive such advertising, you should notify us in writing or in accordance with the procedure determined by the Company. The Company reserves the right to charge you a higher fee for or deny you use of the Service should you choose not to receive these advertising services. This higher fee, if applicable, will be posted on the Company’s website located at Heavygari.com. 
        <a href="www.heavygari.com">
            www.heavygari.com
        </a>
        You agree and allow the Company to compile and release information regarding you and your use of the Service on an anonymous basis as part of a customer profile or similar report or analysis. You agree that it is your responsibility to take all precautions in all actions and interactions with any third party transportation provider, other third party providers, advertisers and/or sponsors you interact with through the Service and/or advertising or marketing material supplied through the Service
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        14. REPAIR OR CLEANING FEES
    </h3>
    <p>
        You shall be responsible for the cost of repair for any damage to or necessary cleaning of the third party service provider’s vehicle as a result of your misuse of the Service or in breach of the Terms of Use herein. The Company reserves the right to facilitate payment for reasonable cost of such repair or cleaning on behalf of the third party service provider via your designated payment method or demand from you in cash, in the event a request for repair or cleaning request by the third party service provider has been verified by the Company.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        15. INDEMNIFICATION
    </h3>
    <p>
        By agreeing to the Terms of Use upon using the Service, you agree that you shall defend, indemnify and hold the Company, its licensors and each such party’s parent organizations, subsidiaries, affiliates, officers, directors, members, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including laweyrs’ fees and costs and/or regulatory action) arising out of or in connection with: (a) your use of the Service, the Software and/or the Application in your dealings with the third party transportation providers, third party providers, partners, advertisers and/or sponsors, or (b) your violation or breach of any of the Terms of Use or any applicable law or regulation, whether or not referenced herein, or (c) your violation of any rights of any third party, including third party transportation providers arranged via the Service, or (d) your use or misuse of the Service, the Software and/or the Application.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        16. DISCLAIMER OF WARRANTIES
    </h3>
    <p>
        THE COMPANY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE. THE COMPANY DOES NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE SERVICE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIALS PURCHASED OR OBTAINED BY YOU THROUGH THE APPLICATION WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS IN THE APPLICATION AND/OR THE SOFTWARE WILL BE CORRECTED, OR (F) THE APPLICATION OR THE SERVER(S) THAT MAKE THE APPLICATION AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS, OR (G) THE APPLICATION AND/OR THE SOFTWARE TRACKS YOU OR THE VEHICLE USED BY THE THIRD PARTY TRANSPORTATION PROVIDER. THE SERVICE IS PROVIDED TO YOU STRICTLY ON AN “AS IS” BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY EXCLUDED AND DISCLAIMED TO THE HIGHEST AND MAXIMUM EXTENT. 
        <br/>
        FURTHERMORE, THE COMPANY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE AS TO THE RELIABILITY, SAFETY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF ANY SERVICES, INCLUDING BUT NOT LIMITED TO THE THIRD PARTY TRANSPORTATION SERVICES OBTAINED BY OR FROM THIRD PARTIES THROUGH THE USE OF THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE. YOU ACKNOWLEDGE AND AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE SERVICE, AND ANY THIRD PARTY SERVICES, INCLUDING BUT NOT LIMITED TO THE THIRD PARTY TRANSPORTATION SERVICES REMAINS SOLELY AND ABSOLUTELY WITH YOU AND YOU SHALL HAVE NO RECOURSE WHATSOEVER TO THE COMPANY.
    <p>
        <!--EndFragment-->
    </p>
     <h3>
        17. INTERNET DELAYS
    </h3>
    <p>  
        THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER PROBLEMS INHERENT IN THE USE OF THE INTERNET AND ELECTRONIC COMMUNICATIONS INCLUDING THE DEVICE USED BY YOU OR THE THIRD PARTY TRANSPORTATION PROVIDER BEING FAULTY, NOT CONNECTED, OUT OF RANGE, SWITCHED OFF OR NOT FUNCTIONING. THE COMPANY IS NOT RESPONSIBLE FOR ANY DELAYS, DELIVERY FAILURES, DAMAGES OR LOSSES RESULTING FROM SUCH PROBLEMS.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        18. LIMITATION OF LIABILITY
    </h3>
    <p>
        ANY CLAIMS AGAINST THE COMPANY BY YOU SHALL IN ANY EVENT BE LIMITED TO THE AGGREGATE AMOUNT OF ALL AMOUNTS ACTUALLY PAID BY AND/OR DUE FROM YOU IN UTILISING THE SERVICE DURING THE EVENT GIVING RISE TO SUCH CLAIMS. IN NO EVENT SHALL THE COMPANY AND/OR ITS LICENSORS BE LIABLE TO YOU OR ANYONE FOR ANY DIRECT, INDIRECT, PUNITIVE, ECONOMIC, FUTURE SPECIAL, EXEMPLARY, INCIDENTAL, CONSEQUENTIAL OR OTHER DAMAGES OR LOSSES OF ANY TYPE OR KIND (INCLUDING PERSONAL INJURY OF ANY KIND WHATSOEVER INCLUDING LOSS OF LIFE OR LIMBS OR SERIOUS HARM OF ANY KIND WHATSOEVER, EMOTIONAL DISTRESS AND LOSS OF DATA, GOODS, REVENUE, PROFITS, USE OR OTHER ECONOMIC ADVANTAGE). THE COMPANY AND/OR ITS LICENSORS SHALL NOT BE LIABLE FOR ANY LOSS, DAMAGE OR INJURY WHICH MAY BE INCURRED BY OR CAUSED TO YOU OR TO ANY PERSON FOR WHOM YOU HAVE BOOKED THE SERVICE FOR, INCLUDING BUT NOT LIMITED TO LOSS, DAMAGE OR INJURY ARISING OUT OF, OR IN ANY WAY CONNECTED WITH THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE, INCLUDING BUT NOT LIMITED TO THE USE OR INABILITY TO USE THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE, ANY RELIANCE PLACED BY YOU ON THE COMPLETENESS, ACCURACY OR EXISTENCE OF ANY ADVERTISING, OR AS A RESULT OF ANY RELATIONSHIP OR TRANSACTION BETWEEN YOU AND ANY THIRD PARTY PROVIDER, ADVERTISER OR SPONSOR WHOSE ADVERTISING APPEARS ON THE WEBSITE OR IS REFERRED TO BY THE SERVICE, THE APPLICATION AND/OR THE SOFTWARE, EVEN IF THE COMPANY AND/OR ITS LICENSORS HAVE BEEN PREVIOUSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
        <br/>
        THE COMPANY DOES NOT AND WILL NOT ASSESS NOR MONITOR THE SUITABILITY, LEGALITY, ABILITY, MOVEMENT OR LOCATION OF ANY THIRD PARTY PROVIDERS INCLUDING THIRD PARTY TRANSPORTATION PROVIDERS, ADVERTISERS AND/OR SPONSORS AND YOU EXPRESSLY WAIVE AND RELEASE THE COMPANY FROM ANY AND ALL LIABILITY, CLAIMS OR DAMAGES ARISING FROM OR IN ANY WAY RELATED TO THE THIRD PARTY PROVIDERS INCLUDING THIRD PARTY TRANSPORTATION PROVIDERS, ADVERTISERS AND/OR SPONSORS. 
        <br/>
        THE COMPANY WILL NOT BE A PARTY TO DISPUTES, NEGOTIATIONS OF DISPUTES BETWEEN YOU AND SUCH THIRD PARTY PROVIDERS INCLUDING THIRD PARTY TRANSPORTATION PROVIDERS, ADVERTISERS AND/OR SPONSORS. UNLESS YOU ARE A CORPORATE CUSTOMER WITH A CURRENT CORPORATE ACCOUNT WITH THE COMPANY, THE COMPANY CANNOT AND WILL NOT PLAY ANY ROLE IN MANAGING PAYMENTS BETWEEN YOU AND THE THIRD PARTY PROVIDERS, INCLUDING THIRD PARTY TRANSPORTATION PROVIDERS, ADVERTISERS AND/OR SPONSORS. RESPONSIBILITY FOR THE DECISIONS YOU MAKE REGARDING SERVICES AND PRODUCTS OFFERED VIA THE SERVICE, THE SOFTWARE AND/OR THE APPLICATION (WITH ALL ITS IMPLICATIONS) RESTS SOLELY WITH AND ON YOU. YOU EXPRESSLY WAIVE AND RELEASE THE COMPANY FROM ANY AND ALL LIABILITY, CLAIMS, CAUSES OF ACTION, OR DAMAGES ARISING FROM YOUR USE OF THE SERVICE, THE SOFTWARE AND/OR THE APPLICATION, OR IN ANY WAY RELATED TO THE THIRD PARTIES INCLUDING THIRD PARTY TRANSPORTATION PROVIDERS, ADVERTISERS AND/OR SPONSORS INTRODUCED TO YOU BY THE SERVICE, THE SOFTWARE AND/OR THE APPLICATION. 
        <br/>
        THE QUALITY OF THE THIRD PARTY TRANSPORTATION SERVICES SCHEDULED THROUGH THE USE OF THE SERVICE IS ENTIRELY THE RESPONSIBILITY OF THE THIRD PARTY TRANSPORTATION PROVIDER WHO ULTIMATELY PROVIDES SUCH TRANSPORTATION SERVICES TO YOU. YOU UNDERSTAND, THEREFORE, THAT BY USING THE SERVICE, YOU MAY BE EXPOSED TO TRANSPORTATION THAT IS POTENTIALLY DANGEROUS, OFFENSIVE, HARMFUL TO MINORS, UNSAFE OR OTHERWISE OBJECTIONABLE, AND THAT YOU USE THE SERVICE AT YOUR OWN RISK.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        19. OPTIONAL INSURANCE PREMIUM
    </h3>
    <p>
        At your sole discretion, you may opt to reap the benefits of the Company’s good relation with insurance companies and secure an optional insurance coverage against damages to your vehicle or injuries to yourself and others, at such premium and conditions as the Company offers from time to time. In the event that you opt for such an option, the relevant insurance premium shall be forthwith paid to the Company by you in addition to the aforesaid Service Fees for the use of the Company’s Software, Application and / or Service. This is short term general road transport insurance. The goods can be insured if the parties opt to pay the premium at the start of a trip. This agreement is between the concerned parties of a trip and the insurance company; HeavyGari has no say in this and/or the promotion of the insurance premium. It is completely up to the concerned parties to decide if they want to take insurance and HeavyGari will bear no responsibilities and/or liabilities in case of any issues between the parties and the insurance provided. Taking up the option of insurance is completely up to the parties involved in a trip and its in their sole discretion. In case of any issues associated with the insurance policy and/or the insurance company, the parties involved in the trip will take up the issue with the insurance company in order to resolve it. HeavyGari bears no responsibilities and/or liabilities and shall be no part of the negotiations between the parties involved and the insurance company. In case of any misconduct, HeavyGari will provide the full information of the parties involved. 
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        20. PARCEL SERVICE
    </h3>
    <p>
        HeavyGari, through the application service and via vehicles, and based on the customer’s parcel and feasibility to pick up will determine the carrier, methods of transport, handling and route for the parcel service. HeavyGari reserves the right to engage with sub-contractors or other third parties for the parcel service. HeavyGari shall have no responsibility or liability for any acts or omissions and also of such actions of third parties. The parcel service shall be at the sole discretion of the parties of the trip and the terms and conditions for the parcel service can change at any time. The goods will have to be lawful and in case of any misconduct we will provide the full information of the parties of the trip involved.

    </p>
    <p>
        <!--StartFragment-->
    </p>

    <h3>
        21. NOTICE
    </h3>
    <p>
        The Company may give notice by means of a general notice on the Application, electronic mail to your email address in the records of the Company, or by written communication sent by registered mail or pre-paid post to your address in the record of the Company. Such notice shall be deemed to have been given upon the expiration of 48 hours after mailing or posting (if sent by registered mail or pre-paid post) or 1 hour after sending (if sent by email). You may give notice to the Company (such notice shall be deemed given when received by the Company) by letter sent by courier or registered mail to the Company using the contact details as provided in the Application.
    </p>
    <p>
        <!--StartFragment-->
    </p>

    <h3>
        22. ASSIGNMENT
    </h3>
    <p>
        This Agreement as constituted by the Terms of Use as modified from time to time may not be assigned by you without the prior written approval of the Company but may be assigned without your consent by the Company. Any purported assignment by you in violation of this section shall be void.
    </p>
    <p>
        <!--StartFragment-->
    </p>


    <h3>
        23. MISCELLANEOUS
    </h3>
    <p>
        This Agreement shall be governed by Bangladesh law, without regard to the choice or conflicts of law provisions of any jurisdiction, and any disputes, actions, claims or causes of action arising out of or in connection with the Terms of Use or the Service shall be subject to the exclusive jurisdiction of the courts of Bangladesh to which you hereby agree to submit to. 
        <br/>
        In the event that the law in an Alternate Country does not allow jurisdiction to be that of the courts of Bangladesh or where judgment of a Bangladesh court is unenforceable in the Alternate Country, unresolved disputes shall be referred to the Bangladesh International Arbitration Centre (“BIAC”), in accordance with the Rules of the BIAC as modified or amended from time to time (the “Rules”) by a sole arbitrator appointed by the mutual agreement of the parties (the “Arbitrator”). If parties are unable to agree on an arbitrator, the Arbitrator shall be appointed by the President of BIAC in accordance with the Rules. 
        <br/>
        The seat and venue of the arbitration shall be Dhaka, in the English language and the fees of the Arbitrator shall be borne equally by the parties, provided that the Arbitrator may require that such fees be borne in such other manner as the Arbitrator determines is required in order for this arbitration clause to be enforceable under applicable law. 
        <br/>
        No joint venture, partnership, employment, or agency relationship exists between you, the Company or any third party provider as a result of the Terms of Use or use of the Service. 
        <br/>
        If any provision of the Terms of Use is held to be invalid or unenforceable, such provision shall be struck and the remaining provisions shall be enforced to the fullest extent under law. This shall, without limitation, also apply to the applicable law and jurisdiction as stipulated above. 
        <br/>
        The failure of the Company to enforce any right or provision in the Terms of Use shall not constitute a waiver of such right or provision unless acknowledged and agreed to by the Company in writing. The Terms of Use comprises the entire agreement between you and the Company and supersedes all prior or contemporaneous negotiations or discussions, whether written or oral (if any) between the parties regarding the subject matter contained herein. 
        <br/>
        You hereby agree that the Company is entitled to terminate this Agreement immediately in the event that you are found to be in breach of any of the terms stipulated in this Agreement. For the avoidance of doubt, the termination of this Agreement shall not require the Company to compensate, reimburse or cover any cost incurred by you in the course of you acquiring services from the third party transportation provider under this Agreement
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
    </h3>
    <p>
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
</div>