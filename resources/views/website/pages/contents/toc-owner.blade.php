<h1 class="page-heading">
    Driver and Owner Terms and Condition
</h1>
<div class="text-widget -style-2">
    <h3>
        1. INTRODUCTORY
    </h3>
    <p>
        Important – please read these terms carefully. By using Heavygari, you have agreed, read, understood and accepted with the Terms and Conditions, and the circumstances stated in the Driver’s Code of Conduct. You are further approving to the representations made by yourself below. If you do not agree to or fall within the Terms and Conditions of the Service (as defined below) and wish to discontinue using the Service, please do not continue using this Application or Service. The terms and conditions stated herein (collectively, the “Terms and Conditions” or this “Agreement”) constitute a legal agreement between you and Heavygari technologies Ltd.(the “Company”). 
        <br/>
        In order to use the Service (each as defined below) you must agree to the Terms and Conditions that are set out below. By using the mobile application supplied to you by the Company (the “Application”), and downloading, installing or using any associated software supplied by the Company (the “Software”) which overall purpose is to enable the Service (each as defined below), you hereby expressly acknowledge and agree to be bound by the Terms and Conditions, and any future amendments and additions to this Terms and Conditions as published from time to time at Heavygari or through the Application.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        2. THE SERVICE
    </h3>
    <p>
        The Company offers information, method and platform for transportation providers, drivers and vehicle operators to schedule, obtain and establish contact with Customers or recipients or passengers, but does not intend to provide transportation services or act in any way as a taxi operator, transportation carrier or provider, and has no responsibility or liability for any transportation services provided to Passengers or Customers by you.
        <br/>
        The Company reserves the right to modify, vary or change the terms and conditions of this Agreement or its policies relating to the service at any time as it deems fit. Such modifications, variations or changes to the Terms and Conditions policies relating to the Service shall be effective upon the posting of an updated version at Heavygari. You agree that it shall be your responsibility to review this Agreement regularly whereupon the continued use of the service after any such changes, whether or not reviewed by you, shall constitute your consent and acceptance to such changes.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        3. NOT A TRANSPORTATION PROVIDER
    </h3>
    <p>
        Heavygari is a technology company that does not provide or engage in any transportation services. The company is not a transportation provider. The software and the application are intended to be used for facilitating you (as a transportation provider) to offer transportation services for passengers or customers. The Company will not be responsible or liable for any act or mission of any services 
        <br/>
        you provide to your passengers/clients and for any legal action committed by the owner shall not claim nor cause any misunderstanding between the agent and the employee or staff of the company and the services provided by you will not be deemed as the services of the company. You will be forbidden from promoting competitions, applications, giving out coupons and suggesting any other form of discount to the owners/clients.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        4. RIGHT TO REFUSE AND BLACKLIST
    </h3>
    <p>
        Notwithstanding anything herein written, the Company may, at its sole and absolute discretion, blacklist you permanently or temporarily and reject your request to use the Application and / or Service or any part thereof for such reasons as it deems fit, including but not limited to receiving complaints about you from customers or employees of the Company about your behavior or interaction with anyone whatsoever (including but not limited to Customers, Company’s employees, law enforcement, government authorities) or driving with a competence lower than reasonably expected whilst using the Service.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        5. REPRESENTATION AND WARRANTIES
    </h3>
    <p>
        By using this service, you expressly represent and warrant that you are legally entitled to accept and agree to the Terms and Conditions and that you are at least eighteen (18) years old. Without limiting the generality of the foregoing, the service is not available to person(s) under the age of eighteen (18) or such persons that are forbidden for any reason whatsoever to enter into a contractual relationship. By using the Service, you further represent and warrant that you have the right, authority and capacity to use the Service and to abide by the Terms and Conditions. You further confirm that all the information that you provide shall be true and accurate. Your use of the Service is for your own sole, personal use. You undertake not to authorize others to use your identity or user status, and you may not assign or otherwise transfer your user account to any other person or entity. When using the Service you agree to comply with all applicable laws of Bangladesh. You may only access the Service using authorized and legal means. It is your responsibility to check and ensure that you download the correct Software for your device. The Company will not be liable if you do not have a compatible handset or if you have downloaded the wrong version of the Software for your handset. The Company reserves the right to terminate this Agreement should you be using the Service with an incompatible or unauthorized device or for purposes other than which the Application or Software is intended to be used.
        <br/>
        By using the Service, you represent, warrant, undertake and agree that:
    
    </p>
    <p>
        a.  You possess a valid driver’s license and are authorized to operate a motor vehicle and have all the appropriate licenses, approvals and authority to provide transportation for hire to third parties in the jurisdiction in which you use the Service;
        <br/>
        b.  You own, or have the legal right and authority to operate, the vehicle which you intend to use when accepting passengers or customers, and such vehicle is in good operating condition, the fitness, insurance and tax certificates are up to date and meets the industry safety standards for vehicles of its kind;
        <br/>
        c.  You have a valid policy of liability insurance (in industry-standard coverage amounts) for the operation of your motor vehicle/passenger vehicle and/or business insurance to cover any anticipated losses related to the operation of a taxi or delivery service;
        <br/>
        d.  You shall forthwith provide to the Company such information and identity documents, including but not limited to National ID Card and Driver’s Licenses, as reasonably requested by the Company;
        <br/>
        e.  You understand and acknowledge that the Company may take up to Seven (7) working days for all identity documents to be reflected in your account or for any payments cleared to or from your account.
        <br/>
        f.  You shall be solely responsible for any and all claims, judgments and liabilities resulting from any accident, loss or damage including, but not limited to personal injuries, death, total loss and property damages which is due to or is alleged to be a result of the passenger transport and/or delivery service howsoever operated;
        <br/>
        g.  You shall obey all local laws related to the operation of passenger transport and/or delivery services and will be solely responsible for any violations of such local laws; In terms of capacity and in accordance with local laws and regulations, the allowable highest load (including weight of goods and vehicle) for each two-excel (six-wheel) vehicle is 22 tonnes, while for each three-excel (10-wheel) vehicle the load is 30 tonnes, and for each four-excel (14-wheel) the highest allowed load is 40 tonnes.
        <br/>
        h.  You will only use the Service for lawful purposes;
        <br/>
        i.  You will only use the Service for the purpose for which it is intended to be used;
        <br/>
        j.  You will not use the Application for sending or storing any unlawful material or for fraudulent purposes;
        <br/>
        k.  You will not use the Application and/or the Software to cause nuisance, annoyance, inconvenience or make fake bookings;
        <br/>
        l.  You will not use the Service, Application and/or Software for purposes other than obtaining the Service;
        <br/>
        m.  You shall not contact the Customers for purposes other than the Service;
        <br/>
        n.  You will not impair the proper operation of the network;
        <br/>
        o.  You will not try to harm the Service, Application and/or the Software in any way whatsoever;
        <br/>
        p.  You will only use the Software and/or the Application for your own use and will not resell it to a third party;
        <br/>
        q.  You will keep secure and confidential your account password or any identification we provide you which allows access to the Service;
        <br/>
        r.  You will provide us with whatever proof of identity we may reasonably request or require;
        <br/>
        s.  You agree to provide accurate, current and complete information as required for the Service and undertake the responsibility to maintain and update your information in a timely manner to keep it accurate, current and complete at all times during the term of the Agreement. You agree that the Company may rely on your information as accurate, current and complete. You acknowledge that if your information is untrue, inaccurate, not current or incomplete in any respect, the Company has the right but not the obligation to terminate this Agreement and your use of the Service at any time with or without notice;
        <br/>
        t.  You will only use an access point or data account (AP) which you are authorized to use;
        <br/>
        u.  You shall not employ any means to defraud the Company or enrich yourself, through any means, whether fraudulent or otherwise, whether or not through any event, promotion or campaign launched by the Company to encourage new subscription or usage of the Service by new or existing passengers;
        <br/>
        v.  You will not use the Service or any part thereof for carrying contraband items and if, in the event that you display suspicious behavior, you will fully comply with the request of the third party service provider, any government authority and / or law enforcement, to inspect any bags and / or items you are carrying with you which may or may not be readily visible;
        <br/>
        w.  You are aware that when responding to Customers or recipients or passengers requests for transportation services, standard telecommunication charges will apply and which shall be solely borne by you;
        <br/>
        x.  Before collecting goods from customers for delivery, you can commence an inspection of the exterior of the sealed package of the products in order to ensure that they are visibly fine/legal goods as per the law. If in doubt, you can cancel the trip on the spot and contact the authorized person(s) as intended and required; 
        <br/>
        y.  After collecting goods from customers, you must sign a receipt stating that you have received the products in good conditions. Hence, any damage or defect caused to the product while they remain in your possession shall be your responsibility and must be compensated for personally by you;
        <br/>
        z.  You shall not impair or circumvent the proper operation of the network which the Service operates on;
        <br/>
        aa. You agree that the Service is provided on a reasonable effort basis;
        <br/>
        You unconditionally agree to assume full responsibility and liability for all loss or damage suffered by yourself, the Customers or recipient or Passenger, the Company or any third party as a result of any breach of the Terms and Conditions.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        6. LICENSE GRANT & RESTRICTIONS
    </h3>
    <p>
        The Company and its licensors, where applicable, hereby grants you a revocable, non-exclusive, non- transferable, non-assignable, personal, limited license to use the Application and/or the Software, solely for your own personal purpose, subject to the terms and conditions of this Agreement. All rights not expressly granted to you are reserved by the Company and its licensors.
    </p>
    <p>
        <strong>
            a) You shall not:
        <br>
        A) License, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Application and/or the Software in any way;
        </br>
        </strong>
        <br/>
        1)  Modify or make derivative works based on the Application and/or the Software;
        <br/>
        2)  Create internet “links” to the Application or “frame” or “mirror” the Software on any other server or wireless or internet-based device;
        <br/>
        3)  Reverse engineer or access the Software in order to (i) build a competitive product or service, (ii) build a product using similar ideas, features, functions or graphics of the Application and/or the Software, or (iii) copy any ideas, features, functions or graphics of the Application and/or the Software;
        <br/>
        4)  Launch an automated program or script, including, but not limited to, web spiders, web crawlers, web robots, web ants, web indexers, bots, viruses or worms, or any program which may make multiple server requests per second, or unduly burdens or hinders the operation and/or performance of the Application and/or the Software;
        <br/>
        5)  Use any robot, spider, site search/retrieval application, or other manual or automatic device or process to retrieve, index, “data mine”, or in any way reproduce or circumvent the navigational structure or presentation of the Service or its contents;
        <br/>
        6)  Post, distribute or reproduce in any way any copyrighted material, trademarks, or other proprietary information without obtaining the prior consent of the owner of such proprietary rights;
        <br/>
        7)  Remove any copyright, trademark or other proprietary rights notices contained in the Service.
        <br/>
        <strong>
            B) You may use the Software and/or the Application only for your personal, non-commercial purposes and shall not use the Software and/or the Application to:
        </strong>
        <br/>
        1) send spam or otherwise duplicative or unsolicited messages;
        <br/>
        2) send or store infringing, obscene, threatening, libelous, or otherwise unlawful or tortious material, including but not limited to materials harmful to children or violation of third party privacy rights;
        <br/>
        3) send material containing software viruses, worms, trojan horses or other harmful computer code, files, scripts, agents or programs;
        <br/>
        4) interfere with or disrupt the integrity or performance of the Software and/or the Application or the data contained therein;
        <br/>
        5) attempt to gain unauthorized access to the Software and/or the Application or its related systems or networks; or
        <br/>
        6) Impersonate any person or entity or otherwise misrepresent your affiliation with a person or entity
        <br/>
        7) to abstain from any conduct that could possibly damage the Company’s reputation or amount to being disreputable.
        <br/>
    </p>
        <strong>
            INTELLECTUAL PROPERTY OWNERSHIP
        <strong>
    <p>
        The Company and its licensors, where applicable, shall own all right, title and interest, including all related intellectual property rights, in and to the Software and/or the Application and by extension, the Service and any suggestions, ideas, enhancement requests, feedback, recommendations or other information provided by you or any other party relating to the Service. The Terms of Use do not constitute a sale agreement and do not convey to you any rights of ownership in or related to the Service, the Software and/or the Application, or any intellectual property rights owned by the Company and/or its licensors. The Company’s name, the Company’s logo, the Service, the Software and/or the Application and the third party transportation providers’ logos and the product names associated with the Software and/or the Application are trademarks of the Company or third parties, and no right or license is granted to use them. For the avoidance of doubt, the term the Software and the Application herein shall include its respective components, processes and design in its entirety.
    </p>
    <br/>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        7. PAYMENT TERMS
    </h3>
    <p>
        Any fees, which the Company may charge you for the Service, are due immediately and are non-refundable (“Service Fee”). This no-refund policy shall apply at all times regardless of your decision to terminate your usage, our decision to terminate or suspend your usage, disruption caused to the Service either planned, accidental or intentional, or any reason whatsoever.
        <br/>
        YOU ACKNOWLEDGE THAT THE TOTAL AMOUNT OF FARE PAID TO YOU BY THE PASSENGER OR CUSTOMER INCLUDES THE SOFTWARE USAGE FEE, WHICH YOU ARE COLLECTING ON BEHALF OF THE COMPANY. SUCH SOFTWARE USAGE FEE MAY BE UP TO 100% OF THE FARE STIPULATED FOR THE SERVICE FOR EACH TIME THE PASSENGER OR CUSTOMER COMPLETES A RIDE, WHICH SHALL BE DETERMINED BY THE COMPANY, AT ITS DISCRETION, FROM TIME TO TIME.
        <br/>
        Each day, when the Service Payable to the Company reaches a maximum threshold set by the company (“Service Fee Threshold”), you shall, on your own accord, deposit the Service Fee for the day in favor of the Company in such method, and to such person, as determined by the Company from time to time. You understand that significance of this requirement and you acknowledge that you may be blacklisted for failure to comply with the aforesaid requirement and that your ability to use the Service shall be barred until due compliance is made in this regard.
        <br/>
        The Company may, at its sole discretion, make promotional offers with different features and different rates to any of the Passengers or Customers whereby these promotional offers shall accordingly be honored by you. The Company may determine or change the Service Fee as the Company deems in its absolute discretion as necessary or appropriate for the business.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        8. PAYMENT BY CUSTOMER OR RECEIPIENT OR PASSENGER
    </h3>
    <p>
        The Customer or recipient or passenger may choose to pay for the Service by cash and where available, the Company shall reimburse to you the portion of the said payment that is due to you in case of any offer or promotional activity done by the company as per these Terms and Conditions.
        <br/>
        Any complaints that the Customer or recipient or passenger shall have regarding the transportation provided by you shall be taken up by the Customer or recipient or passenger with you directly.
        <br/>
        The Company retains the right to suspend the processing of any transaction where it reasonably believes that the transaction may be fraudulent, illegal or involves any criminal activity or where it reasonably believes the Customer or recipient or passenger to be in breach of the Terms and Conditions between the Customer or recipient or passenger and the Company. In such an event, you shall not hold the Company liable for any withholding of, delay in, suspension of or cancellation of, any payment to you.
        <br/>
        You agree that you will cooperate in relation to any criminal investigation that is required and to assist the Company in complying with any internal investigations, instructions from the authorities or requirements of prevailing laws or regulations in place.

    </p>
    <p>
        <!--EndFragment-->
    </p>
    
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        9. TAXES
    </h3>
    <p>
        You agree that this Agreement shall be subject to all prevailing statutory taxes, duties, fees, charges and/or costs, however denominated, as may be in force and in connection with any future taxes that may be introduced at any point of time. You further agree to use your best efforts to do everything necessary and required by the relevant laws to enable, assist and/or defend the Company to claim or verify any input tax credit, set off, rebate or refund in respect of any taxes paid or payable in connection with the Services supplied under this Agreement.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        10. CONFIDENTIALITY
    </h3>
    <p>
        You shall maintain in confidence all information and data relating to the Company, its services, products, business affairs, marketing and promotion plans or other operations and its associated companies which are disclosed to you by or on behalf of the Company (whether orally or in writing and whether before, on or after the date of this Agreement) or which are otherwise directly or indirectly acquired by you from the Company, or any of its affiliated companies, or created in the course of this Agreement. You shall further ensure that you only use such confidential information in order to perform the Services, and shall not without the Company’s prior written consent, disclose such information to any third-party nor use it for any other purpose. You shall only disclose such information to such officers, employees and agents as need to know it to fulfill its obligations under this Agreement.
        <br/>
        The above obligations of confidentiality shall not apply to the extent that you can show that the relevant information:
        <br/>
        pp. was at the time of receipt already in the Recipient’s possession;
        <br/>
        qq. is, or becomes in the future, public knowledge through no fault or omission of the Recipient;
        <br/>
        rr. was received from a third-party having the right to disclose it; or
        <br/>
        ss. is required to be disclosed by law.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        11. PERSONAL DATA PROTECTION
    </h3>
    <p>
        You agree and consent to the Company using and processing your Personal Data for the Purposes and in the manner as identified hereunder.
        <br/>
        For the purposes of this Agreement, “Personal Data” means information about you, from which you are identifiable, including but not limited to your name, identification card number, birth certificate number, passport number, nationality, address, telephone number, credit or debit card details, race, gender, date of birth, email address, any information about you which you have provided to the Company in registration forms, application forms or any other similar forms and/or any information about you that has been or may be collected, stored, used and processed by the Company from time to time and includes sensitive personal data such as data relating to health, religious or other similar beliefs
        <br/>
        The provision of your Personal Data is voluntary. However, if you do not provide the Company your Personal Data, your request for the Application may be incomplete and the Company will not be able to process your Personal Data for the Purposes outlined below and may cause the Company to be unable to allow you to use the Service.
        <br/>
        The Company may use and process your Personal Data for business and activities of the Company which shall include, without limitation the following (“the Purpose”):
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
        tt. To perform the Company’s obligations in respect of any contract entered into with you;
        <br/>
        uu. To provide you with any services pursuant to the Terms and Conditions herein;
        <br/>
        vv. To process your participation in any events, promotions, activities, focus groups, research studies, contests, promotions, polls, surveys or any productions and to communicate with you regarding your attendance thereto;
        <br/>
        ww. Process, manage or verify your application for the Service pursuant to the Terms and Conditions herein;
        <br/>
        xx. To validate and/or process payments pursuant to the Terms and Conditions herein;
        <br/>
        yy. To develop, enhance and provide what is required pursuant to the Terms and Conditions herein to meet your needs;
        <br/>
        zz. To process any refunds, rebates and/or charges pursuant to the Terms and Conditions herein;
        <br/>
        aaa. To facilitate or enable any checks as may be required pursuant to the Terms and Conditions herein;
        <br/>
        bbb. To respond to questions, comments and feedback from you;
        <br/>
        ccc. To communicate with you for any of the purposes listed herein;
        <br/>
        ddd. For internal administrative purposes, such as auditing, data analysis, database records;
        <br/>
        eee. For purposes of detection, prevention and prosecution of crime;
        <br/>
        fff. For the Company to comply with its obligations under law;
        <br/>
        ggg. To send you alerts, newsletters, updates, mailers, promotional materials, special privileges, festive greetings from the Company, its partners, advertisers and/or sponsors;
        <br/>
        hhh. To notify and invite you to events or activities organized by the Company, its partners, advertisers, and/or sponsors;
        <br/>
        iii.    To share your Personal Data amongst the companies within the Company’s group of companies comprising the subsidiaries, associate companies and/or jointly controlled entities of the holding company of the group (“the Group”) and with the Company’s and Group’s agents, third party providers, developers, advertisers, partners, event companies or sponsors who may communicate with you for any reasons whatsoever.
        <br/>
        If you do not consent to the Company processing your Personal Data for any of the Purposes, please notify the Company using the support contact details as provided in the Application.     
        <br/>
        If you do not consent to the Company processing your Personal Data for any of the Purposes, please notify the Company using the support contact details as provided in the Application. If any of the Personal Data that you have provided to us changes, for example, if you change your e- mail address, telephone number, payment details or if you wish to cancel your account, please update your details by sending your request to the support contact details as provided in the Application. We will, to the best of our abilities, effect such changes as requested within fourteen (14) working days of receipt of such notice of change.
        <br/>
        By submitting your information, you consent to the use of that information as set out in the form of submission and in this Agreement.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        12. THIRD PARTY INTERACTIONS
    </h3>
    <p>
        During use of the Service, you may enter into correspondence with, provide services to, or participate in promotions of third party providers, advertisers or sponsors showing their goods and/or services through the Service, Software and/or the Application. Any such activity, and any terms, conditions, warranties or representations associated with such activity, is solely between you and the applicable third-party. The Company and its licensors shall have no liability, obligation or responsibility for any such correspondence, purchase, transaction or promotion between you and any such third- party. The Group does not endorse any applications or sites on the Internet that are linked through the Service, Application and/or the Software, and in no event shall the Company, its licensors or the Group be responsible for any content, products, services or other materials on or available from such sites or third party providers. The Company provides the Service to you pursuant to the Terms and Conditions. You recognize, however, that certain third party providers of transportation, goods and/or services may require your agreement to additional or different terms and conditions prior to your use of or access to such goods or services, and the Company is not a party to and disclaims any and all responsibility and/or liability arising from such agreements between you and the third party providers.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        13. INDEMNIFICATION
    </h3>
    <p>
        By agreeing to this Agreement upon using the Service, you agree that you shall defend, indemnify and hold the Company, its licensors and each such party’s parent organizations, subsidiaries, affiliates, officers, directors, members, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys’ fees and costs) arising out of or in connection with: (a) your violation or breach of any term of this Terms and Conditions or any applicable law or regulation, including any local laws or ordinances, whether or not referenced herein; (b) your violation of any rights of any third party, including, but not limited to passengers of your vehicle or the vehicle that you have control over, other motorists, and pedestrians, as a result of your own interaction with any third party (c) your use (or misuse) of the Application and/or Software; and (d) your ownership, use or operation of a motor vehicle or passenger vehicle, including your carriage of Passengers or Customers who have procured your transportation services via the Service, or of their goods.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        14. DISCLAIMER OF WARRANTIES
    </h3>
    <p>
        THE COMPANY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE SERVICES, APPLICATION AND/OR THE SOFTWARE. THE COMPANY DOES NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE SERVICE, APPLICATION AND/OR THE SOFTWARE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE SERVICE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIALS PURCHASED OR OBTAINED BY YOU THROUGH THE APPLICATION WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS IN THE APPLICATION AND/OR THE SOFTWARE WILL BE CORRECTED, OR (F) THE APPLICATION OR THE SERVER(S) THAT MAKE THE APPLICATION AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS, OR (G) THE APPLICATION AND/OR THE SOFTWARE TRACKS YOU OR THE VEHICLE USED BY THE TRANSPORTATION PROVIDER. THE SERVICE IS PROVIDED TO YOU STRICTLY ON AN “AS IS” BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY EXCLUDED AND DISCLAIMED TO THE HIGHEST AND MAXIMUM EXTENT.
        <br/>
         THE COMPANY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE AS TO THE RELIABILITY, SAFETY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF ANY SERVICES, INCLUDING BUT NOT LIMITED TO THE TRANSPORTATION SERVICES PROVIDED BY YOU TO CUSTOMERS OR PASSENGERS THROUGH THE USE OF THE SERVICE, APPLICATION AND/OR THE SOFTWARE. YOU ACKNOWLEDGE AND AGREE THAT THE ENTIRE RISK ARISING OUT OF SUCH USE OF THE SERVICES REMAINS SOLELY AND ABSOLUTELY WITH YOU AND YOU SHALL HAVE NO RECOURSE WHATSOEVER AGAINST THE COMPANY.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        15. INTERNET DELAYS
    </h3>
    <p>
        THE SERVICE, APPLICATION AND/OR THE SOFTWARE MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER PROBLEMS INHERENT IN THE USE OF THE INTERNET AND ELECTRONIC COMMUNICATIONS INCLUDING THE DEVICE USED BY YOU BEING FAULTY, NOT CONNECTED, OUT OF RANGE, SWITCHED OFF OR NOT FUNCTIONING. THE COMPANY IS NOT RESPONSIBLE FOR ANY DELAYS, DELIVERY FAILURES, DAMAGES OR LOSSES RESULTING FROM SUCH PROBLEMS
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        16. LIMITATION OF LIABILITY
    </h3>
    <p>
        ANY CLAIMS AGAINST THE COMPANY BY YOU SHALL IN ANY EVENT BE LIMITED TO THE AGGREGATE AMOUNT OF ALL AMOUNTS ACTUALLY PAID BY AND/OR DUE FROM YOU IN UTILISING THE SERVICE DURING THE EVENT GIVING RISE TO SUCH CLAIMS. IN NO EVENT SHALL THE COMPANY AND/OR ITS LICENSORS BE LIABLE TO YOU OR ANYONE FOR ANY DIRECT, INDIRECT, PUNITIVE, ECONOMIC, FUTURE SPECIAL, EXEMPLARY, INCIDENTAL, CONSEQUENTIAL OR OTHER DAMAGES OR LOSSES OF ANY TYPE OR KIND (INCLUDING PERSONAL INJURY OF ANY KIND WHATSOEVER INCLUDING LOSS OF LIFE OR LIMBS OR SERIOUS HARM OF ANY KIND WHATSOEVER, EMOTIONAL DISTRESS AND LOSS OF DATA, GOODS, REVENUE, PROFITS, USE OR OTHER ECONOMIC ADVANTAGE). THE COMPANY AND/OR ITS LICENSORS SHALL NOT BE LIABLE FOR ANY LOSS, DAMAGE OR INJURY WHICH MAY BE INCURRED BY OR CAUSED TO YOU OR TO ANY PERSON FOR WHOM YOU HAVE BOOKED THE SERVICE FOR, INCLUDING BUT NOT LIMITED TO LOSS, DAMAGE OR INJURY ARISING OUT OF, OR IN ANY WAY CONNECTED WITH THE SERVICE, APPLICATION AND/OR THE SOFTWARE, INCLUDING BUT NOT LIMITED TO THE USE OR INABILITY TO USE THE SERVICE, APPLICATION AND/OR THE SOFTWARE, ANY RELIANCE PLACED BY YOU ON THE COMPLETENESS, ACCURACY OR EXISTENCE OF ANY ADVERTISING, OR AS A RESULT OF ANY RELATIONSHIP OR TRANSACTION BETWEEN YOU AND ANY CUSTOMER, PASSENGER OR THIRD PARTY APPLICATION AND/OR THE SOFTWARE, EVEN IF THE COMPANY AND/OR ITS LICENSORS HAVE BEEN PREVIOUSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
        <br/>
        IN ANY EVENT, THE COMPANY SHALL HAVE NO RESPONSIBILITY OR LIABILITY WHATSOEVER FOR ANY DAMAGES, INJURIES, LOSSES OF ANY KIND WHATSOEVER SUFFERED BY THE CUSTOMER OR PASSENGER, EITHER DIRECTLY OR INDIRECTLY, BECAUSE OF YOUR ACT OR OMISSION OR LACK OF CONTROL OVER THE VEHICLE OR AWARENESS OF THE ROAD OR ANY OTHER CAUSE THAT IS ATTRIBUTABLE TO YOU DURING THE COURSE OF THE SERVICE. 
        <br/>
        SIMILARLY, AND IN ANY EVENT, THE COMPANY SHALL NOT BE HELD RESPONSIBLE OR LIABLE IN ANY MANNER WHATSEOVER FOR ANY DAMAGE, INJURIES, LOSSES OF ANY KIND WHATSOEVER SUFFERED BY YOU, EITHER DIRECTLY OR INDIRECTLY, BY THE ACTION OR OMISSION OF A PASSENGER OR A CUSTOMER.
        <br/>
        IN NO EVENT WHATSOEVER, SHALL THE COMPANY BE HELD RESPONSIBLE OR LIABLE, IN ANY MANNER WHATSOEVER, FOR CRIMINAL INVESTIGATION BY POLICE OR OTHER LAW ENFORCEMENT AUTHORITIES, FOR YOUR ACTIONS OR INACTIONS NOR THE ACTIONS OR INACTIONS OF A PASSENGER OR CUSTOMER INCLUDING BUT NOT LIMITED TO, FOR YOUR BREACH OF YOUR REPRESENTATIONS, WARRANTIES AND ACKNOWLEDGEMENTS MADE THROUGHOUT THIS TERMS AND CONDITIONS AND SPECIFICALLY THOSE REPRESENTATIONS AND WARRANTIES MADE BY YOU IN PARAGRAPH  ABOVE.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        17. OPTIONAL INSURANCE PREMIUM
    </h3>
    <p>
        At your sole discretion, you may opt to reap the benefits of the Company’s good relation with insurance companies and secure an optional insurance coverage against damages to your vehicle or injuries to yourself and others, at such premium and conditions as the Company offers from time to time. In the event that you opt for such an option, the relevant insurance premium shall be forthwith paid to the Company by you in addition to the aforesaid Service Fees for the use of the Company’s Software, Application and / or Service.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        18. NOTICE
    </h3>
    <p>
        The Company may give notice by means of a general notice on the Application, electronic mail to your email address in the records of the Company, or by written communication sent by Registered mail or pre-paid post to your address in the record of the Company. Such notice shall be deemed to have been given upon the expiration of 48 hours after mailing or posting (if sent by Registered mail or pre-paid post) or 1 hour after sending (if sent by email). You may give notice to the Company (such notice shall be deemed given when received by the Company) by letter sent by courier or registered mail to the Company using the contact details as provided in the Application.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        19. ASSIGNMENT
    </h3>
    <p>
        The agreement as constituted by the terms and conditions as modified from time to time may not be assigned by you without the prior written approval of the Company but may be assigned without your consent by the Company. Any purported assignment by you in violation of this section shall be void.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        20. MISCELLANEOUS
    </h3>
    <p>
        This Agreement shall be governed by Bangladesh law, without regard to the choice or conflicts of law provisions of any jurisdiction, and any disputes, actions, claims or causes of action arising out of or in connection with this Agreement or the Service shall be subject to the exclusive jurisdiction of the courts of Bangladesh to which you hereby agree to submit to. In the event that the law in an Alternate Country does not allow jurisdiction to be that of the courts of Bangladesh or where judgment of a Bangladesh court is unenforceable in the Alternate Country, unresolved disputes shall be referred to the Bangladesh International Arbitration Centre (“BIAC”), in accordance with the Rules of the BIAC as modified or amended from time to time (the “Rules”) by a sole arbitrator appointed by the mutual agreement of the parties (the “Arbitrator”). If parties are unable to agree on an arbitrator, the Arbitrator shall be appointed by the President of BIAC in accordance with the Rules.
        <br/>
        The seat and venue of the arbitration shall be Dhaka, in the English language and the fees of the Arbitrator shall be borne equally by the parties, provided that the Arbitrator may require that such fees be borne in such other manner as the Arbitrator determines is required in order for this arbitration clause to be enforceable under applicable law. 
        <br/>
        No joint venture, partnership, employment, or agency relationship exists between you, the Company or any third party provider as a result of this Agreement or use of the Service. 
        <br/>
        If any provision of the Agreement is held to be invalid or unenforceable, such provision shall be struck and the remaining provisions shall be enforced to the fullest extent under law. This shall, without limitation, also apply to the applicable law and jurisdiction as stipulated above. 
        <br/>
        The failure of the Company to enforce any right or provision in the Agreement shall not constitute a waiver of such right or provision unless acknowledged and agreed to by the Company in writing. The Agreement comprises the entire agreement between you and the Company and supersedes all prior or contemporaneous negotiations or discussions, whether written or oral (if any) between the parties regarding the subject matter contained herein. 
        <br/>
        You hereby agree that the Company is entitled to terminate this Agreement immediately in the event that you are found to be in breach of any of the terms stipulated in this Agreement. For the avoidance of doubt, the termination of this Agreement shall not require the Company to compensate, reimburse or cover any cost incurred by you, including but not limited to the credit reserved with the Company or any other monies paid to the Company in the course of performing your obligations under this Agreement.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
</div>