<h1 class="page-heading">
    HeavyGari Terms Of Use
</h1>
<div class="text-widget -style-2">
    <h3>
        How we collect information
    </h3>
	<p>
		This privacy policy describes how the www.heavygari.com website and related application (the "Site", "we" or "us") collects, uses, shares and protects the personal information that we collect through this Site. Heavygari technologies ltd has established this Site to link up users who need something shipped or delivered ("Customers") with individuals who will provide the shipping and delivery services ("Couriers" or “parcel”) or teams of affiliated Couriers ("Teams"). This policy also applies to any mobile applications that we develop for use with our services and Team specific pages on the Site, and references to this "Site", "we" or "us" is intended to also include these mobile applications. Please read below to learn more about our information practices. By using this Site, you agree to these practices.
    </p>

    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Information provided by your web browser
    </h3>
	<p>
		You have to provide us with personal information like your name, contact no, mailing address and email id, our app will also fetch your location information in order to give you the best service. Like many other websites, we may record information that your web browser routinely shares, such as your browser type, browser language, software and hardware attributes, the date and time of your visit, the web page from which you came, your Internet Protocol address and the geographic location associated with that address, the pages on this Site that you visit and the time you spent on those pages. This will generally be anonymous data that we collect on an aggregate basis. We may also use Google Analytics or a similar service to gather statistical information about the visitors to this Site and how they use the Site. This, also, is done on an anonymous basis. We will not try to associate anonymous data with your personally identifiable data. If you would like to learn more about Google Analytics, please click here.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Personal information that you provide
    </h3>
	<p>
		If you want to use our service or contact a Heavygari member, you must create an account on our Site. To establish your account, we will ask for personally identifiable information that can be used to contact or identify you, which may include your name, phone number, and e-mail address. We may also collect demographic information about you, such as your zip code, and allow you to submit additional information that will be part of your Heavygari profile.
    </p>
	<p>
		Other than basic information that we need to establish your account, it will be up to you to decide how much information to share as part of your profile. We encourage you to think carefully about the information that you share and we recommend that you guard your identity and your sensitive information. Of course, you can review and revise your profile at any time.
    </p>
	<p>
		From time to time, we may run contests or promotions and ask for a postal mailing address and other personal information relating to the contest or promotion. It will always be your choice whether to provide your personal information in order to participate in these events.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Payment Information
    </h3>
	<p>
		Our payment method is cash only. We will have your address and contact no. in order to identify the customer.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Information about others
    </h3>
	<p>
		If you are a Customer planning to ship something using the Heavygari service, we will ask you for information about the sender location and contact and recipient location and contact of the shipment.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Session and Persistent Cookies
    </h3>
    <p>
        As is commonly done on websites, we may use cookies and similar technology to keep track of our users and the services they have elected. A "cookie" is a small text file containing alphanumeric characters that is stored on your computer’s hard drive and uniquely identifies your browser. We use both "session" and "persistent" cookies. Session cookies are deleted after you leave our website and when you close your browser. We use data collected with session cookies to enable certain features on our Site, to help us understand how users interact with our Site, and to monitor at an aggregate level Site usage and web traffic routing.
    </p>
    <p>
        If you have created an account, we will also use persistent cookies that remain on your computer’s hard-drive between visits, so that when you return to our Site we can remember who you are and your preferences. For example, after you log out of our Site, these persistent cookies can enable you to return to our Site without the need to log back in.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Use of Cookies and similar Technology
    </h3>
    <p>
        We may allow business partners who provide services to our Site to place cookies on your computer that assist us in analyzing usage data. We do not allow these business partners to collect your personal information from our website except as may be necessary for the services that they provide.
    </p>
    <p>
       You can manage these cookies. For example, you can configure your browser to accept all cookies, reject all cookies, or notify you when a cookie is set. If you disable cookies, however, it may interfere with the functionality of our Site and you may not be able to use all of the Site’s features.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Web Beacons
    </h3>
    <p>
        We may also use web beacons or similar technology to help us track the effectiveness of our communications. For example, if you have elected to receive any of our e-mail newsletters, we may use technology that allows us to see how many recipients have opened the message and how many have clicked on one of its links.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Advertising Cookies
    </h3>
    <p>
        We may use third parties, such as Google, to serve ads about our website over the internet. These third parties may use cookies to identify ads that may be relevant to your interest (for example, based on your recent visit to our website), to limit the number of times that you see an ad, and to measure the effectiveness of the ads. You can disable these advertising cookies by opting out at 
        <a href="http://www.google.com/privacy_ads.html.">
            http://www.google.com/privacy_ads.html.
        </a>
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        What We Do With the information We Collect
    </h3>
    <p>
        We will generally use the information that we collect to provide our services, to monitor and analyze visitor activity on our website, promote and support our services, and develop a knowledge base regarding our website users. As detailed below, certain information that you provide may be available to visitors to the Site, and some information will be shared between Customers and Teams and/or Couriers.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Registered Heavygari users
    </h3>
    <p>
        When you register on our Site or App, you will create a user name and profile. Your user name and profile will be accessible by the users of our Site. With your prior permission, we may also share information about your use of the service on third party sites. For example, we may allow you to elect to share information about the services we provide through this site as updates to your Facebook or Linkedin or Twitter accounts.
    </p>
    <p>
        If you post a job as a Customer, we may publish the address of the pickup and drop off locations on the Site, viewable by all Heavygari Teams or Couriers. For example, if you choose to post a job to the Site for a specific Team, we will publish the address of the pickup and drop off locations on the Site or App, viewable to that specific Team.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Your contact information
    </h3>
    <p>
        When you provide us with your contact information, we will use that information to communicate with you about your use of our service. For example, when you have entered into a confirmed transaction with us, we will use your contact information to notify you of the transaction. We will also share your contact information with the couriers so that you may contact each other about the transaction. If you agree, we may also use your e-mail address to send you a newsletter or other information about our services or about other products or services in which you may be interested. You may change your preferences at any time, though you will not be able to opt out of messages relating to your use of our service.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Testimonials
    </h3>
    <p>
        We may allow you to submit testimonials about your experience with our Site. If you provide a testimonial, we may post it on this website along with your name. If you want your testimonial removed, please contact us at support@heavygari.com.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Ratings and Reviews
    </h3>
    <p>
        If you are a Driver, you will be able to rate and review a customer. If you choose to submit a rating, this will be aggregated with other ratings and available to other registered users of the Site. If you submit a review, your review along with your user name will be posted for everyone to see.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Anonymous Data
    </h3>
    <p>
        We use the anonymous data that we collect on an aggregate basis to gain a better understanding of the visitors to our Site and to improve our website and product offerings. We reserve the right to license or sell this aggregated information to third parties for industry analysis, demographic profiling and other purposes, but this information will not contain your individually identifiable personal information.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Shipment recipients and referrals
    </h3>
    <p>
        We may provide an e-mail/ text messaging service for alerting recipients that a shipment is on its way through the Heavygari service. If you elect to provide us with an e-mail address or contact no. of the person or organization receiving your shipment, we will send the recipient a notification message of the shipment and the expected delivery time frame. We may also use the recipient's contact information from time to time to send the recipient a promotional message about our services, and will always give the recipient the option to decline any future promotional messages.
    </p>
    <p>
        We may also provide you with the opportunity to refer a potential customer to our Heavygari services and earn a commission on the referral. To take advantage of this opportunity, you will need to provide Heavygari with the e-mail address of the potential customer. We will include your name in the promotional message that we send the potential customer, but will always give the potential customer the option to decline any future promotional messages.
    </p>
    <p>
        Of course, we will not share recipient or potential customer contact information with other third parties for their marketing purposes. We may, however, share the recipient or potential customer contact information with Teams or Super Teams for purpose of performing the delivery services.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Information shared with our business partners
    </h3>
    <p>
       We may use business partners to help us design and operate our Site and provide services to support the Site. We may also hire a company to run certain website applications, provide data storage and processing services, or help us analyze our data. These business partners may have access to the personal information that we keep, but only so that they may perform these tasks on our behalf. We do not allow these business partners to make any independent commercial use of the individually identifiable information that we store, to share such data with third parties or from making the data publicly available. Keep in mind, however, that if you establish a separate relationship with one of these business partners, the information you provide directly to that organization will be subject to its terms of use and its privacy practices.
    </p>
    <p>
        We may also provide your personal information to our business partners or other trusted entities for the purpose of providing you with information on goods or services we believe will be of interest to you. You can, at any time, opt out of receiving such communications. Third party vendors use cookies to serve ads based on a user's prior visits to Heavygari. You can disable these specific cookies that track prior visits for the sake of follow-up advertising by opting out at 
        <a href="http://www.google.com/privacy_ads.html.">
            http://www.google.com/privacy_ads.html.
        </a>
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Information shared with Super Teams
    </h3>
    <p>
        Teams for different geographic areas may be organized under a larger entity (a "Super Team"). Any information that you share with the Site may also be shared with a Super Team and may be used by the Super Team in any manner consistent with this Privacy Policy as if the Super Team were us.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Business operations, law enforcement and legal actions
    </h3>
    <p>
        We may release your information without prior notice when we believe it is appropriate to prevent fraud or to prevent or stop activity that we know or suspect may be illegal, unethical or legally actionable; to comply with law or to cooperate with law enforcement activity or other governmental request; to respond to subpoenas, court orders or administrative agency requests for information; to enforce our policies; to protect the rights, property and safety of our business and of others; or when otherwise required by law. If there is a sale or merger of the company, division or business unit that operates this Site, we may also transfer the information we have collected in connection with such sale or merger.
    </p>
    <p>
        We will use the information we collect to continuously improve our business and our website development. Your comments and suggestions are always appreciated. Please contact us at support@heavygari.com if you have any comments or suggestions.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Third Party Websites
    </h3>
    <p>
        This Site may contain links to other websites operated by companies that are not affiliated with us. Also, you may have come to this website from a website that is not operated by us. We are not responsible for the operation of these other sites or the information that they collect from their visitors. If you would like to know how another site collects and uses your information, please review its privacy policy.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Changes to Your Information
    </h3>
    <p>
    </p>
        The information you provide us isn't set in stone. You may review, update, correct or delete the personal information in your profile at any time. If you would like us to remove your information from our records, please contact as at support@heavygari.com. We will attempt to accommodate your request if we do not have a legal obligation to retain the record.
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        How We Protect Your Data
    </h3>
    <p>
        If there is a breach of security involving your personal data that requires notification, you agree that we may notify you about the breach via email or by a conspicuous posting on this Site. We will make the notification without unreasonable delay, consistent with the legitimate needs of law enforcement and any measures necessary to determine the scope of the breach and restore the reasonable integrity of the data system.
    </p>
    <p>
        If you have any questions about security on our website, you can contact us at support@heavygari.com.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Data Integrity and Retention
    </h3>
    <p>
        We use the information that we collect about you only for the purposes for which it is collected and consistent with this policy. We keep information provided to us for as long as we believe necessary for our business purposes and as permitted by applicable law.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Changes to This Policy
    </h3>
    <p>
        Our business and the laws that regulate us change from time to time, and we reserve the right to change this policy. If we do change this policy, we will post the revised version on this Site. If we propose to change our policy in a way that would permit us to make additional uses of information that we had previously collected about you, we will provide you with a meaningful way to opt out of those additional uses.
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Data Subject to Bangladesh Law
    </h3>
    <p>
        Heavygari is located and operates its website in Bangladesh. Depending on where you live, the information that you provide and that this Site collects may be stored on servers that are outside of your state, province, country or other governmental jurisdiction, and the privacy laws that apply may not be as protective as those in your home jurisdiction. If you are located outside the Bangladesh and choose to provide information to us, Bangladesh transfers personal information to other countries and processes it there. By using this website, you consent to this transfer and processing of data.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Children
    </h3>
    <p>
        While our Site is available for all to come visit, you must be an adult to register on our website and use our services. We will not knowingly collect information about children under the age of 18. If you are a parent who believes that we have collected information about a child under age 18, please contact us at support@heavygari.com with your child's name and address, and we will be happy to delete the information we have about your child from our records in accordance with children’s act 2013.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        This Policy is an Agreement
    </h3>
    <p>
        When you visit this Site, you are accepting the practices described in this Privacy Policy.
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Contacting Us
    </h3>
    <p>
        Please contact us at support@heavygari.com and let us know if you have any questions or comments about our policies
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Summary
    </h3>
    <p>
        <strong>
            HeavyGari collects:
        </strong>
        <br/>
        Information that you provide to Heavygari, such as when you create your Heavygari account.
        <br/>
        Information created when you use our services, such as location, usage and device information.
        <br/>
        Information from other sources, such as Heavygari partners and third parties that use Heavygari’s API.
        <br/>
        Heavygari collects the following categories of information:
        <br/>
        Information you provide
        <br/>
        This includes information submitted when you:
        <br/>
        Create or update your Heavygari account, which depending on your location and the Heavygari services you use may include your name, email, phone number, login name and password, address, payment or banking information, government identification numbers, birth date, and photo
        <br/>
        Submit information about your vehicle or insurance (for driver partners)
        <br/>
        Consent to a background check (for driver partners where permitted by law)
        <br/>
        Request services through the Heavygari app or website
        <br/>
        Contact Heavygari, including for customer support
        <br/>
        Contact other Heavygari users through our services
        <br/>
        Complete surveys sent to you by Heavygari or on behalf of HeavygariEnable features that require Heavygari's access to your address book or calendar
        <br/>
        Information created when you use our services
    </p>
    <p>
        <strong>
            This includes:
        </strong>
        <br/>
        Location Information
        <br/>
        Depending on the Heavygari services that you use, and your app settings or device permissions, heavygari may collect your precise or approximate location information as determined through data such as GPS, IP address and WiFi.
        <br/>
        If you are a driver or delivery partner, Heavygari collects location information when the Heavygari app is running in the foreground (app open and on-screen) or background (app open but not on screen) of your device.
        <br>
        If you are a driver, Heavygari may collect location information when the Heavygari app is running in the foreground. In certain regions, Heavygari may also collect this information when the Heavygari app is running in the background of your device if this collection is enabled through your app settings or device permissions.
       <br/>
        Drivers and delivery recipients may use the Heavygari app without enabling Heavygari to collect their location information. However, this may affect the functionality available on your Heavygari app. For example, if you do not enable Heavygari to collect your location information, you will have to manually enter your pickup address. In addition, location information will be collected from the driver partner during your trip, even if you have not enabled Heavygari to collect your location information.
        <br/>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Transaction Information
    </h3>
    <p>
        We collect transaction details related to your use of our services, including the type of services you requested or provided, date and time the service was provided, amount charged, distance traveled, and other related transaction details. Additionally, if someone uses your promotion code, we may associate your name with that person.
    </p> 
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Usage and Preference Information
    </h3>
    <p>
        We collect information about how you interact with our services, preferences expressed, and settings chosen. In some cases, we do this through the use of cookies, pixel tags, and similar technologies that create and maintain unique identifiers. To learn more about these technologies, please see our Cookie Statement.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Device Information
    </h3>
    <p>
        We may collect information about the devices you use to access our services, including the hardware models, operating systems and versions, software, file names and versions, preferred languages, unique device identifiers, advertising identifiers, serial numbers, device motion information, and mobile network information.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Log Information
    </h3>
    <p>
        When you interact with our services, we collect server logs, which may include information like device IP address, access dates and times, app features or pages viewed, app crashes and other system activity, type of browser, and the third-party site or service you were using before interacting with our services.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Calls and text messages
    </h3>
    <p>
        We enable users to call or text each other through the Heavygari apps. For example, in some countries, we enable driver partners and drivers, and delivery partners and recipients, to call or text each other without disclosing their telephone numbers. To provide this service, Heavygari receives some information regarding the calls or texts, including the date and time of the call/text, and the content of the text messages. Heavygari may also use this information for customer support services (including to resolve disputes between users), for safety and security purposes, and for analytics.
    </p>

    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Address book and calendar information
    </h3>
    <p>
        If you permit the Heavygari app to access the address book on your device, we may collect names and contact information from your address book to facilitate social interactions through our services and for other purposes described in this policy or at the time of consent or collection. If you permit the Heavygari app to access the calendar on your device, we collect calendar information such as event title and description, your response (Yes, No, Maybe), date and time, location, and number of attendees.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Information from other sources
    </h3>
    <p>
        <strong>
            These may include:
        </strong>
        <br/>
        Users providing feedback, such as ratings or compliments
        <br/>
        Heavygari business partners through which you create or access your Heavygari account, such as payment providers, social media services, on-demand music services, or apps or websites who use Heavygari's APIs or whose API Heavygari uses
        <br/>
        Insurance providers (if you are a driver or delivery partner)
        <br/>
        Financial services providers (if you are a driver or delivery partner)
        <br/>
        Partner transportation companies (if you are a driver partner who uses our services through an account associated with such a company)
        <br/>
        The owner of an Heavygari for Business or Heavygari Family profile that you use Publicly available sources
        <br/>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Marketing service providers
    </h3>
    <p>
        Heavygari may combine the information collected from these sources with other information in its possession.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        SUMMARY
    </h3>
    <p>
        <strong>
            Heavygari collects and uses information to enable reliable and convenient transportation, delivery and other products and services. We also use the information we collect:
        </strong>
    </p>
    <ul>
        <li>
            To enhance the safety and security of our users and services
        </li>
        <li>
            For customer support
        </li>
        <li>
            For research and development
        </li>
        <li>
            To enable communications to or between users
        </li>
        <li>
            To provide promotions or contests
        </li>
        <li>
            In connection with legal proceedings
        </li>
        <li>
            Heavygari does not sell or share your personal information to third parties for third party direct marketing purposes.

        </li>
    </ul>
    <p>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Heavygari uses the information it collects for purposes including:
    </h3>
    <p>
        Providing services and features
        <br/>
            Heavygari uses the information we collect to provide, personalize, maintain and improve our products and services. This includes using the information to:
        <br/>
        Enable transportation, deliveries, and other services
        <br/>
        Process or facilitate payments for those services
        <br/>
        Offer, obtain, provide or facilitate insurance or financing solutions in connection with our services
        <br/>
        Enable features that allow you to share information with other people, such as when you submit a compliment about a driver partner, refer a friend to Heavygari, split fares, or share your ETA
        <br/>
        Enable features to personalize your Heavygari account, such as creating bookmarks for your favorite places
        <br/>
        Perform internal operations necessary to provide our services, including to troubleshoot software bugs and operational problems, to conduct data analysis, testing, and research, and to monitor and analyze usage and activity trends
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Safety and security
    </h3>
    <p>
        We use your data to help maintain the safety, security and integrity of our services. For example, we collect information from driver partners' devices to identify unsafe driving behavior such as speeding or harsh braking and acceleration, and to raise awareness among driver partners regarding such behaviors. This also includes, for example, our Real-Time ID Check feature, which prompts driver partners to share a selfie before going online. This helps ensure that the driver partner using the app matches the Heavygari account we have on file, preventing fraud and helping to protect other users.
        <br/>
        <!--StartFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <h3>
        Customer support
    </h3>
    <p>
        Heavygari uses the information we collect (including recordings of customer support calls after notice to you and with your consent) to assist you when you contact our customer support services, including to:
        <br/>
        Direct your questions to the appropriate customer support person
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Investigate and address your concerns
    </h3>
    <p>
        Monitor and improve our customer support responses
        <br/>
        Research and development
        <br/>
        We may use the information we collect for testing, research, analysis and product development. This allows us to improve and enhance the safety and security of our services, develop new features and products, and facilitate insurance and finance solutions in connection with our services.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Communications among users
    </h3>
    <p>
        Heavygari uses the information we collect to enable communications between our users. For example, a driver partner may text or call a driver to confirm a pickup location, or a restaurant or delivery partner may call a delivery recipient with information about their order.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Communications from HeavyGari
    </h3>
    <p>
        Heavygari may use the information we collect to communicate with you about products, services, promotions, studies, surveys, news, updates and events. 
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Promotional Codes or discounts.
    </h3>
    <p>
        Heavygari may, in heavygari’s sole discretion, create promotional codes that may be redeemed for Account credit, or other features or benefits related to the Services and/or a Third-Party Provider’s services, subject to any additional terms that Heavygari establishes on a per promotional code basis (“Promo Codes “or “Discounts”). You agree that Promo Codes or discounts: (i) must be used for the intended audience and purpose, and in a lawful manner; (ii) may not be duplicated, sold or transferred in any manner, or made available to the general public (whether posted to a public form or otherwise), unless expressly permitted by Heavygari; 
        <br/>
        (iii) may be disabled by Heavygari at any time for any reason without liability to Heavygari; (iv) may only be used pursuant to the specific terms that Heavygari establishes for such Promo Code or discounts; (v) are not valid for cash; and (vi) may expire prior to your use. Heavygari reserves the right to withhold or deduct credits or other features or benefits obtained through the use of Promo Codes by you or any other user in the event that Heavygari determines or believes that the use or redemption of the Promo Code or discounts was in error, fraudulent, illegal, or in violation of the applicable Promo Code terms or discount terms or these terms. may also use the information to promote and process contests and sweepstakes, fulfill any related awards, and serve you relevant ads and content about our services and those of our business partners. Heavygari may also use the information to inform you about elections, ballots, referenda and other political and policy processes that relate to our services.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <h3>
        Legal proceedings and requirements
    </h3>
    <p>
        We may use the information we collect to investigate or address claims or disputes relating to your use of Heavygari’s services, or as otherwise allowed by applicable law of the country.
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--StartFragment-->
    </p>
    <p>
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
    <p>
        <!--EndFragment-->
    </p>
</div>