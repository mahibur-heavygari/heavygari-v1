@extends('website.layout')

@section('content-body')
<div class="main-container">
    <div class="main-content">
        <div class="content-body">
            <div class="container">
            	<ul class="list-group">
				  <li class="list-group-item"><a href="/page/toc-owner">Driver and Owner Terms and Condition</a></li>
				  <li class="list-group-item"> <a href="/page/toc-customer">Customer and Recepient Terms and Condition</a></li>
				</ul>
            </div>
        </div>
    </div>
</div>
@stop