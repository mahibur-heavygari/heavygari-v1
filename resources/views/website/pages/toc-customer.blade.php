@extends('website.layout')

@section('content-body')
<div class="main-container">
    <div class="main-content">
        <div class="content-body">
            <div class="container">
                @include('website.pages.contents.toc-customer')
            </div>
        </div>
    </div>
</div>
@stop
