<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
            <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
                <title>
                    HeavyGari
                </title>
                <style>
                    a {
            color: #FAA435;
        }
                </style>
            </meta>
        </meta>
    </head>
    <body style="background-color: #d6d6d5;">
        <table bgcolor="#d6d6d5" border="0" cellpadding="0" cellspacing="0" id="" style="width:100%!important;height:100%!important;line-height:100%!important;border-spacing:0;border-collapse:collapse;background-color:#d6d6d5;margin:0;padding:0;border:none">
            <tbody>
                <tr>
                    <td align="center" class="" style="vertical-align:top" valign="top">
                        <table bgcolor="#f8f8f9" border="0" cellpadding="0" cellspacing="0" class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;border:none">
                                            <tbody>
                                                <tr>
                                                    <td align="center" background="" bgcolor="#FAA435" class="" style="height:50px;" valign="bottom">
                                                        <div>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none">
                                                                <tbody>
                                                                    <tr>
                                                                        <td height="10px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 6%; min-width: 15px; margin: 0; padding: 0">
                                                                        </td>
                                                                        <td>
                                                                            <a href="https://heavygari.com" target="_blank"><img alt="HeavyGari" src="{{URL::asset('emails/logo-white.png')}}" style="outline:none;text-decoration:none;display:block">
                                                                            </img></a>
                                                                        </td>
                                                                        <td style="width: 85%;font-size: 20px; color: white; min-width: 15px; margin: 0; text-align:right; padding-right: 50px; padding-left: 0; padding-top: 0px; padding-bottom: 0;">
                                                                            <b>
                                                                                @if(isset($title))
                                                                                    {{$title}}
                                                                                @endif
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="10px">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @yield('content')
                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="right">
                                                        <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#ffffff;border:none" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="padding-right:15px;padding-left:15px">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td height="25px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td bgcolor="#eee" height="1" style="background-color:#ddd;font-size:0px;line-height:0px;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="padding-top:10px;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px; vertical-align: middle">
                                                                                                                        Thank you very much for choosing HeavyGari. If you have any queries, please call our customer care at
                                                                                                                        <a href="tel:+8801909222777">
                                                                                                                            <img alt="..." src="{{URL::asset('emails/phone-outline.png')}}" style="width: 18px; display: inline-block; vertical-align: middle;" width="20">
                                                                                                                                +8801909222777.
                                                                                                                            </img>
                                                                                                                        </a>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="padding-right:15px;padding-left:15px">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td align="left" style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left; color: #898989;">
                                                                                                                        Best regards,
                                                                                                                        <br>
                                                                                                                            The HeavyGari Team
                                                                                                                        </br>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td height="40px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- FOOTER -->
                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="background: #F79F35 url('{{URL::asset('emails/bg.png')}}') bottom left repeat-x;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;border:none" width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    
                                                                                    <td style="padding:0 15px">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:280px;table-layout:fixed;border:none">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td style="padding-top:40px">
                                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" id="ogo" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <a href="https://www.heavygari.com" target="_blank" class="email-link">
                                                                                                                                                            <img alt="Heavygari logo" src="{{URL::asset('emails/logo-text.png')}}" style="outline:none;text-decoration:none;display:block" width="120" class="email-link" />
                                                                                                                                                        </a>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <div align="left">
                                                                                                                            <table align="right" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:300px;border:none">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td style="padding-top:40px">
                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td style="padding-right:20px">
                                                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="32">
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <a href="https://www.facebook.com/Heavygari-1970065606543494/" target="_blank" class="email-link">
                                                                                                                                                                                <img alt="Share on Facebook" height="34" src="{{URL::asset('emails/facebook.png')}}" style="outline:none;text-decoration:none;display:block;border:none" width="34"/>
                                                                                                                                                                            </a>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                        <td style="padding-left:10px;padding-right:10px">
                                                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="32">
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <a data-saferedirecturl="" href="https://twitter.com/heavygari" target="_blank" class="email-link">
                                                                                                                                                                                <img alt="Share on Twitter" height="34" src="{{URL::asset('emails/twitter.png')}}" style="outline:none;text-decoration:none;display:block;border:none" width="34"/>
                                                                                                                                                                            </a>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                        <td style="padding-left:10px;padding-right:10px">
                                                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="32">
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <a data-saferedirecturl="" href="https://www.linkedin.com/company/13669172/" target="_blank" class="email-link">
                                                                                                                                                                                <img alt="Share on Linkedin" height="34" src="{{URL::asset('emails/linkedin.png')}}" style="outline:none;text-decoration:none;display:block;border:none" width="34"/>
                                                                                                                                                                            </a>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                        <td class="Icons" style="padding-left:10px;padding-right:10px">
                                                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="32">
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <a href="https://www.youtube.com/channel/UCbK5eLsnc1SKgC-qe7RMTDw?view_as=subscriber" target="_blank" class="email-link">
                                                                                                                                                                                <img alt="Call Pnone" class=" " height="34" src="{{URL::asset('emails/youtube.png')}}" style="outline:none;text-decoration:none;display:block;border:none" width="34"/>
                                                                                                                                                                            </a>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                        <td style="padding-left:20px">
                                                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="32">
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <a href="mailto:support@heavygari.com" target="_blank" class="email-link">
                                                                                                                                                                                <img alt="Share as Email" class=" " height="34" src="{{URL::asset('emails/mail.png')}}" style="outline:none;text-decoration:none;display:block;border:none" width="34"/>
                                                                                                                                                                            </a>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                            <tbody>

                                                                                                <tr>
                                                                                                    <td style="padding-top:40px">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td bgcolor="#ddd" height="1" style="background-color:#ddd;font-size:0px;line-height:0px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding:0 15px">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%"> 
                                                                                            <tbody>

                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td style="vertical-align:top" valign="top">
                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:380px;table-layout:fixed;border:none">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td align="left" style="vertical-align:top;padding-top:40px" valign="top">
                                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:260px;border:none" width="100%">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="left" class=" " style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#111;padding-bottom:10px">
                                                                                                                                                        Need help?
                                                                                                                                                        <a href="mailto:support@heavygari.com" style="color:#ffffff;text-decoration:none" target="_blank">
                                                                                                                                                            Contact us
                                                                                                                                                        </a>
                                                                                                                                                        if you have any queries
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="left" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000;padding-bottom:10px">
                                                                                                                                                        © 2018 HeavyGari Technologies Ltd.
                                                                                                                                                        <br>
                                                                                                                                                            All rights reserved
                                                                                                                                                        </br>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="left" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#ffffff;padding-bottom:10px">
                                                                                                                                                        <a href="http://www.heavygari.com/page/toc" style="text-decoration: none;color:#ffffff;padding-right:16px;">Terms & Condition</a>
                                                                                                                                                        <a href="http://www.heavygari.com/page/privacy-policy" style="text-decoration: none;color:#ffffff;">Privacy Policy</a>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <div align="left">
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td height="15px" style="padding-top:15px">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>