@extends('emails.layout')

@section('content')
<table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
    <tbody>
        <tr>
            <td align="right">
                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#fff;border:none" width="100%">
                    <tbody>
                        <tr>
                            <td align="center">
                                <table border="0" cellpadding="0" cellspacing="0" class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="center">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding-right:15px;padding-left:15px">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="" style="padding-top:20px;padding-bottom:30px">
                                                                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="" style="padding-top:20px;padding-bottom:30px">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td align="left" class="" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                                <strong>
                                                                                                                    Greetings from the Heavygari Team,
                                                                                                                </strong>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="left" class="" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                                We have been notified that you have forgotten your password. If you require a new one, please click below.
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="10px">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                                <a href="{{url('session/recover/password?email='.$user->email.'&reminder_code='.$reminder_code)}}" style="display: inline-block; min-width: 160px; max-width: 200px; border-radius: 4px; color: #fff; background: #FAA435; padding: 7px 10px; text-decoration: none; text-align: center;">
                                                                                                                    Choose a new password
                                                                                                                </a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                                <strong>
                                                                                                                    Help
                                                                                                                </strong>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                                <a href="{{url('/')}}" style="display: inline-block; min-width: 160px; max-width: 200px; border-radius: 4px; color: #000; background: #ebebeb; padding: 7px 10px; text-decoration: none; text-align: center;">
                                                                                                                    I did not request this
                                                                                                                </a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
@stop
