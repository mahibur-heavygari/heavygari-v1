@extends('emails.layout')

@section('content')
<table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
    <tbody>
        <tr>
            <td align="right">
                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#fff;border:none" width="100%">
                    <tbody>
                        <tr>
                            <td align="center">
                                <table border="0" cellpadding="0" cellspacing="0" class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="center">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding-right:15px;padding-left:15px">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="" style="padding-top:20px;padding-bottom:30px">
                                                                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                <strong>
                                                                                                    Hello {{$invoice->owner->user->name}},
                                                                                                </strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" class="" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#333;padding-bottom:10px">
                                                                                                Weekly payment was paid. You can download the payment acknowledgement report from this <a href="{{url(\Storage::url($invoice->pdf_paid))}}">link</a>.
                                                                                            </td>
                                                                                        </tr>                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
@stop
