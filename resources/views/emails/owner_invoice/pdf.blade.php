<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Heavygari</title>

    <style>
        a {
            color: #FAA435;
        }
    </style>
</head>

<body style="background-color: #d6d6d5;">
    <table id="" style="width:100%!important;height:100%!important;line-height:100%!important;border-spacing:0;border-collapse:collapse;background-color:#d6d6d5;margin:0;padding:0;border:none" border="0" bgcolor="#d6d6d5" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td class="" style="vertical-align:top" valign="top" align="center">
                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%" border="0" bgcolor="#f8f8f9" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;border:none" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style="height:50px;" class="" valign="bottom" bgcolor="#EB374F" style="background-color: #EB374F" align="center">
                                                    <div>
                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="10px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 6%; min-width: 15px; margin: 0; padding: 0"></td>
                                                                    <td>
                                                                        <img style="outline:none;text-decoration:none;display:block" src="{{URL::asset('emails/logo-white.png')}}" width="50" alt="Heavygari">
                                                                    </td>
                                                                    <td style="width: 6%; min-width: 15px; margin: 0; padding: 0"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="10px"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#fff;border:none" width="100%" border="0" bgcolor="#fff" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-right:15px;padding-left:15px">
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="" style="padding-top:20px;padding-bottom:30px">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr align="left">
                                                                                                                                <td>
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                        <tbody><tr>
                                                                                                                                            <td style="font-size:24px; font-weight: bold;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111;padding-bottom:10px; text-transform: uppercase;text-align: left;">
                                                                                                                                                Invoice
                                                                                                                                            </td>
                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px; text-align: right;" align="right">
                                                                                                                                                Heavygari<br> 01909-222777
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody></table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="20"></td>
                                                                                                                            </tr>
                                                                                                                            <tr align="left">
                                                                                                                                <td>
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                        <tbody><tr>
                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                                                                                <strong style="color: #111;">{{$invoice->owner->user->name}}</strong><br> {{$invoice->owner->user->address}}
                                                                                                                                            </td>
                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                                                                                <table width="100%">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#898989;padding-bottom:2px">
                                                                                                                                                                Invoice Number : {{$invoice->invoice_number}}
                                                                                                                                                            </td>
                                                                                                                                                            <!-- <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:5px">
                                                                                                                                                                
                                                                                                                                                            </td> -->
                                                                                                                                                        </tr>
                                                                                                                                                        <!-- <tr>
                                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#898989;padding-bottom:2px">
                                                                                                                                                                Month : {{$invoice->month}}, {{$invoice->year}}
                                                                                                                                                            </td>
                                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                                                                                                
                                                                                                                                                            </td> 
                                                                                                                                                        </tr>-->
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody></table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="50"></td>
                                                                                                                            </tr>
                                                                                                                            <tr align="left">
                                                                                                                                <td>
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                        <thead>
                                                                                                                                            <tr>
                                                                                                                                                <th style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111; background: #ebebeb;padding:8px">
                                                                                                                                                    Booking ID
                                                                                                                                                </th>
                                                                                                                                                <th style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111;background: #ebebeb;padding:8px">
                                                                                                                                                    Cash Collected
                                                                                                                                                </th>
                                                                                                                                                <th style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111;background: #ebebeb;padding:8px">
                                                                                                                                                    Discount
                                                                                                                                                </th>
                                                                                                                                                <th style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111;background: #ebebeb;padding:8px">
                                                                                                                                                    Owner's Earning
                                                                                                                                                </th>
                                                                                                                                                <th style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111;background: #ebebeb;padding:8px">
                                                                                                                                                    Heevygari Will Pay
                                                                                                                                                </th> 
                                                                                                                                                <th style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111;background: #ebebeb;padding:8px">
                                                                                                                                                    Pay to Heavygari
                                                                                                                                                </th>                                                                                                            
                                                                                                                                            </tr>
                                                                                                                                        </thead>
                                                                                                                                        <tbody>
                                                                                                                                            @php
                                                                                                                                                $payable = 0;
                                                                                                                                            @endphp
                                                                                                                                            @foreach($invoice->invoiceItems as $item)
                                                                                                                                                <tr>
                                                                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding:8px">
                                                                                                                                                        {{$item->bookings->unique_id}}
                                                                                                                                                    </td>
                                                                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#898989;padding:8px">
                                                                                                                                                        {{$item->bookings->invoice->total_cost}}
                                                                                                                                                    </td>
                                                                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#898989;padding:8px">
                                                                                                                                                        {{$item->bookings->invoice->discount}}
                                                                                                                                                    </td> 
                                                                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#898989;padding:8px">
                                                                                                                                                        {{$item->bookings->earnings->owner_earning}}
                                                                                                                                                    </td> 
                                                                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#898989;padding:8px">
                                                                                                                                                        {{$item->bookings->invoice->discount}}
                                                                                                                                                    </td>                                                                                                            
                                                                                                                                                    <td style="font-family: 'Quicksand', sans-serif; font-size: 16px; color: #404040; line-height: 1.6; font-weight: 400; text-align: right;padding:8px">
                                                                                                                                                        {{$item->bookings->earnings->heavygari_earning + $item->bookings->earnings->driver_earning}}
                                                                                                                                                    </td>
                                                                                                                                                </tr> 
                                                                                                                                                @php
                                                                                                                                                    $payable = $payable + $item->bookings->invoice->discount;
                                                                                                                                                @endphp
                                                                                                                                            @endforeach
                                                                                                                                        </tbody>
                                                                                                                                        <tfoot>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="6" style="border-top: 2px solid #ebebeb"></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="4" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding: 8px;">&nbsp;</td>
                                                                                                                                                <td colspan="2" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111;padding:8px">
                                                                                                                                                                    <strong>Tk. {{$payable}}</strong>
                                                                                                                                                                </td>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111; padding: 8px;">
                                                                                                                                                                    <strong>Tk. {{$invoice->total_amount}}</strong>
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tfoot>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%" border="0" bgcolor="#f8f8f9" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="" style="padding:40px 15px">
                                                                                                    <table class="" id="" style="display:table;border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="98%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="https://ci3.googleusercontent.com/proxy/z7CISSfuy3G8tWyE76ENuFiB9L5PnnQNPrgcX0tGQ1z5E8mAWvkPpYCOckkQ7e5PousEwltmZOi1z4026ESwyWJTUXDBH-7PYnMAjmSVPYkztT1U=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/green-left.png" style="outline:none;text-decoration:none;display:block" class="" width="5" height="18"></td>
                                                                                                                                <td class="" style="font-size:1;line-height:1;background-color:#cdcdd3" width="2" valign="top" bgcolor="#cdcdd3"><img alt="" src="https://ci5.googleusercontent.com/proxy/nJfedRekVzoWWDNT8qkG9y3k3b8hINaEsXfKQwScs9HwZRuP1ddzsl_qqZJrGbToUx-Jag5FImMICtRLq4Zmy7S6SZq1_c4E9As2P2BrteCxVNf1QJk=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/green-middle.png" style="outline:none;text-decoration:none;display:block" class="" width="2" height="18"></td>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="https://ci5.googleusercontent.com/proxy/VTd9gqQdWzqLHSmEogRzBuqRcSabFlyEw9Sn2OHpvrYHiNVTZWvzQ06y3960fETlu_2Th6myPRWOZxmkDsACF0c_3W8EKt4QBpDX4j2IcVO9GFa3EQ=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/green-right.png" style="outline:none;text-decoration:none;display:block" class="" width="5" height="18"></td>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="10" valign="top"></td>
                                                                                                                                <td class="    " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left;vertical-align:middle;padding-bottom:20px" valign="top" align="left"><span class=" " style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000"> <span class="aBn" data-term="goog_2111631963" tabindex="0"><span class="aQJ">02:51pm</span></span> | </span> Barun Bhaban, Kemal Ataturk Ave, Dhaka 1212, Bangladesh </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="https://ci4.googleusercontent.com/proxy/2rQ9r5m_CziatUMU7jQn46xOg6yfuHp7ZKbybsY2OO0SfyDbUQNQXH6-Y7MHdxdgzwucKy4iP7V8bw-N9RpAAdFgmOQ75WSpmVZc1Mg7L0uX8A=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/red-left.png" style="outline:none;text-decoration:none;display:block" class="" width="5" height="18"></td>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="2" valign="top"><img alt="" src="https://ci3.googleusercontent.com/proxy/EOfXPLooq0PKJX8Bv7qD-dcC0AhaTqH8-_f52GcmqyLcU3lq-eqUlUt1OS2CUSXqbedHD7dMPoSc0vTA6td1pYD80senD4hO1xY-Jm6QhHbKI7BV=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/red-middle.png" style="outline:none;text-decoration:none;display:block" class="" width="2" height="18"></td>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="https://ci6.googleusercontent.com/proxy/InPQjp4YwUxjDkWR9JTbh8gTuE6ancYHDaRjxclAqzRQct3ShSKDNDfMWh6NA3HHmnMSJOIiwMqECAKAO6BiUffpxP6A-AD0cKGVw5YkVESl4IM=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/red-right.png" style="outline:none;text-decoration:none;display:block" class="" width="5" height="18"></td>
                                                                                                                                <td class="" style="font-size:1;line-height:1" width="10" valign="top"></td>
                                                                                                                                <td class="   " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left;vertical-align:middle" valign="top" align="left"><span class="  " style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;vertical-align:middle"> <span class="aBn" data-term="goog_2111631964" tabindex="0"><span class="aQJ">03:24pm</span></span> | </span> <a href="https://maps.google.com/?q=95+Bir+Uttam+AK+Khandakar+Road,+Dhaka+1212,+Bangladesh&amp;entry=gmail&amp;source=g" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://maps.google.com/?q%3D95%2BBir%2BUttam%2BAK%2BKhandakar%2BRoad,%2BDhaka%2B1212,%2BBangladesh%26entry%3Dgmail%26source%3Dg&amp;source=gmail&amp;ust=1519880556634000&amp;usg=AFQjCNFjPSP95vBq6MOWEyIWPuC0fdX5zg">95 Bir Uttam AK Khandakar Road, Dhaka 1212, Bangladesh</a> </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                    <!-- <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%" border="0" bgcolor="#f8f8f9" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-right:15px;padding-left:15px">
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="" style="padding-bottom:30px">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="" style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="https://ci6.googleusercontent.com/proxy/bjlmwp1lfIp2fevkoEO9P7LRLJDB3htzJ41P3i-rSE5ZwtA6fw0XQ__C7XIfeKMxLCjsuN8uGnjTZ77lPg_2Q3X-nIE1drc87j_Mf3u2_Iy1exK-tg=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/no_collapse.png" alt="" style="outline:none;text-decoration:none;display:block" class=""></td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:520px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:40px;padding-bottom:20px">
                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:170px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td class="" style="padding-bottom:20px" align="left">
                                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:135px;border:none" align="left">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td align="left"><img src="https://ci6.googleusercontent.com/proxy/Y_U0nFF03yyEarTI2t7m-q8fslPciMQINDFSR7HMoA2-4zO9114A-qbGahG3JD_DHsYJr_6x9iln9I2NzwTwfiJ95ugyBJUHtPhQohvvfDf5ba4VKg=s0-d-e1-ft#https://d1w2poirtb3as9.cloudfront.net/9ded6c3c67c4bdaabb36.jpeg" alt="Picture of Md. Ashraful" class="-8656769059417000728driverImage " style="outline:none;text-decoration:none;display:block;border-radius:40px;border:3px solid #f1f1f1" width="66" height="66"></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:275px;border:none" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td class="" style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left" align="left">You rode with Md. Ashraful</td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:240px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td height="50" align="center">
                                                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;min-width:70px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                                        <tbody>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td class=" " style="font-size:14px;line-height:20px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left" align="center">3.67 </td>
                                                                                                                                                                            </tr>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td class="" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left" align="center"> kilometers </td>
                                                                                                                                                                            </tr>
                                                                                                                                                                        </tbody>
                                                                                                                                                                    </table>
                                                                                                                                                                </td>
                                                                                                                                                                <td class="" style="background-color:#e6e6e9" width="2" bgcolor="#e6e6e9"></td>
                                                                                                                                                                <td align="center">
                                                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;min-width:70px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                                        <tbody>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td class="  " style="font-size:14px;line-height:20px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;padding-right:15px;padding-left:15px;text-align:left" align="left"><span class="aBn" data-term="goog_2111631965" tabindex="0"><span class="aQJ">00:32:13</span></span>
                                                                                                                                                                                </td>
                                                                                                                                                                            </tr>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td class="" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-right:15px;padding-left:15px;text-align:left" align="left"> Trip time </td>
                                                                                                                                                                            </tr>
                                                                                                                                                                        </tbody>
                                                                                                                                                                    </table>
                                                                                                                                                                </td>
                                                                                                                                                                <td class="" style="background-color:#e6e6e9" width="2" bgcolor="#e6e6e9"></td>
                                                                                                                                                                <td align="center">
                                                                                                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;min-width:70px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                                        <tbody>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td class="  " style="font-size:14px;line-height:20px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;padding-right:15px;padding-left:15px;text-align:left" align="left">PREMIER </td>
                                                                                                                                                                            </tr>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td class="" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-right:15px;padding-left:15px;text-align:left" align="center"> Car </td>
                                                                                                                                                                            </tr>
                                                                                                                                                                        </tbody>
                                                                                                                                                                    </table>
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="" style="padding-bottom:40px">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="" style="background-color:#ffffff;padding:10px 15px" bgcolor="#ffffff">
                                                                                                                                    <table class="Wrapper" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td width="140">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td width="35%"></td>
                                                                                                                                                                <td class="" style="width:6%;text-align:center;padding:0 4px" align="center"><a href="" target="_blank"><img src="receipt_stars.png" alt="One star" style="outline:none;text-decoration:none;display:block;border:none" class=""></a></td>
                                                                                                                                                                <td class="" style="width:6%;text-align:center;padding:0 4px" align="center"><a target="_blank"><img src="receipt_stars.png" alt="two stars" style="outline:none;text-decoration:none;display:block;border:none" class=""></a></td>
                                                                                                                                                                <td class="" style="width:6%;text-align:center;padding:0 4px" align="center"><a href="" target="_blank" ><img src="receipt_stars.png" alt="three stars" style="outline:none;text-decoration:none;display:block;border:none" class=""></a></td>
                                                                                                                                                                <td class="" style="width:6%;text-align:center;padding:0 4px" align="center"><a href="" target="_blank" ><img src="receipt_stars.png" alt="four stars" style="outline:none;text-decoration:none;display:block;border:none" class=""></a></td>
                                                                                                                                                                <td class="" style="width:6%;text-align:center;padding:0 4px" align="center"><a href="" target="_blank" ><img src="receipt_stars.png" alt="five stars" style="outline:none;text-decoration:none;display:block;border:none" class=""></a></td>
                                                                                                                                                                <td width="35%"></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                    <!-- <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#ffffff;border:none" width="100%" border="0" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="" style="padding-right:15px;padding-left:15px;padding-top:25px">
                                                                                                    <table class="" id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="" style="padding-bottom:25px">
                                                                                                                    <div align="left">
                                                                                                                        <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:120px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td width="100"><img src="message_icon.png" alt="" style="outline:none;text-decoration:none;display:block" class="" width="72" height="71"></td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                    <div align="left">
                                                                                                                        <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:450px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td class=" " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left" align="left"> does not include fees that may be charged by your bank. Please contact your bank directly for inquiries. </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                    <!-- <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="center">
                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#ffffff;border:none" width="100%" border="0" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="" style="padding-right:15px;padding-left:15px;padding-top:25px">
                                                                                                    <div>
                                                                                                        <input id="" class="" style="display:none!important;max-height:0px;overflow:hidden" type="checkbox">
                                                                                                        <label for="">
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="" style="font-size:20px;line-height:30px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;text-align:left;padding-bottom:25px" align="left">Your </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </label>
                                                                                                        <div class="">
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="" style="padding-bottom:30px">
                                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td class="" style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="no_collapse.png" alt="" style="outline:none;text-decoration:none;display:block" class=""></td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="" style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:24px" align="left">Trip </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:24px" align="right">209.09 </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="" style="padding-bottom:30px">
                                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td class="" style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="no_collapse.png" alt="" style="outline:none;text-decoration:none;display:block" class=""></td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table class="" id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;padding-bottom:24px" align="left">Subtotal </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;padding-bottom:24px" align="right">Tk&nbsp;209.09 </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table class="" id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:24px" align="left">Discounts </td>
                                                                                                                        <td class="" style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#FAA435;padding-bottom:24px" align="right">-74.09 </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class="" style="padding-bottom:30px">
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td class="" style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="no_collapse.png" alt="" style="outline:none;text-decoration:none;display:block" class=""></td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class="" style="font-size:10px;line-height:16px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-transform:uppercase;color:#898989" align="left">collected</td>
                                                                                                                    <td rowspan="2" class="  " style="font-size:32px;line-height:40px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;padding-bottom:30px" align="right"> Tk&nbsp;135.00 </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="" style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;vertical-align:top;padding-bottom:30px" valign="top" align="left"><span align="left" class="" style="display:inline-block;width:20px;vertical-align:middle;padding-right:5px"><img alt="" id="" src="cash_24.png" class="-8656769059417000728card-image " style="outline:none;text-decoration:none;display:block;max-width:100%" width="auto"></span> <span class="-8656769059417000728ccNumbers" style="word-break:keep-all">Cash</span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class="" style="padding-bottom:30px">
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td class="" style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="no_collapse.png" alt="" style="outline:none;text-decoration:none;display:block" class=""></td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody></tbody>
                                                                                                        </table>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class=" " style="font-size:10px;line-height:16px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left" align="left">License Plate: GA278313</td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class="" style="font-size:10px;line-height:16px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left" align="left"><a href="" target="_blank">Visit the trip page</a> for more information, including invoices (where available)</td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class="" style="line-height:1px;font-size:1px;color:#ffffff">
                                                                                                                        </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="" style="line-height:1px;font-size:1px;color:#ffffff"></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="" style="padding-top:40px"> </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                    <!-- <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="center">
                                                    <table class="-8656769059417000728calloutWrapper" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;background-color:#ffffff;border:none" width="100%" border="0" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right">
                                                                    <table class="-8656769059417000728calloutInnerWrapper" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:615px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="" style="padding-right:15px;padding-left:15px;text-align:left" align="left">
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="-8656769059417000728promoWrapper" style="padding:50px 0 30px">
                                                                                                                    <table class="-8656769059417000728promoImageContainer" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:320px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="-8656769059417000728promoImageTd" style="padding-bottom:20px"><img src="https://ci6.googleusercontent.com/proxy/vEF6vYiurF2G5o__KnMKf8DcZaBEiON22LkAB3GlGS_A15Nle17Rn8aqBdMK8-VblIRyQVar1asiyC7Kb5bz3Eq8-WWdjF8leOPQK6raV5cABiEDwU9ISI8bq5E=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/sample_offer_image.png" alt="" class="-8656769059417000728promoImage " style="outline:none;text-decoration:none;display:block;max-width:100%;height:auto;border:none"></td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <table class="-8656769059417000728promoText" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:220px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="-8656769059417000728promoText " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989">Invite your friends and family. Give friends free ride credit to try Heavygari. You'll get BDT100 off each of your next 2 rides when they start riding. </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="-8656769059417000728promoCode " style="font-size:20px;line-height:30px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#5dbb5f"> Share code: arifa10624ui </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                    <!-- <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td class="ayoutSpace" style="padding-top:60px"> </td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="background: #EB374F url('{{URL::asset('emails/bg.png')}}') bottom left repeat-x;" align="center">
                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding:0 15px">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:380px;table-layout:fixed;border:none" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:30px">
                                                                                                                                    <table id="ogo" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left"><img src="{{URL::asset('emails/logo-text.png')}}" alt="Heavygari logo" style="outline:none;text-decoration:none;display:block" width="120"></td>
                                                                                                                                            </tr>
                                                                                                                                            
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <div align="left">
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:200px;border:none" cellspacing="0" cellpadding="0" border="0" align="right">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td style="padding-top:20px">
                                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td style="font-size:16px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:0px; text-align: right;">
                                                                                                                                                        গ্রাহক সেবা +৮৮০১৯০৯২২২৭৭৭ 
                                                                                                                                                    </td>
                                                                                                                                                    
                                                                                                                                                
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:100%;table-layout:fixed;border:none" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:30px">
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    HeavyGari Technologies Ltd. All Rights Reserved 2018.
                                                                                                                                                    
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    This invoice has been automatically generated for third party convenience. Please read the terms and conditions and privacy policy for further details.

                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    
                                                                                                                                                    *conditions apply
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            
                                                                                            <tr>
                                                                                                <td style="padding-top:10px" height="10px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>