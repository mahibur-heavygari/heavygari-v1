@extends('emails.layout')

@section('content')
<table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
    <tbody>
        <tr>
            <td align="right">
                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#fff;border:none" width="100%">
                    <tbody>
                        <tr>
                            <td align="center">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="center">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding-right:15px;padding-left:15px">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-top:20px;padding-bottom:30px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111;padding-bottom:10px">
                                                                                <strong>
                                                                                    Dear {{$booking->owner->user->name}},
                                                                                </strong>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                Driver {{$booking->owner->user->name}}, vehicle registration number {{$booking->owner->user->name}} has delivered the particulars from {{$booking->trips[0]->origin->title}} to {{$booking->trips[0]->destination->title}}.  Please find below your total earnings for this trip.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                Please note: Heavygari fee from the total fare will have to be adjusted and paid to HeavyGari by the 30th of this month.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-top:10px;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right:15px;padding-left:15px">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:275px;border:none" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:1px solid #ebebeb" width="100%">
                                                                                    <tr>
                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;">
                                                                                            <img alt="" src="{{$booking->trips[0]->staticMapUrl()}}" style="width: 100%; height: auto;">
                                                                                            </img>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="10px" style="height: 10px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="0" 174
                                                                                            cellspacing="0" style="display:table;border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td height="10px" style="width: 15px" width="15px">
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="5">
                                                                                                                            <img alt="" height="18" src="{{URL::asset('emails/green-left.png')}}" style="outline:none;text-decoration:none;display:block" width="5"/>
                                                                                                                        </td>
                                                                                                                        <td bgcolor="#cdcdd3" style="font-size:1;line-height:1;background-color:#cdcdd3" valign="top" width="2">
                                                                                                                            <img alt="" height="18" src="{{URL::asset('emails/green-middle.png')}}" style="outline:none;text-decoration:none;display:block" width="2"/>
                                                                                                                        </td>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="5">
                                                                                                                            <img alt="" height="18" src="{{URL::asset('emails/green-right.png')}}" style="outline:none;text-decoration:none;display:block" width="5"/>
                                                                                                                        </td>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="10">
                                                                                                                        </td>
                                                                                                                        <td align="left" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left;vertical-align:middle;padding-bottom:20px" valign="top">
                                                                                                                            <span class=" " style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000">
                                                                                                                                <span class="aBn" data-term="goog_2111631963" tabindex="0">
                                                                                                                                    <span class="aQJ">
                                                                                                                                        {{$booking->trips[0]->started_at}}
                                                                                                                                    </span>
                                                                                                                                </span>
                                                                                                                            </span>
                                                                                                                            <br>
                                                                                                                                {{$booking->trips[0]->from_address}}
                                                                                                                            </br>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="5">
                                                                                                                            <img alt="" height="18" src="{{URL::asset('emails/red-left.png')}}" style="outline:none;text-decoration:none;display:block" width="5"/>
                                                                                                                        </td>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="2">
                                                                                                                            <img alt="" height="18" src="{{URL::asset('emails/red-middle.png')}}" style="outline:none;text-decoration:none;display:block" width="2"/>
                                                                                                                        </td>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="5">
                                                                                                                            <img alt="" height="18" src="{{URL::asset('emails/red-right.png')}}" style="outline:none;text-decoration:none;display:block" width="5"/>
                                                                                                                        </td>
                                                                                                                        <td style="font-size:1;line-height:1" valign="top" width="10">
                                                                                                                        </td>
                                                                                                                        <td align="left" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left;vertical-align:middle" valign="top">
                                                                                                                            <span class=" " style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;vertical-align:middle">
                                                                                                                                <span class="aBn" data-term="goog_2111631964" tabindex="0">
                                                                                                                                    <span class="aQJ">
                                                                                                                                        {{$booking->trips[0]->completed_at}}
                                                                                                                                    </span>
                                                                                                                                </span>
                                                                                                                            </span>
                                                                                                                            <br>
                                                                                                                                {{$booking->trips[0]->to_address}}
                                                                                                                            </br>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td height="10px" style="width: 15px" width="15px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="10px" style="height: 10px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="height: 1px; background: #ebebeb;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td height="10px" style="width: 15px" width="15px">
                                                                                                    </td>
                                                                                                    <td style="width: 245px">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:100%;border:none" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td height="8px" style="height: 8px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="center">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;min-width:70px;border:none" width="100%">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td align="center" style="font-size:10px;line-height:16px; color: #898989;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center; text-transform: uppercase">
                                                                                                                                        <strong>
                                                                                                                                            Vehicle Type
                                                                                                                                        </strong>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="center" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#111;text-align:center">
                                                                                                                                        {{$booking->vehicle->vehicleType->title}}
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td align="center">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;min-width:70px;border:none" width="100%">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td align="center" style="font-size:10px;line-height:16px; color: #898989;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center; text-transform: uppercase">
                                                                                                                                        <strong>
                                                                                                                                            Distance
                                                                                                                                        </strong>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="center" style="font-size:12px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#111;text-align:center">
                                                                                                                                        {{$booking->trips[0]->distance}} Km
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>                                                                                                                    
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td height="8px" style="height: 8px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td height="10px" style="width: 15px" width="15px">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:30px;border:none;" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="left" style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px">
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:275px;border:none;" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 15%; vertical-align: middle;">
                                                                                                        <div style="width: 100%; height: 1px; background: #ddd;">
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:center;color:#111111;">
                                                                                                        FARE BREAKDOWN
                                                                                                    </td>
                                                                                                    <td style="width: 15%; vertical-align: middle;">
                                                                                                        <div style="width: 100%; height: 1px; background: #ddd;">
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="20px" style="height: 20px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                                @if($booking->invoice->base_fare && $booking->invoice->base_fare!=0)
                                                                                                <tr>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                        Base Fare
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                        Tk. {{$booking->invoice->base_fare}}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                @endif
                                                                                                @if($booking->booking_category=='shared')
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                            Pickup Charge
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            Tk. {{$booking->invoice->pickup_charge}}
                                                                                                        </td>
                                                                                                    </tr>  
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                            Dropoff Charge
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            Tk. {{$booking->invoice->dropoff_charge}}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                            Weight Cost
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            Tk. {{$booking->invoice->weight_cost}}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                @endif
                                                                                                <tr>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                        Distance Cost @if(!is_null($booking->trips[0]->multiplier))
                                                                                                            <span align="left" style="display:inline-block;width:20px;vertical-align:middle;padding-right:5px">
                                                                                                                <img alt="" id="" src="{{URL::asset('website/images/flash.png')}}" style="outline:none;text-decoration:none;display:block;max-width:100%" width="auto">
                                                                                                            </span>
                                                                                                        @endif
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                        Tk. {{$booking->invoice->distance_cost}}
                                                                                                    </td>
                                                                                                </tr>                                                                                                  
                                                                                                <tr>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                        Surcharge
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                        Tk. {{$booking->invoice->surcharge}}
                                                                                                    </td>
                                                                                                </tr>    
                                                                                                @if($booking->booking_category=='full' && $booking->trips[0]->distance_type=='short')  
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                            Time charge
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            Tk. {{$booking->invoice->waiting_cost}}
                                                                                                        </td>
                                                                                                    </tr>     
                                                                                                @endif                                                                                                   
                                                                                                <tr>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                        Heavygari Fee
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                        Tk. {{$booking->invoice->heavygari_fee}}
                                                                                                    </td>
                                                                                                </tr>                               
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="height: 1px; background: #ebebeb;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="10px" style="height: 10px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                                <tr>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111111; padding-bottom: 10px;">
                                                                                                        <strong>
                                                                                                            Total Fare
                                                                                                        </strong>
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                        <strong>
                                                                                                            Tk. {{$booking->invoice->total_fare}}
                                                                                                        </strong>
                                                                                                    </td>
                                                                                                </tr>                                                                                                 
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    @if($booking->invoice->discount > 0)  

                                                                                        <tr>
                                                                                            <td>
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom: 10px;">
                                                                                                            Discount
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            Tk. {{$booking->invoice->discount}}
                                                                                                        </td>
                                                                                                    </tr>                                                                                             
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 1px; background: #ebebeb;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="10px" style="height: 10px">
                                                                                            </td>
                                                                                        </tr>                                                                                        
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                                    
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111111; padding-bottom: 10px;">
                                                                                                            <strong>
                                                                                                                Fare ( from customer )
                                                                                                            </strong>
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            <strong>
                                                                                                                Tk. {{$booking->invoice->total_cost}}
                                                                                                            </strong>
                                                                                                        </td>
                                                                                                    </tr>  
                                                                                                    <tr>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111111; padding-bottom: 10px;">
                                                                                                            Earning from heavygari
                                                                                                        </td>
                                                                                                        <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                            Tk. {{$booking->invoice->discount}}
                                                                                                        </td>
                                                                                                    </tr>                                                                                                
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endif

                                                                                    <tr>
                                                                                        <td style="height: 1px; background: #ebebeb;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="10px" style="height: 10px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:100%;border:none;" width="100%">
                                                                                                <tr>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#111111; padding-bottom: 10px;">
                                                                                                        <strong>
                                                                                                            Your Total Earning
                                                                                                        </strong>
                                                                                                    </td>
                                                                                                    <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:right;color:#111111; padding-bottom: 10px;">
                                                                                                        <strong>
                                                                                                            Tk. {{$booking->earnings->owner_earning}}
                                                                                                        </strong>
                                                                                                    </td>
                                                                                                </tr>                                                                                                
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>                                                                      
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="20px" style="height: 15px">
                                                            </td>
                                                        </tr>                                                        
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
@stop
