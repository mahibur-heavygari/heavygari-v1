<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Heavygari</title>
    <style>
    a {
        color: #EB374F;
    }



    </style>
</head>

<body>
    <table id="" style="width:100%!important;height:100%!important;line-height:100%!important;border-spacing:0;border-collapse:collapse;background-color:#d6d6d5;margin:0;padding:0;border:none" border="0" bgcolor="#d6d6d5" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td style="vertical-align:top" valign="top" align="center">
                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%" border="0" bgcolor="#f8f8f9" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;border:none" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style="height:50px;" valign="bottom" bgcolor="#EB374F" background="" align="center">
                                                    <div>
                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="10px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 6%; min-width: 15px; margin: 0; padding: 0"></td>
                                                                    <td>
                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <img style="outline:none;text-decoration:none;display:block" src="{{URL::asset('emails/logo-white.png')}}" width="50" alt="Heavygari">
                                                                                </td>
                                                                                <td style="font-size:24px;line-height:26px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#ffffff;text-align:right;padding:0" align="right">
                                                                                    Invoice/Chalan
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        
                                                                    </td>
                                                                    <td style="width: 6%; min-width: 15px; margin: 0; padding: 0"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="10px"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%" border="0" bgcolor="#f8f8f9" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-right:15px;padding-left:15px">
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td style="padding-top:20px;padding-bottom:15px">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="font-size:20px;line-height:26px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;text-align:left;padding-bottom:10px" align="left">Order No : {{$booking->unique_id}}
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left">গ্রাহক :  {{$booking->customer->user->name}}  ({{$booking->customer->user->phone}})</td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left">প্রাপক : {{$booking->recipient_name}}  ({{$booking->recipient_phone}})</td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left">ড্রাইভার : @if(is_null($booking->is_hired_driver)) {{$booking->driver->user->name}}  ({{$booking->driver->user->phone}}) @else {{$booking->hiredDriver->name}}  ({{$booking->hiredDriver->phone}}) @endif</td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td style="padding-top:20px;padding-bottom:30px;padding-left:10px;width:50%"></td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img alt="" src="{{URL::asset('emails/no_collapse.png')}}" style="outline:none;text-decoration:none;display:block"></td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#ffffff;border:none" width="100%" border="0" bgcolor="#ffffff" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding:20px 15px 30px">
                                                                                                    <table id="" style="display:table;border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="98%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="{{URL::asset('emails/green-left.png')}}" style="outline:none;text-decoration:none;display:block" width="5" height="18"></td>
                                                                                                                                <td style="font-size:1;line-height:1;background-color:#cdcdd3" width="2" valign="top" bgcolor="#cdcdd3"><img alt="" src="{{URL::asset('emails/green-middle.png')}}" style="outline:none;text-decoration:none;display:block" width="2" height="18"></td>
                                                                                                                                <td style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="{{URL::asset('emails/green-right.png')}}" style="outline:none;text-decoration:none;display:block" width="5" height="18"></td>
                                                                                                                                <td style="font-size:1;line-height:1" width="10" valign="top"></td>
                                                                                                                                <td class="    " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left;vertical-align:middle;padding-bottom:20px" valign="top" align="left"><span class=" " style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000"> <span class="aBn" data-term="goog_2111631963" tabindex="0"><span class="aQJ">পিক-আপের ঠিকানা</span></span> : </span>{{$booking->trips[0]->from_address}}</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="{{URL::asset('emails/red-left.png')}}" style="outline:none;text-decoration:none;display:block" width="5" height="18"></td>
                                                                                                                                <td style="font-size:1;line-height:1" width="2" valign="top"><img alt="" src="{{URL::asset('emails/red-middle.png')}}" style="outline:none;text-decoration:none;display:block" width="2" height="18"></td>
                                                                                                                                <td style="font-size:1;line-height:1" width="5" valign="top"><img alt="" src="{{URL::asset('emails/red-right.png')}}" style="outline:none;text-decoration:none;display:block" width="5" height="18"></td>
                                                                                                                                <td style="font-size:1;line-height:1" width="10" valign="top"></td>
                                                                                                                                <td class="   " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;text-align:left;vertical-align:middle" valign="top" align="left"><span class="  " style="font-size:16px;line-height:24px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;vertical-align:middle"> <span class="aBn" data-term="goog_2111631964" tabindex="0"><span class="aQJ">গন্তব্যের ঠিকানা</span></span> : </span> <span>{{$booking->trips[0]->to_address}}</span></td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    
                                                                                                                    @if($booking->status=='ongoing')
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left">গাড়ী : {{$booking->vehicle->number_plate}}  </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                                <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left">গাড়ীর মালিক : {{$booking->owner->user->name}} </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                    @endif
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#ffffff;border:none" width="100%" border="0" bgcolor="#ffffff" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-right:15px;padding-left:15px">
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td style="padding-bottom:10px">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="{{URL::asset('emails/no_collapse.png')}}" alt="" style="outline:none;text-decoration:none;display:block"></td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    @if ($booking->booking_category=='full')
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:520px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td style="padding-top:10px;padding-bottom:0px">
                                                                                                                                        
                                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:50%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                                            <tr>
                                                                                                                                                <td>                                                                                                                                                    
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left">বুকিং-এর ধরণ : সম্পূর্ণ গাড়ীর বুকিং</td> 
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left">গাড়ীর ধরণ : {{$booking->fullBookingDetails->vehicleType->title}} ({{$booking->vehicle->number_plate}})</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left"> ধারণ ক্ষমতা : {{$booking->fullBookingDetails->capacity}} {{$booking->fullBookingDetails->vehicleType->capacityType->title}}</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left"> পণ্যের বিবরণ : {{$booking->particular_details}}</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>  
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left"> দূরত্ব : {{$booking->trips[0]->distance}} km</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>                                                                                                                                                       
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    @elseif ($booking->booking_category=='shared')
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:520px;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td style="padding-top:10px;padding-bottom:20px">
                                                                                                                                        
                                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:50%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left"> বুকিং-এর ধরণ: পার্সেল ডেলিভারি </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left">পণ্য এর ক্যাটাগরি : {{$booking->sharedBookingDetails->productCategory->title}} </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left"> পণ্য সংখ্যা : {{$booking->sharedBookingDetails->quantity}}</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>                                                                                                                                                    
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989;padding-bottom:10px" align="left"> ওজন এর ধরন : 
                                                                                                                                                                    @if($booking->sharedBookingDetails->weightCategory)
                                                                                                                                                                        {{$booking->sharedBookingDetails->weightCategory->title}}
                                                                                                                                                                    @else
                                                                                                                                                                        {{$booking->sharedBookingDetails->weight}} kg
                                                                                                                                                                    @endif
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <!--<table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989; padding-bottom:10px" align="left"> আয়তনের ধরন: 
                                                                                                                                                                    @if($booking->sharedBookingDetails->volumetricCategory)
                                                                                                                                                                        {{$booking->sharedBookingDetails->volumetricCategory->title}}
                                                                                                                                                                    @else
                                                                                                                                                                        {{$booking->sharedBookingDetails->volumetric_width}} inch * {{$booking->sharedBookingDetails->volumetric_height}} inch * {{$booking->sharedBookingDetails->volumetric_length}} inch
                                                                                                                                                                    @endif
                                                                                                                                                                 </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>-->
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-align:left;color:#898989" align="left">বিবরণ : {{$booking->particular_details}}</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>                                                                                                                                                    
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    @endif
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="center">
                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#ffffff;border:none" width="100%" border="0" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:610px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-right:15px;padding-left:15px;padding-top:15px">
                                                                                                    <div>
                                                                                                        <input id="" style="display:none!important;max-height:0px;overflow:hidden" type="checkbox">
                                                                                                        <label for="">
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:20px;line-height:30px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;text-align:left;padding-bottom:25px" align="left">গাড়ী ভাড়ার মূল্যনির্ধারণ </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </label>
                                                                                                        <div>
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="padding-bottom:15px">
                                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="{{URL::asset('emails/no_collapse.png')}}" alt="" style="outline:none;text-decoration:none;display:block"></td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:10px" align="left">বেজ ফেয়ার </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->base_fare}} </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            @if($booking->booking_category=='shared')
                                                                                                                <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">পিকআপ চার্জ </td>
                                                                                                                            <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->pickup_charge}} </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                                <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">ড্রপঅফ চার্জ </td>
                                                                                                                            <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->dropoff_charge}}</td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            @endif
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">দূরত্ব খরচ @if(!is_null($booking->trips[0]->multiplier))
                                                                                                                                <span align="left" style="display:inline-block;width:20px;vertical-align:middle;padding-right:5px">
                                                                                                                                    <img alt="" id="" src="{{URL::asset('website/images/flash.png')}}" style="outline:none;text-decoration:none;display:block;max-width:100%" width="auto">
                                                                                                                                </span>
                                                                                                                            @endif
                                                                                                                        </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->distance_cost}}</td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            @if($booking->booking_category=='shared')
                                                                                                                <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">ওজন খরচ </td>
                                                                                                                            <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->weight_cost}} </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            @endif
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">সারচার্জ </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->surcharge}} </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            @if($booking->booking_category=='full' && $booking->trips[0]->distance_type=='short') 
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">ওয়েটিং চার্জ </td>
                                                                                                                            <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->waiting_cost}} </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            @endif
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="left">সার্ভিস চার্জ  </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:15px" align="right">{{$booking->invoice->heavygari_fee}} </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="padding-bottom:30px">
                                                                                                                            <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="{{URL::asset('emails/no_collapse.png')}}" alt="" style="outline:none;text-decoration:none;display:block"></td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;padding-bottom:24px" align="left">মোট ভাড়া </td>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;padding-bottom:24px" align="right">ট {{$booking->invoice->total_fare}} </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                            <table id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="  " style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;padding-bottom:24px" align="left">ডিসকাউন্ট </td>
                                                                                                                        <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#EB374F;padding-bottom:24px" align="right">-{{$booking->invoice->discount}} </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td style="padding-bottom:30px">
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td style="background-color:#bfbfbf;font-size:0px;line-height:0px" height="1" bgcolor="#bfbfbf"><img src="{{URL::asset('emails/no_collapse.png')}}" alt="" style="outline:none;text-decoration:none;display:block"></td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table id="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td style="font-size:10px;line-height:16px;font-family:'ClanPro-News','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;text-transform:uppercase;color:#898989" align="left">মোট খরচ</td>
                                                                                                                    <td rowspan="2" class="  " style="font-size:32px;line-height:40px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#000000;padding-bottom:30px" align="right"> ট {{$booking->invoice->total_cost}}</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="font-size:14px;line-height:22px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#898989;vertical-align:top;padding-bottom:30px" valign="top" align="left"><span align="left" style="display:inline-block;width:20px;vertical-align:middle;padding-right:5px"><img alt="" id="" src="{{URL::asset('emails/cash_24.png')}}" class="-8656769059417000728card-image " style="outline:none;text-decoration:none;display:block;max-width:100%" width="auto"></span> <span class="-8656769059417000728ccNumbers" style="word-break:keep-all">নগদ</span></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                <td class="ayoutSpace" style="padding-top:60px"> </td>
                                            </tr> -->
                                        </tbody>
                                    </table>

                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="background: #EB374F url('{{URL::asset('emails/bg.png')}}') bottom left repeat-x;" align="center">
                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding:0 15px">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:100%;table-layout:fixed;border:none" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:30px">
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td style="padding: 10px 0 0" align="left"><img src="{{URL::asset('emails/logo-text.png')}}" alt="Heavygari logo" style="outline:none;text-decoration:none;display:block" width="120"></td>
                                                                                                                                                <td style="padding: 10px 0 0" align="right">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:16px;line-height:18px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:0px; text-align: right;">
                                                                                                                                                                    গ্রাহক সেবা <br /> +৮৮০১৯০৯২২২৭৭৭ 
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>                                                                                                                                           
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>   
                                                                                                                                                                                                
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:100%;table-layout:fixed;border:none" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:30px">
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    HeavyGari Technologies Ltd. All Rights Reserved 2018.
                                                                                                                                                    
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    This invoice has been automatically generated for third party convenience. Please read the terms and conditions and privacy policy for further details.

                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;font-family:'ClanPro-Book','HelveticaNeue-Light','Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    
                                                                                                                                                    *conditions apply
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            
                                                                                            <tr>
                                                                                                <td style="padding-top:10px" height="10px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>