
<form class="" method="POST" enctype="multipart/form-data"  >
    {{ csrf_field() }}

    <input type="file" name="file" id="file">
    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
</form>

