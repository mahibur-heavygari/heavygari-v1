<style>
#map {
    height: 600px;
    width: 100%;
}
</style>
@extends('website.layout')

@section('content-body')
<h4>Tracker Demo: #{{ $demo_trip_id }}</h4>
<div id="map"></div>
@stop

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>

<script type="text/javascript">
    function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 23.800008, // TODO : MAP CENTER
                lng: 90.396601
            },
            zoom: 10
        });

        // Draw location history on Map 
        var tripPath = [];
        var trip_path_history = <?php echo json_encode($trip_path_history); ?>;
        for(var i=0;i<trip_path_history.length;i++) {
            tripPath.push( { lat: Number(trip_path_history[i].latitude), lng: Number(trip_path_history[i].longitude) } );
        }
        var tripPath = new google.maps.Polyline({
            path: tripPath,
            geodesic: true,
            strokeColor: 'Blue',
            strokeOpacity: 0.3,
            strokeWeight: 5,
        });
        tripPath.setMap(map);
        // end

        // Ongoing Trip : Live Update
        /* if ongoing trip, update map when get new location data */
            console.log('Tracking Server : <?= config('heavygari.tracker.url') ?>');
            var socket = io('<?= config('heavygari.tracker.url') ?>');
            var tripUniqueId = '<?= $demo_trip_id ?>';

            // prepare the marker
            var vehicleLat = (trip_path_history) ? trip_path_history[trip_path_history.length-1].latitude : 23.00; // TODO : TRIP START LOCATIONS
            var vehicleLon = (trip_path_history) ? trip_path_history[trip_path_history.length-1].longitude : 90.00; 
            var vehicleMarker = new google.maps.Marker({
                position: { lat: vehicleLat, lng: vehicleLon },
                map: map
            });
            console.log('vehicle current location =>');
            console.log(vehicleLat+','+vehicleLon);

            attachPopUpMessage(vehicleMarker, 'Demo Metro Ka-1234'); // TODO : GET REAL TITLE
            // end 

            // When we get any update from driver app, draw line on map
            socket.on('liveTripUpdates', function (data) {
                console.log('---Got New Data---');
                console.log(data);
                console.log(tripUniqueId);
                if (data.tripId==tripUniqueId) {
                    console.log('new updates for current trip :'+tripUniqueId+'.  data =>');
                    console.log(data);

                    // Update trip path
                    var lastLatlng = new google.maps.LatLng(data.latitude, data.longitude);
                    tripPath.getPath().push(lastLatlng);
                    console.log('point added : '+data.latitude+', '+data.longitude);

                    // Update map & marker
                    vehicleMarker.setPosition(lastLatlng);
                    map.setCenter(lastLatlng);
                }
            });
        // end
    }

    // Attaches an info window to a marker with the provided message.
    function attachPopUpMessage(marker, message) {
        var infowindow = new google.maps.InfoWindow({
            content: message
        });
        marker.addListener('click', function() {
            infowindow.open(marker.get('map'), marker);
        });

        infowindow.open(marker.get('map'), marker);
    }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key={{config('heavygari.google_maps.api_key')}}&callback=initMap" ></script>
@stop