<style>
#map {
    height: 600px;
    width: 100%;
}
</style>

@extends('website.layout')

@section('content-body')
<h3>Demo Map Builder : #{{ $demo_trip_id }}</h3>
<div id="map"></div>
@stop

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>

<script>

var socket = io('<?= config('heavygari.tracker.url') ?>');

function initMap() {

    var center = {
        lat: 23.800008,
        lng: 90.396601
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: center
    });

    var marker = new google.maps.Marker({
        position: center,
        map: map,
        draggable:true,
        title:"Drag me!"
    });

    marker.addListener('dragend', function() {
        console.log(marker.getPosition().lat(), marker.getPosition().lng());
        map.setCenter(marker.getPosition());
        sendData(marker.getPosition());
    });
}

function sendData(location){
    var jsonData = {
      tripId: '<?= $demo_trip_id ?>',
      timestamp: Date.now(),
      latitude: location.lat(),
      longitude: location.lng()
    };

    socket.emit('liveTripUpdates', jsonData);
    
    console.log(jsonData);
}
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key={{config('heavygari.google_maps.api_key')}}&callback=initMap" ></script>
@stop