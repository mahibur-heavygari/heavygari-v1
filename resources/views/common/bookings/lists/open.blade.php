<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="booking-table" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="5%">তারিখ ও সময়</th>
                        <th>বুকিং আইডি ও গাড়ীর ধরণ</th>
                        <th>পারিশ্রমিক</th>
                        <th>দূরত্ব</th>
                        @if(isset($ownerPanel))
                        <th>গাড়ির মালিকের আয়</th>
                        @endif
                       <!--  <th>বুকিং-এর ধরণ</th> -->
                       <!--  <th>গাড়ি-এর ধরণ</th> -->
                        <th>পিক আপ</th>
                        <th>গন্তব্য</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bookings as $booking)
                    <tr>
                        <td width="5%">
                            <div class="text-nowrap">{{$booking->datetime->setTimezone('UTC')->format('h:ia d/m/y')}}</div>
                        </td>
                        <td>
                            <a href="{{Request::url()}}/{{$booking->unique_id}}" class="text-nowrap">{{$booking->unique_id}}</a> ( @if($booking->booking_category == 'full') {{$booking->fullBookingDetails->vehicleType->title}} @else &nbsp; @endif )
                        </td>

                        <td>
                            <div class="text-nowrap text-lighter text-sm">
								টাকা {{$booking->invoice->total_cost}}                               
                            </div>
                        </td>
                        <td>
                            <div class="text-nowrap text-lighter text-sm">
                                {{$booking->trips[0]->distance}}km
                            </div>
                        </td>
                        @if(isset($ownerPanel))
                        <td>
                            <div class="text-nowrap text-lighter text-sm">
									টাকা {{$booking->earnings->owner_earning}}
                            </div>
                        </td>
                        @endif
                        <!-- <td>
                            <div class="text-nowrap text-lighter text-sm">  
                            {{($booking->booking_category=='full') ? 'সম্পূর্ণ গাড়ির বুকিং' : 'পার্সেল ডেলিভারি'}}
                            </div>
                        </td> -->
                        <!-- <td>
                            <div class="text-nowrap text-lighter text-sm"> 
                            @if($booking->booking_category == 'full') 
                                {{$booking->fullBookingDetails->vehicleType->title}}
                            @else
                                &nbsp;
                            @endif
                            </div>
                        </td> -->
                        @if(isset($ownerPanel) || isset($adminPanel))
                            <td><b>{{$booking->trips[0]->origin->fullname}}</b> <span style="font-size: 12px;">({{$booking->trips[0]->from_address}})</span></td>
                            <td><b>{{$booking->trips[0]->destination->fullname}}</b>  <span style="font-size: 12px;">({{$booking->trips[0]->to_address}})</span></td>
                        @else
                            <td>{{$booking->trips[0]->origin->fullname}}</td>
                            <td>{{$booking->trips[0]->destination->fullname}}</td>
                        @endif
                        <td class="text-right">
                            <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                            	@if(isset($adminPanel))
                            		@if($booking->booking_type=='on-demand')
                            			@php
                            				$expire_at_str = (string)date('Y-m-d H:i:s', strtotime($booking->created_at . ' +60 minutes'));    
									        $cancel_left_time = (int) round(abs(strtotime($expire_at_str) - strtotime($present_time_str)) / 60, 2); 
                            			@endphp
	                            		<li>
		                                    <a href="{{Request::url()}}/{{$booking->unique_id}}/extend-expiry-time" class="btn btn-success btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">Extend</a>
		                                    <br>
		                                    <span class="booking_cancel_text_blink text-right">{{$cancel_left_time}} minutes&nbsp;</span>
		                                </li>
		                                <br>
		                            @endif
	                                <li>
	                                    <a href="{{Request::url()}}/{{$booking->unique_id}}/accept" class="btn btn-success btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">গ্রহণ</a>
	                                </li>                     	
	                                <li>
	                                    <a href="{{Request::url()}}/{{$booking->unique_id}}/cancel" class="btn btn-danger btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">বাতিল </a>
	                                </li>
                            	@elseif(isset($ownerPanel))                            		
	                                <li>
	                                    <a href="{{Request::url()}}/{{$booking->unique_id}}/accept" class="btn btn-success btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">গ্রহণ</a>
	                                </li>
                            	@else                            	
	                                <li>
	                                    <a href="{{Request::url()}}/{{$booking->unique_id}}/cancel" class="btn btn-danger btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">বাতিল </a>
	                                </li>
                            	@endif                                
                                <li>
                                    <a href="{{Request::url()}}/{{$booking->unique_id}}" class="btn btn-gray btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">বিস্তারিত</a>
                                </li>
                              
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('footer')
  <script>
    function ConfirmAction()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop