<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>তারিখ ও সময়</th>
                        <th>বুকিং আইডি</th>
                        <th>পারিশ্রমিক</th>
                        <th>দূরত্ব</th>
                        @if(isset($ownerPanel))
                        <th>গাড়ির মালিকের আয়</th>
                        @endif
                        <th>বুকিং-এর ধরণ</th>
                        <th>পিক আপ</th>
                        <th>গন্তব্য</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bookings as $booking)
                    <tr>
                        <td>
                            <div class="text-nowrap">{{$booking->datetime->setTimezone('UTC')->toDayDateTimeString()}}</div>
                        </td>
                        <td>
                            <a href="{{Request::url()}}/{{$booking->unique_id}}" class="text-nowrap">{{$booking->unique_id}}</a>
                        </td>
                        <td>
                            <div class="text-nowrap text-lighter text-sm">
                                টাকা {{$booking->invoice->total_cost}}                               
                            </div>
                        </td>
                        <td>
                            <div class="text-nowrap text-lighter text-sm">
                                {{$booking->trips[0]->distance}}km
                            </div>
                        </td>
                        @if(isset($ownerPanel))
                        <td>
                            <div class="text-nowrap text-lighter text-sm">
                                    টাকা {{$booking->earnings->owner_earning}}
                            </div>
                        </td>
                        @endif
                        <td>
                            <div class="text-nowrap text-lighter text-sm">{{$booking->booking_category}}</div>
                        </td>
                        <td>{{$booking->trips[0]->origin->fullname}}</td>
                        <td>{{$booking->trips[0]->destination->fullname}}</td>
                        <td class="text-right">
                            <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">                                
                                <li>
                                    <a href="{{Request::url()}}/{{$booking->unique_id}}" class="btn btn-gray btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">বিস্তারিত</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>