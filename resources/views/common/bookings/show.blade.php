@section('header')
<link rel="stylesheet" href="/dashboard/plugins/datatable/css/dataTables.bootstrap4.min.css">
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            @if((isset($adminPanel)))
            <h1 class="title">Booking </h1>
            @else
            <h1 class="title">বুকিং </h1>
            @endif
            <div class="title-sub">
                <span class="fs-18">Id #{{$booking->unique_id}}</span>
            </div>
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                @if(isset($adminPanel) && $booking->status=='open' && $booking->msg_to_owners=='no')
                    <li>
                        <a href="{{Request::url()}}/msg-to-owners" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();"><i class="fa fa-paper-plane"></i> SMS to Owners</a>
                        <br>
                    </li>
                @endif
                @if(isset($adminPanel) && $booking->status=='open' && $booking->msg_to_potential_owners=='no')
                    <li>
                        <a href="{{Request::url()}}/msg-to-potential-owners" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();"><i class="fa fa-paper-plane"></i> SMS to Potential Owners</a>
                        <br>
                    </li>
                @endif
                @if(isset($adminPanel) && $booking->status=='open' && $booking->msg_to_drivers=='no')
                    <li>
                        <a href="{{Request::url()}}/msg-to-drivers" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();"><i class="fa fa-paper-plane"></i> SMS to Drivers</a>
                        <br>
                    </li>
                @endif
                @if(isset($adminPanel) && $booking->status=='open' && $booking->booking_type=='on-demand')
                    <li>
                        <a href="{{Request::url()}}/extend-expiry-time" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();"><i class="fa fa-plus"></i> Extend Expiry Time </a>
                        <br>
                        <span class="booking_cancel_text_blink text-right">{{$cancel_left_time}} minutes&nbsp;left to cancel</span>
                    </li>
                @endif
                @if(isset($adminPanel))
                    <li>
                        <a href="{{Request::url()}}/edit" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1"><i class="fa fa-edit"></i> Edit </a>
                        <br>
                    </li>
                @endif
                <li>
                    <span class="btn @if($booking->status=='completed') btn-success @elseif($booking->status=='ongoing') btn-success @elseif($booking->status=='open') btn-info @elseif($booking->status=='upcoming') btn-primary  @elseif($booking->status=='cancelled') btn-danger @endif btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-check"></i>@if(isset($adminPanel)) {{$booking->status}} @else {{$booking->banglaStatus()}} @endif
                    @if(!is_null($booking->cancelled_by) && isset($adminPanel)) ( {{$booking->cancelled_by}} cancellation ) @endif</span>
                </li>
                @if(isset($adminPanel) && $booking->status=='cancelled' && $booking->booking_type=='on-demand')
                    <li>
                        <a href="{{Request::url()}}/make-open" class="btn btn-success btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">Make it open</a>
                        <br>
                    </li>
                @endif
                {{--@if($booking->status=='ongoing') 
                <li>
                    <a href="{{url('/booking/'.$booking->tracking_code)}}" target="_blank" class="btn"><i class="fa fa-map-marker"></i> ট্র্যাকিং পেজ</a>
                </li>
                @endif--}}
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3">                
                @if($booking->vehicle)
                    <div class="card card-site card-trip-boat mb-4" data-mh="trip-details-card-match">
                        <div class="card-image" style="background-image: url('{{URL::asset('dashboard/images/vehicles/boat-lg1.jpg')}}')"></div>
                        <div class="card-block">
                            <h4 class="card-title">{{$booking->vehicle->number_plate}}</h4>
                            <!--<p class="card-text">{{$booking->vehicle->about}}</p>-->
                        </div>
                    </div>
                @else
                    <div class="card card-site card-lg card-trip-metas mb-4" data-mh="trip-details-card-match">
                        <div class="card-header">
                            @if((isset($adminPanel)))
                            <div class="card-heading-title">Vehicle</div>
                            @else
                            <div class="card-heading-title">গাড়ি</div>
                            @endif
                        </div>                        
                    </div>
                @endif
            </div>            
            <div class="col-sm-3">
                <div class="card card-site card-lg card-trip-metas mb-4" data-mh="trip-details-card-match">
                    <div class="card-header">
                        @if((isset($adminPanel)))
                        <div class="card-heading-title">Customer</div>
                        @else
                        <div class="card-heading-title">গ্রাহক</div>
                        @endif
                    </div>
                    <div class="card-block text-center">
                        <div class="avater-block">
                            <img class="rounded-circle" src="{{Storage::url($booking->customer->user->thumb_photo)}}" alt="...">
                            <h5 class="card-title"><a href="/cms/users/customers/{{$booking->customer->id}}">{{$booking->customer->user->name}}</a></h5>
                        </div>
                        <div class="rating-site" style="font-size: .5em;">
                            <input type="text" class="kv-uni-star rating-loading" value="{{$booking->customer->myAverageRating()}}" title="" readonly="" >
                        </div>
                        <ul class="ctm-gen">
                            <li>
                                <span>{{$booking->customer->user->email}}</span>
                            </li>
                            <li><span>{{$booking->customer->user->phone}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">                
                    <div class="card card-site card-lg card-trip-metas mb-4" data-mh="trip-details-card-match">
                        <div class="card-header">
                            @if((isset($adminPanel)))
                            <div class="card-heading-title">Driver</div>
                            @else
                            <div class="card-heading-title">চালক</div>
                            @endif
                        </div>
                        @if($booking->driver)
                        <div class="card-block text-center">
                            <div class="avater-block">
                                <img class="rounded-circle" src="{{Storage::url($booking->driver->user->thumb_photo)}}" alt="...">
                                
                                @if(!is_null($booking->hiredDriver))
                                    <h5 class="card-title">{{$booking->hiredDriver->name}} <span class="badge badge-warning">Hired</span></h5>
                                @else
                                    <h5 class="card-title"><a href="/cms/users/drivers/{{$booking->driver->id}}">{{$booking->driver->user->name}}</a></h5>
                                @endif

                            </div>
                            <div class="rating-site" style="font-size: .5em;">
                                @if(!is_null($booking->hiredDriver))
                                    &nbsp;
                                @else
                                    <input type="text" class="kv-uni-star rating-loading" value="{{$booking->driver->myAverageRating()}}"  title="" readonly="">
                                @endif
                            </div>
                            <ul class="ctm-gen">
                                <li>
                                    @if(!is_null($booking->hiredDriver))
                                        <span>&nbsp;</span>
                                    @else
                                        <span>{{$booking->driver->user->email}}</span>
                                    @endif
                                </li>
                                <li>
                                    @if(!is_null($booking->hiredDriver))
                                        <span>{{$booking->hiredDriver->phone}}</span>
                                    @else
                                        <span>{{$booking->driver->user->phone}}</span>
                                    @endif
                                </li>
                            </ul>
                            <!--<div class="note">
                                Cards are built with as little markup and styles as possible, but still manage to deliver a ton of control and customization.
                            </div>-->
                            @if(isset($adminPanel) && $booking->status == 'upcoming')
                                <a href="{{Request::url()}}/driver-change" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();"> Change Driver</a>
                            @endif
                        </div>
                        @endif
                    </div>                
            </div>
            <div class="col-sm-3">                
                <div class="card card-site card-lg card-trip-metas mb-4" data-mh="trip-details-card-match">
                    @if(isset($adminPanel))
                        <div class="card-header">
                            @if((isset($adminPanel)))
                            <div class="card-heading-title">Owner</div>
                            @else
                            <div class="card-heading-title">মালিক</div>
                            @endif
                        </div>
                        @if($booking->owner)
                            <div class="card-block text-center">
                                <div class="avater-block">
                                    <img class="rounded-circle" src="{{Storage::url($booking->owner->user->thumb_photo)}}" alt="...">
                                    <h5 class="card-title"><a href="/cms/users/owners/{{$booking->owner->id}}">{{$booking->owner->user->name}}</a></h5>
                                </div>
                                <ul class="ctm-gen">
                                    <li>
                                        <span>{{$booking->owner->user->email}}</span>
                                    </li>
                                    <li>
                                        <span>{{$booking->owner->user->phone}}</span>
                                    </li>
                                </ul>
                            </div>
                        @endif
                    @endif
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="banner-negative-left-right border-top">
    <div class="banner-positive-left">
        <div class="row">
            <div class="col-md-6 col-xl-5">
                <div class="booking-form-card booking-confrim">
                    @if((isset($adminPanel)))
                    <h5 class="bfc-title mt-0">Booking Details</h5>
                    @else
                    <h5 class="bfc-title mt-0">বুকিং-এর  বিবরণ</h5>
                    @endif

                    <ul class="confrim-list">
                        @if($booking->booking_category=='full')
                            <li>
                                @if((isset($adminPanel)))
                                <span>Booking ID:</span>
                                @else
                                <span>বুকিং আইডি:</span>
                                @endif
                                <span>{{$booking->unique_id}}</span>
                            </li>  
                            <li>@if((isset($adminPanel)))
                                <span>Booking Type:</span>
                                @else
                                <span>বুকিং-এর ধরণ:</span>
                                @endif
                                @if((isset($adminPanel)))
                                <span>Full Booking:</span>
                                @else
                                <span>সম্পূর্ণ গাড়ির বুকিং</span>
                                @endif
                            </li>                                
                            <li>
                                @if((isset($adminPanel)))
                                <span>Vehicle Type:</span>
                                @else
                                <span>গাড়ীর ধরণ :</span>
                                @endif
                                <span>{{$booking->fullBookingDetails->vehicleType->title}}</span>
                            </li>                        
                            <li>
                                @if((isset($adminPanel)))
                                <span>Capacity:</span>
                                @else
                                <span> ধারণ ক্ষমতা:</span>
                                @endif
                                <span>{{$booking->fullBookingDetails->capacity}} {{$booking->fullBookingDetails->vehicleType->capacityType->title}}</span>
                            </li>
                            
                        @else
                            <li>
                                @if((isset($adminPanel)))
                                <span>Booking Type:</span>
                                @else
                                <span>বুকিং-এর ধরণ:</span>
                                @endif

                                @if((isset($adminPanel)))
                                <span>Parcel Booking:</span>
                                @else
                                <span>পার্সেল ডেলিভারি</span>
                                @endif
                            </li>
                        @endif
                        <!--<li>
                            <span>বুকিং-এর সময়:</span>
                            <span>{{ucfirst($booking->booking_type)}}</span>
                        </li>-->
                        <li>
                            @if((isset($adminPanel)))
                            <span>Date & Time:</span>
                            @else
                            <span>তারিখ ও সময়:</span>
                            @endif
                            <span>{{$booking->datetime->setTimezone('UTC')->toDayDateTimeString()}}</span>
                        </li>

                        <li>
                            @if((isset($adminPanel)))
                            <span>Rent:</span>
                            @else
                            <span>পারিশ্রমিক:</span>
                            @endif
                            @if(isset($ownerPanel))
                                <span>{{$booking->invoice->total_cost+$booking->invoice->discount}} টাকা</span>
                            @else
                                @if($booking->invoice->discount>0)
                                    <span><strike>{{$booking->invoice->total_cost+$booking->invoice->discount}}</strike> {{$booking->invoice->total_cost}} 
                                        @if(isset($adminPanel)) TK. @else টাকা @endif
                                        @if(isset($adminPanel))<a href="/cms/bookings/{{$booking->unique_id}}/price-update">Edit</a>@endif</span>
                                @else
                                    <span>{{$booking->invoice->total_cost}} টাকা @if(isset($adminPanel))<a href="/cms/bookings/{{$booking->unique_id}}/price-update">Edit</a>@endif</span>
                                @endif
                            @endif
                            
                            <span>
                            @if(isset($adminPanel)) Invoice: @else ইনভয়েস : @endif    
                            
                            <!--
                            <a target="_blank" alt="show" href="{{url('/customer/panel/bookings/'.$booking->unique_id.'/invoice')}}"><i class="icofont icofont-ui-note"></i></a> | -->

                            @if($booking->invoice_pdf)
                            
                            <a target="_blank" alt="download" href="{{url(\Storage::url($booking->invoice_pdf))}}
                            " title="PDF File"><img src="/website/images/pdf-icon.png" height="25px" width="25px"></a>
                            @endif

                            </span>
                        </li>
                        @if(isset($adminPanel))
                            <li>
                                <span>HG Fee</span>
                                <span>{{ucfirst($booking->invoice->heavygari_fee)}} টাকা</span>
                            </li>
                        @endif

                        @if(isset($adminPanel))
                            <li>
                                <span>Owner's Earning</span>
                                <span><strong>{{$booking->invoice->total_cost - ucfirst($booking->invoice->heavygari_fee)}} টাকা</strong></span>
                            </li>
                        @endif
                        
                        <li>
                            <span>@if(isset($adminPanel)) Fare Payer: @else পারিশ্রমিকদাতা: @endif </span>
                            <span>{{ucfirst($booking->payment_by)}}</span>
                        </li>
                        <li>
                            <span>@if(isset($adminPanel)) Receiver: @else প্রাপক: @endif</span>
                            <span>{{$booking->recipient_name}} ({{$booking->recipient_phone}})</span>
                        </li>
                    </ul>

                    <h5 class="bfc-title">@if(isset($adminPanel)) Journey Information @else যাত্রার তথ্য @endif</h5>
                    <ul class="confrim-list">
                        @if($booking->status=='ongoing' || $booking->status=='completed')
                            <li>
                                <span>@if(isset($adminPanel)) Tracking @else ট্র্যাকিং  @endif URL:</span>
                                <span>                               
                                    <a href="{{url('/booking/'.$booking->tracking_code)}}" target="_blank" class="btn">{{url('/booking/'.$booking->tracking_code)}}</a>
                                </span>
                            </li>
                        @endif
                        <li>
                            <span>@if(isset($adminPanel)) Pick-up Address: @else পিক-আপের ঠিকানা:  @endif </span>
                            <span>                               
                                {{$booking->trips[0]->from_address}} ( {{$booking->trips[0]->origin->title}} )
                            </span>
                        </li>
                        @if($booking->status=='ongoing' || $booking->status=='completed')
                            <li>
                                <span>@if(isset($adminPanel)) Pick-up Time: @else পিক-আপের সময়:  @endif</span>
                                <span>                               
                                    {{$booking->trips[0]->started_at->toDayDateTimeString()}}
                                </span>
                            </li>
                        @endif
                        <li>
                            <span>@if(isset($adminPanel)) Drop-off Address: @else গন্তব্যের ঠিকানা:  @endif</span>
                            <span>                               
                                {{$booking->trips[0]->to_address}} ( {{$booking->trips[0]->destination->title}} )
                            </span>
                        </li>
                        @if($booking->status=='completed')
                            <li>
                                <span>@if(isset($adminPanel)) Drop-off Time: @else গন্তব্যে পৌঁছানোর সময়:  @endif </span>
                                <span>                               
                                    {{$booking->trips[0]->completed_at->toDayDateTimeString()}}
                                </span>
                            </li>
                        @endif
                        <li>
                            <span>@if(isset($adminPanel)) Distance: @else দূরত্ব: @endif </span>
                            @if($booking->trip_type=='single')
                                <span>{{$booking->trips[0]->distance}} @if(isset($adminPanel)) Km. @else কিমি @endif </span>
                            @else
                                <span>{{$booking->trips[0]->distance}} + {{$booking->trips[0]->distance}} = {{$booking->trips[0]->distance * 2}}@if(isset($adminPanel)) Km. @else কিমি @endif </span>
                            @endif
                        </li>
                        <li>
                            <span>@if(isset($adminPanel)) Trip Type: @else যাত্রার ধরণ: @endif </span>
                            <span>{{$booking->trip_type}}</span>
                        </li>
                        <!--
                        <li>
                            <span>যাত্রা শুরু হয়েছে:</span>
                            <span>@if($booking->trips[0]->started_at) {{$booking->trips[0]->started_at->toDayDateTimeString()}}  @else - @endif</span>
                        </li>
                        <li>
                            <span>যাত্রা সম্পন্ন  হয়েছে:</span>
                            <span>@if($booking->trips[0]->completed_at) {{$booking->trips[0]->completed_at->toDayDateTimeString()}}  @else - @endif</span>
                        </li>
                        -->
                    </ul>

                    <!--
                    <h5 class="bfc-title">প্রাপকের বিবরণ</h5>
                    <ul class="confrim-list">
                        <li>
                            <span>নাম:</span>
                            <span>{{$booking->recipient_name}}</span>
                        </li>
                        <li>
                            <span>ফোন নং:</span>
                            <span>{{$booking->recipient_phone}}</span>
                        </li>
                    </ul>
                    -->

                    <h5 class="bfc-title">@if(isset($adminPanel)) Product Details @else পণ্যের  বিবরণ @endif </h5>
                    <ul class="confrim-list">
                        @if($booking->booking_category=='shared')
                            <li>
                                <span>@if(isset($adminPanel)) Product Category @else পণ্য এর ক্যাটাগরি: @endif </span>
                                <span>
                                    {{$booking->sharedBookingDetails->productCategory->title}}
                                </span>                                
                            </li>
                            <li>
                                <span>@if(isset($adminPanel)) Product Quantity @else পণ্য সংখ্যা: @endif </span>
                                <span>
                                    {{$booking->sharedBookingDetails->quantity}}
                                </span>
                            </li>

                            <li>
                                <span>@if(isset($adminPanel)) Weight: @else ওজন: @endif </span>
                                <span>
                                    @if($booking->sharedBookingDetails->weightCategory)
                                        {{$booking->sharedBookingDetails->weightCategory->title}}
                                    @else
                                        {{$booking->sharedBookingDetails->weight}} kg
                                    @endif
                                </span>                       
                            </li>

                            <li>
                                <span>@if(isset($adminPanel)) Volume: @else আয়তন: @endif </span>
                                <span>
                                    @if($booking->sharedBookingDetails->volumetricCategory)
                                        {{$booking->sharedBookingDetails->volumetricCategory->title}}
                                    @else
                                        {{$booking->sharedBookingDetails->volumetric_width}} inch * {{$booking->sharedBookingDetails->volumetric_height}} inch * {{$booking->sharedBookingDetails->volumetric_length}} inch
                                    @endif
                                </span>
                            </li>
                        @endif
                        <li>
                            <span>@if(isset($adminPanel)) Product Category: @else পণ্য এর ক্যাটাগরি: @endif </span>
                            <span>{{$booking->particular_details}}</span>
                        </li>
                        <li>
                            <span>@if(isset($adminPanel)) Waiting Time: @else অপেক্ষার সময়: @endif </span>
                            <span>{{$booking->waiting_time}} <span class="small">(approx.)</span></span>
                        </li>
                        <li>
                            <span>@if(isset($adminPanel)) Special Instructions: @else বিশেষ নির্দেশনা: @endif </span>
                            <span>{{$booking->special_instruction}}</span>
                        </li>  

                        @if(isset($adminPanel))
                            <li>
                                <span>@if(isset($adminPanel)) Delivery Order Photos @else ডেলিভারি অর্ডার ছবি @endif </span>
                                <span>
                                    @if($booking->delivery_order_photo)
                                        <a href="/cms/bookings/{{$booking->unique_id}}/delivery-order-photo">Photo</a>
                                    @else
                                        Not uploaded
                                    @endif
                                </span>                       
                            </li>
                            <li>
                                <span>@if(isset($adminPanel)) Delivery Invoice Photos @else ডেলিভারি ইনভয়েস ছবি @endif</span>
                                <span>
                                    @if($booking->delivery_invoice_photo)
                                        <a href="/cms/bookings/{{$booking->unique_id}}/delivery-invoice-photo">Photo</a>
                                    @else
                                        Not uploaded
                                    @endif
                                </span>                       
                            </li>
                        @endif
                        
                        @if(isset($adminPanel) && $booking->status=='cancelled')
                            <li>
                                <span>@if(isset($adminPanel)) Reason of Cancellation @else বাতিলের কারণ: @endif </span>
                                @if(!is_null($booking->cancel_reason))
                                    <span>{{$booking->cancel_reason}}</span>
                                @elseif($booking->cancelled_by=='Admin')
                                    <span>Admin cancelled the booking</span>
                                @elseif($booking->cancelled_by=='Customer')
                                    <span>Customer cancelled the booking without any reason</span>
                                @elseif($booking->cancelled_by=='Owner')
                                    <span>Owner cancelled the booking</span>
                                @elseif($booking->cancelled_by=='Driver')
                                    <span>Driver cancelled the booking</span>
                                @elseif($booking->cancelled_by=='Automatic')
                                    <span>Automatically cancelled</span>
                                @else
                                    <span>&nbsp;</span>
                                @endif
                            </li> 
                        @endif
                                                    
                    </ul>
                </div>
                @if(isset($adminPanel))
                    <h3 class="bfc-title">@if(isset($adminPanel)) Admin @else অ্যাডমিন @endif </h3>
                    <div class="form-group">
                        <form class="" method="POST" enctype="multipart/form-data" action="/cms/bookings/{{$booking->unique_id}}/save_admin_note" >
                        {{ csrf_field() }}
                            <div class="input-group input-flex">
                                <span class="input-group-addon" id="addon-description">&nbsp;&nbsp;&nbsp;&nbsp;@if(isset($adminPanel)) Admin Note @else অ্যাডমিন নোট @endif</span>
                                <textarea id="admin_note" name="admin_note" rows="5" class="form-control" aria-describedby="addon-description">@if(!is_null($booking->admin_note)){{$booking->admin_note}}@endif</textarea>
                            </div>
                            <div class="input-group input-flex">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </form>
                    </div> 
                @endif
            </div>
            <div class="col-md-6 col-xl-7">
                <div class="map-block map-booking">
                <iframe src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking->trips[0]->from_lat}},{{$booking->trips[0]->from_lon}}&destination={{$booking->trips[0]->to_lat}},{{$booking->trips[0]->to_lon}}" width="800" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<div class="map-block-overlay"></div>-->
            </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
  <script>
    function ConfirmAction()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop