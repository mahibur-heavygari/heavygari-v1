<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
<script type="text/javascript">
    var socket = io('<?= config('heavygari.tracker.url') ?>');
    var tracking_details = <?php echo json_encode($tracking_details); ?>;
</script>
<script src="/website/js/tracker.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{config('heavygari.google_maps.api_key')}}&libraries=places&callback=initMap" async defer></script>