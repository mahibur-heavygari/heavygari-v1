<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="driver-payments-table" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Driver</th>
                        <th>Driver Phone</th>
                        <th>Owner</th>
                        <th>Owner Phone</th>
                        <th>Total Bonus</th>
                        <th>Paid on</th>
                        <th>Payment Method</th>
                        <th>Status</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>                    
                    @foreach($list as $payment)
                        <tr>
                            <td>
                                <div class="text-nowrap">{{date('d/m/y', strtotime($payment->created_at))}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap"><a href="/cms/users/drivers/{{$payment->driver->id}}">{{$payment->driver->user->name}}</a></div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$payment->driver->user->phone}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap"><a href="/cms/users/owners/{{$payment->owner->id}}">{{$payment->owner->user->name}}</a></div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$payment->owner->user->phone}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$payment->total_amount}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
	                                 @if($payment->paid_at) 
	                                    {{$payment->paid_at}}
	                                 @else
	                                    N/A
	                                 @endif
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$payment->payment_method}}</div>
                            </td>
                            <td>
                                <span class="badge @if($payment->status=='paid') badge-success @elseif($payment->status=='unpaid') badge-danger  @else badge-warning @endif">{{$payment->status}}</span>
                            </td>
                            <td>
                                <a href="/cms/payment/drivers/{{$payment->id}}/commissions" title="All Commissions" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1"><i class="icofont icofont-list"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>