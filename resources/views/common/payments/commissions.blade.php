<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="driver-payments-table" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Booking Date</th>
                        <th>Booking Id</th>
                        <th>Customer</th>
                        <th>Owner</th>
                        <th>Status</th>
                        <th>Bonus</th>
                    </tr>
                </thead>
                <tbody>                    
                    @foreach($commission_items as $item)
                        <tr>
                        	<td>
                        		<div class="text-nowrap">{{date('d/m/Y', strtotime($item->bookings->datetime))}}</div>
                        	</td>
                            <td>
                                <div class="text-nowrap"><a href="/cms/bookings/{{$item->bookings->unique_id}}">{{$item->bookings->unique_id}}</a></div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$item->bookings->customer->user->name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$item->commission->owner->user->name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap">
                                	<span class="badge @if($item->commission->status=='paid') badge-success @elseif($item->commission->status=='unpaid') badge-danger  @else badge-warning @endif">{{$item->commission->status}}</span>
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap">{{$item->payable}}</div>
                            </td>               
                        </tr>
                    @endforeach
                </tbody>
                <thead>
	              <tr>
	                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
	                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
	                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
	                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
	                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
	                <th class="text-left"><span class="text-lighter">Total: {{$driver_commission->total_amount}}</span></th>
	              </tr>
	            </thead>
            </table>
        </div>
    </div>
</div>