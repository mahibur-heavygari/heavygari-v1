@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">{{$vehicle->number_plate}}</h1>
            <div class="title-sub">
                <span class="fs-18"></span>
            </div>
        </div>
        <div class="page-header-cell">
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="vehicle-details-banner" style="background-image: url({{Storage::url($vehicle->thumb_photo)}})">
    <div class="vd-banner-content">
        <div class="vd-content">
            <h1 class="banner-title">{{$vehicle->name}}</h1>
            <div class="banner-text">
                {{$vehicle->about}}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-site card-lg card-trip-metas mb-4 card-vehicle-details" data-mh="trip-details-card-match">
                    <div class="card-block">
                        <div class="card-divider-title">গাড়ীর বিবরণ</div>
                        <ul class="list-unstyled vehicle-sort-info">
                            <li><span class="name-text">রেফারেন্স নং :</span> <span>{{$vehicle->ref_no}}</span></li>
                            <li><span class="name-text">রেজিস্ট্রেশন নং :</span> <span>{{$vehicle->vehicle_registration_number}}</span></li>
                            <li><span class="name-text">ফিটনেস সনদ নং :</span> <span>{{$vehicle->fitness_number}}</span></li>
                            <li><span class="name-text">ফিটনেস সনদ মেয়াদোত্তীর্ণ তারিখ:</span> <span>{{$vehicle->fitness_expiry}}</span></li>
                            <li><span class="name-text">নাম্বার প্লেট :</span> <span>{{$vehicle->number_plate}}</span></li>
                            <li><span class="name-text">ট্যাক্স টোকেন নং :</span> <span>{{$vehicle->tax_token_number}}</span></li>
                            <li><span class="name-text">ট্যাক্স টোকেন মেয়াদোত্তীর্ণ তারিখ:</span> <span>{{$vehicle->tax_token_expirey}}</span></li>
                            <li><span class="name-text">চেসিস নং :</span> <span>{{$vehicle->chesis_no}}</span></li>
                            <li><span class="name-text">সিসি লিমিট :</span> <span>{{$vehicle->engine_capacity_cc}}</span></li>
                            <li><span class="name-text">গাড়ীর রাইড শেয়ারিং এনলিসমেন্ট সনদ :</span> <span>{{$vehicle->ride_sharing_certificate_no}}</span></li>
                            <li><span class="name-text">গাড়ীর ইন্সুরেন্স নং :</span> <span>{{$vehicle->insurance_no}}</span></li>
                            <li><span class="name-text">রেজিস্ট্রেশনকারীর নাম :</span> <span>{{$vehicle->registered_by}}</span></li>
                            <li><span class="name-text">উচ্চতা ( Height in feet ):</span> <span>{{$vehicle->height}}</span></li>
                            <li><span class="name-text">দৈর্ঘ্য ( Length in feet ):</span> <span>{{$vehicle->length}}</span></li>
                            <li><span class="name-text">প্রস্থ ( Width in feet ):</span> <span>{{$vehicle->width}}</span></li>
                            <li><span class="name-text">ধারণক্ষমতা ( Custom Capacity ):</span> <span>{{$vehicle->custom_capacity}}</span></li>
                        </ul>
                        @if(isset($adminPanel))
                            <div class="card-divider-title mt-4">মালিকের নাম</div>
                            <div class="me-feed">                            
                                <div class="title"><a href="/cms/users/owners/{{$vehicle->owner->id}}">{{$vehicle->owner->user->name}}</a></div> 
                            </div>  
                        @endif

                        <div class="card-divider-title mt-4">বর্তমান চালক </div>
                        <div class="me-feed">
                            @if(isset($adminPanel))
                                @if($vehicle->currentDriver)
                                    <a href="/cms/users/drivers/{{$vehicle->currentDriver->id}}" class="avater">
                                        <img src="{{URL::asset('dashboard/images/user.jpg')}}" alt="...">
                                    </a>
                                    <div class="denote">
                                        <div class="title"><a href="/cms/users/drivers/{{$vehicle->currentDriver->id}}">{{$vehicle->currentDriver->user->name}}</a></div>
                                        <div class="meta-note">{{$vehicle->currentDriver->user->phone}}</div>
                                    </div>
                                @else
                                    N/A
                                @endif
                                <div class="card-divider-title mt-4">অথোরাইজড চালকগণ</div>                       
                                <div class="me-feed">
                                    @foreach($vehicle->authorizedDrivers as $driver)
                                        <div class="title"><a href="/cms/users/drivers/{{$driver->id}}">{{$driver->user->name}}</a></div>
                                    @endforeach     
                                </div>  
                            @elseif(isset($ownerPanel))
                                @if($vehicle->currentDriver)
                                    <a href="/owner/panel/drivers/{{$vehicle->currentDriver->id}}" class="avater">
                                        <img src="{{URL::asset('dashboard/images/user.jpg')}}" alt="...">
                                    </a>
                                    <div class="denote">
                                        <div class="title"><a href="/owner/panel/drivers/{{$vehicle->currentDriver->id}}">{{$vehicle->currentDriver->user->name}}</a></div>
                                        <div class="meta-note">{{$vehicle->currentDriver->user->phone}}</div>
                                    </div>
                                @else
                                    N/A
                                @endif
                                <div class="card-divider-title mt-4">অথোরাইজড চালকগণ</div>                       
                                <div class="me-feed">
                                    @foreach($vehicle->authorizedDrivers as $driver)
                                        <div class="title"><a href="/owner/panel/drivers/{{$driver->id}}">{{$driver->user->name}}</a></div>
                                    @endforeach     
                                </div> 
                            @endif   
                        </div>                   
                    </div>
                </div>
            </div>
            {{--
            <div class="col-md-8">
                <div class="card card-site card-trip-boat mb-4" data-mh="trip-details-card-match">
                    <div class="card-block">
                    <div class="card-divider-title"> যেখানে আছেন</div>
                        <div class="map-block map-vehicle-details">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3686886.057445041!2d-101.80805374574473!3d40.63812396049718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1499758580250" width="800" height="520" frameborder="0" style="border:0"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            --}}
        </div>
    </div>
</div>
@stop