<div class="row">
      <div class="col-sm-12 col-md-12">
          <div class="mb-5 transaction-chart">
            {{--<canvas id="transactionChart" width="400" height="200"></canvas>--}}
          </div>
          <div class="row">
             <div class="col-md-6">
                <div class="mb-4">
                    &nbsp;
                </div>
              </div>
              <div class="col-md-6">
                  <div class="mb-4 text-right">
                      <ul class="list-inline mb-0">
                          <li>
                              <span class="text-lighter">Paid Amount:</span>
                              <span class="text-lighter">{{$admin_paid_commission_total}}</span>
                          </li>
                          <li>
                              <span class="text-lighter">Due Amount:</span>
                              <span class="text-lighter">{{$admin_due_commission_total}}</span>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="table-responsive">
            <table id="transaction-table" class="table table-transaction table-bordered">
                <thead>
                    <tr>
                        <th class="text-left"><span class="text-lighter">Date</span></th>
                        <th class="text-center">
                          <span class="text-lighter">Trip Id</span>
                        </th>
                        <th class="text-center">
                          <span class="text-lighter">Customer</span>
                        </th>
                        <th class="text-center">
                          <span class="text-lighter">Driver</span>
                        </th>
                        <th class="text-center">
                          <span class="text-lighter">Your Earnings</span>
                        </th>
                        <th class="text-center text-lighter">
                          <table>
                            <tr>
                              <th colspan="2">Admin Earnings</th>
                            </tr>
                            <tr>
                              <th class="text-center">
                                <div class="text-lighter">Amount</div>
                              </th>
                              <th class="text-center">
                                <div class="text-lighter">Status</div>
                              </th>
                            </tr>
                          </table>
                        </th>
                        <th class="text-center">
                          <span class="text-lighter">Fare</span>
                        </th>
                       </tr> 
                </thead>
                <tbody>
                  @foreach($bookings as $booking)
                  <tr>
                    <td class="text-left"><span class="text-lighter">{{date('d-m-Y', strtotime($booking['datetime']))}}</span></td>
                    <td class="text-center"><span class="text-lighter"><a href="/owner/panel/trips-by-my-vehicles/{{$booking['unique_id']}}">{{$booking['unique_id']}}</a></span></td>

                    <td class="text-left"><span class="text-lighter">{{$booking['customer_name']}} ({{$booking['customer_phone']}})</span></td>
                    <td class="text-left"><span class="text-lighter"><a href="/owner/panel/drivers/{{$booking['driver_id']}}">{{$booking['driver_name']}}</a> ({{$booking['driver_phone']}})</span></td>
                    <td class="text-center"><span class="text-lighter">{{$booking['owner_earning']}}</span></td>
                    <td>
                      <table>
                        <tr>
                          <td class="text-center">
                            <div class="text-lighter">{{$booking['admin_commission']+$booking['driver_commission']}}</div>
                          </td>
                          <td class="text-center">
                            @if($booking['admin_commission_status']=='paid')
                              <div class="text-success">{{$booking['admin_commission_status']}}</div>
                            @else
                              <div class="text-danger">{{$booking['admin_commission_status']}}</div>
                            @endif
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td class="text-center"><span class="text-lighter">{{$booking['customer_cost']}}</span></td>
                  </tr>
                  @endforeach
                </tbody>
            <thead>
              <tr>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-center"><span class="text-lighter">Total Earnings: {{$owner_earning_total}}</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-center"><span class="text-lighter">Total Fare: {{$total_fare}}</span></th>
              </tr>
            </thead>
            </table>
          </div>
      </div>
  </div>