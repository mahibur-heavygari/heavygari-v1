<div class="row">
  <div class="col-sm-12 col-md-12">
      {{--<div class="mb-5 transaction-chart">
        <canvas id="transactionChart" width="400" height="200"></canvas>
      </div>--}}
      <div class="table-responsive">
        <table id="transaction-table" class="table table-transaction table-bordered">
            <thead>
                <tr>
                    <th class="text-left"><span class="text-lighter">Date</span></th>
                    <th class="text-center">
                      <span class="text-lighter">Trip Id</span>
                    </th>
                    <th class="text-center">
                      <span class="text-lighter">Customer</span>
                    </th>
                    <th class="text-center">
                      <span class="text-lighter">Owner</span>
                    </th>
                    <th class="text-center">
                      <span class="text-lighter">Driver</span>
                    </th>
                    <th class="text-center text-lighter">
                      <table>
                        <tr>
                          <th colspan="2">Admin Earnings</th>
                        </tr>
                        <tr>
                          <th class="text-center">
                            <div class="text-lighter">Amount</div>
                          </th>
                          <th class="text-center">
                            <div class="text-lighter">Status</div>
                          </th>
                        </tr>
                      </table>
                    </th>
                    <th class="text-center text-lighter">
                      <table>
                        <tr>
                          <th colspan="2">Owner Earnings</th>
                        </tr>
                        <tr>
                          <th class="text-center">
                            <div class="text-lighter">Amount</div>
                          </th>
                          <th class="text-center">
                            <div class="text-lighter">Status</div>
                          </th>
                        </tr>
                      </table>
                    </th>
                    <th class="text-center text-lighter">
                      <table>
                        <tr>
                          <th colspan="2">Driver Earnings</th>
                        </tr>
                        <tr>
                          <th class="text-center">
                            <div class="text-lighter">Amount</div>
                          </th>
                          <th class="text-center">
                            <div class="text-lighter">Status</div>
                          </th>
                        </tr>
                      </table>
                    </th>  
                    <th class="text-center">
                      <span class="text-lighter">Discount</span>
                    </th> 
                    <th class="text-center">
                      <span class="text-lighter">Customer Fare</span>
                    </th> 
                    <th class="text-center">
                      <span class="text-lighter">Total Fare</span>
                    </th>                        
                </tr> 
            </thead>
            <tbody>
              @foreach($bookings as $booking)
                <tr>
                  <td class="text-left"><span class="text-lighter">{{date('d-m-Y', strtotime($booking['datetime']))}}</span></td>
                  <td class="text-center"><span class="text-lighter"><a href="/cms/bookings/{{$booking['unique_id']}}">{{$booking['unique_id']}}</a></span></td>

                  <td class="text-center"><span class="text-lighter"><a href="/cms/users/customers/{{$booking['customer_id']}}"> {{$booking['customer_name']}}</a> ({{$booking['customer_phone']}}) <a href="/cms/users/customers/{{$booking['customer_id']}}/transactions"><i class="icofont icofont-money"></i></a></span></td>

                  <td class="text-center"><span class="text-lighter"><a href="/cms/users/owners/{{$booking['owner_id']}}"> {{$booking['owner_name']}}</a> ({{$booking['owner_phone']}}) <a href="/cms/users/owners/{{$booking['owner_id']}}/transactions"><i class="icofont icofont-money"></i></a></span></td>

                  <td class="text-center"><span class="text-lighter"><a href="/cms/users/drivers/{{$booking['driver_id']}}"> {{$booking['driver_name']}}</a> ({{$booking['driver_phone']}}) <a href="/cms/users/drivers/{{$booking['driver_id']}}/transactions"><i class="icofont icofont-money"></i></a></span></td>

                  <td>
                    <table>
                      <tr>
                        <td class="text-center">
                          <div class="text-lighter">{{$booking['admin_commission'] - $booking['discount']}}</div>
                        </td>
                        <td class="text-center">
                          @if($booking['admin_commission_status']=='paid')
                            <div class="text-success">{{ucfirst($booking['admin_commission_status'])}}</div>
                          @else
                            <div class="text-danger">{{ucfirst($booking['admin_commission_status'])}}</div>
                          @endif
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td>
                    <table>
                      <tr>
                        <td class="text-center">
                          <div class="text-lighter">
                            @if($booking['discount']>0)
                              {{$booking['owner_earning']}}<br>
                              @if($booking['owner_earning']>$booking['customer_cost']) 
                                {{$booking['customer_cost']}}&nbsp;(<span class="text-success">paid</span>)<br>
                                {{$booking['owner_earning']-$booking['customer_cost']}}&nbsp;(<span class="text-danger">unpaid</span>)
                              @else
                                {{$booking['owner_earning']-$booking['discount']}}&nbsp;(<span class="text-success">paid</span>)<br>
                                {{$booking['discount']}}&nbsp;(<span class="text-danger">unpaid</span>)
                              @endif
                            @else
                              {{$booking['owner_earning']}}&nbsp;(<span class="text-success">paid</span>)
                            @endif


                          </div>
                        </td>
                        <td class="text-center">
                          <!-- <div class="text-success">{{ucfirst($booking['owner_earning_status'])}}</div> -->
                          &nbsp;
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td>
                    <table>
                      <tr>
                        <td class="text-center">
                          <div class="text-lighter">{{$booking['driver_commission']}}</div>
                        </td>
                        <td class="text-center">
                          @if($booking['driver_commission_status']=='paid')
                            <div class="text-success">{{ucfirst($booking['driver_commission_status'])}}</div>
                          @else
                            <div class="text-danger">{{ucfirst($booking['driver_commission_status'])}}</div>
                          @endif
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="text-center"><span class="text-lighter"> @if($booking['discount']>0){{$booking['discount']}}@else &nbsp; @endif </span>@if($booking['discount']>0)<span><i class="icofont icofont-sale-discount"></i></span>@endif</td>
                  <td class="text-center"><span class="text-lighter"> {{$booking['customer_cost']}} </span></td>
                  <td class="text-center"><span class="text-lighter"> {{$booking['total_fare']}} </span></td>
                </tr>
              @endforeach
            </tbody>
            <thead>
              <tr>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">{{$admin_commission_total}}</span></th>
                <th class="text-left"><span class="text-lighter">{{$owner_earning_total}}</span></th>
                <th class="text-left"><span class="text-lighter">{{$driver_commission_total}}</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">{{$total_customer_cost}}</span></th>
                <th class="text-left"><span class="text-lighter">{{$total_fare}}</span></th>
              </tr>
            </thead>
        </table>
      </div>
  </div>
</div>