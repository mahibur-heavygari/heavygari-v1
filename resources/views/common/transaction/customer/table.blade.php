<div class="row">
      <div class="col-sm-12 col-md-12">
          <div class="mb-5 transaction-chart">
            {{--<canvas id="transactionChart" width="400" height="200"></canvas>--}}
          </div>
          <div class="table-responsive">
            <table id="transaction-table" class="table table-transaction table-bordered">
                <thead>
                    <tr>
                        <th class="text-left"><span class="text-lighter">Date</span></th>
                        <th class="text-center">
                          <span class="text-lighter">Trip Id</span>
                        </th>

                        <th class="text-center">
                          <span class="text-lighter">Driver</span>
                        </th>
                        
                        <th class="text-center">
                          <span class="text-lighter">Original Fare</span>
                        </th>

                        <th class="text-center">
                          <span class="text-lighter">Discount</span>
                        </th>
                        <th class="text-center">
                          <span class="text-lighter">Fare ( after discount )</span>
                        </th>
                       </tr> 
                </thead>
                <tbody>
                  @foreach($bookings as $booking)
                  <tr>
                    <td class="text-left"><span class="text-lighter">{{date('d-m-Y', strtotime($booking['datetime']))}}</span></td>
                    <td class="text-center"><span class="text-lighter"><a href="/customer/panel/bookings/{{$booking['unique_id']}}">{{$booking['unique_id']}}</a></span></td>

                    <td class="text-center"><span class="text-lighter">{{$booking['driver_name']}} ({{$booking['driver_phone']}})</span></td>
                    
                    <td class="text-center"><span class="text-lighter">{{$booking['total_fare']}}</span></td>
                    <td class="text-center"><span class="text-lighter">{{$booking['discount']}}</span></td>
                    <td class="text-center"><span class="text-lighter">{{$booking['customer_cost']}}</span></td>
                  </tr>
                  @endforeach
                </tbody>
            <thead>
              <tr>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
                <th class="text-center"><span class="text-lighter">Total Discount: {{$total_discount}}</span></th>
                <th class="text-center"><span class="text-lighter">Total Fare: {{$total_customer_cost}}</span></th>
              </tr>
            </thead>
            </table>
          </div>
      </div>
  </div>