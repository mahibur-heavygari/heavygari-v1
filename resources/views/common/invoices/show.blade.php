<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-10 offset-xxl-1">
       <div class="row">
            <div class="col-md-6">
                <div class="mb-4">
                    <strong>{{$invoice->owner->user->name}}</strong><br> {{$invoice->owner->user->address}}
                </div>
            </div>
            <div class="col-md-6">
                <div class="mb-4 text-right">
                    <ul class="list-inline mb-0">
                        <li>
                            <span>Invoice Number:</span>
                            <span>{{$invoice->invoice_number}}</span>
                        </li>
                        <li>
                            <span>Invoice Create Date:</span>
                            <span>{{date('d/m/y', strtotime($invoice->created_at))}}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="table-flex table-responsive">
            <table class="table table-invoice-details">
                <thead>
                    <tr>
                        <th class="text-left">Booking ID</th>
                        <th class="text-center">Cash Collected</th>
                        <th class="text-center">Discount</th>
                        <th class="text-center">Owner's Earning</th>
                        <th class="text-center">Pay to owner</th>
                        <th class="text-center">Receivable</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $payable = 0;
                @endphp
                @foreach($invoice->invoiceItems as $item)
                    <tr>
                        <td class="text-left">
                            <div class="text-nowrap">{{$item->bookings->unique_id}}</div>
                        </td>
                        <td class="text-center">
                            <div class="text-nowrap text-lighter">{{$item->bookings->invoice->total_cost}}</div>
                        </td>  
                        <td class="text-center">
                            <div class="text-nowrap text-lighter">{{$item->bookings->invoice->discount}}</div>
                        </td>                           
                        <td class="text-center">
                            <div class="text-nowrap text-lighter text-sm">{{$item->bookings->earnings->owner_earning}}</div>
                        </td>
                        <td class="text-center">
                            <div class="text-nowrap text-lighter">{{$item->bookings->invoice->discount}}</div>
                        </td>  
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">{{$item->bookings->earnings->heavygari_earning + $item->bookings->earnings->driver_earning}}</div>
                        </td>
                    </tr>
                    @php
                        $payable = $payable + $item->bookings->invoice->discount;
                    @endphp
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-right" colspan="4">
                            <div class="text-nowrap">&nbsp;</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-center"><strong>Tk. {{$payable}}</strong></div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap"><strong>Tk. {{$invoice->total_amount}}</strong></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        @if(isset($adminPanel))
            <div class="btns-flex-inline">
                @if($invoice->status=='due')
                <div class="btn-flex-item">
                     <a href="{{Request::url()}}/send-email" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-envelope-o"></i> Send Email</a>
                </div>

                <div class="btn-flex-item">
                    <div class="text-md-right">
                        <button type="submit" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#myModal">Pay {{$invoice->total_amount}} Taka</button>
                    </div>
                    <form class="" method="POST" action="{{Request::url()}}/mark-as-paid/" >
                        {{ csrf_field() }}
                       <input name="_method" type="hidden" value="PUT">
                       <div id="myModal" class="modal fade" role="dialog">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                     <h4 class="modal-title" style="text-align: left;">Payment</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control form-control-lg datepick-only" type="text" value="" id="ex-field-datetime" name="paid_at" placeholder="Payment Date" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Payment Method</label>
                                        <select class="form-control custom-select" name="payment_method" required="">
                                            <option value="">Select One</option>
                                            <option value="cash">Cash</option>
                                            <option value="bkash">Bkash</option>
                                            <option value="rocket">Rocket</option>
                                            <option value="bank">Bank</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input type="text" name="amount" class="form-control" value="{{$invoice->total_amount}}" readonly="" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Account Number</label>
                                        <input type="text" name="account_number" class="form-control">
                                    </div>
                                    <div class="form-check mb-4 checking-placement">
                                        <label class="form-check-label">
                                            <input id="paid" type="checkbox" class="">
                                            <span class="px-1 text-uppercase letter-spacing-1" style="font-size: 0.689rem !important;">Confirm?</span>
                                        </label>
                                    </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" id="submitButton" disabled="">Submit</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                   </div>
                                     
                                  </div>
                                </div>
                            </div>
                          </div>
                        
                    </form>
                </div>
                @endif
            </div>
        @endif
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#paid').click(function() {
                if ($(this).is(':checked')) {
                    $("#submitButton").prop('disabled', false);
                }if (!$(this).is(':checked')) {
                    $("#submitButton").prop('disabled', true);
                }
            })
        }) 
    </script>
</div>