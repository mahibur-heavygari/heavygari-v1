<div class="row">
    <div class="col-sm-12">
        @if(isset($adminPanel))
            <div class="row">
               <div class="col-md-6">
                  <div class="mb-4">
                      &nbsp;
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-4 text-right">
                        <ul class="list-inline mb-0">
                            <li>
                                <span class="text-lighter"><strong>Total Due:</strong></span>
                                <span class="text-lighter"><strong>Tk {{$total_due or ''}}</strong></span>
                            </li>
                            <li>
                                <span class="text-lighter"><strong>Total Paid:</strong></span>
                                <span class="text-lighter"><strong>Tk {{$total_paid or ''}}</strong></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="invoice-table" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Owner</th>
                        <th>Phone</th>
                        <th>Invoice ID</th>
                        <th>Total Receivable</th>
                        <th>Paid on</th>
                        <th>Payment Method</th>
                        <th>Status</th>
                        @if(isset($adminPanel))
                        <th class="text-right">Actions</th>
                        <th>Note</th>
                        @endif
                    </tr>
                </thead>
                <tbody>                    
                    @foreach($list as $invoice)
                        <tr>
                            <td>
                                <div class="text-nowrap">{{date('d/m/y', strtotime($invoice->created_at))}}</div>
                            </td>
                            <td>
                               <div class="text-nowrap text-lighter text-sm">{{$invoice->owner->user->name}}</div>
                            </td>
                            <td>
                               <div class="text-nowrap text-lighter text-sm">{{$invoice->owner->user->phone}}</div>
                            </td>
                            <td>
                                <a href="{{Request::url()}}/{{$invoice->invoice_number}}" class="text-nowrap">{{$invoice->invoice_number}}</a>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">TK. {{$invoice->total_amount}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                 @if($invoice->paid_at) 
                                    {{$invoice->paid_at}}
                                 @else
                                    N/A
                                 @endif
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                    {{$invoice->payment_method}}                                    
                                </div>
                            </td>
                            <td>
                                <span class="badge @if($invoice->status=='paid') badge-success @else badge-warning @endif">{{$invoice->status}}</span>
                            </td>
                            @if(isset($adminPanel))
                            <td>
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                    <li>
                                        <a href="{{Request::url()}}/{{$invoice->invoice_number}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url(\Storage::url($invoice->pdf))}}" title="Download PDF" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{Request::url()}}/{{$invoice->id}}/send-sms" title="Send SMS" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="icofont icofont-ui-messaging"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <input type="text" name="invoice_note_{{$invoice->id}}" value="{{$invoice->note}}">
                                <button class="save-note btn btn-warning" data-item-id="{{$invoice->id}}">Save</button> <span class="saved_success" style="color:green;
                                display: none;">saved</span>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('footer')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $( "#invoice-table" ).on( "click", ".save-note", function() {
            let itemId = $(this).attr('data-item-id');
            let name = 'invoice_note_'+itemId;
            let note = $("input[name="+name+"]").val();
            let ulr_str = '/cms/invoices/'+itemId+'/save-note';
            var _this = $(this);
            
            $.ajax({
                data: {note: note},
                url: ulr_str,
                type: 'POST',
                complete: function (response) {
                    var result = jQuery.parseJSON(response.responseText);
                    if (result.success) {
                        _this.siblings().show();
                    }
                }
            });
        });
    });
</script>
@stop