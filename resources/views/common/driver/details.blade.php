<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">ব্যাক্তিগত তথ্য</h4>
                </div>
                <div class="wh-col">
                    <div class="wh-meta">
                    </div>
                </div>
            </div>
            <div class="wagon-body">
                <div class="row">

                    <div class="col-md-3 col-lg-3">
                        <div>ছবি</div>
                        <div class="upload-styled-image rounded-circle upload-image-caption" style="width: 120px; height: 120px;">
                            <div class="uploaded-image uploaded-here" style="background-image: url('{{Storage::url($profile->user->thumb_photo)}}');"></div>
                            <a href="{{Storage::url($profile->user->photo)}}" class="eye-view image-popup"><i class="fa fa-eye"></i></a>
                        </div>
                        <div class="rating-site mt-2">
                             <h4 class="dd-title color-primary">
                                    @if($rating <= 0.0 || $rating <= 0)
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 1.0 || $rating ===1)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 2.0 || $rating ===2)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 3.0 || $rating ===3)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 4.0 || $rating ===4)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 5.0 || $rating ===5)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @elseif($rating === 1.5 || $rating > 1.0 && $rating < 1.99 || $rating > 1 && $rating < 1.99 )
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        
                                    @elseif($rating === 2.5 || $rating > 2.0 && $rating < 2.99  || $rating > 2 && $rating < 2.99 )
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 3.5 || $rating > 3.0 && $rating < 3.99 || $rating > 3 && $rating < 3.99 )
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 4.5 || $rating > 4.0 && $rating < 4.99 || $rating > 4 && $rating < 4.99)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    @endif
                                </h4>
                        </div>
                    </div>

                    <div class="col-md-9 col-lg-9">

                        <div class="form-group row">
                            <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নাম</label>
                            <div class="col-md-12 col-lg-8">
                                <input class="form-control form-control-lg like-field" type="text" value="{{$profile->user->name}}" id="name" name="name" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফোন নং</label>
                            <div class="col-md-12 col-lg-8">
                                <input class="form-control form-control-lg like-field" type="text" value="{{$profile->user->phone}}" id="phone" name="phone" readonly>
                            </div>
                        </div>
                        @if(isset($adminPanel)) 
                            <div class="form-group row">
                                <label for="owner_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">মালিকের নাম</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="text" value="{{$profile->myOwner->user->name}}" id="owner_name" name="owner_name" readonly>
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <label for="ref_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেফারেন্স নং</label>
                            <div class="col-md-12 col-lg-8">
                                <input class="form-control form-control-lg like-field" type="text" value="{{$profile->ref_no}}" id="ref_no" name="ref_no" readonly>
                            </div>
                        </div>

                        {{--<div class="form-group row">
                            <label for="email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ইমেইল</label>
                            <div class="col-md-12 col-lg-8">
                                <input class="form-control form-control-lg like-field" type="email" value="{{$profile->user->email}}" id="email" name="email" readonly>
                            </div>
                        </div>  --}}     

                        <div class="form-group row">
                            <label for="national_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">জাতীয় পরিচয় পত্র নং</label>                               
                            <div class="col-md-12 col-lg-8">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <input class="form-control form-control-lg like-field" type="text" value="{{$profile->user->national_id}}" id="national_id" name="national_id" readonly>
                                    </div>
                                    <div class="col-sm-1">ছবি</div>
                                    <div class="col-sm-4">
                                        <div class="tumblr-img tumblr-img-pro">
                                            <img src="{{Storage::url($profile->user->national_id_photo)}}" alt="...">
                                            <a href="{{Storage::url($profile->user->national_id_photo)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ঠিকানা</label>
                            <div class="col-md-12 col-lg-8">
                                <textarea class="form-control form-control-lg like-field" type="text" id="address" name="address" readonly>{{$profile->user->address}}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="about" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নিজ সম্পর্কিত</label>
                            <div class="col-md-12 col-lg-8">
                                <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="about" name="about" readonly>{{$profile->about}}</textarea>
                            </div>
                        </div>                       

                        <div class="form-group row">
                            <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">লাইসেন্স নং</label>
                           
                            <div class="col-md-12 col-lg-8">     
                                <div class="row">
                                    <div class="col-sm-7">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$profile->license_no}}" id="license_no" name="license_no" readonly>
                                    </div>
                                    <div class="col-sm-1">ছবি</div>
                                    <div class="col-sm-4">
                                        <div class="tumblr-img tumblr-img-pro">
                                            <img src="{{Storage::url($profile->license_image)}}" alt="...">
                                            <a href="{{Storage::url($profile->license_image)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>

                        <div class="form-group row">
                            <label for="license_date_of_issue" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">সনদ প্রদানের তারিখ</label>
                            <div class="col-md-12 col-lg-8">
                                <div class="field-only-date has-control-lg">
                                    <input class="form-control form-control-lg like-field datepick-only form-control-danger" type="text" value="{{$profile->license_date_of_issue}}" id="license_date_of_issue" name="license_date_of_issue" readonly>
                                </div>                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license_date_of_expire" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">মেয়াদোত্তীর্ণ তারিখ</label>
                            <div class="col-md-12 col-lg-8">
                                <div class="field-only-date has-control-lg">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$profile->license_date_of_expire}}" id="license_date_of_expire" name="license_date_of_expire" readonly>
                                </div>
                                <div class="form-control-feedback">@if($errors->first('license_date_of_expire')!=null) {{ $errors->first('license_date_of_expire')}} @endif</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license_issuing_authority" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">সনদপদ্রানকারী কর্তৃপক্ষ</label>
                            <div class="col-md-12 col-lg-8">
                                <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$profile->license_issuing_authority}}" id="license_issuing_authority" name="license_issuing_authority" readonly>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ঠিকানা</label>
                            <div class="col-md-12 col-lg-8">
                                <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="address" name="address" readonly>{{$profile->user->address}}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">2. Preferencs</h4>
                </div>
            </div>

            <div class="wagon-body pt-3">
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div>Distance Type</div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="short_distance" class="custom-control-input" id="short_distance" disabled @if($profile->bookingDistancePreference->short_distance) checked @endif>
                                    <label class="custom-control-label" for="short_distance">Short</label>
                                </div>
                            </li>
                            <li class="list-inline-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="long_distance" class="custom-control-input" id="long_distance" disabled @if($profile->bookingDistancePreference->long_distance) checked @endif>
                                    <label class="custom-control-label" for="long_distance">Long</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div>Booking Type</div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="on_demand" class="custom-control-input" id="on_demand" disabled @if($profile->bookingTypePreference->on_demand) checked @endif>
                                    <label class="custom-control-label" for="on_demand">On Demand</label>
                                </div>
                            </li>
                            <li class="list-inline-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="bookingType" class="custom-control-input" id="advance" disabled @if($profile->bookingTypePreference->advance) checked @endif >
                                    <label class="custom-control-label" for="advance">Advance</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div>Booking Categories</div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="full" class="custom-control-input" id="full" disabled @if($profile->bookingCategoryPreference->full) checked @endif>
                                    <label class="custom-control-label" for="full">Full</label>
                                </div>
                            </li>
                            <li class="list-inline-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="shared" class="custom-control-input" id="shared" disabled @if($profile->bookingCategoryPreference->shared) checked @endif>
                                    <label class="custom-control-label" for="shared">Shared</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>