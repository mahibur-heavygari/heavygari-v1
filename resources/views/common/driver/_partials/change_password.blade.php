<div class="wagon wagon-huge wagon-borderd-dashed rounded">
    <div class="wagon-header">
        <div class="wh-col">
            <h4 class="wh-title">2. পাসওয়ার্ড পরিবর্তন করুন</h4>
        </div>
        <div class="wh-col">
            <div class="wh-meta">
            </div>
        </div>
    </div>
    <div class="wagon-body">
        <div class="row">
            <div class="col-md-3 col-lg-3">
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="form-group row @if($errors->first('old_password')!=null) has-danger @endif">
                    <label for="old_password" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">বর্তমান পাসওয়ার্ড</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="password" value="" id="old_password" name="old_password">
                        <div class="form-control-feedback">@if($errors->first('old_password')!=null) {{ $errors->first('old_password')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('password')!=null) has-danger @endif"">
                    <label for="password" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নতুন পাসওয়ার্ড</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="password" value="" id="password" name="password">
                        <div class="form-control-feedback">@if($errors->first('password')!=null) {{ $errors->first('password')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('password_confirmation')!=null) has-danger @endif"">
                    <label for="password_confirmation" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নতুন পাসওয়ার্ড নিশ্চিত করুন</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="password" value="" id="password_confirmation" name="password_confirmation">
                        <div class="form-control-feedback">@if($errors->first('password_confirmation')!=null) {{ $errors->first('password_confirmation')}} @endif</div>
                    </div>
                </div>                                                                        

                <div class="form-group row mb-0">
                    <div class="col-md-12 col-lg-8 offset-lg-4">
                        <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা করুন</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
