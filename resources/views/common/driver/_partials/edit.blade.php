<div class="wagon wagon-huge wagon-borderd-dashed rounded">
    <div class="wagon-header">
        <div class="wh-col">
            <h4 class="wh-title">1. ব্যক্তিগত তথ্য</h4>
        </div>
        <div class="wh-col">
            <div class="wh-meta">
            </div>
        </div>
    </div>
    <div class="wagon-body">
        <div class="row">
           <div class="col-md-3 col-lg-3">
                <div>ছবি</div>
                <div class="upload-styled-image rounded-circle" style="width: 120px; height: 120px;">
                    <div class="uploaded-image uploaded-here" style="background-image: url('{{Storage::url($profile->user->thumb_photo)}}');"></div>
                    <div class="input-file">
                        <input type="file" name="photo" class="file-input">
                        <span class="upload-icon">
                            <i class="icofont icofont-upload-alt"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="form-group row @if($errors->first('name')!=null) has-danger @endif">
                    <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নাম</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('name', $profile->user->name)}}" id="name" name="name">
                        <div class="form-control-feedback">@if($errors->first('name')!=null) {{ $errors->first('name')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('phone')!=null) has-danger @endif"">
                    <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ফোন নং</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('phone', $profile->user->phone)}}" id="phone" name="phone" disabled>
                        <div class="form-control-feedback">@if($errors->first('phone')!=null) {{ $errors->first('phone')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('ref_no')!=null) has-danger @endif">
                    <label for="ref_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">রেফারেন্স নং</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('ref_no', $profile->ref_no)}}" id="ref_no" name="ref_no">
                        <div class="form-control-feedback">@if($errors->first('ref_no')!=null) {{ $errors->first('ref_no')}} @endif</div>
                    </div>
                </div>

                {{--<div class="form-group row @if($errors->first('email')!=null) has-danger @endif"">
                    <label for="ইমেইল" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ইমেইল</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="email" value="{{old('email',$profile->user->email)}}" id="email" name="email">
                        <div class="form-control-feedback">@if($errors->first('email')!=null) {{ $errors->first('email')}} @endif</div>
                    </div>
                </div>--}}
                <div class="form-group row @if($errors->first('national_id')!=null) has-danger @endif"">
                    <label for="national_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">জাতীয় পরিচয় পত্র নং</label>
                   
                    <div class="col-md-12 col-lg-8">     
                        <div class="row">
                            <div class="col-sm-7">
                                <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('national_id',$profile->user->national_id)}}" id="national_id" name="national_id">
                                <div class="form-control-feedback">@if($errors->first('national_id')!=null) {{ $errors->first('national_id')}} @endif</div>
                            </div>
                            <div class="col-sm-1">ছবি</div>
                            <div class="col-sm-4">
                                <div class="upload-styled-image upload-styled-image-40p mb-2">
                                    <div class="uploaded-image uploaded-here" style="background-image: url('{{Storage::url($profile->user->national_id_photo)}}');"></div>
                                    <div class="input-file">
                                        <input class="file-input" type="file" name="national_id_photo" id="national_id_photo">
                                        <span class="upload-icon">
                                            <i class="icofont icofont-upload-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('about')!=null) has-danger @endif"">
                    <label for="about" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">নিজ সম্পর্কিত</label>
                    <div class="col-md-12 col-lg-8">
                        <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="about" name="about">{{old('about',$profile->about)}}</textarea>
                        <div class="form-control-feedback">@if($errors->first('about')!=null) {{ $errors->first('about')}} @endif</div>
                    </div>
                </div>                       

                <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif"">
                    <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">লাইসেন্স নং</label>
                   
                    <div class="col-md-12 col-lg-8">     
                        <div class="row">
                            <div class="col-sm-7">
                                <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('license_no',$profile->license_no)}}" id="license_no" name="license_no">
                                <div class="form-control-feedback">@if($errors->first('license_no')!=null) {{ $errors->first('license_no')}} @endif</div>
                            </div>
                            <div class="col-sm-1">ছবি</div>
                            <div class="col-sm-4">
                                <div class="upload-styled-image upload-styled-image-40p mb-2">
                                    <div class="uploaded-image uploaded-here" style="background-image: url('{{Storage::url($profile->license_image)}}');"></div>
                                    <div class="input-file">
                                        <input class="file-input" type="file" name="license_image" id="license_image">
                                        <span class="upload-icon">
                                            <i class="icofont icofont-upload-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group row @if($errors->first('license_date_of_issue')!=null) has-danger @endif"">
                    <label for="license_date_of_issue" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">সনদ প্রদানের তারিখ</label>
                    <div class="col-md-12 col-lg-8">
                        <div class="field-only-date has-control-lg">
                            <input class="form-control form-control-lg like-field datepick-only form-control-danger" type="text" value="{{old('license_date_of_issue',$profile->license_date_of_issue)}}" id="license_date_of_issue" name="license_date_of_issue">
                        </div>
                        <div class="form-control-feedback">@if($errors->first('license_date_of_issue')!=null) {{ $errors->first('license_date_of_issue')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('license_date_of_expire')!=null) has-danger @endif"">
                    <label for="license_date_of_expire" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">মেয়াদোত্তীর্ণ তারিখ</label>
                    <div class="col-md-12 col-lg-8">
                        <div class="field-only-date has-control-lg">
                            <input class="form-control form-control-lg like-field datepick-only form-control-danger" type="text" value="{{old('license_date_of_expire',$profile->license_date_of_expire)}}" id="license_date_of_expire" name="license_date_of_expire">
                        </div>
                        <div class="form-control-feedback">@if($errors->first('license_date_of_expire')!=null) {{ $errors->first('license_date_of_expire')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('license_issuing_authority')!=null) has-danger @endif"">
                    <label for="license_issuing_authority" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">সনদ প্রদানকারী কর্তৃপক্ষ</label>
                    <div class="col-md-12 col-lg-8">
                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('license_issuing_authority',$profile->license_issuing_authority)}}" id="license_issuing_authority" name="license_issuing_authority">
                        <div class="form-control-feedback">@if($errors->first('license_issuing_authority')!=null) {{ $errors->first('license_issuing_authority')}} @endif</div>
                    </div>
                </div>
                <div class="form-group row @if($errors->first('address')!=null) has-danger @endif"">
                    <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ঠিকানা</label>
                    <div class="col-md-12 col-lg-8">
                        <textarea class="form-control form-control-lg like-field form-control-danger" type="text" id="address" name="address">{{old('address',$profile->user->address)}}</textarea>
                        <div class="form-control-feedback">@if($errors->first('address')!=null) {{ $errors->first('address')}} @endif</div>
                    </div>
                </div> 

                <div class="form-group row @if($errors->first('authorized_vehicles')!=null) has-danger @endif">
                    <label for="ex-cc-limit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">অথরাইজড গাড়ি</label>
                    <div class="col-md-12 col-lg-8">
                        <input id="checkBox" type="checkbox" class="form-group"> Select All
                        <br>
                        @foreach($my_vehicles as $vehicle)
                            <input class="checkbox form-group" type="checkbox" name="authorized_vehicles[]" value="{{$vehicle->id}}"  @if($authorized_vehicles->contains($vehicle->id)) checked @endif>&nbsp; {{$vehicle->number_plate}}
                            <br>
                        @endforeach
                        <div class="form-control-feedback">@if($errors->first('authorized_vehicles')!=null) {{ $errors->first('authorized_vehicles')}} @endif</div>
                    </div>
                </div>                       

                <div class="form-group row mb-0">
                    <div class="col-md-12 col-lg-8 offset-lg-4">
                        <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">জমা করুন</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('footer')
<script>
    $(document).ready(function() {
        $( '#checkBox ' ).click( function () {
            $( 'input[type="checkbox"]' ).prop('checked', this.checked)
        });
    });
</script>
@stop