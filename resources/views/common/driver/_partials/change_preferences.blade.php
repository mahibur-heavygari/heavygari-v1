<div class="wagon wagon-huge wagon-borderd-dashed rounded">
    <div class="wagon-header">
        <div class="wh-col">
            <h4 class="wh-title">2. Preferencs</h4>
        </div>
    </div>

    <div class="wagon-body pt-3">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div>Distance Type</div>
            </div>
            <div class="col-md-9 col-lg-9">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="short_distance" class="custom-control-input" id="short_distance" @if($profile->bookingDistancePreference->short_distance) checked @endif>
                            <label class="custom-control-label" for="short_distance">Short</label>
                        </div>
                    </li>
                    <li class="list-inline-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="long_distance" class="custom-control-input" id="long_distance" @if($profile->bookingDistancePreference->long_distance) checked @endif>
                            <label class="custom-control-label" for="long_distance">Long</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div>Booking Type</div>
            </div>
            <div class="col-md-9 col-lg-9">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="on_demand" class="custom-control-input" id="on_demand" @if($profile->bookingTypePreference->on_demand) checked @endif>
                            <label class="custom-control-label" for="on_demand">On Demand</label>
                        </div>
                    </li>
                    <li class="list-inline-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="advance" class="custom-control-input" id="advance" @if($profile->bookingTypePreference->advance) checked @endif >
                            <label class="custom-control-label" for="advance">Advance</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div>Booking Categories</div>
            </div>
            <div class="col-md-9 col-lg-9">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="full" class="custom-control-input" id="full" @if($profile->bookingCategoryPreference->full) checked @endif>
                            <label class="custom-control-label" for="full">Full</label>
                        </div>
                    </li>
                    <li class="list-inline-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="shared" class="custom-control-input" id="shared" @if($profile->bookingCategoryPreference->shared) checked @endif>
                            <label class="custom-control-label" for="shared">Shared</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-lg-9 ml-auto">
                <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1 mt-3">জমা করুন</button>
            </div>
        </div>
    </div>
</div>