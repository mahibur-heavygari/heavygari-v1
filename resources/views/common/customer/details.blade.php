<div class="row">
    <div class="col-md-12 col-lg-6 col-xxl-6">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-site card-white card-lg mb-4 card-driver-mandatory">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="dm-avater">
                                    <img class="rounded-circle" src="{{Storage::url($profile->user->thumb_photo)}}" alt="...">
                                </div>
                            </div>
                            <div class="pl-4">
                                <h4 class="dd-title color-primary">{{$profile->user->name}}!</h4>
                                <span class="exp-tag">{{$profile->user->about}}</span>
                                <h4 class="dd-title color-primary">
                                    @if($rating <= 0.0 || $rating <= 0)
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 1.0 || $rating ===1)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 2.0 || $rating ===2)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 3.0 || $rating ===3)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 4.0 || $rating ===4)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 5.0 || $rating ===5)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @elseif($rating === 1.5 || $rating > 1.0 && $rating < 1.99 || $rating > 1 && $rating < 1.99 )
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        
                                    @elseif($rating === 2.5 || $rating > 2.0 && $rating < 2.99  || $rating > 2 && $rating < 2.99 )
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 3.5 || $rating > 3.0 && $rating < 3.99 || $rating > 3 && $rating < 3.99 )
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    @elseif($rating === 4.5 || $rating > 4.0 && $rating < 4.99 || $rating > 4 && $rating < 4.99)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    @endif
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="row">
                                    <div class="col-sm-12 driver-description">
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ইমেইল</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->email}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ফোন নং</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->phone}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ঠিকানা</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->address}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">নিজ সম্পর্কিত</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->about}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">জাতীয় পরিচয় পত্র নং</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->national_id}}</div>
                                            </div>
                                        </div>                                        
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">জাতীয় পরিচয় পত্রের ছবি</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="tumblr-img">
                                                    <img src="{{Storage::url($profile->user->national_id_photo)}}" alt="...">
                                                    <a href="{{Storage::url($profile->user->national_id_photo)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ডিভাইস টাইপ</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->device_type}}</div>
                                            </div>
                                        </div>                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-6 col-xxl-6">
        {{--<div class="card card-site card-lg card-white header-border-0 mb-4">
            <div class="card-header">
                <div class="card-header-row">
                    <div class="ch-cell">
                        <h4 class="card-heading-title title-sm color-primary mb-0">সর্বশেষ মন্তব্য</h4>
                    </div>
                    <div class="ch-cell">
                        <a href="" class="ch-ancore">
                            আরও <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <ul class="review-lists">
                <li>
                    <div class="review-item">
                        <div>
                            <a href=""><img src="{{URL::asset('dashboard/images/user.jpg')}}" alt="..."></a>
                        </div>
                        <div>
                            <div class="post-text">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</div>
                            <div class="post-meta">
                                <span>লিখেছেন সন ক্লার্ক</span>
                                <span>২৫ জানুয়ারী, ২০১৬ তারিখে</span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="review-item">
                        <div>
                            <a href=""><img src="{{URL::asset('dashboard/images/user.jpg')}}" alt="..."></a>
                        </div>
                        <div>
                            <div class="post-text">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</div>
                            <div class="post-meta">
                                <span>লিখেছেন সন ক্লার্ক </span>
                                <span>২৫ জানুয়ারী, ২০১৬</span>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="table-default table-stroked  table-thead-stroked table-large table-responsive table-round table-td-vmiddle">
            <div class="table-ex-header">
                <div class="table-ex-header-row">
                    <div class="ex-cell">
                        <div class="ex-title">সর্বশেষ যাত্রা</div>
                    </div>
                    <div class="ex-cell">
                        <a href="" class="ex-ancore">
								আরও <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <table class="table mb-0" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>যাত্রার সময় ও তারিখ</th>
                        <th>যাত্রার আইডি</th>
                        <th>খরচ</th>
                        <th class="text-right">স্ট্যাটাস</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="text-nowrap">২০১৭-০৬-১১ ১৭:৫৫:৪৫</div>
                        </td>
                        <td>
                            <a href="" class="text-nowrap">A226622</a>
                        </td>
                        <td>
                            <div class="text-nowrap">২৩০ টাকা</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap">
                                <span class="badge badge-primary">আসন্ন</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-nowrap">২০১৭-০৬-১১ ১৭:৫৫:৪৫</div>
                        </td>
                        <td>
                            <a href="" class="text-nowrap">A001122</a>
                        </td>
                        <td>
                            <div class="text-nowrap">২৩০ টাকা</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap">
                                <span class="badge badge-info">চলমান</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>--}}
    </div>
</div>