<div class="row">
    <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-site card-white card-lg mb-4 card-driver-mandatory">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="dm-avater">
                                    <img class="rounded-circle" src="{{URL::asset('dashboard/images/user.jpg')}}" alt="...">
                                </div>
                            </div>
                            <div class="pl-4">
                                <h4 class="dd-title color-primary">{{$profile->user->name}}!</h4>
                                <span class="exp-tag">{{$profile->user->about}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="col-sm-12 driver-description">
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ফোন নং</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->phone}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">রেফারেন্স নং</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->ref_no}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ইমেইল</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->email}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ঠিকানা</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->address}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">জাতীয় পরিচয় পত্র নং</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->user->national_id}}</div>
                                            </div>
                                        </div>                                        
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">জাতীয় পরিচয় পত্রের ছবি</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="tumblr-img">
                                                    <img src="{{ Storage::url($profile->user->national_id_photo) }}" alt="...">
                                                    <a href="{{ Storage::url($profile->user->national_id_photo) }}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ম্যানেজারের নাম</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->manager_name}}</div>
                                            </div>
                                        </div>
                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">ম্যানেজারের ফোন</div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="dd-text">{{$profile->manager_phone}}</div>
                                            </div>
                                        </div>  <br>

                                        <div class="row dd-item">
                                            <div class="col-md-4">
                                                <div class="dd-name">গাড়ীসমূহ</div>
                                            </div>   
                                        </div> 
                                        <div class="row dd-item">
                                            <div class="col-md-12">
                                                @foreach($profile->myVehicles as $vehicle)
                                                    <div class="dd-text"> <a href="/cms/vehicles/{{$vehicle->id}}"> {{$vehicle->number_plate or ''}} ( {{$vehicle->vehicleType->title}} )</a></div>
                                                @endforeach
                                            </div>
                                        </div>                                        
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>