@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Complains</h1>
            <div class="title-sub">
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link @if($filter=='ongoing') active  @endif" href="/cms/complain/history_complain?filter=ongoing">Ongoing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='resolved') active @endif" href="/cms/complain/history_complain?filter=resolved">Resolved</a>
        </li>        
    </ul>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>
                        <th class="text-center"><span class="text-lighter">Date & Time</span></th> 
                        <th class="text-center"><span class="text-lighter">Unique ID</span></th>    
                        <th class="text-center"><span class="text-lighter">Complain Text</span></th>
                        <th class="text-center"><span class="text-lighter">User Type</span></th>    
                        <th class="text-center"><span class="text-lighter">User Name</span></th>      
                        <th class="text-center"><span class="text-lighter">User Phone</span></th> 
                        <th class="text-center"><span class="text-lighter">View</span></th>                   
                    </tr>
                </thead>
                <tbody>
                    @foreach($lists as $list)
                    <tr>
                        <td class="text-center">{{$list->created_at}}</td>
                        <td class="text-center">{{$list->unique_id}}</td>
                        <td class="text-center">{{$list->note}}</td>
                        <td class="text-center">{{$list->user_type}}</td>
                        <td class="text-center">{{$list->name}}</td>
                        <td class="text-center">{{$list->phone}}</td>
                        <td class="text-center">
                            <a href="/cms/complain/edit/{{$list->id}}" class="btn btn-red btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">Details</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop