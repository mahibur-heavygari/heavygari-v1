@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">
                {{$editables['0']->unique_id}} </h1>
            <div class="title-sub">
                complain
            </div>
        </div>
    </div>
</div>
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
    }
</style>
@stop
@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Given Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                @foreach($editables as $editable)
                <form class="" method="POST" action="/cms/complain/update/{{$editable->id}}/" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('user_type')!=null) has-danger @endif">
                                <label for="user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">User Type</label>
                                <div class="col-md-12 col-lg-8">
                                    {{$editable->user_type}} 
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_type') !=null ) 
                                            {{ $errors->first('user_type')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('phone_number')!=null) has-danger @endif phone_number_div">
                                <label for="phone_number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                <div class="col-md-12 col-lg-8">
                                   {{$editable->phone}} 
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('user_name')!=null) has-danger @endif user_name_div">
                                <label for="user_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Name</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    {{$editable->name}}
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_name') !=null) 
                                            {{ $errors->first('user_name')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('text')!=null) has-danger @endif">
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Note</label>
                                <div class="col-md-12 col-lg-8" name="text">
                                     {{$editable->note}}
                                     <div class="form-control-feedback">@if($errors->first('text')!=null) {{ $errors->first('text')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('platform')!=null) has-danger @endif">
                                <label for="platform" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Platform Name</label>
                                <div class="col-md-12 col-lg-8" name="platform">
                                    @if($editable->android === "yes")Android,
                                    @endif
                                    @if($editable->ios === "yes")IOS,
                                    @endif
                                    @if($editable->web === "yes")Web,
                                    @endif
                                    @if($editable->others === "yes")Others,
                                    @endif
                                    @if($editable->network === "yes")Network
                                    @endif
                                </div>
                                    <div class="form-control-feedback">@if($errors->first('platform')!=null) {{ $errors->first('platform')}} @endif</div>
                        </div>
                        @if($editable->status !== "resolved")
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button id="submit" type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Resolve</button>
                                </div>
                            </div>
                        @else
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button id="submit" style="display: none;" type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Resolve</button>
                                </div>
                            </div>
                        @endif
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $( "#submit" ).click(function(){
            var delCheck = confirm("Are you sure?");
            if(delCheck)
                return true;
            else
                return false;
        });
    });
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
</script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
@stop