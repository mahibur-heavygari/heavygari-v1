@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Complain</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
    }
</style>
@stop
@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Select Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" action="/cms/complain/save_complain" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('user_type')!=null) has-danger @endif">
                                <label for="user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">User Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="user_type" name="user_type" required="">
                                        <option value="">Select</option>
                                        <option value="customer">Customer</option>
                                        <option value="owner">Owner</option>
                                        <option value="driver">Driver</option>
                                    </select>  
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_type') !=null ) 
                                            {{ $errors->first('user_type')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('phone_number')!=null) has-danger @endif phone_number_div">
                                <label for="phone_number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="phone_number" name="phone_number">
                                    </select>  
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('user_name')!=null) has-danger @endif user_name_div">
                                <label for="user_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Name</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <input class="form-control form-control-lg like-field input-phone" type="text" value="" id="user_name" name="user_name" readonly="">
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_name') !=null) 
                                            {{ $errors->first('user_name')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('text')!=null) has-danger @endif">
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Note</label>
                                <div class="col-md-12 col-lg-8" name="text">
                                    <!-- <div type="text" id="editor" name="text"></div> -->
                                    <textarea class="form-control form-control-lg like-field" rows="6" type="text" id="text" name="text" required=""></textarea> 
                                     <div class="form-control-feedback">@if($errors->first('text')!=null) {{ $errors->first('text')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('platform')!=null) has-danger @endif">
                                <label for="platform" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Platform Name</label>
                                <div class="col-md-12 col-lg-8" name="platform">
                                    <label class="checkbox-line" id="android">
                                    <input type="checkbox" name="android" value="yes" type="text" rows="6">Android App</label><br>
                                    <label class="checkbox-line" id="ios" ><input type="checkbox" name="ios" value="yes" type="text" rows="6">IOS App</label><br>
                                    <label class="checkbox-line" id="web" >
                                    <input type="checkbox" name="web" value="yes" type="text" id="web" rows="6">Web App</label><br>
                                    <label class="checkbox-line" id="others">
                                    <input type="checkbox" name="others" value="yes" type="text" id="others" rows="6">Others</label><br>
                                    <label class="checkbox-line" id="network">
                                    <input type="checkbox" name="network" value="yes" type="text" id="network" rows="6">Network<br><br></label>
                                </div>
                                    <div class="form-control-feedback">@if($errors->first('platform')!=null) {{ $errors->first('platform')}} @endif</div>
                            </div>
                        <input type="hidden" name="phone" value="" id="phone">
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button id="submit" type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="/js/select2.full.min.js"></script>
<script>
    var customers = <?php echo $customers; ?>;
    var owners = <?php echo $owners; ?>;
    var drivers = <?php echo $drivers; ?>;
    $(document).ready(function() {
        $( "#user_type" ).change(function(){
           $("#phone_number").empty();
            $('#phone_number').val('').trigger('change');
            $("#user_name").val('');
            $("#others_phone").val('');
            $("#phone").val('');
            var selected = $('#user_type :selected').val();
            var userArr = [];
            if(selected == 'customer'){
                userArr = customers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".user_type_div").hide();
            }else if(selected == 'owner'){
                userArr = owners;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".user_type_div").hide();      
            }else if(selected == 'driver'){
                userArr = drivers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".user_type_div").hide();
            
            }
            if(selected == 'customer' ){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'driver'){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'owner'){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }
         });

       
        $('#phone_number').select2().on('change', function (e) {
            var user_name = $('#phone_number :selected').val()
            var phone = $('#phone_number :selected').text()
            var email_address = $('#phone_number :selected').attr('elementName');
            if(user_name=='all'){
                $(".email_address_div").hide();
                $(".user_name_div").hide();
                $("#phone").val('');
                $("#email_address").val('all');
                
            }else{
                $(".user_name_div").show();
                $(".email_address_div").show();
                $("#user_name").val(user_name);
                $("#phone").val(phone); 
                $("#email_address").val(email_address); 
                
            }
        });
    });

</script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script>
  var quill = new Quill('#editor', {
    theme: 'snow'
  });
</script>
@stop