@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-body')
    @include('common/invoices/show')
@stop

@section('footer')
     <script type="text/javascript">
        $(document).ready(function() {            
            $('.al-paid-block').slideUp();
            $('#alreadyPaid').change(function(){
                if ($(this).is(':checked')) {
                    $('.al-paid-block').slideDown();
                }else {
                    $('.al-paid-block').slideUp();
                }
            });
        });
    </script>
@stop

