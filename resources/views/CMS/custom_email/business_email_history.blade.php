@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Business Email History<br></h1>
        </div>    
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       
        <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>
                        <th class="text-center"><span class="text-lighter">Date & Time</span></th>    
                        <th class="text-center"><span class="text-lighter">Email Text</span></th>   
                        <th class="text-center"><span class="text-lighter">To</span></th>      
                        <th class="text-center"><span class="text-lighter">User Type</span></th>      
                        <th class="text-center"><span class="text-lighter">Email Type</span></th>  
                        <th class="text-center"><span class="text-lighter">Image</span></th>                   
                    </tr>
                </thead>
                <tbody>   
                    @foreach($all_email as $email)
                    <tr>
                        <td class="text-center">{{ $email->created_at->toDayDateTimeString()}}</td>
                        <td class="text-center">{{ $email->email_text}}</td>
                        <td class="text-center">{{ $email->email or $email->other_email }}</td>
                        <td class="text-center">{{ $email->user_type}}</td>
                        <td class="text-center">{{ $email->email_type}}</td>
                        <td class="text-center">
                        @if($email->image === "0" || $email->image == null)
                            No Image
                        @else
                            <a href="https://www.heavygari.com{{Storage::url($email->image)}}" target="_blank">Click for view</a>
                           <a href="https://www.heavygari.com{{Storage::url($email->image)}}" target="_blank"><img src="https://www.heavygari.com{{Storage::url($email->image)}}" width="100px" height="100px"></a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
               
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop