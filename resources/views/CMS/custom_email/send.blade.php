@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Email</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
    }
</style>
@stop
@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Select Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" action="/cms/send/email" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('user_type')!=null) has-danger @endif">
                                <label for="user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">User Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="user_type" name="user_type" required="">
                                        <option value="">Select</option>
                                        <option value="customer">Customer ( Registered )</option>
                                        <option value="other_customers">Customer ( Non Registered )</option>
                                        <option value="potential_customers">Customer ( Potential )</option>
                                        <option value="owner">Owner ( Registered )</option>
                                        <option value="other_owners">Owner ( Non Registered )</option>
                                        <option value="potential_owners">Owner ( Potential )</option>
                                        <option value="owner_manager">Owner's Manager</option>
                                        <option value="driver">Driver ( Registered )</option>
                                        <option value="other_drivers">Driver ( Non Registered )</option>
                                        <option value="potential_drivers">Driver ( Potential )</option>
                                        <option value="arp">Authorized Registration Point</option>
                                        <option value="not_defined">Other Non Registered</option>
                                    </select>  
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_type') !=null ) 
                                            {{ $errors->first('user_type')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('phone_number')!=null) has-danger @endif phone_number_div">
                                <label for="phone_number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="phone_number" name="phone_number">
                                    </select>  
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('user_name')!=null) has-danger @endif user_name_div">
                                <label for="user_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Name</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <input class="form-control form-control-lg like-field input-phone" type="text" value="" id="user_name" name="user_name">
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_name') !=null) 
                                            {{ $errors->first('user_name')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                                <div class="form-group row @if($errors->first('email_address')!=null) has-danger @endif email_address_div">
                                <label for="email_address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Email Address</label>
                                    <div class="col-md-12 col-lg-8 has-form-control-lg">
                                        <input class="form-control form-control-lg like-field input-phone" type="text" value="" id="email_address" name="email">
                                        <div class="form-control-feedback">
                                            @if($errors->first('email_address') !=null) 
                                                {{ $errors->first('email_address')}} 
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('email_address')!=null) has-danger @endif other_email_address_div">
                                <label for="email_address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Email Address</label>
                                    <div class="col-md-12 col-lg-8 has-form-control-lg">
                                        <Select class="form-control-lg custom-select width-100per" type="text" id="other_email_address" name="other_email">
                                        </Select>
                                        <div class="form-control-feedback">
                                            @if($errors->first('other_email_address') !=null) 
                                                {{ $errors->first('other_email_address')}} 
                                            @endif
                                        </div>
                                    </div>
                                </div>
                             <div class="form-group row @if($errors->first('other_user_type')!=null) has-danger @endif user_type_div">
                                <label for="other_user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Other User Type</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <select class="form-control-lg custom-select width-100per" id="other_user_type" name="other_user_type">
                                        <option value=" ">Select User Type</option>
                                        <option value="customer">Customer</option>
                                        <option value="owner">Owner</option>
                                        <option value="driver">Driver</option>
                                    </select>  
                                    <div class="form-control-feedback">
                                        @if($errors->first('other_user_type') !=null) 
                                            {{ $errors->first('other_user_type')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row email_type_div">
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed ">Email Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per"  name="email_type" required="">
                                        <option value=" ">Select Email Type</option>
                                        <option value="Communication">Communication</option>
                                        <option value="Promotion">Promotion</option>
                                        <option value="Discount">Discount</option>
                                        <option value="Special Offer">Special Offer</option>
                                        <option value="Business">Business</option>
                                    </select>  
                                </div>
                                <div class="form-control-feedback">
                                        @if($errors->first('email_type') !=null) 
                                            {{ $errors->first('email_type')}} 
                                        @endif
                                    </div>
                            </div>
                            <div class="form-group row @if($errors->first('text')!=null) has-danger @endif">
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Text</label>
                                <div class="col-md-12 col-lg-8" name="text">
                                    <!-- <div type="text" id="editor" name="text"></div> -->
                                    <textarea class="form-control form-control-lg like-field" rows="6" type="text" id="text" name="text" required=""></textarea> 
                                     <div class="form-control-feedback">@if($errors->first('text')!=null) {{ $errors->first('text')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('image')!=null) has-danger @endif">     
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Image</label>
                                <div class="col-md-4 col-lg-3">
                                    <div class="upload-styled-image upload-styled-image-40p mb-2">
                                        <div class="uploaded-image uploaded-here"></div>
                                        <div class="input-file">
                                            <input class="file-input" type="file" name="image" id="image" alt="HeavyGari Image">
                                            <span class="upload-icon">
                                                <i class="icofont icofont-upload-alt"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="phone" value="" id="phone">
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button id="submit" type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="/js/select2.full.min.js"></script>
<script>
    var customers = <?php echo $customers; ?>;
    var owners = <?php echo $owners; ?>;
    var owner_managers = <?php echo $owner_managers; ?>;
    var drivers = <?php echo $drivers; ?>;
    var authorized_points = <?php echo $authorized_points; ?>;
    var other_customers = <?php echo $other_customers; ?>;
    var other_owners = <?php echo $other_owners; ?>;
    var other_drivers = <?php echo $other_drivers; ?>;
    var potential_owners = <?php echo $potential_owners; ?>;
    var potential_customers = <?php echo $potential_customers; ?>;
    var potential_drivers = <?php echo $potential_drivers; ?>;
    //console.log(potential_owners);
    $(document).ready(function() {
        $(".others_phone_div").hide();
        $("#other_user_type").hide();
        $(".user_type_div").hide();
        $(".other_email_address_div").hide();
        $( "#user_type" ).change(function(){
           $("#phone_number").empty();
            $('#phone_number').val('').trigger('change');
            $("#user_name").val('');
            $("#others_phone").val('');
            $("#phone").val('');
            var selected = $('#user_type :selected').val();
            var userArr = [];
            //console.log(selected)
            if(selected == 'customer'){
                userArr = customers;
                //console.log(userArr)
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".email_address_div").show();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".email_type_div").show();
                $(".other_email_address_div").hide();

            }else if(selected == 'other_customers'){
                userArr = other_customers;

                //console.log(userArr);
                $(".phone_number_div").hide();
                $(".user_name_div").hide();
                $(".email_address_div").hide();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".other_email_address_div").show();
            }else if(selected == 'owner'){
                userArr = owners;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".email_address_div").show();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".email_type_div").show();
                $(".other_email_address_div").hide();
            }else if(selected == 'other_owners'){
                userArr = other_owners;
                $(".phone_number_div").hide();
                $(".user_name_div").hide();
                $(".email_address_div").hide();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".other_email_address_div").show();
            }else if(selected == 'owner_manager'){
                userArr = owner_managers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".email_address_div").show();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".email_type_div").show();
                $(".other_email_address_div").hide();
            }else if(selected == 'other_owner_manager'){
                //userArr = other_owner_manager;
                $(".phone_number_div").hide();
                $(".user_name_div").hide();
                $(".email_address_div").hide();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".other_email_address_div").show();
            }else if(selected == 'driver'){
                userArr = drivers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".email_address_div").show();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".email_type_div").show();
                $(".other_email_address_div").hide();
            }else if(selected == 'other_drivers'){
                userArr = other_drivers;
                $(".phone_number_div").hide();
                $(".user_name_div").hide();
                $(".email_address_div").hide();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".other_email_address_div").show();
            }else if(selected == 'arp'){
                userArr = authorized_points;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".email_address_div").show();
                $(".others_phone_div").hide();
                $("#other_user_type").hide();
                $(".user_type_div").hide();
                $(".email_type_div").show();
                $(".other_email_address_div").hide();
            }else if(selected == 'not_defined'){
                $("#phone").val('');
                $(".others_phone_div").show();
                $(".email_address_div").show();
                $(".phone_number_div").hide();
                $(".user_name_div").hide();
                $("#other_user_type").show();
                $(".user_type_div").show();
                $(".other_email_address_div").hide();
            }else if(selected == 'potential_owners'){
                userArr = potential_owners;
                $("#phone").val('');
                $(".others_phone_div").hide();
                $(".email_address_div").show();
                $(".phone_number_div").show();
                $(".user_name_div").hide();
                $("#other_user_type").show();
                $(".user_type_div").hide();
                $(".other_email_address_div").hide();
            }else if(selected == 'potential_customers'){
                userArr = potential_customers;
                $("#phone").val('');
                $(".others_phone_div").hide();
                $(".email_address_div").show();
                $(".phone_number_div").show();
                $(".user_name_div").hide();
                $("#other_user_type").show();
                $(".user_type_div").hide();
                $(".other_email_address_div").hide();
            }else if(selected == 'potential_drivers'){
                userArr = potential_drivers;
                $("#phone").val('');
                $(".others_phone_div").hide();
                $(".email_address_div").show();
                $(".phone_number_div").show();
                $(".user_name_div").hide();
                $("#other_user_type").show();
                $(".user_type_div").hide();
                $(".other_email_address_div").hide();
            }
            
            if(selected == 'customer' ){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'driver'){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'owner'){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'owner_manager'){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'arp'){
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                    html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                });
                $("#phone_number").append(html);
            }else if(selected == 'other_drivers'){
                $('#other_email_address option').remove();
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                html = html + "<option value='"+element.email+"'>"+element.email+"</option>";
                });
                $("#other_email_address").append(html);
            }else if(selected == 'other_owners'){
                $('#other_email_address option').remove();
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                html = html + "<option value='"+element.email+"'>"+element.email+"</option>";
                });
                $("#other_email_address").append(html);
            }else if(selected == 'potential_owners'){
                $('#email_address option').remove();
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';

                userArr.forEach(function(element) {

                html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                }
            )
                $("#phone_number").append(html);
            
            }else if(selected == 'potential_customers'){
                $('#email_address option').remove();
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';

                userArr.forEach(function(element) {

                html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                }
            )
                $("#phone_number").append(html);
            
            }else if(selected == 'potential_drivers'){
                $('#email_address option').remove();
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';

                userArr.forEach(function(element) {

                html = html + "<option elementName='"+element.email+"' value='"+element.name+"'>"+element.phone+"</option>";
                }
            )
                $("#phone_number").append(html);
            
            }else{
                $('#other_email_address option').remove();
                var html = '<option value=" "></option><option elementName="all" value="all">All</option>';
                userArr.forEach(function(element) {
                html = html + "<option value='"+element.email+"'>"+element.email+"</option>";
                });
                $("#other_email_address").append(html);
            }
         });

       
        $('#phone_number').select2().on('change', function (e) {
            var user_name = $('#phone_number :selected').val()
            var phone = $('#phone_number :selected').text()
            var email_address = $('#phone_number :selected').attr('elementName');
            if(user_name=='all'){
                $(".email_address_div").hide();
                $(".user_name_div").hide();
                $("#phone").val('');
                $("#email_address").val('all');
                
            }else{
                $(".user_name_div").show();
                $(".email_address_div").show();
                $("#user_name").val(user_name);
                $("#phone").val(phone); 
                $("#email_address").val(email_address); 
                
            }
        });
    });

</script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script>
  var quill = new Quill('#editor', {
    theme: 'snow'
  });
</script>
@stop