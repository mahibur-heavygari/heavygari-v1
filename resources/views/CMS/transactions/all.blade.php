@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')

<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Transaction History<br></h1>
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <button type="button" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodal">Daily </button>
                </li>
                <li>
                    <button type="button" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalweek">Weekly </button>
                </li>
                <li>
                    <button type="button" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalmnth">Monthly</button>
                </li>
                <li>
                    <button type="button" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalyear">Yearly</button>
                </li>
                <li>
                    <a href="/cms/transactions" class="btn btn-success btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update">All</a>
                </li>
            </ul>
        </div>
    </div>
</div>
  
@stop

@section('content-body')
  
  @include('/common/transaction/modal')

  @include('/common/transaction/CMS/table')

@stop