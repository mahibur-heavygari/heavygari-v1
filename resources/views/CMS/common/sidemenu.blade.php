<ul class="aside-nav">


    <li class="aside-nav-item {{ Request::is('cms/dashboard*') ? 'active' : '' }}"><a href="/cms/dashboard"><i class="icofont icofont-dashboard"></i> <span class="nav-text">Dashboard</span> </a></li>

     <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/website*') ? 'active' : '' }}">
        <a href="javascript:void(0)"><i class="fa fa-globe"></i> <span class="nav-text">Website</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/website/features*') ? 'active' : '' }}"><a href="/cms/website/features">Features</a></li>
        </ul>
    </li>

    <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/base*') ? 'active' : '' }}">
        <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="nav-text">Base</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/base/points*') ? 'active' : '' }}"><a href="/cms/base/points">Points</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/trip_categories*') ? 'active' : '' }}"><a href="/cms/base/trip_categories">Trip Categories</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/capacity_types*') ? 'active' : '' }}"><a href="/cms/base/vehicle_types">Vehicle Types</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/capacity_types*') ? 'active' : '' }}"><a href="/cms/base/capacity_types">Capacity Types</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/product_categories*') ? 'active' : '' }}"><a href="/cms/base/product_categories">Product Category</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/weight_categories*') ? 'active' : '' }}"><a href="/cms/base/weight_categories">Weight Category</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/volumetric_weight_categories*') ? 'active' : '' }}"><a href="/cms/base/volumetric_weight_categories">Volumetric Weight Category</a></li>
            <li class="aside-nav-item {{ Request::is('cms/base/settings*') ? 'active' : '' }}"><a href="/cms/base/settings">Settings</a></li>
        </ul>
    </li>

    <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/users*') ? 'active' : '' }} ">
        <a href="javascript:void(0)"><i class="fa fa-users"></i> <span class="nav-text">Users</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/users/customers*') ? 'active' : '' }}"><a href="/cms/users/customers"><span class="nav-text">Customers</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/corporate_customers*') ? 'active' : '' }}"><a href="/cms/corporate_customers"><span class="nav-text">Corporate Customers</span> </a></li>
   
            <li class="aside-nav-item {{ Request::is('cms/users/owners*') ? 'active' : '' }}"><a href="/cms/users/owners"><span class="nav-text">Vehicle Owners</span> </a></li>
           
            <li class="aside-nav-item {{ Request::is('cms/users/drivers*') ? 'active' : '' }}"><a href="/cms/users/drivers"><span class="nav-text">Drivers</span> </a></li>

            <li class="aside-nav-item {{ Request::is('cms/users/potential_owners*') ? 'active' : '' }}"><a href="/cms/users/potential_owners"><span class="nav-text">Potential Owners</span> </a></li> 
            <li class="aside-nav-item {{ Request::is('cms/users/potential_customers*') ? 'active' : '' }}"><a href="/cms/users/potential_customers"><span class="nav-text">Potential Customers</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/users/potential_drivers*') ? 'active' : '' }}"><a href="/cms/users/potential_drivers"><span class="nav-text">Potential Drivers</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/users/field_team') ? 'active' : '' }}"><a href="/cms/users/field_team"><span class="nav-text">Field Teams</span> </a></li>
        </ul>
    </li>    

    <!-- Vehicles -->
    <li class="aside-nav-item {{ Request::is('cms/vehicles*') ? 'active' : '' }}"><a href="/cms/vehicles"><i class="icofont icofont-truck-loaded"></i> <span class="nav-text">Vehicles</span> </a></li>

    <!-- New Booking -->
    <li class="aside-nav-item {{ Request::is('cms/booking/create') ? 'active' : '' }}"><a href="/cms/booking/create"><i class="icofont icofont-calendar"></i> <span class="nav-text">Create Booking</span> </a></li>

    <!-- Bookings -->
    <li class="aside-nav-item {{ Request::is('cms/bookings*') ? 'active' : '' }}"><a href="/cms/bookings"><i class="icofont icofont-calendar"></i> <span class="nav-text">Bookings</span> </a></li>
    <!-- Complain -->
    <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/complains*') ? 'active' : '' }}">
        <a href="javascript:void(0)"><i class="icofont icofont-eye-alt"></i> <span class="nav-text">Complains</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/complain/get_complain') ? 'active' : '' }} "><a href="/cms/complain/get_complain"> <span class="nav-text">Make Complain</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/complain/history_complain') ? 'active' : '' }}"><a href="/cms/complain/history_complain"><span class="nav-text">Complain History</span> </a></li>
        </ul>
    </li> 
    <!-- Owner Invoices -->
    <li class="aside-nav-item {{ Request::is('cms/invoices*') ? 'active' : '' }}"><a href="/cms/invoices"><i class="icofont icofont-money"></i> <span class="nav-text">Owner Invoices</span> </a></li>
    <!-- Owner Invoices -->
    <li class="aside-nav-item {{ Request::is('cms/payment/drivers*') ? 'active' : '' }}"><a href="/cms/payment/drivers"><i class="icofont icofont-pay"></i> <span class="nav-text">Driver Payments</span> </a></li>

    <!-- Transaction History -->
    <li class="aside-nav-item {{ Request::is('cms/transactions*') ? 'active' : '' }}"><a href="/cms/transactions"><i class="fa fa-money"></i> <span class="nav-text">Transactions</span> </a></li>

    <!--Operational Accounts-->
    <li class="aside-nav-item {{ Request::is('cms/operational_accounts*') ? 'active' : '' }}"><a href="/cms/operational_accounts/paid"><i class="icofont icofont-calendar"></i> <span class="nav-text">Operational Accounts</span> </a></li>

    <!-- Price Manager -->
    <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/price-manager*') ? 'active' : '' }}">
        <a href="javascript:void(0)"><i class="icofont icofont-price"></i> <span class="nav-text">Price Manager</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/price-manager/full-booking*') ? 'active' : '' }} "><a href="/cms/price-manager/full-booking"> <span class="nav-text">Full Booking</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/price-manager/shared-booking*') ? 'active' : '' }}"><a href="/cms/price-manager/shared-booking"><span class="nav-text">Shared Booking</span> </a></li>
        </ul>
    </li>   

    <!-- SMS & Notifications -->
    <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/send*') ? 'active' : '' }} ">
        <a href="javascript:void(0)"><i class="icofont icofont-ui-message"></i> <span class="nav-text">SMS, Email & Notification</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/send/sms') ? 'active' : '' }}"><a href="/cms/send/sms"><span class="nav-text">Send SMS</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/sms/history') ? 'active' : '' }}"><a href="/cms/send/sms/history"><span class="nav-text">SMS History</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/email') ? 'active' : '' }}"><a href="/cms/send/email"><span class="nav-text">Send Email</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/email/history') ? 'active' : '' }}"><a href="/cms/send/email/history"><span class="nav-text">Email History</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/business_email/history') ? 'active' : '' }}"><a href="/cms/send/business_email/history"><span class="nav-text">Business Email History</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/popup_notification') ? 'active' : '' }}"><a href="/cms/send/popup_notification"><span class="nav-text">Popup Notification</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/popup_notification/history') ? 'active' : '' }}"><a href="/cms/send/popup_notification/history"><span class="nav-text">Popup Notification History</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/send/push_notification') ? 'active' : '' }}"><a href="/cms/send/push_notification"> <span class="nav-text">Send Notification</span></a></li>
             <li class="aside-nav-item {{ Request::is('cms/send/push_notification/history') ? 'active' : '' }}"><a href="/cms/send/push_notification/history"> <span class="nav-text">Notification History</span> </a></li>
        </ul>
    </li> 

    <!--Feedbacks & Ratings-->
    <li class="aside-nav-item nav-has-dropdown {{ Request::is('cms/feedbacks_and_ratings*') ? 'active' : '' }}">
        <a href="javascript:void(0)"><i class="icofont icofont-ui-rating"></i> <span class="nav-text">Feedbacks & Ratings</span> </a>
        <ul class="aside-nav-sub">
            <li class="aside-nav-item {{ Request::is('cms/feedbacks_and_ratings/user_feedbacks') ? 'active' : '' }}"><a href="/cms/feedbacks_and_ratings/user_feedbacks"><span class="nav-text">User Feedbacks</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/feedbacks_and_ratings/public_feedbacks') ? 'active' : '' }}"><a href="/cms/feedbacks_and_ratings/public_feedbacks"><span class="nav-text">Public Feedbacks</span> </a></li>

            <li class="aside-nav-item {{ Request::is('cms/feedbacks_and_ratings/customer_ratings') ? 'active' : '' }}"><a href="/cms/feedbacks_and_ratings/customer_ratings"><span class="nav-text">Customer Ratings</span> </a></li>
            <li class="aside-nav-item {{ Request::is('cms/feedbacks_and_ratings/driver_ratings') ? 'active' : '' }}"><a href="/cms/feedbacks_and_ratings/driver_ratings"><span class="nav-text">Driver Ratings</span> </a></li>
        </ul>
    </li> 

    <!-- Authorized Points -->
    <li class="aside-nav-item {{ Request::is('cms/authorized_points*') ? 'active' : '' }}"><a href="/cms/authorized_points"><i class="icofont icofont-home-search"></i> <span class="nav-text">Authorized Points</span> </a></li>
</ul>