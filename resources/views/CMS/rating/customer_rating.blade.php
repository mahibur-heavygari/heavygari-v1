@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Customer Ratings<br></h1>
        </div>    
    </div>
</div>
@stop


@section('header')
<link rel="stylesheet" href="/dashboard/plugins/datatable/css/dataTables.bootstrap4.min.css">
@stop


@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       
        <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                  <thead>
                     <tr>
                            <th class="text-center"><span class="text-lighter">Date</span></th>
                            <th class="text-center" >
                                <span class="text-lighter">Booking Id</span>
                            </th>                        
                          
                            <th class="text-center">
                                <span class="text-lighter">Customer Name
                                </span>
                            </th> 
                            <th class="text-center"> <span class="text-lighter">Rating
                                </span></th>   
                            <th class="text-center"> <span class="text-lighter">Driver Review
                                </span></th>                      
                      </tr>
                  </thead>
                <tbody>   
                 @foreach($ratings as $rating)
                         <tr>
                            <td class="text-center">{{ $rating->created_at}}</td>
                            <td class="text-center">{{ $rating->booking->unique_id }}</td>
                            <td class="text-center"> <a href="/cms/users/customers/{{ $rating->customer->id}}">{{ $rating->customer->user->name }}</a></td>


                             <td class="text-center">
                                @if($rating->rating <= 0)
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                @elseif($rating->rating === 1)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                @elseif($rating->rating === 2)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                @elseif($rating->rating === 3)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                @elseif($rating->rating === 4)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                @elseif($rating->rating >= 5)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="text-center">{{ $rating->review}}</td>
                          </tr>  
         
                 @endforeach
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop