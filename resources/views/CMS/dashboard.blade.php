@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Dashboard</h1>
            <div class="title-sub">
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="card-group">
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/cms/bookings?filter=ongoing">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['ongoing']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-fast-delivery"></i>
                        </div>
                        <span class="stat-card-name">Ongoing Bookings</span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/cms/bookings?filter=upcoming">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['upcoming']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-delivery-time"></i>
                        </div>
                        <span class="stat-card-name">Upcoming Bookings</span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/cms/bookings?filter=open">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['open']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-box"></i>
                        </div>
                        <span class="stat-card-name">Open Bookings</span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/cms/bookings?filter=completed">
                    <div class="card-block">
                        <h2 class="stat-card-number">{{$booking_stats['completed']}}</h2>
                        <div class="stat-card-icon">
                            <i class="icofont icofont-vehicle-delivery-van"></i>
                        </div>
                        <span class="stat-card-name">Completed Bookings</span>
                    </div>
                </a>
            </div>
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/cms/bookings?filter=completed"><div class="card-block">
                    <h2 class="stat-card-number">{{$booking_stats['total_distance']}} km</h2>
                    <div class="stat-card-icon">
                        <i class="icofont icofont-truck-loaded"></i>
                    </div>
                    <span class="stat-card-name">Total Kilometers Run</span>
                </div>
            </a>
            </div>            
            <div class="card card-site card-lg card-statistics text-center mb-4 booking-cards">
                <a href="/cms/transactions">
                    <div class="card-block">
                        <h2 class="stat-card-number" style="font-size: 23px;">BDT. {{$payment_stats['total_fare']}}</h2>
                        <!-- <h2 class="stat-card-number" style="font-size: 23px;">BDT. {{$payment_stats['admin_cost']+$payment_stats['owner_revenue']+$payment_stats['driver_earning']}}</h2> -->
                        <div class="stat-card-icon">
                            <i class="icofont icofont-cur-taka-plus"></i>
                        </div>
                        <span class="stat-card-name">Total Revenue</span>

                    </div>
                </a>
            </div>
        </div><!-- 
        <div class="row">
            <div class="col-sm-6">
                <div id="chart_div"></div>
            <br/>
            <div id="btn-group">
              <button class="btn btn-primary" id="none">No Format</button>
              <button class="btn btn-primary" id="scientific">Scientific Notation</button>
              <button class="btn btn-primary" id="decimal">Decimal</button>
              <button class="btn btn-primary" id="short">Short</button>
            </div>
            </div>
        </div> -->
        

        <div class="row">
            <div class="col-sm-12">
                <div class="card-group">
                    <!-- <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <h4 class="card-heading-title title-sm">Vehicles</h4>
                        </div>
                        <ul class="static-lists">
                            <li>
                                <span><a style="color:black;" href="/cms/vehicles"><strong>Total</strong></a></span>
                                <span class="text-center"><strong>{{$total_vehicles}}</strong></span>
                            </li>
                            @foreach($vehicles as $title => $count)
                            <li>
                                <span><a style="color:black;" href="/cms/vehicles?title={{ $title }}">{{ $title }}</a></span>
                                <span class="text-center">{{ $count }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div> -->
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block">
                            <b>Booking Statistics</b>
                            <canvas id="chartTrips" width="400" height="500"></canvas>   
                            <p>Total Completed: {{$chart_booking_stats['total_completed']}}, Total Cancelled: {{$chart_booking_stats['total_cancelled']}}</p>                        
                        </div>
                    </div>
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block">
                            <b>Revenue Statistics</b>
                            <canvas id="chartRevenue" width="400" height="500"></canvas>   
                            <p>Total Revenue: {{$chart_booking_stats['total_revenue']}}</p>                        
                        </div>
                    </div>
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block">
                            <b>Vehicle Statistics</b>
                            <canvas id="chartVehicle" width="400" height="700"></canvas>   
                            <p>Total Vehicles: {{$total_vehicle}}</p>                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-12">
                <div class="card-group">
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block">
                            <div class="chart-vehicle-holder">
                                <div id="line_top_x"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-sm-12 col-md-6">
                <div class="card-group">
                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <b>Top 10 Trip Destinations (In Demand)</b>
                        </div>
                        <ul class="static-lists">
                            @foreach($trip_destination as $trip_destination)
                            <li>
                                <span>{{ $trip_destination->to_point_title }}</span>
                                <span class="text-center">{{ $trip_destination->total }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
<!-- 
                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        
                        <ul class="static-lists"> 
                         
                            <li>
                                <span></span>
                                <span class="text-center"></span>
                            </li>
                        
                        
                        </ul>
                    </div> -->
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="card-group">

                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <b>Top 10 Vehicle Types (In Demand)</b>
                        </div>
                        <ul class="static-lists">
                            @foreach($vehicle_type as $vehicle_type)
                            <li>
                                <span>{{ $vehicle_type->vehicle_title }}</span>
                                <span class="text-center">{{ $vehicle_type->total }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <b>Top 10 Particular Types (In Demand)</b>
                        </div>
                        <ul class="static-lists">
                        @foreach($particulars as $key=>$particular) 
                         
                            <li>
                                <span>{{$key}}</span>
                                <span class="text-center">{{$particular}}</span>
                            </li>
                        
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-12 col-md-6">
                <div class="card-group">
                    <div class="card card-site card-white card-lg header-border-0 mb-4">
                        <div class="card-header mb-2">
                            <h4 class="card-heading-title title-sm">Average summary</h4>
                        </div>
                        <table class="table table-profile-avag">
                            <tbody>
                                <tr>
                                    <td><span class="tp-text">5 Star</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">4 Star</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">3 Star</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">2 Star</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="tp-text">1 Star</span></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card card-site card-lg card-statistics text-center mb-4">
                        <div class="card-block text-center ">
                            <div class="avg-block">
                                <div>
                                    <h2>4.2/5</h2>
                                    <span class="text-lighter">Avarage User profile visited</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>

<script>
     // google.charts.load('current', {'packages':['bar']});
     //  google.charts.setOnLoadCallback(drawChart);

     //  function drawChart() {
     //    var data = google.visualization.arrayToDataTable([
     //      ['Year', 'Sales', 'Expenses', 'Profit'],
     //      ['2014', 1000, 400, 200],
     //      ['2015', 1170, 460, 250],
     //      ['2016', 660, 1120, 300],
     //      ['2017', 1030, 540, 350]
     //    ]);

     //    var options = {
     //      chart: {
     //        title: 'Company Performance',
     //        subtitle: 'Sales, Expenses, and Profit: 2014-2017',
     //      },
     //      bars: 'horizontal', // Required for Material Bar Charts.
     //      hAxis: {format: 'decimal'},
     //      height: 400,
     //      colors: ['#1b9e77', '#d95f02', '#7570b3']
     //    };

     //    var chart = new google.charts.Bar(document.getElementById('chart_div'));

     //    chart.draw(data, google.charts.Bar.convertOptions(options));

     //    var btns = document.getElementById('btn-group');

     //    btns.onclick = function (e) {

     //      if (e.target.tagName === 'BUTTON') {
     //        options.hAxis.format = e.target.id === 'none' ? '' : e.target.id;
     //        chart.draw(data, google.charts.Bar.convertOptions(options));
     //      }
     //    }
     //  }
/*    var vehicles_number = <?php //echo $vehicles_number; ?>;
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable(vehicles_number);
        var options = {
          title: 'Vehicles',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
    };
*/
    google.charts.load('current', {'packages':['line']});
    google.charts.setOnLoadCallback(newdrawChart);

    function newdrawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Day');
      data.addColumn('number', 'Guardians of the Galaxy');
      data.addColumn('number', 'The Avengers');
      data.addColumn('number', 'Transformers: Age of Extinction');

      data.addRows([
        [1,  37.8, 80.8, 41.8],
        [2,  30.9, 69.5, 32.4],
        [3,  25.4,   57, 25.7],
        [4,  11.7, 18.8, 10.5],
        [5,  11.9, 17.6, 10.4],
        [6,   8.8, 13.6,  7.7],
        [7,   7.6, 12.3,  9.6],
        [8,  12.3, 29.2, 10.6],
        [9,  16.9, 42.9, 14.8],
        [10, 12.8, 30.9, 11.6],
        [11,  5.3,  7.9,  4.7],
        [12,  6.6,  8.4,  5.2],
        [13,  4.8,  6.3,  3.6],
        [14,  4.2,  6.2,  3.4]
      ]);

      var options = {
        chart: {
          title: 'Bookings Completed and canceled'
        },
        width: 500,
        height: 500,
        axes: {
          x: {
            0: {side: 'bottom'}
          }
        }
      };

      var chart = new google.charts.Line(document.getElementById('line_top_x'));

      chart.draw(data, google.charts.Line.convertOptions(options));
    }
    

    // $(function() {
    //     var ctx = document.getElementById("chartVehicles");
    //     var myChart = new Chart(ctx, {
    //         type: 'doughnut',
    //         data: {
    //             labels: [<?php //echo '"'.implode('","', $vehicle_titles).'"' ?>],
    //             datasets: [{
    //                 label: 'Vehicles',
    //                 data: [<?php //echo '"'.implode('","', $vehicle_counts).'"' ?>],
    //                 backgroundColor: [
    //                     'rgba(172, 181, 179, 1)',
    //                     'rgba(255, 99, 132, 0.5)',
    //                     'rgba(155, 105, 171, 1)',
    //                     'rgba(54, 162, 235, 0.5)',
    //                     'rgba(6, 182, 255, 0.47)',
    //                     'rgba(7, 38, 0, 0.47)',
    //                     'rgba(255, 105, 171, 1)',
    //                     'rgba(81, 221, 7, 1)',
    //                     'rgba(255, 0, 0, 0.4)',
    //                     'rgba(46, 105, 171, 1)',
    //                     'rgba(255, 206, 86, 0.5)',
    //                     'rgba(255, 0, 0, 0.8)',
    //                     'rgba(11, 156, 49, 0.4)',
    //                     'rgba(255, 100, 8, 0.47)',
    //                     'rgba(54, 0, 255, 1)'
    //                 ]
    //             }]
    //         },
    //         options: {
    //             responsive: true,
    //             legend: {
    //                 position: 'top',
    //             },
    //             animation: {
    //                 animateScale: true,
    //                 animateRotate: true
    //             }
    //         }
    //     });
    // });

    
    $(function() {
        var ctx = document.getElementById("chartTrips");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [<?php echo '"'.implode('","', $chart_booking_stats['stat']['month_year']).'"' ?>],
                datasets: [{
                    label: 'Completed',
                    data: [<?php echo '"'.implode('","', $chart_booking_stats['stat']['completed']).'"' ?>],
                    backgroundColor: [
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)'
                    ]
                },{
                    label: 'Cancelled',
                    data: [<?php echo '"'.implode('","', $chart_booking_stats['stat']['cancelled']).'"' ?>],
                    backgroundColor: [
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                        'rgba(207, 0, 15, 1)',
                    ]
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    });

    $(function() {
        var ctx = document.getElementById("chartRevenue");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [<?php echo '"'.implode('","', $chart_booking_stats['stat']['month_year']).'"' ?>],
                datasets: [{
                    label: 'Revenue',
                    data: [<?php echo '"'.implode('","', $chart_booking_stats['stat']['revenue']).'"' ?>],
                    backgroundColor: [
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)',
                        'rgba(46, 204, 113, 1)'
                    ]
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    });

    $(function() {
        var ctx = document.getElementById("chartVehicle");
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [<?php echo '"'.implode('","', $vehicle_stats['title']).'"' ?>],
                datasets: [{
                    label: 'Revenue',
                    data: [<?php echo '"'.implode('","', $vehicle_stats['number']).'"' ?>],
                    backgroundColor: [
                        'rgba(172, 181, 179, 1)',
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(155, 105, 171, 1)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(6, 182, 255, 0.47)',
                        'rgba(7, 38, 0, 0.47)',
                        'rgba(255, 105, 171, 1)',
                        'rgba(81, 221, 7, 1)',
                        'rgba(255, 0, 0, 0.4)',
                        'rgba(46, 105, 171, 1)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(255, 0, 0, 0.8)',
                        'rgba(11, 156, 49, 0.4)',
                        'rgba(255, 100, 8, 0.47)',
                        'rgba(54, 0, 255, 1)'
                    ]
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });
    });
    
</script>
@stop