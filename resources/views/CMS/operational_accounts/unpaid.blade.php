@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Operational Accounts - Unpaid</h1>
            <div class="title-sub">
                
            </div>
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                  <a data-toggle="tooltip" title="Print" id="unpaid" class="btn btn-primary" target="_blank"> <img src="{{asset('/')}}website/images/print.png" height="25px" width="25px"></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-header-meta" style="margin-top: 32px;">
      <div class="page-header-cell"></div>
      <div class="page-header-cell">
        <ul class="page-header-meta-list">
            <li>
                <button type="button" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalweek">Weekly </button>
            </li>
            <li>
                <button type="button" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalmnth">Monthly</button>
            </li>
            <li>
                <a href="unpaid" class="btn btn-success btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update">All</a>
            </li>
        </ul>
      </div>
    </div>

    <ul class="nav nav-exit">
      <li class="nav-item">
          <a class="nav-link" href="/cms/operational_accounts/paid">Paid</a>
      </li>
      <li class="nav-item">
          <a class="nav-link active" href="/cms/operational_accounts/unpaid">Unpaid</a>
      </li>
    </ul>
</div>
@stop

@section('content-body')
@include('/CMS/operational_accounts/modal')

<div class="row">
  <div class="col-sm-12 col-md-12">
    <div class="row">
       <div class="col-md-6">
          <div class="mb-4">
              &nbsp;
          </div>
        </div>
        <div class="col-md-6">
            <div class="mb-4 text-right">
                <ul class="list-inline mb-0">
                    <li>
                        <span class="text-lighter">Total Unpaid Amount From Owner:</span>
                        <span class="text-lighter">{{round($total_owner_due_amount)}}</span>
                    </li>
                    <li>
                        <span class="text-lighter">Heavygari Unpaid Earnings Amount:</span>
                        <span class="text-lighter">{{round($total_admin_due_amount)}}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
      <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="transaction-table">
        <thead>
          <tr>

            <th class="text-center" >
                <span class="text-lighter">Booking ID</span>
            </th> 

            <th class="text-center"> 
              <span class="text-lighter">Credit</span>
            </th>                      
          
            <th class="text-center">
                <span class="text-lighter">Debit ( Discount )</span>
            </th>                 
          
            <th class="text-center">
                <span class="text-lighter">Debit ( Driver )</span>
            </th>  

            <th class="text-center"> 
              <span class="text-lighter">Balance</span>
            </th>  

          </tr>
        </thead>
        <tbody>   
          @foreach($unpaidAccounts as $data)             
            <tr>
              <td class="text-center"><span class="text-lighter">{{$data['booking_id']}}</span></td>
              <td class="text-center"><span class="text-lighter">{{$data['owner_amount']}}</span></td>
              <td class="text-center"><span class="text-lighter">{{$data['discount']}}</span></td>
              <td class="text-center"><span class="text-lighter">{{$data['driver_amount']}}</span></td>
              <td class="text-center"><span class="text-lighter">{{$data['admin_amount']-$data['discount']}}</span></td>
            </tr>  
          @endforeach  
        </tbody> 
        <thead>
          <tr>
            <th class="text-left"><span class="text-lighter">&nbsp;</span></th>
            <th class="text-center"><span class="text-lighter">Tk. {{round($total_owner_due_amount)}}</span></th>
            <th class="text-center"><span class="text-lighter">Tk. {{round($total_discount)}}</span></th>
            <th class="text-center"><span class="text-lighter">Tk. {{round($total_driver_due_amount)}}</span></th>
            <th class="text-center"><span class="text-lighter">Tk. {{round($total_admin_due_amount)}}</span></th>

          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
@stop
@section('footer')
<script>

  var htmlContent = '<?php echo $print_html; ?>';

  let dropdown = document.getElementById("unpaid");

  dropdown.addEventListener("click", function(event) {
    newwin=window.open('','Operational Accounts Unpaid')
    newwin.document.write('<HTML>\n<HEAD>\n')
    newwin.document.write('<TITLE></TITLE>\n')
    newwin.document.write('<script>\n')
    newwin.document.write('function chkstate(){\n')
    newwin.document.write('if(document.readyState=="complete"){\n')
    newwin.document.write('window.close()\n')
    newwin.document.write('}\n')
    newwin.document.write('else{\n')
    newwin.document.write('setTimeout("chkstate()",2000)\n')
    newwin.document.write('}\n')
    newwin.document.write('}\n')
    newwin.document.write('function print_win(){\n')
    newwin.document.write('window.print();\n')
    newwin.document.write('chkstate();\n')
    newwin.document.write('}\n')
    newwin.document.write('<\/script>\n')
    newwin.document.write('</HEAD>\n')
    newwin.document.write('<BODY onload="print_win()">\n')
    newwin.document.write(htmlContent)
    newwin.document.write('</BODY>\n')
    newwin.document.write('</HTML>\n')
    newwin.document.close()
  });
</script>
@stop