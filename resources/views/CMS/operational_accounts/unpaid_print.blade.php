<html>
    <head>
        <style>
            @media print {
             .name{
               color:  #FAA435;
               padding-top: 10px;

              }
              
            }
            @page:first {
                    .logo { display:block }
                }
        </style>
    </head>
    <body>
       <body style="background-color: #d6d6d5;">
    <table id="" style="width:100%!important;height:100%!important;line-height:100%!important;border-spacing:0;border-collapse:collapse;background-color:#d6d6d5;margin:0;padding:0;border:none" border="0" bgcolor="#d6d6d5" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td class="" style="vertical-align:top" valign="top" align="center">
                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#f8f8f9;border:none" width="100%" border="0" bgcolor="#f8f8f9" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;border:none" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style="height:50px;" class="bg-colors" valign="bottom" bgcolor="#EB374F" style="background-color: #EB374F" align="center">
                                                    <div>
                                                      
                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="10px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 6%; min-width: 15px; margin: 0; padding: 0"></td>
                                                                    <td><img alt="Heavygari logo" src="/dashboard/images/logo.png"  class="logo" style="outline:none;text-decoration:none;display:block" width="120"/></td>
                                                                    <td style="width: 6%; min-width: 15px; margin: 0; padding: 0"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="10px"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="right">
                                                    <table class=" " style="border-spacing:0;border-collapse:collapse;width:100%;max-width:740px;background-color:#fff;border:none" width="100%" border="0" bgcolor="#fff" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table class="" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" align="left" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-right:15px;padding-left:15px">
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="" style="padding-top:20px;padding-bottom:30px">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                            <tr align="left">
                                                                                                                                <td>
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                        <tbody><tr>
                                                                                                                                            <td style="font-size:24px; font-weight: bold;line-height:20px;text-align:left;color:#111;padding-bottom:10px; text-transform: uppercase;text-align: left;">
                                                                                                                                                Operational Accounts Unpaid
                                                                                                                                            </td>
                                                                                                                                            <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding-bottom:10px; text-align: right;" align="right">
                                                                                                                                                Heavygari<br> 01909-222777
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody></table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="20"></td>
                                                                                                                            </tr>
                                                                                                                            <tr align="left">
                                                                                                                                <td>
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                        <tbody><tr>
                                                                                                                                            <td style="font-size:14px;line-height:20px;color:#898989;padding-bottom:10px">
                                                                                                                                                <strong style="color: #111;">
                                                                                                                                            </td>
                                                                                                                                            <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding-bottom:10px">
                                                                                                                                                <table width="100%">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="font-size:14px;line-height:20px;text-align:right;color:#898989;padding-bottom:2px">
                                                                                                                                                                Total Unpaid Amount From Owner:<span>{{round($total_owner_due_amount)}}</span> 
                                                                                                                                                            </td>
                                                                                                                                                            
                                                                                                                                                        </tr>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="font-size:14px;line-height:20px;text-align:right;color:#898989;padding-bottom:2px">Heavygari Unpaid Earnings Amount:<span>{{round($total_admin_due_amount)}}</span></td>
                                                                                                                                                        </tr>
                                                                                                                                                       
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody></table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="50"></td>
                                                                                                                            </tr>
                                                                                                                            <tr align="left">
                                                                                                                                <td>
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                        <thead>
                                                                                                                                            <tr>
                                                                                                                                               
                                                                                                                                                <th width="15%" style="font-size:14px;line-height:20px;text-align:left;color:#111; background: #ebebeb;padding:8px">
                                                                                                                                                    Booking ID
                                                                                                                                                </th>
                                                                                                                                                <th width="25%" style="font-size:14px;line-height:20px;text-align:left;color:#111; background: #ebebeb;padding:8px">
                                                                                                                                                    Credit
                                                                                                                                                </th>
                                                                                                                                                <th width="25%" style="font-size:14px;line-height:20px;text-align:left;color:#111; background: #ebebeb;padding:8px">
                                                                                                                                                    Debit (Discount)
                                                                                                                                                </th>
                                                                                                                                                <th width="25%" style="font-size:14px;line-height:20px;text-align:left;color:#111; background: #ebebeb;padding:8px">
                                                                                                                                                    Debit (Driver)
                                                                                                                                                </th>
                                                                                                                     
                                                                                                                                                <th width="25%" style="font-size:14px;line-height:20px;text-align:right;color:#111;background: #ebebeb;padding:8px">
                                                                                                                                                    Balance
                                                                                                                                                </th>                                                                                                            
                                                                                                                                            </tr>
                                                                                                                                        </thead>
                                                                                                                                        <tbody>
                                                                                                                                        @foreach($unpaidAccounts as $data)  
                                                                                                                                            <tr>
                                                                                                                                                
                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding:8px">
                                                                                                                                              {{$data['booking_id']}}
                                                                                                                                                </td>
                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding:8px">
                                                                                                                                              {{$data['owner_amount']}}
                                                                                                                                                </td>
                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding:8px">
                                                                                                                                              {{$data['discount']}}
                                                                                                                                                </td>
                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding:8px">
                                                                                                                                              {{$data['driver_amount']}}
                                                                                                                                                </td>
                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:right;color:#898989;padding:8px">
                                                                                                                                            {{$data['admin_amount']-$data['discount']}}
                                                                                                                                                </td>
                                                                                                                                                 
                                                                                                                                            </tr> 
                                                                                                                                        @endforeach  
                                                                                                                                        </tbody>
                                                                                                                                        <tfoot>
                                                                                                                                            
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;padding: 8px;">&nbsp;</td>
                                                                                                                                                 <td style="font-size:14px;line-height:20px;text-align:left;color:#898989;">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#111;"><strong>Tk. {{round($total_owner_due_amount)}}</strong></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                                 <td colspan="1" style="font-size:14px;line-height:20px;text-align:left;color:#898989;">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#111;"><strong>Tk. {{round($total_discount)}}</strong></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                                 <td colspan="1" style="font-size:14px;line-height:20px;text-align:left;color:#898989;">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:left;color:#111;"><strong>Tk. {{round($total_driver_due_amount)}}</strong></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                                <td colspan="1" style="font-size:14px;line-height:20px;text-align:left;color:#898989;">
                                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="font-size:14px;line-height:20px;text-align:right;color:#111;"><strong>Tk. {{round($total_admin_due_amount)}}</strong></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tfoot>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="background: #EB374F url() bottom left repeat-x;" align="center">
                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:700px;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding:0 15px">
                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:380px;table-layout:fixed;border:none" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:30px">
                                                                                                                                    <table id="logo" style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                     <img alt="Heavygari logo" src="/dashboard/images/Heavygari-logo-for-mail.png" style="outline:none;text-decoration:none;display:block" width="120"/>
                                                                                                                                               </td>
                                                                                                                                            </tr>
                                                                                                                                            
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <div align="left">
                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:200px;border:none" cellspacing="0" cellpadding="0" border="0" align="right">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td style="padding-top:20px">
                                                                                                                                        <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td style="font-size:16px;line-height:18px;color:#fff;padding-bottom:0px; text-align: right;">
                                                                                                                                                        গ্রাহক সেবা +৮৮০১৯০৯২২২৭৭৭ 
                                                                                                                                                    </td>
                                                                                                                                                    
                                                                                                                                                
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;max-width:100%;table-layout:fixed;border:none" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:30px">
                                                                                                                                    <table style="border-spacing:0;border-collapse:collapse;width:100%;border:none" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                        <tbody>
                                                                                                                                            
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    HeavyGari Technologies Ltd. All Rights Reserved 2018.
                                                                                                                                                    
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    This invoice has been automatically generated for third party convenience. Please read the terms and conditions and privacy policy for further details.

                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:12px;line-height:14px;color:#fff;padding-bottom:8px; text-align: left;">
                                                                                                                                                    
                                                                                                                                                    *conditions apply
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            
                                                                                            <tr>
                                                                                                <td style="padding-top:10px" height="10px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    </body>
</html>