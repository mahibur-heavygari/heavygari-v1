<div class="row transaction_menu">
      <!-- <button type="button" class="btn btn-primary menu-button-update" data-toggle="modal" data-target="#mymodal">Daily </button> -->
      <div class="modal fade" id="mymodal" role="dialog">
          <div class="modal-dialog modal-md modal-placement">
              <div class="modal-content text-center">                
                <form class="" method="GET" enctype="multipart/form-data" action="" >
                  {{ csrf_field() }}
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                      <select required="" name="day">
                          <option value="">Select Day</option>
                          <option value="01">1</option>
                          <option value="02">2</option>
                          <option value="03">3</option>
                          <option value="04">4</option>
                          <option value="05">5</option>
                          <option value="06">6</option>
                          <option value="07">7</option>
                          <option value="08">8</option>
                          <option value="09">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                          <option value="13">13</option>
                          <option value="14">14</option>
                          <option value="15">15</option>
                          <option value="16">16</option>
                          <option value="17">17</option>
                          <option value="18">18</option>
                          <option value="19">19</option>
                          <option value="20">20</option>
                          <option value="21">21</option>
                          <option value="22">22</option>
                          <option value="23">23</option>
                          <option value="24">24</option>
                          <option value="25">25</option>
                          <option value="26">26</option>
                          <option value="27">27</option>
                          <option value="28">28</option>
                          <option value="29">29</option>
                          <option value="30">30</option>
                          <option value="31">31</option>
                      </select>
                      <select required="" name="month">
                          <option value="">Select Month</option>
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                      </select>
                      <select required="" name="year">
                          <option value="">Select Year</option>
                          <option value="2018">2018</option>
                          <option value="2019">2019</option>
                          <option value="2020">2020</option>
                      </select>
                      <input type="hidden" name="modal" value="daily">

                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Search</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </form>
              </div>
          </div>
      </div>

      <div class="modal fade" id="mymodalweek" role="dialog">
          <div class="modal-dialog modal-md modal-placement">
              <div class="modal-content text-center">
                <form class="" method="GET" enctype="multipart/form-data" action="" >
                  {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <select required="" name="week">
                            <option value="">Select Week</option>
                            <option value="1">First</option>
                            <option value="2">Second</option>
                            <option value="3">Third</option>
                            <option value="4">Fourth</option>
                        </select>
                        <select required="" name="month">
                          <option value="">Select Month</option>
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                      </select>
                      <select required="" name="year">
                          <option value="">Select Year</option>
                          <option value="2018">2018</option>
                          <option value="2019">2019</option>
                          <option value="2020">2020</option>
                      </select>
                      <input type="hidden" name="modal" value="weekly">
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Search</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
              </div>
          </div>
      </div>
      
      <div class="modal fade" id="mymodalmnth" role="dialog">
          <div class="modal-dialog modal-sm modal-placement">
              <div class="modal-content text-center">
                <form class="" method="GET" enctype="multipart/form-data" action="" >
                  {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <select required="" name="month">
                            <option value="">Select Month</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select required="" name="year">
                            <option value="">Select Year</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select>
                        <input type="hidden" name="modal" value="monthly">
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Search</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
      <div class="modal fade" id="mymodalyear" role="dialog">
          <div class="modal-dialog modal-sm modal-placement">
              <div class="modal-content text-center">
                <form class="" method="GET" enctype="multipart/form-data" action="" >
                  {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <select required="" name="year">
                            <option value="">Select Year</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select>
                        <input type="hidden" name="modal" value="yearly">
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Search</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
              </div>
          </div>
      </div>
  </div>