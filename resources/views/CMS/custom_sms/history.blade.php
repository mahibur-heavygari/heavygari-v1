@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">SMS History<br></h1>
        </div>    
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       
        <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>
                        <th class="text-center"><span class="text-lighter">Date & Time</span></th>    
                        <th class="text-center"><span class="text-lighter">SMS Text</span></th>   
                        <th class="text-center"><span class="text-lighter">To</span></th>      
                        <th class="text-center"><span class="text-lighter">User Type</span></th>                   
                    </tr>
                </thead>
                <tbody>   
                    @foreach($all_sms as $sms)
                    <tr>
                        <td class="text-center">{{ $sms->created_at->toDayDateTimeString()}}</td>
                        <td class="text-center">{{ $sms->sms_text}}</td>
                        <td class="text-center">{{ $sms->user or '' }}</td>
                        <td class="text-center">{{ $sms->user_type or ''}}</td>
                    </tr>
                    @endforeach
               
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop