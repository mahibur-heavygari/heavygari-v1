@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">SMS</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
    }
</style>
@stop
@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Select Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" action="/cms/send/sms" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('user_type')!=null) has-danger @endif">
                                <label for="user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">User Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="user_type" name="user_type" required="">
                                        <option value="">Select</option>
                                        <option value="customer">Customers ( Registered )</option>
                                        <option value="potential_customer">Customers ( Potential )</option>
                                        <option value="other_customer">Customers ( Not Registered )</option>
                                        <option value="owner">Owners ( Registered )</option>
                                        <option value="potential_owner">Owners ( Potential )</option>
                                        <option value="other_owner">Owners ( Not Registered )</option>
                                        <option value="owner_manager">Owners' Managers</option>
                                        <option value="driver">Drivers ( Registered )</option>
                                        <option value="potential_driver">Drivers ( Potential )</option>
                                        <option value="other_driver">Drivers ( Not Registered )</option>
                                        <option value="arp">Authorized Registration Points</option>
                                        <option value="others">Other ( new )</option>
                                    </select>  
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_type') !=null ) 
                                            {{ $errors->first('user_type')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('phone_number')!=null) has-danger @endif phone_number_div">
                                <label for="phone_number" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="phone_number" name="phone_number">
                                    </select>  
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('user_name')!=null) has-danger @endif user_name_div">
                                <label for="user_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Name</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <input class="form-control form-control-lg like-field input-phone" type="text" value="" id="user_name" name="user_name">
                                    <div class="form-control-feedback">
                                        @if($errors->first('user_name') !=null) 
                                            {{ $errors->first('user_name')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('others_phone')!=null) has-danger @endif others_phone_div">
                                <label for="others_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <input class="form-control form-control-lg like-field input-phone" type="text" value="" id="others_phone" name="others_phone">
                                    <div class="form-control-feedback">
                                        @if($errors->first('others_phone') !=null) 
                                            {{ $errors->first('others_phone')}} 
                                        @endif
                                    </div>
                                </div>

                                <label for="others_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Other Type</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <select class="form-control-lg custom-select width-100per" id="others_type" name="others_type">
                                        <option value="">Select</option>
                                        <option value="customer">Customer type</option>
                                        <option value="owner">Owner type</option>
                                        <option value="driver">Driver type</option>
                                        <option value="unknown">Unknown</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row @if($errors->first('text')!=null) has-danger @endif">
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Text</label>
                                <div class="col-md-12 col-lg-8">
                                    <textarea class="form-control form-control-lg like-field" rows="6" type="text" id="text" name="text"></textarea>
                                    <div class="form-control-feedback">@if($errors->first('text')!=null) {{ $errors->first('text')}} @endif</div>
                                </div>
                            </div>
                            <input type="hidden" name="phone" value="" id="phone">
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="/js/select2.full.min.js"></script>
<script>
    var customers = <?php echo $customers; ?>;
    var owners = <?php echo $owners; ?>;
    var owner_managers = <?php echo $owner_managers; ?>;
    var drivers = <?php echo $drivers; ?>;
    var authorized_points = <?php echo $authorized_points; ?>;
    var potential_owners = <?php echo $potential_owners; ?>;
    var potential_customers = <?php echo $potential_customers; ?>;
    var potential_drivers = <?php echo $potential_drivers; ?>;
    var other_customers = <?php echo $other_customers; ?>;
    var other_owners = <?php echo $other_owners; ?>;
    var other_drivers = <?php echo $other_drivers; ?>;
    $(document).ready(function() {
        $(".others_phone_div").hide();
        $( "#user_type" ).change(function() {
            $("#user_name").prop('disabled', false);

            $("#phone_number").empty();
            $('#phone_number').val('').trigger('change');
            $("#user_name").val('');
            $("#others_phone").val('');
            $("#others_type").val('');
            $("#phone").val('');
            var selected = $('#user_type :selected').val();
            var userArr = [];

            if(selected == 'customer'){
                userArr = customers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'owner'){
                userArr = owners;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'owner_manager'){
                userArr = owner_managers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'driver'){
                userArr = drivers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'arp'){
                userArr = authorized_points;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'potential_owner'){
                userArr = potential_owners;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'potential_customer'){
                userArr = potential_customers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'potential_driver'){
                userArr = potential_drivers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
            }else if(selected == 'other_customer'){
                userArr = other_customers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
                $("#user_name").prop('disabled', true);
            }else if(selected == 'other_owner'){
                userArr = other_owners;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
                $("#user_name").prop('disabled', true);
            }else if(selected == 'other_driver'){
                userArr = other_drivers;
                $(".phone_number_div").show();
                $(".user_name_div").show();
                $(".others_phone_div").hide();
                $("#user_name").prop('disabled', true);
            }else if(selected == 'others'){
                $("#phone").val('');
                $(".others_phone_div").show();
                $(".phone_number_div").hide();
                $(".user_name_div").hide();
            }
            var html = '<option value=""></option><option value="all">All</option>';
            userArr.forEach(function(element) {
                html = html + "<option value='"+element.name+"'>"+element.phone+"</option>";
            });
            $("#phone_number").append(html);
        });

       
        $('#phone_number').select2().on('change', function (e) {
            var user_name = $('#phone_number :selected').val()
            var phone = $('#phone_number :selected').text()
            if(user_name=='all'){
                $(".user_name_div").hide();
                $("#phone").val('');
            }else{
                $(".user_name_div").show();
                $("#user_name").val(user_name);
                $("#phone").val(phone);
            }
        });
    });
    
</script>
@stop