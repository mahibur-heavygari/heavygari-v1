@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">বুকিং </h1>
            <div class="title-sub">
                <span class="fs-18">Id #{{$booking->unique_id}}</span>
            </div>
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link active" href="#">Price Update</a>
        </li>        
    </ul>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">            
            <div class="wagon-body">
                <form class="" method="POST" enctype="multipart/form-data" action="/cms/bookings/{{$booking->unique_id}}/price-update" >
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('total_fare')!=null) has-danger @endif">
                                <label for="total_fare" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Fare</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$booking->invoice->total_fare}}" id="total_fare" name="total_fare" required="">
                                    <div class="form-control-feedback">@if($errors->first('total_fare')!=null) {{ $errors->first('total_fare')}} @endif</div>
                                </div>
                            </div>  
                            <div class="form-group row @if($errors->first('discount')!=null) has-danger @endif">
                                <label for="discount" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Discount</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$booking->invoice->discount}}" id="discount" name="discount">
                                    <div class="form-control-feedback">@if($errors->first('discount')!=null) {{ $errors->first('discount')}} @endif</div>
                                </div>
                            </div> 
                            <div class="form-group row @if($errors->first('total_cost')!=null) has-danger @endif">
                                <label for="total_cost" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Final Fare ( after discount )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$booking->invoice->total_cost}}" id="total_cost" name="total_cost" readonly="" style="background-color: #DCDCDC">
                                    <div class="form-control-feedback">@if($errors->first('total_cost')!=null) {{ $errors->first('total_cost')}} @endif</div>
                                </div>
                            </div>                                                                       

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
  <script>
    $(document).ready(function() {
    
        $("#total_fare").on("change, keyup" ,function(e) 
        {
            let currentTotalFare = $(this).val();
            currentTotalFare = currentTotalFare.replace(/[^0-9.]/g,'');
            $(this).val(currentTotalFare);

            let discount = $("#discount").val();
            let total_cost = parseFloat(currentTotalFare) - parseFloat(discount);

            $("#total_cost").val(total_cost);
        });

        $("#discount").on("change, keyup" ,function(e) 
        {
            let currentDiscount = $(this).val();
            if(currentDiscount=='') {
			    currentDiscount = '0';
			}
            currentDiscount = currentDiscount.replace(/[^0-9.]/g,'');
            $(this).val(currentDiscount);

            let total_fare = $("#total_fare").val();
            let total_cost = parseFloat(total_fare) - parseFloat(currentDiscount);

            $("#total_cost").val(total_cost);
        });
    });   
  </script>
@stop