@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
        border:none !important;
        padding-top:10px;
    }
    .driver-name-li, .driver-vehicle-li{
        background-color: white
    }
    .driver-name-span{
        background-color: white; padding:10px 0px 0px 10px;
    }
    .driver-no-auth-vehicle-span{
        color: red;
    }
</style>
@stop

@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card booking-confrim">
                <div class="booking-confrim-block">
                    <!-- <div class="cost-text-card">
                        <h4 class="">Total Cost : BDT. 9276.75</h4>
                        <h5 class="">My Earning : BDT. 8835.00</h5>
                    </div> -->
                    <div class="form-control-feedback" style="color:red;">
                        @if($errors->first('owner_vehicle')!=null) {{ $errors->first('owner_vehicle')}} @endif</div>
                    <div class="form-control-feedback" style="color:red;">@if($errors->first('owner')!=null) {{ $errors->first('owner')}} @endif</div>
                    <div class="form-control-feedback" style="color:red;">@if($errors->first('hired_driver')!=null) {{ $errors->first('hired_driver')}} @endif</div>
                    <div class="form-control-feedback" style="color:red;">@if($errors->first('hired_driver_phone')!=null) {{ $errors->first('hired_driver_phone')}} @endif</div>
                    <ul class="confrim-list mb-3">
                        <li>
                            <span>বুকিং এর মোট খরচ :</span>
                            <span>টাকা {{$booking->invoice->total_cost}}</span>
                        </li>
                        <li>
                            <span>গাড়ির মালিকের আয়:</span>
                            <span>টাকা {{$booking->earnings->owner_earning}}</span>
                        </li>
                        <li>
                            <span>গাড়ির চালকের আয়:</span>
                            <span>টাকা {{$booking->earnings->driver_earning}}</span>
                        </li>
                    </ul>
                    <form method="POST">
                        {{ csrf_field() }}
                        <div class="form-group field-join">
                            <div id="existing_driver">
                                <div class="input-group input-flex input-flex-on-mobile @if($errors->first('driver')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">চালকের ফোন</span>
                                    <select name="driver" class="custom-select" id="driver-select">
                                        <option value="" selected >--অনুগ্রহ পূর্বক চালকের ফোন নির্বাচন করুন--</option>
                                        @foreach($drivers as $driver)

                                            <option driver_mobile="{{$driver->user->phone }}" value="{{ $driver->user->name }}" ownername="{{$driver->myOwner->user->name }}" ownerphone="{{$driver->myOwner->user->phone }}" phone="{{$driver->user->phone }}" @if(isset($driver->currentVehicle->name) ) isred="{{$driver->currentVehicle->number_plate}} ( {{$driver->currentVehicle->name}})-({{$driver->currentVehicle->vehicleType->title}})" @else isred="" @endif >{{ $driver->user->phone }}</option>
                                        
                                        @endforeach                                    
                                    </select>
                                    <input type="hidden" id="driver_phone" name="driver_phone">
                                </div>

                                <ul class="confrim-list mb-3">
                                    <li class="driver-name-li">
                                        <span>চালকের নাম</span>
                                        <span class="driver-name-span">
                                            
                                        </span>
                                    </li>
                                    <li class="driver-vehicle-li" id="driver_vehicle">
                                        <span>চালকের গাড়ি</span>
                                        <span class="driver-vehicle-span"></span>
                                    </li>
                                    <li class="driver-vehicle-li" id="driver_auth_vehicle">
                                        <span>চালকের গাড়ি</span>
                                        <select id="driver_auth_vehicle_dropdown">
                                            <option value="">গাড়ি নির্বাচন করুন</option>
                                            <option></option>
                                        </select>
                                        <span class="driver-auth_no-vehicle-span  driver-no-auth-vehicle-span"></span>
                                        
                                    </li>
                                    <li class="owner-vehicle-li">
                                        <span>মালিকের নাম</span>
                                        <span class="owner-name-span"></span>
                                    </li>
                                    <li class="owner-vehicle-li">
                                        <span>মালিকের ফোন</span>
                                        <span class="owner-phone-span"></span>
                                    </li>
                                </ul>
                            </div>

                            <div id="hired_driver">
                                <div class="input-group input-flex @if($errors->first('owner')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">মালিকের ফোন</span>
                                    <select name="owner" class="custom-select" id="owner-select"  style="width:72%">
                                        <option value="" selected >--অনুগ্রহ পূর্বক মালিকের ফোন নির্বাচন করুন--</option>
                                        @foreach($owners as $owner)
                                            <option value="{{ $owner->id }}" vehicles="{{$owner->myVehicles }}">{{ $owner->user->phone }}</option>
                                        @endforeach                                    
                                    </select>
                                </div>
                                <div class="input-group input-flex @if($errors->first('owner_vehicle')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">গাড়ি</span>
                                    <select name="owner_vehicle" id="owner_vehicle" class="custom-select" style="width:72%">
                                                                           
                                    </select>
                                </div>
                                <div class="input-group input-flex @if($errors->first('hired_driver')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">চালক এর নাম</span>
                                    <input class="form-control form-control-lg like-field" type="text" value="{{old('hired_driver')}}" id="hired_driver" name="hired_driver" autocomplete="off">
                                </div>

                                <div class="input-group input-flex input-flex-on-mobile @if($errors->first('hired_driver_phone')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">চালক এর ফোন</span>
                                    <input class="form-control form-control-lg like-field" type="text" value="{{old('hired_driver_phone')}}" id="hired_driver_phone" name="hired_driver_phone" autocomplete="off">
                                </div>
                                <input type="hidden" value="" id="is_hired" name="is_hired">
                            </div><br>

                            <div id="short_vehicle">
                                <div class="input-group input-flex @if($errors->first('owner_vehicle')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">গাড়ির টাইপ</span>
                                    <input class="form-control form-control-lg like-field" type="text" value="{{$booking->fullBookingDetails->vehicleType->title}}" readonly="">
                                </div>
                                <div class="input-group input-flex @if($errors->first('hired_driver')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">গাড়ির নাম</span>
                                    <input class="form-control form-control-lg like-field" type="text" value="" id="vehicle_name" name="vehicle_name" autocomplete="off">
                                </div>
                                <div class="input-group input-flex @if($errors->first('hired_driver')!=null) has-danger @endif">
                                    <span class="input-group-addon" id="addon-from">গাড়ির নাম্বার প্লেট</span>
                                    <input class="form-control form-control-lg like-field" type="text" value="" id="number_plate" name="number_plate" autocomplete="off">
                                </div>
                                <input type="hidden" value="" id="is_short_vehicle" name="is_short_vehicle">
                            </div>
                            <div class="input-group input-flex" id="hired_driver_dive">
                                <a class="new-driver" href="javascript:void(0)">Add a hired driver</a>
                            </div>
                            <br>

                            <div class="input-group input-flex" id="short_vehicle_div">
                                <a class="short_vehicle_click" href="javascript:void(0)">Add a short vehicle</a>
                            </div>
                            
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary text-uppercase fs-14 font-weight-semibold letter-spacing-1 px-4 submit-button">Accept Job</button>
                        </div>
                        <input type="hidden" id="driver_selected_vehicle" name="driver_selected_vehicle" value="">
                    </form>
                </div>                        
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div class="map-block map-booking">
                <iframe src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking->trips[0]->from_lat}},{{$booking->trips[0]->from_lon}}&destination={{$booking->trips[0]->to_lat}},{{$booking->trips[0]->to_lon}}" width="800" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<div class="map-block-overlay"></div>-->
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
 <script src="/js/select2.full.min.js"></script>
 <script>
   $('#driver-select').change(function(){
    $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} });
        var driver_mobile = $(this).find(':selected').attr('driver_mobile')
        $.post('/cms/authorized-vehicles', {driver_mobile:driver_mobile}, function( response ){
            if(response.success === true){
                var no_auth_driver = response.data['0'];
                var len = response.data.length;
                $("#driver_auth_vehicle_dropdown").empty();
                $("#driver_auth_vehicle_dropdown").append("<option value=''></option>");
                for( var i = 0; i<len; i++){
                    var id = response.data[i]['id'];
                    var name = response.data[i]['name'];
                    var number_plate = response.data[i]['number_plate'];
                    //var vehicle_type = response.vehicle_type;
                    //console.log(vehicle_type); 
                     $("#driver_auth_vehicle_dropdown").append("<option value='"+id+"'>"+number_plate+"("+name+")"+"</option>");
                        var abc = $("#driver_auth_vehicle_dropdown").val();
                        $('#driver_auth_vehicle_dropdown').change(function(){
                            var values = $(this).children("option:selected").attr('value');
                           var something = $("#driver_selected_vehicle").val(values);
                        }); 
                 }
                  var vehicle = $('#driver-select').find(':selected').attr('isred');
                    if(vehicle != ''){
                    $(".submit-button").removeAttr("disabled");   
                    $('.driver-no-vehicle-span').hide();
                    $('#driver_auth_vehicle').hide();
                    $('.driver-vehicle-span').show();
                    $('#driver_auth_vehicle').hide();
                    $('#driver_vehicle').show();
                    $('.driver-vehicle-span').text(vehicle);
                }else{
                    if( no_auth_driver === undefined){
                        $(".submit-button").attr("disabled", "disabled");
                        $('.driver-vehicle-span').hide();
                        $('#driver_vehicle').hide();
                        $('#driver_auth_vehicle').show();
                        $('#driver_auth_vehicle_dropdown').hide();
                        $('.driver-auth_no-vehicle-span').show();
                        $('.driver-auth_no-vehicle-span').text('চালকের কোন গাড়ি নেই');
                    }
                    else{
                        $(".submit-button").removeAttr("disabled");
                        $('.driver-vehicle-span').hide();
                        $('#driver_vehicle').hide();
                        $('#driver_auth_vehicle').show();
                        $('#driver_auth_vehicle_dropdown').show();
                        $('.driver-auth_no-vehicle-span').hide();
                        
                    }
                }
            }
        });
    });
    $(document).ready(function() {
        $('#driver_auth_vehicle').hide();
        $(".submit-button").attr("disabled", "disabled");
        $('#driver-select').select2().on('change', function (e) {

            $('.driver-name-span').text(this.value);
            $('.driver-vehicle-span').text('');

            var phone = $(this).find(':selected').attr('phone');
            $('#driver_phone').val(phone);
            if(this.value == ''){
                var ownername = '';
                var ownerphone = '';
            }else{
                var ownername = $(this).find(':selected').attr('ownername');
                var ownerphone = $(this).find(':selected').attr('ownerphone');
            }
            $('.owner-name-span').text(ownername);
            $('.owner-phone-span').text(ownerphone);

        });

        $("#hired_driver").hide();
        $(".new-driver").click(function() {
            $("#existing_driver").hide();
            $("#hired_driver").show();
            $(".submit-button").removeAttr("disabled"); 
            $("#is_hired").val('true');
            $("#hired_driver_dive").hide();
        });

        $('#owner-select').select2().on('change', function (e) {
            $("#owner_vehicle").empty();
            var vehicles = $(this).find(':selected').attr('vehicles');
            var parsed = JSON.parse(vehicles);
            var html = '<option value=""><option>';
            var option = '';
            parsed.forEach(function(element) {
                html = html + "<option value='"+element.id+"'>"+element.number_plate+" ( "+element.name+" ) "+"</option>";
            });
            $("#owner_vehicle").append(html);
            var owner = this.value;
        });

        $("#short_vehicle").hide();
        $(".short_vehicle_click").click(function() {
            $("#short_vehicle").show();
            $(".submit-button").removeAttr("disabled"); 
            $("#is_short_vehicle").val('true');
            $("#short_vehicle_div").hide();
        });

        var hired_driver = $("#hired_driver").value;
        
    });
</script>
@stop