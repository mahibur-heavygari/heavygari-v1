@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
        border:none !important;
        padding-top:10px;
    }
</style>
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">বুকিং নিশ্চিত করুন</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card">
                <form method="POST" action="/cms/booking/confirm" enctype="multipart/form-data" onsubmit="return validateForm()">
                    
                    {{ csrf_field() }}

                    <ul class="confrim-list">
                    
                        <li>
                            <span>গাড়ীর ধরণ :</span>
                            <span>{{$vehicle_type->title}}</span>
                            <input name="vehicle_type" type="hidden" value="{{$booking_fields['vehicle_type']}}">
                        </li>

                        <li>
                            <span>ধারণক্ষমতা:</span>
                            <span>{{$booking_fields['capacity']}} {{$vehicle_type->capacityType->title}}</span>
                            <input name="capacity" type="hidden" value="{{$booking_fields['capacity']}}">
                        </li>

                        <li>
                            <span>পিক-আপের ঠিকানা:</span>
                            <span>
                                {{$booking_fields['from_address']}}
                            </span>
                            <input name="from_point" type="hidden" value="{{$booking_fields['from_point']}}">
                            <input name="from_address" type="hidden" value="{{$booking_fields['from_address']}}">     
                            <input name="from_lat" type="hidden" value="{{$booking_fields['from_lat']}}">
                            <input name="from_lon" type="hidden" value="{{$booking_fields['from_lon']}}">                       
                        </li>
                        
                        <li>
                            <span>গন্তব্যের ঠিকানা:</span>
                            <span>
                                {{$booking_fields['to_address']}}
                            </span>
                            <input name="to_point" type="hidden" value="{{$booking_fields['to_point']}}">
                            <input name="to_address" type="hidden" value="{{$booking_fields['to_address']}}">
                            <input name="to_lat" type="hidden" value="{{$booking_fields['to_lat']}}">
                            <input name="to_lon" type="hidden" value="{{$booking_fields['to_lon']}}">
                        </li>
                        
                        <li>
                            <span>দূরত্ব:</span>
                            <span>
                                {{$route['distance']}} km
                            </span>
                        </li>

                        <li>
                            <span>যাত্রার ধরণ:</span>
                            <span>
                               {{$booking_fields['trip_type']}}
                            </span>
                             <input name="trip_type" type="hidden" value="{{$booking_fields['trip_type']}}">
                        </li>

                        <li>
                            <span>বুকিং এর সময়:</span>
                            <span>{{$booking_fields['booking_type']}}</span>
                            <input name="booking_type" type="hidden" value="{{$booking_fields['booking_type']}}">
                        </li>
                        
                        @if($booking_fields['booking_type']=='advance')
                        <li>
                            <span>তারিখ ও সময়:</span>
                            <span>{{$booking_fields['date_time']}}</span>
                            <input name="date_time" type="hidden" value="{{$booking_fields['date_time']}}">
                        </li>
                        @endif

                    </ul>

                    <hr />

                    @if($fare_breakdown['fare_breakdown']['discount']>0)
                        <h2 class="your-cost text-center">আপনার খরচঃ <strike>{{$fare_breakdown['total_cost']+$fare_breakdown['fare_breakdown']['discount']}}</strike>&nbsp;{{$fare_breakdown['total_cost']}} টাকা &nbsp;<span class="badge badge-success home-discount-pirce">{{$fare_breakdown['fare_breakdown']['discount']}} টাকা ডিসকাউন্ট </span></h2>
                    @else
                        <h2 class="your-cost text-center">আপনার খরচঃ   টাকা. {{$fare_breakdown['total_cost']}}</h2>
                    @endif

                    <h5 class="bfc-title mt-5">কাস্টোমার নির্বাচন করুন</h5>
                    <div class="form-group existing-customer">
                        <div class="input-group input-flex input-flex-on-mobile @if($errors->first('customer')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-from">কাস্টোমারের ফোন</span>
                            <select name="customer" class="custom-select" id="customer-select">
                                <option value="" selected >--অনুগ্রহ পূর্বক কাস্টোমারের ফোন নির্বাচন করুন--</option>
                                @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}" customername="{{$customer->user->name }}" customerphone="{{$customer->user->phone }}">{{ $customer->user->phone }}</option>
                                @endforeach                                    
                            </select>
                            <div class="form-control-feedback">{{$errors->first('[customer]')}}</div>
                        </div>
                    </div> 

                    <div id="short_customer_div">
                        <div class="input-group input-flex @if($errors->first('short_customer_name')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-from">কাস্টোমারের নাম</span>
                            <input class="form-control form-control-lg like-field" type="text" value="{{old('short_customer_name')}}" id="short_customer_name" name="short_customer_name" autocomplete="off">
                        </div>

                        <div class="input-group input-flex input-flex-on-mobile @if($errors->first('short_customer_phone')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-from">কাস্টোমারের ফোন</span>
                            <input class="form-control form-control-lg like-field" type="text" value="{{old('short_customer_phone')}}" id="short_customer_phone" name="short_customer_phone" autocomplete="off">
                        </div>
                        <input type="hidden" value="" id="is_short_customer" name="is_short_customer">
                    </div>

                    <div class="input-group input-flex" id="short_customer_click_div">
                        <a class="short-customer-click" href="javascript:void(0)">Add a short customer</a>
                    </div>

                    <h5 class="bfc-title mt-5">পারিশ্রমিকের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('[payment_by]')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">পারিশ্রমিকদাতা</span>
                            <select id="payment_by" name="payment_by" class="custom-select" aria-describedby="addon-booking-type">                                
                                <option @if(old('payment_by')=='recipient') selected @endif value="recipient">প্রাপক</option>
                                <option @if(old('payment_by')=='customer') selected @endif value="customer">কাস্টোমার নিজে</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('[payment_by]')}}</div>
                    </div>                    

                    <h5 class="bfc-title mt-5">প্রাপকের বিবরণ</h5>
                    <div class="form-group">
                        <input id="self_recipient" type="checkbox"> কাস্টোমার নিজে?
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('recipient_name')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">নাম*</span>
                            <input id="recipient_name" name="recipient_name" type="text" class="form-control" aria-describedby="addon-name" required="">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('recipient_name')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('recipient_phone')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-phone">ফোন নং*</span>
                            <input id="recipient_phone" name="recipient_phone" type="text" class="form-control" aria-describedby="addon-phone" required="">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('recipient_phone')}}</div>
                    </div>

                    <h5 class="bfc-title mt-5">পণ্যের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('particular_details')!=null) has-danger @endif">
                            <span class="input-group-addon">পণ্য এর ক্যাটাগরি</span>                           
                           <select id="particular_details" name="particular_details" class="custom-select" required="">    
                                <option value=""></option>
                                @foreach($product_categories as $category)
                                
                                    <option value="{{$category->title}}">{{$category->title}}</option>
                                @endforeach
                            </select>
                            @foreach($product_categories as $category)
                            @if($category['category'] == 'ride_type')
                            <input name="category_type" type="hidden" value="ride_type">
                            @elseif($category['category'] == 'transport_type')
                            <input name="category_type" type="hidden" value="transport_type">
                            @else
                            <input name="category_type" type="hidden" value=" ">
                            @endif
                            @endforeach
                        </div>
                      <div class="form-control-feedback">{{$errors->first('particular_details')}}
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon">অপেক্ষার সময় (প্রায়)</span>
                            <select id="waiting_time" name="waiting_time" class="custom-select">    
                                <option value=""></option>   
                                <option value="1 hour">1 hour</option>
                                <option value="2 hours">2 hours</option>
                                <option value="3 hours">3 hours</option>
                                <option value="4 hours">4 hours</option>
                                <option value="5 hours">5 hours</option>
                                <option value="6 hours">6 hours</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('waiting_time')}}</div>
                    </div>  
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-description">বিশেষ নির্দেশনা</span>
                            <textarea id="special_instruction" name="special_instruction" rows="3" class="form-control" aria-describedby="addon-description"></textarea>
                        </div>
                    </div>  
                    
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon">ডেলিভারি অর্ডার ছবি</span>                           
                            <div class="col-md-12 col-lg-8">     
                                <div class="">
                                    <div class="col-sm-5">
                                        <div class="upload-styled-image upload-styled-image-40p mb-2">
                                            <div class="uploaded-image uploaded-here" style=""></div>
                                            <div class="input-file">
                                                <input class="file-input" type="file" name="delivery_order" id="delivery_order">
                                                <span class="upload-icon">
                                                    <i class="icofont icofont-upload-alt"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <span>এখানে ক্লিক করুন</span>
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                    </div>                   
                    
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary book-submit btn-block">বুক করুন</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div class="map-block map-booking">
                <iframe src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking_fields['from_lat']}},{{$booking_fields['from_lon']}}&destination={{$booking_fields['to_lat']}},{{$booking_fields['to_lon']}}" width="800" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<div class="map-block-overlay"></div>-->
            </div>
        </div>
    </div>
    
    <!--<ul class="confrim-list">
        <pre /> {{ print_r($fare_breakdown) }} </pre>
    </ul>-->
    
</div>
@stop

@section('footer')
<script src="/js/select2.full.min.js"></script>
<script type = "text/javascript" > 
    var customer_name = "";
    var customer_phone = "";

    $(document).ready(function() {
        $('#customer-select').select2().on('change', function (e) {
            if(this.value == ''){
                var customername = '';
                var customerphone = '';
            }else{
                var customername = $(this).find(':selected').attr('customername');
                var customerphone = $(this).find(':selected').attr('customerphone');
            }

            customer_name = customername;
            customer_phone = customerphone;   

            $("#recipient_name").val("");
            $("#recipient_phone").val("");     
        });

        function validateForm() {
            var recipient_name = document.getElementById("recipient_name").value;
            var recipient_phone = document.getElementById("recipient_phone").value;
            if (recipient_name == "" || recipient_phone == "") {
                alert("Please provide Recipient's Name & Phone");
                return false;
            }
        }

        $("#short_customer_div").hide();

        $(".short-customer-click").click(function() {
            $(".existing-customer").hide();
            $("#short_customer_div").show();
            $(".short-customer-click").hide();
            $("#is_short_customer").val('true');

            $("#customer-select").val('');
            customer_name = "";
            customer_phone = "";  
            $("#recipient_name").val("");
            $("#recipient_phone").val("");

        });


        $('#self_recipient').on('click', function() {
            if($("#is_short_customer").val()=='true'){
                //console.log("adfsd")
                customer_name = $("#short_customer_name").val();
                customer_phone = $("#short_customer_phone").val();
            }
            $("#recipient_name").val(customer_name);
            $("#recipient_phone").val(customer_phone);
        });
    });
</script>
@stop