@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Edit Booking ( {{$booking->unique_id}} )</h1>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="banner-negative-top-right">
    <div class="row">
        <div class="col-md-6 col-xl-5">
            <div class="booking-form-card">
                <form method="POST" action="/cms/bookings/{{$booking->unique_id}}/edit/confirm"  onsubmit="return validateForm()">
                    
                    {{ csrf_field() }}

                    <ul class="confrim-list">
                    
                        <li>
                            <span>Customer Name</span>
                            <span>{{$customer['name']}}</span>
                        </li>
                        <li>
                            <span>Customer Phone</span>
                            <span>{{$customer['phone']}}</span>
                        </li>
                        <li>
                            <span>গাড়ীর ধরণ :</span>
                            <span>{{$vehicle_type->title}}</span>
                            <input name="vehicle_type" type="hidden" value="{{$booking_fields['vehicle_type']}}">
                        </li>

                        <li>
                            <span>ধারণক্ষমতা:</span>
                            <span>{{$booking_fields['capacity']}} {{$vehicle_type->capacityType->title}}</span>
                            <input name="capacity" type="hidden" value="{{$booking_fields['capacity']}}">
                        </li>

                        <li>
                            <span>পিক-আপের ঠিকানা:</span>
                            <span>
                                {{$booking_fields['from_address']}}
                            </span>
                            <input name="from_point" type="hidden" value="{{$booking_fields['from_point']}}">
                            <input name="from_address" type="hidden" value="{{$booking_fields['from_address']}}">     
                            <input name="from_lat" type="hidden" value="{{$booking_fields['from_lat']}}">
                            <input name="from_lon" type="hidden" value="{{$booking_fields['from_lon']}}">                       
                        </li>
                        
                        <li>
                            <span>গন্তব্যের ঠিকানা:</span>
                            <span>
                                {{$booking_fields['to_address']}}
                            </span>
                            <input name="to_point" type="hidden" value="{{$booking_fields['to_point']}}">
                            <input name="to_address" type="hidden" value="{{$booking_fields['to_address']}}">
                            <input name="to_lat" type="hidden" value="{{$booking_fields['to_lat']}}">
                            <input name="to_lon" type="hidden" value="{{$booking_fields['to_lon']}}">
                        </li>
                        
                        <li>
                            <span>দূরত্ব:</span>
                            <span>
                                {{$route['distance']}} km
                            </span>
                        </li>

                        <li>
                            <span>যাত্রার ধরণ:</span>
                            <span>
                               {{$booking_fields['trip_type']}}
                            </span>
                             <input name="trip_type" type="hidden" value="{{$booking_fields['trip_type']}}">
                        </li>

                        <li>
                            <span>বুকিং এর সময়:</span>
                            <span>{{$booking_fields['booking_type']}}</span>
                            <input name="booking_type" type="hidden" value="{{$booking_fields['booking_type']}}">
                        </li>
                        
                        @if($booking_fields['booking_type']=='advance')
                        <li>
                            <span>তারিখ ও সময়:</span>
                            <span>{{$booking_fields['date_time']}}</span>
                            <input name="date_time" type="hidden" value="{{$booking_fields['date_time']}}">
                        </li>
                        @endif

                    </ul>

                    <hr />

                    <!-- @if($fare_breakdown['fare_breakdown']['discount']>0)
                        <h2 class="your-cost text-center">মোট খরচঃ <strike>{{$fare_breakdown['total_cost']+$fare_breakdown['fare_breakdown']['discount']}}</strike>&nbsp;{{$fare_breakdown['total_cost']}} টাকা &nbsp;<span class="badge badge-success home-discount-pirce">{{$fare_breakdown['fare_breakdown']['discount']}} টাকা ডিসকাউন্ট </span></h2>
                    @else
                        <h2 class="your-cost text-center">আপনার খরচঃ   টাকা. {{$fare_breakdown['total_cost']}}</h2>
                    @endif -->

                    <h5 class="bfc-title mt-5">ভাড়ার তথ্য</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('total_fare')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">ভাড়া</span>
                            <input id="total_fare" name="total_fare" type="text" class="form-control" aria-describedby="addon-name" required="" value="{{$fare_breakdown['fare_breakdown']['total_fare']}}">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('total_fare')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('discount')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">ডিসকাউন্ট</span>
                            <input id="discount" name="discount" type="text" class="form-control" aria-describedby="addon-name" required="" value="{{$fare_breakdown['fare_breakdown']['discount']}}">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('discount')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('total_cost')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">ডিসকাউন্ট এর পর ভাড়া</span>
                            <input id="total_cost" name="total_cost" type="text" class="form-control" aria-describedby="addon-name" required="" value="{{$fare_breakdown['fare_breakdown']['total_cost']}}">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('total_cost')}}</div>
                    </div>


                    <!-- <input type="hidden" name="total_fare" value="{{$fare_breakdown['total_cost']+$fare_breakdown['fare_breakdown']['discount']}}">
                    <input type="hidden" name="discount" value="{{$fare_breakdown['fare_breakdown']['discount']}}">
                    <input type="hidden" name="total_cost" value="{{$fare_breakdown['total_cost']}}"> -->
                    <input type="hidden" name="distance" value="{{$route['distance']}}">
                    <input type="hidden" name="distance_type" value="{{$route['distance_type']}}">
                    <input type="hidden" name="duration" value="{{$route['duration']}}">
                    <input type="hidden" name="off_amount" value="{{$fare_breakdown['fare_breakdown']['off_amount']}}">

                    <h5 class="bfc-title mt-5">পারিশ্রমিকের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('[payment_by]')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">পারিশ্রমিকদাতা</span>
                            <select id="payment_by" name="payment_by" class="custom-select" aria-describedby="addon-booking-type">                                
                                <option @if(old('payment_by')=='recipient') selected @endif value="recipient">প্রাপক</option>
                                <option @if(old('payment_by')=='customer') selected @endif value="customer">নিজে</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('[payment_by]')}}</div>
                    </div>                    

                    <h5 class="bfc-title mt-5">প্রাপকের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('recipient_name')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-name">নাম*</span>
                            <input id="recipient_name" name="recipient_name" type="text" class="form-control" aria-describedby="addon-name" required="">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('recipient_name')}}</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-flex @if($errors->first('recipient_phone')!=null) has-danger @endif">
                            <span class="input-group-addon" id="addon-phone">ফোন নং*</span>
                            <input id="recipient_phone" name="recipient_phone" type="text" class="form-control" aria-describedby="addon-phone" required="">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('recipient_phone')}}</div>
                    </div>

                    <h5 class="bfc-title mt-5">পণ্যের বিবরণ</h5>
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-from-address">পণ্য এর ক্যাটাগরি</span>
                            <input id="particular_details" name="particular_details" type="text" class="form-control map_field" value="{{old('particular_details')}}" aria-describedby="addon-from-address" required="">
                        </div>
                        <div class="form-control-feedback">{{$errors->first('particular_details')}}</div>
                    </div>

                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon">অপেক্ষার সময় (প্রায়)</span>
                            <select id="waiting_time" name="waiting_time" class="custom-select">    
                                <option value=""></option>   
                                <option value="1 hour">1 hour</option>
                                <option value="2 hours">2 hours</option>
                                <option value="3 hours">3 hours</option>
                                <option value="4 hours">4 hours</option>
                                <option value="5 hours">5 hours</option>
                                <option value="6 hours">6 hours</option>
                            </select>
                        </div>
                        <div class="form-control-feedback">{{$errors->first('waiting_time')}}</div>
                    </div>  
                    <div class="form-group">
                        <div class="input-group input-flex">
                            <span class="input-group-addon" id="addon-description">বিশেষ নির্দেশনা</span>
                            <textarea id="special_instruction" name="special_instruction" rows="3" class="form-control" aria-describedby="addon-description"></textarea>
                        </div>
                    </div>                 
                    
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary book-submit btn-block">Confirm Edit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-6 col-xl-7">
            <div class="map-block map-booking">
                <iframe src="https://www.google.com/maps/embed/v1/directions?key={{config('heavygari.google_maps.api_key')}}&origin={{$booking_fields['from_lat']}},{{$booking_fields['from_lon']}}&destination={{$booking_fields['to_lat']}},{{$booking_fields['to_lon']}}" width="800" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<div class="map-block-overlay"></div>-->
            </div>
        </div>
    </div>
    
    <!--<ul class="confrim-list">
        <pre /> {{ print_r($fare_breakdown) }} </pre>
    </ul>-->
    
</div>
@stop

@section('footer')
<script type = "text/javascript" > 
$(document).ready(function() {

    document.getElementById("recipient_name").value = '<?php echo $booking->recipient_name;?>';
    document.getElementById("recipient_phone").value = '<?php echo $booking->recipient_phone;?>';
    document.getElementById("payment_by").value = '<?php echo $booking->payment_by;?>';
    document.getElementById("particular_details").value = '<?php echo $booking->particular_details;?>';
    document.getElementById("waiting_time").value = '<?php echo $booking->waiting_time;?>';
    document.getElementById("special_instruction").value = '<?php echo $booking->special_instruction;?>';


    function validateForm() {
        var recipient_name = document.getElementById("recipient_name").value;
        var recipient_phone = document.getElementById("recipient_phone").value;
        if (recipient_name == "" || recipient_phone == "") {
            alert("Please provide Recipient's Name & Phone");
            return false;
        }
    }

    $("#total_fare").on("change, keyup" ,function(e) 
        {
            let currentTotalFare = $(this).val();
            currentTotalFare = currentTotalFare.replace(/[^0-9.]/g,'');
            $(this).val(currentTotalFare);

            let discount = $("#discount").val();
            let total_cost = parseFloat(currentTotalFare) - parseFloat(discount);

            $("#total_cost").val(total_cost);
        });

        $("#discount").on("change, keyup" ,function(e) 
        {
            let currentDiscount = $(this).val();
            if(currentDiscount=='') {
                currentDiscount = '0';
            }
            currentDiscount = currentDiscount.replace(/[^0-9.]/g,'');
            $(this).val(currentDiscount);

            let total_fare = $("#total_fare").val();
            let total_cost = parseFloat(total_fare) - parseFloat(currentDiscount);

            $("#total_cost").val(total_cost);
        });
});


</script>
@stop