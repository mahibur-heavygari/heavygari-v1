@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Bookings</h1>
            <div class="title-sub">
                Showing {{count($bookings)}} trips
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link @if($filter=='open') active @endif" href="/cms/bookings?filter=open">Open</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='upcoming') active @endif" href="/cms/bookings?filter=upcoming">Upcoming</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='ongoing') active @endif" href="/cms/bookings?filter=ongoing">Ongoing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='completed') active @endif" href="/cms/bookings?filter=completed">Completed</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($filter=='cancelled') active @endif" href="/cms/bookings?filter=cancelled">Cancelled / Incompleted</a>
        </li>
    </ul>
</div>
@stop

@section('content-body')
    @include('/common/bookings/lists/'.$filter)
@stop