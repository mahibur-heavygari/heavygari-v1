@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Deliver Invoice Photo</h1>
            <div class="title-sub">
                {{$booking->unique_id}}
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
</div>
@stop

@section('content-body')
    <div class="text-center">
        <img src="{{Storage::url($booking->delivery_invoice_photo)}}" alt="...">
    </div>
@stop