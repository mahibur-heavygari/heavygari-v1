@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
    <div class="page-header-content">
        <div class="page-header-meta">
            <div class="page-header-cell">
                <h1 class="title">Volumetric Weight Category</h1>

                <div class="title-sub">
                    Showing {{count($volumetric_categories)}} Volumetric Weight Categories
                </div>
            </div>
            <div class="page-header-cell">
                <ul class="page-header-meta-list">
                    <li>
                        <a href="/cms/base/volumetric_weight_categories/create"
                           class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add New Category</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop

@section('content-body')
    <div class="row">
        <div class="col-sm-12 alert alert-primary">
            <span>Volumetric Weight = ( Length x Breadth x Height) / 305 </span>
        </div>
        <hr />
        <div class="col-sm-12">
            <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
                <table id="example" class="table" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Height</th>
                        <th>Width</th>
                        <th>Length</th>
                        <th>Volumetric Weight</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($volumetric_categories as $volumetric_category)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$volumetric_category->title}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$volumetric_category->height}} inch</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$volumetric_category->width}} inch</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$volumetric_category->length}} inch</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$volumetric_category->volumetric_weight}} kg</div>
                            </td>
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                    <li>
                                        <a href="/cms/base/volumetric_weight_categories/{{$volumetric_category->id}}/edit"
                                           title="Edit"
                                           class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form class="" method="POST" enctype='multipart/form-data'
                                              action="/cms/base/volumetric_weight_categories/{{$volumetric_category->id}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit"
                                                    class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmDelete();">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('footer')
  <script>
    function ConfirmDelete()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop