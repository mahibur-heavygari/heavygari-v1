@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Pop Up Notification</h1>
            <div class="title-sub">
            </div>
        </div>
    </div>
</div>
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
    }
</style>
@stop
@section('content-body')
<div class="row">
    <div class="col-md-12 col-lg-12 col-xxl-12">
        <div class="row">
            <div class="col-md-12">
                <h6 class="title">Current Popup Notification</h6>
                <br>
                <div class="card card-site card-white card-lg mb-4 card-driver-mandatory">
                    <div class="card-header">
                        <div class="d-flex align-items-center"> 
                            @if($not_expired)
                                <div>
                                    <div class="dm-avater">
                                        <img src="https://www.heavygari.com{{Storage::url($pop_up->photo)}}" alt="...">
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h6 class="dd-title color-primary">Button Position: 
                                        @if($pop_up->button_position==0) Upper left @endif
                                        @if($pop_up->button_position==1) Upper middle @endif
                                        @if($pop_up->button_position==2) Upper right @endif
                                        @if($pop_up->button_position==3) Lower left @endif
                                        @if($pop_up->button_position==4) Lower middle @endif
                                        @if($pop_up->button_position==5) Lower right @endif
                                    </h6>
                                    <h6 class="wh-title color-primary">
                                        Created Date & Time: {{$pop_up->created_at->format('h:ia d/m/y')}}
                                    </h6>
                                    <h6 class="wh-title color-primary">
                                        Expired Date & Time: 
                                        @if(!is_null($pop_up->expired_at))
                                            {{$pop_up->expired_at->format('h:ia d/m/y')}}
                                        @endif
                                    </h6>
                                    <form class="" method="POST" enctype="multipart/form-data" action="/cms/make-pop-up-expired" >
                                        {{ csrf_field() }}
                                        <input class="btn btn-danger" type="submit" value="Make pop up expired">
                                    </form>
                                  
                                </div>
                            @else
                                <div class="pl-6">
                                    <h6>No pop up found</h6>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Select Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" action="/cms/send/post_popup_notification" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('button_position')!=null) has-danger @endif">
                                <label for="button_position" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Button Position</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="button_position" name="button_position" required=""> 
                                        <option value="">Select Position</option>
                                        <option value="0">Upper Left</option>
                                        <option value="1">Upper Middle</option>
                                        <option value="2">Upper Right</option>
                                        <option value="3">Lower Left</option>
                                        <option value="4">Lower Middle</option>
                                        <option value="5">Lower Right</option>
                                        <option value="-1">No Button</option>
                                        
                                    </select>  
                                    <div class="form-control-feedback">
                                        @if($errors->first('button_position') !=null ) 
                                            {{ $errors->first('button_position')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('image')!=null) has-danger @endif">     
                                <label for="image" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Image</label>
                                <div class="col-md-4 col-lg-3">
                                    <div class="upload-styled-image upload-styled-image-40p mb-2">
                                        <div class="uploaded-image uploaded-here"></div>
                                        <div class="input-file">
                                            <input class="file-input" type="file" name="image" id="image" alt="HeavyGari Image" required="">
                                            <span class="upload-icon">
                                                <i class="icofont icofont-upload-alt"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-control-feedback">
                                        @if($errors->first('image') !=null ) 
                                            {{ $errors->first('image')}} 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('expired_at')!=null) has-danger @endif">     
                                <label for="expired_at" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Validity Date</label>
                                <div class="col-md-12 col-lg-8">
                                    <div class='input-group date' id="datetimepicker5">
                                        <input type='text' class="form-control validity" name="expired_at"/>
                                        <div class="input-group-postpend" id="calendar">
                                          <div class="input-group-text">
                                               <span class="input-group-addon">
                                                <span class="icofont icofont-calendar"></span>
                                                </span>
                                          </div>
                                        </div>
                                        <div class="form-check mb-2 push-notify-unlimited">
                                        <input class="form-check-input" type="checkbox" id="autoSizingCheck" />
                                        <label class="form-check-label" for="autoSizingCheck">
                                          Unlimited Time
                                        </label>
                                      </div>
                                    </div>
                                    <div class="form-control-feedback">
                                        @if($errors->first('expired_at') !=null ) 
                                            {{ $errors->first('expired_at')}} 
                                        @endif
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker5').datetimepicker({
            /*disabledDates: [
                moment("12/25/2013"),
                new Date(2013, 11 - 1, 21),
                "11/22/2013 00:53"
            ]*/
        });
    });
    $("#autoSizingCheck").change(function() {
        if($(this).prop('checked')) {
            var check = $(this).val(null);
        } else {
            var check = $(this).val(); 
        }    
    });
</script>
@stop