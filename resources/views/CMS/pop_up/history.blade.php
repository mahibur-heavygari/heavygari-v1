@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop
@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Pop-up Notification History<br></h1>
        </div>    
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>
                        <th class="text-center"><span class="text-lighter"> Created Date & Time</span>
                        </th>    
                        <th class="text-center"><span class="text-lighter">Image</span></th>   
                        <th class="text-center"><span class="text-lighter">Button Position</span>
                        </th> 
                        <th class="text-center"><span class="text-lighter">Expired Date & Time</span>
                        </th>     
                                           
                    </tr>
                </thead>
                <tbody>   
                    @foreach($pop_up as $pop_up)
                    <tr>
                        <td class="text-center">{{ $pop_up->created_at->toDayDateTimeString()}}</td>
                        <td class="text-center">
                            <a href="https://www.heavygari.com{{Storage::url($pop_up->photo)}}" target="_blank">Click for view</a>
                           <a href="https://www.heavygari.com{{Storage::url($pop_up->photo)}}" target="_blank"><img src="https://www.heavygari.com{{Storage::url($pop_up->photo)}}" width="100px" height="100px"></a>
                       </td>
                        <td class="text-center">{{ $pop_up->button_position}}</td>
                        @if($pop_up->expired_at == null)
                        <td class="text-center">Unlimited Time</td>
                        @else
                        <td class="text-center">{{ $pop_up->expired_at->toDayDateTimeString()}}</td>
                        @endif
                       
                    </tr>
                    @endforeach
               
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop
@section('footer')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker5').datetimepicker({
            /*disabledDates: [
                moment("12/25/2013"),
                new Date(2013, 11 - 1, 21),
                "11/22/2013 00:53"
            ]*/
        });
    });
    $("#autoSizingCheck").change(function() {
        if($(this).prop('checked')) {
            var check = $(this).val(null);
        } else {
            var check = $(this).val(); 
        }    
    });
</script>
@stop