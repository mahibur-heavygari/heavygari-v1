@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">{{$trip_category->sourcePoint->title}} to {{$trip_category->destinationPoint->title}} ( {{$trip_category->trip_category}} trip)</h1>
            <div class="title-sub">
                Showing {{count($vehicle_types)}} Vehicles
            </div>            
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Add Multiplier</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/base/trip_categories/vehicle_wise_charge">
                    {{ csrf_field() }}
                    <input type="hidden" name="trip_category_id" value="{{$trip_category->id}}">
                    <div class="row">
                        <div class="col-md-9 col-lg-9">   
                            @foreach($vehicle_types as $vehicle)   
                                <input type="hidden" name="vehicle_types[]" value="{{$vehicle->id}}">                 
                                <div class="form-group row">
                                    <label for="title" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">{{$vehicle->title}}</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$vehicle->multiplier}}" id="title" name="multiplier[]"  autocomplete="off">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group row">
                                <label for="title" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed"><strong>Distance Type</strong></label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="radio">
                                        <label><input type="radio" name="distance_type" value="short" @if($distance_type=='short') checked @endif> Short</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="distance_type" value="normal" @if($distance_type=='normal') checked @endif> Normal</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="distance_type" value="long" @if($distance_type=='long') checked @endif> Long</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop