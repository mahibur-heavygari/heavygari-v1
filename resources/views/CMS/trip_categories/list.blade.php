@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Trip Categorys</h1>
            <div class="title-sub">
                Showing {{count($list)}} Trip Categorys
            </div>            
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/cms/base/trip_categories/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add Trip Category</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Source</th>
                        <th>Destination</th>
                        <th>Trip Category</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->sourcePoint->title}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->destinationPoint->title}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->trip_category}}</div>
                            </td>
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                    <li>
                                        <a href="/cms/base/trip_categories/{{$item->id}}/vehicle_wise_charge" title="Add Multiplier" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="icofont icofont-money-bag"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/base/trip_categories/{{$item->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/base/trip_categories/{{$item->id}}/delete" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmDelete();">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('footer')
  <script>
    function ConfirmDelete()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop