@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Add Trip Category</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Add Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/base/trip_categories">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('src_point_id')!=null) has-danger @endif">
                                <label for="src_point_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Source</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="src_point_id" name="src_point_id">
                                        @foreach($points as $point)
                                            <option  @if(old('src_point_id')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</optsion>
                                        @endforeach
                                    </select>  
                                    <div class="form-control-feedback">@if($errors->first('src_point_id')!=null) {{ $errors->first('src_point_id')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('dest_point_id')!=null) has-danger @endif">
                                <label for="dest_point_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Destiantion</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="dest_point_id" name="dest_point_id">
                                        @foreach($points as $point)
                                            <option  @if(old('dest_point_id')==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</optsion>
                                        @endforeach
                                    </select>
                                    <div class="form-control-feedback">@if($errors->first('dest_point_id')!=null) {{ $errors->first('dest_point_id')}} @endif</div>
                                </div>
                            </div>                            
                            <div class="form-group row @if($errors->first('trip_category')!=null) has-danger @endif">
                                <label for="trip_category" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="trip_category" name="trip_category">
                                        @foreach($types as $type)
                                            <option  @if(old('trip_category')==$type) selected @endif value="{{$type}}">{{$type}}</optsion>
                                        @endforeach
                                    </select>
                                    <div class="form-control-feedback">@if($errors->first('trip_category')!=null) {{ $errors->first('trip_category')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop