@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Add Vehicle Type</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">1. Basic Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/base/vehicle_types">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">                        
                            <div class="form-group row @if($errors->first('title')!=null) has-danger @endif">
                                <label for="title" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Title (In English)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('title')}}" id="title" name="title">
                                    <div class="form-control-feedback">@if($errors->first('title')!=null) {{ $errors->first('title')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('title_bn')!=null) has-danger @endif">
                                <label for="title_bn" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Title (বাংলায়)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('title_bn')}}" id="title_bn" name="title_bn">
                                    <div class="form-control-feedback">@if($errors->first('title_bn')!=null) {{ $errors->first('title_bn')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('capacity_type_id')!=null) has-danger @endif">
                                <label for="capacity_type_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Capacity Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="capacity_type_id" name="capacity_type_id">
                                        <option value="">--Please Select--</option>
                                        @foreach($capacities as $capacity)
                                            <option value="{{$capacity->id}}">{{$capacity->title}}</option>
                                        @endforeach
                                    </select>                                
                                    <div class="form-control-feedback">@if($errors->first('capacity_type_id')!=null) {{ $errors->first('capacity_type_id')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('capacity')!=null) has-danger @endif">
                                <label for="capacity" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Capacity</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('capacity')}}" id="capacity" name="capacity">
                                    <div class="form-control-feedback">@if($errors->first('capacity')!=null) {{ $errors->first('capacity')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('max_capacity')!=null) has-danger @endif">
                                <label for="max_capacity" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Maximum Capacity</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg likemax_capacity-field form-control-danger" type="text" value="{{old('max_capacity')}}" id="max_capacity" name="max_capacity">
                                    <div class="form-control-feedback">@if($errors->first('max_capacity')!=null) {{ $errors->first('max_capacity')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('icon')!=null) has-danger @endif">
                                <label for="icon" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Icon</label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="input-file">
                                        <input type="file" name="icon" id="icon" class="file-input">
                                        <span class="upload-icon">
                                            <i class="icofont icofont-upload-alt"></i>
                                        </span>
                                    </div>
                                    <div class="form-control-feedback">@if($errors->first('icon')!=null) {{ $errors->first('icon')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop