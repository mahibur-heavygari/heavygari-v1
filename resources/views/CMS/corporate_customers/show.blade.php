@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Corporate Customer Profile</h1>
            <div class="title-sub">
                {{$corporate_customer->company_name}}
            </div>
        </div>    
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/cms/corporate_customers/{{$corporate_customer->id}}/users" class="btn btn-warning btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1"><i class="fa fa-users"></i> Users </a>
                    <br>
                </li>
            </ul>
        </div>    
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Basic Information </h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="" enctype="multipart/form-data" action="" >
                {{ csrf_field() }}
                    <div class="row">
                        <!-- <div class="col-md-3 col-lg-3">
                            <div>Photo</div>
                            <div class="upload-styled-image rounded-circle" style="width: 120px; height: 120px;">
                                <div class="uploaded-image uploaded-here" style=""></div>
                                <div class="input-file">
                                    <input type="file" name="photo" class="file-input">
                                    <span class="upload-icon">
                                        <i class="icofont icofont-upload-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-9 col-lg-9">                           
                            <div class="form-group row @if($errors->first('company_name')!=null) has-danger @endif">
                                <label for="company_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Company Name</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$corporate_customer->company_name}}" id="company_name" name="company_name" required="">
                                    <div class="form-control-feedback">@if($errors->first('company_name')!=null) {{ $errors->first('company_name')}} @endif</div>
                                </div>
                            </div>    
                            <hr />                           
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                                <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Cheque</label>                           
                                <div class="col-md-12 col-lg-8">     
                                    <div class="col-sm-6">
                                        <div class="tumblr-img tumblr-img-pro">
                                            <img src="{{Storage::url($corporate_customer->cheque)}}" alt="...">
                                            <a href="{{Storage::url($corporate_customer->cheque)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                                <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Agreement</label>                           
                                <div class="col-md-12 col-lg-8">     
                                    <div class="col-sm-6">
                                        <div class="tumblr-img tumblr-img-pro">
                                            <img src="{{Storage::url($corporate_customer->agreement)}}" alt="...">
                                            <a href="{{Storage::url($corporate_customer->Agreement)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                                <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Bank Guarantee</label>                           
                                <div class="col-md-12 col-lg-8">     
                                    <div class="col-sm-6">
                                        <div class="tumblr-img tumblr-img-pro">
                                            <img src="{{Storage::url($corporate_customer->bank_guarantee)}}" alt="...">
                                            <a href="{{Storage::url($corporate_customer->bank_guarantee)}}" class="ti-view image-popup"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>  
                            <div class="form-group row @if($errors->first('amount')!=null) has-danger @endif">
                                <label for="amount" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Current Balance</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$corporate_customer->balance}}" id="amount" name="amount" readonly="">
                                    <div class="form-control-feedback">@if($errors->first('amount')!=null) {{ $errors->first('amount')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('debit_credit')!=null) has-danger @endif">
                                <label for="debit_credit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Debit/Credit</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{$corporate_customer->debit_credit}}" id="debit_credit" name="debit_credit" readonly="">
                                    <div class="form-control-feedback">@if($errors->first('debit_credit')!=null) {{ $errors->first('debit_credit')}} @endif</div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
