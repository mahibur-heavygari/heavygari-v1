@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Corporate Customer {{$user_type}} Profile</h1>
            <div class="title-sub">
                {{$profile->user->name}}
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
    @include('common/customer/details')

    <br>
    <h4 class="dd-title">Booking History</h4>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
                <table id="example" class="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date & Time</th>
                            <th>Booking ID & Vehicle</th>
                            <th>Status</th>

                            <th>Owner</th>
                            <th>Driver</th>

                            <th>Fare</th>
                            <th>Distance</th>

                            <th>Source</th>
                            <th>Distance</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($history as $booking)
                                <tr>
                                    <td>
                                        <div class="text-nowrap">{{$booking->datetime->format('h:ia d/m/y')}}</div>
                                    </td>
                                    <td>
                                        <a href="/cms/bookings/{{$booking->unique_id}}" class="text-nowrap">{{$booking->unique_id}}</a> ( @if($booking->booking_category == 'full') {{$booking->fullBookingDetails->vehicleType->title}} @else &nbsp; @endif )
                                    </td>
                                    <td>
                                        <div class="text-nowrap">{{$booking->status}}</div>
                                    </td>
                                    <td>
                                        <div class="text-nowrap text-lighter text-sm">
                                            @if(isset($booking->owner)){{$booking->owner->user->name}}<br>( {{$booking->owner->user->phone}} )@else &nbsp; @endif                              
                                        </div>
                                    </td>
                                    <td>
                                        <div class="text-nowrap text-lighter text-sm">
                                            @if(isset($booking->hiredDriver))
                                                {{$booking->hiredDriver->name}}<br>( {{$booking->hiredDriver->phone}} )
                                            @elseif(isset($booking->driver))
                                                {{$booking->driver->user->name}}<br>( {{$booking->driver->user->phone}} )
                                            @else 
                                                &nbsp; 
                                            @endif
                                        </div>
                                    </td>

                                    <td>
                                        <div class="text-nowrap text-lighter text-sm">
                                            টাকা {{$booking->invoice->total_cost}}                               
                                        </div>
                                    </td> 
                                    <td>
                                        <div class="text-nowrap text-lighter text-sm">
                                            {{$booking->trips[0]->distance}}km
                                        </div>
                                    </td>
 
                                    <td>{{$booking->trips[0]->origin->fullname}}</td>
                                    <td>{{$booking->trips[0]->destination->fullname}}</td>

                                    <td class="text-right">
                                        <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                            <li>
                                                <a href="/cms/bookings/{{$booking->unique_id}}" class="btn btn-gray btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">Details</a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop