@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Corporate Customers</h1>
            <div class="title-sub">
                Showing {{count($list)}} companies
            </div>            
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/cms/corporate_customers/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add Corporate Customer</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Create Date</th>
                        <th>Company Name</th>
                        <th>Current Balance / Due</th>
                        <th>Debit/Credit</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{date('d/m/Y', strtotime($item->created_at))}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->company_name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">Tk {{abs($item->balance)}} @if($item->debit_credit=='credit') ( Limit Tk {{$item->limit}})@endif</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{ucfirst($item->debit_credit)}}</div>
                            </td>
                            
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">                                    
                                    <li>
                                        <a href="/cms/corporate_customers/{{ $item->id }}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>  
                                    <!-- 
                                    <li>
                                        <a href="/cms/users/potential_customers/{{$item->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="/cms/corporate_customers/{{$item->id}}/delete" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>                           
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('footer')
  <script>
    function ConfirmAction()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop