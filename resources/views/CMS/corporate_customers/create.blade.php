@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Add Corporate Customer</h1>
        </div>        
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Basic Information </h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype="multipart/form-data" action="/cms/corporate_customers" >
                {{ csrf_field() }}
                    <div class="row">
                        <!-- <div class="col-md-3 col-lg-3">
                            <div>Photo</div>
                            <div class="upload-styled-image rounded-circle" style="width: 120px; height: 120px;">
                                <div class="uploaded-image uploaded-here" style=""></div>
                                <div class="input-file">
                                    <input type="file" name="photo" class="file-input">
                                    <span class="upload-icon">
                                        <i class="icofont icofont-upload-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-9 col-lg-9">                           
                            <div class="form-group row @if($errors->first('company_name')!=null) has-danger @endif">
                                <label for="company_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Company Name</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('company_name')}}" id="company_name" name="company_name" required="">
                                    <div class="form-control-feedback">@if($errors->first('company_name')!=null) {{ $errors->first('company_name')}} @endif</div>
                                </div>
                            </div>    
                            <hr />                           
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                                <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Cheque</label>                           
                                <div class="col-md-12 col-lg-8">     
                                    <div class="col-sm-6">
                                        <div class="upload-styled-image upload-styled-image-40p mb-2">
                                            <div class="uploaded-image uploaded-here" style=""></div>
                                            <div class="input-file">
                                                <input class="file-input" type="file" name="cheque" id="cheque">
                                                <span class="upload-icon">
                                                    <i class="icofont icofont-upload-alt"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                                <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Agreement</label>                           
                                <div class="col-md-12 col-lg-8">     
                                    <div class="col-sm-6">
                                        <div class="upload-styled-image upload-styled-image-40p mb-2">
                                            <div class="uploaded-image uploaded-here" style=""></div>
                                            <div class="input-file">
                                                <input class="file-input" type="file" name="agreement" id="agreement">
                                                <span class="upload-icon">
                                                    <i class="icofont icofont-upload-alt"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('license_no')!=null) has-danger @endif">
                                <label for="license_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Bank Guarantee</label>                           
                                <div class="col-md-12 col-lg-8">     
                                    <div class="col-sm-6">
                                        <div class="upload-styled-image upload-styled-image-40p mb-2">
                                            <div class="uploaded-image uploaded-here" style=""></div>
                                            <div class="input-file">
                                                <input class="file-input" type="file" name="bank_guarantee" id="bank_guarantee">
                                                <span class="upload-icon">
                                                    <i class="icofont icofont-upload-alt"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>   

                            <div class="form-group row @if($errors->first('debit_credit')!=null) has-danger @endif">
                                <label for="debit_credit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Debit/Credit</label>
                                <div class="col-md-12 col-lg-8">
                                    <input type="radio" name="debit_credit" value="debit"> Debit<br>
                                    <input type="radio" name="debit_credit" value="credit"> Credit
                                    <div class="form-control-feedback">@if($errors->first('debit_credit')!=null) {{ $errors->first('debit_credit')}} @endif</div>
                                </div>
                            </div>     
                            <div class="form-group row @if($errors->first('amount')!=null) has-danger @endif">
                                <label for="amount" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed" id="debit_credit_label">Amount/Limit</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('amount')}}" id="amount" name="amount" onkeypress="return isNumber(event)" required="">
                                    <div class="form-control-feedback">@if($errors->first('amount')!=null) {{ $errors->first('amount')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')

<script>
    function isNumber(event){
        var keycode = event.keyCode;
        if(keycode>=48 && keycode<=57){
            return true;
        }
        return false;
    }

    $(document).ready(function(){
        $("input[name='debit_credit']").change(function(){
            if($(this).val()=='debit'){
                $("#debit_credit_label").text("Amount");
            }else{
                $("#debit_credit_label").text("Limit");
            }
        }); 
    }); 
</script>
@stop