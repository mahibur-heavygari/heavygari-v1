@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Send SMS</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-site card-lg card-trip-metas mb-4 card-vehicle-details" data-mh="trip-details-card-match">
                    <div class="card-block">
                        <div class="card-divider-title">Details</div>
                        <ul class="list-unstyled vehicle-sort-info">
                            <li><span class="name-text">Shop Name :</span> <span>{{$item->shop_name}}</span></li>
                            <li><span class="name-text">Owner Name :</span> <span>{{$item->owner_name}}</span></li>
                            <li><span class="name-text">Address:</span> <span>{{$item->address}}</span></li>
                            <li><span class="name-text">Nature Of Business:</span> <span>{{$item->nature_of_business}}</span></li>                            
                            <li><span class="name-text">Point :</span> <span>{{$item->point->title}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card card-site card-lg card-trip-metas mb-4 card-vehicle-details" data-mh="trip-details-card-match">
                    <div class="card-block">
                        <div class="card-divider-title">SMS</div>
                        <div class="col-md-8 col-lg-8">
                            <form class="" method="POST" action="/cms/authorized_points/{{$item->id}}/send_sms">
                            {{ csrf_field() }}
                                <div class="form-group row @if($errors->first('phone')!=null) has-danger @endif">
                                    <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone Number</label>
                                    <div class="col-md-12 col-lg-8 input-phone-group has-form-control-lg">
                                        <input class="form-control form-control-lg form-control-danger input-phone" type="text" value="{{old('phone')}}" id="phone" name="phone">
                                        <span class="country-code">+88</span>
                                        <div class="form-control-feedback">@if($errors->first('phone')!=null) {{ $errors->first('phone')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('message')!=null) has-danger @endif">
                                    <label for="message" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Message</label>
                                    <div class="col-md-12 col-lg-8">
                                        <textarea class="form-control form-control-danger" id="message" name="message">{{$item->shop_name.', '.$item->address.', '.$item->point->title.'. ফোন# : '.$item->phone}}</textarea>
                                        <div class="form-control-feedback">@if($errors->first('message')!=null) {{ $errors->first('message')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-12 col-lg-8 offset-lg-4">
                                        <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Send SMS</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop