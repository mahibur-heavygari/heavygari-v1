@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Edit Authorized Point</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">1. Basic Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/authorized_points/{{$item->id}}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row @if($errors->first('base_point_id')!=null) has-danger @endif">
                                <label for="base_point_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Point (City)</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="base_point_id" name="base_point_id">
                                        @foreach($points as $point)
                                            <option  @if(old('base_point_id', $item->base_point_id)==$point->id) selected @endif value="{{$point->id}}">{{$point->title}}</optsion>
                                        @endforeach
                                    </select>
                                    <div class="form-control-feedback">@if($errors->first('base_point_id')!=null) {{ $errors->first('base_point_id')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('arp_no')!=null) has-danger @endif">
                                <label for="arp_no" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">ARP No.</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('arp_no', $item->arp_no)}}" id="arp_no" name="arp_no">
                                    <div class="form-control-feedback">@if($errors->first('arp_no')!=null) {{ $errors->first('arp_no')}} @endif</div>
                                </div>
                            </div> 
                            <div class="form-group row @if($errors->first('owner_name')!=null) has-danger @endif">
                                <label for="owner_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Owner Name</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('owner_name', $item->owner_name)}}" id="owner_name" name="owner_name">
                                    <div class="form-control-feedback">@if($errors->first('owner_name')!=null) {{ $errors->first('owner_name')}} @endif</div>
                                </div>
                            </div>                            
                            <div class="form-group row @if($errors->first('shop_name')!=null) has-danger @endif">
                                <label for="shop_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Shop Name</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('shop_name', $item->shop_name)}}" id="shop_name" name="shop_name">
                                    <div class="form-control-feedback">@if($errors->first('shop_name')!=null) {{ $errors->first('shop_name')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('nature_of_business')!=null) has-danger @endif">
                                <label for="nature_of_business" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Nature of Business</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('nature_of_business', $item->nature_of_business)}}" id="nature_of_business" name="nature_of_business">
                                    <div class="form-control-feedback">@if($errors->first('nature_of_business')!=null) {{ $errors->first('nature_of_business')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('address')!=null) has-danger @endif">
                                <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Address</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('address', $item->address)}}" id="address" name="address">
                                    <div class="form-control-feedback">@if($errors->first('address')!=null) {{ $errors->first('address')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('phone')!=null) has-danger @endif">
                                <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('phone', $item->phone)}}" id="phone" name="phone">
                                    <div class="form-control-feedback">@if($errors->first('phone')!=null) {{ $errors->first('phone')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop