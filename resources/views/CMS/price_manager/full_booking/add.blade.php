@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Add Rate</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Price Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/price-manager/full-booking">
                    {{ csrf_field() }}
                     <div class="row">
                        <div class="col-md-9 col-lg-9">

                            <div class="form-group row @if($errors->first('vehicle_type_id')!=null) has-danger @endif">
                                <label for="vehicle_type_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Vehicle Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="vehicle_type_id" name="vehicle_type_id">
                                        @foreach($vehicle_types as $vehicle_type)
                                            <option  @if(old('vehicle_type_id')==$vehicle_type->id) selected @endif value="{{$vehicle_type->id}}">{{$vehicle_type->title}}</optsion>
                                        @endforeach
                                    </select>                                
                                    <div class="form-control-feedback">@if($errors->first('vehicle_type_id')!=null) {{ $errors->first('vehicle_type_id')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <h5 class="bfc-title">SHORT TRIP</h5>

                            <div class="form-group row @if($errors->first('short_trip_rate')!=null) has-danger @endif">
                                <label for="short_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('short_trip_rate')}}" id="short_trip_rate" name="short_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('short_trip_rate')!=null) {{ $errors->first('short_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <h5 class="bfc-title">NORMAL TRIP</h5>

                            <div class="form-group row @if($errors->first('up_trip_rate')!=null) has-danger @endif">
                                <label for="up_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Up Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('up_trip_rate')}}" id="up_trip_rate" name="up_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('up_trip_rate')!=null) {{ $errors->first('up_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('down_trip_rate')!=null) has-danger @endif">
                                <label for="down_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Down Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="down_trip_rate" value="{{old('down_trip_rate')}}" id="down_trip_rate" name="down_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('down_trip_rate')!=null) {{ $errors->first('down_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <h5 class="bfc-title">LONG TRIP</h5>

                            <div class="form-group row @if($errors->first('long_up_trip_rate')!=null) has-danger @endif">
                                <label for="long_up_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">UP Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="long_up_trip_rate" value="{{old('long_up_trip_rate')}}" id="long_up_trip_rate" name="long_up_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('long_up_trip_rate')!=null) {{ $errors->first('long_up_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('long_down_trip_rate')!=null) has-danger @endif">
                                <label for="long_down_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Down Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="long_down_trip_rate" value="{{old('long_down_trip_rate')}}" id="long_down_trip_rate" name="long_down_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('long_down_trip_rate')!=null) {{ $errors->first('long_down_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <div class="form-group row @if($errors->first('base_fare')!=null) has-danger @endif">
                                <label for="base_fare" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Base Fare</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="base_fare" value="{{old('base_fare')}}" id="base_fare" name="base_fare">
                                    <div class="form-control-feedback">@if($errors->first('base_fare')!=null) {{ $errors->first('base_fare')}} @endif</div>
                                </div>
                            </div>
                           
                            <div class="form-group row @if($errors->first('surcharge_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Surcharge</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="surcharge_rate" value="{{old('surcharge_rate')}}" id="surcharge_rate" name="surcharge_rate">
                                    <div class="form-control-feedback">@if($errors->first('surcharge_rate')!=null) {{ $errors->first('surcharge_rate')}} @endif</div>
                                </div>
                            </div>                            

                            <div class="form-group row @if($errors->first('discount_percent')!=null) has-danger @endif">
                                <label for="discount_percent" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Discount</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="discount_percent" value="{{old('discount_percent')}}" id="discount_percent" name="discount_percent">
                                    <div class="form-control-feedback">@if($errors->first('discount_percent')!=null) {{ $errors->first('discount_percent')}} @endif</div>
                                </div>
                            </div>

                             <div class="form-group row @if($errors->first('admin_commission')!=null) has-danger @endif">
                                <label for="admin_commission" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Admin Commission</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="admin_commission" value="{{old('admin_commission')}}" id="admin_commission" name="admin_commission">
                                    <div class="form-control-feedback">@if($errors->first('admin_commission')!=null) {{ $errors->first('admin_commission')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop