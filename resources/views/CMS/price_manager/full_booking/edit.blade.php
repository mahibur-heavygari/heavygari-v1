@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="up_trip_rate">Edit Rate</h1>
            <div class="up_trip_rate-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-up_trip_rate">1. Basic Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/price-manager/full-booking/{{$item->id}}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <input name="vehicle_type_id" type="hidden" value="{{$item->vehicle_type_id}}">
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                           <div class="form-group row @if($errors->first('vehicle_type_id')!=null) has-danger @endif">
                                <label for="vehicle_type_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Vehicle Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select disabled class="form-control-lg custom-select width-100per" id="vehicle_type_id" name="vehicle_type_id">
                                        @foreach($vehicle_types as $vehicle_type)
                                            <option value="{{$vehicle_type->id}}" @if($item->vehicle_type_id==$vehicle_type->id) selected @endif >{{$vehicle_type->title}}</optsion>
                                        @endforeach
                                    </select>                                
                                    <div class="form-control-feedback">@if($errors->first('vehicle_type_id')!=null) {{ $errors->first('vehicle_type_id')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <h5 class="bfc-title">SHORT TRIP</h5>
                            
                            <div class="form-group row @if($errors->first('short_trip_rate')!=null) has-danger @endif">
                                <label for="short_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('short_trip_rate', $item->short_trip_rate)}}" id="short_trip_rate" name="short_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('short_trip_rate')!=null) {{ $errors->first('short_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <h5 class="bfc-title">NORMAL TRIP</h5>

                            <div class="form-group row @if($errors->first('up_trip_rate')!=null) has-danger @endif">
                                <label for="up_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Up Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('up_trip_rate', $item->up_trip_rate)}}" id="up_trip_rate" name="up_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('up_trip_rate')!=null) {{ $errors->first('up_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('down_trip_rate')!=null) has-danger @endif">
                                <label for="down_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Down Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="down_trip_rate" value="{{old('down_trip_rate',$item->down_trip_rate)}}" id="down_trip" name="down_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('down_trip_rate')!=null) {{ $errors->first('down_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <h5 class="bfc-title">LONG TRIP</h5>

                            <div class="form-group row @if($errors->first('long_up_trip_rate')!=null) has-danger @endif">
                                <label for="long_up_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Up Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="long_up_trip_rate" value="{{old('long_up_trip_rate',$item->long_up_trip_rate)}}" id="long_up_trip_rate" name="long_up_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('long_up_trip_rate')!=null) {{ $errors->first('long_up_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('long_down_trip_rate')!=null) has-danger @endif">
                                <label for="long_down_trip_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Down Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="long_down_trip_rate" value="{{old('long_down_trip_rate',$item->long_down_trip_rate)}}" id="long_down_trip_rate" name="long_down_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('long_down_trip_rate')!=null) {{ $errors->first('long_down_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <div class="form-group row @if($errors->first('base_fare')!=null) has-danger @endif">
                                <label for="base_fare" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Base Fare</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="base_fare" value="{{old('base_fare',$item->base_fare)}}" id="base_fare" name="base_fare">
                                    <div class="form-control-feedback">@if($errors->first('base_fare')!=null) {{ $errors->first('base_fare')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('surcharge_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Surcharge Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="surcharge_rate" value="{{old('surcharge_rate',$item->surcharge_rate)}}" id="surcharge_rate" name="surcharge_rate">
                                    <div class="form-control-feedback">@if($errors->first('surcharge_rate')!=null) {{ $errors->first('surcharge_rate')}} @endif</div>
                                </div>
                            </div>                            

                            <div class="form-group row @if($errors->first('discount_percent')!=null) has-danger @endif">
                                <label for="discount_percent" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Discount Percent</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="discount_percent" value="{{old('discount_percent',$item->discount_percent)}}" id="discount_percent" name="discount_percent">
                                    <div class="form-control-feedback">@if($errors->first('discount_percent')!=null) {{ $errors->first('discount_percent')}} @endif</div>
                                </div>
                            </div>                      

                            <div class="form-group row @if($errors->first('from_discount_date')!=null) has-danger @endif">
                                <label for="from_discount_date" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Discount Date & Time ( From )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger discount-datetime-picker" type="text" value="@if(!is_null($item->from_discount_date)) {{old('from_discount_date',$item->from_discount_date->format('d/m/Y h:ia'))}} @else '' @endif" id="from_discount_date" name="from_discount_date">
                                    <div class="form-control-feedback">@if($errors->first('from_discount_date')!=null) {{ $errors->first('from_discount_date')}} @endif</div>
                                </div>
                            </div>                    

                            <div class="form-group row @if($errors->first('to_discount_date')!=null) has-danger @endif">
                                <label for="to_discount_date" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Discount Date & Time ( To )</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger discount-datetime-picker" type="text" value="@if(!is_null($item->to_discount_date)) {{old('to_discount_date',$item->to_discount_date->format('d/m/Y h:ia'))}} @else '' @endif" id="to_discount_date" name="to_discount_date">
                                    <div class="form-control-feedback">@if($errors->first('to_discount_date')!=null) {{ $errors->first('to_discount_date')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('admin_commission')!=null) has-danger @endif">
                                <label for="admin_commission" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Admin Commission</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="admin_commission" value="{{old('admin_commission',$item->admin_commission)}}" id="admin_commission" name="admin_commission">
                                    <div class="form-control-feedback">@if($errors->first('admin_commission')!=null) {{ $errors->first('admin_commission')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop


@section('footer')
<script type="text/javascript">
    $(document).ready(function() {    
        $(function() {
            $('.discount-datetime-picker').datetimepicker({
                format: 'DD/MM/YYYY LT'
            });
        });
    });
</script>
@stop