@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Full Booking Price Chart</h1>           
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/cms/price-manager/full-booking/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add Rate</a>
                </li>                 
            </ul>
        </div>
    </div>   
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="text-right">
        
        </div>
        <div class="table-responsive">
            <table  class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                     <tr>
                        <th></th>
                        <th class="text-right" colspan="2">
                            <span class="text-warning">SHORT DISTANCE <br />
                                <span class="small">(<{{config('heavygari.distance.full_booking.short')}} km)</span>
                            </span>
                        </th>                        
                        <th class="text-right" colspan="2">
                            <span class="text-success">NORMAL DISTANCE <br /> 
                                <span class="small">({{config('heavygari.distance.full_booking.short')}} km - {{config('heavygari.distance.full_booking.long')}} km)</span>
                            </span>
                        </th>
                        <th class="text-right" colspan="2">
                            <span class="text-primary">LONG DISTANCE <br /> 
                                <span class="small">({{config('heavygari.distance.full_booking.long')}} km+)</span>
                            </span>
                        </th>
                        <th class="text-right"></th>                        
                        <th class="text-right"></th>                        
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                    </tr>
                    <tr>
                        <th>Vehicle Type</th>
                        <th class="text-right text-warning">
                            Base Fare
                        </th>
                        <th class="text-right text-warning">
                            Rate <span class="small">(/km)</span>
                        </th>
                        <th class="text-right text-success">
                            Up <span class="small">(/km)</span>
                        </th>
                        <th class="text-right text-success">
                            Down <span class="small">(/km)</span>
                        </th>
                        <th class="text-right text-primary">
                            Up <span class="small">(/km)</span>
                        </th>
                        <th class="text-right text-primary">
                            Down <span class="small">(/km)</span>
                        </th>                        
                        <th class="text-right">
                            Surcharge <span class="small">(/km)</span>
                        </th>                        
                        <th class="text-right">
                            Discounts <span class="small">(%)</span>
                        </th>                      
                        <th class="text-right">
                            Discount Duration
                        </th>
                        <th class="text-right">
                            Admin Commission <span class="small">(%)</span>
                        </th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                <tbody>                   
                    @foreach($rates as $rate)
                    <tr>
                        <td>
                            <a class="text-nowrap">{{$rate->vehicleType->title}}</a>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->base_fare}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->short_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->up_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->down_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->long_up_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->long_down_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">
                                @if($rate->surcharge_rate)
                                    Tk. {{$rate->surcharge_rate}}
                                @endif
                            </div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">
                                @if($rate->discount_percent)
                                    {{$rate->discount_percent}}%
                                @endif
                            </div>
                        </td>  
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">
                                @if(isset($rate->from_discount_date))
                                    From: {{$rate->from_discount_date->format('d/m/Y h:ia')}}
                                @else
                                    &nbsp;
                                @endif
                                <br>
                                @if(isset($rate->to_discount_date))
                                    To: {{$rate->to_discount_date->format('d/m/Y h:ia')}}
                                @else
                                    &nbsp;
                                @endif
                            </div>
                        </td>                      
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">{{$rate->admin_commission}}%</div>
                        </td>
                        <td class="text-right">
                            <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                <li>
                                    <a href="/cms/price-manager/full-booking/{{$rate->id}}/edit" class="btn btn-gray btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">Edit</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop