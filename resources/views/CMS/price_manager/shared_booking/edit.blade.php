@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="up_trip_rate">Edit Rate</h1>
            <div class="up_trip_rate-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/price-manager/shared-booking/{{$item->id}}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="row">
                        <div class="col-md-9 col-lg-9">

                             <div class="form-group row @if($errors->first('min_weight')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Min Weight</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="min_weight" value="{{old('min_weight', $item->min_weight)}}" id="min_weight" name="min_weight">
                                    <div class="form-control-feedback">@if($errors->first('min_weight')!=null) {{ $errors->first('min_weight')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('max_weight')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Max Weight</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="max_weight" value="{{old('max_weight', $item->max_weight)}}" id="max_weight" name="max_weight">
                                    <div class="form-control-feedback">@if($errors->first('max_weight')!=null) {{ $errors->first('max_weight')}} @endif</div>
                                </div>
                            </div>

                            <hr />

                            <div class="form-group row @if($errors->first('base_fare')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Base Fare</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="base_fare" value="{{old('base_fare', $item->base_fare)}}" id="base_fare" name="base_fare">
                                    <div class="form-control-feedback">@if($errors->first('base_fare')!=null) {{ $errors->first('base_fare')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('pickup_charge')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Pick-up Charge</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="pickup_charge" value="{{old('pickup_charge', $item->pickup_charge)}}" id="pickup_charge" name="pickup_charge">
                                    <div class="form-control-feedback">@if($errors->first('pickup_charge')!=null) {{ $errors->first('pickup_charge')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('drop_off_charge')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Drop-off Charge</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="drop_off_charge" value="{{old('drop_off_charge', $item->drop_off_charge)}}" id="drop_off_charge" name="drop_off_charge">
                                    <div class="form-control-feedback">@if($errors->first('drop_off_charge')!=null) {{ $errors->first('drop_off_charge')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('weight_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Weight Rate</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="weight_rate" value="{{old('weight_rate', $item->weight_rate)}}" id="weight_rate" name="weight_rate">
                                    <div class="form-control-feedback">@if($errors->first('weight_rate')!=null) {{ $errors->first('weight_rate')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('shortest_trip_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Shortest Distance Rate (/km)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="shortest_trip_rate" value="{{old('shortest_trip_rate', $item->shortest_trip_rate)}}" id="shortest_trip_rate" name="shortest_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('shortest_trip_rate')!=null) {{ $errors->first('shortest_trip_rate')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('short_trip_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Short Distance Rate (/km)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="short_trip_rate" value="{{old('short_trip_rate', $item->short_trip_rate)}}" id="short_trip_rate" name="short_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('short_trip_rate')!=null) {{ $errors->first('short_trip_rate')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('normal_trip_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Normal Distance Rate (/km)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="normal_trip_rate" value="{{old('normal_trip_rate', $item->normal_trip_rate)}}" id="normal_trip_rate" name="normal_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('normal_trip_rate')!=null) {{ $errors->first('normal_trip_rate')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('long_trip_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Long Distance (/km)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="long_trip_rate" value="{{old('long_trip_rate', $item->long_trip_rate)}}" id="long_trip_rate" name="long_trip_rate">
                                    <div class="form-control-feedback">@if($errors->first('long_trip_rate')!=null) {{ $errors->first('long_trip_rate')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('surcharge_rate')!=null) has-danger @endif">
                                <label for="surcharge_rate" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Surcharge (/km)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="surcharge_rate" value="{{old('surcharge_rate', $item->surcharge_rate)}}" id="surcharge_rate" name="surcharge_rate">
                                    <div class="form-control-feedback">@if($errors->first('surcharge_rate')!=null) {{ $errors->first('surcharge_rate')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('discount_percent')!=null) has-danger @endif">
                                <label for="discount_percent" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Discount (%)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="discount_percent" value="{{old('discount_percent', $item->discount_percent)}}" id="discount_percent" name="discount_percent">
                                    <div class="form-control-feedback">@if($errors->first('discount_percent')!=null) {{ $errors->first('discount_percent')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row @if($errors->first('admin_commission')!=null) has-danger @endif">
                                <label for="admin_commission" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Admin Commission (%)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="admin_commission" value="{{old('admin_commission', $item->admin_commission)}}" id="admin_commission" name="admin_commission">
                                    <div class="form-control-feedback">@if($errors->first('admin_commission')!=null) {{ $errors->first('admin_commission')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop