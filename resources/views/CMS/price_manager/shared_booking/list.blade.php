@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Delivery Price Chart (Shared)</h1>
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/cms/price-manager/shared-booking/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add Rate</a>
                </li>                 
            </ul>
        </div>
    </div>   
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="text-right">
        
        </div>
        <div class="table-responsive">
            <table  class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Weight Range (kg)</th>
                        <th>Base Fare</th>
                        <th class="text-right text-success">
                            Pick-up Charge
                        </th>
                        <th class="text-right text-success">
                            Drop-off Charge
                        </th>
                        <th class="text-right text-success">
                            Weight Rate
                        </th>
                        <th class="text-right text-warning">
                            Shortest Distance <span class="small">(<3km)</span>
                        </th>
                        <th class="text-right text-warning">
                            Short Distance <span class="small"> (<15km)</span>
                        </th>
                        <th class="text-right text-warning">
                            Normal Distance <span class="small">(15km - 400km)</span>
                        </th>
                        <th class="text-right text-warning">
                            Long Distance <span class="small">(400km+)</span>
                        </th>
                        <th class="text-right">
                            Surcharge <span class="small">(/km)</span>
                        </th>
                        <th class="text-right">
                            Discounts <span class="small">(%)</span>
                        </th>
                        <th class="text-right">
                            Admin Commission <span class="small">(%)</span>
                        </th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                <tbody>                   
                    @foreach($rates as $rate)
                    <tr>
                        <td>
                            <a class="text-nowrap">{{$rate->min_weight}} - {{$rate->max_weight}} kg</a>
                        </td>
                        <td>
                            <a class="text-nowrap">Tk. {{$rate->base_fare}}</a>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->pickup_charge}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->drop_off_charge}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->weight_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->shortest_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->short_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->normal_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">Tk. {{$rate->long_trip_rate}}</div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">
                                @if($rate->surcharge_rate)
                                    Tk. {{$rate->surcharge_rate}}
                                @endif
                            </div>
                        </td>
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">
                                @if($rate->discount_percent)
                                    {{$rate->discount_percent}}%
                                @endif
                            </div>
                        </td>                        
                        <td class="text-right">
                            <div class="text-nowrap text-lighter text-sm">{{$rate->admin_commission}}%</div>
                        </td>
                        <td class="text-right">
                            <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                <li>
                                    <a href="/cms/price-manager/shared-booking/{{$rate->id}}/edit" class="btn btn-gray btn-round btn-sm px-4 py-2 text-uppercase font-weight-semibold letter-spacing-1">Edit</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop