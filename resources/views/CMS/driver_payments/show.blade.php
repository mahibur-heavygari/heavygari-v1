@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h2 class="title">{{$driver_commission->driver->user->name}}</h2>
            <div class="title-sub">
                Showing {{count($commission_items)}} trips' bonuses
            </div> 
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    @if($driver_commission->status=='unpaid')
                        <button type="submit" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" style="margin-top: 20px;" data-toggle="modal" data-target="#myModal">Pay {{$driver_commission->total_amount}} Taka</button>
                        <form action="/cms/payment/drivers/commissions/save" method="POST" enctype="multipart/form-data">
                          <input type="hidden" name="driver_commission_id" value="{{$driver_commission->id}}">
                           {{csrf_field()}}
                            <div id="myModal" class="modal fade" role="dialog">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                     <h4 class="modal-title" style="text-align: left;">Payment</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control form-control-lg datepick-only" type="text" value="" id="ex-field-datetime" name="paid_at" placeholder="Payment Date" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Payment Method</label>
                                        <select class="form-control custom-select" name="payment_method" required="">
                                            <option value="">Select One</option>
                                            <option value="cash">Cash</option>
                                            <option value="bkash">Bkash</option>
                                            <option value="rocket">Rocket</option>
                                            <option value="bank">Bank</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input type="text" name="amount" class="form-control" value="{{$driver_commission->total_amount}}" readonly="" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Account Number</label>
                                        <input type="text" name="account_number" class="form-control">
                                    </div>
                                    <div class="form-check mb-4 checking-placement">
                                        <label class="form-check-label">
                                            <input id="alreadyPaid" type="checkbox" class="">
                                            <span class="px-1 text-uppercase letter-spacing-1" style="font-size: 0.689rem !important;">Confirm?</span>
                                        </label>
                                    </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" id="submit" disabled="">Submit</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                   </div>
                                     
                                  </div>
                                </div>
                            </div>
                          </div>
                         </form>
                    @elseif($driver_commission->status=='paid')
                        <button class="btn btn-success btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" id="submit"><i class="icofont icofont-tick-mark"></i> Paid</button>
                    @elseif($driver_commission->status=='due')
                        <button class="btn btn-danger btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update"><i class="icofont icofont-warning"></i> Owner did not pay bill</button>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
    @include('/common/payments/commissions')
@stop

@section('footer')
  <script>
    function ConfirmDelete()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
$(document).ready(function(){
  $('#alreadyPaid').click(function() {
  if ($(this).is(':checked')) {
    $("#submit").prop('disabled', false);
  }
}) 
  $('#alreadyPaid').click(function() {
  if (!$(this).is(':checked')) {
    $("#submit").prop('disabled', true);
  }
})
  
});

   
                               
  </script>
@stop