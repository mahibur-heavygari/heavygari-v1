@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Driver Payments</h1>
            <div class="title-sub">
                Showing {{count($list)}} payments
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
</div>
@stop

@section('content-body')
    @include('/common/payments/driver_payment_list')
@stop