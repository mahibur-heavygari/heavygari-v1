@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Add Point</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">1. Basic Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/base/points">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">                        
                            <div class="form-group row @if($errors->first('title')!=null) has-danger @endif">
                                <label for="title" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Title</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('title')}}" id="title" name="title">
                                    <div class="form-control-feedback">@if($errors->first('title')!=null) {{ $errors->first('title')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('fullname')!=null) has-danger @endif">
                                <label for="fullname" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Full Name</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="fullname" value="{{old('fullname')}}" id="fullname" name="fullname">
                                    <div class="form-control-feedback">@if($errors->first('fullname')!=null) {{ $errors->first('fullname')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('lat')!=null) has-danger @endif">
                                <label for="lat" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Latitude</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="lat" value="{{old('lat')}}" id="lat" name="lat">
                                    <div class="form-control-feedback">@if($errors->first('lat')!=null) {{ $errors->first('lat')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('lon')!=null) has-danger @endif">
                                <label for="lon" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Longitude</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="lon" value="{{old('lon')}}" id="lon" name="lon">
                                    <div class="form-control-feedback">@if($errors->first('lon')!=null) {{ $errors->first('lon')}} @endif</div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop