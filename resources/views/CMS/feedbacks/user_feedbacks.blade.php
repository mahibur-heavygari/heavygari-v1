@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Feedbacks</h1>
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link active" href="#">User Feedbacks</a>
        </li>        
    </ul>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="">
            @foreach($feedbacks as $feedback)
                <div class="feedback-item">
                    <div class="bubble speech-bubble">
                        <span>{{$feedback->message}} </span>
                    </div>  
                    <ul class="feadback-meta">
                        <li>
                            <span>{{date('g:ia d/m/y', strtotime($feedback->created_at))}} </span>
                        </li>
                        <li>
                            @if($feedback->user_type=='customer')
                                <a href="/cms/users/customers/{{$feedback->user->customerProfile->id}}">{{$feedback->user->name}}</a>
                            @elseif($feedback->user_type=='owner')
                                <a href="/cms/users/owners/{{$feedback->user->ownerProfile->id}}">{{$feedback->user->name}}</a>
                            @elseif($feedback->user_type=='driver')
                                <a href="/cms/users/drivers/{{$feedback->user->driverProfile->id}}">{{$feedback->user->name}}</a>
                            @endif
                        </li>
                    </ul>
                </div>
            @endforeach
            <div class="pagination">            
                {{ $feedbacks->links("pagination::bootstrap-4") }}
            </div>
        </div>
    </div>
</div>
@stop