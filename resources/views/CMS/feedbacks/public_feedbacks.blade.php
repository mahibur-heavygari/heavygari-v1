@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Feedbacks</h1>
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link active" href="#">Public Feedbacks</a>
        </li>        
    </ul>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div>            
            <div class="">
                @foreach($feedbacks as $feedback)
                    <div class="feedback-item">
                        <div class="bubble speech-bubble">
                            <span> {{$feedback->message}} </span>
                        </div>  
                        <ul class="feadback-meta">
                            <li>
                                <span>{{date('g:ia d/m/y', strtotime($feedback->created_at))}}</span>
                            </li>
                            <li>
                                <i class="fa fa-comment"></i>
                                <span>{{$feedback->name}}</span>
                            </li>
                            <li>
                                <i class="fa fa-envelope-open"></i>
                                <span>{{$feedback->email}}</span>
                            </li>
                            <li>
                                <i class="fa fa-mobile"></i>
                                <span>{{$feedback->phone}}</span>
                            </li> 
                        </ul>
                    </div>           
                @endforeach               
            </div>                
            {{ $feedbacks->links("pagination::bootstrap-4") }}
        </div>
    </div>
</div>
@stop