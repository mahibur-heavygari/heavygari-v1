@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Ratings<br></h1>
        </div>    
    </div>
</div>
@stop


@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       
        <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0">
                <thead >
                     <tr>
                        <th class="text-center"><span class="text-lighter">Date</span></th>
                        <th class="text-center" >
                            <span class="text-lighter">Booking Id</span>
                        </th>                        
                      
                        <th class="text-center">
                            <span class="text-lighter">Driver ID
                            </span>
                        </th> 
                        <th class="text-center text-lighter" colspan="2">Customer's Review & Rating
                        </th> 
                        <th class="text-center">
                            <span class="text-lighter">Customer ID
                            </span>
                        </th>
                       
                        <th class="text-center text-lighter" colspan="2">Driver's Review & Rating
                        </th>                        
                     
                        
                    </tr>
                     <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-center"> <span class="text-lighter">Customer Rating
                            </span></td>
                    <td class="text-center"> <span class="text-lighter">Customer Review
                            </span></td>
                    <td></td>
                    <td class="text-center"> <span class="text-lighter">Driver Rating
                            </span></td>
                   <td class="text-center"> <span class="text-lighter">Driver Review
                            </span></td>
                   
                </tr> 
                   
                </thead>
                <tbody>   
                 

                @foreach($results as $result)
             
                <tr>
                    
                  <td class="text-center">{{$result['cdate'] or ''}} </td>
                  <td class="text-center">{{$result['booking_id'] or ''}} </td>
                  <td class="text-center"><a href="/cms/users/drivers/{{$result['driverprofile'] or ''}}" >{{$result['driverprofile'] or ''}}</a> </td>
                  <td class="text-center">{{$result['customerrating'] or ''}}</td>
                  <td class="text-center">{{$result['customer_comment'] or ''}}</td>
                  <td class="text-center"><a href="/cms/users/customers/{{$result['customerprofile'] or ''}}">{{$result['customerprofile'] or ''}}</a></td>
                  <td class="text-center">{{$result['driverrating'] or ''}}</td>
                  <td class="text-center">{{$result['driver_comment'] or ''}}</td>
                  
                </tr>  
         
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop