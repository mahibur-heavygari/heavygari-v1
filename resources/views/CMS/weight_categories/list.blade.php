@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
    <div class="page-header-content">
        <div class="page-header-meta">
            <div class="page-header-cell">
                <h1 class="title">Weight Category</h1>

                <div class="title-sub">
                    Showing {{count($base_weight_categories)}} Weight Categories
                </div>
            </div>
            <div class="page-header-cell">
                <ul class="page-header-meta-list">
                    <li>
                        <a href="/cms/base/weight_categories/create"
                           class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i
                                    class="fa fa-plus"></i> Add Weight Category</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop

@section('content-body')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
                <table id="example" class="table" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>title Weight</th>
                        <th>Actual Weight</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($base_weight_categories as $base_weight_category)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$base_weight_category->title}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$base_weight_category->weight}}</div>
                            </td>
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                    <li>
                                        <a href="/cms/base/weight_categories/{{$base_weight_category->id}}/edit"
                                           title="Edit"
                                           class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form class="" method="POST" enctype='multipart/form-data'
                                              action="/cms/base/weight_categories/{{$base_weight_category->id}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit"
                                                    class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmDelete();">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('footer')
  <script>
    function ConfirmDelete()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop