@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop
@php
    if(isset($appends['vehicle_type_id'])) 
        $vehicle_type_id = $appends['vehicle_type_id'];
    else
        $vehicle_type_id = "";
@endphp
@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Vehicles</h1>
            <div class="title-sub">
                Showing {{count($list)}} vehicles
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
    <ul class="nav nav-exit">
    </ul>
</div>
<div class="page-header-content">
    <div class="page-header-meta" style="margin-top: 32px;">
    <div class="page-header-cell"></div>
    <div class="page-header-cell">
        <form class="" method="GET" enctype='multipart/form-data' action="/cms/vehicles" name="vehicle_search">
            <ul class="page-header-meta-list">
            <li>
                <input name="address" id="address" type="text" value="@if(isset($appends['address'])){{$appends['address']}}@endif" class="form-control" placeholder="Address">
            </li>
            <li>
                <select id="vehicle_type_id" name="vehicle_type_id" class="custom-select" aria-describedby="addon-from">
                    <option value="">Select Vehicle Type</option>
                    @foreach($vehicle_types as $vehicle)
                        <option value="{{$vehicle->id}}">{{$vehicle->title}}</option>
                    @endforeach
                </select>
            </li>
            <input type="hidden" name="filter" value="{{ app('request')->input('filter') }}">
            <li>
                <button type="submit" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalmnth">Search</button>
            </li>
            </ul>
        </form>
    </div>
  </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Registration Date</th>
                        <th>Name</th>
                        <th>Vehicle Type</th>
                        <th>Registration Number</th>
                        <th>Fitness Number</th>
                        <th>Engine Capacity</th>
                        <th>Owner Name</th>
                        <th>Current Driver</th>
                        <th>Height</th>
                        <th>Length</th>
                        <th>Width</th>
                        <th>Capacity (custom)</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{date('d/m/Y', strtotime($item->created_at))}}</div>
                            </td>
                            <td>
                                <div class="me-feed">
                                    <a href="" class="avater">
                                        <img src="{{Storage::url($item->thumb_photo)}}" alt="...">
                                    </a>
                                    <div class="denote">
                                        <div class="title">{{$item->name}}</div>
                                        <div class="meta-note">{{$item->number_plate}}</div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->vehicleType->title}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->vehicle_registration_number}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->fitness_number}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->engine_capacity_cc}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm"><a href="/cms/users/owners/{{$item->owner->id}}">{{$item->owner->user->name}}</a></div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                @isset($item->currentDriver)
                                <a href="{{url('/cms/users/drivers/'.$item->currentDriver->id)}}">{{$item->currentDriver->user->name}}</a>
                                @endif
                                </div>
                            </td>  
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->height}}</div>
                            </td> 
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->length}}</div>
                            </td> 
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->width}}</div>
                            </td> 
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->custom_capacity}}</div>
                            </td>                       
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                    <li>
                                        <a href="/cms/vehicles/{{$item->id}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/vehicles/{{$item->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/vehicles/{{$item->id}}" title="Trips" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
@section('footer')
<script type="text/javascript">
    document.forms['vehicle_search'].elements['vehicle_type_id'].value=<?php echo $vehicle_type_id; ?>
</script>
@stop