@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Notification History<br></h1>
        </div>    
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       
        <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>  
                        <th class="text-center"><span class="text-lighter">Date & Time</span></th>  
                        <th class="text-center"><span class="text-lighter">Notification Title</span></th>   
                        <th class="text-center"><span class="text-lighter">Body</span></th>   
                        <th class="text-center"><span class="text-lighter">User Type</span></th>   
                        <th class="text-center"><span class="text-lighter">User</span></th>                    
                    </tr>
                </thead>
                <tbody>   
                    @foreach($all_notifications as $notification)
                    <tr>
                        <td class="text-center">@if(isset($notification->created_at)){{ $notification->created_at->toDayDateTimeString()}} @else &nbsp; @endif</td>
                        <td class="text-center">{{ $notification->title}}</td>
                        <td class="text-center">{{ $notification->body }}</td>
                        <td class="text-center">{{ $notification->type_user or ''}}</td>
                        <td class="text-center">{{ $notification->user}}</td>
                    </tr>

                    @endforeach
               
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop