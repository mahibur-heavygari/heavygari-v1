@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Push Notification</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<style type="text/css">
    .select2-selection--single {
        height: 45px !important;
    }
</style>
@stop
@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title">Select Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" action="/cms/send/push_notification" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">
                            <div class="form-group row user_type_div @if($errors->first('user_type')!=null) has-danger @endif">
                                <label for="user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Notification Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="user_type" name="user_type">
                                        <option value="">Select</option>
                                        <option value="customer">Send to Customer</option>
                                        <option value="driver">Send to Driver</option> 
                                        <option value="trafic">Traffic Updates</option>  
                                        <option value="market">Market Updates</option>                              
                                    </select>  
                                </div>
                            </div>
                            <div class="form-group row special_user_type @if($errors->first('special_user_type')!=null) has-danger @endif">
                                <label for="special_user_type" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">User Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="special_user_type" name="special_user_type">
                                        <option value="">Select One Option</option>
                                        <option value="customer">Customer</option>
                                        <option value="driver">Driver</option>
                                    </select>  
                                </div>
                            </div>

                            <div class="form-group row user_div @if($errors->first('user')!=null) has-danger @endif">
                                <label for="user" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">User</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control-lg custom-select width-100per" id="user" name="user">
                                        
                                    </select>  
                                </div>
                            </div>

                            <div class="form-group row user_name_div">
                                <label for="user_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Name</label>
                                <div class="col-md-12 col-lg-8 has-form-control-lg">
                                    <input class="form-control form-control-lg like-field input-phone" type="text" value="" id="user_name" name="user_name">
                                </div>
                            </div>

                             <div class="form-group row title_div @if($errors->first('title')!=null) has-danger @endif">
                                <label for="title" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Title</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field" type="text" id="title" name="title">
                                </div>
                            </div>
                            <div class="form-group row text_div
                            @if($errors->first('text')!=null) has-danger @endif">
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Text</label>
                                <div class="col-md-12 col-lg-8">
                                    <textarea class="form-control form-control-lg like-field" type="text" id="text" name="text"></textarea>
                                </div>
                            </div>

                            <div class="form-group row choose_trafic_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">North Bengal Traffic Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="north" name="north">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_trafic_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">South Bengal Traffic Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="south" name="south">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div> 
                            <div class="form-group row choose_trafic_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">East Bengal Traffic Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="east" name="east">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_trafic_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">West Bengal Traffic Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="west" name="west">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_trafic_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Dhaka City Traffic Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="dhaka" name="dhaka">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_trafic_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Chittagong City Traffic Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="ctg" name="ctg">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>




                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Dhaka Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="dhaka_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক </option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম</option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Chittagong Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="chittagong_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম</option>
                                </select> 

                             </div>
                            </div>

                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Rangpur Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="rangpur_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>

                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Rajshahi Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="rajshahi_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক </option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Sylhet Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="sylhet_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক </option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Khulna Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="khulna_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>


                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Barisal Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="barisal_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>

                            <div class="form-group row choose_market_div
                            @if($errors->first('choose_trafic')!=null) has-danger @endif">
                            <label for="choose_trafic" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Mymensingh Market Update</label>
                            <div class="col-md-12 col-lg-8">

                                <select class="form-control-lg custom-select width-100per" id="choose_market_div" name="mymensingh_market">
                                      <option value="">Select</option> <option value="normal">স্বাভাবিক</option>      
                                      <option value="more">একটু বাড়তি </option><option value="less">একটু কম </option>
                                </select> 

                             </div>
                            </div>
                            <div class="form-group row image_div @if($errors->first('image')!=null) has-danger @endif">     
                                <label for="text" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Image</label>
                                <div class="col-md-4 col-lg-3">
                                    <div class="upload-styled-image upload-styled-image-40p mb-2">
                                        <div class="uploaded-image uploaded-here"></div>
                                        <div class="input-file">
                                            <input class="file-input" type="file" name="image" id="image">
                                            <span class="upload-icon">
                                                <i class="icofont icofont-upload-alt"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script src="/js/select2.full.min.js"></script>
<script>
    var customers = <?php echo $customers; ?>;
    var drivers = <?php echo $drivers; ?>;
    $(document).ready(function(){
        $(".choose_trafic_div").hide();
        $(".division_div").hide();
        $(".choose_market_div").hide();
        $(".special_user_type").hide();
        $("#user_type" ).change(function() {
            $("#user").empty();
            $('#user').val('').trigger('change');
            var selected = $('#user_type :selected').val();
            var userArr = [];

            if(selected == 'customer'){
                userArr = customers;
                $(".user_div").show();
                $(".user_name_div").show();
                $(".title_div").show();
                $(".text_div").show();
                $(".image_div").show();
                $(".choose_trafic_div").hide();
                $(".division_div").hide();
                $(".choose_market_div").hide();
                $(".special_user_type").hide();
            }else if(selected == 'driver'){
                userArr = drivers;
                $(".user_div").show();
                $(".user_name_div").show();
                $(".title_div").show();
                $(".text_div").show();
                $(".image_div").show();
                $(".choose_trafic_div").hide();
                $(".division_div").hide();
                $(".choose_market_div").hide();
                $(".special_user_type").hide();
            }else if(selected == 'trafic'){
                $(".special_user_type").show();
                //$(document).ready(function(){
                $("#special_user_type").change(function() {
                    $('#user').val('').trigger('change');
                    $("#user").empty();
                    var user_type = $('#special_user_type :selected').val();
                    if(user_type == 'customer'){
                        userArr = customers;
                    }else if(user_type == 'driver'){
                        userArr = drivers;
                    }
                    var html1 = '<option value=""></option><option user_name="all" value="all">All</option>';
                    userArr.forEach(function(element){
                    html1 = html1 + "<option user_name='"+element.name+"' value='"+element.user_id+"'>"+element.phone+"</option>";
                });
                    $("#user").append(html1);
                    $(".user_div").show();
                    $(".user_name_div").show();
                    $(".title_div").hide();
                    $(".text_div").hide();
                    $(".image_div").hide();
                    $(".choose_trafic_div").show();
                    $(".division_div").hide();
                    $(".choose_market_div").hide();  
                }); 
                //});  
            }else if(selected == 'market'){
                userArr = customers;
                $(".user_div").show();
                $(".user_name_div").show();
                $(".title_div").hide();
                $(".text_div").hide();
                $(".image_div").hide();
                $(".choose_trafic_div").hide();
                $(".division_div").show();
                $(".choose_market_div").show();
                $(".special_user_type").hide();
            }
            if(selected == 'customer'){
                var html = '<option value=""></option><option user_name="all" value="all">All</option>';
                userArr.forEach(function(element){
                    html = html + "<option user_name='"+element.name+"' value='"+element.user_id+"'>"+element.phone+"</option>";
                });
                $("#user").append(html);
            }else if(selected == 'driver'){
               var html = '<option value=""></option><option user_name="all" value="all">All</option>';
                userArr.forEach(function(element){
                    html = html + "<option user_name='"+element.name+"' value='"+element.user_id+"'>"+element.phone+"</option>";
                });
                $("#user").append(html); 
            }else if(selected == 'market'){
                var html = '<option value=""></option><option user_name="all" value="all">All</option>';
                userArr.forEach(function(element){
                    html = html + "<option user_name='"+element.name+"' value='"+element.user_id+"'>"+element.phone+"</option>";
                });
                $("#user").append(html); 
            }
                
        });

        $('#user').select2().on('change', function (e) {
            var user_name = $('#user :selected').attr('user_name');
            if(user_name=='all'){
                $(".user_name_div").hide();
            }else{
                $(".user_name_div").show();
                $("#user_name").val(user_name);
            }
        });
    });

 
</script>
@stop