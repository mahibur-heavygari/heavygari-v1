@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Add Product Category</h1>
            <div class="title-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/base/product_categories">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-9 col-lg-9">                        
                            <div class="form-group row @if($errors->first('title')!=null) has-danger @endif">
                                <label for="title" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Product Category Name</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('title')}}" id="title" name="title">
                                    <div class="form-control-feedback">@if($errors->first('title')!=null) {{ $errors->first('title')}} @endif</div>
                                </div> 
                                 <label for="categoryType" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Product Category Type</label>
                                <div class="col-md-12 col-lg-8">
                                    <select class="form-control form-control-lg like-field form-control-danger" name="category" id="category">
                                        <option value="transport_type">Transport Type</option>
                                        <option value="ride_type">Ride Type</option>
                                    </select>
                                    <div class="form-control-feedback">@if($errors->first('title')!=null) {{ $errors->first('title')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop