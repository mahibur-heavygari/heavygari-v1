@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Potential Owners</h1>
            <div class="title-sub">
                Showing {{count($list)}} users
            </div>            
        </div>
        <div class="page-header-cell">
            <ul class="page-header-meta-list">
                <li>
                    <a href="/cms/users/potential_owners/create" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1"><i class="fa fa-plus"></i> Add Potential Owner</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Company Name</th>
                        <th>Owner Name</th>
                        <th>Owner Phone</th>
                        <th>Owner Email</th>
                        <th>Manager Name</th>
                        <th>Manager Phone</th>
                        <th>Location</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{date('d/m/Y', strtotime($item->created_at))}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->company_name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->owner_name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->owner_phone}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->owner_email}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->manager_name}}</div>
                            </td>  
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->manager_phone}}</div>
                            </td>  
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->location}}</div>
                            </td>  
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">                                    
                                    <li>
                                        <a href="/cms/users/potential_owners/{{$item->id}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>  
                                    <li>
                                        <a href="/cms/users/potential_owners/{{$item->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/users/potential_owners/{{$item->id}}/delete" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>                           
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('footer')
  <script>
    function ConfirmAction()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop