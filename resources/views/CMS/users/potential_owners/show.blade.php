@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Potential Owner Profile</h1>
            <div class="title-sub">
                {{$profile->owner_name}}
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
            <form class="" method="POST" enctype="multipart/form-data" action="/cms/users/potential_owners/1" >
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                
                <div class="wagon wagon-huge wagon-borderd-dashed rounded">
                    <div class="wagon-header">
                        <div class="wh-col">
                            <h4 class="wh-title">Personal Information</h4>
                        </div>
                        <div class="wh-col">
                            <div class="wh-meta">
                            </div>
                        </div>
                    </div>
                    <div class="wagon-body">
                        <div class="row">
                            <div class="col-md-9 col-lg-9">
                                <div class="form-group row @if($errors->first('company_name')!=null) has-danger @endif">
                                    <label for="company_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Company Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('company_name', $profile->company_name)}}" id="company_name" name="company_name" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('company_name')!=null) {{ $errors->first('company_name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('owner_name')!=null) has-danger @endif">
                                    <label for="owner_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Owner Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('owner_name', $profile->owner_name)}}" id="owner_name" name="owner_name" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('owner_name')!=null) {{ $errors->first('owner_name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('owner_phone')!=null) has-danger @endif">
                                    <label for="owner_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Owner Phone</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('owner_phone', $profile->owner_phone)}}" id="owner_phone" name="owner_phone" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('owner_phone')!=null) {{ $errors->first('owner_phone')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('owner_email')!=null) has-danger @endif">
                                    <label for="owner_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Owner Email</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('owner_email', $profile->owner_email)}}" id="owner_email" name="owner_email" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('owner_email')!=null) {{ $errors->first('owner_email')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('manager_name')!=null) has-danger @endif">
                                    <label for="manager_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Manager Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('manager_name', $profile->manager_name)}}" id="manager_name" name="manager_name" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('manager_name')!=null) {{ $errors->first('manager_name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('manager_phone')!=null) has-danger @endif">
                                    <label for="manager_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Manager Phone</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('manager_phone', $profile->manager_phone)}}" id="manager_phone" name="manager_phone" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('manager_phone')!=null) {{ $errors->first('manager_phone')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('location')!=null) has-danger @endif">
                                    <label for="location" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Location</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('location', $profile->location)}}" id="location" name="location" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('location')!=null) {{ $errors->first('location')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('vehicle_type')!=null) has-danger @endif">
                                    <label for="ex-cc-limit" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Vehicle Type</label>
                                    <div class="col-md-12 col-lg-8">
                                        <br>
                                        @foreach($vehicle_types as $vehcicle_type)
                                            <input class="checkbox form-group" type="checkbox" name="vehicle_type[]" value="{{$vehcicle_type->id}}" @if($profile->myVehicleTypes->contains($vehcicle_type->id)) checked @endif >&nbsp; {{$vehcicle_type->title}}
                                            <br>
                                        @endforeach
                                        <div class="form-control-feedback">@if($errors->first('vehicle_type')!=null) {{ $errors->first('vehicle_type')}} @endif</div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>  

            </form>
        </div>
    </div>
@stop

