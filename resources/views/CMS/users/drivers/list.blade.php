@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
@php
    if(isset($appends['vehicle_type_id'])) 
        $vehicle_type_id = $appends['vehicle_type_id'];
    else
        $vehicle_type_id = "";
@endphp
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Drivers</h1>
            <div class="title-sub">
                Showing {{count($list)}} users
            </div>            
        </div>
        
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link @if($fitler=='active') active @endif" href="/cms/users/drivers">Verified</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($fitler=='inactive') active @endif" href="/cms/users/drivers?filter=inactive">Inactive</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($fitler=='blocked') active @endif" href="/cms/users/drivers?filter=blocked">Blocked</a>
        </li>
    </ul>
</div>
<div class="page-header-content">
    <div class="page-header-meta" style="margin-top: 32px;">
    <div class="page-header-cell"></div>
    <div class="page-header-cell">
        <form class="" method="GET" enctype='multipart/form-data' action="/cms/users/drivers" name="driver_search">
            <ul class="page-header-meta-list">
            <li>

                <input name="address" id="address" type="text" value="@if(isset($appends['address'])){{$appends['address']}}@endif" class="form-control" placeholder="Address">
               
            </li>
            <li>
                
                <select id="vehicle_type_id" name="vehicle_type_id" class="custom-select" aria-describedby="addon-from">
                    <option value="">Select Vehicle Type</option>
                    
                    @foreach($vehicle_types as $vehicle)

                        <option value="{{$vehicle->id}}">{{$vehicle->title}}</option>
                    }
                    }
                    @endforeach
                </select>
            </li>
            <input type="hidden" name="filter" value="{{ app('request')->input('filter') }}">
            <li>
                <button type="submit" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalmnth">Search</button>
            </li>
            </ul>
        </form>
    </div>
  </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Registration Date</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <!-- <th>Email</th> -->
                        <th>National ID</th>
                        <th>Owner Name</th>
                        <th>Current Vehicle</th>
                        <th>Authorized Vehicles</th>
                        <th>SMS CODE</th>
                        <th>Status</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{date('d/m/Y', strtotime($item->created_at))}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->phone}}</div>
                            </td>
                            <!-- <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->email}}</div>
                            </td> -->
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->national_id}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm"><a href="/cms/users/owners/{{$item->myOwner->id}}">{{$item->myOwner->user->name}}</a></div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">@if(isset($item->currentVehicle)) {{$item->currentVehicle->vehicleType->title}} @else &nbsp; @endif</div>
                            </td>

                        
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                @foreach($item->myAuthorizedVehicles as $vehicle)
                                   <a href="{{url('cms/vehicles/'.$vehicle->id)}}"> {{$vehicle->number_plate}} ( {{$vehicle->vehicleType->title}} )</a> <br> 
                                @endforeach
                                </div>
                            </td>
                            <td>
                                @isset($item->user->activations[0])
                                <div class="text-nowrap text-lighter text-sm"><span class="test text-danger">{{$item->user->activations[0]->code}}</span></div>
                                @endif
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm"><span class="badge @if($item->user->status=='active') badge-success @elseif($item->user->status=='inactive') badge-warning @elseif($item->user->status=='blocked') badge-danger @endif ">{{$item->user->status}}</span></div>
                            </td>
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">                                    
                                    <li>
                                        <a href="/cms/users/drivers/{{$item->id}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/users/drivers/{{$item->id}}/edit" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>                                    
                                    <li>
                                        <a href="/cms/users/drivers/{{$item->id}}/transactions" title="Transactions" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="icofont icofont-money"></i>
                                        </a>
                                    </li>
                                    @if($item->user->status!='active')
                                        <li>
                                           <form method="POST" enctype='multipart/form-data' action="/cms/users/activate/{{$item->user->id}}">
                                            {{ csrf_field() }}
                                                <button type="submit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                                    <i class="fa fa-check-circle"></i>
                                                </button>
                                            </form>
                                        </li>
                                    @else 
                                        <li>
                                           <form method="POST" enctype='multipart/form-data' action="/cms/users/block/{{$item->user->id}}">
                                            {{ csrf_field() }}
                                                <button type="submit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                                    <i class="fa fa fa-ban"></i>
                                                </button>
                                            </form>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="#" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
@section('footer')

<script>
    document.forms['driver_search'].elements['vehicle_type_id'].value=<?php echo $vehicle_type_id; ?>;
</script>
@stop
