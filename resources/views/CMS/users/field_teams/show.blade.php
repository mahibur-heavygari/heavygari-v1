@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Field Team Profile</h1>
            <div class="title-sub">
                {{$profile->owner_name}}
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
            <form class="" method="POST" enctype="multipart/form-data" action="" >
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                
                <div class="wagon wagon-huge wagon-borderd-dashed rounded">
                    <div class="wagon-header">
                        <div class="wh-col">
                            <h4 class="wh-title">Personal Information</h4>
                        </div>
                        <div class="wh-col">
                            <div class="wh-meta">
                            </div>
                        </div>
                    </div>
                    <div class="wagon-body">
                        <div class="row">
                            <div class="col-md-9 col-lg-9">
                                <div class="form-group row @if($errors->first('name')!=null) has-danger @endif">
                                    <label for="name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('name', $profile->name)}}" id="name" name="name" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('name')!=null) {{ $errors->first('name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('phone')!=null) has-danger @endif">
                                    <label for="phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Phone</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('phone', $profile->phone)}}" id="phone" name="phone" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('owner_phone')!=null) {{ $errors->first('owner_phone')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('email')!=null) has-danger @endif">
                                    <label for="email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Email</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('email', $profile->email)}}" id="email" name="email" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('email')!=null) {{ $errors->first('email')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('national_id')!=null) has-danger @endif">
                                    <label for="national_id" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">National ID</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('national_id', $profile->national_id)}}" id="national_id" name="national_id" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('national_id')!=null) {{ $errors->first('national_id')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('address')!=null) has-danger @endif">
                                    <label for="address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Address</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('address', $profile->address)}}" id="address" name="address" readonly="">
                                        <div class="form-control-feedback">@if($errors->first('address')!=null) {{ $errors->first('address')}} @endif</div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>  

            </form>
        </div>
    </div>
@stop

