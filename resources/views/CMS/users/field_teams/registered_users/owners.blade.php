@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Owners</h1>
            <div class="title-sub">
                <span>registered by {{$admin['0']->user->name}}</span>
            </div>            
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>
                        <th class="text-center"><span class="text-lighter">Date & Time</span></th> 
                        <th class="text-center"><span class="text-lighter">Phone</span></th>    
                        <th class="text-center"><span class="text-lighter">Name</span></th>
                        <th class="text-center"><span class="text-lighter">Email</span></th>    
                        <th class="text-center"><span class="text-lighter">Image</span></th>      
                        <th class="text-center"><span class="text-lighter">Action</span></th>                   
                    </tr>
                </thead>
                <tbody>
                   @foreach($owners as $owner)
                    <tr>
                        <td class="text-center">{{$owner->created_at}}</td>
                        <td class="text-center">{{$owner->user->phone}}</td>
                        <td class="text-center"><a href="/cms/users/owners/{{$owner->id}}">{{$owner->user->name}}</a></td>
                        <td class="text-center">{{$owner->user->email}}</td>
                        <td class="text-center">
                            <a href="https://www.heavygari.com{{Storage::url($owner->user->image)}}" target="_blank">
                                <img src="https://www.heavygari.com{{Storage::url($owner->user->image)}}" width="100px" height="100px">
                            </a>
                        </td>
                        <td class="text-center"></td>
                    </tr>
                    @endforeach
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop