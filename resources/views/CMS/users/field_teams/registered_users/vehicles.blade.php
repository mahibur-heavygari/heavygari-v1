@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop



@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Vehicles</h1>
            <div class="title-sub">
                <span>registered by {{$admin['0']->user->name}}</span>
            </div>            
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12">
       <div class="table-responsive">
            <table  class="table table-bordered table-striped" width="100%" cellspacing="0" id="booking-table">
                <thead >
                    <tr>
                        <th class="text-center"><span class="text-lighter">Registration Date</span></th> 
                        <th class="text-center"><span class="text-lighter">Name</span></th>    
                        <th class="text-center"><span class="text-lighter">Vehicle Type</span></th>
                        <th class="text-center"><span class="text-lighter">Registration Number</span></th>    
                        <th class="text-center"><span class="text-lighter">Owner Name</span></th>      
                        <th class="text-center"><span class="text-lighter">Height</span></th>
                        <th class="text-center"><span class="text-lighter">Action</span></th>                   
                    </tr>
                </thead>
                <tbody>
                   @foreach($vehicles as $vehicle)
                    <tr>
                        <td class="text-center">{{$vehicle->created_at}}</td>
                        <td class="text-center">{{$vehicle->name}}</td>
                        <td class="text-center">{{$vehicle->vehicleType->title}}</td>
                        <td class="text-center">{{$vehicle->vehicle_registration_number}}</td>
                        <td class="text-center">{{$vehicle->owner->user->name}}</td>
                        <td class="text-center">{{$vehicle->height}}</td>
                        <td class="text-center"></td>
                    </tr>
                    @endforeach
                </tbody>
               
            </table>

        </div>
       
    </div>
</div>
@stop