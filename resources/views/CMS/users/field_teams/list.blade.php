@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Field Teams</h1>
            <div class="title-sub">
                Showing {{count($users)}}  users
            </div>            
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Field Admin Phone</th>
                        <th>Field Admin Name</th>
                        <th>Customer Registration</th>
                        <th>Owner Registration</th>
                        <th>Driver Registration</th>
                        <th>Vehicle Registration</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$user->user->created_at}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$user->user->phone}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$user->user->name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                    <a href="/cms/users/field_team/{{$user->id}}/customers" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">{{count($user->customersByMe)}}</a>
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                    <a href="/cms/users/field_team/{{$user->id}}/owners" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">{{count($user->ownersByMe)}}</a>
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                    <a href="/cms/users/field_team/{{$user->id}}/drivers" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">{{count($user->driversByMe)}}</a>
                                </div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">
                                    <a href="/cms/users/field_team/{{$user->id}}/vehicles" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">{{count($user->vehiclesByMe)}}
                                    </a>
                                </div>
                            </td>
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">                                    
                                    <li>
                                        <a href="/cms/users/field_team/{{$user->user->id}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>  
                                    <li>
                                        <a href="/cms/users/field_team/{{$user->user->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/users/field_team/{{$user->user->id}}/delete" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1" onclick="return ConfirmAction();">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>                           
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('footer')
  <script>
    function ConfirmAction()
    {
      var delCheck = confirm("Do you really want to proceed this action?");
      if(delCheck)
        return true;
      else
        return false;
    }
  </script>
@stop