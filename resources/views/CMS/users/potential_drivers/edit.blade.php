@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Potential Driver Profile</h1>
            <div class="title-sub">
                {{$profile->driver_name}}
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
            <form class="" method="POST" enctype="multipart/form-data" action="/cms/users/potential_drivers/{{$profile->id}}" >
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                
                <div class="wagon wagon-huge wagon-borderd-dashed rounded">
                    <div class="wagon-header">
                        <div class="wh-col">
                            <h4 class="wh-title">Personal Information</h4>
                        </div>
                        <div class="wh-col">
                            <div class="wh-meta">
                            </div>
                        </div>
                    </div>
                    <div class="wagon-body">
                        <div class="row">
                            <div class="col-md-9 col-lg-9">
                                <div class="form-group row @if($errors->first('driver_name')!=null) has-danger @endif">
                                    <label for="driver_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Driver Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('driver_name', $profile->driver_name)}}" id="driver_name" name="driver_name">
                                        <div class="form-control-feedback">@if($errors->first('driver_name')!=null) {{ $errors->first('driver_name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('driver_phone')!=null) has-danger @endif">
                                    <label for="driver_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Driver Phone</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('driver_phone', $profile->driver_phone)}}" id="driver_phone" name="driver_phone">
                                        <div class="form-control-feedback">@if($errors->first('driver_phone')!=null) {{ $errors->first('driver_phone')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('driver_email')!=null) has-danger @endif">
                                    <label for="driver_email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Driver Email</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('driver_email', $profile->driver_email)}}" id="driver_email" name="driver_email">
                                        <div class="form-control-feedback">@if($errors->first('driver_email')!=null) {{ $errors->first('driver_email')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('driver_address')!=null) has-danger @endif">
                                    <label for="driver_address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Driver Address</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('driver_address', $profile->driver_address)}}" id="driver_address" name="driver_address">
                                        <div class="form-control-feedback">@if($errors->first('driver_address')!=null) {{ $errors->first('driver_address')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('driver_area')!=null) has-danger @endif">
                                    <label for="driver_area" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Driver Area</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('driver_area', $profile->driver_area)}}" id="driver_area" name="driver_area">
                                        <div class="form-control-feedback">@if($errors->first('driver_area')!=null) {{ $errors->first('driver_area')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-12 col-lg-8 offset-lg-4">
                                        <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

            </form>
        </div>
    </div>
@stop

