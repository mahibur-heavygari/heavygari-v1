@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Potential Customer Profile</h1>
            <div class="title-sub">
                {{$profile->customer_name}}
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
            <form class="" method="POST" enctype="multipart/form-data" action="/cms/users/potential_customers/{{$profile->id}}" >
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                
                <div class="wagon wagon-huge wagon-borderd-dashed rounded">
                    <div class="wagon-header">
                        <div class="wh-col">
                            <h4 class="wh-title">Personal Information</h4>
                        </div>
                        <div class="wh-col">
                            <div class="wh-meta">
                            </div>
                        </div>
                    </div>
                    <div class="wagon-body">
                        <div class="row">
                            <div class="col-md-9 col-lg-9">
                                <div class="form-group row @if($errors->first('customer_name')!=null) has-danger @endif">
                                    <label for="customer_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Customer Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('customer_name', $profile->customer_name)}}" id="customer_name" name="customer_name">
                                        <div class="form-control-feedback">@if($errors->first('customer_name')!=null) {{ $errors->first('customer_name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('company_name')!=null) has-danger @endif">
                                    <label for="company_name" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Company Name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('company_name', $profile->company_name)}}" id="company_name" name="company_name">
                                        <div class="form-control-feedback">@if($errors->first('company_name')!=null) {{ $errors->first('company_name')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('customer_phone')!=null) has-danger @endif">
                                    <label for="customer_phone" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Customer Phone</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('customer_phone', $profile->customer_phone)}}" id="customer_phone" name="customer_phone">
                                        <div class="form-control-feedback">@if($errors->first('customer_phone')!=null) {{ $errors->first('customer_phone')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('customer_email')!=null) has-danger @endif">
                                    <label for="customer_email" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Customer Email</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('customer_email', $profile->customer_email)}}" id="customer_email" name="customer_email">
                                        <div class="form-control-feedback">@if($errors->first('customer_email')!=null) {{ $errors->first('customer_email')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('customer_address')!=null) has-danger @endif">
                                    <label for="customer_address" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Customer Address</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('customer_address', $profile->customer_address)}}" id="customer_address" name="customer_address">
                                        <div class="form-control-feedback">@if($errors->first('customer_address')!=null) {{ $errors->first('customer_address')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row @if($errors->first('customer_area')!=null) has-danger @endif">
                                    <label for="customer_area" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Customer Area</label>
                                    <div class="col-md-12 col-lg-8">
                                        <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('customer_area', $profile->customer_area)}}" id="customer_area" name="customer_area">
                                        <div class="form-control-feedback">@if($errors->first('customer_area')!=null) {{ $errors->first('customer_area')}} @endif</div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-12 col-lg-8 offset-lg-4">
                                        <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

            </form>
        </div>
    </div>
@stop

