@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')

@php
    if(isset($appends['vehicle_type_id'])) 
        $vehicle_type_id = $appends['vehicle_type_id'];
    else
        $vehicle_type_id = "";
@endphp

<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Owners</h1>
            <div class="title-sub">
                Showing {{count($list)}} users
            </div>            
        </div>
        <div class="page-header-cell">
            
        </div>
    </div>
    <ul class="nav nav-exit">
        <li class="nav-item">
            <a class="nav-link @if($fitler=='active') active @endif" href="/cms/users/owners">Verified</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($fitler=='inactive') active @endif" href="/cms/users/owners?filter=inactive">Inactive</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($fitler=='blocked') active @endif" href="/cms/users/owners?filter=blocked">Blocked</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($fitler=='withoutapp') active @endif" href="/cms/users/owners?filter=withoutapp">Without Mobile App</a>
        </li>
    </ul>
</div>
<div class="page-header-content">
    <div class="page-header-meta" style="margin-top: 32px;">
    <div class="page-header-cell"></div>
    <div class="page-header-cell">
        <form class="" method="GET" enctype='multipart/form-data' action="/cms/users/owners" name="owner_search">
            <ul class="page-header-meta-list">
            <li>
                <input name="address" id="address" type="text" value="@if(isset($appends['address'])){{$appends['address']}}@endif" class="form-control" placeholder="Address">
            </li>
            <li>
                <select id="vehicle_type_id" name="vehicle_type_id" class="custom-select" aria-describedby="addon-from">
                    <option value="">Select Vehicle Type</option>
                    @foreach($vehicle_types as $vehicle)
                        <option value="{{$vehicle->id}}">{{$vehicle->title}}</option>
                    @endforeach
                </select>
            </li>
            <input type="hidden" name="filter" value="{{ app('request')->input('filter') }}">
            <li>
                <button type="submit" class="btn btn-primary btn-round px-4 text-uppercase fs-14 font-weight-semibold letter-spacing-1 menu-button-update" data-toggle="modal" data-target="#mymodalmnth">Search</button>
            </li>
            </ul>
        </form>
    </div>
  </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Registration Date</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Manager Name</th>
                        <th>Address</th>
                        <th>Vehicles</th>
                        <th>Company Name</th>
                        <th>SMS CODE</th>
                        <!--<th>Status</th>-->
                        <th>Mobile App?</th>
                        <th class="text-right">Actions</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{date('d/m/Y', strtotime($item->created_at))}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->name}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->phone}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->user->email}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">@if(is_null($item->manager_name) || $item->manager_name=='null' || $item->manager_name=='NULL') &nbsp; @else {{$item->manager_name}}<br>({{$item->manager_phone or ''}})@endif</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{!! str_replace(',', ',<br>', $item->user->address) !!}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{!! $item->vehicle_info !!}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->company_name}}</div>
                            </td>
                            <!--<td>
                                <div class="text-nowrap text-lighter text-sm"><span class="badge @if($item->user->status=='active') badge-success @elseif($item->user->status=='inactive') badge-warning @elseif($item->user->status=='blocked') badge-danger @endif ">{{$item->user->status}}</span></div>
                            </td>-->
                            <td>
                                @isset($item->user->activations[0])
                                <div class="text-nowrap text-lighter text-sm"><span class="test text-danger">{{$item->user->activations[0]->code}}</span></div>
                                @endif
                            </td>
                            <td>

                                @if($item->without_app=='yes') 
                                    No
                                    <a href="/cms/users/owners/change_mobile_app_status/{{$item->id}}?status=no" type="submit" href="" class="btn  btn-success"><i class="fa fa-check-circle"></i></a>
                                @else 
                                    Yes
                                    <a href="/cms/users/owners/change_mobile_app_status/{{$item->id}}?status=yes" type="submit" href="" class="btn  btn-warning"><i class="fa fa fa-ban"></i></a>
                                @endif
                                                                
                            </td>
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">             
                                    <li>
                                        <a href="/cms/users/owners/{{$item->id}}" title="View" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/users/owners/{{$item->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cms/users/owners/{{$item->id}}/transactions" title="Transactions" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="icofont icofont-money"></i>
                                        </a>
                                    </li>
                                    @if($item->user->status!='active')
                                        <li>
                                           <form method="POST" enctype='multipart/form-data' action="/cms/users/activate/{{$item->user->id}}">
                                            {{ csrf_field() }}
                                                <button type="submit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                                    <i class="fa fa-check-circle"></i>
                                                </button>
                                            </form>
                                        </li>
                                    @else 
                                        <li>
                                           <form method="POST" enctype='multipart/form-data' action="/cms/users/block/{{$item->user->id}}">
                                            {{ csrf_field() }}
                                                <button type="submit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                                    <i class="fa fa fa-ban"></i>
                                                </button>
                                            </form>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="#" title="Delete" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <input type="text" name="owner_note_{{$item->id}}" value="{{$item->note}}">
                                <button class="save-note btn btn-warning" data-item-id="{{$item->id}}">Save</button> <span class="saved_success" style="color:green;
                                display: none;">saved</span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@stop

@section('footer')

<script>
    document.forms['owner_search'].elements['vehicle_type_id'].value=<?php echo $vehicle_type_id; ?>

    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $( "#example" ).on( "click", ".save-note", function() {
            let itemId = $(this).attr('data-item-id');
            let name = 'owner_note_'+itemId;
            let note = $("input[name="+name+"]").val();
            let ulr_str = '/cms/users/owners/'+itemId+'/save-note';
            var _this = $(this);
            
            $.ajax({
                data: {note: note},
                url: ulr_str,
                type: 'POST',
                complete: function (response) {
                    var result = jQuery.parseJSON(response.responseText);
                    if (result.success) {
                        _this.siblings().show();
                    }
                }
            });
        });
    });
</script>
@stop