@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title">Features</h1>
            <div class="title-sub">
                Showing {{count($list)}} features
            </div>            
        </div>
        <div class="page-header-cell">            
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12">
        <div class="table-default has-datatable table-large table-responsive table-round table-td-vmiddle">
            <table id="example" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>English Title</th>
                        <th>Bangla Title</th>  
                        <th>Comming Soon?</th>                     
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td>
                                <div class="text-nowrap text-lighter text-sm"><img width="48px" src="{{Storage::url($item->icon)}}" /></div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->title_en}}</div>
                            </td>
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->title_bn}}</div>
                            </td>  
                            <td>
                                <div class="text-nowrap text-lighter text-sm">{{$item->comming_soon}}</div>
                            </td>                            
                            <td class="text-right">
                                <ul class="list-unstyled d-flex flex-nowrap justify-content-end list-actions">
                                    <li>
                                        <a href="/cms/website/features/{{$item->id}}/edit" title="Edit" class="btn btn-gray rounded-circle btn-only-icon text-uppercase font-weight-semibold letter-spacing-1">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop