@extends('dashboards.layout')

@section('menu')
    @include('CMS.common.sidemenu')
@stop

@section('content-header')
<div class="page-header-content">
    <div class="page-header-meta">
        <div class="page-header-cell">
            <h1 class="title_en">Edit Feature</h1>
            <div class="title_en-sub">
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content-body')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xxl-8 offset-xxl-2">
        <div class="wagon wagon-huge wagon-borderd-dashed rounded">
            <div class="wagon-header">
                <div class="wh-col">
                    <h4 class="wh-title_en">1. Basic Information</h4>
                </div>
            </div>
            <div class="wagon-body">
                <form class="" method="POST" enctype='multipart/form-data' action="/cms/website/features/{{$item->id}}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="row">
                        <div class="col-md-9 col-lg-9">                        
                            <div class="form-group row @if($errors->first('title_en')!=null) has-danger @endif">
                                <label for="title_en" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Title (English)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('title_en', $item->title_en)}}" id="title_en" name="title_en">
                                    <div class="form-control-feedback">@if($errors->first('title_en')!=null) {{ $errors->first('title_en')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('title_bn')!=null) has-danger @endif">
                                <label for="title_bn" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Title (Bangla)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('title_bn', $item->title_bn)}}" id="title_bn" name="title_bn">
                                    <div class="form-control-feedback">@if($errors->first('title_bn')!=null) {{ $errors->first('title_bn')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('text_en')!=null) has-danger @endif">
                                <label for="text_en" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Text (English)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('text_en', $item->text_en)}}" id="text_en" name="text_en">
                                    <div class="form-control-feedback">@if($errors->first('text_en')!=null) {{ $errors->first('text_en')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('text_bn')!=null) has-danger @endif">
                                <label for="text_bn" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Text (Bangla)</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('text_bn', $item->text_bn)}}" id="text_bn" name="text_bn">
                                    <div class="form-control-feedback">@if($errors->first('text_bn')!=null) {{ $errors->first('text_bn')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('comming_soon')!=null) has-danger @endif">
                                <label for="comming_soon" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Comming Soon?</label>
                                <div class="col-md-12 col-lg-8">
                                    <input class="form-control form-control-lg like-field form-control-danger" type="text" value="{{old('comming_soon', $item->comming_soon)}}" id="comming_soon" name="comming_soon">
                                    <div class="form-control-feedback">@if($errors->first('comming_soon')!=null) {{ $errors->first('comming_soon')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row @if($errors->first('icon')!=null) has-danger @endif">
                                <label for="icon" class="col-md-12 col-lg-4 col-form-label col-form-label-lg-custom col-form-label-md-max-full lable-site needed">Icon</label>
                                <div class="col-md-12 col-lg-8">
                                    <div class="input-file">
                                        <img width="48px" src="{{Storage::url($item->icon)}}" />
                                        <input type="file" name="icon" id="icon" class="file-input">
                                        <span class="upload-icon">
                                            <i class="icofont icofont-upload-alt"></i>
                                        </span>
                                    </div>
                                    <div class="form-control-feedback">@if($errors->first('icon')!=null) {{ $errors->first('icon')}} @endif</div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-lg fs-16 text-uppercase font-weight-semibold letter-spacing-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop